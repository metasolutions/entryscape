import PropTypes from 'prop-types';
import config from 'config';
import { Typography } from '@mui/material';
import { useTranslation } from 'commons/hooks/useTranslation';
import escoPermissionNLS from 'commons/nls/escoPermission.nls';
import './index.scss';

const Error = ({ header, body }) => {
  const themePath = config.get('theme.default.themePath');
  const t = useTranslation(escoPermissionNLS);

  return (
    <div className="escoError">
      <img
        src={new URL('images/error.svg', themePath).href}
        alt={t('errorImageAlt')}
      />
      <Typography variant="h1" className="escoError__header" gutterBottom>
        {header}
      </Typography>
      <p className="escoError__body">{body}</p>
    </div>
  );
};

Error.propTypes = {
  header: PropTypes.string.isRequired,
  body: PropTypes.string.isRequired,
};

export default Error;
