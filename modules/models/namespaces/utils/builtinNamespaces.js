import { RDF_TYPE_PREFIX } from 'models/utils/ns';
import { getExporter } from 'admin/configs/utils/exporters';
import { getNamespaceAbbreviation, isBuiltin } from './namespaceMetadata';
import { fieldDefinitions } from './fieldDefinitions';

export const NAMESPACE_ID_PREFIX = '_ns';
const NAMESPACE_BUILTIN = 'builtin';

const namespacesRegistry = {
  ical: 'http://www.w3.org/2002/12/cal/ical#',
  vcard: 'http://www.w3.org/2006/vcard/ns#',
  dcterms: 'http://purl.org/dc/terms/',
  skos: 'http://www.w3.org/2004/02/skos/core#',
  rdfs: 'http://www.w3.org/2000/01/rdf-schema#',
  rdf: 'http://www.w3.org/1999/02/22-rdf-syntax-ns#',
  owl: 'http://www.w3.org/2002/07/owl#',
  foaf: 'http://xmlns.com/foaf/0.1/',
  dc: 'http://purl.org/dc/elements/1.1/',
  xsd: 'http://www.w3.org/2001/XMLSchema#',
  dcat: 'http://www.w3.org/ns/dcat#',
  org: 'http://www.w3.org/ns/org#',
  gn: 'http://www.geonames.org/ontology#',
  locn: 'http://www.w3.org/ns/locn#',
  schema: 'http://schema.org/',
  ex: 'http://example.com/',
  gsp: 'http://www.opengis.net/ont/geosparql#',
  odrs: 'http://schema.theodi.org/odrs#',
};

export const builtinNamespacesAbbreviations = Object.keys(namespacesRegistry);

/**
 * Creates a built-in namespace prototype entry
 *
 * @param {object} context
 * @param {object} builtinNamespaceConfig
 * @returns {object}
 */
export const createBuiltInNamespaceEntry = (
  context,
  builtinNamespaceConfig
) => {
  const prototypeEntry = context.newEntry();
  const resourceURI = prototypeEntry.getResourceURI();
  const graph = prototypeEntry.getMetadata();
  graph.add(resourceURI, 'rdf:type', RDF_TYPE_PREFIX);
  graph.addD(resourceURI, 'esterms:builtin', 'true', 'xsd:boolean');

  fieldDefinitions.forEach((fieldDefinition) => {
    const getValue = () => {
      if (fieldDefinition.nodetype === 'LANGUAGE_LITERAL') {
        return { en: builtinNamespaceConfig[fieldDefinition.name] };
      }
      return builtinNamespaceConfig[fieldDefinition.name];
    };
    const exportToGraph = getExporter(fieldDefinition);
    if (exportToGraph) {
      exportToGraph({
        graph,
        resourceURI,
        value: getValue(),
        ...fieldDefinition,
      });
    }
  });

  return prototypeEntry;
};

/**
 * Gets the built-in namespace configurations
 *
 * @param {Function} translate
 * @returns {object}
 */
export const getBuiltinNamespaces = (translate) => {
  const builtinNamespacesConfigs = Object.entries(namespacesRegistry).map(
    ([abbreviation, namespaceUri]) => {
      const id = `${NAMESPACE_ID_PREFIX}-${abbreviation}`;
      return {
        id,
        label: abbreviation,
        abbreviation,
        fullTitle: translate(`${abbreviation}Title`),
        namespace: namespaceUri,
        description: translate(`${abbreviation}Description`),
        type: NAMESPACE_BUILTIN,
      };
    }
  );

  return builtinNamespacesConfigs;
};

export const getBuiltinNamespacesWithEntries = (
  context,
  namespaceEntries,
  translate
) => {
  const builtinEntries = namespaceEntries.filter((entry) => isBuiltin(entry));

  return getBuiltinNamespaces(translate).map((namespaceConfig) => {
    const importedEntry = builtinEntries.find(
      (builtinEntry) =>
        getNamespaceAbbreviation(builtinEntry) === namespaceConfig.abbreviation
    );
    const isImported = Boolean(importedEntry);

    return {
      ...namespaceConfig,
      entry: isImported
        ? importedEntry
        : createBuiltInNamespaceEntry(context, namespaceConfig),
      isImported,
    };
  });
};
