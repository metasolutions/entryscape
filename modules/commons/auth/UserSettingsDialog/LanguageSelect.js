import PropTypes from 'prop-types';
import { TextField, MenuItem, InputAdornment } from '@mui/material';
import { Translate as TranslateIcon } from '@mui/icons-material';
import { useTranslation } from 'commons/hooks/useTranslation';
import escoLayoutNLS from 'commons/nls/escoLayout.nls';
import { sortLanguages } from 'commons/locale';
import config from 'config';

const LanguageSelect = ({ language, handleChange, textFieldProps }) => {
  const languages = sortLanguages(config.get('locale.supported'));
  const t = useTranslation(escoLayoutNLS);

  return (
    <TextField
      select
      label={t('language')}
      value={language}
      onChange={handleChange}
      variant="outlined"
      InputProps={{
        startAdornment: (
          <InputAdornment position="start">
            <TranslateIcon color="secondary" />
          </InputAdornment>
        ),
        inputProps: {
          id: 'language-select-input',
        },
      }}
      InputLabelProps={{ htmlFor: 'language-select-input' }}
      {...textFieldProps}
    >
      {languages.map(({ lang, label }) => (
        <MenuItem key={lang} value={lang}>
          {label}
        </MenuItem>
      ))}
    </TextField>
  );
};

LanguageSelect.propTypes = {
  language: PropTypes.string,
  handleChange: PropTypes.func.isRequired,
  textFieldProps: PropTypes.objectOf(PropTypes.string),
};

export default LanguageSelect;
