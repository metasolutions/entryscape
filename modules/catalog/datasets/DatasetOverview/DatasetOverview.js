import LinkedDataBrowserDialog from 'commons/components/LinkedDataBrowserDialog';
import config from 'config';
import { VisualizationsProvider } from 'catalog/datasets/DatasetOverview/hooks/useVisualizations';
import Overview from 'commons/components/overview/Overview';
import {
  OverviewSection,
  overviewPropsPropType,
} from 'commons/components/overview';
import { ACTION_INFO_WITH_ICON } from 'commons/components/overview/actions';
import DistributionList from './DistributionList';
import VisualizationList from './VisualizationList';
import SuggestionList from './SuggestionList';
import useDatasetOverviewCommons, {
  nlsBundles,
} from './useDatasetOverviewCommons';
import { TEMPLATE_TAG } from '../tags';

const DatasetOverview = ({ overviewProps }) => {
  const {
    entry,
    descriptionItems,
    sidebarActions,
    rowActions,
    includeSuggestions,
    backLabel,
    renderPrimaryContent,
  } = useDatasetOverviewCommons();

  return (
    <VisualizationsProvider datasetEntry={entry}>
      <Overview
        {...overviewProps}
        backLabel={backLabel}
        headerAction={{
          ...ACTION_INFO_WITH_ICON,
          Dialog: LinkedDataBrowserDialog,
        }}
        entry={entry}
        nlsBundles={nlsBundles}
        descriptionItems={descriptionItems}
        sidebarActions={sidebarActions}
        rowActions={rowActions}
        renderPrimaryContent={renderPrimaryContent}
        tags={[TEMPLATE_TAG]}
      >
        <OverviewSection>
          <DistributionList entry={entry} />
        </OverviewSection>
        {config.get('catalog.includeVisualizations') && (
          <OverviewSection>
            <VisualizationList entry={entry} />
          </OverviewSection>
        )}

        {includeSuggestions ? (
          <OverviewSection>
            <SuggestionList entry={entry} />
          </OverviewSection>
        ) : null}
      </Overview>
    </VisualizationsProvider>
  );
};

DatasetOverview.propTypes = {
  overviewProps: overviewPropsPropType,
};

export default DatasetOverview;
