import { useTranslation } from 'commons/hooks/useTranslation';
import { getIconFromActionId } from 'commons/actions';
import { ACTION_EDIT_ID } from 'commons/actions/actionIds';

/**
 * Checks whether the edit action has been excluded via list props
 *
 * @param {object} listProps
 * @returns {boolean}
 */
export const getIncludeEdit = (listProps) =>
  !listProps.excludeActions?.includes(ACTION_EDIT_ID);

export const useFilterListActions = ({
  listActions,
  listActionsProps,
  excludeActions,
  nlsBundles,
}) => {
  const translate = useTranslation(nlsBundles);
  return listActions
    .filter(({ id }) => !excludeActions.includes(id))
    .filter(({ isVisible }) => (isVisible ? isVisible(listActionsProps) : true))
    .map(
      ({
        id,
        isVisible,
        labelNlsKey,
        tooltipNlsKey,
        getNLSParam,
        ...props
      }) => ({
        id,
        ...(labelNlsKey ? { label: translate(labelNlsKey) } : {}),
        icon: getIconFromActionId(id),
        tooltip: tooltipNlsKey
          ? translate(
              tooltipNlsKey,
              getNLSParam?.(listActionsProps?.actionParams)
            )
          : undefined,
        ...props,
        ...listActionsProps,
      })
    );
};
