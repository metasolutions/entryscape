import PropTypes from 'prop-types';
import { Entry } from '@entryscape/entrystore-js';
import { List, ListItemButton, Grid, Typography } from '@mui/material';
import { getLabel } from 'commons/util/rdfUtils';
import { getPathFromViewName } from 'commons/util/site';
import { useTranslation } from 'commons/hooks/useTranslation';
import escaCatalogNLS from 'catalog/nls/escaCatalog.nls';
import escaCatalogSearchNLS from 'catalog/nls/escaCatalogSearch.nls';
import Tooltip from 'commons/components/common/Tooltip';
import { Link, useParams } from 'react-router-dom';
import { getNumberOfDatasetsInCatalog } from './data';
import 'commons/nav/components/Cards/index.scss';
import './index.scss';

const getEntryKey = (entry) => `${entry.getId()}-${entry.getContext().getId()}`;

const CatalogsList = ({ catalogs }) => {
  const { contextId } = useParams();

  const t = useTranslation([escaCatalogNLS, escaCatalogSearchNLS]);
  // merge reducers if needed
  return (
    <List component="nav" aria-labelledby={t('listNavAriaLabel')}>
      {catalogs?.map((entry) => (
        <div
          className="escoLinkWrapper escaCatalogListItemContainer"
          key={`catalog-${getEntryKey(entry)}`}
        >
          <ListItemButton
            key={getEntryKey(entry)}
            selected={contextId === entry.getContext().getId()}
            component={Link}
            classes={{ selected: 'escaCatalogListItem__selectedItem' }}
            to={getPathFromViewName('catalog__dataset__search', {
              contextId: entry.getContext().getId(),
            })}
          >
            <Grid
              container
              spacing={2}
              justifyContent="flex-start"
              alignItems="center"
            >
              <Grid item xs={10}>
                <Tooltip title={getLabel(entry)}>
                  <Typography
                    className="escaCatalogListItemLabel"
                    variant="body1"
                  >
                    {getLabel(entry)}
                  </Typography>
                </Tooltip>
              </Grid>
              <Grid
                item
                xs={2}
                aria-label={t(
                  'ariaLabelText',
                  getNumberOfDatasetsInCatalog(entry)
                )}
              >
                <Typography
                  className="escaCatalogListItemLabel__number"
                  variant="body1"
                >
                  {getNumberOfDatasetsInCatalog(entry)}
                </Typography>
              </Grid>
            </Grid>
          </ListItemButton>
        </div>
      ))}
    </List>
  );
};

CatalogsList.propTypes = {
  catalogs: PropTypes.arrayOf(PropTypes.instanceOf(Entry)),
};

export default CatalogsList;
