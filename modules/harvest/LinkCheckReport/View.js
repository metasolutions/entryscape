import { useESContext } from 'commons/hooks/useESContext';
import LinkCheckReport from './index';

const LinkCheckReportView = () => {
  const { context } = useESContext();

  return <LinkCheckReport contextId={context.getId()} />;
};

export default LinkCheckReportView;
