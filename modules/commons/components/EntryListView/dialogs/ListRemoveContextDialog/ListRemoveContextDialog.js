import PropTypes from 'prop-types';
import { Entry } from '@entryscape/entrystore-js';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogContent from '@mui/material/DialogContent';
import DialogActions from '@mui/material/DialogActions';
import escoListNLS from 'commons/nls/escoList.nls';
import {
  useTranslation,
  nlsBundlesPropType,
} from 'commons/hooks/useTranslation';
import { useUserState } from 'commons/hooks/useUser';
import { deleteContext } from 'commons/util/context';
import { useListModel, DELETE } from 'commons/components/ListView';
import { useSnackbar, ERROR } from 'commons/hooks/useSnackbar';
import useAsync, { PENDING } from 'commons/hooks/useAsync';
import LoadingButton from 'commons/components/LoadingButton';

const ListRemoveContextDialog = ({ entry, closeDialog, nlsBundles = [] }) => {
  const [, dispatch] = useListModel();
  const { userEntry } = useUserState();
  const translate = useTranslation([...nlsBundles, escoListNLS]);
  const [addSnackbar] = useSnackbar();
  const { runAsync, status } = useAsync();

  const handleRemoveRequest = () =>
    runAsync(
      deleteContext(entry, userEntry)
        .then(() => {
          closeDialog();
          dispatch({ type: DELETE });
          addSnackbar({ message: translate('contextRemoveSuccess') });
        })
        .catch((err) => {
          console.error(err);
          addSnackbar({ message: translate('contextRemoveFail'), type: ERROR });
        })
    );

  return (
    <Dialog open onClose={closeDialog} fullWidth maxWidth="xs">
      <DialogContent>
        <div>{translate('removeContext')}</div>
      </DialogContent>
      <DialogActions>
        <Button onClick={closeDialog} variant="text">
          {translate('cancel')}
        </Button>
        <LoadingButton
          autoFocus
          onClick={handleRemoveRequest}
          loading={status === PENDING}
          color="primary"
        >
          {translate('removeButtonLabel')}
        </LoadingButton>
      </DialogActions>
    </Dialog>
  );
};

ListRemoveContextDialog.propTypes = {
  entry: PropTypes.instanceOf(Entry),
  closeDialog: PropTypes.func,
  nlsBundles: nlsBundlesPropType,
};

export default ListRemoveContextDialog;
