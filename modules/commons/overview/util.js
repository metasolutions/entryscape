import { entrystore } from 'commons/store';
import { Context } from '@entryscape/entrystore-js';
import { getMultipleDateFormats } from 'commons/util/date';
import { getDescription } from 'commons/util/rdfUtils';
import { getEntryLabel } from 'commons/util/entry';
import {
  getPathFromViewName,
  getRouteFromView,
  getModuleNameOfCurrentView,
} from 'commons/util/site';
import Lookup from 'commons/types/Lookup';
import { EntityType } from 'entitytype-lookup';
import {
  getEntityTypesIncludingRefined,
  getBuiltinEntityTypes,
} from 'commons/types/utils/entityType';

const defaultQuery = async (context, entityType) => {
  const searchList = entrystore
    .newSolrQuery()
    .context(context)
    .rdfType(entityType.get('rdfType'))
    .list('1');

  await searchList.getEntries();
  return searchList.getSize();
};

/**
 *
 * @param {EntityType} entityTypes
 * @param {object} viewParams
 * @param {Context} context
 * @param {Function} translate
 * @returns {Promise<object[]>}
 */
const getStatsList = async (entityTypes, viewParams, context, translate) => {
  // only show entity types available in secondary nav
  const typesIncludingRefined = getEntityTypesIncludingRefined(entityTypes);
  const overviewEntityTypes = typesIncludingRefined.filter((entityType) => {
    return (
      getRouteFromView(entityType.get('listview')) &&
      !entityType.get('refines') &&
      !entityType.get('hideFromOverview')
    );
  });

  const statsListPromises = overviewEntityTypes.map(async (entityType) => {
    const queryCount = await defaultQuery(context, entityType);

    const name = entityType.get('name');
    const link = getPathFromViewName(entityType.get('listview'), viewParams);

    return {
      key: name,
      label: translate(`${name}Label`) || `${name}Label`,
      value: queryCount,
      link,
      isEmpty: queryCount === 0,
      isVisible: true,
      color: entityType.get('main') ? 'red' : 'blue',
    };
  });

  return Promise.all(statsListPromises);
};

/**
 * Extracts overview items from config
 *
 * @param {string} viewParams
 * @param {Context} context
 * @param {Function} translate
 * @returns {Promise}
 */

const getOverviewData = async (viewParams, context, translate, userInfo) => {
  const data = {};
  const entry = await context.getEntry();
  const modificationDate = entry.getEntryInfo().getModificationDate();
  const creationDate = entry.getEntryInfo().getCreationDate();
  const modificationDateFormats = getMultipleDateFormats(modificationDate);
  const creationDateFormats = getMultipleDateFormats(creationDate);
  const projectType = Lookup.getProjectTypeInUse(context, true);
  const moduleName = getModuleNameOfCurrentView();
  const builtinEntityTypes = getBuiltinEntityTypes(userInfo, moduleName);
  const entityTypesInContext = await Lookup.getEntityTypesInContext(
    context,
    userInfo,
    true
  );
  data.entityTypes = projectType?.getIncludeBuiltins()
    ? builtinEntityTypes
    : entityTypesInContext;
  data.description = getDescription(entry);
  data.title = getEntryLabel(entry);
  data.entry = entry;
  data.statsList = await getStatsList(
    data.entityTypes,
    viewParams,
    context,
    translate
  );

  data.modsList = [
    {
      key: 'create',
      label: translate('createdLabel'),
      value: creationDateFormats.short,
    },
    {
      key: 'update',
      label: translate('lastUpdatedLabel'),
      value: modificationDateFormats.short,
    },
    {
      key: 'projectType',
      label: translate('projectTypeLabel'),
      value: projectType?.source.name || translate('defaultProjectType'),
    },
  ];
  return data;
};

export default getOverviewData;
