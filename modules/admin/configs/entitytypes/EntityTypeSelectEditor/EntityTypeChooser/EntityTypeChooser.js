/* eslint-disable react/jsx-props-no-spreading */
import { useState } from 'react';
import PropTypes from 'prop-types';
import { Box, Button } from '@mui/material';
import InfoIcon from '@mui/icons-material/Info';
import BuiltinIcon from '@mui/icons-material/Settings';
import CustomIcon from '@mui/icons-material/Widgets';
import { useTranslation } from 'commons/hooks/useTranslation';
import {
  ListActions,
  List,
  ListItem,
  ListItemAction,
  ListItemText,
  ListHeader,
  ListHeaderItemText,
  ListHeaderItemSort,
  ListItemIcon,
  ListItemIconButton,
  ListSearch,
  ListView,
  ListPagination,
  useListQuery,
  withListModelProvider,
  useListModel,
} from 'commons/components/ListView';
import { RefreshButton } from 'commons/components/EntryListView';
import FieldsLinkedDataBrowserDialog from 'commons/components/FieldsLinkedDataBrowserDialog';
import ListActionDialog from 'commons/components/common/dialogs/ListActionDialog';
import escoListNLS from 'commons/nls/escoList.nls';
import esadEntityTypeNLS from 'admin/nls/esadEntityType.nls';
import esadTypeNLS from 'admin/nls/esadType.nls';
import {
  ENTITY_TYPE_ID_PREFIX,
  ENTITY_TYPE_BUILTIN,
} from 'admin/configs/utils/namespace';
import { nlsBundles } from 'commons/components/EntryListView/lists/UsersList/actions';
import { typeConfigToGraph } from '../../../utils/exporters';

const NLS_BUNDLES = [escoListNLS, esadEntityTypeNLS, esadTypeNLS];
const SEARCH_FIELDS = ['label'];

const EntityTypeChooser = ({
  onSelect,
  entityTypes,
  closeDialog,
  presenters,
}) => {
  const [entityTypeInfo, setEntityTypeInfo] = useState(null);
  const t = useTranslation(NLS_BUNDLES);
  const [{ search }] = useListModel();

  const { result: entityTypeListItems, size } = useListQuery({
    items: entityTypes,
    searchFields: SEARCH_FIELDS,
  });

  const handleSelectEntityType = (id) => {
    onSelect(id);
    closeDialog();
  };

  return (
    <ListActionDialog
      id="select-entitytype-chooser"
      closeDialog={closeDialog}
      closeDialogButtonLabel={t('close')}
      title={t('selectEntityTypeTitle')}
      fixedHeight
    >
      <Box maxWidth="924px">
        <ListView
          status="resolved"
          search={search}
          size={size}
          nlsBundles={NLS_BUNDLES}
        >
          <ListActions>
            <ListSearch />
            <RefreshButton />
          </ListActions>
          <ListHeader>
            <ListHeaderItemText xs={1} text="" />
            <ListHeaderItemSort
              xs={11}
              sortBy="label"
              text={t('labelListHeader')}
            />
          </ListHeader>
          <List>
            {entityTypeListItems.map((entityTypeListItem) => {
              const { id, label, type } = entityTypeListItem;
              const isBuiltin = type === ENTITY_TYPE_BUILTIN;
              return (
                <ListItem key={id}>
                  <ListItemIcon
                    Icon={isBuiltin ? BuiltinIcon : CustomIcon}
                    tooltip={isBuiltin ? t('builtinLabel') : t('customLabel')}
                  />
                  <ListItemText xs={8} primary={label} />
                  <ListItemAction xs={2}>
                    <ListItemIconButton
                      onClick={() => setEntityTypeInfo(entityTypeListItem)}
                      title={t('showTypeInfoDialogTitle')}
                    >
                      <InfoIcon />
                    </ListItemIconButton>
                  </ListItemAction>
                  <ListItemAction>
                    <Button
                      variant="text"
                      onClick={() => handleSelectEntityType(id)}
                    >
                      {t('selectEntityTypeLabel')}
                    </Button>
                  </ListItemAction>
                </ListItem>
              );
            })}
          </List>
          <ListPagination />
          {entityTypeInfo ? (
            <FieldsLinkedDataBrowserDialog
              entry={entityTypeInfo.entry}
              fields={presenters}
              title={t('typeInfoDialogTitle')}
              closeDialog={() => setEntityTypeInfo(null)}
              nlsBundles={nlsBundles}
              {...typeConfigToGraph({
                entry: entityTypeInfo.entry,
                typeConfig: entityTypeInfo.config,
                idPrefix: ENTITY_TYPE_ID_PREFIX,
                fields: presenters,
              })}
            />
          ) : null}
        </ListView>
      </Box>
    </ListActionDialog>
  );
};

EntityTypeChooser.propTypes = {
  closeDialog: PropTypes.func.isRequired,
  onSelect: PropTypes.func.isRequired,
  entityTypes: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string,
      label: PropTypes.string,
      type: PropTypes.string,
    })
  ),
  presenters: PropTypes.shape({}),
};

export default withListModelProvider(EntityTypeChooser, () => ({
  sort: { field: 'label', order: 'asc' },
}));
