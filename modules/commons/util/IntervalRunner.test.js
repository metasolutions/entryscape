import IntervalRunner from './IntervalRunner';

const INTERVAL = 100;

jest.useFakeTimers();

describe('IntervalRunner', () => {
  let mockCallback;
  beforeEach(() => {
    mockCallback = jest.fn();
  });

  afterEach(() => {
    jest.clearAllTimers();
  });

  test('start method starts execution', () => {
    const items = [1, 2, 3];
    const runner = new IntervalRunner(items, INTERVAL, mockCallback);

    runner.start();
    jest.advanceTimersByTime(INTERVAL);

    expect(mockCallback).toHaveBeenCalledWith(items);
  });

  test('stop method stops execution', () => {
    const items = [1, 2, 3];
    const runner = new IntervalRunner(items, INTERVAL, mockCallback);

    runner.start();
    runner.stop();
    jest.advanceTimersByTime(INTERVAL);

    expect(mockCallback).toHaveBeenCalledWith(items);
    expect(runner.isRunning()).toBe(false);
  });

  test('setItems updates items and callback runs with with new items', () => {
    const items = [1, 2, 3];
    const runner = new IntervalRunner(items, INTERVAL, mockCallback);

    const newItems = [4, 5, 6];
    runner.setItems(newItems);

    jest.advanceTimersByTime(INTERVAL);

    expect(mockCallback).toHaveBeenCalledWith(newItems);
  });

  test('setItems stops execution if items are empty', () => {
    const items = [1, 2, 3];
    const runner = new IntervalRunner(items, INTERVAL, mockCallback);

    runner.setItems([]);

    expect(runner.isRunning()).toBe(false);
  });

  test('setItems starts execution if items are not empty', () => {
    const items = [];
    const runner = new IntervalRunner(items, INTERVAL, mockCallback);

    runner.setItems([1, 2, 3]);

    expect(runner.isRunning()).toBe(true);
  });
});
