/* eslint-disable max-len */
export const CATALOGS_LISTVIEW = 'catalog__list';
export const SUGGESTIONS_LISTVIEW = 'catalog__suggestions';
export const DATASETS_LISTVIEW = 'catalog__datasets';
export const DATASERVICES_LISTVIEW = 'catalog__dataservices';
export const SHOWCASES_LISTVIEW = 'catalog__showcases';
export const IDEAS_LISTVIEW = 'catalog__ideas';
export const PUBLISHERS_LISTVIEW = 'catalog__publishers';
export const CONTACTS_LISTVIEW = 'catalog__contacts';
export const DOCUMENTS_LISTVIEW = 'catalog__documents';

export const CATALOG_OVERVIEW_NAME = 'catalog__overview';
export const SUGGESTION_OVERVIEW_NAME = 'catalog__suggestions__suggestion';
export const DATASET_OVERVIEW_NAME = 'catalog__datasets__dataset';
export const DATA_SERVICE_OVERVIEW_NAME = 'catalog__dataservices__dataservice';
export const SHOWCASE_OVERVIEW_NAME = 'catalog__showcases__showcase';
export const IDEA_OVERVIEW_NAME = 'catalog__ideas__idea';
export const PUBLISHER_OVERVIEW_NAME = 'catalog__publishers__publisher';
export const CONTACT_OVERVIEW_NAME = 'catalog__contacts__contact';
export const DOCUMENT_OVERVIEW_NAME = 'catalog__documents__document';

export default {
  catalog: {
    // Determines whether file scheme URIs are allowed e.g. on the distribution access URL
    allowFileURI: false,
    /**
     * RDForms catalog template id
     *
     * @type {string}
     * @memberof catalog
     */
    catalogTemplateId: undefined,

    /**
     * RDForms dataset template id
     *
     * @type {string}
     * @memberof catalog
     */
    datasetTemplateId: undefined,

    /**
     * RDForms distribution template id
     *
     * @type {string}
     * @memberof catalog
     */
    distributionTemplateId: undefined,
    /**
     * RDForms contact point template id
     *
     * @type {string}
     * @memberof catalog
     */
    contactPointTemplateId: 'dcat:contactPoint',

    /**
     * RDForms document template id
     * @type {string}
     * @memberof catalog
     */
    documentTemplateId: 'esc:Documentish',

    /**
     * RDForms contact template id
     * @type {string}
     * @memberof catalog
     */
    contactTemplateId: 'dcat:contactPoint', // @todo perhaps not needed and is a duplicate of contactPointTemplateId. But used in code

    /**
     * RDForms agent template id
     * @type {string}
     * @memberof catalog
     */
    agentTemplateId: 'dcat:foaf:Agent',

    /**
     * RDForms template id for dataservices
     * @type {string}
     * @memberof catalog
     */
    dataServiceTemplateId: 'dcat:DataService',
    /**
     * RDForms result template id
     * @type {string}
     * @memberof catalog
     */
    datasetResultTemplateId: 'esc:Results',

    /**
     * RDForms idea template id
     * @type {string}
     * @memberof catalog
     */
    datasetIdeaTemplateId: 'esc:Ideas',

    /**
     * RDForms suggestion template id
     * @type {string}
     * @memberof catalog
     */
    suggestionTemplateId: 'esc:Suggestion',

    /**
     * RDForms format template id
     * @type {string}
     * @memberof catalog
     */
    formatTemplateId: 'dcat:format-group_di',

    /**
     * @type {string}
     * @memberof catalog
     */
    formatProperty: 'dcterms:format',

    // default rdf types

    /**
     * Default RDF agent type
     * @type {string}
     * @memberof catalog
     */
    defaultAgentType: 'foaf:Agent',

    /**
     * Specifies who is allowed to create catalogs. Usually set to a group's entry ID.
     * @type {string}
     * @memberof catalog
     */
    catalogCreationAllowedFor: '_users',

    /**
     * Specifies who may (un)publish datasets
     * @type {string}
     * @memberof catalog
     */
    publishDatasetAllowedFor: '_users',

    /**
     * @type {string}
     * @memberof catalog
     */
    previewURL: '#view=dataset&resource=${url}', // eslint-disable-line no-template-curly-in-string

    // feature toggles

    /**
     * Include/exclude download action in catalog list row
     * @type {boolean}
     * @memberof catalog
     */
    includeExportOption: true,

    /**
     * Include/exclude embed action in catalog list row
     * @type {boolean}
     * @memberof catalog
     */
    includeEmbeddOption: true,

    /**
     * Include/exclude publisher view in the secondary navigation
     * @type {boolean}
     * @memberof catalog
     */
    includePublishers: true,

    /**
     * Include/exclude contact view in the secondary navigation
     * @type {boolean}
     * @memberof catalog
     */
    includeContacts: true,

    /**
     * Include/exclude document view in the secondary navigation
     * @type {boolean}
     * @memberof catalog
     */
    includeDocuments: true,

    /**
     * @type {boolean}
     * @memberof catalog
     */
    includeShowcases: true,
    /**
     * @type {boolean}
     * @memberof catalog
     */
    includeIdeas: true,

    /**
     * @type {boolean}
     * @memberof catalog
     */
    includeStatistics: true,
    /**
     * @type {boolean}
     * @memberof catalog
     */
    includeTableView: true,
    /**
     * @type {boolean}
     * @memberof catalog
     */
    includePreparations: true,

    /**
     * @type {boolean}
     * @memberof catalog
     */
    includeVisualizations: false,

    /**
     * @type {boolean}
     * @memberof catalog
     */
    includeMapServiceVisualizations: false,
    /**
     * @type {boolean}
     * @memberof catalog
     */
    includeOverview: true,
    /**
     * Determines whether dataset series are available
     *
     * @type {boolean}
     */
    includeDatasetSeries: true,

    /**
     * Determines whether or which filters are visible in catalog. Using a boolean shows/hides
     * all filters, while an array containing the names of the filters to exclude can
     * also be provided.
     *
     * @type {boolean|string[]}
     */
    excludeFilters: false,

    /**
     * @type {string}
     * @memberof catalog
     */
    startView: 'catalog__overview',
    /**
     * @type {boolean}
     * @memberof catalog
     */
    distributionTemplateCreate: false,
    /**
     * @type {boolean}
     * @memberof catalog
     */
    excludeEmptyCatalogsInSearch: true,
    /**
     * @type {boolean}
     * @memberof catalog
     */
    allowInternalDatasetPublish: false,

    /**
     * @type {boolean}
     * @memberof catalog
     */
    includeInternalPublish: false,

    /**
     * @type {boolean}
     * @memberof catalog
     */
    includeListSizeByDefault: false,

    /**
     * Toggles ability to create a catalog without a publisher
     * @type {boolean}
     * @memberof catalog
     */
    createWithoutPublisher: false,

    /**
     * empty string works as no limit
     * @type {string|number}
     * @memberof catalog
     */
    catalogLimit: '',

    /**
     * A pointer to a text file in the configuration directory.
     * @type {string}
     * @memberof catalog
     */
    catalogLimitDialog: '',

    /**
     * empty string works as no limit
     * @type {string|number}
     * @memberof catalog
     */
    datasetLimit: '',

    /**
     * A pointer to a text file in the configuration directory.
     * @type {string}
     * @memberof catalog
     */
    datasetLimitDialog: '',

    /**
     * A pointer to a text file in the configuration directory.
     * @type {string}
     * @memberof catalog
     */
    disallowCatalogPublishingDialog: '',

    /**
     * A pointer to a text file in the configuration directory.
     * @type {string}
     * @memberof catalog
     */
    disallowCatalogCollaborationDialog: '',

    /**
     * A pointer to a text file in the configuration directory.
     * @type {string}
     * @memberof catalog
     */
    disallowFileuploadDistributionDialog: '',

    /**
     * @type {boolean}
     * @memberof catalog
     */
    excludeFileuploadDistribution: false,

    /**
     * @type {string}
     * @memberof catalog
     */
    previewURLNewWindow: '',

    /**
     * @type {number}
     * @memberof catalog
     */
    maxFileSizeForAPI: 25000000,

    /**
     * Store hosting specifications that can be imported as link reference entries.
     * Also used as flag for remote specification import functionality.
     * @type {string}
     */
    remoteSpecEndpoint: '',

    /**
     * Store hosting dataset templates that can be used to create datasets or suggestions.
     * Also used as flag for dataset template functionality.
     * @type {string}
     */
    datasetTemplateEndpoint: '',

    /**
     * Default selected catalog on the dataset template filters' context dropdown.
     * Should be a catalog ID or empty. If specified, the dropdown autohides currently.
     * @type {string}
     */
    datasetTemplateDefaultCatalog: '',

    /**
     * Default RDF type used to query dataset templates.
     */
    datasetTemplateRDFType: 'esterms:DatasetTemplate',

    /**
     * Default RDF type used to query catalog templates.
     */
    catalogTemplateRDFType: 'esterms:CatalogTemplate',

    /**
     * The checklist consists of a task name, title (short label), short description (label) and detailed description. These are all provided in different languages.
     * @typedef {{en: string, sv: string, da: string, de: string}} TitleTranslationObj
     * @typedef {{sv: string, da: string, en: string, de: string}} TranslationObj
     * @typedef {{name: string, shortLabel: TitleTranslationObj, label: TranslationObj, description: TranslationObj, mandatory: boolean}} checklistObj
     * @type checklistObj
     * @memberof catalog
     */
    checklist: [
      {
        name: 'noPrivacyIssues',
        shortLabel: {
          en: 'Privacy',
          sv: 'Sekretess',
          da: 'Fortrolig',
          de: 'Datenschutz',
        },
        label: {
          sv: 'Inga sekretessbelagda eller personuppgifter ingår i datamängden',
          da: 'Datasættet indeholder ikke fortrolige eller personlige information',
          en: 'The dataset does not contain confidential or personal information',
          de: 'Der Datensatz beinhaltet keine vertraulichen oder persönlichen Informationen',
        },
        description: {
          sv: 'Om genomgång av datamängd visat på hinder iform av sekretessbelagd information eller personuppgifter så redovisas detta som kommentarer på datamängden. Eventuella alternativ för publicering av delar av datamängden ska också redovisas.',
          da: 'Hvis gennemgangen af datasættet frembringer forhindringer såsom fortrolighed eller personlige informationinformation skal dette dokumenteres som kommentarer på datasættet. Et eventuelt alternativ er publicering af dele af datasættet skal også beskrives i kommentarerne.',
          en: 'If the review of the dataset revealed obstacles such as confidential or personal information this is documented as comments on the dataset. Possible alternatives such as partial publication of the dataset should also be presented as comments.',
          de: 'Falls beim Durchsehen des Datensatzes Hürden wie vertrauliche oder persönliche Informationen offenlegt werden, werden diese als Kommentar am Datensatz dokumentiert. Mögliche Alternativen wie eine teilweise Veröffentlichung des Datensatzes können ebenfalls als Kommentare dargestellt werden.',
        },
        mandatory: true,
      },
      {
        name: 'clearLicense',
        shortLabel: {
          en: 'License',
          sv: 'Upphovsrätt',
          da: 'Uphavsrett',
          de: 'Lizenz',
        },
        label: {
          sv: 'Upphovsrätten är klargjord',
          da: 'Ophavsretten er sikret',
          en: 'Copyright is cleared',
          de: 'Urheberrecht ist geklärt',
        },
        description: {
          sv: 'Det upphovsrättskydd som gäller för datamängden har klargjorts. Om erkännande krävs i samband med vidarutnyttjande är det också viktigt att det tydliggörs, redovisa i så fall ägare av upphovsrätten och annan relevant information i en kommentar på datamängden.',
          da: 'Den ophavsret og ejerrettigheder på datasættet som er sikret. Hvis anerkendelse af ophavsret eller rettighedsejer er nødvendig i forbindelse med genbrug eller videreanvendelse, er det vigtigt at navnet på rettighedsindehaveratt samt andre relevante informationer er beskrevet i kommentarerne.',
          en: 'The copyright and ownership rights of the dataset have been cleared. If recognition is required in connection with re-use, it is important that the name of the copyright holder and other relevant information are provided as comments.',
          de: 'Das Urheberrecht und die Eigentümerrechte des Datensatzes wurden geklärt. Falls Wiedererkennung benötigt wird in Verbindung mit der Wiederverwendung, ist es wichtig, dass der Name des Urheberrechtsinhabers und andere relevante Informationen als Kommentare bereitgestellt werden.',
        },
        mandatory: true,
      },
      {
        name: 'clearOwnership',
        shortLabel: {
          en: 'Ownership',
          sv: 'Ägarskap',
          da: 'Ejendom',
          de: 'Eigentum',
        },
        label: {
          sv: 'Dataägare i organisationen är identifierad',
          da: 'Dataejere indenfor organisationen er identificeret',
          en: 'Data owner within the organization is identified',
          de: 'Eigentümer der Daten innerhalb der Organisation ist identifiziert',
        },
        description: {
          sv: 'Ansvar för datamängden i organisationen är känd. Kontaktpunkt, i form av en person eller funktionsadress lämnas som kommentar på datamängden.',
          da: 'Ansvar for datasættet i organisationen er kendt. Kontaktpunkt, i form af en person eller funktionspostkasse beskrives i kommentarerne på datasættet.',
          en: 'The responsible for the dataset is known within the organization. The contact point, in the form of an email address, should be provided as comment on the dataset.',
          de: 'Die Verantwortlichen für den Datensatz sind innerhalb der Organisation bekannt. Der Kontakt in Form einer E-Mail-Adresse sollte als Kommentar im Datensatz bereitgestellt werden.',
        },
        mandatory: true,
      },
      {
        name: 'digitallyAccessible',
        shortLabel: {
          en: 'Digital',
          sv: 'Digitalt',
          da: 'Digitald',
          de: 'Digital',
        },
        label: {
          sv: 'Datamängden är tillgänglig digitalt',
          da: 'Datasættet er tilgængeligt digitalt',
          en: 'The dataset can be digitally accessed',
          de: 'Auf den Datensatz kann digital zugegriffen werden',
        },
        description: {
          sv: 'Datamängden finns tillgänglig i kända format. Hur man kommer åt datamängden i respektive format är också klargjort.',
          da: 'Datasættet findes tilgængeligt i kendte formatter. Det er gjort tydeligt hvordan man får adgang til datasættets respektive formatter.',
          en: "The dataset is available in known formats. It is clear how the dataset's respective formats can be accessed.",
          de: 'Der Datensatz ist in verschiedenen Formaten verfügbar. Es ist klar wie auf die entsprechenden Formate zugegriffen werden kann.',
        },
      },
      {
        name: 'addedValue',
        shortLabel: {
          en: 'Added value',
          sv: 'Mervärde',
          da: 'Merværdi',
          de: 'Mehrwert',
        },
        label: {
          sv: 'Publicering av datamängden innebär ett tydligt mervärde',
          da: 'Publicering af datasættet har en tydelig merværdi',
          en: 'The publication of the dataset implies a clear added value',
          de: 'Die Veröffentlichung des Datensatzes stellt einen Mehrwert dar',
        },
        description: {
          sv: 'Datamängden medför ökad transparens, innovation eller effektivisering. Effektivisering omfattar förbättringar inom såväl den egna organisationen som externt.',
          da: 'Datasættet medfører øget transparens, innovation eller effektivisering. Effektivisering omfatter forbedringer både indenfor egen organisation såvel som udenfor.',
          en: 'The dataset contributes to increased transparency, innovation or efficiency. Efficiency includes improvements both within the own organization as well as externally.',
          de: 'Der Datensatz trägt zu steigender Transparenz, Innovation oder Effizienz bei. Effizienz beinhaltet Verbesserungen innerhalb der Organisation als auch externer Art.',
        },
      },
      {
        name: 'demand',
        shortLabel: {
          en: 'Demand',
          sv: 'Efterfråga',
          da: 'Efterspørgsel',
          de: 'Nachfrage',
        },
        label: {
          sv: 'Datamängden är efterfrågad',
          da: 'Datasættet er efterspurgt',
          en: 'The dataset is sought after',
          de: 'Nach dem Datensatz wird gesucht',
        },
        description: {
          sv: 'Datamängden är efterfrågad antingen inom den egna organisationen eller externt. Detta innebär att minst en målgrupp till datamängden är identifierad.',
          da: 'Datasættet er efterspurgt indenfor egen organisation eller udenfor. Dette indebærer at der er identificeret mindst en målgruppe til datasættet.',
          en: 'The dataset is sought after within the own organization or externally. This means that at least one target group for the dataset has been identified.',
          de: 'Nach dem Datensatz wird innerhalb der Organisation oder von externer Seite aus gesucht. Das bedeutet das mindestens eine Zielgruppe für den Datensatz identifiziert wurde.',
        },
      },
      {
        name: 'formatDemand',
        shortLabel: {
          en: 'Formats',
          sv: 'Format',
          da: 'Formatter',
          de: 'Formate',
        },
        label: {
          sv: 'Efterfrågade format eller protokoll stöds',
          da: 'Efterspurgte formatter eller protokoller er understøttet',
          en: 'Requested formats or protocols are supported',
          de: 'Angefragte Formate oder Protokolle werden unterstützt',
        },
        description: {
          sv: 'Datamängden är tillgänglig i de format och protokoll som efterfrågas. Om nya format efterfrågats görs en kommentar om vilka på datamängden, t.ex. om API önskas för målgruppen.',
          da: 'Datasættet er tilgængeligt i de formatter og protokoller som er efterspurgt. Det skal registreres som kommentarer hvis yderligere formatter eller protokoller er efterspurgt, f.eks. hvis et API er efterspurgt af en specifik målgruppe.',
          en: 'The dataset is available in the formats and protocols that have been requested. It should be noted as a comment if additional formats or protocols are requested, e.g., if an API is requested by a target group.',
          de: 'Der Datensatz ist verfügbar in den Formaten und Protokollen die angefragt wurden. Es sollte eine Notiz als Kommentar gemacht werden, falls zusätzliche Formate oder Protokolle angefragt werden, zum Beispiel falls eine API durch eine Zielgruppe angefragt wurde.',
        },
      },
      {
        name: 'addedCost',
        shortLabel: {
          en: 'Cost',
          sv: 'Kostnader',
          da: 'Omkostninger',
          de: 'Kosten',
        },
        label: {
          sv: 'Resurser, kostnader och effektiviseringar är kända',
          da: 'Resurser, omkostninger og effektiviseringar er kendte',
          en: 'Resources, costs and increased efficiency are known',
          de: 'Ressourcen, Kosten und eine ansteigende Effizienz sind bekannt',
        },
        description: {
          sv: 'Resurser för publicering och eventuella engångskostnader är kända och sammanvägda med eventuell intern effektivisering. Nya kostnader för framtagning eller underhåll av datamängd lämnas som kommentar.',
          da: 'Nødvendigte resurser or publicering og eventuelle engangsomkostninger er kendte vurderet i forhold til mulige forbedringer af intern effektivitet. Yderligere omkostninger for at bevare eller vedligeholde datasættet er registreret i en kommentar.',
          en: 'Resources for publication and any one-time costs are known and weighted with possible improvements of internal efficiency. Additional costs for obtaining or maintaining the dataset are provided as comment.',
          de: 'Ressourcen zum Veröffentlichen und alle einmaligen Kosten sind bekannt und gewichtet nach möglichen Verbesserungen der internen Effizienz. Zusätzliche Kosten zum Erhalt oder Pflege des Datensatzes werden als Kommentar zur Verfügung gestellt.',
        },
      },
      {
        name: 'maintenancePlan',
        shortLabel: {
          en: 'Maintenance',
          sv: 'Underhåll',
          da: 'Vedligehold',
          de: 'Pflege',
        },
        label: {
          sv: 'Plan för underhåll av datamängden finns',
          da: 'Plan for vedligehold af datasættet findes',
          en: 'A plan for maintenance of the dataset is available',
          de: 'Ein Konzept für die Pflege des Datensatzes ist verfügbar',
        },
        description: {
          sv: 'Plan och metod för underhåll av datamängden i den frekvens som krävs är undersökt och klargjort.',
          da: 'Plan og metode for vedligeholdelse af datasættet i den frekvens som er påkrævet er undersøgt og tydeliggjort.',
          en: 'Plan and method for maintenance of the dataset in the required frequency have been investigated and clarified.',
          de: 'Konzept und Methode für die Pflege des Datensatzes in der benötigten Frequenz sind untersucht und geklärt wurden.',
        },
      },
    ],
  },
  itemstore: {
    /**
     * Names of available choosers
     *
     * @type {string[]}
     * @memberof itemstore
     */
    choosers: ['EntryChooser', 'GeonamesChooser', 'GeoChooser'],
    /**
     * Map projections in order of importance
     *
     * @type {object[]}
     * @memberof itemstore
     */
    projections: [
      {
        code: 'EPSG:3006',
        boundingBox: [10.03, 54.96, 24.17, 69.07],
        definition:
          '+proj=utm +zone=33 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +axis=neu +no_defs',
      },
      {
        code: 'EPSG:3007',
        boundingBox: [10.03, 54.96, 24.17, 69.07],
        definition:
          '+proj=tmerc +lat_0=0 +lon_0=12 +k=1 +x_0=150000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +axis=neu +no_defs',
      },
      {
        code: 'EPSG:3008',
        boundingBox: [10.03, 54.96, 24.17, 69.07],
        definition:
          '+proj=tmerc +lat_0=0 +lon_0=13.5 +k=1 +x_0=150000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +axis=neu +no_defs',
      },
      {
        code: 'EPSG:3009',
        boundingBox: [-16.1, 32.88, 40.18, 84.17],
        definition:
          '+proj=tmerc +lat_0=0 +lon_0=15 +k=1 +x_0=150000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +axis=neu +no_defs',
      },
      {
        code: 'EPSG:3010',
        boundingBox: [-16.1, 32.88, 40.18, 84.17],
        definition:
          '+proj=tmerc +lat_0=0 +lon_0=16.5 +k=1 +x_0=150000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +axis=neu +no_defs',
      },
      {
        code: 'EPSG:3011',
        boundingBox: [10.03, 54.96, 24.17, 69.07],
        definition:
          '+proj=tmerc +lat_0=0 +lon_0=18 +k=1 +x_0=150000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +axis=neu +no_defs',
      },
      {
        code: 'EPSG:3012',
        boundingBox: [10.03, 54.96, 24.17, 69.07],
        definition:
          '+proj=tmerc +lat_0=0 +lon_0=14.25 +k=1 +x_0=150000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +axis=neu +no_defs',
      },
      {
        code: 'EPSG:3013',
        boundingBox: [10.03, 54.96, 24.17, 69.07],
        definition:
          '+proj=tmerc +lat_0=0 +lon_0=15.75 +k=1 +x_0=150000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +axis=neu +no_defs',
      },
      {
        code: 'EPSG:3014',
        boundingBox: [10.03, 54.96, 24.17, 69.07],
        definition:
          '+proj=tmerc +lat_0=0 +lon_0=17.25 +k=1 +x_0=150000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +axis=neu +no_defs',
      },
      {
        code: 'EPSG:3015',
        boundingBox: [10.03, 54.96, 24.17, 69.07],
        definition:
          '+proj=tmerc +lat_0=0 +lon_0=18.75 +k=1 +x_0=150000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +axis=neu +no_defs',
      },
      {
        code: 'EPSG:3016',
        boundingBox: [10.03, 54.96, 24.17, 69.07],
        definition:
          '+proj=tmerc +lat_0=0 +lon_0=20.25 +k=1 +x_0=150000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +axis=neu +no_defs',
      },
      {
        code: 'EPSG:3017',
        boundingBox: [10.03, 54.96, 24.17, 69.07],
        definition:
          '+proj=tmerc +lat_0=0 +lon_0=21.75 +k=1 +x_0=150000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +axis=neu +no_defs',
      },
      {
        code: 'EPSG:3018',
        boundingBox: [10.03, 54.96, 24.17, 69.07],
        definition:
          '+proj=tmerc +lat_0=0 +lon_0=23.25 +k=1 +x_0=150000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +axis=neu +no_defs',
      },
      {
        code: 'EPSG:3019',
        boundingBox: [10.03, 54.96, 24.17, 69.07],
        definition:
          '+proj=tmerc +lat_0=0 +lon_0=11.30827777777778 +k=1 +x_0=1500000 +y_0=0 +ellps=bessel +towgs84=414.1,41.3,603.1,-0.855,2.141,-7.023,0 +units=m +axis=neu +no_defs',
      },
      {
        code: 'EPSG:3020',
        boundingBox: [10.03, 54.96, 24.17, 69.07],
        definition:
          '+proj=tmerc +lat_0=0 +lon_0=13.55827777777778 +k=1 +x_0=1500000 +y_0=0 +ellps=bessel +towgs84=414.1,41.3,603.1,-0.855,2.141,-7.023,0 +units=m +axis=neu +no_defs',
      },
      {
        code: 'EPSG:3021',
        boundingBox: [10.03, 54.96, 24.17, 69.07],
        definition:
          '+proj=tmerc +lat_0=0 +lon_0=15.80827777777778 +k=1 +x_0=1500000 +y_0=0 +ellps=bessel +towgs84=414.1,41.3,603.1,-0.855,2.141,-7.023,0 +units=m +axis=neu +no_defs',
      },
      {
        code: 'EPSG:3034',
        boundingBox: [-16.1, 32.88, 40.18, 84.17],
        definition:
          '+proj=lcc +lat_1=35 +lat_2=65 +lat_0=52 +lon_0=10 +x_0=4000000 +y_0=2800000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +axis=neu +no_defs',
      },
      {
        code: 'EPSG:31467',
        boundingBox: [5.87, 47.27, 13.84, 55.09],
        definition:
          '+proj=tmerc +lat_0=0 +lon_0=9 +k=1 +x_0=3500000 +y_0=0 +ellps=bessel +towgs84=598.1,73.7,418.2,0.202,0.045,-2.455,6.7 +units=m +axis=neu +no_defs',
      },
      {
        code: 'EPSG:31468',
        boundingBox: [5.87, 47.27, 13.84, 55.09],
        definition:
          '+proj=tmerc +lat_0=0 +lon_0=12 +k=1 +x_0=4500000 +y_0=0 +ellps=bessel +towgs84=598.1,73.7,418.2,0.202,0.045,-2.455,6.7 +units=m +axis=neu +no_defs',
      },
      {
        code: 'EPSG:25833',
        boundingBox: [-16.1, 32.88, 40.18, 84.17],
        definition:
          '+proj=utm +zone=33 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +axis=enu +no_defs',
      },
    ],
  },

  /**
   * Specifies the different types of entities,
   * sets their name,
   * which module they belong to,
   * the CSS class of their icon,
   * and various options like adding files or link and the possibility for inline creation.
   *
   * @typedef {{en: string}} LabelObj
   * @typedef {{name: string, label: LabelObj, rdfType: string[], module: string, template: string,
   *            includeInternal: boolean, includeFile: boolean, includeLink: boolean, inlineCreation: boolean,
   *            faClass: string}} EntityObj
   * @type {object} EntityObj
   */
  entitytypes: [
    {
      name: 'publisher',
      label: {
        en: 'Publisher',
        sv: 'Utgivare',
        de: 'Herausgeber',
      },
      rdfType: [
        'http://xmlns.com/foaf/0.1/Agent',
        'http://xmlns.com/foaf/0.1/Person',
        'http://xmlns.com/foaf/0.1/Organization',
      ],
      mainModule: 'catalog',
      module: 'catalog',
      template: 'dcat:foaf:Agent',
      includeInternal: true, // TODO will this exist after migration, if yes in what form?
      includeFile: false,
      includeLink: false,
      inlineCreation: true,
      faClass: 'users',
      listview: PUBLISHERS_LISTVIEW,
      overview: PUBLISHER_OVERVIEW_NAME,
    },
    {
      name: 'contactPoint',
      label: {
        en: 'Contact point',
        sv: 'Kontakt',
        de: 'Kontakt',
      },
      rdfType: [
        'http://www.w3.org/2006/vcard/ns#Individual',
        'http://www.w3.org/2006/vcard/ns#Organization',
        'http://www.w3.org/2006/vcard/ns#Kind',
      ],
      mainModule: 'catalog',
      module: 'catalog',
      template: 'dcat:contactPoint',
      includeInternal: true,
      includeFile: false,
      includeLink: false,
      inlineCreation: true,
      faClass: 'phone',
      listview: CONTACTS_LISTVIEW,
      overview: CONTACT_OVERVIEW_NAME,
    },
    {
      name: 'dataset',
      label: {
        en: 'Dataset',
        sv: 'Datamängd',
        de: 'Datensatz',
      },
      main: true,
      rdfType: ['http://www.w3.org/ns/dcat#Dataset'],
      mainModule: 'catalog',
      module: 'catalog',
      template: 'dcat:Dataset',
      includeInternal: true,
      includeFile: false,
      includeLink: false,
      inlineCreation: false,
      faClass: 'cubes',
      listview: DATASETS_LISTVIEW,
      overview: DATASET_OVERVIEW_NAME,
      draftsEnabled: true,
    },
    {
      name: 'datasetSeries',
      label: {
        en: 'Dataset series',
        sv: 'Datamängdsserie',
        de: 'Datensatzserie',
      },
      rdfType: ['http://www.w3.org/ns/dcat#DatasetSeries'],
      mainModule: 'catalog',
      module: 'catalog',
      template: 'dcat:DatasetSeries',
      includeInternal: true,
      includeFile: false,
      includeLink: false,
      inlineCreation: false,
      listview: DATASETS_LISTVIEW,
      overview: DATASET_OVERVIEW_NAME,
      draftsEnabled: true,
      hideFromOverview: true,
    },
    {
      name: 'dataService',
      label: {
        en: 'Data service',
        sv: 'Datatjänst',
        de: 'Datendienst',
      },
      rdfType: ['http://www.w3.org/ns/dcat#DataService'],
      mainModule: 'catalog',
      module: 'catalog',
      template: 'dcat:DataService',
      includeInternal: true,
      includeFile: false,
      includeLink: false,
      inlineCreation: false,
      faClass: 'exchange-alt',
      listview: DATASERVICES_LISTVIEW,
      overview: DATA_SERVICE_OVERVIEW_NAME,
      draftsEnabled: true,
    },
    {
      name: 'distribution',
      label: {
        en: 'Distribution',
        sv: 'Distribution',
        de: 'Distribution',
      },
      rdfType: ['http://www.w3.org/ns/dcat#Distribution'],
      mainModule: 'catalog',
      module: 'catalog',
      template: 'dcat:Distribution',
      useWith: 'dataset',
      useWithProperty: 'http://www.w3.org/ns/dcat#distribution',
      includeInternal: true,
      includeFile: false,
      includeLink: false,
      inlineCreation: false,
      overview: DATASET_OVERVIEW_NAME,
    },
    {
      name: 'catalog',
      label: {
        en: 'Catalog',
        sv: 'Katalog',
        de: 'Katalog',
      },
      rdfType: ['http://www.w3.org/ns/dcat#Catalog'],
      mainModule: 'catalog',
      module: 'catalog',
      template: 'dcat:Catalog',
      includeInternal: true,
      includeFile: false,
      includeLink: false,
      inlineCreation: false,
      overview: CATALOG_OVERVIEW_NAME,
    },
    {
      name: 'datasetResult',
      label: {
        en: 'Showcase',
        sv: 'Showcase',
        de: 'Showcase',
      },
      rdfType: ['http://entryscape.com/terms/Result'],
      mainModule: 'catalog',
      module: 'catalog',
      template: 'esc:Results',
      includeInternal: true,
      includeFile: false,
      includeLink: false,
      inlineCreation: false,
      faClass: 'gem',
      listview: SHOWCASES_LISTVIEW,
      overview: SHOWCASE_OVERVIEW_NAME,
    },
    {
      name: 'datasetIdea',
      label: {
        en: 'Idea',
        sv: 'Ide',
        de: 'Idee',
      },
      rdfType: ['http://entryscape.com/terms/Idea'],
      mainModule: 'catalog',
      module: 'catalog',
      template: 'esc:Ideas',
      includeInternal: true,
      includeFile: false,
      includeLink: false,
      inlineCreation: false,
      faClass: 'lightbulb',
      listview: IDEAS_LISTVIEW,
      overview: IDEA_OVERVIEW_NAME,
    },
    {
      name: 'catalogSuggestion',
      label: {
        en: 'Suggestion',
        sv: 'Förslag',
        de: 'Vorschlag',
      },
      rdfType: ['http://entryscape.com/terms/Suggestion'],
      mainModule: 'catalog',
      module: 'catalog',
      template: 'esc:Suggestion',
      includeInternal: true,
      includeFile: false,
      includeLink: false,
      inlineCreation: false,
      listview: SUGGESTIONS_LISTVIEW,
      overview: SUGGESTION_OVERVIEW_NAME,
      relations: [
        {
          entityType: 'dataset',
          type: 'aggregation',
          property: 'dcterms:references',
          label: {
            en: 'Datasets',
            sv: 'Datamängder',
            de: 'Datensätze',
          },
        },
      ],
    },
    {
      uniqueURIScope: 'entitytypeContextScope',
      name: 'datasetDocument',
      label: {
        en: 'Document',
        sv: 'Dokument',
        de: 'Dokument',
      },
      rdfType: [
        'foaf:Document',
        'dcterms:LicenseDocument',
        'dcterms:Standard',
        'prof:Profile',
      ],
      mainModule: 'catalog',
      module: 'catalog',
      template: 'esc:Documentish',
      externalTemplate: 'prof:Profile',
      complementaryTemplate: 'esc:complementaryDocumentish',
      includeInternal: false,
      includeFile: true,
      includeLink: true,
      inlineCreation: true,
      faClass: 'file',
      listview: DOCUMENTS_LISTVIEW,
      overview: DOCUMENT_OVERVIEW_NAME,
    },
    {
      name: 'datasettemplate',
      label: {
        en: 'Dataset Template',
        sv: 'Datamängdsmall',
        de: 'Datensatzvorlage',
      },
      rdfType: 'http://entryscape.com/terms/DatasetTemplate',
      mainModule: 'catalog',
      module: 'catalog',
      template: 'dcat:Dataset',
      includeInternal: true,
      includeFile: false,
      includeLink: false,
      inlineCreation: false,
      faClass: 'cubes',
    },
    {
      name: 'distributionFile',
      label: {
        en: 'Distribution file',
        sv: 'Distributionsfilen',
        de: 'Distributionsdatei',
      },
      rdfType: 'http://entryscape.com/terms/File',
      mainModule: 'catalog',
      module: 'catalog',
      template: 'esc:File',
      includeInternal: false,
      includeFile: true,
      includeLink: false,
      inlineCreation: false,
      faClass: 'file',
    },
    {
      name: 'visualization',
      label: {
        en: 'Visualization',
        sv: 'Visualisering',
        de: 'Visualisierung',
      },
      rdfType: 'http://entryscape.com/terms/visualization/Visualization',
      mainModule: 'catalog',
      module: 'catalog',
      template: 'esc:context',
      useWith: 'dataset',
      useWithProperty: 'http://schema.org/diagram',
      includeInternal: false,
      includeFile: false,
      includeLink: false,
      inlineCreation: false,
      overview: DATASET_OVERVIEW_NAME,
    },
  ],
};
