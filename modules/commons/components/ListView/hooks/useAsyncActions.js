import { useEffect } from 'react';
import useAsync, { IDLE } from 'commons/hooks/useAsync';

const useAsyncActions = (actions) => {
  const {
    runAsync,
    status,
    isLoading,
    data: visibleActions,
  } = useAsync({ data: [] });

  useEffect(() => {
    if (status !== IDLE) return;

    const getVisibleActions = async () =>
      Promise.all(
        actions?.map(({ action }) => {
          const { isVisible } = action;
          try {
            return Promise.resolve(isVisible ? isVisible(action) : true);
          } catch (error) {
            console.error(error);
            return Promise.resolve(false);
          }
        })
      ).then((isVisibleArr) =>
        actions?.filter((_, index) => Boolean(isVisibleArr[index]))
      );
    runAsync(getVisibleActions());
  }, [actions, runAsync, status]);

  return {
    actions: visibleActions,
    isLoading,
  };
};

export default useAsyncActions;
