import ListActionDialog from 'commons/components/common/dialogs/ListActionDialog';
import PropTypes from 'prop-types';
import { Entry } from '@entryscape/entrystore-js';
import ContentWrapper from 'commons/components/common/ContentWrapper';
import escoRdformsNLS from 'commons/nls/escoRdforms.nls';
import { useTranslation } from 'commons/hooks/useTranslation';
import DatasetPreview from 'catalog/datasets/preview/DatasetPreview';

const DatasetInfoDialog = ({ entry, closeDialog }) => {
  const t = useTranslation(escoRdformsNLS);
  return (
    <ListActionDialog
      id="dataset-info-entry"
      title={t('metadataPresentDialogHeader')}
      closeDialog={closeDialog}
      closeDialogButtonLabel={t('metadataPresentDialogCloseLabel')}
      maxWidth="md"
    >
      <ContentWrapper md={12}>
        <DatasetPreview entry={entry} />
      </ContentWrapper>
    </ListActionDialog>
  );
};

DatasetInfoDialog.propTypes = {
  entry: PropTypes.instanceOf(Entry),
  closeDialog: PropTypes.func,
};

export default DatasetInfoDialog;
