import EntityListView from 'commons/components/views/EntityListView';
import { listPropsPropType } from 'commons/components/ListView';

const SpecificationsView = ({ listProps }) => {
  return (
    <EntityListView entityTypeName="specifications" listProps={listProps} />
  );
};

SpecificationsView.propTypes = {
  listProps: listPropsPropType,
};

export default SpecificationsView;
