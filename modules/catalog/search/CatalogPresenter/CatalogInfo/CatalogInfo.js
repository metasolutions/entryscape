import PropTypes from 'prop-types';
import { Typography, Grid, Divider } from '@mui/material';
import { getLabel } from 'commons/util/rdfUtils';
import {
  useTranslation,
  nlsBundlesPropType,
} from 'commons/hooks/useTranslation';
import useRDFormsPresenter from 'commons/components/rdforms/hooks/useRDFormsPresenter';
import ExpandButton from 'commons/components/buttons/ExpandButton';
import { spreadEntry } from 'commons/util/store';
import { Entry } from '@entryscape/entrystore-js';
import config from 'config';
import './index.scss';

const CatalogMetadataPresenter = ({ catalogEntry, nlsBundles }) => {
  const { allMetadata: metadata, ruri } = spreadEntry(catalogEntry);
  const template = config.get('catalog.catalogTemplateId');
  const filterPredicates = {
    'http://purl.org/dc/terms/title': true,
    'http://purl.org/dc/terms/description': true,
    'http://purl.org/dc/terms/publisher': true,
  };

  const presenterFilteredPredicates = [
    'http://purl.org/dc/terms/title',
    'http://purl.org/dc/terms/description',
    'http://purl.org/dc/terms/publisher',
    'http://www.w3.org/1999/02/22-rdf-syntax-ns#type',
    'http://www.w3.org/ns/dcat#dataset',
    'http://entryscape.com/terms/Suggestion',
  ];

  const showMetadataButton = metadata
    .find(ruri, null)
    .some(
      (statement) =>
        presenterFilteredPredicates.indexOf(statement.getPredicate()) === -1
    );

  const { PresenterComponent } = useRDFormsPresenter(
    metadata,
    ruri,
    template,
    false,
    filterPredicates
  );
  return showMetadataButton && PresenterComponent ? (
    <ExpandButton
      PresenterComponent={PresenterComponent}
      initiallyExpanded={false}
      nlsBundles={nlsBundles}
      alternativeClass="escaCatalogMetadataPresenter__button"
    />
  ) : null;
};

CatalogMetadataPresenter.propTypes = {
  catalogEntry: PropTypes.instanceOf(Entry),
  nlsBundles: nlsBundlesPropType,
};

const CatalogInfo = ({ entry, description, publisher, nlsBundles }) => {
  const t = useTranslation(nlsBundles);
  return (
    <>
      <Grid item className="escaCatalogInfo__header">
        <Typography
          display="inline"
          variant="h3"
          classes={{ h3: 'escaCatalogInfo__heading' }}
          aria-live="polite"
        >
          {getLabel(entry)}
        </Typography>
      </Grid>
      <Divider className="escaCatalogInfo__divider" />
      <Grid
        container
        spacing={2}
        direction="row"
        wrap="nowrap"
        alignItems="flex-start"
        item
      >
        <Grid item xs={8}>
          <Typography variant="h4">{t('descriptionHeading')}</Typography>
          <Typography>{description}</Typography>
        </Grid>
        <Grid item xs={4} zeroMinWidth>
          <Typography variant="h4">{t('publisherHeading')}</Typography>
          {publisher ? (
            <Typography key={getLabel(publisher)} noWrap>
              {getLabel(publisher)}
            </Typography>
          ) : null}
        </Grid>
      </Grid>
      <Grid item className="escaCatalogInfo__presenter">
        <CatalogMetadataPresenter
          catalogEntry={entry}
          nlsBundles={[nlsBundles]}
        />
      </Grid>
    </>
  );
};

CatalogInfo.propTypes = {
  entry: PropTypes.instanceOf(Entry),
  description: PropTypes.string,
  publisher: PropTypes.instanceOf(Entry),
  nlsBundles: nlsBundlesPropType,
};
export default CatalogInfo;
