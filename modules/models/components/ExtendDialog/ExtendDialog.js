import PropTypes from 'prop-types';
import { useMemo } from 'react';
import { useESContext } from 'commons/hooks/useESContext';
import CreateDialog from 'models/components/CreateDialog';
import { EditorProvider } from 'commons/components/forms/editors';
import {
  useTranslation,
  nlsBundlesPropType,
} from 'commons/hooks/useTranslation';
import { useSnackbar } from 'commons/hooks/useSnackbar';

const ExtendDialog = ({
  closeDialog,
  fields,
  nlsBundles,
  headerNlsKey,
  beforeCreate,
}) => {
  const { context } = useESContext();
  const translate = useTranslation(nlsBundles);
  const [addSnackbar] = useSnackbar();

  const prototypeEntry = useMemo(() => {
    return context.newEntry();
  }, [context]);

  return (
    <EditorProvider entry={prototypeEntry}>
      <CreateDialog
        title={translate(headerNlsKey)}
        closeDialog={closeDialog}
        fields={fields}
        beforeCreate={beforeCreate}
        nlsBundles={nlsBundles}
        onEditEntry={() => addSnackbar({ message: translate('createSuccess') })}
      />
    </EditorProvider>
  );
};

ExtendDialog.propTypes = {
  closeDialog: PropTypes.func.isRequired,
  fields: PropTypes.arrayOf(PropTypes.shape({})),
  nlsBundles: nlsBundlesPropType,
  headerNlsKey: PropTypes.string,
  beforeCreate: PropTypes.func,
};

export default ExtendDialog;
