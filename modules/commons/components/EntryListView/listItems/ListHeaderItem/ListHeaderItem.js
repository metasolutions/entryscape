import PropTypes from 'prop-types';
import {
  ListHeaderItemSort,
  ListHeaderItemText,
} from 'commons/components/ListView';

export const ListHeaderItem = ({
  sortBy,
  translate,
  headerNlsKey,
  ...props
}) => {
  const ListHeaderItemComponent = sortBy
    ? ListHeaderItemSort
    : ListHeaderItemText;

  return (
    <ListHeaderItemComponent
      text={headerNlsKey ? translate(headerNlsKey) : ''}
      sortBy={sortBy}
      {...props}
    />
  );
};

ListHeaderItem.propTypes = {
  sortBy: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
  headerNlsKey: PropTypes.string,
  translate: PropTypes.func,
};
