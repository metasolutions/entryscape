import { useCallback, useMemo, useState } from 'react';
import {
  getDiagramLabel,
  getDiagramDescription,
} from 'models/diagrams/utils/diagram';
import { DEFAULT_DIAGRAM_ID } from 'models/diagrams/utils/definitions';
import { useTranslation } from 'commons/hooks/useTranslation';
import { IDLE, PENDING, RESOLVED } from 'commons/hooks/useAsync';
import esmoDiagramsNLS from 'models/nls/esmoDiagrams.nls';
import useGetDiagramEntries from './useGetDiagramEntries';

const useGetDiagramOptions = (context) => {
  const translate = useTranslation([esmoDiagramsNLS]);
  const { diagramEntries, status } = useGetDiagramEntries(context);
  const [selected, setSelected] = useState(DEFAULT_DIAGRAM_ID);

  // In case a prototype entry is used, it needs be memoized to avoid it from
  // being recreated on every rerender
  const defaultPrototypeEntry = useMemo(() => {
    return context
      .newGraph({}, DEFAULT_DIAGRAM_ID)
      .add('rdf:type', 'esterms:Diagram');
  }, [context]);

  const getDiagramOptions = useCallback(() => {
    if (status !== RESOLVED) return [];

    // Default entry needs to handled separate, since it should be selected by
    // default and be first in order. Also a default entry may not have been
    // created yet in which case a fallback item should be used.
    const defaultEntry = diagramEntries.find(
      (digramEntry) => digramEntry.getId() === DEFAULT_DIAGRAM_ID
    );
    const nonDefaultEntries = diagramEntries.filter(
      (diagramEntry) => diagramEntry.getId() !== DEFAULT_DIAGRAM_ID
    );
    const nonDefaultOptions = nonDefaultEntries.map((diagramEntry) => {
      return {
        label: getDiagramLabel(diagramEntry, translate),
        value: diagramEntry.getId(),
        description: getDiagramDescription(diagramEntry, translate),
        entry: diagramEntry,
      };
    });

    return [
      {
        label: getDiagramLabel(defaultEntry, translate, true),
        value: DEFAULT_DIAGRAM_ID,
        description: getDiagramDescription(defaultEntry, translate, true),
        entry: defaultEntry || defaultPrototypeEntry,
        isDefault: true,
      },
      ...nonDefaultOptions,
    ];
  }, [diagramEntries, defaultPrototypeEntry, status, translate]);

  const options = getDiagramOptions();

  const getOption = (optionValue) => {
    return status === RESOLVED
      ? options.find(({ value }) => value === optionValue)
      : null;
  };

  return {
    isLoading: status === IDLE || status === PENDING,
    options,
    selected,
    setSelected,
    getOption,
  };
};

export default useGetDiagramOptions;
