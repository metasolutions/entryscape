import PropTypes from 'prop-types';
import { Entry } from '@entryscape/entrystore-js';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogContent from '@mui/material/DialogContent';
import DialogActions from '@mui/material/DialogActions';
import escoListNLS from 'commons/nls/escoList.nls';
import {
  useTranslation,
  nlsBundlesPropType,
} from 'commons/hooks/useTranslation';
import useAsync, { PENDING } from 'commons/hooks/useAsync';
import { useUserState } from 'commons/hooks/useUser';
import { deleteContext } from 'commons/util/context';
import useNavigate from 'commons/components/router/useNavigate';
import { useSnackbar, ERROR } from 'commons/hooks/useSnackbar';
import LoadingButton from 'commons/components/LoadingButton';
import { getCurrentViewName, getParentViewName } from 'commons/util/site';

const RemoveContextDialog = ({
  entry,
  closeDialog,
  onRemove = () => {},
  nlsBundles = [],
}) => {
  const { userEntry } = useUserState();
  const translate = useTranslation([...nlsBundles, escoListNLS]);
  const { runAsync } = useAsync();
  const { goBackToListView } = useNavigate();
  const [addSnackbar] = useSnackbar();

  const handleRemoveRequest = () =>
    runAsync(
      deleteContext(entry, userEntry)
        .then(() => {
          closeDialog();
          onRemove();
          goBackToListView(getParentViewName(getCurrentViewName()));
          addSnackbar({ message: translate('contextRemoveSuccess') });
        })
        .catch((err) => {
          console.error(err);
          addSnackbar({ message: translate('contextRemoveFail'), type: ERROR });
        })
    );

  return (
    <Dialog open onClose={closeDialog} fullWidth maxWidth="xs">
      <DialogContent>
        <div>{translate('removeContext')}</div>
      </DialogContent>
      <DialogActions>
        <Button onClick={closeDialog} variant="text">
          {translate('cancel')}
        </Button>
        <LoadingButton
          autoFocus
          onClick={handleRemoveRequest}
          loading={runAsync.status === PENDING}
          color="primary"
        >
          {translate('removeButtonLabel')}
        </LoadingButton>
      </DialogActions>
    </Dialog>
  );
};

RemoveContextDialog.propTypes = {
  entry: PropTypes.instanceOf(Entry),
  closeDialog: PropTypes.func,
  onRemove: PropTypes.func,
  nlsBundles: nlsBundlesPropType,
};

export default RemoveContextDialog;
