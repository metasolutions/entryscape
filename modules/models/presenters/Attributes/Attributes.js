import PropTypes from 'prop-types';
import { graphPropType } from 'commons/util/entry';
import { Fields, Field } from 'commons/components/forms/presenters';
import { Chip } from '@mui/material';
import Tooltip, { TOOLTIP_DELAY } from 'commons/components/common/Tooltip';
import { getDescription } from 'models/utils/attribute';
import './Attributes.scss';

export const Attributes = ({
  graph,
  resourceURI,
  property,
  inline,
  children,
  ...fieldsProps
}) => {
  const statements = graph.find(resourceURI, property);

  return statements.length ? (
    <Fields {...fieldsProps}>
      <Field>
        <div>
          {statements.map((statement, index) => {
            return (
              <Tooltip // eslint-disable-next-line react/no-array-index-key
                key={index}
                enterDelay={TOOLTIP_DELAY}
                enterNextDelay={TOOLTIP_DELAY}
                title={getDescription(statement.getValue())}
              >
                <Chip
                  label={statement.getValue()}
                  className="esmoAttributes__chip"
                />
              </Tooltip>
            );
          })}
        </div>
      </Field>
      {children}
    </Fields>
  ) : null;
};

Attributes.propTypes = {
  graph: graphPropType,
  resourceURI: PropTypes.string,
  property: PropTypes.string,
  inline: PropTypes.bool,
  children: PropTypes.node,
};
