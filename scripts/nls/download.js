/* eslint-disable no-await-in-loop */
/* eslint-disable no-plusplus */
/* eslint-disable no-restricted-syntax */
const nls = require('./nls');
const poe = require('./poeclient');
const { languages, projects } = require('./projects');
const { logGroup, getModuleNames } = require('./utils');

let apikey;
try {
  apikey = require('./apikey');
} catch (e) {
  console.log(
    'You have to provide an API-key from POEditor and put it in the apikey.js file,' +
      ' see how it is done in apikey.js_example'
  );
  process.exit(1);
}

(async () => {
  // eslint-disable-next-line no-restricted-syntax
  logGroup('Downloading POEditor projects...\n', false);
  for (const { id, paths } of projects) {
    logGroup(getModuleNames(paths), '\n');

    const terms = [];
    const poeAuth = poe(apikey, id);

    let count = 0;
    for (const lang of languages) {
      terms.push(await poeAuth.exportTermsAndDefinitions(lang));

      console.log(`Fetched ${lang} - ${terms[count++].length} terms`);
    }
    console.groupEnd();

    // Don't forget to remove old dir first.
    nls.writeTranslations(languages, terms, paths);
  }
})();
