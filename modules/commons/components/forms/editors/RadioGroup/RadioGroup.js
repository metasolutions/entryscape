import PropTypes from 'prop-types';
import {
  FormControl,
  FormControlLabel,
  FormHelperText,
  FormLabel,
  Grid,
  RadioGroup as MuiRadioGroup,
  Radio,
} from '@mui/material';
import './RadioGroup.scss';

export const RadioGroup = ({
  xs = 12,
  label,
  labelId,
  choices,
  value,
  row,
  onChange,
  errorText,
}) => {
  const id = label ? `radio-group-${labelId}` : labelId;

  return (
    <Grid item xs={xs}>
      <FormControl
        component="fieldset"
        error={Boolean(errorText)}
        className="escoRadioGroup__control"
      >
        {label ? <FormLabel id={id}>{label}</FormLabel> : null}
        <MuiRadioGroup
          aria-labelledby={id}
          name={`radio-buttons-group-${labelId}`}
          value={value}
          onChange={onChange}
          row={row}
        >
          {choices.map((choice) => (
            <FormControlLabel
              key={choice.value}
              value={choice.value}
              control={<Radio />}
              label={choice.label}
            />
          ))}
        </MuiRadioGroup>
        <FormHelperText>{errorText}</FormHelperText>
      </FormControl>
    </Grid>
  );
};

RadioGroup.propTypes = {
  choices: PropTypes.arrayOf(
    PropTypes.shape({
      value: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
      label: PropTypes.string,
    })
  ),
  label: PropTypes.string,
  labelId: PropTypes.string,
  onChange: PropTypes.func,
  value: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  xs: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  row: PropTypes.bool,
  errorText: PropTypes.string,
};
