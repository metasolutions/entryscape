/**
 * Re-ordering after drag (helper function for drag & drop)
 * @param {Object[]} Items
 * @param {number} sourceIndex
 * @param {number} destinationIndex
 * @returns {Object[]}
 */

export const reorder = (Items, sourceIndex, destinationIndex) => {
  const reorderedItems = [...Items];
  const [removed] = reorderedItems.splice(sourceIndex, 1);
  reorderedItems.splice(destinationIndex, 0, removed);
  return reorderedItems;
};
