import PropTypes from 'prop-types';
import { Link as MuiLink } from '@mui/material';

const ReferenceList = ({ references }) => {
  return (
    <ul>
      {references.map(({ label, href }) => (
        <li key={href}>
          {href ? (
            <MuiLink target="_blank" href={href}>
              {label}
            </MuiLink>
          ) : (
            label
          )}
        </li>
      ))}
    </ul>
  );
};

ReferenceList.propTypes = {
  references: PropTypes.arrayOf(
    PropTypes.shape({ label: PropTypes.string, href: PropTypes.string })
  ),
};

export default ReferenceList;
