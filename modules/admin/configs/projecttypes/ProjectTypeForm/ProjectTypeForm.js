import { Form } from 'commons/components/forms/editors';
import { nlsBundlesPropType } from 'commons/hooks/useTranslation';
import { editorFields } from '../utils/editorFields';

const ProjectTypeForm = ({ nlsBundles }) => {
  return <Form nlsBundles={nlsBundles} fields={editorFields} size="small" />;
};

ProjectTypeForm.propTypes = {
  nlsBundles: nlsBundlesPropType,
};

export default ProjectTypeForm;
