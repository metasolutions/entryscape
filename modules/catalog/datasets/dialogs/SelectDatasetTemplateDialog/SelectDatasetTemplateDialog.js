import PropTypes from 'prop-types';
import { useReducer, useMemo, useCallback } from 'react';
import { Button } from '@mui/material';
import { OpenInNew as OpenInNewIcon } from '@mui/icons-material';
import config from 'config';
import ListActionDialog from 'commons/components/common/dialogs/ListActionDialog';
import ContentWrapper from 'commons/components/common/ContentWrapper';
import escaDatasetTemplateNLS from 'catalog/nls/escaDatasetTemplate.nls';
import { useTranslation } from 'commons/hooks/useTranslation';
import {
  getDatasetsBasedOnTemplates,
  getDatasetTemplateStore,
  createDatasetTemplateQuery,
} from 'catalog/datasetTemplates/util';
import { useESContext } from 'commons/hooks/useESContext';
import useAsyncCallback from 'commons/hooks/useAsyncCallback';
import DatasetTemplateFilters from 'catalog/datasetTemplates/DatasetTemplateListFilters';
import { reducer, getInitialState } from 'catalog/datasetTemplates/reducer';
import useCatalogs from 'catalog/datasetTemplates/useCatalogs';
import useCategories from 'catalog/datasetTemplates/useCategories';
import useRestrictionDialog from 'commons/hooks/useRestrictionDialog';
import {
  EntryListView,
  useSolrQuery,
  TITLE_COLUMN,
  MODIFIED_COLUMN,
  INFO_COLUMN,
} from 'commons/components/EntryListView';
import { ACTION_INFO } from 'commons/actions';
import {
  withListModelProvider,
  ListItemButtonAction,
  ListItemIconButton,
  ListItemActionIconButton,
} from 'commons/components/ListView';
import {
  languageUriToIsoCode,
  getBasedOnProperty,
} from 'commons/util/metadata';
import { applyQueryParams } from 'commons/util/solr';
import LinkedDataBrowserDialog from 'commons/components/LinkedDataBrowserDialog';
import CreateDatasetFromTemplateDialog from '../CreateDatasetFromDatasetTemplateDialog';
import useDatasetRestriction from '../../useDatasetRestriction';

const SelectDatasetTemplateDialog = ({
  closeDialog,
  actionParams: { numberOfDatasets, refreshDatasets, prototypeCallback },
  ...rest
}) => {
  const translate = useTranslation([escaDatasetTemplateNLS]);
  const catalogConfiguration = config.get(
    'catalog.datasetTemplateDefaultCatalog'
  );

  const { context } = useESContext();
  const [existingTemplateDatasets] = useAsyncCallback(
    getDatasetsBasedOnTemplates,
    context,
    []
  );

  const [
    { category: selectedCategory, context: selectedCatalogId },
    filtersDispatch,
  ] = useReducer(reducer, getInitialState());

  const { isRestricted, contentPath } = useDatasetRestriction(
    numberOfDatasets,
    context
  );
  useRestrictionDialog({
    open: isRestricted,
    contentPath,
    callback: closeDialog,
  });

  const datasetTemplateStore = useMemo(() => getDatasetTemplateStore(), []);
  const catalogEntries = useCatalogs(datasetTemplateStore);

  const selectedCatalogEntry =
    catalogEntries.find(
      (catalogEntry) => catalogEntry.getContext().getId() === selectedCatalogId
    ) || '';

  const [categories, setCategories] = useCategories(
    datasetTemplateStore,
    selectedCatalogEntry,
    filtersDispatch
  );

  const selectedCatalogLanguage = selectedCatalogEntry
    ? selectedCatalogEntry
        .getMetadata()
        .findFirstValue(null, 'dcterms:language')
    : '';

  const createQuery = useCallback(
    () =>
      createDatasetTemplateQuery(
        datasetTemplateStore,
        selectedCatalogEntry,
        selectedCategory
      ),
    [datasetTemplateStore, selectedCatalogEntry, selectedCategory]
  );

  const applyParamsCustomSort = useCallback(
    (query, { sort, ...queryParams }) =>
      applyQueryParams(query, {
        ...queryParams,
        sort: {
          ...sort,
          locale: languageUriToIsoCode(selectedCatalogLanguage),
        },
      }),
    [selectedCatalogLanguage]
  );
  const { size, status, ...queryResults } = useSolrQuery({
    createQuery,
    applyQueryParams: applyParamsCustomSort,
  });

  const getCreateButtonProps = ({ entry: templateEntry, translate: t }) => {
    const basedOnTemplate = existingTemplateDatasets.some(
      (entry) =>
        getBasedOnProperty(entry) === templateEntry.getResourceURI() ||
        entry.getResourceURI() === templateEntry.getResourceURI()
    );

    return {
      action: {
        entry: templateEntry,
        Dialog: CreateDatasetFromTemplateDialog,
        existingTemplates: existingTemplateDatasets,
        closeDialog,
        refreshDatasets,
        prototypeCallback,
        ...rest,
      },
      disabled: basedOnTemplate,
      children: basedOnTemplate
        ? t('createdFromTemplateDialogButton')
        : t('createFromTemplateDialogButton'),
      variant: 'text',
    };
  };

  const getSourceLinkProps = ({ entry, translate: t }) => {
    const landingPageLink = entry
      .getMetadata()
      .findFirstValue(entry.getResourceURI(), 'esterms:templateLandingPage');

    if (!landingPageLink) return null;

    return {
      title: t('landingPageLinkTooltip'),
      'aria-label': t('landingPageLinkTooltip'),
      children: <OpenInNewIcon />,
      href: landingPageLink,
      target: '_blank',
    };
  };

  if (isRestricted) return null;

  return (
    <ListActionDialog
      id="create-from-template-dialog"
      title={translate('createFromTemplateDialogHeader')}
      closeDialog={closeDialog}
      closeDialogButtonLabel={translate('createFromTemplateDialogCloseButton')}
      maxWidth="md"
      fixedHeight
    >
      <ContentWrapper md={12}>
        <EntryListView
          // eslint-disable-next-line react/jsx-props-no-spreading
          {...queryResults}
          size={size}
          nlsBundles={[escaDatasetTemplateNLS]}
          listPlaceholderProps={{
            label: translate('emptyListWarning'),
          }}
          columns={[
            {
              ...TITLE_COLUMN,
              headerNlsKey: 'templatesListHeaderLabel',
              xs: 6,
            },
            {
              ...MODIFIED_COLUMN,
              headerNlsKey: 'templatesListHeaderModified',
              sortBy: false,
            },
            {
              ...INFO_COLUMN,
              title: translate('templateMetadataTooltip'),
              Component: ListItemActionIconButton,
              getProps: ({ entry }) => ({
                action: {
                  ...ACTION_INFO,
                  entry,
                  Dialog: LinkedDataBrowserDialog,
                },
              }),
            },
            {
              id: 'source-link',
              Component: ListItemIconButton,
              xs: 1,
              sx: { padding: 0 },
              getProps: getSourceLinkProps,
            },
            {
              id: 'create-dataset-from-template',
              xs: 2,
              Component: ListItemButtonAction,
              ButtonComponent: Button,
              getProps: getCreateButtonProps,
            },
          ]}
        >
          <DatasetTemplateFilters
            filtersDispatch={filtersDispatch}
            includeCatalogFilter={!catalogConfiguration}
            selectedCatalogId={selectedCatalogId}
            catalogEntries={catalogEntries}
            categories={categories}
            setCategories={setCategories}
            selectedCategory={selectedCategory}
          />
        </EntryListView>
      </ContentWrapper>
    </ListActionDialog>
  );
};

SelectDatasetTemplateDialog.propTypes = {
  closeDialog: PropTypes.func.isRequired,
  actionParams: PropTypes.shape({
    numberOfDatasets: PropTypes.number,
    refreshDatasets: PropTypes.func,
    prototypeCallback: PropTypes.func,
  }),
};

export default withListModelProvider(SelectDatasetTemplateDialog, () => ({
  sort: { field: 'title', order: 'asc' },
}));
