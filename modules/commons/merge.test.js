import merge from './merge';

describe('merge function', () => {
  test('merges plain objects deeply', () => {
    const obj1 = { a: { b: 1 } };
    const obj2 = { a: { c: 2 } };
    const result = merge(obj1, obj2);
    expect(result).toEqual({ a: { b: 1, c: 2 } });
  });

  test('concatenates arrays', () => {
    const obj1 = { a: [1, 2] };
    const obj2 = { a: [3, 4] };
    const result = merge(obj1, obj2);
    expect(result).toEqual({ a: [1, 2, 3, 4] });
  });

  test('merges arrays of objects with "name" keys', () => {
    const obj1 = { entitytypes: [{ name: 'art', module: 'catalog' }] };
    const obj2 = {
      entitytypes: [
        { name: 'art', module: 'workbench' },
        { name: 'dataset', module: 'catalog' },
      ],
    };
    const result = merge(obj1, obj2);
    expect(result).toEqual({
      entitytypes: [
        { name: 'art', module: 'workbench' },
        { name: 'dataset', module: 'catalog' },
      ],
    });
  });

  test('concatenates arrays without "name" keys', () => {
    const obj1 = { modules: [{ type: 'moduleA' }] };
    const obj2 = { modules: [{ type: 'moduleB' }] };
    const result = merge(obj1, obj2);
    expect(result).toEqual({
      modules: [{ type: 'moduleA' }, { type: 'moduleB' }],
    });
  });

  test('handles invalid inputs gracefully', () => {
    const obj1 = { a: 1 };
    const obj2 = null;
    const obj3 = undefined;
    const result = merge(obj1, obj2, obj3);
    expect(result).toEqual({ a: 1 });
  });

  test('overrides keys prefixed with "!"', () => {
    const obj1 = { a: { b: 1 } };
    const obj2 = { '!a': { c: 2 } };
    const result = merge(obj1, obj2);
    expect(result).toEqual({ '!a': { c: 2 }, a: { c: 2 } });
  });

  test('overrides nested keys prefixed with "!"', () => {
    const obj1 = { a: { b: 1, c: 3 } };
    const obj2 = { a: { '!b': 2 } };
    const result = merge(obj1, obj2);
    expect(result).toEqual({ a: { '!b': 2, b: 2, c: 3 } });
  });

  test('overrides nested keys prefixed with "!" with array as values', () => {
    const obj1 = { a: { b: [1, 2, 4] } };
    const obj2 = { a: { '!b': [1, 2, 3, 4] } };
    const result = merge(obj1, obj2);
    expect(result).toEqual({ a: { '!b': [1, 2, 3, 4], b: [1, 2, 3, 4] } });
  });

  test('merges deeply nested structures', () => {
    const obj1 = { a: { b: { c: { d: 1 } } } };
    const obj2 = { a: { b: { c: { e: 2 } } } };
    const result = merge(obj1, obj2);
    expect(result).toEqual({ a: { b: { c: { d: 1, e: 2 } } } });
  });

  test('preserves order in arrays when merging on name', () => {
    const obj1 = {
      items: [
        { name: 'item1', value: 1 },
        { name: 'item2', value: 2 },
      ],
    };

    const obj2 = {
      items: [
        { name: 'item1', value: 10 }, // Merge with original 'item1'
        { name: 'item3', value: 3 }, // Adds a new item
      ],
    };

    const result = merge(obj1, obj2);

    expect(result).toEqual({
      items: [
        { name: 'item1', value: 10 }, // Merged 'item1'
        { name: 'item2', value: 2 }, // Original 'item2'
        { name: 'item3', value: 3 }, // Newly added 'item3'
      ],
    });
  });
});
