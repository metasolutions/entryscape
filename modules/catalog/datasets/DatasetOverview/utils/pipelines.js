import { addIgnore, GENERIC_PROBLEM } from 'commons/errors/utils/async';
import { createRowstorePipeline } from 'commons/util/store';

/**
 *
 * @returns {Promise<store/Resource>}
 */
export const getPipelineResource = (context) => {
  addIgnore('getEntry', GENERIC_PROBLEM, true);
  return context
    .getEntryById('rowstorePipeline')
    .catch(() => createRowstorePipeline(context))
    .then((pipeline) => pipeline.getResource());
};
