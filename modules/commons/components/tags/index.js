import PropTypes from 'prop-types';
import escoListNLS from 'commons/nls/escoList.nls';
import { nlsBundlesPropType } from 'commons/hooks/useTranslation';
import { isDraftEntry } from 'commons/util/entry';

export const DRAFT_TAG = {
  id: 'draftTag',
  labelNlsKey: 'draftTag',
  nlsBundles: escoListNLS,
  isVisible: (entry) => isDraftEntry(entry),
};

export const tagPropType = PropTypes.shape({
  id: PropTypes.string,
  labelNlsKey: PropTypes.string,
  nlsBundles: nlsBundlesPropType,
  isVisible: PropTypes.func,
});

export const tagsPropType = PropTypes.oneOfType([
  tagPropType,
  PropTypes.arrayOf(tagPropType),
]);

export { default as Tag } from './Tag';
