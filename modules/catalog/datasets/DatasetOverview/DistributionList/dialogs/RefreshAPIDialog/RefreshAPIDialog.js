import { useState, useEffect, useCallback } from 'react';
import PropTypes from 'prop-types';
import { entryPropType } from 'commons/util/entry';
import ListActionDialog from 'commons/components/common/dialogs/ListActionDialog';
import ContentWrapper from 'commons/components/common/ContentWrapper';
import ProgressList from 'commons/components/common/ProgressList';
import { useTranslation } from 'commons/hooks/useTranslation';
import escaFilesNLS from 'catalog/nls/escaFiles.nls';
import escaApiProgressNLS from 'catalog/nls/escaApiProgress.nls';
import { STATUS_FAILED, useTasksList } from 'commons/hooks/useTasksList';
import useValidateFiles from 'catalog/datasets/DatasetOverview/hooks/useValidateFiles';
import useRefreshAPI from './useRefreshAPI';
import { TASKS_REFRESH_API } from './tasks'; // Same as activate api, should be common-ified
import {
  ignoreHandlers,
  stopIgnoringHandlers,
  handlersToIgnore,
} from '../ActivateAPIDialog/util';

const RefreshAPIDialog = ({
  closeDialog,
  apiDistribution,
  sourceDistribution,
  refreshDistributions,
}) => {
  const translate = useTranslation([escaFilesNLS, escaApiProgressNLS]);
  const { tasks, updateTask, allTasksDone } = useTasksList([
    ...TASKS_REFRESH_API,
  ]);

  const {
    validFiles,
    dialogType: fileValidationType,
    confirmFiles,
  } = useValidateFiles(sourceDistribution);

  const [refreshAPI] = useRefreshAPI(
    sourceDistribution,
    updateTask,
    apiDistribution
  );

  const [alertMessage, setAlertMessage] = useState('');
  const hasFailedTask = tasks.some(({ status }) => status === STATUS_FAILED);

  useEffect(() => {
    const successMessage = allTasksDone && translate('nlsProgressSuccess');
    const failureMessage = hasFailedTask && translate('nlsProgressFailure');
    const newAlertMessage = successMessage || failureMessage || '';
    setAlertMessage(newAlertMessage);
  }, [allTasksDone, hasFailedTask, translate]);

  const confirmAndRefreshAPI = useCallback(async () => {
    const proceed = await confirmFiles();
    if (fileValidationType === 'acknowledge' || !proceed) {
      closeDialog();
      return;
    }

    refreshAPI();
  }, [confirmFiles, refreshAPI, fileValidationType]); // closeDialog missing, infinite loop

  useEffect(() => {
    ignoreHandlers(handlersToIgnore);

    if (validFiles === null) {
      return stopIgnoringHandlers(handlersToIgnore);
    }

    confirmAndRefreshAPI();
    return stopIgnoringHandlers(handlersToIgnore);
  }, [confirmAndRefreshAPI, validFiles]);

  return (
    <ListActionDialog
      id="refresh-api-dialog"
      closeDialog={() => {
        refreshDistributions();
        closeDialog();
      }}
      maxWidth="sm"
      title={translate('apiRegenerateHeader')}
      closeDialogButtonLabel={translate('apiRegenerateButton')}
      alert={{
        message: alertMessage,
        props: allTasksDone ? { severity: 'success' } : undefined,
      }}
    >
      <ContentWrapper>
        <ProgressList tasks={tasks} nlsBundles={[escaApiProgressNLS]} />
      </ContentWrapper>
    </ListActionDialog>
  );
};

RefreshAPIDialog.propTypes = {
  apiDistribution: entryPropType,
  sourceDistribution: entryPropType,
  refreshDistributions: PropTypes.func,
  closeDialog: PropTypes.func,
};

export default RefreshAPIDialog;
