import { findExtendedEntry } from 'models/utils/extension';
import { isForm } from 'models/forms/utils/forms';
import { getFormItemEntries } from 'models/forms/utils/util';
import {
  RDF_TYPE_PROFILE_FORM,
  RDF_PROPERTY_BUILD_EXCLUDE,
  RDF_PROPERTY_BUILD_INCLUDE,
  RDF_PROPERTY_BUILD_SELECTION,
  RDF_PROPERTY_ITEM,
  RDF_PROPERTY_INLINE,
} from 'models/utils/ns';
import { entrystore, entrystoreUtil } from 'commons/store';
import { Context, Entry } from '@entryscape/entrystore-js';
import { Graph } from '@entryscape/rdfjson';
import { isInlineField } from 'models/fields/utils/fields';
import { getLabel } from 'commons/util/rdfUtils';
import { CircularError } from 'models/utils/errors';

/**
 * Retrieves all extended entries for an entry including itself.
 *
 * @param {Entry} entry
 * @param {Entry[]} entries
 * @returns {Promise<Entry[]>}
 */
const getExtendedEntries = async (entry, entries = []) => {
  const extendedEntry = await findExtendedEntry(
    entry.getMetadata(),
    entry.getResourceURI()
  );
  if (!extendedEntry && !entries.length) return [entry];
  if (!extendedEntry) return [entry, ...entries];
  return getExtendedEntries(extendedEntry, [entry, ...entries]);
};

/**
 * @param {Entry} inlineFieldEntry
 * @returns {Promise<Entry>}
 */
export const getObjectFormEntry = (inlineFieldEntry) => {
  const metadata = inlineFieldEntry.getMetadata();
  const resourceURI = inlineFieldEntry.getResourceURI();
  const objectFormResourceURI = metadata.findFirstValue(
    resourceURI,
    RDF_PROPERTY_INLINE
  );
  return entrystoreUtil.getEntryByResourceURI(objectFormResourceURI);
};

/**
 * Extracts both extended entries and form item entries if it's a form. Also
 * relation to form items are added for forms, which is needed for the build
 * process.
 *
 * @param {object} buildItem
 * @returns {Promise<object[]>}
 */
export const extractBuildItems = async (buildItem, excludeEntries = []) => {
  const { entry } = buildItem;

  /**
   * Extracts the items included in a form
   *
   * @param {Entry} formEntry
   * @returns {Promise<object[]>}
   */
  const extractFormBuildItems = async (formEntry) => {
    const metadata = formEntry.getMetadata();
    const resourceURI = formEntry.getResourceURI();

    // get form items for the form
    const formItemEntries = await getFormItemEntries(metadata, resourceURI);

    // extract dependencies for each form item. This is needed since a form item
    // can be a form or an extended field
    const extractedBuildItems = [];
    for (const formItemEntry of formItemEntries) {
      if (excludeEntries.includes(formItemEntry)) {
        throw new CircularError(getLabel(formItemEntry));
      } else {
        const formBuildItems = await extractBuildItems(
          { entry: formItemEntry },
          [...excludeEntries, formEntry]
        );
        extractedBuildItems.push(formBuildItems);
      }
    }
    const buildItems = extractedBuildItems
      .flat()
      .filter((formBuildItem) => formBuildItem);
    return [...buildItems, { entry: formEntry, items: formItemEntries }];
  };

  // If it's a form, its form items needs to be extracted.
  // Since a form can be extended, that needs be handled as well
  if (isForm(entry)) {
    const formEntries = await getExtendedEntries(entry);
    const formBuildItems = [];
    for (const formEntry of formEntries) {
      const buildItems = await extractFormBuildItems(formEntry);
      formBuildItems.push(...buildItems);
    }
    return formBuildItems;
  }

  // If it's an inline field, it will be merged with its object form when the
  // template is generated. Thus the extracted build item is the combination of
  // the inline field and object form. Note the following rules:
  // - An object form can be extended but an inline field can't
  // - The object form must be in same model
  if (isInlineField(entry)) {
    const objectFormEntry = await getObjectFormEntry(entry);
    if (!objectFormEntry) return [];
    if (excludeEntries.includes(entry)) {
      // since an inline field will have form items indirectly, circular
      // dependencies needs to be checked, as for other forms
      throw new CircularError(getLabel(entry));
    }
    const buildItems = await extractBuildItems({ entry: objectFormEntry }, [
      ...excludeEntries,
      entry,
    ]);
    return buildItems.map((item) =>
      item.entry === objectFormEntry
        ? { ...item, entry, inlineEntry: objectFormEntry }
        : item
    );
  }

  // if it's not a form, make sure all field extensions are included
  const extendedFields = await getExtendedEntries(entry);
  return extendedFields.map((extended) => ({ entry: extended }));
};

/**
 *
 * @param {object[]} buildItems
 * @param {Context[]} excludedContexts
 * @returns {object}
 */
export const getDependenciesByContext = async (
  buildItems,
  excludedContexts = []
) => {
  const dependenciesByContext = {};

  for (const buildItem of buildItems) {
    const { entry } = buildItem;
    const context = entry.getContext();
    const contextEntry = await context.getEntry();
    const modelId = contextEntry.getId();

    if (!(modelId in dependenciesByContext)) {
      dependenciesByContext[modelId] = {
        include: !excludedContexts.includes(context),
        contextEntry,
        items: [],
      };
    }
    dependenciesByContext[modelId].items.push(buildItem);
  }
  return dependenciesByContext;
};

/**
 * Divides build items by context, treated as separate dependencies when
 * constructing a build.
 *
 * @param {object[]} buildItems
 * @param {Context} currentContext
 * @param {Context[]} excludedContexts
 * @returns {Promise<object[]>}
 */
export const getDependencyItems = async (
  buildItems,
  currentContext,
  excludedContexts
) => {
  const dependenciesByContext = await getDependenciesByContext(
    buildItems,
    excludedContexts
  );

  // convert dependency object to array and place current context first in order
  const currentContextId = currentContext.getEntry(true).getId();
  const { [currentContextId]: currentProjectItem, ...otherItems } =
    dependenciesByContext;

  if (currentProjectItem) {
    currentProjectItem.labelNlsKey = 'currentProjectLabel';
    return [currentProjectItem, ...Object.values(otherItems)];
  }
  return Object.values(otherItems);
};

/**
 * @param {object[]} buildItems
 * @returns {Context[]}
 */
export const getDependentContexts = (buildItems) => {
  const contexts = [];

  for (const buildItem of buildItems) {
    const { entry } = buildItem;
    const context = entry.getContext();
    if (!contexts.includes(context)) {
      contexts.push(context);
    }
  }
  return contexts;
};

/**
 *
 * @param {Context} context
 * @returns {Promise<Entry[]>}
 */
export const getProfileFormEntries = async (context) => {
  const formEntries = await entrystore
    .newSolrQuery()
    .context(context)
    .rdfType([RDF_TYPE_PROFILE_FORM])
    .sort('created+desc')
    .list()
    .getAllEntries();

  // Ignore propertyForms without items (typically those that are extending another form)
  return formEntries.filter((formEntry) => {
    const resourceURI = formEntry.getResourceURI();
    const metadata = formEntry.getMetadata();
    return metadata.find(resourceURI, 'rdf:type', RDF_TYPE_PROFILE_FORM)
      .length === 1
      ? metadata.find(resourceURI, RDF_PROPERTY_ITEM).length > 0
      : true;
  });
};

/**
 * As default all profile forms for a project are used as default selection.
 *
 * @param {Context} context
 * @returns {Promise<object>}
 */
export const getDefaultSelection = async (context) => {
  const profileFormEntries = await getProfileFormEntries(context);
  const selectionPromises = [];
  for (const profileFormEntry of profileFormEntries) {
    const selectionPromise = extractBuildItems({ entry: profileFormEntry });
    selectionPromises.push(selectionPromise);
    await selectionPromise;
  }
  const items = await Promise.all(selectionPromises).then((result) => {
    return result.flat();
  });
  return {
    selection: profileFormEntries,
    items,
  };
};

/**
 * Removes duplicated items
 *
 * @param {object[]} items
 * @returns {object[]}
 */
const excludeDuplicates = (items) => {
  const uniqueItems = [];
  items.forEach((item) => {
    if (uniqueItems.find(({ entry }) => entry === item.entry)) return;
    uniqueItems.push(item);
  });
  return uniqueItems;
};

/**
 * Extracts all forms and field entries, detected as form items or extensions,
 * with addional info used for generating a build and separate dependencies.
 *
 * @param {Entry[]} formOrFieldEntries
 * @returns {Promise<object[]>}
 */
export const getBuildItems = async (formOrFieldEntries) => {
  const buildItemPromises = [];
  for (const formOrFieldEntry of formOrFieldEntries) {
    const buildItemPromise = extractBuildItems({ entry: formOrFieldEntry });
    buildItemPromises.push(buildItemPromise);
    await buildItemPromise;
  }
  const buildItems = await Promise.all(buildItemPromises).then((result) => {
    return result.flat();
  });
  return excludeDuplicates(buildItems);
};

/**
 * Adds selection to metadata graph
 *
 * @param {Graph} graph
 * @param {string} resourceURI
 * @param {Entry[]} selection
 * @returns {Graph}
 */
export const addBuildSelection = (graph, resourceURI, selection) => {
  for (const entry of selection) {
    graph.add(
      resourceURI,
      RDF_PROPERTY_BUILD_SELECTION,
      entry.getResourceURI()
    );
  }
  return graph;
};

/**
 * Adds information about which dependencies should be included or excluded, in
 * the metadata graph
 *
 * @param {Graph} graph
 * @param {string} resourceURI
 * @param {Context[]} dependentContexts
 * @param {Context[]} excludedContexts
 * @returns {Graph}
 */
export const addBuildIncludeAndExclude = (
  graph,
  resourceURI,
  dependentContexts,
  excludedContexts
) => {
  for (const context of dependentContexts) {
    const property = excludedContexts.includes(context)
      ? RDF_PROPERTY_BUILD_EXCLUDE
      : RDF_PROPERTY_BUILD_INCLUDE;
    graph.add(resourceURI, property, context.getEntry(true).getResourceURI());
  }
  return graph;
};
