import PropTypes from 'prop-types';
import { useState } from 'react';
import {
  Divider,
  Drawer,
  IconButton,
  Link as MuiLink,
  List,
  ListItemButton,
  Typography,
} from '@mui/material';
import {
  Close as CloseIcon,
  OpenInNew as OpenInNewIcon,
  QuestionAnswer as CommunityIcon,
  Description as DocsIcon,
  Support as SupportIcon,
} from '@mui/icons-material';
import { warning } from 'commons/theme/mui/colors';
import Tooltip from 'commons/components/common/Tooltip';
import SearchInput from 'commons/components/SearchInput';
import { i18n } from 'esi18n';
import { nlsPropType, useTranslation } from 'commons/hooks/useTranslation';
import { localize } from 'commons/locale';
import escoLayoutNLS from 'commons/nls/escoLayout.nls';
import config from 'config';
import './HelpDrawer.scss';

const PREFERS_REDUCED_MOTION = window.matchMedia(
  '(prefers-reduced-motion)'
).matches;

const MAIN_CONTENT_ID = 'main-content';

const resetScroll = () =>
  document.getElementById(MAIN_CONTENT_ID).scrollTo({
    top: 0,
    left: 0,
    behavior: PREFERS_REDUCED_MOTION ? 'auto' : 'smooth',
  });

const HelpDrawer = ({ open, toggle, helpLinks = [] }) => {
  const locale = i18n.getLocale();
  const translate = useTranslation(escoLayoutNLS);

  const [searchQuery, setSearchQuery] = useState('');
  const clearSearch = () => setSearchQuery('');
  const onSearchQueryChange = (event) => {
    setSearchQuery(event.target.value.toLowerCase());
  };

  const {
    enabled: supportEnabled,
    url: supportURL,
    label: supportLabel,
  } = config.get('entryscape.support');
  const {
    enabled: communityEnabled,
    url: communityURL,
    label: communityLabel,
  } = config.get('entryscape.community');

  if (!helpLinks.length) return null;
  return (
    <Drawer
      open={open}
      onClose={toggle}
      anchor="right"
      classes={{ paper: 'escoHelpDrawer' }}
      variant="persistent"
      hideBackdrop
      onMouseLeave={resetScroll}
    >
      <div className="escoHelpDrawer__header">
        <div className="escoHelpDrawer__headerTitle">
          <Typography variant="h2">{translate('helpTitle')}</Typography>
          <Tooltip title={translate('helpCloseTooltip')}>
            <IconButton onClick={toggle} size="small">
              <CloseIcon />
            </IconButton>
          </Tooltip>
        </div>
        <div className="escoHelpDrawer__headerSearch">
          <SearchInput
            query={searchQuery}
            onChange={onSearchQueryChange}
            clear={clearSearch}
            placeholder={translate('helpSearchPlaceholder')}
            clearTooltip={translate('helpSearchClearTooltip')}
            inputClass="escoHelpDrawer__searchInput"
          />
        </div>
      </div>
      <Divider />
      <div className="escoHelpDrawer__content">
        <List>
          {helpLinks
            .filter(({ label }) =>
              label[locale]?.toLowerCase().includes(searchQuery.trim())
            )
            .map(({ label, links, highlightId }, index) => (
              <HelpLink
                label={label}
                link={links}
                highlightId={highlightId}
                // eslint-disable-next-line react/no-array-index-key
                key={`helplink-${highlightId}-${index}`}
              />
            ))}
        </List>
      </div>
      <Divider />
      <div className="escoHelpDrawer__footer">
        <List>
          <FooterLink
            href={`https://docs.entryscape.com/${locale}`}
            label={translate('docsLinkLabel')}
            Icon={DocsIcon}
          />
          {communityEnabled ? (
            <FooterLink
              href={communityURL}
              label={localize(communityLabel)}
              Icon={CommunityIcon}
            />
          ) : null}
          {supportEnabled ? (
            <FooterLink
              href={supportURL}
              label={localize(supportLabel)}
              Icon={SupportIcon}
            />
          ) : null}
        </List>
      </div>
    </Drawer>
  );
};

HelpDrawer.propTypes = {
  open: PropTypes.bool,
  toggle: PropTypes.func,
  helpLinks: PropTypes.arrayOf(
    PropTypes.shape({
      label: nlsPropType,
      links: PropTypes.shape({
        en: PropTypes.string,
        de: PropTypes.string,
        sv: PropTypes.string,
      }),
      highlight: PropTypes.string,
    })
  ),
};

const HelpLink = ({ label, link, highlightId }) => {
  const locale = i18n.getLocale();
  const defaultLocale = config.get('locale.fallback');

  const localizedLabel = label[locale] || label[defaultLocale];
  const localizedURI =
    typeof link === 'string' ? link : link[locale] || link[defaultLocale];

  const handleMouseOver = () => {
    const elements = document.querySelectorAll(
      `[data-highlight-id="${highlightId}"]`
    );
    elements.forEach((element) => {
      element.style.outline = `8px solid ${warning.main}`;
      element.scrollIntoView({
        behavior: PREFERS_REDUCED_MOTION ? 'auto' : 'smooth',
        block: 'end',
        inline: 'nearest',
      });
    });
  };
  const handleMouseOut = () => {
    const elements = document.querySelectorAll(
      `[data-highlight-id="${highlightId}"]`
    );
    elements.forEach((element) => {
      element.style.outline = 0;
    });
  };

  const highlightProps = highlightId
    ? {
        onMouseOver: handleMouseOver,
        onMouseOut: handleMouseOut,
      }
    : {};

  return (
    <ListItemLink
      href={localizedURI}
      key={localizedURI}
      className="escoHelpDrawer__listItem"
      {...highlightProps}
    >
      <Typography>{localizedLabel}</Typography>
      <OpenInNewIcon className="escoHelpDrawer__linkIcon" />
    </ListItemLink>
  );
};

HelpLink.propTypes = {
  label: nlsPropType,
  link: PropTypes.oneOfType([PropTypes.string, nlsPropType]),
  highlightId: PropTypes.string,
};

const ListItemLink = ({ children, href, ...props }) => (
  <ListItemButton
    key={href}
    component={MuiLink}
    href={href}
    target="_blank"
    alignItems="center"
    {...props}
  >
    {children}
  </ListItemButton>
);

ListItemLink.propTypes = {
  children: PropTypes.node,
  href: PropTypes.string,
};

const FooterLink = ({ href, label, Icon }) => (
  <ListItemLink href={href}>
    <Icon size="small" className="escoHelpDrawer__footerIcon" />
    <Typography variant="body2">{label}</Typography>
    <OpenInNewIcon className="escoHelpDrawer__linkIcon" />
  </ListItemLink>
);

FooterLink.propTypes = {
  href: PropTypes.string,
  label: PropTypes.string,
  Icon: PropTypes.elementType,
};

export default HelpDrawer;
