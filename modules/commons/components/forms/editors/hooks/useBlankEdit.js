import { useFieldsEdit } from './useFieldsEdit';
import { BLANK } from '../../utils/nodetypes';
import { BlankGroup } from '../utils/BlankGroup';

export const useBlankEdit = ({
  graph,
  resourceURI,
  rdfType,
  name,
  property,
  dispatch,
  validate,
  insertEmptyField,
}) => {
  const { fieldGroup, removeField, addField, ...fieldEditResult } =
    useFieldsEdit({
      name,
      graph,
      resourceURI,
      rdfType,
      property,
      dispatch,
      nodetype: BLANK,
      FieldGroupClass: BlankGroup,
      validate,
      insertEmptyField,
    });

  return {
    ...fieldEditResult,
    blanks: fieldGroup.getFields(),
    removeBlank: removeField,
    addBlank: addField,
    fieldGroup,
  };
};
