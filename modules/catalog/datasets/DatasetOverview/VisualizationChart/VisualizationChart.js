import { useMemo } from 'react';
import PropTypes from 'prop-types';
import { Chart, getScaleOptions } from 'commons/components/chart/Chart';
import { useTranslation } from 'commons/hooks/useTranslation';
import escoListNLS from 'commons/nls/escoList.nls';
import escaDatasetNLS from 'catalog/nls/escaDataset.nls';
import escaVisualizationNLS from 'catalog/nls/escaVisualization.nls';
import escoGeoMapNLS from 'commons/nls/escoGeoMap.nls';
import { getChartData } from './utils/csvData';

const nlsBundles = [
  escoListNLS,
  escaDatasetNLS,
  escaVisualizationNLS,
  escoGeoMapNLS,
];

const CHART_OPTIONS = {
  legend: {
    display: false,
  },
};

const PIE_CHART_OPTIONS = {
  legend: {
    display: true,
  },
  scales: {
    ticks: {
      display: false,
    },
  },
};

const useYAxisLabel = (yField, operation) => {
  const translate = useTranslation(nlsBundles);
  if (operation === 'sum') return translate('yAxisSumLabel');
  if (operation === 'count') return translate('yAxisCountLabel');
  return yField;
};

const VisualizationChart = ({ data, type, xField, yField, operation }) => {
  const yAxisLabel = useYAxisLabel(yField, operation);

  const { chartData, chartOptions } = useMemo(
    () => ({
      chartData: getChartData({
        data,
        xField,
        yField,
        operation: type === 'bar' ? operation : '',
        type,
      }),
      chartOptions: {
        ...CHART_OPTIONS,
        ...getScaleOptions(xField, yAxisLabel),
        ...(type === 'pie' ? PIE_CHART_OPTIONS : {}),
      },
    }),
    [data, type, xField, yField, yAxisLabel, operation]
  );

  return (
    <Chart data={chartData} type={type} height="400px" options={chartOptions} />
  );
};
VisualizationChart.propTypes = {
  data: PropTypes.shape({}),
  type: PropTypes.string,
  xField: PropTypes.string,
  yField: PropTypes.string,
  operation: PropTypes.string,
};

export default VisualizationChart;
