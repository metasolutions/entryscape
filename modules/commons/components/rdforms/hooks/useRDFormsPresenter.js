import { useState, useMemo } from 'react';
import { itemStore } from 'commons/rdforms/itemstore';
import { Presenter as RDFormsPresenter } from '@entryscape/rdforms/renderers/react';
import { useLanguage } from 'commons/hooks/useLanguage';
import 'commons/components/rdforms/LinkBehaviour';

const useRDFormsPresenter = (
  graph,
  resource,
  templateId,
  compact = false,
  filterPredicates = {},
  rdformsTemplate,
  markOrigin = true,
  filterTranslations = true,
  defaultLanguage,
  locale = null
) => {
  const [language] = useLanguage();
  const [predicates] = useState(filterPredicates);
  const presenter = useMemo(() => {
    if ((rdformsTemplate || templateId) && graph && resource) {
      return new RDFormsPresenter({
        graph,
        template: rdformsTemplate || itemStore.getItem(templateId),
        resource,
        compact,
        filterPredicates: predicates,
        markOrigin,
        filterTranslations,
        defaultLanguage,
        locale,
      });
    }
    return null;
  }, [
    graph,
    resource,
    rdformsTemplate,
    predicates,
    templateId,
    compact,
    markOrigin,
    filterTranslations,
    defaultLanguage,
    locale,
    language, // language is used as a dependency to trigger a re-render
  ]);

  return {
    PresenterComponent: presenter?.domNode.component,
    presenter,
  };
};

export default useRDFormsPresenter;
