import { entrystore } from 'commons/store';
import { RDF_TYPE_BUNDLES } from 'models/utils/ns';
import { Entry } from '@entryscape/entrystore-js';

/**
 *
 * @param {Entry} entry
 * @returns {Promise<Entry[]>}
 */
const getResourceEntries = async (entry) => {
  const resourceDescriptionURIs = entry
    .getMetadata()
    .find(entry.getResourceURI(), 'prof:hasResource')
    .map((statement) => statement.getValue());

  if (!resourceDescriptionURIs.length) return [];

  const resourceEntries = await entrystore
    .newSolrQuery()
    .resource(resourceDescriptionURIs)
    .context(entry.getContext())
    .getEntries();
  return resourceEntries;
};

/**
 *
 * @param {Entry[]} resourceEntries
 * @returns {Entry[]}
 */
const filterOnDiagram = (resourceEntries) => {
  return resourceEntries.filter(
    (entry) =>
      entry
        .getMetadata()
        .find(null, 'rdf:type', 'http://purl.org/dc/dcmitype/Image').length
  );
};

/**
 *
 * @param {Entry[]} entries
 * @param {string} format
 * @returns {Entry[]}
 */
const filterOnFormat = (entries, format) => {
  return entries.filter((entry) => entry.getEntryInfo().getFormat() === format);
};

/**
 *
 * @param {Entry} entry
 * @returns {Promise<object>}
 */
export const getSpecificationResources = async (entry) => {
  const resourceEntries = await getResourceEntries(entry);

  // get image
  const diagramEntries = filterOnDiagram(resourceEntries);
  const [svgEntry] = filterOnFormat(diagramEntries, 'image/svg+xml');
  const [pngEntry] = filterOnFormat(diagramEntries, 'image/png');
  const imageEntry = svgEntry || pngEntry;
  const image = imageEntry?.getResourceURI() || '';

  // get json bundle from bundle entry
  let bundle = {};
  const [bundleEntry] = resourceEntries.filter(
    (resourceEntry) =>
      resourceEntry.getMetadata().find(null, 'rdf:type', RDF_TYPE_BUNDLES)
        .length
  );
  if (bundleEntry) {
    bundle = await bundleEntry.getResource(true).getJSON();
  }

  return { bundle, image };
};
