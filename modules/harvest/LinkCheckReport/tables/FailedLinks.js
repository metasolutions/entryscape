import { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import Table from '@mui/material/Table';
import TableHead from '@mui/material/TableHead';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableSortLabel from '@mui/material/TableSortLabel';
import SortOrderIcon from '@mui/icons-material/ArrowDropUp';
import Link from '@mui/material/Link';
import Typography from '@mui/material/Typography';
import { Button, TablePagination } from '@mui/material';
import TableContainer from '@mui/material/TableContainer';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import { useTranslation } from 'commons/hooks/useTranslation';
import esreLinkCheckReportNLS from 'harvest/nls/esreLinkCheckReport.nls';
import escoListNLS from 'commons/nls/escoList.nls';
import LinkCheckDetails from 'harvest/LinkCheckReport/dialogs/LinkCheckDetails';

const getLinkCheckResult = ({
  uri,
  status,
  statusCode,
  statusMessage,
  attempts,
  checkedAt,
}) => {
  return [
    {
      label: 'uri',
      value: uri,
    },
    {
      label: 'status',
      value: status,
    },
    {
      label: 'statusCode',
      value: statusCode,
    },
    {
      label: 'statusMessage',
      value: statusMessage,
    },
    {
      label: 'attempts',
      value: attempts,
    },
    {
      label: 'checkedAt',
      value: checkedAt,
    },
  ];
};

const getEntryData = ({ entryType, entryLabel, entryURI, property }) => {
  return [
    {
      label: 'entryType',
      value: entryType,
    },
    {
      label: 'entryLabel',
      value: entryLabel,
    },
    {
      label: 'entryURI',
      value: entryURI,
    },
    {
      label: 'property',
      value: property,
    },
  ];
};

/**
 *
 * @param {object} a
 * @param {object} b
 * @param {string} orderBy
 * @returns {number}
 */
const descendingComparator = (a, b, orderBy) => {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
};

/**
 *
 * @param {string} order
 * @param {string} orderBy
 * @returns {Function}
 */
const getComparator = (order, orderBy) => {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
};

const LIMIT = 10;
const URI = 'uri';
const STATUS = 'status';

const SortableTableCell = ({
  label,
  property,
  order,
  orderBy,
  handleSort,
  align = 'center',
}) => {
  const translate = useTranslation(escoListNLS);
  const tooltip =
    order === 'asc'
      ? translate('sortFieldInDesc', property)
      : translate('sortFieldInAsc', property);
  const active = orderBy === property;

  return (
    <TableCell align={align} sortDirection={orderBy === URI ? order : false}>
      <TableSortLabel
        active={active}
        direction={active ? order : 'asc'}
        onClick={() => handleSort(property)}
        IconComponent={SortOrderIcon}
        title={tooltip}
      >
        {label}
      </TableSortLabel>
    </TableCell>
  );
};

SortableTableCell.propTypes = {
  label: PropTypes.string,
  property: PropTypes.oneOf([URI, STATUS]),
  order: PropTypes.oneOf(['asc', 'desc']),
  orderBy: PropTypes.oneOf([URI, STATUS]),
  handleSort: PropTypes.func,
  align: PropTypes.oneOf(['center', 'left', 'right']),
};

const FailedLinks = ({ failedLinks }) => {
  const translate = useTranslation(esreLinkCheckReportNLS);
  const [open, setOpen] = useState(false);
  const [selectedEntryData, setSelectedEntryData] = useState([]);
  const [selectedLinkCheckResult, setLinkCheckResult] = useState([]);
  const [page, setPageNumber] = useState(0);
  const [failedLinksPerPage, setFailedLinksPerPage] = useState([]);
  const [order, setOrder] = useState('asc');
  const [orderBy, setOrderBy] = useState(URI);
  const count = failedLinks.length;

  useEffect(() => {
    const sorted = failedLinks.sort(getComparator(order, orderBy));

    if (count) {
      setFailedLinksPerPage(sorted.slice(page * LIMIT, (page + 1) * LIMIT));
    }
  }, [count, failedLinks, page, order, orderBy]);

  const handleClick = (failedLink) => {
    setOpen(true);
    setSelectedEntryData(getEntryData(failedLink));
    setLinkCheckResult(getLinkCheckResult(failedLink));
  };

  // eslint-disable-next-line no-unused-vars
  const handleSort = (property) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  return (
    <div>
      {failedLinks.length > 0 && (
        <>
          <h3 className="esreTable__header">{translate('failedHeader')}</h3>
          <TableContainer component={Paper}>
            <Table size="small" aria-label={translate('failedHeader')}>
              <TableHead>
                <TableRow>
                  <SortableTableCell
                    label={translate('uri')}
                    property={URI}
                    order={order}
                    orderBy={orderBy}
                    handleSort={handleSort}
                    align="left"
                  />
                  <SortableTableCell
                    label={translate('status')}
                    property={STATUS}
                    order={order}
                    orderBy={orderBy}
                    handleSort={handleSort}
                  />
                  <TableCell className="esreTableCell--bold" align="center">
                    {translate('details')}
                  </TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {failedLinksPerPage.map((failedLink) => (
                  <TableRow hover key={failedLink.uri}>
                    <TableCell
                      className="esreTableCell--bold"
                      component="th"
                      scope="row"
                    >
                      <Typography classes={{ root: 'esreUriLink' }}>
                        <Link
                          href={failedLink.uri}
                          color="inherit"
                          target="_blank"
                          rel="noopener"
                        >
                          {failedLink.uri}
                        </Link>
                      </Typography>
                    </TableCell>
                    <TableCell
                      classes={{ root: 'esreTableCell--broken' }}
                      align="center"
                    >
                      {failedLink.statusMessage}
                    </TableCell>
                    <TableCell align="center">
                      <Button
                        onClick={() => handleClick(failedLink)}
                        variant="text"
                      >
                        {translate('view')}
                      </Button>
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
            {open && (
              <LinkCheckDetails
                open={open}
                onClose={() => setOpen(false)}
                selectedEntryData={selectedEntryData}
                selectedLinkCheckResult={selectedLinkCheckResult}
              />
            )}
          </TableContainer>
          <TablePagination
            component="div"
            count={count}
            page={page}
            onPageChange={(_, newPage) => {
              setPageNumber(newPage);
            }}
            rowsPerPage={LIMIT}
            rowsPerPageOptions={[]}
          />
        </>
      )}
    </div>
  );
};

FailedLinks.propTypes = {
  failedLinks: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
};

export default FailedLinks;
