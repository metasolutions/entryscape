import PropTypes from 'prop-types';
import {
  LiteralEditor,
  REMOVE_FIELD_GROUPS,
} from 'commons/components/forms/editors';
import SelectEditMode from 'models/components/SelectEditMode';
import esmoCommonsNls from 'models/nls/esmoCommons.nls';
import { RESOURCE } from 'models/utils/metadata';

export const ResourceEditor = ({
  editModeChoices,
  onEditModeChange,
  dispatch,
  ...props
}) => {
  const handleSelectEditMode = (value, fieldGroup) => {
    dispatch({
      type: REMOVE_FIELD_GROUPS,
      fieldGroups: [fieldGroup],
    });
    onEditModeChange(value);
  };

  return (
    <LiteralEditor
      renderInlineContent={(groupField) => (
        <SelectEditMode
          value={RESOURCE}
          choices={editModeChoices}
          nlsBundles={esmoCommonsNls}
          onChange={(value) => handleSelectEditMode(value, groupField)}
        />
      )}
      dispatch={dispatch}
      {...props}
    />
  );
};

ResourceEditor.propTypes = {
  editModeChoices: PropTypes.arrayOf(PropTypes.shape({})),
  onEditModeChange: PropTypes.func,
  dispatch: PropTypes.func,
};
