import { useMemo } from 'react';
import { ListQuery } from '../utils/ListQuery';
import { useListModel } from './useListModel';

/**
 * Hook for client side list querying
 *
 * @param {object} listQueryParams
 * @param {object[]} listQueryParams.items
 * @param {string[]} listQueryParams.searchFields
 * @returns {object}
 */
export const useListQuery = ({ items, searchFields }) => {
  const [{ search, page, sort, limit }] = useListModel();

  const listQuery = useMemo(() => {
    return new ListQuery({ items, rowsPerPage: limit })
      .search(search, searchFields)
      .sort(sort);
  }, [items, search, limit, sort, searchFields]);

  const listQueryObject = useMemo(() => {
    return {
      size: listQuery.getSize(),
      result: listQuery.getResult(page),
      search,
    };
  }, [listQuery, page, search]);

  if (!items) return { size: -1, result: [], search: '' };
  return listQueryObject;
};
