import { Graph } from '@entryscape/rdfjson';
import { Entry } from '@entryscape/entrystore-js';
import { entrystore } from 'commons/store';
import { getOrIgnoreEntry } from 'commons/util/entry';
import {
  RDF_PROPERTY_EXTENDS,
  RDF_PROPERTY_OVERRIDE_FORM_ITEMS,
} from 'models/utils/ns';

/**
 *
 * @param {Graph} graph
 * @param {string} resourceURI
 * @returns {boolean}
 */
export const isExtended = (graph, resourceURI) => {
  return Boolean(graph.findFirstValue(resourceURI, RDF_PROPERTY_EXTENDS));
};

/**
 * Tries to find extension value in graph and if found return extended entry.
 *
 * @param {Graph} graph
 * @param {string} resourceURI
 * @returns {Promise<Entry|undefined>}
 */
export const findExtendedEntry = async (graph, resourceURI) => {
  const extendedURI = graph.findFirstValue(resourceURI, RDF_PROPERTY_EXTENDS);
  if (!extendedURI) return;

  return getOrIgnoreEntry(entrystore.getEntryURIFromURI(extendedURI));
};

/**
 * Tries find extension and match given property recursively.
 *
 * @param {Graph} graph
 * @param {string} resourceURI
 * @param {string} property
 * @param {boolean} hasValue
 * @returns {Promise<Entry|undefined>}
 */
export const getExtendedEntry = async (
  graph,
  resourceURI,
  property,
  hasValue
) => {
  const extendedEntry = await findExtendedEntry(graph, resourceURI);
  if (!extendedEntry) return;

  const extendedMetadata = extendedEntry.getMetadata();
  const extendedResourceURI = extendedEntry.getResourceURI();

  for (const prop of property.split(' ')) {
    if (hasValue(extendedMetadata, extendedResourceURI, prop))
      return extendedEntry;
  }

  return getExtendedEntry(
    extendedMetadata,
    extendedResourceURI,
    property,
    hasValue
  );
};

/**
 *
 * @param {Graph} graph
 * @param {string} resourceURI
 * @returns {boolean}
 */
export const checkOverrideFormItems = (graph, resourceURI) => {
  return (
    graph.findFirstValue(resourceURI, RDF_PROPERTY_OVERRIDE_FORM_ITEMS) ===
    'true'
  );
};
