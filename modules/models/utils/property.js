import { Graph } from '@entryscape/rdfjson';
import { Entry } from '@entryscape/entrystore-js';
import { RDF_PROPERTY_PROPERTY } from './ns';
import {
  RESOURCE,
  NAMESPACE_ENTRY,
  RESOURCE_ENTRY,
  getURIEditorType,
  getNamespaceEntryValue,
  getResourceEntryValue,
} from './metadata';

/**
 * @param {{graph: Graph, resourceURI: string}} props
 * @returns {Promise<string>}
 */
export const getPropertyValue = ({ graph, resourceURI }) => {
  const propertyType = getURIEditorType(
    graph,
    resourceURI,
    RDF_PROPERTY_PROPERTY,
    RESOURCE_ENTRY
  );

  if (propertyType === RESOURCE)
    return Promise.resolve(
      graph.findFirstValue(resourceURI, RDF_PROPERTY_PROPERTY)
    );

  if (propertyType === NAMESPACE_ENTRY) {
    return getNamespaceEntryValue(graph, resourceURI, RDF_PROPERTY_PROPERTY);
  }

  return getResourceEntryValue(graph, resourceURI, RDF_PROPERTY_PROPERTY);
};

/**
 * Wrapper for getPropertyValue using entry
 *
 * @param {Entry} entry
 * @returns {Promise<string>}
 */
export const getProperty = (entry) => {
  return getPropertyValue({
    graph: entry.getAllMetadata(),
    resourceURI: entry.getResourceURI(),
  });
};
