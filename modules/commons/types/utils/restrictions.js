import { EntityType } from 'entitytype-lookup';

/**
 *
 * @param {object} userInfo
 * @param {string} restrictTo
 * @returns {boolean}
 */
export const hasUserAccess = (userInfo, restrictTo) => {
  if (restrictTo) {
    switch (restrictTo) {
      case 'adminUser':
        return userInfo.isAdmin;
      case 'adminGroup':
        return userInfo.inAdminGroup;
      case 'admin':
        return userInfo.hasAdminRights;
      default:
        throw new Error(`Invalid restrictTo group '${restrictTo}' is used`);
    }
  }
  return true;
};

/**
 * Entitytypes can be restricted to one or several modules, if no restriction is given
 * it is interpreted as it is available in all modules.
 *
 * @param {EntityType} entityType
 * @param {string} currentModule
 * @param {boolean} restrictedToModule
 * @returns {boolean}
 */
const availableForModule = (entityType, currentModule, restrictedToModule) => {
  const module = entityType.get('module');
  if (!restrictedToModule && !module) return true;
  return Array.isArray(module)
    ? module.includes(currentModule)
    : module === currentModule;
};

/**
 *
 * @param {EntityType[]} entityTypes
 * @param {object} userInfo
 * @param {string} currentModule
 * @param {boolean} restrictedToModule
 * @returns {EntityType[]}
 */
export const filterOnRestrictions = (
  entityTypes,
  userInfo,
  currentModule,
  restrictedToModule
) => {
  return entityTypes.filter((entityType) => {
    return (
      entityType &&
      availableForModule(entityType, currentModule, restrictedToModule) &&
      hasUserAccess(userInfo, entityType.get('restrictTo')) &&
      entityType.global()
    );
  });
};
