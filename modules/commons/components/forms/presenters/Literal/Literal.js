import PropTypes from 'prop-types';
import { Fields } from '../Fields';
import { Field } from '../Field';
import { Text } from '../Text';

export const Literal = ({
  graph,
  resourceURI,
  property,
  inline,
  children,
  ...fieldsProps
}) => {
  const statements = graph.find(resourceURI, property);

  return statements.length ? (
    <Fields inline={inline} {...fieldsProps}>
      {statements.map((statement, index) => {
        return (
          // eslint-disable-next-line react/no-array-index-key
          <Field key={index} inline={inline}>
            <Text text={statement.getValue()} />
          </Field>
        );
      })}
      {children}
    </Fields>
  ) : null;
};

Literal.propTypes = {
  graph: PropTypes.shape({ find: PropTypes.func }),
  resourceURI: PropTypes.string,
  property: PropTypes.string,
  inline: PropTypes.bool,
  children: PropTypes.node,
};
