import { Grid, Box } from '@mui/material';
import ConditionalWrapper from 'commons/components/common/ConditionalWrapper';
import Tooltip from 'commons/components/common/Tooltip';
import PropTypes from 'prop-types';
import './ListItemIcon.scss';

export const ListItemIcon = ({
  xs = 1,
  justifyContent = 'center',
  Icon,
  icon = null,
  tooltip,
}) => {
  return (
    <Grid item xs={xs} justifyContent={justifyContent}>
      <Box
        component="span"
        className="escoListItemIcon"
        justifyContent={justifyContent}
      >
        <ConditionalWrapper
          condition={Boolean(tooltip)}
          // eslint-disable-next-line react/no-unstable-nested-components
          wrapper={(children) => <Tooltip title={tooltip}>{children}</Tooltip>}
        >
          {Icon ? <Icon /> : icon}
        </ConditionalWrapper>
      </Box>
    </Grid>
  );
};

ListItemIcon.propTypes = {
  xs: PropTypes.number,
  justifyContent: PropTypes.string,
  Icon: PropTypes.shape({}),
  icon: PropTypes.node,
  tooltip: PropTypes.string,
};
