import PropTypes from 'prop-types';
import { Box, Stack, Typography } from '@mui/material';
import BrokenIcon from '@mui/icons-material/BrokenImage';
import './ErrorPlaceholder.scss';

const ErrorPlaceholder = ({
  Icon = BrokenIcon,
  message,
  header,
  className = 'escoErrorPlaceholder',
}) => {
  return (
    <Box className={className}>
      <Stack
        direction="column"
        alignItems="center"
        className="escoErrorPlaceholder__content"
      >
        <div>
          <Icon classes={{ root: 'escoErrorPlaceholder__icon' }} />
        </div>
        <Typography
          variant="h2"
          gutterBottom
          sx={{ marginBottom: '18px' }}
          align="center"
        >
          {header}
        </Typography>
        <Typography variant="body1">{message}</Typography>
      </Stack>
    </Box>
  );
};

ErrorPlaceholder.propTypes = {
  Icon: PropTypes.elementType,
  header: PropTypes.string,
  message: PropTypes.string,
  className: PropTypes.string,
};

export default ErrorPlaceholder;
