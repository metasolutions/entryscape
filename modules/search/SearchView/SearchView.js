import { useCallback, useMemo } from 'react';
import { entrystore } from 'commons/store';
import { types } from '@entryscape/entrystore-js';
import registry from 'commons/registry';
import config from 'config';
import {
  withListModelProviderAndLocation,
  ListItemText,
} from 'commons/components/ListView';
import {
  EntryListView,
  useSolrQuery,
  TITLE_COLUMN,
  MODIFIED_COLUMN,
  INFO_COLUMN,
} from 'commons/components/EntryListView';
import { LIST_ACTION_INFO } from 'commons/components/EntryListView/actions';
import useSearchFilters from 'commons/components/filters/hooks/useSearchFilters';
import Lookup from 'commons/types/Lookup';
import { getFullLengthLabel } from 'commons/util/rdfUtils';
import { isGuest } from 'commons/util/user';
import { getActiveModules } from 'commons/util/config';
import { useUserState } from 'commons/hooks/useUser';
import { localize } from 'commons/locale';
import { useTranslation } from 'commons/hooks/useTranslation';
import escoListNLS from 'commons/nls/escoList.nls';
import OverviewLinkIcon from 'commons/components/OverviewLinkIcon';
import {
  RDF_CLASS_CUSTOM_ENTITY_TYPE,
  RDF_CLASS_ENTITY_TYPE,
  RDF_CLASS_PROJECT_TYPE,
} from 'admin/configs/utils/namespace';
import { ANY_FILTER_ITEM } from 'commons/components/filters/utils/filterDefinitions';
import {
  CONTEXT_FILTER,
  TYPE_FILTER,
  getContextItems,
  getEntityTypeItems,
  getExcludedUris,
  getEntityTypes,
  getOverviewName,
  getTo,
  getCanAccessOverview,
} from 'commons/util/overview';
import esseSearchNLS from '../nls/esseSearch.nls';
import LinkedDataBrowserDialog from './SearchLDBDialog';

const nlsBundles = [escoListNLS, esseSearchNLS];

const SearchView = () => {
  const translate = useTranslation(nlsBundles);
  const { userEntry, userInfo } = useUserState();
  const isGuestUser = useMemo(() => isGuest(userEntry), [userEntry]);
  const activeModuleNames = getActiveModules(userInfo).map(({ name }) => name);
  const defaultContext = config.get('search.defaultContext');
  const defaultType = config.get('search.defaultEntityType');

  const filters = [
    {
      ...CONTEXT_FILTER,
      autoHide: false,
      items: [ANY_FILTER_ITEM],
      loadItems: () => getContextItems(isGuestUser, activeModuleNames),
      ...(defaultContext
        ? { initialSelection: defaultContext.toString() }
        : {}),
    },
    {
      ...TYPE_FILTER,
      items: getEntityTypeItems(activeModuleNames, translate),
      ...(defaultType ? { initialSelection: defaultType } : {}),
    },
  ];
  const { applyFilters, isLoading, hasFilters, ...filterProps } =
    useSearchFilters(filters);

  const createQuery = useCallback(() => {
    const query = entrystore.newSolrQuery().title('*');
    query.uri(getExcludedUris(), true);
    query.rdfType(
      [
        'oa:Annotation',
        'http://entryscape.com/terms/CatalogContext',
        'http://entryscape.com/terms/TerminologyContext',
        'http://entryscape.com/terms/MQA',
        RDF_CLASS_ENTITY_TYPE,
        RDF_CLASS_CUSTOM_ENTITY_TYPE,
        RDF_CLASS_PROJECT_TYPE,
      ],
      true
    );
    if (registry.getApp() === 'registry')
      query.graphType(types.GT_PIPELINERESULT, true);
    query.context(['_principals'], true);
    if (isGuestUser) query.publicRead(true);

    return query;
  }, [isGuestUser]);
  const {
    entries,
    callbackResults: items = [],
    status,
    ...queryResults
  } = useSolrQuery({
    createQuery,
    applyFilters,
    wait: isLoading,
    callback: getEntityTypes,
  });

  const getTitleProps = ({ entry }) => {
    const label = getFullLengthLabel(entry);
    const contextLabel = getFullLengthLabel(entry.getContext().getEntry(true));
    return {
      entry,
      translate,
      primary: label,
      ...(label !== contextLabel ? { secondary: contextLabel } : {}),
    };
  };

  const getListItemProps = ({ entry }) => {
    const { entityType } = items.find((item) => item.entry === entry);
    if (!entityType) return;

    const overviewName = getOverviewName(entityType, entry.getContext());
    const canAccessOverview = getCanAccessOverview(entry, entityType);

    return canAccessOverview
      ? {
          to: async () => getTo(entityType, overviewName, entry),
        }
      : {
          tooltip: overviewName ? translate('noAccessTooltip') : null,
        };
  };

  const getTypeProps = ({ entry }) => {
    const { entityType } = items.find((item) => item.entry === entry);
    if (!entityType) return {};

    const refinedTypeName = entityType.get('refines');
    if (!refinedTypeName)
      return { secondary: localize(entityType.get('label')) };

    const refinedType = Lookup.getByName(refinedTypeName);
    return { secondary: refinedType ? localize(refinedType.get('label')) : '' };
  };

  return (
    <EntryListView
      {...queryResults}
      entries={items.map(({ entry }) => entry)}
      status={status}
      nlsBundles={nlsBundles}
      includeFilters={hasFilters}
      filtersProps={hasFilters ? filterProps : null}
      getListItemProps={getListItemProps}
      columns={[
        {
          ...TITLE_COLUMN,
          xs: 7,
          getProps: getTitleProps,
        },
        {
          id: 'entityType',
          xs: 2,
          headerNlsKey: 'listHeaderEntityType',
          Component: ListItemText,
          getProps: getTypeProps,
        },
        MODIFIED_COLUMN,
        {
          ...INFO_COLUMN,
          getProps: ({ entry, translate: t }) => ({
            action: {
              ...LIST_ACTION_INFO,
              entry,
              nlsBundles,
              Dialog: LinkedDataBrowserDialog,
            },
            title: t('infoEntry'),
          }),
        },
        {
          id: 'overviewLinkIcon',
          Component: OverviewLinkIcon,
          getProps: getListItemProps,
        },
      ]}
    />
  );
};

export default withListModelProviderAndLocation(SearchView, () => ({
  showFilters: true,
}));
