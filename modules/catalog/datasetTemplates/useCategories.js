import { useState, useEffect } from 'react';
import { getThemeTaxonomy } from 'commons/util/metadata';
import { CHANGE_CATEGORY } from 'catalog/datasetTemplates/reducer';
import useAsync from 'commons/hooks/useAsync';
import { fetchCategories } from 'catalog/datasetTemplates/util';

const useCategories = (
  datasetTemplateEntrystore,
  selectedCatalogEntry,
  filtersDispatch
) => {
  const [categories, setCategories] = useState([]);
  const { data: categoriesData, runAsync, status } = useAsync();

  useEffect(() => {
    if (!selectedCatalogEntry) return;

    const taxonomyURI = getThemeTaxonomy(selectedCatalogEntry);
    filtersDispatch({ type: CHANGE_CATEGORY, value: '' });

    runAsync(
      fetchCategories(
        datasetTemplateEntrystore,
        taxonomyURI,
        selectedCatalogEntry
      )
    );
  }, [
    datasetTemplateEntrystore,
    filtersDispatch,
    runAsync,
    selectedCatalogEntry,
  ]);

  useEffect(() => {
    if (!categoriesData || status !== 'resolved') return;
    setCategories(categoriesData);
  }, [categoriesData, status]);

  return [categories, setCategories];
};

export default useCategories;
