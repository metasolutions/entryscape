import { useState, useMemo, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Entry } from '@entryscape/entrystore-js';
import { entrystore } from 'commons/store';
import {
  ListActions,
  ListItemOpenMenuButton,
  List,
  ListItem,
  ListItemIcon,
  ActionsMenu,
  ListItemText,
  ListHeader,
  ListHeaderItemSort,
  ListSearch,
  ListView,
  ListPagination,
  withListModelProvider,
  useListQuery,
  useListModel,
  ListItemActionsGroup,
} from 'commons/components/ListView';
import { getIconFromActionId } from 'commons/actions';
import {
  LIST_ACTION_REMOVE,
  LIST_ACTION_INFO,
} from 'commons/components/EntryListView/actions';
import { useTranslation } from 'commons/hooks/useTranslation';
import esteCollectionNLS from 'terms/nls/esteCollection.nls';
import escoListNLS from 'commons/nls/escoList.nls';
import Tooltip from 'commons/components/common/Tooltip';
import { IconButton } from '@mui/material';
import { Edit as EditIcon, Info as InfoIcon } from '@mui/icons-material';
import { OverviewListHeader } from 'commons/components/overview/OverviewListHeader';
import { getTitle, getShortModifiedDate } from 'commons/util/metadata';
import { OverviewListPlaceholder } from 'commons/components/overview';
import { getConceptPath } from 'commons/tree/skos/util';
import { TreePath } from 'commons/components/rdforms/SkosChooser/EntrySelectListItem';
import useAsync, {
  asyncStatusPropType,
  getAsyncStatus,
  RESOLVED,
} from 'commons/hooks/useAsync';
import LinkedDataBrowserDialog from 'commons/components/LinkedDataBrowserDialog';
import RemoveCollectionConceptDialog from '../../dialogs/RemoveCollectionConceptDialog';
import EditCollectionConceptsDialog from '../../dialogs/EditCollectionConceptsDialog';
import OtherStoreIcon from '../OtherStoreIcon';
import './index.scss';

const nlsBundles = [escoListNLS, esteCollectionNLS];
const SEARCH_FIELDS = ['label'];

const InfoButton = ({ label, onClick }) => {
  return (
    <Tooltip title={label}>
      <IconButton aria-label={label} onClick={onClick}>
        <InfoIcon />
      </IconButton>
    </Tooltip>
  );
};

InfoButton.propTypes = {
  label: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
};

const getConceptItems = (conceptEntries) => {
  const conceptItemPromises = conceptEntries.map(async (conceptEntry) => {
    const path = await getConceptPath(conceptEntry);
    return {
      concept: conceptEntry,
      path,
      store: conceptEntry.getEntryStore(),
    };
  });

  return Promise.all(conceptItemPromises);
};

const CollectionConceptsList = ({
  collectionConcepts,
  collectionEntry,
  onConceptsChange,
  conceptsStatus,
}) => {
  const t = useTranslation(nlsBundles);
  const [infoDialogEntry, setInfoDialogEntry] = useState(null);
  const [{ search }] = useListModel();

  const {
    runAsync,
    data: conceptItems,
    status: conceptItemsStatus,
  } = useAsync();

  useEffect(() => {
    if (conceptsStatus !== RESOLVED) return;

    runAsync(getConceptItems(collectionConcepts));
  }, [collectionConcepts, conceptsStatus, runAsync]);

  const listQueryItems = useMemo(() => {
    return conceptItemsStatus === RESOLVED
      ? conceptItems.map(({ concept, ...rest }) => ({
          entry: concept,
          label: getTitle(concept),
          modified: getShortModifiedDate(concept),
          ...rest,
        }))
      : null;
  }, [conceptItems, conceptItemsStatus]);

  const { result: listItems, size: listItemsSize } = useListQuery({
    items: listQueryItems,
    searchFields: SEARCH_FIELDS,
  });

  const status = getAsyncStatus([conceptsStatus, conceptItemsStatus]);

  return (
    <>
      <OverviewListHeader
        heading={t('conceptsListHeader')}
        action={{
          Dialog: EditCollectionConceptsDialog,
          tooltip: t('editConceptsButtonTooltip'),
        }}
        actionParams={[collectionEntry, collectionConcepts, onConceptsChange]}
        actionIcon={<EditIcon />}
      />
      <ListView
        search={search}
        size={listItemsSize}
        nlsBundles={nlsBundles}
        renderPlaceholder={() => (
          <OverviewListPlaceholder label={t('conceptsListPlaceholder')} />
        )}
        status={status}
      >
        <ListActions>
          <ListSearch />
        </ListActions>
        <ListHeader>
          <ListHeaderItemSort
            xs={8}
            sortBy="label"
            text={t('conceptsListHeaderLabel')}
          />
          <ListHeaderItemSort
            sortBy="modified"
            text={t('conceptsListHeaderModifiedDate')}
            xs={3}
          />
        </ListHeader>
        <List
          renderPlaceholder={(Placeholder) => (
            <Placeholder
              className="esteCollectionConceptsListPlaceholder"
              search={search}
            />
          )}
          renderLoader={(Loader) => <Loader height="10vh" />}
        >
          {listItems.length
            ? listItems.map(({ entry, label, modified, path, store }) => {
                return (
                  <ListItem
                    key={`${entry.getId()}-${entry.getContext().getId()}`}
                  >
                    <ListItemText
                      xs={7}
                      direction="column"
                      classes={{
                        primary: 'esteCollectionConceptsListItemText__primary',
                      }}
                      primary={label}
                      secondary={
                        <TreePath conceptEntry={entry} entries={path} />
                      }
                    />
                    <ListItemIcon
                      xs={1}
                      icon={store !== entrystore ? <OtherStoreIcon /> : null}
                    />
                    <ListItemText secondary={modified} xs={2} />
                    <ListItemActionsGroup
                      xs={1}
                      actions={[
                        {
                          ...LIST_ACTION_INFO,
                          Dialog: LinkedDataBrowserDialog,
                          getProps: () => ({
                            entry,
                            icon: <InfoIcon />,
                            dialogTitle: t('conceptInfoTooltip'),
                            onClick: () => setInfoDialogEntry(entry),
                          }),
                        },
                      ]}
                    />
                    <ListItemOpenMenuButton />
                    <ActionsMenu
                      items={[
                        {
                          action: {
                            ...LIST_ACTION_REMOVE,
                            Dialog: RemoveCollectionConceptDialog,
                            collectionEntry,
                            callback: onConceptsChange,
                          },
                          icon: getIconFromActionId('remove'),
                          label: t('collectionRemove'),
                        },
                      ]}
                    />
                  </ListItem>
                );
              })
            : null}
        </List>
        <ListPagination />
      </ListView>
      {infoDialogEntry && (
        <LinkedDataBrowserDialog
          entry={infoDialogEntry}
          closeDialog={() => setInfoDialogEntry(null)}
        />
      )}
    </>
  );
};

CollectionConceptsList.propTypes = {
  collectionConcepts: PropTypes.arrayOf(PropTypes.instanceOf(Entry)),
  collectionEntry: PropTypes.instanceOf(Entry),
  onConceptsChange: PropTypes.func,
  conceptsStatus: asyncStatusPropType,
};

export default withListModelProvider(CollectionConceptsList, () => ({
  limit: 5,
}));
