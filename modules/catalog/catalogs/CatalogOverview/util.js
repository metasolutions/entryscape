import { Context } from '@entryscape/entrystore-js';
import statsAPI from 'commons/statistics/api';
import {
  RESOURCE_TYPE_ALL,
  RESOURCE_TYPE_API,
  RESOURCE_TYPE_FILE,
} from 'catalog/statistics/utils/distribution';

/**
 * Get top statistics for a specific time. Used here only with specific dates
 *
 * @param {Context} context
 * @param {object} timeRangeDay
 * @returns {Promise<*>}
 */
const getCatalogStatistics = (context, timeRangeDay) =>
  statsAPI.getTopStatistics(context.getId(), RESOURCE_TYPE_ALL, timeRangeDay);

/**
 * Sum downloads of api/file or both
 *
 * @param {object[]} results
 * @param {null|String} type
 * @returns {number}
 */
const sumTotalCountFromResult = (results, type = null) =>
  results.reduce((totalCount, res) => {
    if (type === null || type === res.type) {
      return totalCount + res.count;
    }

    return totalCount;
  }, 0);

/**
 * Get statistics for catalog in the last 7 days
 *
 * @param {Context} context
 * @param {Function} translate
 * @param {Function} callback
 * @returns {Promise<{bar: {datasets: Array}, doughnut: {datasets: {data: number[], label: *}[], labels: *[]}}>}
 */
export const getStatisticsData = async (context, translate, callback) => {
  // prepare data structures and api calls
  const barData = { datasets: [] };
  const today = new Date();
  const timeRanges = [];
  for (let i = 0; i < 7; i++) {
    const date = new Date();
    date.setDate(today.getDate() - i);
    timeRanges.push({
      year: date.getFullYear(),
      month: date.getMonth(), // month is zero indexed
      date: date.getDate(),
    });
  }

  // make api call and calculate results
  const dataPointsFiles = [];
  const dataPointsAPI = [];
  let fileTotalCount = 0;
  let apiTotalCount = 0;
  const results = await Promise.all(
    timeRanges.map((timeRange) => getCatalogStatistics(context, timeRange))
  );
  results.forEach((result, idx) => {
    const fileCount = sumTotalCountFromResult(result, RESOURCE_TYPE_FILE);
    const apiCount = sumTotalCountFromResult(result, RESOURCE_TYPE_API);
    const timeRange = timeRanges[idx];
    dataPointsFiles.push({
      x: new Date(timeRange.year, timeRange.month, timeRange.date),
      y: fileCount,
    });
    dataPointsAPI.push({
      x: new Date(timeRange.year, timeRange.month, timeRange.date),
      y: apiCount,
    });

    fileTotalCount += fileCount;
    apiTotalCount += apiCount;
  });

  // populate the data structures for the charts
  if (fileTotalCount || apiTotalCount) {
    // at least some data, not all 0
    barData.datasets.push({
      data: dataPointsFiles,
      label: translate('statsCatalogOverviewChartFileLabel'),
    });
    barData.datasets.push({
      data: dataPointsAPI,
      label: translate('statsCatalogOverviewChartAPILabel'),
    });
  }

  callback();

  return { bar: barData, fileTotalCount, apiTotalCount };
};
