import React, { useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import { InputBase, InputAdornment, IconButton } from '@mui/material';
import { Search as SearchIcon, Clear as ClearIcon } from '@mui/icons-material';
import MenuItem from '@mui/material/MenuItem';
import InputLabel from '@mui/material/InputLabel';
import FormControl from '@mui/material/FormControl';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormGroup from '@mui/material/FormGroup';
import Select from '@mui/material/Select';
import Checkbox from '@mui/material/Checkbox';
import CircularProgress from '@mui/material/CircularProgress';
import Tooltip from 'commons/components/common/Tooltip';
import useAsync from 'commons/hooks/useAsync';
import Placeholder from 'commons/components/common/Placeholder';
import { useTranslation } from 'commons/hooks/useTranslation';
import escaStatistics from 'catalog/nls/escaStatistics.nls';
import escoChart from 'commons/nls/escoChart.nls';
import TimeChart from 'commons/components/chart/Time';
import { COLOURS_BAR_CHART } from 'commons/components/chart/colors';
import { useESContext } from 'commons/hooks/useESContext';
import { getStatisticsGraphData } from './utils/data';
import timeRangeUtil from './utils/timeRange';
import './index.scss';

export const TimeRangeControl = ({
  timeRangeValue,
  updateTimeRange,
  className = '',
}) => {
  const translate = useTranslation(escaStatistics);
  const timeRanges = timeRangeUtil.getTimeRanges();
  return (
    <FormControl
      classes={{ root: `escaStatisticsFilterControl__timeRange ${className}` }}
      variant="filled"
    >
      <InputLabel id="time-range-select-label">
        {translate('statsDialogTimeRange')}
      </InputLabel>
      <Select
        inputProps={{ 'aria-labelledby': 'time-range-select-label' }}
        onChange={({ target }) => updateTimeRange(target.value)}
        value={timeRangeValue}
        variant="filled"
        margin="none"
      >
        {timeRanges.map((timeRange) => (
          <MenuItem key={timeRange.value} value={timeRange.value}>
            {translate(timeRange.label)}
          </MenuItem>
        ))}
      </Select>
    </FormControl>
  );
};

TimeRangeControl.propTypes = {
  timeRangeValue: PropTypes.string,
  updateTimeRange: PropTypes.func,
  className: PropTypes.string,
};

export const ChartFilterControls = ({ filter, updateFilter }) => {
  const t = useTranslation(escaStatistics);

  return (
    <div className="escaStatisticsFilterControls">
      <div className="escaStatisticsFilterControl__search">
        <InputBase
          value={filter.searchQuery}
          placeholder={t('searchPlaceholder')}
          classes={{ root: 'escaStatisticsSearch_root' }}
          onChange={(e) => updateFilter('searchQuery', e.target.value)}
          fullWidth
          inputProps={{ 'aria-label': 'Search' }}
          startAdornment={
            <InputAdornment
              position="start"
              classes={{ root: 'escaStatisticsSearch_adornment' }}
            >
              <SearchIcon />
            </InputAdornment>
          }
          endAdornment={
            filter.searchQuery ? (
              <InputAdornment position="end">
                <Tooltip title={t('searchClearLabel')}>
                  <IconButton
                    aria-label={t('searchClearLabel')}
                    onClick={() => updateFilter('searchQuery', '')}
                    onMouseDown={(e) => e.preventDefault()}
                    color="secondary"
                  >
                    <ClearIcon />
                  </IconButton>
                </Tooltip>
              </InputAdornment>
            ) : null
          }
        />
      </div>
      <div className="escaStatisticsFilterControl__moreFilters">
        <TimeRangeControl
          timeRangeValue={filter.timeRange}
          updateTimeRange={(timeRangeValue) =>
            updateFilter('timeRange', timeRangeValue)
          }
          className="escaStatisticsFilterControl__timeRange--margin-left"
        />
        <FormControl
          classes={{ root: 'escaStatisticsFilterControl__resourceType' }}
          margin="none"
        >
          <FormGroup classes={{ root: 'escaStatisticsResourceType__item' }}>
            <FormControlLabel
              control={
                <Checkbox
                  checked={filter.file}
                  onChange={() => {
                    if (filter.api || filter.documentFile) {
                      updateFilter('file', !filter.file);
                    }
                  }}
                  name="File"
                />
              }
              label={t('tabItemFiles')}
            />
            <FormControlLabel
              control={
                <Checkbox
                  checked={filter.documentFile}
                  onChange={() => {
                    if (filter.file || filter.api) {
                      updateFilter('documentFile', !filter.documentFile);
                    }
                  }}
                  name="Documents"
                />
              }
              label={t('tabItemDocuments')}
            />
            <FormControlLabel
              control={
                <Checkbox
                  checked={filter.api}
                  onChange={() => {
                    if (filter.file || filter.documentFile) {
                      updateFilter('api', !filter.api);
                    }
                  }}
                  name="API"
                />
              }
              label={t('tabItemApiCalls')}
            />
          </FormGroup>
        </FormControl>
      </div>
    </div>
  );
};

ChartFilterControls.propTypes = {
  filter: PropTypes.shape({
    searchQuery: PropTypes.string,
    timeRange: PropTypes.string,
    file: PropTypes.bool,
    documentFile: PropTypes.bool,
    api: PropTypes.bool,
  }),
  updateFilter: PropTypes.func,
};

const Chart = ({ chartData }) => {
  const translate = useTranslation([escoChart, escaStatistics]);
  const dataExists = chartData?.datasets?.length;

  return (
    <>
      {dataExists ? (
        <TimeChart
          data={chartData}
          colors={COLOURS_BAR_CHART}
          dimensions={{ height: '300px' }}
          options={{
            scales: {
              yAxes: [
                {
                  scaleLabel: {
                    display: true,
                    labelString: translate('statsChartAxisY'),
                  },
                },
              ],
            },
          }}
        />
      ) : (
        <Placeholder label={translate('noDataMessage')} />
      )}
    </>
  );
};

Chart.propTypes = {
  chartData: PropTypes.objectOf(PropTypes.arrayOf(PropTypes.shape({})))
    .isRequired,
};

const StatisticsChart = ({ statisticsData, timeRange }) => {
  const { context } = useESContext();
  const { data: chartData, status, runAsync } = useAsync(null);
  const previousStatisticsData = useRef(statisticsData);
  /**
   * New graphData should only be requested when new statisticsData.
   * Timerange is only used as an render option for the chart and
   * should not trigger a new request.
   */
  useEffect(() => {
    if (statisticsData === previousStatisticsData.current) return;
    previousStatisticsData.current = statisticsData;
    runAsync(getStatisticsGraphData(statisticsData, context, timeRange));
  }, [runAsync, statisticsData, context, timeRange]);

  return (
    <>
      {status === 'resolved' ? (
        <Chart chartData={chartData} />
      ) : (
        <div className="escaStatistics__loading">
          <CircularProgress />
        </div>
      )}
    </>
  );
};

StatisticsChart.propTypes = {
  statisticsData: PropTypes.arrayOf(PropTypes.shape({})),
  timeRange: PropTypes.string,
};

export default StatisticsChart;
