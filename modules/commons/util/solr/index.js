import config from 'config';
import { i18n } from 'esi18n';
import { SolrQuery } from '@entryscape/entrystore-js';
import { namespaces } from '@entryscape/rdfjson';
import { entrystore } from 'commons/store';
import { USER_ENTRY_ID_ADMIN, USER_ENTRY_ID_GUEST } from 'commons/util/userIds';

const SOLR_STRINGS_TO_ESCAPE = ['AND', 'OR'];

/**
 *
 * @param {string} search
 * @returns {string}
 */
export const escapeSolrStrings = (search) => {
  const regex = new RegExp(SOLR_STRINGS_TO_ESCAPE.join('|'), 'g');
  return search.replace(regex, (match) => `\\${match}`);
};

export const createSortString = ({ field, order, locale } = {}) => {
  if (!(field && order)) return;

  const sortingLocale = locale || i18n.getLocale();
  return field === 'title'
    ? `${field}.${sortingLocale}+${order}`
    : `${field}+${order}`;
};

export const applyQueryParams = (solrQuery, { search, sort, limit }) => {
  const sortString = createSortString(sort);
  const searchString = escapeSolrStrings(search);
  const queries = Array.isArray(solrQuery) ? solrQuery : [solrQuery];

  queries.forEach((query) => {
    if (searchString) {
      const fields =
        config.get('entrystore.defaultSolrQuery') === 'all' ? 'all' : 'title';
      query[fields](searchString);
    }
    if (sortString) {
      query.sort(sortString);
    }
    query.limit(limit);
  });

  return solrQuery;
};

/**
 * @param {object} params
 * @param {SolrQuery | SolrQuery[]} params.query - Array in case of MultiList
 * @param {string} params.search
 * @param {string} params.sort
 * @param {number} params.limit
 */
export const defaultQueryParams = ({ query, search, sort, limit = 10 }) => {
  const queryIsArray = Array.isArray(query);

  if (search) {
    if (config.get('entrystore.defaultSolrQuery') === 'all') {
      queryIsArray ? query.map((q) => q.all(search)) : query.all(search);
    } else {
      queryIsArray ? query.map((q) => q.title(search)) : query.title(search);
    }
  }

  if (sort) {
    queryIsArray ? query.map((q) => q.sort(sort)) : query.sort(sort);
  }

  // TODO set limit
  queryIsArray ? query.map((q) => q.limit(limit)) : query.limit(limit);
};

/**
 *
 * @param {object} query
 * @returns {object}
 */
export const excludeGuestAndAdmin = (query) => {
  query.uri(
    [USER_ENTRY_ID_ADMIN, USER_ENTRY_ID_GUEST].map((entryId) =>
      entrystore.getEntryURI('_principals', entryId)
    ),
    true
  );
  return query;
};

/**
 *
 * @param {string} name
 * @param {SolrQuery} query
 * @param {{search: string, sort: string, limit: number, name: string}} queryParams
 * @returns {SolrQuery}
 */
const applyTitleOrNameParams = (name, query, { search, sort, limit = 10 }) => {
  const sortString = createSortString(sort);
  const searchString = escapeSolrStrings(search);

  if (search) {
    query.or({
      title: searchString,
      [name]: `*${searchString}*`,
    });
  }

  if (sort) query.sort(sortString);

  query.limit(limit);

  return query;
};

/**
 *
 * @param {SolrQuery} query
 * @param {{search: string, sort: string, limit: number }} queryParams
 * @returns {SolrQuery}
 */
export const applyTitleOrContextnameParams = (query, queryParams) =>
  applyTitleOrNameParams('contextname', query, queryParams);

/**
 * Applies params to the solr query in a way that allows both title and
 * username matches
 *
 * @param {SolrQuery} query
 * @param {{search: string, sort: string, limit: number}} queryParams
 * @returns {SolrQuery}
 */
export const applyTitleOrUsernameParams = (query, queryParams) =>
  applyTitleOrNameParams('username', query, queryParams);

/**
 *
 * @param {object} query
 * @param {{search: string, sort: object, limit: number}} queryOptions
 * @param {object} entityTypeConfig
 * @returns {object}
 */
export const applyEntityTypeParams = (
  query,
  { search, sort, limit },
  entityTypeConfig
) => {
  const { rdfType, constraints, searchProps } = entityTypeConfig;
  if (rdfType) {
    query.rdfType(rdfType);
  }
  if (constraints) {
    const rdftype = namespaces.expand('rdf:type');
    Object.keys(constraints || {}).forEach((key) => {
      if (key !== rdftype) {
        if (key[0] === '!') {
          query.uriProperty(key.substr(1), constraints[key], 'not');
        } else {
          query.uriProperty(key, constraints[key]);
        }
      } else if (!rdfType) {
        query.rdfType(constraints[key]);
      }
    });
  }

  if (search) {
    if (searchProps) {
      let props = searchProps;
      if (typeof props === 'string') {
        props = [props];
      }
      props.forEach((prop) => {
        query.literalProperty(prop, `*${search}*`);
      });
      query.disjunctiveProperties();
    } else if (config.get('entrystore.defaultSolrQuery') === 'all') {
      query.all(search);
    } else {
      query.title(search);
    }
  }

  if (sort) {
    query.sort(createSortString(sort));
  }

  if (limit) {
    query.limit(limit);
  }

  return query;
};
