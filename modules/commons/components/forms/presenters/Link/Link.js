import PropTypes from 'prop-types';
import { graphPropType } from 'commons/util/entry';
import { Link as MuiLink } from '@mui/material';
import RouterLink from 'commons/components/router/RouterLink';
import { Fields } from '../Fields';
import { Field } from '../Field';

export const Link = ({
  graph,
  resourceURI,
  property,
  inline,
  to,
  ...fieldsProps
}) => {
  return (
    <Fields inline={inline} {...fieldsProps}>
      <Field inline={inline}>
        <MuiLink component={RouterLink} to={to}>
          {graph.findFirstValue(resourceURI, property)}
        </MuiLink>
      </Field>
    </Fields>
  );
};

Link.propTypes = {
  graph: graphPropType,
  resourceURI: PropTypes.string,
  property: PropTypes.string,
  inline: PropTypes.bool,
  to: PropTypes.string,
};
