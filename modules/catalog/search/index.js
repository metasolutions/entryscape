import { useCallback } from 'react';
import { useParams } from 'react-router-dom';
import { Typography, Grid } from '@mui/material';
import EmptyListIcon from '@mui/icons-material/List';
import CatalogPresenter from 'catalog/search/CatalogPresenter';
import CatalogsList from 'catalog/search/list/catalogs';
import { useTranslation } from 'commons/hooks/useTranslation';
import escaCatalogSearchNLS from 'catalog/nls/escaCatalogSearch.nls';
import escaCatalogNLS from 'catalog/nls/escaCatalog.nls';
import { RDF_TYPE_CATALOG, RDF_PROPERTY_DATASET } from 'commons/util/entry';
import { entrystore } from 'commons/store';
import Placeholder from 'commons/components/common/Placeholder';
import { fetchSortedCatalogs } from 'catalog/search/list/catalogs/data';
import { useSolrQuery } from 'commons/components/EntryListView';
import { withListModelProvider } from 'commons/components/ListView';
import { RESOLVED } from 'commons/hooks/useAsync';
import config from 'config';
import './index.scss';

const SearchView = () => {
  const { contextId } = useParams();
  const translate = useTranslation([escaCatalogSearchNLS, escaCatalogNLS]);

  const createQuery = useCallback(() => {
    const query = entrystore.newSolrQuery().rdfType(RDF_TYPE_CATALOG);
    if (!config.get('catalog.excludeEmptyCatalogsInSearch')) return query;

    query.uriProperty(RDF_PROPERTY_DATASET, '*');
    return query;
  }, []);

  const { entries: catalogEntries, status } = useSolrQuery({
    createQuery,
    runQuery: fetchSortedCatalogs,
    limit: 100,
  });

  if (status === RESOLVED && !catalogEntries.length && !contextId) {
    return (
      <Placeholder
        label={translate('emptyCatalogListWarning')}
        icon={<EmptyListIcon sx={{ fontSize: '60px' }} />}
      />
    );
  }

  const uniqueIds = [];
  const uniqueCatalogEntries = catalogEntries.filter((entry) => {
    const id = `${entry.getContext().getId()}-${entry.getId()}`;
    if (!uniqueIds.includes(id)) {
      uniqueIds.push(id);
      return true;
    }
    return false;
  });

  // TODO perhaps render the single catalog view if there's only one catalog in the installation
  return (
    <Grid container direction="row" className="escoTwoColView">
      <Grid container direction="column" item className="escaSearchSidebar">
        <Grid item className="escaSearchSidebar__header">
          <Typography
            variant="h2"
            id={translate('listNavAriaLabel')}
            className="escaSearchSidebar__heading"
          >
            {translate('listNavAriaLabel')}
          </Typography>
        </Grid>
        <Grid item className="escaSearchSidebar__list">
          <CatalogsList catalogs={uniqueCatalogEntries} />
        </Grid>
      </Grid>
      <Grid item className="escoTwoColView__Main">
        <CatalogPresenter context={contextId} />
      </Grid>
    </Grid>
  );
};

export default withListModelProvider(SearchView);
