import { useCallback, useState } from 'react';
import PropTypes from 'prop-types';
import ErrorBoundary from 'commons/errors/ErrorBoundary';
import SnackbarFallbackComponent from 'commons/errors/SnackbarFallbackComponent';
import { ListActionButton } from './ListActionButton';

// Hook to handle dialog open state.
export const useActionDialog = () => {
  const [isActionDialogOpen, setIsActionDialogOpen] = useState(false);

  const openActionDialog = useCallback(() => {
    setIsActionDialogOpen(true);
  }, []);

  const closeActionDialog = useCallback(() => {
    setIsActionDialogOpen(false);
  }, []);

  return {
    isActionDialogOpen,
    openActionDialog,
    closeActionDialog,
  };
};

const ActionDialog = ({ Dialog, closeDialog, ...actionProps }) => {
  return (
    <ErrorBoundary
      FallbackComponent={SnackbarFallbackComponent}
      onError={closeDialog}
    >
      <Dialog closeDialog={closeDialog} {...actionProps} />
    </ErrorBoundary>
  );
};

ActionDialog.propTypes = {
  Dialog: PropTypes.func.isRequired,
  closeDialog: PropTypes.func.isRequired,
};

// ListAction is the combination of an action button and a corresponding dialog
// component, defined by action.
export const ListAction = ({
  disabled,
  isVisible = true,
  label,
  tooltip,
  icon,
  action,
}) => {
  const { isActionDialogOpen, openActionDialog, closeActionDialog } =
    useActionDialog();

  return (
    <>
      {isVisible ? (
        <ListActionButton
          disabled={disabled}
          label={label}
          tooltip={tooltip}
          icon={icon}
          onClick={openActionDialog}
          highlightId={action.highlightId}
        />
      ) : null}
      {isActionDialogOpen && action
        ? ActionDialog({
            closeDialog: closeActionDialog,
            ...action,
          })
        : null}
    </>
  );
};

ListAction.propTypes = {
  disabled: PropTypes.bool,
  icon: PropTypes.node,
  isVisible: PropTypes.bool,
  tooltip: PropTypes.string,
  label: PropTypes.string,
  action: PropTypes.shape({ highlightId: PropTypes.string }),
};
