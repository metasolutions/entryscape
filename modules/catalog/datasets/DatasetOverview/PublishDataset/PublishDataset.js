import PropTypes from 'prop-types';
import OverviewSidebarToggle from 'commons/components/overview/OverviewSidebarToggle';
import {
  Visibility,
  Public,
  PublicOff,
  VpnLock as InternalPublic,
} from '@mui/icons-material';
import { entryPropType } from 'commons/util/entry';
import useGetMainDialog from 'commons/hooks/useGetMainDialog';
import { renderHtmlString } from 'commons/util/reactUtils';
import useDatasetPublish from '../hooks/useDatasetPublish';
import useDatasetInternalPublish from '../hooks/useDatsetInternalPublish';
import useIsPSI from '../hooks/useIsPSI';

export const PublishDatasetInternal = ({
  entry,
  translate,
  datasetStatus,
  onChange,
}) => {
  const { toggleIsInternallyPublished, isLoading } = useDatasetInternalPublish({
    entry,
    datasetStatus,
    onChange,
  });
  const { isInternallyPublished, isPublished, isDraft } = datasetStatus;

  const draftLabel = isDraft ? translate('draftDisabledDatasetTitle') : null;
  const publishLabel = isPublished
    ? translate('internalPublishDisabled')
    : translate('internalPublishTitle');
  const switchTooltipLabel = draftLabel || publishLabel;

  return (
    <OverviewSidebarToggle
      onChange={toggleIsInternallyPublished}
      isChecked={isInternallyPublished || isPublished}
      disabled={isDraft || isLoading || isPublished}
      primaryLabel={translate('internalPublishStatus')}
      secondaryLabel={
        isInternallyPublished || isPublished
          ? translate('publishedStatus')
          : translate('unpublishedStatus')
      }
      icon={
        isInternallyPublished || isPublished ? (
          <InternalPublic />
        ) : (
          <PublicOff />
        )
      }
      highlightId="overviewInternalPublish"
      switchTooltipLabel={switchTooltipLabel}
    />
  );
};

PublishDatasetInternal.propTypes = {
  entry: entryPropType,
  translate: PropTypes.func,
  onChange: PropTypes.func,
  datasetStatus: PropTypes.shape({
    isDraft: PropTypes.bool,
    isSeries: PropTypes.bool,
    isPublished: PropTypes.bool,
    isInternallyPublished: PropTypes.bool,
  }),
};

export const PublishDataset = ({
  entry,
  catalogEntry,
  translate,
  datasetStatus,
  includeInternalPublish,
  onChange,
}) => {
  const { toggleIsPublished, isLoading } = useDatasetPublish({
    entry,
    datasetStatus,
    onChange,
  });
  const { isPublished, isDraft } = datasetStatus;

  const draftLabel = isDraft ? translate('draftDisabledDatasetTitle') : null;
  const isCatalogPublic = catalogEntry?.isPublic();
  const catalogPublicLabel = !isCatalogPublic
    ? translate('privateDisabledDatasetTitle')
    : null;
  const switchTooltipLabel = draftLabel || catalogPublicLabel;

  return (
    <OverviewSidebarToggle
      onChange={toggleIsPublished}
      isChecked={isPublished}
      disabled={isDraft || !catalogEntry || !isCatalogPublic || isLoading}
      primaryLabel={
        includeInternalPublish
          ? translate('externalPublishStatus')
          : translate('publishedTitle')
      }
      secondaryLabel={
        isPublished
          ? translate('publishedStatus')
          : translate('unpublishedStatus')
      }
      icon={isPublished ? <Public /> : <PublicOff />}
      highlightId="overviewPublish"
      switchTooltipLabel={switchTooltipLabel}
    />
  );
};

PublishDataset.propTypes = {
  entry: entryPropType,
  catalogEntry: entryPropType,
  translate: PropTypes.func,
  onChange: PropTypes.func,
  includeInternalPublish: PropTypes.bool,
  datasetStatus: PropTypes.shape({
    isDraft: PropTypes.bool,
    isSeries: PropTypes.bool,
    isPublished: PropTypes.bool,
  }),
};

export const DatasetPSISwitch = ({ entry, translate }) => {
  const { getConfirmationDialog } = useGetMainDialog();
  const [isPSI, toggleIsPSI] = useIsPSI(entry);

  const handlePSIChange = async () => {
    const proceed = await getConfirmationDialog({
      content: renderHtmlString(translate('togglePsiPrompt')),
      rejectLabel: translate('togglePsiReject'),
      affirmLabel: translate('togglePsiAffirm'),
    });
    if (!proceed) return;
    toggleIsPSI();
  };

  return (
    <OverviewSidebarToggle
      onChange={handlePSIChange}
      isChecked={isPSI}
      primaryLabel={translate('psiDatasetLabel')}
      secondaryLabel={
        isPSI ? translate('activeTitle') : translate('inactiveTitle')
      }
      listItemTextTooltipLabel={translate('psiDatasetTooltip')}
      icon={<Visibility />}
    />
  );
};

DatasetPSISwitch.propTypes = {
  entry: entryPropType,
  translate: PropTypes.func,
};
