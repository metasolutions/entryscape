import { withListRefresh } from 'commons/components/ListView';
import escoListNLS from 'commons/nls/escoList.nls';
import escaResultsNLS from 'catalog/nls/escaResults.nls';
import CreateShowcaseDialog from '../dialogs/CreateShowcaseDialog';

const nlsBundles = [escaResultsNLS, escoListNLS];

const listActions = [
  {
    id: 'create',
    Dialog: withListRefresh(CreateShowcaseDialog, 'onCreate'),
    labelNlsKey: 'createButtonLabel',
    tooltipNlsKey: 'actionTooltip',
    highlightId: 'listActionCreate',
  },
];

export { listActions, nlsBundles };
