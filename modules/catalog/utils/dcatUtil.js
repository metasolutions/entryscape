import registry from 'commons/registry';
import { getLabel } from 'commons/util/rdfUtils';

/**
 * @todo @valentino make generic util
 * @param collection
 * @param value
 * @return {*}
 */
const existsInCollection = (collection, value) => {
  if (Array.isArray(collection)) {
    return collection.includes(value);
  } else if (typeof collection === 'object') {
    return value in collection.keys();
  }

  return collection === value;
};

/**
 * @todo @valentino re-evaluate efficacy
 * @todo re-write as getInversePropertyPath
 *
 *
 * @param resourceURIs
 * @param context
 * @param rdfType
 * @param property
 * @param entriesMap
 * @return {promise}
 */
const requestSolrURIProperty = (resourceURIs, context, rdfType, property, entriesMap) => {
  return es
    .newSolrQuery()
    .context(context)
    .rdfType(rdfType)
    .uriProperty(property, resourceURIs)
    .forEach((entry) => {
      /** @type Array */
      const filteredResultEntriesRURIs = entry
        .getMetadata()
        .objects(entry.getResourceURI(), property)
        .values()
        .filter((uri) => existsInCollection(resourceURIs, uri)); // make sure what was retrieved is relevant

      // make the association between the uri and the entry
      filteredResultEntriesRURIs.forEach((ruri) => entriesMap.set(ruri, entry));
    });
};

/**
 * Split a potentially very long solr request to a shorter one so that apache doesn't return 414.
 * Current max limit is 20 resource uris in the request.
 *
 * @param {String|String[]} resourceURIs
 * @param context
 * @param rdfType
 * @param property
 * @return {Promise<Map<any, any>>}
 */
const requestSolrInChunks = async (resourceURIs, context, rdfType, property) => {
  const entries = new Map();

  const MAX_URIS_IN_OR_QUERY = 20; // avoid too long HTTP requests
  let START_IDX = 0;
  let END_IDX = MAX_URIS_IN_OR_QUERY;
  const queryPromises = [];
  if (Array.isArray(resourceURIs)) {
    let repeat = Math.floor(resourceURIs.length / MAX_URIS_IN_OR_QUERY) + 1;
    while (repeat > 0) {
      // get next chunk
      const ruris = resourceURIs.slice(START_IDX, END_IDX);

      // make a solr request query
      queryPromises.push(requestSolrURIProperty(ruris, context, rdfType, property, entries));

      // update loop
      START_IDX += MAX_URIS_IN_OR_QUERY;
      END_IDX += MAX_URIS_IN_OR_QUERY;
      repeat -= 1;
    }
  } else {
    queryPromises.push(requestSolrURIProperty([resourceURIs], context, rdfType, property, entries));
  }

  await Promise.all(queryPromises);

  return entries;
};

/**
 *
 * @param {String|String[]} resourceURIs
 * @param context
 * @return {Promise<Map<string, store/Entry>>}
 */
const getDatasetByDistributionURI = (resourceURIs, context) =>
  requestSolrInChunks(resourceURIs, context, 'dcat:Dataset', 'dcat:distribution');

/**
 *
 * @param {String|Array} resourceURIs
 * @param context
 * @return {Promise<Map<string, store/Entry>>}
 */
const getDistributionByFileResourceURIs = (resourceURIs, context) =>
  requestSolrInChunks(resourceURIs, context, 'dcat:Distribution', 'dcat:accessURL');

/**
 *
 * @param resourceURIs
 * @param context
 * @return {Promise<Map<any, any>>}
 */
const mapResourceURIsToEntries = async (resourceURIs, context) => {
  const uriToEntryMap = new Map();
  try {
    const entries = await registry.getEntryStoreUtil().loadEntriesByResourceURIs(resourceURIs, context, true);
    resourceURIs.forEach((resourceURI, idx) => {
      if (entries[idx]) {
        uriToEntryMap.set(resourceURI, entries[idx]);
      }
    });
  } catch (err) {
    throw Error(err);
  }
  return uriToEntryMap;
};

/**
 *
 * @param context
 * @return {Promise<Entry>}
 */
const getCatalogEntry = async (context = null) => {
  const inContext = context || registry.getContext();
  try {
    const entries = await registry
      .getEntryStore()
      .newSolrQuery()
      .context(inContext)
      .rdfType('dcat:Catalog')
      .limit(1)
      .getEntries();

    return entries[0]; // by convention we have only one catalog per context
  } catch (err) {
    console.log(err);
    throw Error('No catalog entry found in context ', inContext.getId());
  }
};

/**
 *
 * @param context
 * @return {Promise<string|*>}
 */
const getCatalogLabel = async (context = null) => {
  const catalogEntry = await getCatalogEntry(context);
  return getLabel(catalogEntry);
};

export { mapResourceURIsToEntries, getDatasetByDistributionURI, getDistributionByFileResourceURIs, getCatalogEntry, getCatalogLabel, };

