import PropTypes from 'prop-types';
import { entryPropType } from 'commons/util/entry';
import RemoveEntryDialog from 'commons/components/entry/RemoveEntryDialog';
import { useTranslation } from 'commons/hooks/useTranslation';
import escoListNLS from 'commons/nls/escoList.nls';
import escaDatasetNLS from 'catalog/nls/escaDataset.nls';
import escaVisualizationNLS from 'catalog/nls/escaVisualization.nls';
import escoGeoMapNLS from 'commons/nls/escoGeoMap.nls';
import { removeVisualization } from '../../utils/visualizations';

const nlsBundles = [
  escoListNLS,
  escaDatasetNLS,
  escaVisualizationNLS,
  escoGeoMapNLS,
];

const RemoveVisualizationDialog = ({
  entry: datasetEntry,
  visualizationEntry,
  refreshVisualizations,
  closeDialog,
}) => {
  const translate = useTranslation(nlsBundles);

  const onRemove = () => {
    return removeVisualization(datasetEntry, visualizationEntry);
  };

  return (
    <RemoveEntryDialog
      closeDialog={closeDialog}
      removeConfirmMessage={translate('removalConfirmation')}
      onRemove={onRemove}
      onRemoveCallback={refreshVisualizations}
    />
  );
};
RemoveVisualizationDialog.propTypes = {
  entry: entryPropType,
  visualizationEntry: entryPropType,
  refreshVisualizations: PropTypes.func,
  closeDialog: PropTypes.func.isRequired,
};

export default RemoveVisualizationDialog;
