export default {
  // TODO what do these do? add descriptions to them
  admin: {
    /**
     * @type {boolean}
     */
    showContextName: false,

    /**
     * @type {boolean}
     */
    showGroupName: false,

    /**
     * @type {boolean}
     */
    showContextTypeControl: false,

    /**
     * @type {boolean}
     */
    setGroupNameAsEntryIdOnCreate: false,

    /**
     * @type {boolean}
     */
    setContextNameAsEntryIdOnCreate: false,

    /**
     * @type {boolean}
     */
    advancedAdminVisibleForAdminGroup: true,

    /**
     * Enable the migrate entity types button.
     *
     * @type {boolean}
     */
    enableTypeStoreMigration: false,

    /**
     * Decide whether personal projects should be included when creating a new user.
     *
     * @type {boolean}
     */
    includePersonalProject: false,
  },
};
