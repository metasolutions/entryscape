import PropTypes from 'prop-types';
import { graphPropType } from 'commons/util/entry';
import { useState, useMemo } from 'react';
import { Button, Box, FormHelperText } from '@mui/material';
import { useTranslation } from 'commons/hooks/useTranslation';
import esadEntityTypeSelect from 'admin/nls/esadEntityTypeSelect.nls';
import escoFormsNLS from 'commons/nls/escoForms.nls';
import { Fields, SelectionListItem } from 'commons/components/forms/editors';
import {
  List,
  ListView,
  useListQuery,
  useFetchEntries,
  useListModel,
  withListModelProvider,
} from 'commons/components/ListView';
import { CONFIG_CONTEXT } from 'admin/configs/utils/namespace';
import { nameToURI } from 'commons/types/utils/uri';
import {
  optional,
  defaults,
} from 'admin/configs/projecttypes/utils/fieldDefinitions';
import {
  getEntityTypeEntries,
  getEntityTypesByFilter,
} from 'admin/configs/entitytypes/utils/entityTypeQuery';
import { useFieldsEdit } from 'commons/components/forms/editors/hooks/useFieldsEdit';
import { RESOLVED } from 'commons/hooks/useAsync';
import EntityTypeSelectDialog from '../EntityTypeSelectDialog';

const NLS_BUNDLES = [esadEntityTypeSelect, escoFormsNLS];

const EntityTypeSelectEditor = ({
  name,
  nodetype,
  graph,
  resourceURI,
  property,
  label,
  mandatory,
  dispatch,
}) => {
  const { fields, error, labelId, addField, removeField, updateValue } =
    useFieldsEdit({
      name,
      property,
      nodetype,
      graph,
      resourceURI,
      mandatory,
      dispatch,
      insertEmptyField: false,
    });
  const translate = useTranslation(NLS_BUNDLES);
  const [showEntityTypeSelectDialog, setShowEntityTypeSelectDialog] =
    useState(false);

  const selectedResourceURIs = fields.map((field) => field.getValue());

  const disabledResourceURIs = graph
    .find(
      resourceURI,
      property === optional.property ? defaults.property : optional.property
    )
    .map((statement) => statement.getValue());

  const [{ refreshCount }] = useListModel();
  const { entries: entityTypeEntries, status } = useFetchEntries({
    fetchEntries: getEntityTypeEntries,
    refreshCount,
  });

  const instanceAndBuiltinEntityTypes = useMemo(() => {
    return status === RESOLVED
      ? getEntityTypesByFilter(entityTypeEntries)
      : null;
  }, [entityTypeEntries, status]);

  const { result: entityTypes, size } = useListQuery({
    items: instanceAndBuiltinEntityTypes,
    searchFields: [],
  });

  const selectedEntityTypes = entityTypes.filter((entityType) =>
    selectedResourceURIs.includes(nameToURI(entityType.id, CONFIG_CONTEXT))
  );

  const onSelect = (selection) => {
    fields.forEach((fieldToRemove) => removeField(fieldToRemove));
    selection.forEach((entityType) => {
      const field = addField();
      updateValue(field, nameToURI(entityType.id, CONFIG_CONTEXT));
    });
  };

  const isDisabled = (entityTypeId) =>
    disabledResourceURIs.includes(nameToURI(entityTypeId, CONFIG_CONTEXT));

  return (
    <>
      <Fields mandatory={mandatory} labelId={labelId} label={label} max={1}>
        <ListView
          status={status}
          size={size}
          renderPlaceholder={() => (
            <>
              <Box sx={{ fontStyle: 'italic' }}>
                {translate('selectionListPlaceholder')}
              </Box>
              <Button
                variant="text"
                onClick={() => setShowEntityTypeSelectDialog(true)}
                sx={{ paddingLeft: 0, paddingRight: 0 }}
              >
                {translate('addSelectionLabel')}
              </Button>
            </>
          )}
        >
          <List>
            <SelectionListItem
              translate={translate}
              label={translate('selectionListLabel')}
              onClick={() => setShowEntityTypeSelectDialog(true)}
              selection={selectedEntityTypes}
              disabled={status !== 'resolved'}
            />
          </List>
        </ListView>
        {error ? (
          <FormHelperText error>{translate(error)}</FormHelperText>
        ) : null}
      </Fields>
      {showEntityTypeSelectDialog ? (
        <EntityTypeSelectDialog
          closeDialog={() => {
            setShowEntityTypeSelectDialog(false);
          }}
          onSelect={(selection) => {
            onSelect(selection);
            setShowEntityTypeSelectDialog(false);
          }}
          isDisabled={isDisabled}
          selection={selectedEntityTypes}
        />
      ) : null}
    </>
  );
};

EntityTypeSelectEditor.propTypes = {
  name: PropTypes.string,
  nodetype: PropTypes.string.isRequired,
  graph: graphPropType,
  resourceURI: PropTypes.string,
  property: PropTypes.string,
  label: PropTypes.string,
  mandatory: PropTypes.bool,
  dispatch: PropTypes.func,
};

export default withListModelProvider(EntityTypeSelectEditor, () => ({
  sort: { field: 'label', order: 'asc' },
}));
