import { entrystore as store } from 'commons/store';
import config from 'config';
import { addIgnore } from 'commons/errors/utils/async';

/**
 * Get geonamesAPI configuration
 *
 * @returns {object}
 */
const getGeonamesAPIConfig = () => {
  const geonamesAPIConfig = config.get('itemstore.geonamesAPI');
  if (!geonamesAPIConfig) {
    throw Error('Geonames API configuration not found');
  }
  return geonamesAPIConfig;
};

/**
 * Retrieves the geonameId from a uri
 *
 * @param {string} uri
 * @returns {number}
 */
export const getGeonameId = (uri) =>
  parseInt(
    uri.substr(getGeonamesAPIConfig().uriPrefix.length).replace(/\/$/g, ''),
    10
  );

/**
 * @param {number} geonameId
 * @returns {string}
 */
export const getGeonameURI = (geonameId) =>
  `${getGeonamesAPIConfig().uriPrefix}${geonameId}`;

/**
 * @returns {string}
 */
export const getGeonameLabelProperty = () =>
  getGeonamesAPIConfig().labelProperty;

/**
 * Retrieves a location's hierarchy
 *
 * @param {number} geonameId
 * @returns {Array}
 */
export const getHierarchy = (geonameId) => {
  const url = `${getGeonamesAPIConfig().hierarchyURL}${geonameId}`;
  addIgnore('loadViaProxy', true, true);
  return store.loadViaProxy(url).then((results) => results.geonames);
};

/**
 * Retrieves a location's children
 *
 * @param {number} geonameId
 * @returns {Array}
 */
export const getChildren = (geonameId) => {
  const url = `${getGeonamesAPIConfig().childrenURL}${geonameId}`;
  addIgnore('loadViaProxy', true, true);
  return store.loadViaProxy(url).then((results) => results.geonames);
};
