import PropTypes from 'prop-types';
import { useState, useMemo } from 'react';
import { Button } from '@mui/material';
import InfoIcon from '@mui/icons-material/Info';
import { useTranslation } from 'commons/hooks/useTranslation';
import { useESContext } from 'commons/hooks/useESContext';
import useAsync from 'commons/hooks/useAsync';
import ListActionDialog from 'commons/components/common/dialogs/ListActionDialog';
import {
  List,
  ListHeader,
  ListHeaderItemText,
  ListView,
  ListItem,
  ListItemText,
  withListModelProvider,
  ListItemActionIconButton,
  CREATE,
  REFRESH,
  useListQuery,
  ListActions,
  ListSearch,
  ListPagination,
  ListItemCheckbox,
} from 'commons/components/ListView';
import ContentWrapper from 'commons/components/common/ContentWrapper';
import { getGroupWithHomeContext } from 'commons/util/store';
import { addAccessRightToGroup, entryPropType } from 'commons/util/entry';
import { getBuiltinNamespacesWithEntries } from 'models/namespaces/utils/builtinNamespaces';
import { Presenter } from 'commons/components/forms/presenters';
import { useSnackbar } from 'commons/hooks/useSnackbar';
import { ErrorCatcher } from 'commons/errors/ErrorCatcher';
import { ErrorWithMessage } from 'commons/errors/utils/errors';
import { useConfirmCloseAction } from 'commons/components/EntryListView/dialogs/create/handlers';
import { presenterAction, nlsBundles } from './actions';

/**
 *
 * @param {object} namespaceItem
 * @returns {object}
 */
const getInfoProps = (namespaceItem) => {
  const { fields, ...infoProps } = presenterAction;

  const fieldDefinitions = fields.map((field) => ({
    ...field,
    Presenter,
    getValue: () => namespaceItem[field.name],
  }));

  return {
    ...infoProps,
    fields: fieldDefinitions,
  };
};

const ImportNamespaceDialog = ({ closeDialog, dispatch, entries }) => {
  const translate = useTranslation(nlsBundles);
  const { context } = useESContext();
  const { runAsync, error: importError } = useAsync();
  const [namespacesToImport, setNamespacesToImport] = useState([]);

  const dispatchCreateNamespace = () => dispatch({ type: CREATE });
  const dispatchRefresh = () => dispatch({ type: REFRESH });
  const [addSnackbar] = useSnackbar();
  const confirmClose = useConfirmCloseAction(closeDialog);

  const listQueryItems = useMemo(() => {
    return getBuiltinNamespacesWithEntries(context, entries, translate);
  }, [context, entries, translate]);

  const {
    result: namespaceListItems,
    size,
    search,
  } = useListQuery({
    items: listQueryItems,
    searchFields: ['label', 'fullTitle'],
  });

  const handleSelectionChange = ({ target }, namespace) => {
    const { checked } = target;
    const newSelection = checked
      ? [...namespacesToImport, namespace]
      : namespacesToImport.filter(({ entry }) => entry !== namespace.entry);
    setNamespacesToImport(newSelection);
  };

  const createEntry = async (prototypeEntry) => {
    const group = await getGroupWithHomeContext(prototypeEntry.getContext());
    addAccessRightToGroup(prototypeEntry, group, 'owner');

    return prototypeEntry.commit();
  };

  const importNamespaces = async () => {
    if (namespacesToImport.length) {
      for (const namespaceToImport of namespacesToImport) {
        await createEntry(namespaceToImport.entry);
      }
      dispatchCreateNamespace();
    }

    return namespacesToImport;
  };

  const handleSave = () => {
    runAsync(
      importNamespaces()
        .then((namespaces) => {
          closeDialog();
          dispatchRefresh();
          addSnackbar({
            message: translate('createNamespaceSuccess', namespaces.length),
          });
        })
        .catch((error) => {
          throw new ErrorWithMessage(translate('createFailMessage'), error);
        })
    );
  };

  const actions = (
    <Button
      onClick={handleSave}
      autoFocus
      disabled={Boolean(!namespacesToImport.length)}
    >
      {translate('save')}
    </Button>
  );

  return (
    <>
      <ListActionDialog
        id="import-namespace"
        title={translate('reuseNamespaceTitle')}
        actions={actions}
        closeDialog={() => confirmClose(namespacesToImport.length > 0)}
        maxWidth="md"
        fixedHeight
      >
        <ContentWrapper>
          <ListView size={size} search={search}>
            <ListActions>
              <ListSearch />
            </ListActions>
            <ListHeader>
              <ListHeaderItemText xs={9} text={translate('listHeaderLabel')} />
            </ListHeader>
            <List>
              {namespaceListItems.map((namespaceItem) => {
                const { id, abbreviation, namespace, fullTitle, isImported } =
                  namespaceItem;

                const isSelected = namespacesToImport.includes(namespaceItem);
                return (
                  <ListItem key={id}>
                    <ListItemText
                      xs={8}
                      direction="column"
                      primary={`${abbreviation} - ${fullTitle}`}
                      secondary={namespace}
                    />
                    <ListItemActionIconButton
                      title={translate('infoEntry')}
                      action={{
                        title: translate('infoDialogTitle'),
                        nlsBundles,
                        ...getInfoProps(namespaceItem),
                      }}
                      xs={3}
                    >
                      <InfoIcon />
                    </ListItemActionIconButton>
                    <ListItemCheckbox
                      ariaLabel={abbreviation}
                      checked={isSelected || isImported}
                      disabled={isImported}
                      tooltip={isImported ? translate('importedTooltip') : ''}
                      onChange={(event) =>
                        handleSelectionChange(event, namespaceItem)
                      }
                    />
                  </ListItem>
                );
              })}
            </List>
            <ListPagination />
          </ListView>
        </ContentWrapper>
      </ListActionDialog>
      <ErrorCatcher error={importError} />
    </>
  );
};

ImportNamespaceDialog.propTypes = {
  closeDialog: PropTypes.func.isRequired,
  dispatch: PropTypes.func,
  entries: PropTypes.arrayOf(entryPropType),
};

export default withListModelProvider(ImportNamespaceDialog, () => ({
  sort: { field: 'label', order: 'asc' },
}));
