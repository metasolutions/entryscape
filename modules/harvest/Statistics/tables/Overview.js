import PropTypes from 'prop-types';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableHead from '@mui/material/TableHead';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import Typography from '@mui/material/Typography';
import { useTranslation } from 'commons/hooks/useTranslation';
import 'harvest/Statistics/tables/index.scss';

const Overview = ({ stats, nlsBundle }) => {
  const overviewTypes = [
    {
      name: 'totalDatasetCount',
      nlsKey: 'datasets',
    },
    {
      name: 'totalDistributionCount',
      nlsKey: 'distributions',
    },
    {
      name: 'nrOfPublishers',
      nlsKey: 'publishers',
    },
    {
      name: 'nrOfContactPoints',
      nlsKey: 'contactPoints',
    },
  ];

  const overviewFulfillments = [
    {
      name: 'datasetsMandatoryFulfillment',
      nlsKey: 'fulfillmentMandatoryDataset',
    },
    {
      name: 'distributionsMandatoryFulfillment',
      nlsKey: 'fulfillmentMandatoryDistribution',
    },
    {
      name: 'datasetsRecommendedFulfillment',
      nlsKey: 'fulfillmentRecommendedDataset',
    },
    {
      name: 'distributionsRecommendedFulfillment',
      nlsKey: 'fulfillmentRecommendedDistribution',
    },
  ];

  const t = useTranslation(nlsBundle);

  return (
    <div>
      <Typography
        variant="h6"
        component="h2"
        className="esreStatistics__header"
      >
        {t('header')}
      </Typography>
      <TableContainer component={Paper}>
        <Table size="small" aria-label={t('header')}>
          <TableHead>
            <TableRow>
              <TableCell align="left">{t('type')}</TableCell>
              <TableCell align="center">{t('numberEntries')}</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {overviewTypes.map((data) => (
              <TableRow hover key={data.name}>
                <TableCell
                  className="esreTable__th--bold"
                  component="th"
                  scope="row"
                >
                  {t(data.nlsKey)}
                </TableCell>
                <TableCell align="center">{stats[data.name]}</TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      <Typography
        variant="h6"
        component="h2"
        className="esreStatistics__header"
      >
        {t('metadataFulfillmentHeader')}
      </Typography>
      <TableContainer component={Paper}>
        <Table size="small" aria-label={t('metadataFulfillmentHeader')}>
          <TableHead>
            <TableRow>
              <TableCell align="left">{t('metadata')}</TableCell>
              <TableCell align="center">{t('percentageFulfillment')}</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {overviewFulfillments.map(({ nlsKey, name }) => (
              <TableRow hover key={name}>
                <TableCell
                  className="esreTable__th--bold"
                  component="th"
                  scope="row"
                  align="left"
                >
                  {t(nlsKey)}
                </TableCell>
                <TableCell align="center">{stats[name]}</TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </div>
  );
};

Overview.propTypes = {
  stats: PropTypes.shape(),
  nlsBundle: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
};

export default Overview;
