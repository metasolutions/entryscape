import React from 'react';
import PropTypes from 'prop-types';
import { Icon as MuiIcon } from '@mui/material';
import './Icon.scss';

const Icon = ({ ...props }) => (
  <MuiIcon classes={{ root: 'escoIcon' }} {...props} />
);

Icon.propTypes = {
  children: PropTypes.element.isRequired,
};

export default Icon;
