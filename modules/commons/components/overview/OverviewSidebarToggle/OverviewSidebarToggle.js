import PropTypes from 'prop-types';
import {
  FormControlLabel,
  ListItemIcon,
  ListItemText,
  ListItemSecondaryAction,
  Switch,
} from '@mui/material';
import Tooltip from 'commons/components/common/Tooltip';
import ConditionalWrapper from 'commons/components/common/ConditionalWrapper';
import { withStyles } from '@mui/styles';
import { success } from 'commons/theme/mui/colors';
import './OverviewSidebarToggle.scss';

const SidebarSwitch = withStyles({
  switchBase: {
    '&$checked': {
      color: success.main,
    },
    '&$checked + $track': {
      backgroundColor: success.main,
    },
    '&$disabled$checked': {
      color: success.light,
    },
    '&$checked$disabled + $track': {
      backgroundColor: success.light,
    },
  },
  checked: {},
  track: {},
  disabled: {},
})(Switch);

const OverviewTooltip = (label) => (children) =>
  (
    <Tooltip title={label}>
      <span>{children}</span>
    </Tooltip>
  );

const OverviewSidebarToggle = ({
  onChange,
  disabled,
  isChecked,
  primaryLabel,
  secondaryLabel,
  icon,
  switchTooltipLabel,
  listItemTextTooltipLabel,
  highlightId,
}) => {
  return (
    <Tooltip title={switchTooltipLabel}>
      <span
        className={`escoOverviewSidebarToggle${
          disabled ? ' escoOverviewSidebarToggle--disabled' : ''
        }`}
        data-highlight-id={highlightId}
      >
        <FormControlLabel
          classes={{ root: 'escoOverviewSidebarToggle__switchLabel' }}
          disableTypography
          label={
            <>
              <ListItemIcon className="escoOverviewSidebarToggle__icon--dense">
                {icon}
              </ListItemIcon>
              <ConditionalWrapper
                condition={Boolean(listItemTextTooltipLabel)}
                wrapper={OverviewTooltip(listItemTextTooltipLabel)}
              >
                <ListItemText
                  id="overview-toggle-label"
                  primary={primaryLabel}
                  secondary={secondaryLabel}
                  classes={{
                    primary: 'escaOverviewSidebarToggle__primaryLabel',
                    secondary: `escoOverviewSidebarToggle__labelSecondary ${
                      !isChecked &&
                      'escoOverviewSidebarToggle__labelSecondary--inactive'
                    }`,
                  }}
                />
              </ConditionalWrapper>
            </>
          }
          control={
            <ListItemSecondaryAction
              classes={{
                root: 'escoOverviewSidebarToggle__secondaryAction',
              }}
            >
              <SidebarSwitch
                edge="end"
                inputProps={{
                  'aria-labelledby': 'overview-toggle-label',
                }}
                checked={isChecked}
                disabled={disabled}
                onChange={onChange}
              />
            </ListItemSecondaryAction>
          }
        />
      </span>
    </Tooltip>
  );
};

OverviewSidebarToggle.propTypes = {
  onChange: PropTypes.func.isRequired,
  disabled: PropTypes.bool,
  isChecked: PropTypes.bool.isRequired,
  primaryLabel: PropTypes.string.isRequired,
  secondaryLabel: PropTypes.string.isRequired,
  icon: PropTypes.node.isRequired,
  switchTooltipLabel: PropTypes.string,
  listItemTextTooltipLabel: PropTypes.string,
  highlightId: PropTypes.string,
};

export default OverviewSidebarToggle;
