import { useState, useEffect, useCallback } from 'react';
import config from 'config';
import { getDistributionFilesInfo } from 'catalog/datasets/DatasetOverview/utils/distributions';
import useAsyncCallback from 'commons/hooks/useAsyncCallback';
import { useTranslation } from 'commons/hooks/useTranslation';
import escaFilesNLS from 'catalog/nls/escaFiles.nls';
import { convertBytesToMBytes } from 'commons/util/util';
import useGetMainDialog from 'commons/hooks/useGetMainDialog';
import { CSV_VALID_FORMATS } from '../utils/csv';

const useValidateFiles = (distributionEntry) => {
  const { getConfirmationDialog, getAcknowledgeDialog } = useGetMainDialog();
  const [state, setState] = useState({
    validFiles: null, // Null initialization to use as a flag on the dialog :/
    dialogType: null,
    messages: { content: null, rejectLabel: null, affirmLabel: null },
  });

  const [filesInfo] = useAsyncCallback(
    getDistributionFilesInfo,
    distributionEntry
  );
  const t = useTranslation(escaFilesNLS);

  useEffect(() => {
    if (!filesInfo) {
      return;
    }

    const maxFileSizeForAPI = config.get('catalog.maxFileSizeForAPI');

    let totalFilesSize = 0;
    filesInfo.forEach((file) => {
      totalFilesSize += file.sizeOfFile || 0;
    });

    if (totalFilesSize > maxFileSizeForAPI) {
      const maxSizeInMb = convertBytesToMBytes(maxFileSizeForAPI);
      const acknowledgeMessage = t('activateAPINotAllowedFileToBig', {
        size: maxSizeInMb,
      });

      setState({
        validFiles: false,
        dialogType: 'acknowledge',
        messages: { content: acknowledgeMessage },
      });
      return;
    }

    const nonCsvFile = filesInfo.find(
      (file) => !CSV_VALID_FORMATS.includes(file.format)
    );
    if (nonCsvFile) {
      const messages = {
        content: t('onlyCSVSupported', {
          format: nonCsvFile?.format || '-',
        }),
        affirmLabel: t('confirmAPIActivation'),
        rejectLabel: t('abortAPIActivation'),
      };

      setState({
        validFiles: false,
        dialogType: 'confirm',
        messages,
      });
      return;
    }

    setState({ validFiles: true });
  }, [filesInfo, setState, t]);

  /**
   * Determines whether and what kind of dialog to present, depending on the
   * file validation results.
   *
   * @returns {Promise}
   */
  const confirmFiles = useCallback(async () => {
    if (!state.validFiles && state.dialogType === 'acknowledge') {
      return getAcknowledgeDialog({
        ...state.messages,
        dialogProps: { style: { zIndex: 1301 } },
      });
    }

    if (!state.validFiles && state.dialogType === 'confirm') {
      return getConfirmationDialog({
        ...state.messages,
        dialogProps: { style: { zIndex: 1301 } },
      });
    }

    return Promise.resolve(true);
  }, [
    state.validFiles,
    state.dialogType,
    state.messages,
    getAcknowledgeDialog,
    getConfirmationDialog,
  ]);

  return {
    validFiles: state.validFiles,
    dialogType: state.dialogType,
    confirmFiles,
  };
};

export default useValidateFiles;
