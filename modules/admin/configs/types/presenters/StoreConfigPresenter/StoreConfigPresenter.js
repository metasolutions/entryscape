import PropTypes from 'prop-types';
import { graphPropType } from 'commons/util/entry';
import { Graph } from '@entryscape/rdfjson';
import esadTypeNLS from 'admin/nls/esadType.nls';
import { useTranslation } from 'commons/hooks/useTranslation';
import './StoreConfigPresenter.scss';
import {
  Fields,
  FieldsLabel,
  LanguageLiteral,
} from 'commons/components/forms/presenters';

/**
 * @param {object} params
 * @param {Graph} params.graph
 * @param {string} params.resourceURI
 * @param {string} params.property
 * @returns {Array<object>}
 */
const getValues = ({ graph, resourceURI, property }) => {
  const blankNodeIds = graph
    .find(resourceURI, property)
    .map((statement) => statement.getValue());
  return blankNodeIds.map((blankNodeId) => {
    const endpoint = graph.findFirstValue(blankNodeId, 'dcterms:source');
    return { blankNodeId, endpoint };
  });
};

export const StoreConfig = ({ graph, blankNodeId, endpoint }) => {
  const translate = useTranslation(esadTypeNLS);
  return (
    <div className="esadStoreConfig">
      <FieldsLabel alignItems="start" label={`${translate('labelsLabel')}: `} />
      <LanguageLiteral
        graph={graph}
        resourceURI={blankNodeId}
        property="dcterms:title"
      />

      <FieldsLabel alignItems="start" label={`${translate('endpointLabel')}: `}>
        <span className="esadStoreConfig__value">{endpoint}</span>
      </FieldsLabel>
    </div>
  );
};

StoreConfig.propTypes = {
  graph: graphPropType,
  blankNodeId: PropTypes.string,
  endpoint: PropTypes.string,
};

export const StoreConfigPresenter = ({
  graph,
  resourceURI,
  property,
  ...fieldsProps
}) => {
  const values = getValues({ graph, resourceURI, property });
  return values.length ? (
    <Fields {...fieldsProps}>
      {values.map((value) => {
        return <StoreConfig key={value.endpoint} {...value} graph={graph} />;
      })}
    </Fields>
  ) : null;
};

StoreConfigPresenter.propTypes = {
  graph: graphPropType,
  resourceURI: PropTypes.string,
  property: PropTypes.string,
};
