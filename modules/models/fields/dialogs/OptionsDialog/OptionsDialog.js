import { useState } from 'react';
import PropTypes from 'prop-types';
import { Box, Button } from '@mui/material';
import useAsync from 'commons/hooks/useAsync';
import ContentWrapper from 'commons/components/common/ContentWrapper';
import { RDF_PROPERTY_OPTION, RDF_PROPERTY_TERMINOLOGY } from 'models/utils/ns';
import { useTranslation } from 'commons/hooks/useTranslation';
import esmoFieldsNLS from 'models/nls/esmoFields.nls';
import esmoCommonsNLS from 'models/nls/esmoCommons.nls';
import escoDialogsNLS from 'commons/nls/escoDialogs.nls';
import escoFormsNLS from 'commons/nls/escoForms.nls';
import { useEditorContext } from 'commons/components/forms/editors/EditorContext';
import useGetMainDialog from 'commons/hooks/useGetMainDialog';
import { useSnackbar, SUCCESS_EDIT } from 'commons/hooks/useSnackbar';
import { ErrorWithMessage } from 'commons/errors/utils/errors';
import { ErrorCatcher } from 'commons/errors/ErrorCatcher';
import { hasManagedOptions, removeOptions } from 'models/fields/utils/options';
import ListActionDialog from 'commons/components/common/dialogs/ListActionDialog';
import SelectEditMode from 'models/components/SelectEditMode';
import { useConfirmCloseAction } from 'commons/components/EntryListView/dialogs/create/handlers';
import TermsChooser from './TermsChooser';
import OptionsManager from './OptionsManager';
import './OptionsDialog.scss';

const NLS_BUNDLES = [
  esmoFieldsNLS,
  esmoCommonsNLS,
  escoFormsNLS,
  escoDialogsNLS,
];

const TERMINOLOGY = 'terminology';
const MANAGED = 'managed';

const EDIT_MODE_CHOICES = [
  { labelNlsKey: 'useTerminologyLabel', value: TERMINOLOGY },
  { labelNlsKey: 'useManagedOptionsLabel', value: MANAGED },
];

/**
 * Use managed options if any managed option is detected. This is also the
 * default mode.
 *
 * @param {Graph} graph
 * @param {string} resourceURI
 * @returns {string}
 */
const getInitialEditMode = (graph, resourceURI) => {
  return graph.findFirstValue(resourceURI, RDF_PROPERTY_TERMINOLOGY) &&
    !graph.findFirstValue(resourceURI, RDF_PROPERTY_OPTION)
    ? TERMINOLOGY
    : MANAGED;
};

const OptionsDialog = ({ onSave, closeDialog }) => {
  const { graph, resourceURI, entry: fieldEntry } = useEditorContext();
  const { runAsync, error: saveError } = useAsync();
  const { getConfirmationDialog } = useGetMainDialog();
  const [editMode, setEditMode] = useState(
    getInitialEditMode(graph, resourceURI)
  );
  const [addSnackbar] = useSnackbar();
  const translate = useTranslation(NLS_BUNDLES);
  const [hasChanges, setHasChanges] = useState(false);
  const confirmClose = useConfirmCloseAction(closeDialog);

  const saveOptions = () => {
    const confirmAndSave = async () => {
      if (editMode === TERMINOLOGY && hasManagedOptions(graph, resourceURI)) {
        const proceed = await getConfirmationDialog({
          content: translate('confirmRemoveOptions'),
          affirmLabel: translate('proceed'),
          rejectLabel: translate('cancel'),
        });
        if (!proceed) return;
        removeOptions(graph, resourceURI);
      }

      return fieldEntry
        .setMetadata(graph)
        .commitMetadata()
        .then(() => {
          onSave();
          closeDialog();
        })
        .then(() => addSnackbar({ type: SUCCESS_EDIT }))
        .catch((error) => {
          throw new ErrorWithMessage(translate('saveEditsFail'), error);
        });
    };

    runAsync(confirmAndSave());
  };

  const handleChangeEditMode = () => {
    setEditMode(editMode === TERMINOLOGY ? MANAGED : TERMINOLOGY);
    setHasChanges(true);
  };

  const onImport = () => {
    setEditMode(MANAGED);
    setHasChanges(true);
  };

  const actions = (
    <Button autoFocus onClick={saveOptions} disabled={!hasChanges}>
      {translate('save')}
    </Button>
  );

  return (
    <>
      <ListActionDialog
        id="field-options-dialog"
        title={translate('editOptions')}
        maxWidth="lg"
        fixedHeight
        closeDialog={() => confirmClose(hasChanges)}
        actions={actions}
      >
        <ContentWrapper>
          <Box marginBottom="16px">
            <SelectEditMode
              value={editMode}
              choices={EDIT_MODE_CHOICES}
              onChange={handleChangeEditMode}
              nlsBundles={NLS_BUNDLES}
            />
          </Box>
          {editMode === TERMINOLOGY ? (
            <TermsChooser
              nlsBundles={NLS_BUNDLES}
              onChange={setHasChanges}
              onImport={onImport}
            />
          ) : (
            <OptionsManager onChange={setHasChanges} />
          )}
        </ContentWrapper>
      </ListActionDialog>
      <ErrorCatcher error={saveError} />
    </>
  );
};

OptionsDialog.propTypes = {
  onSave: PropTypes.func,
  closeDialog: PropTypes.func,
};

// make sure entry is available before opening the dialog
const OptionsDialogWrapper = (props) => {
  const { graph } = useEditorContext();
  return graph ? <OptionsDialog {...props} /> : null;
};

export default OptionsDialogWrapper;
