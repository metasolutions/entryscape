import PropTypes from 'prop-types';
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Grid,
} from '@mui/material';
import useRDFormsValidationPresenter from 'toolkit/Validate/InstanceReportDialog/useRDFormsValidationPresenter';

const ReportDialog = ({
  open,
  handleClose,
  buttonLabel,
  title,
  template,
  graph,
  resource,
}) => {
  const { PresenterComponent } = useRDFormsValidationPresenter(
    graph,
    resource,
    template
  );

  return (
    <Dialog
      open={open}
      onClose={handleClose}
      aria-labelledby="report-details-dialog-title"
      aria-describedby="report-details-dialog-description"
      fullWidth
    >
      {title && (
        <DialogTitle id="report-details-dialog-title">{title} </DialogTitle>
      )}
      <DialogContent dividers>
        <Grid container justifyContent="center" alignItems="center">
          <Grid item xs={12} md={10}>
            {PresenterComponent && <PresenterComponent />}
          </Grid>
        </Grid>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose} variant="text" autoFocus>
          {buttonLabel}
        </Button>
      </DialogActions>
    </Dialog>
  );
};

ReportDialog.propTypes = {
  open: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  buttonLabel: PropTypes.string.isRequired,
  title: PropTypes.string,
  template: PropTypes.shape().isRequired,
  graph: PropTypes.shape().isRequired,
  resource: PropTypes.string.isRequired,
};

export default ReportDialog;
