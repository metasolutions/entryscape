import PropTypes from 'prop-types';
import { useEffect, useRef } from 'react';
import Button from '@mui/material/Button';
import LaunchIcon from '@mui/icons-material/Launch';

const SSOButton = ({ children, autofocus, color, onClick }) => {
  const linkButton = useRef(null);

  const setFocus = () => {
    linkButton.current.focus();
  };

  useEffect(() => {
    if (autofocus) {
      setFocus();
    }
  }, []);

  return (
    <Button
      ref={linkButton}
      fullWidth
      onClick={onClick}
      color={color}
      endIcon={<LaunchIcon />}
    >
      {children}
    </Button>
  );
};

export default SSOButton;

SSOButton.propTypes = {
  children: PropTypes.string.isRequired,
  autofocus: PropTypes.bool,
  color: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
};
