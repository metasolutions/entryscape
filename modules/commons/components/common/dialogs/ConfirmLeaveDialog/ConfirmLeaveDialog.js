import { useEffect } from 'react';
import { useBlocker } from 'react-router-dom';
import PropTypes from 'prop-types';
import useGetMainDialog from 'commons/hooks/useGetMainDialog';
import { useTranslation } from 'commons/hooks/useTranslation';
import escoDialogsNLS from 'commons/nls/escoDialogs.nls';
import useAsync from 'commons/hooks/useAsync';

// This is a workaround because of a bug in react router dom, where blocker is
// not being cleaned up properly, when a route has been recreated:
// https://github.com/remix-run/react-router/issues/11430
// The shouldBlockGlobal is used to make sure navigation is not blocked
// after useBlocker has been used.
let shouldBlockGlobal = false;

const getShouldBlockGlobal = () => {
  return shouldBlockGlobal;
};

export const ConfirmLeaveDialog = ({ onClose, shouldBlock }) => {
  const blocker = useBlocker(({ currentLocation, nextLocation }) => {
    return (
      getShouldBlockGlobal() &&
      currentLocation.pathname !== nextLocation.pathname
    );
  });
  const { getConfirmationDialog } = useGetMainDialog();
  const translate = useTranslation(escoDialogsNLS);
  const { runAsync } = useAsync();

  useEffect(() => {
    shouldBlockGlobal = shouldBlock;

    return () => {
      shouldBlockGlobal = false;
    };
  }, [shouldBlock, blocker]);

  useEffect(() => {
    if (blocker.state !== 'blocked') return;

    runAsync(
      getConfirmationDialog({
        content: translate('unsavedChanges'),
        rejectLabel: translate('cancel'),
        affirmLabel: translate('proceed'),
      }).then((proceed) => {
        if (proceed) {
          blocker.proceed();
          onClose();
          return;
        }
        blocker.reset();
        onClose();
      })
    );
  }, [
    runAsync,
    onClose,
    getConfirmationDialog,
    translate,
    blocker.state,
    blocker,
  ]);

  return null;
};

ConfirmLeaveDialog.propTypes = {
  onClose: PropTypes.func,
  shouldBlock: PropTypes.bool,
};
