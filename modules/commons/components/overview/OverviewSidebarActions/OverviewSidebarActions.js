import PropTypes from 'prop-types';
import { useActions, withActionsProvider } from 'commons/components/ListView';
import {
  nlsBundlesPropType,
  useTranslation,
} from 'commons/hooks/useTranslation';
import { getIconFromActionId } from 'commons/actions';
import 'commons/components/EntryListView';
import { truncateLabel } from 'commons/util/util';
import OverviewSidebarButton from '../OverviewSidebarButton';
import OverviewSidebarItem from '../OverviewSidebarItem';

export const OverviewSidebarActions = ({
  actions,
  excludeActions,
  nlsBundles,
  ...actionsParams
}) => {
  const translate = useTranslation(nlsBundles);
  const { openActionDialog } = useActions();
  const handleAction = (action) => () => openActionDialog(action);

  const getActionProps = (actionConfig) => {
    const { getProps = () => ({}), Dialog, isVisible, ...rest } = actionConfig;
    const { labelNlsKey, tooltipNlsKey, action, ...componentProps } = {
      ...rest,
      ...getProps({ ...actionsParams, nlsBundles, translate }),
    };

    return {
      action: {
        Dialog,
        nlsBundles,
        ...actionsParams,
        ...action,
      },
      label: labelNlsKey ? translate(labelNlsKey) : null,
      tooltip: tooltipNlsKey ? translate(tooltipNlsKey) : null,
      Component: OverviewSidebarButton,
      startIcon: getIconFromActionId(actionConfig.id),
      ...componentProps,
    };
  };

  const visibleActions = actions
    .filter(({ id: actionId }) => !excludeActions.includes(actionId))
    .filter(({ isVisible }) => (isVisible ? isVisible(actionsParams) : true))
    .map(getActionProps);

  if (!visibleActions.length) return null;

  return (
    <div>
      {visibleActions.map(
        ({ action, label, tooltip, Component, ...rest }, index) => {
          const [truncatedLabel, labelAsTooltip] = truncateLabel(label, 25);
          return (
            <OverviewSidebarItem key={`sidebar-item-${rest.id}`}>
              <Component
                key={`sidebar-button-${action.id}`}
                color={index === 0 ? 'primary' : 'secondary'}
                tooltip={tooltip || labelAsTooltip}
                {...(action.Dialog ? { onClick: handleAction(action) } : {})}
                // eslint-disable-next-line react/jsx-props-no-spreading
                {...rest}
              >
                {truncatedLabel}
              </Component>
            </OverviewSidebarItem>
          );
        }
      )}
    </div>
  );
};

OverviewSidebarActions.propTypes = {
  actions: PropTypes.arrayOf(PropTypes.shape({})),
  excludeActions: PropTypes.arrayOf(PropTypes.string),
  actionParams: PropTypes.shape({}),
  nlsBundles: nlsBundlesPropType,
};

export default withActionsProvider(OverviewSidebarActions);
