import * as ns from 'models/utils/ns';
import {
  RadioEditor,
  LITERAL,
  CheckboxEditor,
  GroupEditor,
} from 'commons/components/forms/editors';
import * as fields from './fieldDefinitions';
import { FORM_NODETYPE_CHOICES, FORM_TYPE_CHOICES } from './choiceDefinitions';

export const FORM_TYPE = {
  name: 'type',
  property: 'rdf:type',
  labelNlsKey: 'formType',
  nodetype: 'URI',
  choices: FORM_TYPE_CHOICES,
  max: 1,
  Editor: RadioEditor,
  mandatory: true,
  row: true,
  isFormType: true,
};

export const FORM_NODETYPE = {
  ...fields.FIELD_NODETYPE,
  choices: FORM_NODETYPE_CHOICES,
  isVisible: (graph, resourceURI) =>
    [
      ns.RDF_TYPE_PROFILE_FORM,
      ns.RDF_TYPE_PROPERTY_FORM,
      ns.RDF_TYPE_OBJECT_FORM,
    ].some((type) => Boolean(graph.find(resourceURI, 'rdf:type', type).length)),
};

export const FORM_CARDINALITY = {
  ...fields.FIELD_CARDINALITY,
  isVisible: (graph, resourceURI) =>
    graph.find(resourceURI, 'rdf:type', ns.RDF_TYPE_PROPERTY_FORM).length,
};

export const FORM_PROPERTY = {
  ...fields.FIELD_PROPERTY,
  isVisible: (graph, resourceURI) =>
    graph.find(resourceURI, 'rdf:type', ns.RDF_TYPE_PROPERTY_FORM).length,
};

export const FORM_EXTENDS = {
  ...fields.FIELD_EXTENDS,
  choices: FORM_TYPE_CHOICES,
};

const FIELD_OVERRIDE_FORM_ITEMS = {
  name: 'override-formitems',
  labelNlsKey: 'overrideFormItemsLabel',
  max: 1,
  property: ns.RDF_PROPERTY_OVERRIDE_FORM_ITEMS,
  nodetype: LITERAL,
  Editor: CheckboxEditor,
  initialValue: 'true',
};

export const FORM_FORM_ITEMS = {
  name: 'formitems',
  labelNlsKey: 'formItemsTitle',
  property: 'form-items',
  Editor: GroupEditor,
  editors: [FIELD_OVERRIDE_FORM_ITEMS],
  max: 1,
};
