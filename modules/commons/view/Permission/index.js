import { useTranslation } from 'commons/hooks/useTranslation';
import escoPermissionNLS from 'commons/nls/escoPermission.nls';
import Error from 'commons/view/Error';

const PermissionView = () => {
  const t = useTranslation(escoPermissionNLS);

  return <Error header={t('permissionHeader')} body={t('permissionBody')} />;
};

export default PermissionView;
