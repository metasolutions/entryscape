import PropTypes from 'prop-types';
import { Entry } from '@entryscape/entrystore-js';
import { RDF_STATUS_INVESTIGATING } from 'commons/util/entry';
import ConfirmationDialog from '../ConfirmationDialog';
import { archiveOrUnarchiveSuggestion } from '../../util';

const UnarchiveDialog = ({ entry, closeDialog, onUnarchive }) => {
  return (
    <ConfirmationDialog
      buttonLabel="unarchiveButtonLabel"
      dialogConfirmationQuestionNLS="unArchiveSuggestion"
      closeDialog={closeDialog}
      buttonAction={() =>
        archiveOrUnarchiveSuggestion(entry, RDF_STATUS_INVESTIGATING)
          .then(onUnarchive)
          .then(closeDialog)
      }
    />
  );
};

UnarchiveDialog.propTypes = {
  closeDialog: PropTypes.func,
  onUnarchive: PropTypes.func,
  entry: PropTypes.instanceOf(Entry),
};

export default UnarchiveDialog;
