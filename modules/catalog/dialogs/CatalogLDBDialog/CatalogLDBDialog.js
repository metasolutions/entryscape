import { Entry } from '@entryscape/entrystore-js';
import LinkedDataBrowserDialog from 'commons/components/LinkedDataBrowserDialog';
import { RDF_TYPE_DATASET, entryPropType } from 'commons/util/entry';
import DatasetLDBDialog from 'catalog/datasetTemplates/DatasetLinkedDataBrowserDialog';
// eslint-disable-next-line max-len
import VisualizationLDBDialog from 'catalog/datasets/DatasetOverview/dialogs/VisualizationLDBDialog';

const VISUALIZATION_RDF_TYPE =
  'http://entryscape.com/terms/visualization/Visualization';
const CATALOG_RDF_TYPES = [RDF_TYPE_DATASET, VISUALIZATION_RDF_TYPE];

const RDF_TYPE_LDBD_MAP = {
  [RDF_TYPE_DATASET]: DatasetLDBDialog,
  [VISUALIZATION_RDF_TYPE]: VisualizationLDBDialog,
};

/**
 *
 * @param {Entry} entry
 * @returns {Function}
 */
const findLDBDialog = (entry) => {
  const graph = entry.getMetadata();
  const resourceURI = entry.getResourceURI();
  const specialRdfType = CATALOG_RDF_TYPES.find((rdfType) => {
    const statements = graph.find(resourceURI, 'rdf:type', rdfType);
    return statements.length > 0;
  });
  return RDF_TYPE_LDBD_MAP[specialRdfType] || LinkedDataBrowserDialog;
};

const CatalogLDBDialog = ({ entry, ...rest }) => {
  const LdbDialog = findLDBDialog(entry);
  return <LdbDialog entry={entry} {...rest} />;
};

CatalogLDBDialog.propTypes = {
  entry: entryPropType,
};

export default CatalogLDBDialog;
