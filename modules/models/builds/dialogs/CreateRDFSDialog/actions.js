import {
  FIELD_TITLE,
  FIELD_DCT_DESCRIPTION,
} from 'models/utils/fieldDefinitions';
import { LiteralEditor, URI } from 'commons/components/forms/editors';

export const FIELD_ONTOLOGY = {
  property: 'rdfs:ontology',
  nodetype: URI,
  Editor: LiteralEditor,
  mandatory: true,
  max: 1,
};

export const fields = [FIELD_TITLE, FIELD_DCT_DESCRIPTION];
