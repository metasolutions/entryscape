import PropTypes from 'prop-types';
import { useState } from 'react';
import {
  Alert,
  Button,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  Grid,
} from '@mui/material';
import { addIgnore } from 'commons/errors/utils/async';
import EntryImport from 'commons/components/entry/Import';
import esreLoadDialogNLS from 'toolkit/nls/esreLoadDialog.nls';
import { readFileAsText } from 'commons/util/file';
import { converters } from '@entryscape/rdfjson';
import { entrystore } from 'commons/store';
import { useTranslation } from 'commons/hooks/useTranslation';
import useLinkOrFile from 'commons/components/entry/Import/useLinkOrFile';
import { useUserState } from 'commons/hooks/useUser';
import { USER_ENTRY_ID_GUEST } from 'commons/util/userIds';

const UploadDialog = ({ open, setOpen, setGraph, setRDFXML, setEditorTab }) => {
  const [error, setError] = useState(null);
  const { userInfo } = useUserState();
  const {
    link,
    setLink,
    file,
    setFile,
    handleLinkChange,
    handleFileChange,
    handleTabChange,
    linkIsValid,
    buttonEnabled,
    isLink,
    setIsLink,
  } = useLinkOrFile();
  const t = useTranslation(esreLoadDialogNLS);
  const isGuest = userInfo.entryId === USER_ENTRY_ID_GUEST;

  const handleClose = () => {
    setOpen(false);
    setLink('');
    setFile(null);
    setError(null);
    setIsLink(true); // Resets the tab to the default (load via link)
  };

  const loadFromLink = async () => {
    try {
      addIgnore('loadViaProxy', true, true);
      const data = await entrystore
        .loadViaProxy(link, 'application/rdf+xml')
        .catch(() => {
          throw new Error(t('loadLinkError'));
        });
      const report = await converters.detect(data);

      if (report.error) {
        throw new Error(report.error);
      }

      setGraph(report.graph);
      setEditorTab(0);
      setRDFXML(report.graph);
      handleClose();
    } catch (err) {
      setError(err.message);
    }
  };

  const loadFromFile = async () => {
    const uploadedFile = file.files.item(0);

    try {
      const data = await readFileAsText(uploadedFile);
      const report = await converters.detect(data);

      if (report.error) {
        throw new Error(report.error);
      }

      setGraph(report.graph);
      setEditorTab(0);
      setRDFXML(report.graph);
      handleClose();
    } catch (err) {
      setError(err.message);
    }
  };

  const handleLoad = () => {
    setError(null);

    if (isLink) {
      loadFromLink();
      return;
    }

    loadFromFile();
  };

  return (
    <Dialog
      open={open}
      onClose={handleClose}
      aria-labelledby="load-dialog-title"
      maxWidth="sm"
      fullWidth
    >
      <DialogTitle id="load-dialog-title">{t('loadTitle')}</DialogTitle>
      <DialogContent dividers>
        <Grid container justifyContent="center" alignItems="center">
          <Grid item xs={12} md={10}>
            <EntryImport
              disableLink={isGuest}
              link={link}
              file={file}
              handleLinkChange={(event) => {
                setError(null);
                handleLinkChange(event);
              }}
              handleFileChange={(event) => {
                setError(null);
                handleFileChange(event);
              }}
              validURI={!!linkIsValid}
              handleTabChange={(event) => {
                setError(null);
                handleTabChange(event);
              }}
            />
            {error && (
              <Alert severity="error" onClose={() => setError(null)}>
                {error}
              </Alert>
            )}
          </Grid>
        </Grid>
      </DialogContent>

      <DialogActions>
        <Button variant="text" onClick={handleClose}>
          {t('cancelButton')}
        </Button>
        <Button disabled={!buttonEnabled} onClick={handleLoad}>
          {t('loadButton')}
        </Button>
      </DialogActions>
    </Dialog>
  );
};

UploadDialog.propTypes = {
  open: PropTypes.bool,
  setOpen: PropTypes.func.isRequired,
  setGraph: PropTypes.func.isRequired,
  setRDFXML: PropTypes.func.isRequired,
  setEditorTab: PropTypes.func.isRequired,
};

export default UploadDialog;
