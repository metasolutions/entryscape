import { useEntry } from 'commons/hooks/useEntry';
import { getLabel } from 'commons/util/rdfUtils';
import { getViewDefFromName } from 'commons/util/site';
import escoListNLS from 'commons/nls/escoList.nls';
import esmoNamespacesNLS from 'models/nls/esmoNamespaces.nls';
import esmoCommonsNLS from 'models/nls/esmoCommons.nls';
import { NAMESPACES_LISTVIEW } from 'models/config/config';
import escoDialogsNLS from 'commons/nls/escoDialogs.nls';
import { useTranslation } from 'commons/hooks/useTranslation';
import useDocumentTitle from 'commons/hooks/useDocumentTitle';
import usePageTitle from 'commons/hooks/usePageTitle';
import useGetContributors from 'commons/hooks/useGetContributors';
import escoOverviewNLS from 'commons/nls/escoOverview.nls';
import { i18n } from 'esi18n';
import Overview from 'commons/components/overview/Overview';
import {
  withOverviewModelProvider,
  overviewPropsPropType,
} from 'commons/components/overview';
import {
  useOverviewModel,
  REFRESH as REFRESH_OVERVIEW,
  withOverviewRefresh,
} from 'commons/components/overview/hooks/useOverviewModel';
import { useESContext } from 'commons/hooks/useESContext';
import {
  ACTION_EDIT,
  ACTION_REMOVE,
  ACTION_INFO_WITH_ICON,
  DESCRIPTION_UPDATED,
} from 'commons/components/overview/actions';
import { useCallback } from 'react';
import RemoveEntityDialog from 'commons/components/EntityOverview/dialogs/RemoveEntityDialog';
import ModelsLDBDialog from 'models/dialogs/ModelsLDBDialog';
import useGetNamespaceEntries from '../hooks/useGetNamespaceEntries';
import NamespaceEditDialog from '../dialogs/NamespaceEditDialog';
import NamespaceDefaultDialog from '../dialogs/NamespaceDefaultDialog';
import {
  hasDefaultValue,
  getDefaultEntry,
  isBuiltin,
} from '../utils/namespaceMetadata';

const nlsBundles = [
  esmoNamespacesNLS,
  escoListNLS,
  escoOverviewNLS,
  esmoCommonsNLS,
  escoDialogsNLS,
];

const NamespaceOverview = ({ overviewProps }) => {
  const namespaceEntry = useEntry();
  const translate = useTranslation(nlsBundles);
  const [{ refreshCount }] = useOverviewModel();
  const { contributors } = useGetContributors(namespaceEntry, refreshCount);
  const [pageTitle] = usePageTitle();
  const viewDefinition = getViewDefFromName('models__namespaces__namespace');
  const viewDefinitionTitle = viewDefinition.title[i18n.getLocale()];
  const { context } = useESContext();
  const { namespaceEntries, isLoading } = useGetNamespaceEntries(context);
  const defaultEntry = getDefaultEntry(namespaceEntries);
  const [, dispatchOverview] = useOverviewModel();
  const refresh = useCallback(() => {
    dispatchOverview({ type: REFRESH_OVERVIEW });
  }, [dispatchOverview]);

  const descriptionItems = [
    DESCRIPTION_UPDATED,
    {
      id: 'edited',
      labelNlsKey: 'editedByLabel',
      getValues: () => contributors,
    },
  ];

  useDocumentTitle(
    `${pageTitle} - ${viewDefinitionTitle} ${getLabel(namespaceEntry)}`
  );

  const sidebarActions = [
    {
      ...ACTION_EDIT,
      Dialog: NamespaceEditDialog,
      isVisible: ({ entry }) => !isBuiltin(entry),
      getProps: () => ({
        action: {
          onEntryEdit: refresh,
          nlsBundles,
        },
      }),
    },
    {
      id: 'setDefault',
      Dialog: withOverviewRefresh(NamespaceDefaultDialog, 'onConfirm'),
      labelNlsKey: 'setDefaultLabel',
      isVisible: ({ entry }) => !hasDefaultValue(entry),
      getProps: () => ({
        action: {
          currentDefaultEntry: defaultEntry,
        },
      }),
      disabled: isLoading,
    },
    {
      id: 'unsetDefault',
      Dialog: withOverviewRefresh(NamespaceDefaultDialog, 'onConfirm'),
      labelNlsKey: 'unsetDefaultLabel',
      isVisible: ({ entry }) => hasDefaultValue(entry),
      getProps: () => ({
        action: {
          currentDefaultEntry: defaultEntry,
        },
      }),
      disabled: isLoading,
    },
    {
      ...ACTION_REMOVE,
      Dialog: RemoveEntityDialog,
      action: {
        nlsBundles,
        parentViewName: NAMESPACES_LISTVIEW,
      },
    },
  ];

  return (
    <Overview
      {...overviewProps}
      backLabel={translate('backTitle')}
      headerAction={{
        ...ACTION_INFO_WITH_ICON,
        Dialog: ModelsLDBDialog,
      }}
      entry={namespaceEntry}
      nlsBundles={nlsBundles}
      descriptionItems={descriptionItems}
      sidebarActions={sidebarActions}
    />
  );
};

NamespaceOverview.propTypes = {
  overviewProps: overviewPropsPropType,
};

export default withOverviewModelProvider(NamespaceOverview);
