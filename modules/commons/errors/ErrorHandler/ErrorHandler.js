import { useEffect } from 'react';
import { ErrorDialog } from 'commons/errors/ErrorDialog';
import { useTranslation } from 'commons/hooks/useTranslation';
import escoErrors from 'commons/nls/escoErrors.nls';
import { useUserDispatch } from 'commons/hooks/useUser';
import { logout } from 'commons/util/user';
import { AUTH_LOGOUT_SUCCESS } from 'commons/hooks/user/actions';
import {
  CONFLICT_PROBLEM,
  GENERIC_PROBLEM,
  INPROGRESS,
  LOST_CONNECTION,
  NO_PROBLEM,
  UNAUTHORIZED,
} from '../utils/async';
import useEntrystoreError from '../hooks/useEntrystoreError';
import './ErrorHandler.scss';
import { logError } from '../utils/log';

const ERROR_STATUS_KEYS = {
  [UNAUTHORIZED]: 'unauthorizedError',
};

const useStatusText = (status, countdown) => {
  const translate = useTranslation(escoErrors);
  if (status === INPROGRESS) return translate('loading');
  if (status === LOST_CONNECTION)
    return translate('connectionLost', { countdown });
  if (status in ERROR_STATUS_KEYS) return translate(ERROR_STATUS_KEYS[status]);
  if (status === CONFLICT_PROBLEM) return translate('conflictProblem');
  return translate('genericProblem');
};

const useDetailsText = (error) => {
  if (!error) return null;
  return error.name ? `${error.name}: ${error.message}` : error.message;
};

const ErrorHandler = () => {
  const { error, status, countdown, clear } = useEntrystoreError();
  const translate = useTranslation(escoErrors);
  const statusText = useStatusText(status, countdown);
  const detailsText = useDetailsText(error);
  const userDispatch = useUserDispatch();
  // generic errors are excluded and should be handled on component level
  const ignoreError = status === NO_PROBLEM || status === GENERIC_PROBLEM;

  const closeHandler = (proceed) => {
    if (status === UNAUTHORIZED && proceed === true) {
      logout().then(({ userEntry, userInfo }) => {
        userDispatch({
          type: AUTH_LOGOUT_SUCCESS,
          data: {
            userEntry,
            userInfo,
          },
        });
      });
    }
    clear();
  };

  useEffect(() => {
    if (error) {
      logError(error);
      if (ignoreError) {
        // clear error state for generic errors
        clear();
      }
    }
  }, [error, clear, ignoreError]);

  if (ignoreError || status === INPROGRESS) return null;

  return (
    <ErrorDialog
      errorText={statusText}
      errorDetails={detailsText}
      closeLabel={translate(
        status === UNAUTHORIZED ? 'cancelLabel' : 'closeDialog'
      )}
      affirmLabel={
        status === UNAUTHORIZED ? translate('proceedToLoginLabel') : null
      }
      onClose={closeHandler}
      open={!ignoreError}
    />
  );
};

export default ErrorHandler;
