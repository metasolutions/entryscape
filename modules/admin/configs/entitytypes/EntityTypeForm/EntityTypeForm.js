import { useState } from 'react';
import PropTypes from 'prop-types';
import { Form } from 'commons/components/forms/editors';
import { Box, Button, Collapse, Divider } from '@mui/material';
import { Entry } from '@entryscape/entrystore-js';
import {
  nlsBundlesPropType,
  useTranslation,
} from 'commons/hooks/useTranslation';
import { editorFields, advancedEditorFields } from '../utils/editorFields';

const EntityTypeForm = ({ entityTypeEntries, nlsBundles }) => {
  const translate = useTranslation(nlsBundles);
  const [showAdvanced, setShowAdvanced] = useState(false);

  return (
    <>
      <Form fields={editorFields} size="small" nlsBundles={nlsBundles} />
      <Divider sx={{ width: '100%', marginTop: '48px', marginBottom: '8px' }} />
      <Box sx={{ marginBottom: '8px' }}>
        <Button
          variant="text"
          sx={{ paddingLeft: 0 }}
          onClick={() => setShowAdvanced(!showAdvanced)}
        >
          {showAdvanced
            ? translate('hideAdvancedLabel')
            : translate('showAdvancedLabel')}
        </Button>
      </Box>
      <Collapse in={showAdvanced} timeout="auto" unmountOnExit>
        <Form
          fields={advancedEditorFields}
          size="small"
          entityTypeEntries={entityTypeEntries}
          nlsBundles={nlsBundles}
        />
      </Collapse>
    </>
  );
};

EntityTypeForm.propTypes = {
  entityTypeEntries: PropTypes.arrayOf(PropTypes.instanceOf(Entry)),
  nlsBundles: nlsBundlesPropType,
};

export default EntityTypeForm;
