export const PROJECT_LISTVIEW = 'workbench__list';

export const PROJECT_OVERVIEW_NAME = 'workbench__overview';
export const ENTITY_OVERVIEW_NAME = 'workbench__entities__type__overview';

export default {
  itemstore: {
    /**
     * Classes that manage the listing/searching of values in rdforms selects.
     * The value of choosers is an array of strings,
     * the string refer to the filename in a specific path,
     * commons/rdforms/choosers
     * @type {string[]}
     * @memberof itemstore
     */
    choosers: ['EntryChooser', 'SkosChooser'],
  },
  workbench: {
    /**
     * @type {boolean}
     * @memberof workbench
     * Allow lists to apply mass operations on its elements. Currently only deletion supported
     */
    includeMassOperations: false,

    /**
     * Helper text which appears at the top of the project creation dialog.
     * It's shown only if there a non empty string in the active locale.
     * @typedef {{en: string, sv: string, de: string}} HelperTextObj
     * @type HelperTextObj
     * @memberof workbench
     */
    createHelperText: {
      en: '',
      sv: '',
      de: '',
    },

    /**
     * Empty string works as no limit
     * @type {string|number}
     */
    projectLimit: '',

    /**
     * A pointer to a text file in the configuration directory.
     * @type {string}
     */
    projectLimitDialog: '',
    linkedDataBrowserDialog: true,
  },

  entitytypes: [
    {
      name: 'project',
      label: {
        en: 'Project',
        sv: 'Projekt',
        de: 'Projekt',
      },
      rdfType: ['http://entryscape.com/terms/WorkbenchContext'],
      template: 'esc:Context',
      mainModule: 'workbench',
      module: 'workbench',
      overview: 'workbench__overview',
    },
  ],
};
