import { entrystore } from 'commons/store';
import { namespaces as ns } from '@entryscape/rdfjson';

const validate = (text) => {
  const re = /([A-Z]|\s)/g;
  return re.test(text);
};

/**
 * Searches for a given group name
 *
 * @param {string} groupName
 * @returns {string|null}
 */
const groupNameSearch = async (groupName) => {
  if (validate(groupName)) {
    return 'nameMalformed';
  }
  return entrystore
    .getREST()
    .get(
      `${entrystore.getBaseURI()}_principals?entryname=${groupName.toLowerCase()}`
    )
    .then((data) => {
      return data.length > 0 ? 'nameTaken' : null;
    });
};

/**
 * Searches for a given entryId
 *
 * @param {string} entryId
 * @returns {string|null}
 */
const entryIdSearch = async (entryId) => {
  if (validate(entryId)) {
    return 'entryIdMalformed';
  }
  const sq = await entrystore
    .newSolrQuery()
    .uri(`${entrystore.getBaseURI()}_principals/entry/${entryId}`)
    .limit(1)
    .list();

  return sq.getEntries().then((entries) => {
    return entries.length > 0 ? 'entryIdTaken' : null;
  });
};

const createGroupEntry = async ({ name, id, fullName }) => {
  let prototypeEntry;

  if (!id) {
    if (!name) {
      prototypeEntry = entrystore.newGroup();
    } else {
      prototypeEntry = entrystore.newGroup(name);
    }
  } else {
    const groupName = name || null;
    prototypeEntry = entrystore.newGroup(groupName, id);
  }

  if (prototypeEntry) {
    const md = prototypeEntry.getMetadata();
    md.add(prototypeEntry.getResourceURI(), ns.expand('foaf:name'), {
      type: 'literal',
      value: fullName,
    });
  }
  return prototypeEntry.commit();
};

export { groupNameSearch, entryIdSearch, createGroupEntry };
