import { visuallyHidden } from '@mui/utils';
import { Link as LinkIcon } from '@mui/icons-material';
import { Typography } from '@mui/material';
import Tooltip from 'commons/components/common/Tooltip';
import { useTranslation } from 'commons/hooks/useTranslation';
import esteCollectionNLS from 'terms/nls/esteCollection.nls';

const OtherStoreIcon = () => {
  const translate = useTranslation(esteCollectionNLS);

  return (
    <Tooltip title={translate('fromOtherStoreIconTooltip')}>
      <span style={{ display: 'flex' }}>
        <LinkIcon color="primary" />
        <Typography component="span" style={visuallyHidden}>
          {translate('fromOtherStoreIconTooltip')}
        </Typography>
      </span>
    </Tooltip>
  );
};

export default OtherStoreIcon;
