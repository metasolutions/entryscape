import {
  getFieldTemplate,
  generateRdfFormsId,
} from 'models/fields/utils/fields';
import {
  getNamespaceAbbreviation,
  getNamespaceUri,
} from 'models/namespaces/utils/namespaceMetadata';
import { RDF_PROPERTY_NAMESPACE_ENTRY } from 'models/utils/ns';
import { Context, Entry } from '@entryscape/entrystore-js';
import { getOrIgnoreEntry } from 'commons/util/entry';
import { getDependentContexts } from './dependencies';

/**
 * Load context entries, to make sure they are available in cache.
 *
 * @param {object} buildItems
 */
export const loadContextEntries = async (buildItems) => {
  const contexts = getDependentContexts(buildItems);
  for (const context of contexts) {
    await context.getEntry();
  }
};

/**
 * Generate rdforms id for each dependency item
 *
 * @param {object[]} buildItems
 * @returns {Map}
 */
export const mapResourceURIToId = (buildItems) => {
  const dependencyIdMap = new Map();
  const setDependency = (entry) => {
    dependencyIdMap.set(entry.getResourceURI(), generateRdfFormsId(entry));
  };
  buildItems.forEach(({ entry, items, inlineEntry }) => {
    if (items) items.forEach(setDependency);
    if (entry) setDependency(entry);
    if (inlineEntry) setDependency(inlineEntry);
  });
  return dependencyIdMap;
};

/**
 * Create rdforms template for each dependency item
 *
 * @param {object[]} buildItems
 * @param {Map} idMap
 * @param {Context[]} excludedContexts
 * @returns {Promise<object[]>}
 */
export const createRDFormsTemplates = async (
  buildItems,
  idMap,
  excludedContexts
) => {
  const templates = [];

  const createTemplate = async (entry) => {
    const resourceURI = entry.getResourceURI();
    const templateId = idMap.get(resourceURI);
    if (templates.find((template) => template.id === templateId)) return;
    if (excludedContexts.includes(entry.getContext())) return;
    const template = await getFieldTemplate(entry, idMap);
    templates.push(template);
    return template;
  };

  for (const { entry, items, inlineEntry } of buildItems) {
    if (items) {
      for (const item of items) {
        await createTemplate(item);
      }
    }
    if (entry) {
      if (inlineEntry) {
        const template = await createTemplate(inlineEntry);
        if (template) {
          const inlineFieldTemplate = await createTemplate(entry);
          inlineFieldTemplate.extends = template.id;
        }
      } else {
        await createTemplate(entry);
      }
    }
  }
  return templates;
};

/**
 * When referring to a custom namespace, the namespace entry property is used.
 * Thus this property can be used to find all instances where a custom namespace
 * is used in a field or form.
 *
 * @param {Entry} fieldOrFormEntry
 * @returns {string[]}
 */
export const findNamespaceEntryURIsUsedByEntry = (fieldOrFormEntry) => {
  const graph = fieldOrFormEntry.getMetadata();
  const namespaceEntryURIs = graph
    .find(null, RDF_PROPERTY_NAMESPACE_ENTRY)
    .map((statement) => statement.getValue());

  return Array.from(new Set(namespaceEntryURIs)); // remove duplicates
};

/**
 * Find custom namespaces that are used in the included build items. If custom
 * namespaces are used, the namespace definitions are extracted from the
 * entries.
 *
 * @param {object[]} buildItems
 * @param {Context[]} excludedContexts
 * @returns {Promise<object>}
 */
export const getIncludedNamespaces = async (buildItems, excludedContexts) => {
  // extract namespace entry uris
  const namespaceEntryURIsInUse = buildItems
    .map(({ entry }) => {
      return findNamespaceEntryURIsUsedByEntry(entry);
    })
    .flat();
  const namespaceEntryURIs = Array.from(new Set(namespaceEntryURIsInUse)); // remove duplicates

  // Get namespace entries from entry uris. The namespace entry is skipped if it
  // belongs to an excluded context.
  const namespaceEntries = [];
  for (const namespaceEntryURI of namespaceEntryURIs) {
    const namespaceEntry = await getOrIgnoreEntry(namespaceEntryURI);
    if (
      namespaceEntry &&
      !excludedContexts.includes(namespaceEntry.getContext())
    ) {
      namespaceEntries.push(namespaceEntry);
    }
  }

  // extract abbreviation/namespace uri pairs from namespace entries
  const namespaces = {};
  namespaceEntries.forEach((namespaceEntry) => {
    const abbreviation = getNamespaceAbbreviation(namespaceEntry);
    const uri = getNamespaceUri(namespaceEntry);
    namespaces[abbreviation] = uri;
  });
  return namespaces;
};

/**
 * Excludes items form given contexts
 *
 * @param {object[]} buildItems
 * @param {Context[]} excludedContexts
 * @returns {object[]}
 */
export const getIncludedBuildItems = (buildItems, excludedContexts) => {
  return buildItems.filter(({ entry }) => {
    const context = entry.getContext();
    return !excludedContexts.includes(context);
  });
};

/**
 * Generates a template file compatible with rdforms
 *
 * @param {object[]} buildItems
 * @param {Context[]} excludedContexts
 * @returns {Promise<object>}
 */
export const generateJsonBuild = async (buildItems, excludedContexts = []) => {
  const includedBuildItems = excludedContexts
    ? getIncludedBuildItems(buildItems, excludedContexts)
    : buildItems;
  await loadContextEntries(buildItems); // required to generate rdforms ids based on context name
  const resourceURIToIdMap = mapResourceURIToId(buildItems);
  const templates = await createRDFormsTemplates(
    includedBuildItems,
    resourceURIToIdMap,
    excludedContexts
  );

  const namespaces = await getIncludedNamespaces(
    includedBuildItems,
    excludedContexts
  );
  return { templates, namespaces };
};
