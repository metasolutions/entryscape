/**
 * @param {number} milliseconds
 * @return {promise}
 */
const sleep = (milliseconds) =>
  new Promise((resolve) => setTimeout(resolve, milliseconds));

export default sleep;
