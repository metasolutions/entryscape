import { withListRefresh } from 'commons/components/ListView';
import escaIdeasNLS from 'catalog/nls/escaIdeas.nls';
import escoListNLS from 'commons/nls/escoList.nls';
import CreateIdeaDialog from '../dialogs/CreateIdeaDialog';

const nlsBundles = [escaIdeasNLS, escoListNLS];

const listActions = [
  {
    id: 'create',
    Dialog: withListRefresh(CreateIdeaDialog, 'onCreate'),
    labelNlsKey: 'createButtonLabel',
    tooltipNlsKey: 'actionTooltip',
    highlightId: 'listActionCreate',
  },
];

export { listActions, nlsBundles };
