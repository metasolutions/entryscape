import PropTypes from 'prop-types';
import { CircularProgress, Box } from '@mui/material';
import './Loader.scss';

const Loader = ({ height = '45vh', sx = {} }) => (
  <Box className="escoLoader" sx={{ height, ...sx }}>
    <CircularProgress />
  </Box>
);

Loader.propTypes = {
  sx: PropTypes.shape({}),
  height: PropTypes.string,
};

export default Loader;
