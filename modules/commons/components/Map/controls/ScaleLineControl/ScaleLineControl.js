import PropTypes from 'prop-types';
import { useOpenlayers } from '../../hooks/useOpenlayers';
import useBuiltinControl from '../../hooks/useBuiltinControl';
import './ScaleLineControl.scss';

const ScaleLineControl = ({ target }) => {
  const { createScaleLine } = useOpenlayers();
  useBuiltinControl(createScaleLine, target);
  return null;
};

ScaleLineControl.propTypes = {
  target: PropTypes.shape({}),
};

export default ScaleLineControl;
