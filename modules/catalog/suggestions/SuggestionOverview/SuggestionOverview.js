import { useEntry } from 'commons/hooks/useEntry';
import { getLabel, getFullLengthLabel } from 'commons/util/rdfUtils';
import { truncate } from 'commons/util/util';
import { getParentCatalogEntry } from 'commons/util/metadata';
import { getViewDefFromName } from 'commons/util/site';
import escaVisualizationNLS from 'catalog/nls/escaVisualization.nls';
import escaCatalogNLS from 'catalog/nls/escaCatalog.nls';
import escaPreparationsNLS from 'catalog/nls/escaPreparations.nls';
import escoListNLS from 'commons/nls/escoList.nls';
import useAsyncCallback from 'commons/hooks/useAsyncCallback';
import useDocumentTitle from 'commons/hooks/useDocumentTitle';
import usePageTitle from 'commons/hooks/usePageTitle';
import useGetContributors from 'commons/hooks/useGetContributors';
import escoOverviewNLS from 'commons/nls/escoOverview.nls';
import escoEntityOverviewNLS from 'commons/nls/escoEntityOverview.nls';
import { DESCRIPTION_UPDATED } from 'commons/components/overview/actions';
import {
  withOverviewModelProvider,
  overviewPropsPropType,
  useOverviewModel,
} from 'commons/components/overview';
import { localize } from 'commons/locale';
import useCommentsCount from 'commons/CommentsDialog/hooks/useCommentsCount';
import CommentsDialog from 'commons/CommentsDialog';
import EntityOverview from 'commons/components/EntityOverview';
import CreateDatasetFromSuggestionDialog from '../dialogs/CreateDatasetFromSuggestionDialog';
import { isArchived } from '../util';
import { sidebarActions, datasetActions } from './actions';
import { TEMPLATE_TAG } from '../../datasets/tags';
import { ARCHIVED_TAG } from '../tags';

const nlsBundles = [
  escaCatalogNLS,
  escoListNLS,
  escaVisualizationNLS,
  escaPreparationsNLS,
  escoOverviewNLS,
  escoEntityOverviewNLS,
];

const SuggestionOverview = ({ overviewProps }) => {
  const entry = useEntry();
  const [catalogEntry] = useAsyncCallback(getParentCatalogEntry, entry);
  const [{ refreshCount }] = useOverviewModel();
  const { contributors } = useGetContributors(entry, refreshCount);
  const [pageTitle] = usePageTitle();
  const viewDefinition = getViewDefFromName('catalog__suggestions__suggestion');
  const viewDefinitionTitle = localize(viewDefinition.title);
  const [getCommentsCount, refreshCommentsCount] = useCommentsCount(entry);
  const commentCount = getCommentsCount(entry.getResourceURI());

  const tileActions = [
    {
      id: 'comments',
      Dialog: CommentsDialog,
      refreshCommentsCount,
      getProps: ({ translate: t }) => ({
        labelPrefix: commentCount ?? 0,
        label: t('commentsLabel'),
        'data-highlight-id': 'overviewButtonComments',
      }),
    },
  ];

  const descriptionItems = [
    {
      id: 'catalog',
      labelNlsKey: 'overviewCatalogLabel',
      getValues: () =>
        catalogEntry ? [truncate(getFullLengthLabel(catalogEntry), 90)] : [],
    },
    DESCRIPTION_UPDATED,
    {
      id: 'edited',
      labelNlsKey: 'editedByLabel',
      getValues: () => contributors,
    },
  ];

  useDocumentTitle(`${pageTitle} - ${viewDefinitionTitle} ${getLabel(entry)}`);

  const updatedOverviewProps = {
    ...overviewProps,
    tags: [TEMPLATE_TAG, ARCHIVED_TAG],
  };

  return (
    <EntityOverview
      actions={sidebarActions}
      nlsBundles={nlsBundles}
      getAggregationListProps={() => ({
        actions: datasetActions,
        to: 'catalog__datasets__dataset',
        getCreateProps: ({ entry: suggestionEntry }) => ({
          CreateDialog: CreateDatasetFromSuggestionDialog,
          createDisabled: isArchived(suggestionEntry),
          createHighlightId: 'suggestionsCreateDatasetButton',
        }),
      })}
      metadataItems={descriptionItems}
      tileActions={tileActions}
      overviewProps={updatedOverviewProps}
    />
  );
};

SuggestionOverview.propTypes = {
  overviewProps: overviewPropsPropType,
};

export default withOverviewModelProvider(SuggestionOverview);
