import { useEffect } from 'react';
import { chunk, uniq } from 'lodash-es';
import { getPathFromViewName } from 'commons/util/site';
import useAsync from 'commons/hooks/useAsync';
import { entrystore, entrystoreUtil } from 'commons/store';
import {
  RDF_TYPE_DISTRIBUTION,
  RDF_TYPE_DATASET,
  findResourceStatements,
} from 'commons/util/entry';
import escaDataset from 'catalog/nls/escaDataset.nls';
import escaCatalogSearch from 'catalog/nls/escaCatalogSearch.nls';
import escoListNLS from 'commons/nls/escoList.nls';
import {
  useListQuery,
  withListModelProvider,
} from 'commons/components/ListView';
import {
  EntryListView,
  TITLE_COLUMN,
  MODIFIED_COLUMN,
  ACTIONS_GROUP_COLUMN,
} from 'commons/components/EntryListView';
import {
  LIST_ACTION_EDIT,
  LIST_ACTION_INFO,
} from 'commons/components/EntryListView/actions';
import { useESContext } from 'commons/hooks/useESContext';
import useHandleError from 'commons/errors/hooks/useErrorHandler';
import { useEntry } from 'commons/hooks/useEntry';
import { useTranslation } from 'commons/hooks/useTranslation';
import { OverviewListHeader } from 'commons/components/overview/OverviewListHeader';
import {
  OverviewListPlaceholder,
  useOverviewModel,
} from 'commons/components/overview';

const nlsBundles = [escaDataset, escaCatalogSearch, escoListNLS];

const SEARCH_FIELDS = ['label'];

/**
 * get all entries typed as distribution
 * and with the dcat:accessService property pointing to the dataservice,
 * and within the same context
 *
 * @param {object} dataserviceEntry
 * @returns {Promise<Array>} distributionEntries
 */
const getDistributionEntries = (dataserviceEntry) => {
  const context = dataserviceEntry.getContext();
  const resourceURI = dataserviceEntry.getResourceURI();
  const distributionEntries = entrystore
    .newSolrQuery()
    .rdfType(RDF_TYPE_DISTRIBUTION)
    .context(context)
    .uriProperty('dcat:accessService', resourceURI)
    .list()
    .getEntries();

  return distributionEntries;
};

/**
 * @param {object} dataserviceEntry
 * @returns {Promise<Array>} datasetEntries
 */
const getRelatedDatasets = (dataserviceEntry) => {
  const statements = findResourceStatements(
    dataserviceEntry,
    'dcat:servesDataset'
  );
  const servesDatasetURIs = statements.map((statement) => statement.getValue());

  if (!servesDatasetURIs.length) return [];
  return entrystoreUtil.loadEntriesByResourceURIs(
    servesDatasetURIs,
    dataserviceEntry.getContext()
  );
};

/**
 * @param {object} dataserviceEntry
 * @returns {Promise} datasetEntries
 */
const getDatasetEntries = async (dataserviceEntry) => {
  const context = dataserviceEntry.getContext();
  const distributions = await getDistributionEntries(dataserviceEntry);

  const batchSize = 50;
  const datasetEntries = [];

  if (distributions.length > 50) {
    return { entries: [], exceededLimit: true };
  }

  const batchDatasetEntries = chunk(distributions, batchSize);

  const batchEntriesPromises = batchDatasetEntries.map((batch) => {
    const batchUris = batch.map((entry) => entry.getResourceURI());
    return entrystore
      .newSolrQuery()
      .rdfType(RDF_TYPE_DATASET)
      .context(context)
      .uriProperty('dcat:distribution', batchUris)
      .list()
      .getEntries();
  });

  const batchEntries = await Promise.all(batchEntriesPromises);
  datasetEntries.push(...batchEntries.flat());

  const relatedDatasets = await getRelatedDatasets(dataserviceEntry);
  const allLinkedDatasets = [...datasetEntries, ...relatedDatasets];

  return {
    entries: uniq(allLinkedDatasets).map((entry) => ({ entry })),
    exceededLimit: false,
  };
};

const DatasetList = () => {
  const dataserviceEntry = useEntry();
  const { context } = useESContext();
  const translate = useTranslation(nlsBundles);
  const { runAsync, data: datasetItems, error, status } = useAsync();
  const [{ refreshCount }] = useOverviewModel();

  useHandleError(error);

  useEffect(() => {
    runAsync(getDatasetEntries(dataserviceEntry));
  }, [runAsync, dataserviceEntry, refreshCount]);

  const listQueries = useListQuery({
    items: datasetItems && datasetItems.entries,
    searchFields: SEARCH_FIELDS,
  });

  const entries = listQueries?.result.map(({ entry }) => entry);
  const reshapedListQueries = {
    ...listQueries,
    entries,
    exceededLimit: datasetItems && datasetItems.exceededLimit,
  };

  return (
    <>
      <OverviewListHeader
        heading={translate('datasetsHeading')}
        nlsBundles={nlsBundles}
      />
      <EntryListView
        {...reshapedListQueries}
        status={status}
        nlsBundles={nlsBundles}
        getListItemProps={({ entry }) => ({
          to: getPathFromViewName('catalog__datasets__dataset', {
            contextId: context.getId(),
            entryId: entry.getId(),
          }),
        })}
        includeDefaultListActions={false}
        includeHeader={false}
        renderViewPlaceholder={() => (
          <OverviewListPlaceholder
            label={
              datasetItems && datasetItems.exceededLimit
                ? translate('tooManyConnectedDatasets')
                : translate('datasetsListPlaceholder')
            }
          />
        )}
        columns={[
          {
            ...TITLE_COLUMN,
            xs: 8,
          },
          MODIFIED_COLUMN,
          {
            ...ACTIONS_GROUP_COLUMN,
            actions: [LIST_ACTION_INFO, LIST_ACTION_EDIT],
          },
        ]}
      />
    </>
  );
};

export default withListModelProvider(DatasetList);
