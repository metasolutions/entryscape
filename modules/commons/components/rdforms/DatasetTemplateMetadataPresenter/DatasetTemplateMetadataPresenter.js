import PropTypes from 'prop-types';
import { Entry } from '@entryscape/entrystore-js';
import useRDFormsPresenter from 'commons/components/rdforms/hooks/useRDFormsPresenter';
import escaDatasetTemplateNLS from 'catalog/nls/escaDatasetTemplate.nls';
import ExpandButton from 'commons/components/buttons/ExpandButton';

const DatasetTemplateMetadataPresenter = ({
  entry,
  externalTemplateId,
  ...rest
}) => {
  const metadata = entry.getMetadata();
  const resourceURI = entry.getResourceURI();
  const { PresenterComponent } = useRDFormsPresenter(
    metadata,
    resourceURI,
    externalTemplateId
  );

  return (
    <ExpandButton
      PresenterComponent={PresenterComponent}
      nlsBundles={[escaDatasetTemplateNLS]}
      {...rest}
    />
  );
};

DatasetTemplateMetadataPresenter.propTypes = {
  entry: PropTypes.instanceOf(Entry),
  externalTemplateId: PropTypes.string,
};

export default DatasetTemplateMetadataPresenter;
