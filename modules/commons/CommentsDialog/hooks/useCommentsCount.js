import { useCallback, useEffect, useState } from 'react';
import useAsync from 'commons/hooks/useAsync';
import useErrorHandler from 'commons/errors/hooks/useErrorHandler';
import { fetchComments } from '../util';

const useCommentsCount = (entryOrEntries) => {
  const {
    data: commentsCountByEntry,
    error,
    runAsync,
  } = useAsync({
    data: {},
  });
  const [refreshNumber, setRefreshNumber] = useState();

  useErrorHandler(error);

  const getCommentsCount = useCallback(
    (entryURI) => {
      if (!(entryURI in commentsCountByEntry)) return null;
      return commentsCountByEntry[entryURI];
    },
    [commentsCountByEntry]
  );

  const refreshCommentsCount = useCallback(() => {
    setRefreshNumber(Math.random());
  }, []);

  useEffect(() => {
    if (!entryOrEntries) return;
    const entries = Array.isArray(entryOrEntries)
      ? entryOrEntries
      : [entryOrEntries];
    runAsync(fetchComments(entries));
  }, [entryOrEntries, runAsync, refreshNumber]);

  return [getCommentsCount, refreshCommentsCount];
};

export default useCommentsCount;
