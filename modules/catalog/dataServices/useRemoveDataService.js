import { useCallback } from 'react';
import { useTranslation } from 'commons/hooks/useTranslation';
import escaDatasetNLS from 'catalog/nls/escaDataset.nls';
import escaServicesNLS from 'catalog/nls/escaServices.nls';
import useGetMainDialog from 'commons/hooks/useGetMainDialog';
import { entrystoreUtil } from 'commons/store';
import { renderHtmlString } from 'commons/util/reactUtils';
import {
  getReferences,
  getLabels,
} from 'commons/hooks/references/useGetReferences';
import escaReferencesNLS from 'catalog/nls/escaReferences.nls';
import { RDF_TYPE_DATASERVICE } from 'commons/util/entry';
import { isCatalog } from 'catalog/utils/react/catalog';

const useRemoveDataService = (dataserviceEntry) => {
  const translate = useTranslation([
    escaDatasetNLS,
    escaReferencesNLS,
    escaServicesNLS,
  ]);
  const { getAcknowledgeDialog } = useGetMainDialog();

  /**
   *  Removes the actual dataservices.
   *
   *  @returns {Promise<void>}
   */
  const removeDataService = useCallback(async () => {
    await dataserviceEntry.del();

    const resourceURI = dataserviceEntry.getResourceURI();
    const catalog = await entrystoreUtil.getEntryByType(
      'dcat:Catalog',
      dataserviceEntry.getContext()
    );

    catalog.getMetadata().findAndRemove(null, RDF_TYPE_DATASERVICE, {
      value: resourceURI,
      type: 'uri',
    });
    return catalog.commitMetadata();
  }, [dataserviceEntry]);

  /**
   * Checks for dataservices and references are presented,
   * proceed with the actual data service removal.
   *
   * @returns {Promise<boolean>}
   */
  const checkHasReference = useCallback(async () => {
    const references = await getReferences(dataserviceEntry, isCatalog);
    const labels = await getLabels(references, translate);
    if (references.length) {
      await getAcknowledgeDialog({
        content: renderHtmlString(
          translate(
            'unableToRemoveOther',
            `<ul>${labels.map((label) => `<li>${label}</li>`).join('')}</ul>`
          )
        ),
      });
      return false;
    }

    return true;
  }, [dataserviceEntry, getAcknowledgeDialog, translate]);

  return [checkHasReference, removeDataService];
};

export default useRemoveDataService;
