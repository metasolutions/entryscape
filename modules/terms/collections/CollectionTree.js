import Tree from 'commons/tree/Tree';
import skosUtil from 'commons/tree/skos/util';
import CollectionTreeModel from './CollectionTreeModel';

export default class extends Tree {
  showEntry(conceptSchemeEntry, collectionEntry, checkChange) {
    if (this.model) {
      this.model.destroy();
    }

    this.model = new CollectionTreeModel({
      ...skosUtil.getSemanticProperties(),
      rootEntry: conceptSchemeEntry,
      domNode: this.domNode,
      collectionEntry,
      checkChange,
    });
  }
}
