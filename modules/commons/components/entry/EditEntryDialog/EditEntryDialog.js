import PropTypes from 'prop-types';
import React, { useState, useEffect, useCallback, useMemo } from 'react';
import { Entry } from '@entryscape/entrystore-js';
import { validate } from '@entryscape/rdforms';
import { Stack } from '@mui/material';
import escoListNLS from 'commons/nls/escoList.nls';
import escoErrorsNLS from 'commons/nls/escoErrors.nls';
import { useTranslation } from 'commons/hooks/useTranslation';
import {
  setEntryDraftStatus,
  isNewEntry,
  saveEntryByEntityType,
  refreshEntry,
} from 'commons/util/entry';
import Lookup from 'commons/types/Lookup';
import LevelSelector, {
  LEVEL_RECOMMENDED,
} from 'commons/components/rdforms/LevelSelector';
import ListActionDialog from 'commons/components/common/dialogs/ListActionDialog';
import useRDFormsEditor from 'commons/components/rdforms/hooks/useRDFormsEditor';
import { useConfirmCloseAction } from 'commons/components/EntryListView/dialogs/create/handlers';
import ExternalMetadataPresenter from 'commons/components/rdforms/ExternalMetadataPresenter';
// eslint-disable-next-line max-len
import DatasetTemplateMetadataPresenter from 'commons/components/rdforms/DatasetTemplateMetadataPresenter';
import 'commons/components/rdforms/EntryChooser';
import useConflictError from 'commons/errors/hooks/useConflictError';
import {
  useRdformsContext,
  withRdformsContextProvider,
} from 'commons/hooks/useRdformsContext';
import useGetDatasetTemplateEntry from 'catalog/datasetTemplates/useGetDatasetTemplateEntry';
import { getLocalMetadata } from 'commons/util/rdfUtils';
import useLevelProfile from 'commons/components/rdforms/hooks/useLevelProfile';
import {
  RdformsDialogFormWrapper,
  RdformsStickyDialogContent,
  RdformsDialogContent,
} from 'commons/components/rdforms/RdformsDialogFormWrapper';
import DialogTwoColumnLayout, {
  PrimaryColumn,
  SecondaryColumn,
} from 'commons/components/common/dialogs/DialogTwoColumnLayout';
import RdformsOutline from 'commons/components/rdforms/RdformsOutline';
// eslint-disable-next-line max-len
import useRdformsOutlineSearch from 'commons/components/rdforms/RdformsOutline/useRdformsOutlineSearch';
import ProfileChooser from 'commons/types/ProfileChooser';
import useGetEntityTypes from 'commons/types/useGetEntityTypes';
import useAsync, { PENDING } from 'commons/hooks/useAsync';
import { CONFLICT_PROBLEM } from 'commons/errors/utils/async';
import useAddIgnore from 'commons/errors/hooks/useAddIgnore';
import { ErrorCatcher } from 'commons/errors/ErrorCatcher';
import { ErrorWithMessage } from 'commons/errors/utils/errors';
import { useFormErrorMessage } from 'commons/components/rdforms/hooks/useRDFormsValidation';
import { useSnackbar, SUCCESS_EDIT } from 'commons/hooks/useSnackbar';
import LoadingButton from 'commons/components/LoadingButton';
import useGetMainDialog from 'commons/hooks/useGetMainDialog';
import { getDraftsEnabled } from 'commons/types/utils/entityType';

const NLS_BUNDLES = [escoListNLS, escoErrorsNLS];

// TODO nls 'yes' and 'no' and defaults
const EditEntryDialog = ({
  // fixed props - passed by (list) SecondaryActions
  entry,
  parentEntry,
  closeDialog,
  onChange,

  // dynamic props - to be set per edit dialog
  formTemplateId,
  formExternalTemplateId,
  onEntryEdit,
  editorLevel = LEVEL_RECOMMENDED,
  entryUpdatedCallback = () => {},
  filterPredicates,
  getFilterPredicates,
}) => {
  const [templateId, setTemplateId] = useState(formTemplateId);
  const [externalTemplateId, setExternalTemplateId] = useState(
    formExternalTemplateId
  );
  const [selectedEntityType, setSelectedEntityType] = useState(null);
  const draftsEnabled = getDraftsEnabled(selectedEntityType);
  const [isEntityTypeModified, setIsEntityTypeModified] = useState(false);
  const [level, setLevel] = useState(editorLevel);
  const [formError, setFormError] = useState(null);
  const [configError, setConfigError] = useState();
  const [errors, setErrors] = useState([]);
  const {
    runAsync: runSaveChanges,
    error: saveError,
    status: saveStatus,
  } = useAsync();
  const rdformsEditorFilter = useMemo(() => {
    if (getFilterPredicates) return getFilterPredicates(selectedEntityType);
    return filterPredicates;
  }, [selectedEntityType, filterPredicates, getFilterPredicates]);
  const outlineSearchProps = useRdformsOutlineSearch();
  const { editor, EditorComponent, graph } = useRDFormsEditor({
    entry,
    templateId,
    includeLevel: level,
    filterPredicates: rdformsEditorFilter,
  });
  const disabledLevels = useLevelProfile(templateId);
  const { entityTypes } = useGetEntityTypes(entry);
  const showProfileChooser =
    entityTypes.length > 1 && !formTemplateId && selectedEntityType;
  const isPublicEntry = entry.isPublic();

  const [buttonDisabled, setButtonDisabled] = useState(true);
  const getFormErrorMessage = useFormErrorMessage();
  const [addSnackbar] = useSnackbar();

  const confirmCloseAction = useConfirmCloseAction(closeDialog);
  const confirmClose = useCallback(() => {
    confirmCloseAction(!buttonDisabled);
  }, [buttonDisabled, confirmCloseAction]);
  const { setRdformsContext } = useRdformsContext();
  const translate = useTranslation(NLS_BUNDLES);
  const { handleConflictError } = useConflictError({
    entry,
    closeDialog,
  });
  const { getConfirmationDialog, getAcknowledgeDialog } = useGetMainDialog();

  useAddIgnore('commitMetadata', CONFLICT_PROBLEM, false);

  const [datasetTemplateEntry] = useGetDatasetTemplateEntry(entry);

  useEffect(() => {
    setRdformsContext(entry.getContext());
  }, [entry, setRdformsContext]);

  // in edit mode the entry is refreshed (fetched from entrystore)
  // before any edit action, so an update on the list row state is also needed
  useEffect(() => {
    if (entryUpdatedCallback && editor && !isNewEntry(entry)) {
      entryUpdatedCallback();
    }
  }, [editor, entry, entryUpdatedCallback]);

  // enable action button when there's a change in the metadata
  useEffect(() => {
    if (graph) {
      graph.onChange = () => {
        setButtonDisabled(false);
        setFormError(null);
      };
    }
  }, [graph]);

  useEffect(() => {
    if (!formTemplateId) {
      Lookup.inUse(entry, parentEntry).then(
        (entityType) => {
          const template = entry.isLinkReference()
            ? entityType.complementaryTemplateId()
            : entityType.templateId();
          setTemplateId(template);
          setSelectedEntityType(entityType);
          setExternalTemplateId(entityType.externalTemplateId());

          const templateLevel = entityType.templateLevel();
          if (templateLevel) setLevel(templateLevel);
        },
        (error) => {
          setConfigError(translate('configurationError'));
          console.error(error.message);
        }
      );
    }
  }, [entry, parentEntry, formTemplateId, translate]);

  const onChangeEntityType = (entityType) => {
    setSelectedEntityType(entityType);
    setTemplateId(entityType.templateId());
    setIsEntityTypeModified(true);
    setButtonDisabled(false);
  };

  const handleSaveForm = async () => {
    const { errors: validationErrors } = validate.bindingReport(editor.binding);
    let saveAsDraft = false;

    if (
      draftsEnabled &&
      validationErrors.length > 0 &&
      validationErrors.every((error) => error.code === 'min')
    ) {
      if (isPublicEntry) {
        getAcknowledgeDialog({
          content: translate('publicDraftSaveError'),
        });
        return;
      }

      const proceed = await getConfirmationDialog({
        content: translate('confirmDraftSave'),
        rejectLabel: translate('no'),
        affirmLabel: translate('yes'),
      });
      if (!proceed) return;
      saveAsDraft = true;
    }

    if (!saveAsDraft && validationErrors.length > 0) {
      const errorMessage = getFormErrorMessage(validationErrors);
      setFormError(errorMessage);
      setErrors(validationErrors);
      outlineSearchProps.clearSearch();
      return;
    }

    const updateEntityType =
      templateId && isEntityTypeModified
        ? () => saveEntryByEntityType(entry, selectedEntityType)
        : () => Promise.resolve(); // noop

    // run after metadata has been updated
    const afterMetadataUpdate = async () => {
      await setEntryDraftStatus(entry, saveAsDraft, false);
      await refreshEntry(entry);
      return onEntryEdit ? onEntryEdit(entry) : Promise.resolve();
    };

    // If needed remove statements from cached external graph before saving.
    const localGraph = getLocalMetadata(graph, entry);

    const saveChanges = async () => {
      await entry
        .setMetadata(localGraph)
        .commitMetadata()
        .then(updateEntityType)
        .catch((error) => {
          handleConflictError(error);
          throw new ErrorWithMessage(translate('saveEditsFail'), error);
        });
    };

    const saveDraft = () => {
      setEntryDraftStatus(entry, saveAsDraft, false);
    };

    runSaveChanges(
      saveChanges()
        .then(saveDraft)
        .then(afterMetadataUpdate)
        .then(entryUpdatedCallback)
        .then(closeDialog)
        .then(onChange)
        .then(() => addSnackbar({ type: SUCCESS_EDIT }))
    );
  };

  const actions = (
    <LoadingButton
      autoFocus
      onClick={handleSaveForm}
      loading={saveStatus === PENDING}
      disabled={buttonDisabled}
    >
      {translate('saveEditedEntry')}
    </LoadingButton>
  );

  const getAlertProps = () => {
    if (formError) {
      return { message: formError, setMessage: setFormError };
    }
    if (configError) {
      return { message: configError };
    }
  };

  return (
    <>
      <ListActionDialog
        closeDialog={confirmClose}
        id="edit-entry-dialog"
        actions={actions}
        title={translate('editHeader')}
        alert={getAlertProps()}
        fixedHeight
      >
        {!configError ? (
          <DialogTwoColumnLayout>
            <SecondaryColumn>
              <RdformsOutline
                editor={editor}
                root="edit-entry-dialog"
                errors={errors}
                searchProps={outlineSearchProps}
                filterPredicates={rdformsEditorFilter}
              />
            </SecondaryColumn>
            <PrimaryColumn>
              <RdformsDialogFormWrapper>
                <RdformsStickyDialogContent>
                  <Stack direction="row" spacing={2}>
                    <LevelSelector
                      level={level}
                      onUpdateLevel={setLevel}
                      disabledLevels={disabledLevels}
                    />
                    {showProfileChooser ? (
                      <ProfileChooser
                        entityTypes={entityTypes}
                        selectedEntityType={selectedEntityType}
                        onChangeEntityType={onChangeEntityType}
                      />
                    ) : null}
                  </Stack>
                </RdformsStickyDialogContent>
                <RdformsDialogContent>
                  {externalTemplateId && entry.isLinkReference() && (
                    <ExternalMetadataPresenter
                      entry={entry}
                      externalTemplateId={externalTemplateId}
                    />
                  )}
                  {templateId && datasetTemplateEntry ? (
                    <DatasetTemplateMetadataPresenter
                      entry={datasetTemplateEntry}
                      externalTemplateId={templateId}
                    />
                  ) : null}
                  {editor && <EditorComponent />}
                </RdformsDialogContent>
              </RdformsDialogFormWrapper>
            </PrimaryColumn>
          </DialogTwoColumnLayout>
        ) : null}
      </ListActionDialog>
      <ErrorCatcher error={saveError} />
    </>
  );
};

EditEntryDialog.propTypes = {
  entry: PropTypes.instanceOf(Entry),
  parentEntry: PropTypes.instanceOf(Entry),
  closeDialog: PropTypes.func,
  onChange: PropTypes.func,
  formTemplateId: PropTypes.string,
  formExternalTemplateId: PropTypes.string,
  onEntryEdit: PropTypes.func,
  editorLevel: PropTypes.string,
  entryUpdatedCallback: PropTypes.func,
  filterPredicates: PropTypes.shape(),
  getFilterPredicates: PropTypes.func,
};

export default withRdformsContextProvider(EditEntryDialog);
