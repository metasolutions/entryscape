import PropTypes from 'prop-types';
import {
  Route,
  useLocation,
  Navigate,
  createBrowserRouter,
  createRoutesFromElements,
  RouterProvider,
} from 'react-router-dom';
import React, { useState, useEffect } from 'react';
import { useUserState } from 'commons/hooks/useUser';
import { getActiveViews } from 'commons/util/config';
import { useESContext } from 'commons/hooks/useESContext';
import useDocumentTitle from 'commons/hooks/useDocumentTitle';
import { useLanguage } from 'commons/hooks/useLanguage';
import AppProviders from 'commons/providers/AppProviders';
import ViewProviders from 'commons/providers/ViewProviders';
import ParameterizedViewProviders from 'commons/providers/ParameterizedViewProviders';
import usePageTitle from 'commons/hooks/usePageTitle';
import { PageTitleContext } from 'commons/contexts';
import { Layout, PrimaryNavContext } from 'commons/Layout';
import { getLabel } from 'commons/util/rdfUtils';
import { localize } from 'commons/locale';
import {
  getNrOfParamsInRoute,
  getPathFromViewName,
  checkHasViewPermission,
} from 'commons/util/site';
import {
  CONTEXT_ENTRY_ID_PRINCIPALS,
  USER_ENTRY_ID_GUEST,
} from 'commons/util/userIds';
import ErrorHandler from 'commons/errors/ErrorHandler';
import config from 'config';
import registry from 'commons/registry';
import useHandleError from 'commons/errors/hooks/useErrorHandler';
import PermissionView from 'commons/view/Permission';
import useUserPermission from 'commons/hooks/useUserPermission';
import ErrorFallbackComponent from 'commons/errors/ErrorFallbackComponent';
import ErrorBoundary from 'commons/errors/ErrorBoundary';
import { getModuleFromName } from 'commons/util/module';
import { overviewPropsPropType } from 'commons/components/overview';
import { listPropsPropType } from 'commons/components/ListView';

const ContextAwareViewWrapper = ({
  View,
  name,
  title,
  listProps,
  overviewProps,
}) => {
  const { contextEntry, error } = useESContext();
  const [pageTitle, setPageTitle] = usePageTitle();
  const [language] = useLanguage();
  useHandleError(error);

  useEffect(() => {
    const contextEntryLabel = getLabel(contextEntry);
    if (
      contextEntryLabel === CONTEXT_ENTRY_ID_PRINCIPALS ||
      (contextEntryLabel === '' && name.startsWith('admin__project'))
    ) {
      setPageTitle(title);
      return;
    }

    if (
      (contextEntryLabel && contextEntryLabel !== pageTitle) ||
      name === 'catalog__dataset__search'
    ) {
      setPageTitle(contextEntryLabel);
    }
  }, [contextEntry, name, pageTitle, setPageTitle, title, language]);

  // prevent views from mounting twice
  // Todo Rethink! See comment below.
  // Added !contextEntry.getMetadata() so users with no read rights on catalogue
  //  metadata could see dataset preview.
  if (!pageTitle && contextEntry && !contextEntry.getMetadata() === null)
    return null;

  return (
    <View
      default
      name={name}
      listProps={listProps}
      overviewProps={overviewProps}
    />
  );
};

ContextAwareViewWrapper.propTypes = {
  View: PropTypes.func,
  name: PropTypes.string,
  title: PropTypes.string,
  listProps: listPropsPropType,
  overviewProps: overviewPropsPropType,
};

const ParameterizedView = ({
  View,
  name,
  title,
  listProps,
  overviewProps,
  ...rest
}) => {
  useHandleError(registry.getError());

  return (
    <ParameterizedViewProviders {...rest}>
      <ContextAwareViewWrapper
        default
        View={View}
        name={name}
        title={title}
        listProps={listProps}
        overviewProps={overviewProps}
      />
    </ParameterizedViewProviders>
  );
};

ParameterizedView.propTypes = {
  nrOfParamsInRoute: PropTypes.number,
  path: PropTypes.string,
  View: PropTypes.func,
  name: PropTypes.string,
  title: PropTypes.string,
  listProps: listPropsPropType,
  overviewProps: overviewPropsPropType,
};

// TODO rename
const SimpleView = ({ View, name, title, listProps, overviewProps }) => {
  const [pageTitle, setPageTitle] = usePageTitle();
  const [language] = useLanguage();
  useHandleError(registry.getError());

  useEffect(() => {
    setPageTitle(localize(title));
  }, [setPageTitle, title, language]);

  // prevent views from mounting twice
  if (!pageTitle) return null;

  return (
    <View
      key={name}
      name={name}
      listProps={listProps}
      overviewProps={overviewProps}
    />
  );
};

SimpleView.propTypes = {
  View: PropTypes.func,
  name: PropTypes.string,
  title: PropTypes.objectOf(PropTypes.string),
  listProps: listPropsPropType,
  overviewProps: overviewPropsPropType,
};

const makeTitle = (viewName, localizedTitle) => {
  if (!viewName) return '';

  if (viewName === localizedTitle) {
    return `${viewName}`;
  }

  return `${viewName} - ${localizedTitle}`;
};

// TODO refactor
/**
 * Wrapping each view in a ESContext (and possible EntryContext)
 * context is required in order to update the context (and possibly entry id)
 * when the document.pathname changes.
 *
 * However, this comes with 2 downsides:
 *  1) we need to wrap each view with the appropriate contexts (ESContext/EntryContext)
 *  2) after each navigation the context/entry is renewed even if some of them remain the same.
 *    E.g navigating from /catalog/256/datasets to /catalog/256/overview.
 *    In practise, this is not an issue since entrystore-js
 *    caches entries but still is not very elegant.
 *
 * TODO If react-router at some point offers a useMatch(manyRoutes)
 * signature we could do something like
 *
 *     <ESContextProvider path={path}>
        <EntryProvider default>
          {allViews}
        </EntryProvider>
      </ESContextProvider>
 *
 * @param {*} param0
 */

const ViewWrapper = ({
  path,
  name,
  View,
  layout,
  title,
  isPublic = false,
  restrictTo,
  module: moduleName,
  listProps = { excludeActions: [] },
  overviewProps = { excludeActions: [] },
}) => {
  const { userEntry, userInfo } = useUserState();
  const { pathname } = useLocation();
  const nrOfParamsInRoute = getNrOfParamsInRoute(path);
  const [viewName, setViewName] = useState('');
  const userPermission = useUserPermission(userEntry);
  const module = getModuleFromName(moduleName, userInfo);
  const hasViewPermission = checkHasViewPermission(
    userPermission,
    restrictTo,
    module
  );

  const renderedView = hasViewPermission ? View : PermissionView;
  const localizedTitle = localize(title);

  useDocumentTitle(makeTitle(viewName, localizedTitle));

  if (registry.getCurrentView() !== name) {
    registry.setCurrentView(name);
  }

  if (
    // ! This and NotFound should eventually be harmonized
    !isPublic && // e.g signin, singup, search
    name !== 'signin' && // just to make sure we avoid infinite loop
    userInfo.entryId === USER_ENTRY_ID_GUEST
  ) {
    return <Navigate to="/signin" replace state={{ referrer: pathname }} />;
  }

  // TODO key
  return (
    <PageTitleContext.Provider value={[viewName, setViewName]}>
      <Layout layout={layout} path={path} viewName={viewName}>
        {nrOfParamsInRoute !== 0 ? (
          <ParameterizedView
            name={name}
            path={path}
            pathname={pathname}
            View={renderedView}
            title={localizedTitle}
            listProps={listProps}
            overviewProps={overviewProps}
          />
        ) : (
          <SimpleView
            name={name}
            path={path}
            View={renderedView}
            title={title}
            listProps={listProps}
            overviewProps={overviewProps}
          />
        )}
      </Layout>
    </PageTitleContext.Provider>
  );
};

ViewWrapper.propTypes = {
  path: PropTypes.string,
  name: PropTypes.string,
  View: PropTypes.func,
  layout: PropTypes.string,
  title: PropTypes.objectOf(PropTypes.string),
  isPublic: PropTypes.bool,
  restrictTo: PropTypes.string,
  module: PropTypes.string,
  listProps: listPropsPropType,
  overviewProps: overviewPropsPropType,
};

const NotFound = ({ userInfo }) => {
  const isGuest = userInfo.entryId === USER_ENTRY_ID_GUEST;
  const appName = config.get('theme.appName');
  const path =
    isGuest && appName === 'Registry'
      ? getPathFromViewName('start')
      : getPathFromViewName('signin');
  return <Navigate to={path} />;
};

NotFound.propTypes = {
  userInfo: PropTypes.shape({ entryId: PropTypes.string }),
};

const Views = () => {
  const [primaryNavOpen, setPrimaryNavOpen] = useState(true);
  const { userInfo } = useUserState();
  const viewsDefinitions = getActiveViews(userInfo);
  const startView = config.get('site.startView') || 'start';

  const router = createBrowserRouter(
    createRoutesFromElements(
      <>
        {registry.getApp() === 'registry' ? (
          <>
            <Route
              path="/status/public"
              element={<Navigate to={getPathFromViewName('status_reports')} />}
            />
            <Route
              path="/status/private"
              element={<Navigate to={getPathFromViewName('status_reports')} />}
            />
          </>
        ) : null}
        <Route
          index
          path="/"
          element={<Navigate to={getPathFromViewName(startView)} replace />}
        />
        <Route
          path="/admin"
          element={<Navigate to={getPathFromViewName('admin__users')} />}
        />
        {viewsDefinitions.map(
          ({
            exact = true,
            route: path,
            name,
            title,
            layout,
            public: isPublic,
            restrictTo,
            module,
            listProps,
            overviewProps,
          }) => (
            <Route
              key={Math.random()}
              exact={exact}
              path={path}
              element={
                <PrimaryNavContext.Provider
                  value={{ primaryNavOpen, setPrimaryNavOpen }}
                >
                  <ViewWrapper
                    path={path}
                    View={registry.getView(name)}
                    name={name}
                    layout={layout}
                    title={title}
                    isPublic={isPublic}
                    restrictTo={restrictTo}
                    key={name}
                    module={module}
                    listProps={listProps}
                    overviewProps={overviewProps}
                  />
                </PrimaryNavContext.Provider>
              }
            />
          )
        )}
        <Route path="*" element={<NotFound userInfo={userInfo} />} />
      </>
    )
  );

  return (
    <ViewProviders>
      <RouterProvider router={router} />
    </ViewProviders>
  );
};

const App = () => {
  return (
    <AppProviders>
      <ErrorBoundary FallbackComponent={ErrorFallbackComponent}>
        <ErrorHandler />
        <Views />
      </ErrorBoundary>
    </AppProviders>
  );
};

export default App;
