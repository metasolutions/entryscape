/* eslint-disable react/forbid-prop-types */
/* eslint-disable react/jsx-props-no-spreading */
import PropTypes from 'prop-types';
import { Button, Divider, Paper, Typography } from '@mui/material';
import './index.scss';

export const MergeCard = ({ children }) => (
  <Paper className="esreMergeCard">{children}</Paper>
);

MergeCard.propTypes = {
  children: PropTypes.node,
};

const ActionButton = ({ label, ...props }) => (
  <Button {...props}>{label}</Button>
);

ActionButton.propTypes = {
  label: PropTypes.string.isRequired,
};

export const MergeCardHeader = ({ title, actionButtonProps }) => (
  <>
    <div className="esreMergeCardHeader">
      <Typography
        variant="subtitle1"
        component="h3"
        className="esreMergeCardHeader__heading"
      >
        {title}
      </Typography>
      {actionButtonProps && (
        <ActionButton
          className="esreMergeCardHeader__button"
          {...actionButtonProps}
        />
      )}
    </div>
    <Divider />
  </>
);
MergeCardHeader.propTypes = {
  title: PropTypes.string.isRequired,
  actionButtonProps: PropTypes.object,
};

export const MergeCardContent = ({ children }) => (
  <div className="esreMergeCard__content">{children}</div>
);
MergeCardContent.propTypes = {
  children: PropTypes.node,
};
