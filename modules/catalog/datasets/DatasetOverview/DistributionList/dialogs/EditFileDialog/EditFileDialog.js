import PropTypes from 'prop-types';
import { Entry } from '@entryscape/entrystore-js';
import { useState, useMemo, useEffect, useCallback } from 'react';
import ListActionDialog from 'commons/components/common/dialogs/ListActionDialog';
import useRDFormsEditor from 'commons/components/rdforms/hooks/useRDFormsEditor';
import { useTranslation } from 'commons/hooks/useTranslation';
import escoEntryTypeNLS from 'commons/nls/escoEntryType.nls';
import escaFilesListNLS from 'catalog/nls/escaFilesList.nls';
import ContentWrapper from 'commons/components/common/ContentWrapper';
import { useConfirmCloseAction } from 'commons/components/EntryListView/dialogs/create/handlers';
import { LEVEL_OPTIONAL } from 'commons/components/rdforms/LevelSelector';
import Button from '@mui/material/Button';
import Lookup from 'commons/types/Lookup';
import { namespaces } from '@entryscape/rdfjson';

const EditFileDialog = ({
  closeDialog,
  entry: fileEntry,
  refreshFileEntries,
}) => {
  const [buttonDisabled, setButtonDisabled] = useState(true);
  const confirmClose = useConfirmCloseAction(closeDialog);
  const rdformTemplate = useMemo(() => {
    const entitytype = Lookup.getEntityTypeByRdfType(
      namespaces.expand('esterms:File'),
      'catalog'
    );
    return entitytype.template();
  }, []);

  const translate = useTranslation([escoEntryTypeNLS, escaFilesListNLS]);
  const { editor, EditorComponent, graph } = useRDFormsEditor({
    entry: fileEntry,
    includeLevel: LEVEL_OPTIONAL,
    rdformTemplate,
  });

  useEffect(() => {
    if (graph) {
      graph.onChange = () => {
        setButtonDisabled(false);
      };
    }
  }, [graph]);

  const handleCloseAction = useCallback(() => {
    confirmClose(graph.isChanged());
  }, [confirmClose, graph]);

  const handleSaveForm = async () => {
    try {
      await fileEntry.setMetadata(graph).commitMetadata();
      refreshFileEntries();
    } catch (error) {
      console.error(`Could not save changes: ${error}`);
    } finally {
      closeDialog();
    }
  };

  const dialogActions = (
    <Button autoFocus onClick={handleSaveForm} disabled={buttonDisabled}>
      {translate('saveButtonLabel')}
    </Button>
  );
  return (
    <ListActionDialog
      id="edit-file"
      closeDialog={handleCloseAction}
      title={translate('editFileLabel')}
      actions={dialogActions}
      maxWidth="md"
    >
      <ContentWrapper md={12}>
        <form autoComplete="off">{editor && <EditorComponent />}</form>
      </ContentWrapper>
    </ListActionDialog>
  );
};

EditFileDialog.propTypes = {
  closeDialog: PropTypes.func.isRequired,
  entry: PropTypes.instanceOf(Entry),
  refreshFileEntries: PropTypes.func,
};
export default EditFileDialog;
