import { Children, cloneElement } from 'react';
import PropTypes from 'prop-types';
import { Box } from '@mui/material';
import './ListActions.scss';

/**
 * ListActions component uses a common layout for providing list actions to a
 * view. The layout assumes two colums, where the right column is justified to
 * the right.
 * The disabled prop is useful to disable all child actions, for example on
 * initial load.
 */
export const ListActions = ({
  disabled = false,
  justifyContent = 'flex-end',
  compact = false,
  children,
}) => {
  return (
    <Box
      sx={{ justifyContent }}
      className={
        compact ? 'escoListViewActions--compact' : 'escoListViewActions'
      }
    >
      {disabled // Construction to add disabled to child components if set to true
        ? Children.map(children, (child, index) => {
            return child
              ? cloneElement(child, {
                  id: `list-actions-${index}`,
                  disabled,
                })
              : null;
          })
        : children}
    </Box>
  );
};

ListActions.propTypes = {
  disabled: PropTypes.bool,
  justifyContent: PropTypes.string,
  compact: PropTypes.bool,
  children: PropTypes.node,
};
