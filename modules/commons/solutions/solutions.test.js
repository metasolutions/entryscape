import { fetchJson } from 'commons/util/async';
import { loadSolutions } from './solutions';

const SOLUTION_URI_1 = 'http://example.com/solutions/1.json';
const SOLUTION_URI_2 = 'http://example.com/solutions/2.json';

jest.mock('commons/util/async', () => {
  return {
    fetchJson: jest.fn(() => []),
  };
});

test('replaces local entity type names and references with solution name as prefix', async () => {
  fetchJson.mockReturnValue(
    Promise.resolve({
      name: 'es',
      entitytypes: [
        {
          name: '_localPrimary',
        },
        {
          name: 'builtinPrimary',
        },
        {
          name: '_refinesSecondary',
          refines: '_localPrimary',
        },
        {
          name: '_localUseWithSecondary',
          useWith: '_localPrimary',
        },
      ],
    })
  );
  const solutions = await loadSolutions([SOLUTION_URI_1]);
  expect(solutions.entitytypes).toEqual([
    {
      name: 'es_localPrimary', // name should be prefixed
    },
    {
      name: 'builtinPrimary', // should be same
    },
    {
      name: 'es_refinesSecondary', // name and refines should be prefixed
      refines: 'es_localPrimary',
    },
    {
      name: 'es_localUseWithSecondary', // name and useWith should be prefixed
      useWith: 'es_localPrimary',
    },
  ]);
});

test('only replaces entity type names and references with solution name as prefix', async () => {
  fetchJson.mockReturnValue(
    Promise.resolve({
      name: 'es',
      entitytypes: [
        {
          name: '_localPrimary',
          template: '_localPrimaryTemplate',
        },
      ],
    })
  );
  const solutions = await loadSolutions([SOLUTION_URI_1]);
  expect(solutions.entitytypes).toEqual([
    {
      name: 'es_localPrimary', // name should be prefixed
      template: '_localPrimaryTemplate', // should not be prefixed
    },
  ]);
});

test('merges multiple solutions into one', async () => {
  fetchJson.mockImplementation(async (uri) => {
    if (uri === SOLUTION_URI_1)
      return {
        name: 'es1',
        entitytypes: [
          {
            name: 'builtinPrimary',
            template: 'builtinPrimary-es1',
          },
          {
            name: '_localPrimary',
          },
        ],
      };
    return {
      name: 'es2',
      entitytypes: [
        {
          name: 'builtinPrimary',
          template: 'builtinPrimary-es2',
        },
      ],
    };
  });
  const solutions = await loadSolutions([SOLUTION_URI_1, SOLUTION_URI_2]);
  expect(solutions.entitytypes).toEqual([
    {
      name: 'builtinPrimary', // will merge values, but the last in order has precedence
      template: 'builtinPrimary-es2', // template is overridden from solution 2
    },
    {
      name: 'es1_localPrimary', // keeps the local et from solution 1
    },
  ]);
});

test('prefixes relations with local entity type name references', async () => {
  fetchJson.mockReturnValue(
    Promise.resolve({
      name: 'es',
      entitytypes: [
        {
          name: '_localComposition',
        },
        {
          name: '_localAggregation',
        },
        {
          name: '_localPrimaryWithRelations',
          relations: [
            {
              entityType: '_localComposition',
              type: 'composition',
              property: 'dcterms:relation',
            },
            {
              entityType: '_localAggregation',
              type: 'aggregation',
              property: 'dcterms:relation',
            },
          ],
        },
      ],
    })
  );

  const solutions = await loadSolutions([SOLUTION_URI_1, SOLUTION_URI_2]);
  expect(solutions.entitytypes).toEqual([
    {
      name: 'es_localComposition',
    },
    {
      name: 'es_localAggregation',
    },
    {
      name: 'es_localPrimaryWithRelations',
      relations: [
        {
          entityType: 'es_localComposition',
          type: 'composition',
          property: 'dcterms:relation',
        },
        {
          entityType: 'es_localAggregation',
          type: 'aggregation',
          property: 'dcterms:relation',
        },
      ],
    },
  ]);
});
