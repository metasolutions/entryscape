import { useEffect, useCallback } from 'react';
import useAsync from 'commons/hooks/useAsync';
import useGetMainDialog from 'commons/hooks/useGetMainDialog';
import { useTranslation } from 'commons/hooks/useTranslation';
import { useListModel } from 'commons/components/ListView';
import escaDatasetNLS from 'catalog/nls/escaDataset.nls';
import { getDistributions } from '../utils/distributions';
import { getBrokenReferences, removeBrokenReferences } from '../utils/datasets';

/**
 *
 * @param {Entry} datasetEntry
 * @param {Error} error
 * @param {Function} onRemoveReferences
 */
const useRemoveBrokenReferences = (datasetEntry, error, onRemoveReferences) => {
  const { runAsync } = useAsync();

  useEffect(() => {
    if (!error) return;

    /**
     * @returns {Promise<string[]|undefined>}
     */
    const getAndRemoveBrokenReferences = async () => {
      const brokenReferences = await getBrokenReferences(datasetEntry);
      if (!brokenReferences.length) return;
      await removeBrokenReferences(datasetEntry, brokenReferences);
      await onRemoveReferences(brokenReferences);
    };

    runAsync(getAndRemoveBrokenReferences(datasetEntry));
  }, [error, datasetEntry, runAsync, onRemoveReferences]);
};

const useDistributions = (datasetEntry) => {
  const { data: distributions, error, runAsync, status } = useAsync(null);
  const { getAcknowledgeDialog } = useGetMainDialog();
  const t = useTranslation(escaDatasetNLS);

  const refreshDistributions = useCallback(
    () => runAsync(getDistributions(datasetEntry)),
    [datasetEntry, runAsync]
  );

  const acknowledgeBrokenRefsAndRefresh = useCallback(async () => {
    await getAcknowledgeDialog({
      content: t('removeBrokenDatasetRefsWarning'),
    });
    refreshDistributions();
  }, [getAcknowledgeDialog, refreshDistributions, t]);

  useRemoveBrokenReferences(
    datasetEntry,
    error,
    acknowledgeBrokenRefsAndRefresh
  );

  const [{ refreshCount }] = useListModel();

  useEffect(() => {
    refreshDistributions();
  }, [refreshDistributions, refreshCount]);

  return { distributions, refreshDistributions, status };
};

export default useDistributions;
