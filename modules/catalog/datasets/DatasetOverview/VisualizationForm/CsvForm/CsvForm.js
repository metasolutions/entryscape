import { useCallback, useEffect, useRef, useState } from 'react';
import PropTypes from 'prop-types';
import {
  FormControl,
  FormControlLabel,
  FormLabel,
  InputLabel,
  MenuItem,
  RadioGroup,
  Radio,
  Select,
  Checkbox,
} from '@mui/material';
import {
  BarChart as BarChartIcon,
  PieChart as PieChartIcon,
  ShowChart as ShowChartIcon,
  Map as MapIcon,
  TableChart as TableChartIcon,
} from '@mui/icons-material';
import { renderHtmlString } from 'commons/util/reactUtils';
import { Entry } from '@entryscape/entrystore-js';
import useAsync from 'commons/hooks/useAsync';
import { useTranslation } from 'commons/hooks/useTranslation';
// eslint-disable-next-line max-len
import { useVisualizationState } from 'catalog/datasets/DatasetOverview/hooks/useVisualizationState';
import { validateCsvFields } from 'catalog/datasets/DatasetOverview/utils/visualizations';
import PreviewChart from 'catalog/datasets/DatasetOverview/VisualizationForm/PreviewChart';
import ChartPlaceholder from 'catalog/datasets/DatasetOverview/VisualizationForm/ChartPlaceholder';
import escoListNLS from 'commons/nls/escoList.nls';
import escaDatasetNLS from 'catalog/nls/escaDataset.nls';
import escaVisualizationNLS from 'catalog/nls/escaVisualization.nls';
import escoGeoMapNLS from 'commons/nls/escoGeoMap.nls';
import { fetchCsv, isWithinFileSizeLimit } from 'commons/util/fetchCsv';
import { SelectGroup, SelectButton } from '../../SelectGroup';
import FormSection from '../FormSection';
import InputFieldsLabel from '../InputFieldsLabel';

const nlsBundles = [
  escoListNLS,
  escaDatasetNLS,
  escaVisualizationNLS,
  escoGeoMapNLS,
];

const CSV_ENCODINGS = [
  { value: 'UTF-8', label: 'UTF-8' },
  { value: 'ISO-8859-1', label: 'ISO 8859-1' },
];

const xAxisLabelMap = {
  map: 'xAxisMap',
  pie: 'xAxisPie',
};

const yAxisLabelMap = {
  map: 'yAxisMap',
  pie: 'yAxisPie',
};

const useAxisText = (chartType) => {
  const translate = useTranslation(nlsBundles);
  if (!chartType || chartType === 'table') return;

  return {
    xLabel: translate(xAxisLabelMap[chartType] || 'xAxis'),
    yLabel: translate(yAxisLabelMap[chartType] || 'yAxis'),
    helptext: chartType === 'map' ? '' : translate(`${chartType}XAxisHelpText`),
  };
};

const CsvForm = ({ csvData }) => {
  const translate = useTranslation(nlsBundles);
  const fields = csvData?.meta?.fields || [];
  const { fieldsState, isBroken, errors, updateFieldsState } =
    useVisualizationState();
  const {
    chartType,
    encoding,
    xField,
    yField,
    operation,
    featureTitle,
    scaleLineControl,
    initialView = null,
  } = fieldsState;
  const { xField: xFieldError, yField: yFieldError } = errors;

  const hasYFieldIfNeeded = yField || operation === 'count';
  const showChart = (xField && hasYFieldIfNeeded) || chartType === 'table';
  const axis = useAxisText(fieldsState?.chartType);

  const handleChangeChartType = (selectedChartType) => {
    updateFieldsState({
      chartType: selectedChartType,
      columns: selectedChartType === 'table' ? fields : [],
      operation: selectedChartType === 'bar' ? 'none' : '',
      xField: selectedChartType === 'table' ? '' : xField,
      yField: selectedChartType === 'table' ? '' : yField,
    });
  };

  const xFieldClasses = xFieldError
    ? 'escaVisualizationForm__underlinedLabel escaVisualizationForm__underlinedLabel--error'
    : 'escaVisualizationForm__underlinedLabel';

  const yFieldClasses = yFieldError
    ? 'escaVisualizationForm__underlinedLabel escaVisualizationForm__underlinedLabel--error'
    : 'escaVisualizationForm__underlinedLabel';

  const axesControls = axis
    ? [
        <FormControl
          key="x-axis-select"
          variant="filled"
          fullWidth
          error={Boolean(xFieldError)}
        >
          <InputLabel id="x-axis-select-label">{axis.xLabel}</InputLabel>
          <Select
            inputProps={{ 'aria-labelledby': 'x-axis-select-label' }}
            value={xField || ''}
            onChange={({ target }) =>
              updateFieldsState({ xField: target.value })
            }
          >
            {xFieldError ? <MenuItem value={xField}>{xField}</MenuItem> : null}
            {fields.map((field, index) => (
              // eslint-disable-next-line react/no-array-index-key
              <MenuItem key={index} value={field}>
                {field}
              </MenuItem>
            ))}
          </Select>
        </FormControl>,
        <FormControl
          key="y-axis-select"
          variant="filled"
          fullWidth
          error={Boolean(yFieldError)}
        >
          <InputLabel id="y-axis-select-label">{axis.yLabel}</InputLabel>
          <Select
            inputProps={{ 'aria-labelledby': 'y-axis-select-label' }}
            value={yField || ''}
            disabled={operation === 'count'}
            onChange={({ target }) =>
              updateFieldsState({ yField: target.value })
            }
          >
            {yFieldError ? <MenuItem value={yField}>{yField}</MenuItem> : null}
            {fields.map((field, index) => (
              // eslint-disable-next-line react/no-array-index-key
              <MenuItem key={index} value={field}>
                {field}
              </MenuItem>
            ))}
          </Select>
        </FormControl>,
      ]
    : [];

  const [isFirstLoad, setIsFirstLoad] = useState(true);
  const mapOptions = useRef({ initialView });

  const handleViewExtentChange = useCallback(
    ({ frameState }) => {
      const { extent: viewExtent } = frameState;
      updateFieldsState(
        {
          initialView: viewExtent,
        },
        isFirstLoad
      );
      if (isFirstLoad) setIsFirstLoad(false);
    },
    [updateFieldsState, isFirstLoad]
  );

  if (!csvData) return null;

  return (
    <>
      <FormSection>
        <FormControl
          classes={{ root: 'escoVisualizationForm__formControl--margin' }}
          variant="filled"
          fullWidth
        >
          <InputFieldsLabel
            id="encoding-select-label"
            label={translate('encodingLabel')}
          />
          <Select
            inputProps={{ 'aria-labelledby': 'encoding-select-label' }}
            id="encoding-select"
            value={encoding || ''}
            onChange={({ target }) => {
              updateFieldsState({
                encoding: target.value,
              });
            }}
          >
            {CSV_ENCODINGS.map(({ value, label }) => (
              <MenuItem key={value} value={value}>
                {label}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
      </FormSection>
      <FormSection>
        <InputFieldsLabel label={translate('vizDialogTypeTitle')} mandatory />
        <SelectGroup
          value={chartType}
          onChange={(selectedChartType) => {
            handleChangeChartType(selectedChartType);
          }}
        >
          <SelectButton
            value="bar"
            tooltip={translate('barChartHelpText')}
            startIcon={<BarChartIcon />}
          >
            {translate('vizGraphBar')}
          </SelectButton>
          <SelectButton
            value="pie"
            tooltip={translate('pieChartHelpText')}
            startIcon={<PieChartIcon />}
          >
            {translate('vizGraphPie')}
          </SelectButton>
          <SelectButton
            value="line"
            tooltip={translate('lineChartHelpText')}
            startIcon={<ShowChartIcon />}
          >
            {translate('vizGraphLine')}
          </SelectButton>
          <SelectButton
            value="map"
            tooltip={translate('mapChartHelpText')}
            startIcon={<MapIcon />}
          >
            {translate('vizGraphMap')}
          </SelectButton>
          <SelectButton value="table" startIcon={<TableChartIcon />}>
            {translate('vizTable')}
          </SelectButton>
        </SelectGroup>
      </FormSection>
      {chartType !== 'table' ? (
        <FormSection>
          <InputFieldsLabel label={translate('vizDialogAxesTitle')} mandatory />
          {chartType === 'map' ? axesControls.reverse() : axesControls}
        </FormSection>
      ) : null}

      {chartType === 'map' ? (
        <>
          <FormSection>
            <InputFieldsLabel label={translate('vizDialogFeaturesTitle')} />
            <FormControl variant="filled" fullWidth>
              <InputLabel id="feature-title-select-label">
                {translate('featureTitle')}
              </InputLabel>
              <Select
                inputProps={{ 'aria-labelledby': 'feature-title-select-label' }}
                value={featureTitle || ''}
                onChange={({ target }) =>
                  updateFieldsState({ featureTitle: target.value })
                }
              >
                <MenuItem value={featureTitle}>{featureTitle}</MenuItem>
                {fields.map((field) => (
                  <MenuItem key={field} value={field}>
                    {field}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>
          </FormSection>
          <FormSection title={translate('vizDialogMapControlsTitle')}>
            <FormControlLabel
              id="scaleLine-title-checkbox-label"
              control={
                <Checkbox
                  checked={scaleLineControl === 'true'}
                  onChange={({ target }) => {
                    updateFieldsState({
                      scaleLineControl: target.checked.toString(),
                    });
                  }}
                />
              }
              label={translate('vizDialogMapLegendScaleLineLabel')}
            />
          </FormSection>
        </>
      ) : null}

      {chartType === 'bar' ? (
        <FormSection>
          <InputFieldsLabel label={translate('processingMethod')} />
          <FormControl component="fieldset" fullWidth>
            <FormLabel component="legend">{translate('vizBarValue')}</FormLabel>
            <RadioGroup
              aria-label={translate('vizBarValue')}
              name="aggregation-method"
              value={operation}
              onChange={({ target }) =>
                updateFieldsState({ operation: target.value })
              }
            >
              <FormControlLabel
                value="none"
                control={<Radio />}
                label={translate('vizBarValueNone')}
              />
              <FormControlLabel
                value="count"
                control={<Radio />}
                label={renderHtmlString(
                  translate('vizBarValueCount', {
                    xField: `<span class='${xFieldClasses}'>${xField}</span>`,
                  })
                )}
              />
              <FormControlLabel
                value="sum"
                control={<Radio />}
                label={renderHtmlString(
                  translate('vizBarValueSum', {
                    xField: `<span class='${xFieldClasses}'>${xField}</span>`,
                    yField: `<span class='${yFieldClasses}'>${
                      yField || ''
                    }</span>`,
                  })
                )}
              />
            </RadioGroup>
          </FormControl>
        </FormSection>
      ) : null}
      <FormSection title={translate('vizDialogPreview')}>
        {showChart && !isBroken ? (
          <PreviewChart
            csvData={csvData}
            {...fieldsState}
            mapOptions={mapOptions.current}
            onViewExtentChange={handleViewExtentChange}
          />
        ) : (
          <ChartPlaceholder
            label={
              isBroken
                ? translate('brokenVizPlaceholderLabel')
                : translate('placeholderLabel')
            }
            labelClass={isBroken ? 'escaChartPlaceHolderLabel--error' : ''}
            chartType={chartType}
          />
        )}
      </FormSection>
    </>
  );
};

CsvForm.propTypes = {
  csvData: PropTypes.shape({
    meta: PropTypes.shape({ fields: PropTypes.arrayOf(PropTypes.string) }),
  }),
  uri: PropTypes.string,
  visualizationEntry: PropTypes.instanceOf(Entry),
  setVisualizationIsBroken: PropTypes.func,
};

const CsvFormWithCsvData = ({ translate, ...props }) => {
  const { data: csvData, runAsync, isLoading } = useAsync();
  const { fieldsState, selectedSourceChoice, addErrors, initialFields } =
    useVisualizationState();
  const { encoding } = fieldsState;
  const initialLoad = useRef(true);

  // get and validate csv data
  useEffect(() => {
    const getCsvData = async () => {
      const { fileEntry } = selectedSourceChoice;
      if (!isWithinFileSizeLimit(fileEntry.getEntryInfo().getSize())) {
        addErrors({ sourceURI: translate('filesizeError') });
        return;
      }
      try {
        const data = await fetchCsv(fileEntry.getResourceURI(), {
          encoding,
        });
        // validate initial source to make sure it hasn't changed
        if (initialLoad.current && initialFields) {
          const errors = validateCsvFields(initialFields, data);
          if (errors) {
            addErrors(errors);
          }
        }
        return data;
      } catch (csvError) {
        console.log(csvError);
        addErrors({ sourceURI: translate('loadSourceError') });
      }
    };

    runAsync(
      getCsvData().then((data) => {
        initialLoad.current = false;
        return data;
      })
    );
  }, [
    selectedSourceChoice,
    encoding,
    addErrors,
    initialFields,
    runAsync,
    translate,
  ]);

  if (isLoading) return null;

  return <CsvForm csvData={csvData} {...props} />;
};

CsvFormWithCsvData.propTypes = {
  translate: PropTypes.func,
};

export default CsvFormWithCsvData;
