import PropTypes from 'prop-types';
import { Button, Typography } from '@mui/material';
import Tooltip from 'commons/components/common/Tooltip';
import './OverviewSidebarButton.scss';

const OverviewSidebarButton = ({
  children,
  onClick,
  color,
  disabled,
  highlightId,
  tooltip,
  ...buttonProps
}) => {
  return (
    <Tooltip title={tooltip}>
      <Button
        color={color}
        className="escoOverviewSidebarListItem__button"
        onClick={onClick}
        disabled={disabled}
        variant={disabled ? 'outlined' : 'contained'}
        data-highlight-id={highlightId}
        {...buttonProps}
      >
        <Typography
          className={`escoOverviewSidebarButtonLabel${
            disabled ? '--disabled' : ''
          }`}
        >
          {children}
        </Typography>
      </Button>
    </Tooltip>
  );
};

OverviewSidebarButton.propTypes = {
  children: PropTypes.node.isRequired,
  onClick: PropTypes.func,
  color: PropTypes.string,
  disabled: PropTypes.bool,
  tooltip: PropTypes.string,
  highlightId: PropTypes.string,
};

export default OverviewSidebarButton;
