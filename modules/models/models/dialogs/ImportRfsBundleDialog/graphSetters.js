import { Graph } from '@entryscape/rdfjson';
import {
  RDF_PROPERTY_VALUE,
  RDF_PROPERTY_CONSTRAINT,
  RDF_PROPERTY_OPTION,
  RDF_PROPERTY_BLANK,
  RDF_PROPERTY_RESOURCE,
  RDF_PROPERTY_DATATYPE_LITERAL,
  RDF_PROPERTY_LITERAL,
  RDF_PROPERTY_URI,
  RDF_PROPERTY_ONLY_LITERAL,
  RDF_PROPERTY_LANGUAGE_LITERAL,
  RDF_PROPERTY_CONSTRAINT_PROPERTY,
  RDF_PROPERTY_CONSTRAINT_VALUE,
  RDF_TYPE_CONSTRAINT,
  RDF_PROPERTY_ORDER,
  RDF_CLASS_OPTION,
  RDF_PROPERTY_PURPOSE,
} from 'models/utils/ns';
import {
  FIELD_ATTRIBUTE,
  FIELD_CSS,
  FIELD_DATATYPE,
  FIELD_DESCRIPTION,
  FIELD_EDIT_DESCRIPTION,
  FIELD_EDIT_LABEL,
  FIELD_HELP,
  FIELD_LABEL,
  FIELD_LANGUAGE,
  FIELD_NODETYPE,
  FIELD_PATTERN,
  FIELD_PLACEHOLDER,
  FIELD_PROPERTY,
  FIELD_PURPOSE,
  FIELD_SPECIFICATION,
  FIELD_VALUE,
  FIELD_VALUE_TEMPLATE,
} from 'models/utils/fieldDefinitions';
import { setCardinalityFromRfs } from 'models/utils/cardinality';
import { addLanguageLiterals } from 'models/utils/metadata';
import { addAlternativeText } from 'models/utils/alternativeText';

/**
 * Sets property value in graph
 *
 * @param {object} root - JSON data from file or link
 * @param {Graph} graph -
 * @param {string} title - subject position in graph
 */
const setProperty = (root, graph, title) => {
  graph.add(title, FIELD_PROPERTY.property, root.property);
};

const nodeType2URI = (nodetype) => {
  switch (nodetype.toLowerCase()) {
    case 'resource':
      return RDF_PROPERTY_RESOURCE;
    case 'blank':
      return RDF_PROPERTY_BLANK;
    case 'literal':
      return RDF_PROPERTY_LITERAL;
    case 'only_literal':
      return RDF_PROPERTY_ONLY_LITERAL;
    case 'datatype_literal':
      return RDF_PROPERTY_DATATYPE_LITERAL;
    case 'language_literal':
      return RDF_PROPERTY_LANGUAGE_LITERAL;
    case 'uri':
    default:
      return RDF_PROPERTY_URI;
  }
};

/**
 * Sets nodetype value in graph
 *
 * @param {object} root - JSON data from file or link
 * @param {Graph} graph -
 * @param {string} title - subject position in graph
 */
const setNodeType = (root, graph, title) => {
  graph.add(title, FIELD_NODETYPE.property, nodeType2URI(root.nodetype));
};

/**
 * Sets datatype value(s) in graph
 *
 * @param {object} root - JSON data from file or link
 * @param {Graph} graph
 * @param {string} title - subject position in graph
 */
const setDatatype = (root, graph, title) => {
  const datatypes = Array.isArray(root.datatype)
    ? root.datatype
    : [root.datatype];
  for (const [index, value] of Object.entries(datatypes)) {
    const statement = graph.add(title, FIELD_DATATYPE.property);
    const blankNodeURI = statement.getValue();
    graph.addL(blankNodeURI, RDF_PROPERTY_ORDER, (index + 1).toString());
    graph.add(blankNodeURI, 'rdf:value', value);
  }
};

/**
 * Sets a set of language labels for a specific field
 *
 * @param {object} field definition object
 * @returns {Function}
 */
const langStringSetterFor = (field) => (form, graph, title) => {
  if (field.jsonAttribute in form) {
    addLanguageLiterals(
      graph,
      title,
      field.property,
      form[field.jsonAttribute]
    );
  }
};

/**
 * Sets valueTemplate value in graph
 *
 * @param {object} root - JSON data from file or link
 * @param {Graph} graph -
 * @param {string} title - subject position in graph
 */
const setValueTemplate = (root, graph, title) => {
  graph.addL(title, FIELD_VALUE_TEMPLATE.property, root.valueTemplate);
};

/**
 * Sets pattern value in graph
 *
 * @param {object} root - JSON data from file or link
 * @param {Graph} graph -
 * @param {string} title -subject position in graph
 */
const setPattern = (root, graph, title) => {
  graph.addL(title, FIELD_PATTERN.property, root.pattern);
};

/**
 * Sets cardinality values in graph
 *
 * @param {object} root - JSON data from file or link
 * @param {Graph} graph -
 * @param {string} title - subject position in graph
 */
const setCardinality = (root, graph, title) => {
  setCardinalityFromRfs(graph, title, root);
};

/**
 * Creates blank nodes  and sets constraint values in graph
 *
 * @param {object} root - JSON data from file or link
 * @param {Graph} graph - JSON data from file or link
 * @param {string} title - JSON data from file or link
 */
const setConstraints = (root, graph, title) => {
  for (const [property, value] of Object.entries(root.constraints)) {
    const statement = graph.add(title, RDF_PROPERTY_CONSTRAINT);
    const constraintBNode = statement.getValue();
    graph.add(constraintBNode, 'rdf:type', RDF_TYPE_CONSTRAINT);
    graph.add(constraintBNode, RDF_PROPERTY_CONSTRAINT_PROPERTY, property);
    const values = typeof value === 'string' ? [value] : value;
    let order = 1;
    values.forEach((v) => {
      const valueBNode = graph
        .add(constraintBNode, RDF_PROPERTY_CONSTRAINT_VALUE)
        .getValue();
      graph.add(valueBNode, 'rdf:value', v);
      graph.addD(valueBNode, RDF_PROPERTY_ORDER, `${order}`, 'xsd:integer');
      order += 1;
    });
  }
};

const setLangMap = (
  { jsonAttribute: attribute, property },
  { form, graph, node }
) => {
  if (attribute in form) {
    for (const [language, value] of Object.entries(form[attribute])) {
      graph.addL(node, property, value, language);
    }
  }
};

/**
 * Creates blank nodes and sets choice values in graph
 *
 * @param {object} root - JSON data from file or link
 * @param {Graph} graph -
 * @param {string} title - subject position in graph
 */
const setChoices = (root, graph, title) => {
  for (const [index, value] of Object.entries(root.choices)) {
    const statement = graph.add(title, RDF_PROPERTY_OPTION);
    const blankNode = statement.getValue();

    graph.add(blankNode, 'rdf:type', RDF_CLASS_OPTION);
    graph.addL(blankNode, RDF_PROPERTY_ORDER, (index + 1).toString());
    graph.addL(blankNode, RDF_PROPERTY_VALUE, value.value);

    const fixture = { form: value, graph, node: blankNode };
    setLangMap(FIELD_LABEL, fixture);
    setLangMap(FIELD_EDIT_LABEL, fixture);
    setLangMap(FIELD_DESCRIPTION, fixture);
    setLangMap(FIELD_EDIT_DESCRIPTION, fixture);
  }
};

/**
 * Sets attributes values in graph (styles in json)
 *
 * @param {object} root - JSON data from file or link
 * @param {Graph} graph - JSON data from file or link
 * @param {string} title - JSON data from file or link
 */
const setAttributes = (root, graph, title) => {
  (root.styles || []).forEach((style) => {
    graph.addL(title, FIELD_ATTRIBUTE.property, style);
  });
};

/**
 * Sets css classes in graph (styles in json)
 *
 * @param {object} root - JSON data from file or link
 * @param {Graph} graph - JSON data from file or link
 * @param {string} title - JSON data from file or link
 */
const setCSS = (root, graph, title) => {
  (root.cls || []).forEach((cls) => {
    graph.addL(title, FIELD_CSS.property, cls);
  });
};

/**
 * Sets the default language
 *
 * @param {object} root - JSON data from file or link
 * @param {Graph} graph -
 * @param {string} title - subject position in graph
 */
const setLanguage = (root, graph, title) => {
  if (root.language) {
    graph.addL(title, FIELD_LANGUAGE.property, root.language);
  }
};

/**
 * Sets the default value
 *
 * @param {object} root - JSON data from file or link
 * @param {Graph} graph -
 * @param {string} title - subject position in graph
 */
const setValue = (root, graph, title) => {
  if (root.value) {
    graph.addL(title, FIELD_VALUE.property, root.value);
  }
};

/**
 *
 * @param {object} root
 * @param {Graph} graph
 * @param {string} id named title elsewhere
 */
const setTextValues = (root, graph, id) => {
  const text = root.text || {};
  const { title, purpose, ...alternativeTexts } = text;
  if (title) {
    addLanguageLiterals(graph, id, 'dcterms:title', title);
  }
  if (purpose) {
    addLanguageLiterals(graph, id, RDF_PROPERTY_PURPOSE, purpose);
  }
  if (alternativeTexts) {
    Object.entries(alternativeTexts).forEach(([role, texts]) => {
      texts.forEach((lang2val) => {
        addAlternativeText(graph, id, role, lang2val);
      });
    });
  }
};

export default {
  label: langStringSetterFor(FIELD_LABEL),
  editlabel: langStringSetterFor(FIELD_EDIT_LABEL),
  description: langStringSetterFor(FIELD_DESCRIPTION),
  editdescription: langStringSetterFor(FIELD_EDIT_DESCRIPTION),
  purpose: langStringSetterFor(FIELD_PURPOSE),
  help: langStringSetterFor(FIELD_HELP),
  specification: langStringSetterFor(FIELD_SPECIFICATION),
  placeholder: langStringSetterFor(FIELD_PLACEHOLDER),
  property: setProperty,
  nodetype: setNodeType,
  cardinality: setCardinality,
  constraints: setConstraints,
  choices: setChoices,
  valueTemplate: setValueTemplate,
  datatype: setDatatype,
  pattern: setPattern,
  styles: setAttributes,
  cls: setCSS,
  language: setLanguage,
  value: setValue,
  text: setTextValues,
  // TODO deps, uriValueLabelProperties
};
