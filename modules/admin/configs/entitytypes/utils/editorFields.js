import {
  RadioEditor,
  SelectEditor,
  LANGUAGE_LITERAL,
  URI,
} from 'commons/components/forms/editors';
import { ConstraintEditor } from '../../editors/ConstraintEditor';
import { EntityTypeSelectEditor } from '../EntityTypeSelectEditor';
import * as fieldDefinitions from './fieldDefinitions';
import { fieldPresenters } from './entityTypePresenters';

export const editorFields = [
  { ...fieldDefinitions.label },
  { ...fieldDefinitions.description },
  { ...fieldDefinitions.template },
  { ...fieldDefinitions.constraints, Editor: ConstraintEditor },
  {
    ...fieldDefinitions.createOptions,
    editors: [
      fieldDefinitions.includeInternal,
      fieldDefinitions.inlineCreation,
      fieldDefinitions.includeFile,
      fieldDefinitions.includeLink,
    ],
  },
];

export const advancedEditorFields = [
  { ...fieldDefinitions.module, Editor: SelectEditor },
  { ...fieldDefinitions.templateLevel, Editor: RadioEditor },
  {
    ...fieldDefinitions.useWith,
    Editor: EntityTypeSelectEditor,
    dialogProps: { presenters: fieldPresenters },
  },
  {
    ...fieldDefinitions.refines,
    Editor: EntityTypeSelectEditor,
    dialogProps: { presenters: fieldPresenters },
  },
  { ...fieldDefinitions.context },
  { ...fieldDefinitions.restrictTo },
  { ...fieldDefinitions.allContexts },
  { ...fieldDefinitions.publishControl },
  { ...fieldDefinitions.global },
  { ...fieldDefinitions.main },
  { ...fieldDefinitions.uriPattern },
  { ...fieldDefinitions.iconId },
  { ...fieldDefinitions.searchProps },
  {
    ...fieldDefinitions.storeConfig,
    editors: [
      {
        nodetype: LANGUAGE_LITERAL,
        property: 'dcterms:title',
        labelNlsKey: 'labelLabel',
      },
      {
        nodetype: URI,
        property: 'dcterms:source',
        max: 1,
        labelNlsKey: 'endpointLabel',
      },
    ],
  },
];
