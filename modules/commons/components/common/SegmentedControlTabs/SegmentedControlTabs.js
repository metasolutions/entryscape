import React from 'react';
import PropTypes from 'prop-types';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import escaPreparationsNLS from 'catalog/nls/escaPreparations.nls';
import { useTranslation } from 'commons/hooks/useTranslation';
import './SegmentedControlTabs.scss';

const SegmentedControlTabs = ({
  selected,
  items,
  onSelect,
  disabled = false,
}) => {
  const handleChange = (_event, newValue) => {
    // setting value to return to the parent component for filter operation
    onSelect(newValue);
  };

  const translate = useTranslation([escaPreparationsNLS]);

  return (
    <div className="SegmentedControlTabs">
      <div className="SegmentedControlTabs__inner">
        <Tabs
          value={selected}
          textColor="secondary"
          onChange={handleChange}
          classes={{
            root: 'itemBasic',
            indicator: 'segmentedControlTabs__indicator',
            flexContainer: 'segmentedControlTabs__flexContainer',
          }}
          aria-label={translate('segmentedControlTabAriaLabel')}
        >
          {items.map((item) => (
            <Tab
              disabled={disabled}
              label={item.label}
              key={item.label}
              value={item.value}
              className={`${
                selected === item.value
                  ? 'SegmentedControlTabs__item SegmentedControlTabs__item--selected'
                  : 'SegmentedControlTabs__item'
              }`}
            />
          ))}
        </Tabs>
      </div>
    </div>
  );
};

SegmentedControlTabs.defaultProps = {
  selected: 'all',
};

SegmentedControlTabs.propTypes = {
  items: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string.isRequired,
      value: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
        .isRequired,
    })
  ).isRequired,
  onSelect: PropTypes.func.isRequired,
  disabled: PropTypes.bool,
  selected: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
};

export default SegmentedControlTabs;
