import { useLayoutEffect, useCallback, useRef } from 'react';

const useIsMounted = () => {
  const mounted = useRef(false);

  useLayoutEffect(() => {
    mounted.current = true;

    return () => {
      mounted.current = false;
    };
  }, []);

  const isMounted = useCallback(() => mounted.current, []);

  return isMounted;
};

export default useIsMounted;
