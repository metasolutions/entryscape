import PropTypes from 'prop-types';
import { Badge } from '@mui/material';
import {
  ListActionIconButton,
  TOGGLE_TABLEVIEW,
  TOGGLE_FILTERS,
  REFRESH,
  useListModel,
} from 'commons/components/ListView';
import {
  TableChart as TableChartIcon,
  FilterAlt as FilterIcon,
  Refresh as RefreshIcon,
} from '@mui/icons-material';
import { useTranslation } from 'commons/hooks/useTranslation';
import escoListNLS from 'commons/nls/escoList.nls';
import escoFiltersNLS from 'commons/nls/escoFilters.nls';
import { Spin, useSpinState } from 'commons/components/common/Spin';

export const TableViewButton = ({ disabled }) => {
  const translate = useTranslation(escoListNLS);
  const [, dispatch] = useListModel();

  return (
    <ListActionIconButton
      onClick={() => dispatch({ type: TOGGLE_TABLEVIEW })}
      ariaLabel={translate('tableViewButtonTooltip')}
      icon={<TableChartIcon />}
      disabled={disabled}
      tooltip={translate('tableViewButtonTooltip')}
      highlightId="tableViewButton"
    />
  );
};

TableViewButton.propTypes = {
  disabled: PropTypes.bool,
};

export const ToggleFiltersButton = ({ disabled, active }) => {
  const translate = useTranslation(escoFiltersNLS);
  const [, dispatch] = useListModel();

  return (
    <ListActionIconButton
      onClick={() => dispatch({ type: TOGGLE_FILTERS })}
      ariaLabel="filter"
      icon={
        <Badge color="secondary" variant="dot" invisible={!active}>
          <FilterIcon />
        </Badge>
      }
      disabled={disabled}
      tooltip={translate('searchFiltersButtonTooltip')}
    />
  );
};

ToggleFiltersButton.propTypes = {
  disabled: PropTypes.bool,
  active: PropTypes.bool,
};

export const RefreshButton = ({ disabled, status }) => {
  const translate = useTranslation(escoListNLS);
  const [, dispatch] = useListModel();
  const [spinning, startSpinning] = useSpinState(status);

  return (
    <ListActionIconButton
      onClick={() => {
        dispatch({ type: REFRESH });
        startSpinning();
      }}
      ariaLabel={translate('refreshListLabel')}
      icon={
        spinning ? (
          <Spin>
            <RefreshIcon />
          </Spin>
        ) : (
          <RefreshIcon />
        )
      }
      disabled={disabled}
      tooltip={translate('actionTooltipRefresh')}
    />
  );
};

RefreshButton.propTypes = {
  disabled: PropTypes.bool,
  status: PropTypes.string,
};
