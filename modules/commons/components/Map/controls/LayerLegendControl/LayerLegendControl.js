import { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { useMapContext } from 'commons/components/Map/hooks/useMapContext';
import { Button } from '@mui/material';
import Tooltip from 'commons/components/common/Tooltip';
import {
  ChevronRight as ShowIcon,
  ExpandMore as HideIcon,
} from '@mui/icons-material';
import { useTranslation } from 'commons/hooks/useTranslation';
import escoMapNls from 'commons/nls/escoMap.nls';
import './LayerLegendControl.scss';

const LayerLegendItem = ({ layer }) => {
  const [{ map }] = useMapContext();
  const [legendError, setLegendError] = useState(false);
  const translate = useTranslation(escoMapNls);

  return (
    <div className="escoLayerLegendControlItem">
      <div className="escoLayerLegendControlItem__title">
        {layer.get('title')}
      </div>
      {legendError ? (
        <div className="escoLayerLegendControlItem__placeholder--error">
          {translate('legendItemPlaceholder')}
        </div>
      ) : (
        <img
          alt={`${translate('legendImageText')}: ${layer.get('title')}`}
          onError={() => setLegendError(true)}
          src={layer.getSource().getLegendUrl(map.getView().getResolution())}
        />
      )}
    </div>
  );
};

LayerLegendItem.propTypes = {
  layer: PropTypes.shape({ get: PropTypes.func, getSource: PropTypes.func }),
};

export const LayerLegendControl = ({ initialExpand = true }) => {
  const [{ map }] = useMapContext();
  const translate = useTranslation(escoMapNls);
  const [expand, setExpand] = useState(initialExpand);
  const [, setRefresh] = useState({});

  useEffect(() => {
    if (!map) return;

    const refresh = () => {
      setRefresh({});
    };
    const view = map.getView();
    view.on('change:resolution', refresh);

    return () => view.un('change:resolution', refresh);
  }, [map]);

  const layersInLegend = map
    .getLayers()
    .getArray()
    .filter((layer) => layer.get('type') === 'wms' && layer.getVisible())
    .reverse();

  return (
    <div className="escoLayerLegendControl">
      <Tooltip
        title={translate(expand ? 'hideLegendTooltip' : 'showLegendTooltip')}
      >
        <Button
          disableRipple
          className="escoLayerControl__button"
          endIcon={expand ? <HideIcon /> : <ShowIcon />}
          onClick={() => setExpand(!expand)}
        >
          {translate('legendTitle')}
        </Button>
      </Tooltip>
      {expand ? (
        <div className="escoLayerLegendControl__content">
          {layersInLegend.length ? (
            layersInLegend.map((layer) => (
              <LayerLegendItem key={layer.get('name')} layer={layer} />
            ))
          ) : (
            <div className="escoLayerLegendControl__placeholder">
              {translate('legendPlaceholder')}
            </div>
          )}
        </div>
      ) : null}
    </div>
  );
};

LayerLegendControl.propTypes = {
  initialExpand: PropTypes.bool,
};
