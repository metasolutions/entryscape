import { useEffect } from 'react';
import PropTypes from 'prop-types';
import { TextField } from '@mui/material';
import RDFormsFieldWrapper from 'commons/components/rdforms/FieldWrapper';
import { useTranslation } from 'commons/hooks/useTranslation';
import escoEntryTypeNLS from 'commons/nls/escoEntryType.nls';

const UpgradeLinkField = ({
  link,
  setCreateEntryParams,
  setExtraFieldIsValid,
}) => {
  const translate = useTranslation(escoEntryTypeNLS);

  useEffect(() => {
    setCreateEntryParams({ link });
    setExtraFieldIsValid(true); // link is automatically generated, can assume it's valid
  }, [link, setCreateEntryParams, setExtraFieldIsValid]);

  return (
    <RDFormsFieldWrapper label={translate('webAddressLabel')} editor>
      <TextField value={link} disabled />
    </RDFormsFieldWrapper>
  );
};

UpgradeLinkField.propTypes = {
  link: PropTypes.string,
  setCreateEntryParams: PropTypes.func,
  setExtraFieldIsValid: PropTypes.func,
};

export default UpgradeLinkField;
