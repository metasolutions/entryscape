import { FIELD_TITLE, FIELD_DCT_DESCRIPTION } from 'models/utils/fieldDefinitions';

export const fields = [FIELD_TITLE, FIELD_DCT_DESCRIPTION];
