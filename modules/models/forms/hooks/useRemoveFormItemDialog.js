import { useCallback } from 'react';
import useGetMainDialog from 'commons/hooks/useGetMainDialog';
import { useTranslation } from 'commons/hooks/useTranslation';
import esmoFormsNLS from 'models/nls/esmoForms.nls';
import escoDialogsNLS from 'commons/nls/escoDialogs.nls';

const NLS_BUNDLES = [esmoFormsNLS, escoDialogsNLS];

const useRemoveFormItemDialog = () => {
  const translate = useTranslation(NLS_BUNDLES);

  const { getConfirmationDialog } = useGetMainDialog();

  const confirmRemoveFormItem = useCallback(async () => {
    const proceed = await getConfirmationDialog({
      content: translate('confirmRemoveItemMessage'),
      affirmLabel: translate('remove'),
      rejectLabel: translate('cancel'),
    });
    return proceed;
  }, [getConfirmationDialog, translate]);
  return { confirmRemoveFormItem };
};

export default useRemoveFormItemDialog;
