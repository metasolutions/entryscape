import PropTypes from 'prop-types';
import { Button } from '@mui/material';
import ArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import { useTranslation } from 'commons/hooks/useTranslation';
import {
  ActionsMenu,
  ActionsProvider,
  useListView,
  useActions,
} from 'commons/components/ListView';
import Tooltip from 'commons/components/common/Tooltip';
import './MultiCreateButton.scss';

const CreateButton = ({
  disabled,
  tooltip = 'createButtonTooltip',
  highlightId,
}) => {
  const { nlsBundles } = useListView();
  const { openMenu, isOpen, id } = useActions();
  const translate = useTranslation(nlsBundles);

  return (
    <Tooltip title={tooltip || translate('createButtonTooltip')}>
      <span>
        <Button
          onClick={openMenu}
          aria-controls={id}
          aria-haspopup="true"
          aria-expanded={isOpen ? 'true' : undefined}
          endIcon={<ArrowDownIcon />}
          disabled={disabled}
          data-highlight-id={highlightId}
        >
          {translate('createButtonLabel')}
        </Button>
      </span>
    </Tooltip>
  );
};

CreateButton.propTypes = {
  disabled: PropTypes.bool,
  tooltip: PropTypes.string,
  highlightId: PropTypes.string,
};

export const MultiCreateButton = ({ action, disabled }) => {
  const { nlsBundles } = useListView();
  const translate = useTranslation(nlsBundles);
  const { items, id: _createId, highlightId, ...actionParams } = action;

  const actionItems = action.items.map(
    ({ labelNlsKey, id, action: itemAction, ...menuItemProps }) => {
      return {
        ...menuItemProps,
        label: translate(labelNlsKey),
        action: {
          ...actionParams,
          ...itemAction,
        },
      };
    }
  );

  return (
    <ActionsProvider>
      <CreateButton
        disabled={disabled}
        tooltip={action.actionParams?.tooltip}
        highlightId={highlightId}
      />
      <ActionsMenu
        disabled
        menuProps={{
          anchorOrigin: {
            vertical: 'bottom',
            horizontal: 'right',
          },
          transformOrigin: {
            vertical: 'top',
            horizontal: 'right',
          },
          classes: { paper: 'escoListMultiCreateButton' },
        }}
        items={actionItems}
      />
    </ActionsProvider>
  );
};

MultiCreateButton.propTypes = {
  action: PropTypes.shape({
    id: PropTypes.string,
    items: PropTypes.arrayOf(PropTypes.shape({})),
    actionParams: PropTypes.shape({ tooltip: PropTypes.string }),
    highlightId: PropTypes.string,
  }),
  disabled: PropTypes.bool,
};
