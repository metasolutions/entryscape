import { dia, shapes } from 'jointjs';
import 'jointjs/css/layout.css';

export * from './paper';
export * from './layout';
export * from './link';
export * from './shapes';

export const createGraph = () => new dia.Graph({}, { cellNamespace: shapes });
