import { useEffect } from 'react';
import { EntryStore } from '@entryscape/entrystore-js';
import useAsync from 'commons/hooks/useAsync';

import config from 'config';

const fetchDatasetTemplateEntry = async (resourceURI, storeURI) => {
  if (!storeURI) {
    return null;
  }

  const datasetTemplateEntrystore = new EntryStore(storeURI);
  datasetTemplateEntrystore.getREST().disableJSONP();
  datasetTemplateEntrystore.getREST().disableCredentials();

  const searchList = datasetTemplateEntrystore
    .newSolrQuery()
    .resource(resourceURI)
    .limit(1)
    .list();
  const entries = await searchList.getEntries();
  return entries[0];
};

const useGetDatasetTemplateEntry = (entry) => {
  const { data: datasetTemplateEntry, status, runAsync } = useAsync(null);
  const datasetTemplateStoreURI = config.get('catalog.datasetTemplateEndpoint');
  const basedOnURI = entry
    .getMetadata()
    .findFirstValue(entry.getResourceURI(), 'esterms:basedOn');

  useEffect(() => {
    if (basedOnURI) {
      runAsync(fetchDatasetTemplateEntry(basedOnURI, datasetTemplateStoreURI));
    }
  }, [basedOnURI, runAsync, datasetTemplateStoreURI]);

  return [datasetTemplateEntry, basedOnURI ? status : 'resolved'];
};

export default useGetDatasetTemplateEntry;
