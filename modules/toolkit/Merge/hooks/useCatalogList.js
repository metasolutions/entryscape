import { useReducer } from 'react';
import { CatalogDetect } from '../utils/CatalogDetect';

const getMaxId = (list) =>
  list.reduce((maxId, item) => (item.id > maxId ? item.id : maxId), 0);

const catalogListReducer = (catalogDetectList, action) => {
  switch (action.type) {
    case 'add': {
      return [...catalogDetectList, action.catalogDetect];
    }
    case 'remove':
      return catalogDetectList.filter(
        (catalogDetect) => catalogDetect !== action.catalogDetect
      );
    case 'clear':
      return [];
    default:
      throw new Error('Unknown action type in catalogListReducer');
  }
};

const useCatalogList = () => {
  const [catalogDetectList, dispatch] = useReducer(catalogListReducer, []);

  const addCatalogDetect = (catalog) => {
    const maxId = getMaxId(catalogDetectList);
    const catalogDetect = CatalogDetect(catalog, maxId + 1);
    const error = catalogDetect.validateCount();
    if (error) {
      throw new Error(error);
    }
    dispatch({ type: 'add', catalogDetect });
  };

  return {
    addCatalogDetect,
    catalogDetectList,
    dispatch,
  };
};

export default useCatalogList;
