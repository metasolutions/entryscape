import PropTypes from 'prop-types';
import { Grid } from '@mui/material';
import GridList from 'commons/components/common/GridList';
import './OverviewMetadataList.scss';

const OverviewMetadataList = ({ children, ...gridListProps }) => {
  return (
    <Grid container justifyContent="flex-start" direction="row">
      <GridList
        gap={10}
        rowHeight="auto"
        classes={{
          root: 'escoOverview__MetadataList',
        }}
        {...gridListProps}
      >
        {children}
      </GridList>
    </Grid>
  );
};

OverviewMetadataList.propTypes = {
  cols: PropTypes.number,
  children: PropTypes.node,
};

export default OverviewMetadataList;
