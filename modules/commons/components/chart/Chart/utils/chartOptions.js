const defaultYAxis = {
  ticks: {
    beginAtZero: true,
  },
};

const getScaleLabel = (labelString) => ({
  scaleLabel: {
    display: true,
    labelString,
  },
});

export const getScaleOptions = (xAxis, yAxis) => {
  const scales = {
    yAxes: yAxis
      ? [{ ...defaultYAxis, ...getScaleLabel(yAxis) }]
      : [defaultYAxis],
  };
  if (xAxis) scales.xAxes = [getScaleLabel(xAxis)];
  return { scales };
};

const defaultOptions = {
  maintainAspectRatio: false,
  ...getScaleOptions(),
};

export const getChartOptions = (options = {}) => {
  return {
    ...defaultOptions,
    ...options,
  };
};
