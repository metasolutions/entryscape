import React, { useState, useEffect, useCallback } from 'react';
import PropTypes from 'prop-types';
import { Entry } from '@entryscape/entrystore-js';
import {
  Grid,
  IconButton,
  Button,
  Divider as MuiDivider,
  Typography,
} from '@mui/material';
import {
  Edit as EditIcon,
  Delete as DeleteIcon,
  Add,
} from '@mui/icons-material';
import HasOverridesIcon from '@mui/icons-material/StickyNote2';
import useAsync from 'commons/hooks/useAsync';
import {
  ActionsMenu,
  ActionsProvider,
  OpenActionsMenuButton,
} from 'commons/components/ListView';
import AddIconButton from 'commons/components/IconButton';
import ContentWrapper from 'commons/components/common/ContentWrapper';
import ListActionDialog from 'commons/components/common/dialogs/ListActionDialog';
import Tooltip from 'commons/components/common/Tooltip';
import { useTranslation } from 'commons/hooks/useTranslation';
import esmoFormsNLS from 'models/nls/esmoForms.nls';
import esmoCommonsNLS from 'models/nls/esmoCommons.nls';
import escoFormsNLS from 'commons/nls/escoForms.nls';
import escoDialogsNLS from 'commons/nls/escoDialogs.nls';
import {
  getFormItemEntries,
  updateOrderInMetadata,
  getBlankFromExtend,
  getLabelFromExtended,
  removeFormItemInMetadata,
  addFormItemInMetadata,
} from 'models/forms/utils/util';
import {
  DraggableList,
  DraggableListItem,
} from 'commons/components/DraggableList';
import Divider from 'commons/components/Divider';
import { ConfirmLeaveDialog } from 'commons/components/common/dialogs/ConfirmLeaveDialog';
import useRemoveFormItemDialog from 'models/forms/hooks/useRemoveFormItemDialog';
import EditFormItemsDialog from 'models/forms/FormOverview/dialogs/EditFormItemsDialog';
import {
  EditorProvider,
  useEditorContext,
} from 'commons/components/forms/editors/EditorContext';
import { isForm, getFormLabel } from 'models/forms/utils/forms';
import { FieldPresenter } from 'models/presenters';
import { getFieldLabel } from 'models/fields/utils/fields';
import {
  primaryFields,
  secondaryFields,
} from 'models/fields/utils/fieldPresenters';
import {
  primaryFields as primaryFormFields,
  secondaryFields as secondaryFormFields,
} from 'models/forms/utils/formPresenters';
import { RDF_PROPERTY_EXTENDS } from 'models/utils/ns';
import useGetMainDialog from 'commons/hooks/useGetMainDialog';
import NoteAddIcon from '@mui/icons-material/NoteAdd';
import { useSnackbar, SUCCESS_EDIT } from 'commons/hooks/useSnackbar';
import ExternalizeDialog from '../ExternalizeDialog';
import AddItemDialog from '../AddItemDialog';
import './FormItemsDialog.scss';

const NLS_BUNDLES = [
  esmoFormsNLS,
  esmoCommonsNLS,
  escoFormsNLS,
  escoDialogsNLS,
];

const FormItemsDialog = ({ formEntry, closeDialog }) => {
  const { runAsync } = useAsync({ data: [] });
  const { graph, setGraph, resourceURI } = useEditorContext();
  const [formItemEntries, setFormItemEntries] = useState([]);
  const [selectedFormItemEntry, setSelectedFormItemEntry] = useState(null);
  const [hasMetadataChanges, setHasMetadataChanges] = useState(false);
  const [openAddItemDialog, setOpenAddItemDialog] = useState(false);
  const [openEditItemDialog, setOpenEditItemDialog] = useState(false);
  const [overridenValues, setOverridenValues] = useState({});
  const [refreshNeeded, setRefreshNeeded] = useState(true);
  const translate = useTranslation(NLS_BUNDLES);
  const { getConfirmationDialog } = useGetMainDialog();
  const [addSnackbar] = useSnackbar();

  const { confirmRemoveFormItem } = useRemoveFormItemDialog();

  useEffect(() => {
    if (graph) {
      graph.onChange = () => {
        setHasMetadataChanges(true);
      };
    }
  }, [graph]);

  useEffect(() => {
    if (graph === null || !refreshNeeded) return;
    runAsync(
      getFormItemEntries(graph, resourceURI).then((orderedFormItemEntries) => {
        setFormItemEntries(orderedFormItemEntries);
        if (orderedFormItemEntries.length && !selectedFormItemEntry) {
          setSelectedFormItemEntry(orderedFormItemEntries[0]);
        }
        setRefreshNeeded(false);
      })
    );
  }, [graph, resourceURI, runAsync, refreshNeeded, selectedFormItemEntry]);

  const handleSaveChanges = useCallback(
    (externalizedEntry) => {
      if (externalizedEntry) {
        setRefreshNeeded(true);
        setSelectedFormItemEntry(externalizedEntry);
      }
      setHasMetadataChanges(false);
      setOverridenValues({});
      addSnackbar({ type: SUCCESS_EDIT });
    },
    [addSnackbar]
  );

  const saveFormItems = () => {
    formEntry
      .setMetadata(graph)
      .commitMetadata()
      .then(() => {
        addSnackbar({ type: SUCCESS_EDIT });
        closeDialog();
      });
  };

  const closeAddItemDialogDialog = () => {
    setOpenAddItemDialog(false);
  };

  const getPrimaryFields = () => {
    if (!selectedFormItemEntry) return [];
    return isForm(selectedFormItemEntry) ? primaryFormFields : primaryFields;
  };

  const getSecondaryFields = () => {
    if (!selectedFormItemEntry) return [];
    return isForm(selectedFormItemEntry)
      ? secondaryFormFields
      : secondaryFields;
  };

  const saveAction = (
    <Button autoFocus onClick={saveFormItems} disabled={!hasMetadataChanges}>
      {translate('save')}
    </Button>
  );

  const removeFormItemEntry = async () => {
    const proceed = await confirmRemoveFormItem(
      selectedFormItemEntry,
      graph,
      resourceURI
    );
    if (!proceed) return;
    const newFormItemEntries = formItemEntries.filter(
      (formItemEntry) => formItemEntry !== selectedFormItemEntry
    );
    setFormItemEntries(newFormItemEntries);
    setSelectedFormItemEntry(null);
    setRefreshNeeded(true);
    removeFormItemInMetadata(selectedFormItemEntry, graph, resourceURI);
    updateOrderInMetadata(newFormItemEntries, graph);
  };

  const onDragEnd = (reorder, source, destination) => {
    if (!destination) return; // dropped outside the list
    if (source.index !== destination.index) {
      const reorderedFormItemEntries = reorder(
        formItemEntries,
        source.index,
        destination.index
      );
      setFormItemEntries(reorderedFormItemEntries);
      updateOrderInMetadata(reorderedFormItemEntries, graph);
    }
  };

  const handleAddFormItemEntry = (formItemEntry) => {
    const newFormItemEntries = [...formItemEntries, formItemEntry];
    setFormItemEntries(newFormItemEntries);
    setSelectedFormItemEntry(newFormItemEntries[newFormItemEntries.length - 1]);
    addFormItemInMetadata(
      formItemEntry,
      formItemEntries.length + 2,
      graph,
      resourceURI
    );
  };

  const handleEditFormItem = (newGraph) => {
    setGraph(newGraph);
    setHasMetadataChanges(true);
    setOverridenValues((prev) => ({
      ...prev,
      [selectedFormItemEntry.getId()]: true,
    }));
  };

  const closeEditItemDialog = () => {
    setOpenEditItemDialog(false);
  };

  const handleClose = async () => {
    if (!hasMetadataChanges) {
      closeDialog();
      return;
    }
    const proceed = await getConfirmationDialog({
      rejectLabel: translate('keepChanges'),
      affirmLabel: translate('discardChanges'),
      content: translate('discardChangesWarning'),
    });
    if (!proceed) return;
    closeDialog();
  };

  // helper fn to find out if a form item has overridden values
  const checkHasOverrides = (formItemEntry) => {
    const [statement] = graph.find(
      null,
      RDF_PROPERTY_EXTENDS,
      formItemEntry.getResourceURI()
    );
    if (!statement) return false;
    const properties = graph.findProperties(statement.getSubject());

    // there are three mandatory properties.
    // Any extra property indicates that it is an extended property.
    return properties.length > 3;
  };

  const actionsMenuItems = [
    {
      id: 'remove',
      label: translate('remove'),
      icon: <DeleteIcon />,
      onClick: removeFormItemEntry,
    },
    {
      id: 'externalize',
      action: {
        Dialog: ExternalizeDialog,
        formItemEntry: selectedFormItemEntry,
        formEntry,
        graph,
        onSave: handleSaveChanges,
        hasChanges: hasMetadataChanges,
      },
      isVisible: (formItemEntry) =>
        formItemEntry &&
        (checkHasOverrides(formItemEntry) ||
          overridenValues[formItemEntry.getId()]),
      label: translate('externalize'),
      icon: <NoteAddIcon />,
    },
  ].filter(({ isVisible = () => true }) => isVisible(selectedFormItemEntry));

  return (
    <ListActionDialog
      id="select-user-dialog"
      closeDialog={handleClose}
      actions={saveAction}
      title={translate('formItemsDialogHeader')}
      fixedHeight
      maxWidth="xl"
      fullWidth
    >
      <ContentWrapper>
        {!refreshNeeded ? (
          <div className="esmoTwoColView">
            <div className=" escoTwoColView__Sidebar esmoList__sidebar">
              <Grid className="esmoList__sidebarHead">
                <div className="esmoSidebar__sidebarHeader">
                  <div className="esmoSidebar__formDetails">
                    <h2>{translate('formItemsTitle')}</h2>
                  </div>
                  <Tooltip title={translate('addFormItemsTooltip')}>
                    <AddIconButton
                      onClick={() => {
                        setOpenAddItemDialog(true);
                      }}
                      icon={<Add />}
                      className="esmoSidebar__addButton"
                    />
                  </Tooltip>
                </div>
              </Grid>
              <MuiDivider />
              <div className="esmoList__sidebarBody">
                {formItemEntries.length ? (
                  <DraggableList onDragEnd={onDragEnd}>
                    {formItemEntries.map((formItemEntry, index) => (
                      <DraggableListItem
                        id={formItemEntry.getId()}
                        index={index}
                        selected={selectedFormItemEntry === formItemEntry}
                        onClick={() => setSelectedFormItemEntry(formItemEntry)}
                        key={formItemEntry.getId()}
                        primaryText={getLabelFromExtended(graph, formItemEntry)}
                        icon={
                          (overridenValues[formItemEntry.getId()] && (
                            <HasOverridesIcon color="primary" />
                          )) ||
                          (checkHasOverrides(formItemEntry) && (
                            <HasOverridesIcon color="secondary" />
                          )) ||
                          null
                        }
                        iconTooltipLabel={translate('overridenValues')}
                      />
                    ))}
                  </DraggableList>
                ) : null}
              </div>
            </div>
            {selectedFormItemEntry ? (
              <div className="esmoFormOverview__main">
                <div className="esmoFormItemsDialogHeader">
                  <h2 className="esmoFormItemsDialogHeader_heading">
                    {getLabelFromExtended(graph, selectedFormItemEntry)}
                  </h2>
                  <div className="esmoFormItemsDialogHeader_actions">
                    <Grid item>
                      <Tooltip title={translate('editEntry')}>
                        <IconButton
                          aria-label={translate('editEntry')}
                          onClick={() => setOpenEditItemDialog(true)}
                        >
                          <EditIcon />
                        </IconButton>
                      </Tooltip>
                      <ActionsProvider>
                        <OpenActionsMenuButton />
                        <ActionsMenu items={actionsMenuItems} />
                      </ActionsProvider>
                    </Grid>
                  </div>
                </div>
                <Typography
                  variant="h3"
                  className="esmoFormItemsDialogHeader__subheader"
                >
                  {isForm(selectedFormItemEntry)
                    ? getFormLabel(selectedFormItemEntry, translate)
                    : getFieldLabel(selectedFormItemEntry, translate)}
                </Typography>
                {getPrimaryFields().map((field) => (
                  <FieldPresenter
                    key={field.property}
                    graph={graph}
                    resourceURI={getBlankFromExtend(
                      graph,
                      selectedFormItemEntry.getResourceURI()
                    )}
                    {...field}
                  />
                ))}
                <Divider />
                {getSecondaryFields().map((field) => (
                  <FieldPresenter
                    key={field.property}
                    graph={graph}
                    resourceURI={getBlankFromExtend(
                      graph,
                      selectedFormItemEntry.getResourceURI()
                    )}
                    {...field}
                  />
                ))}
                <ConfirmLeaveDialog shouldBlock={hasMetadataChanges} />
              </div>
            ) : (
              <div className="esmoFormOverview__emptyPlaceholder">
                {translate('emptyPlaceHolder')}
              </div>
            )}
          </div>
        ) : null}
      </ContentWrapper>
      {openAddItemDialog && (
        <AddItemDialog
          closeDialog={closeAddItemDialogDialog}
          selectedEntries={formItemEntries}
          entry={formEntry}
          onSelect={handleAddFormItemEntry}
        />
      )}
      {openEditItemDialog && (
        <EditorProvider
          graph={graph}
          resourceURI={getBlankFromExtend(
            graph,
            selectedFormItemEntry.getResourceURI()
          )}
        >
          <EditFormItemsDialog
            closeDialog={closeEditItemDialog}
            extendedEntry={selectedFormItemEntry}
            onAccept={handleEditFormItem}
          />
        </EditorProvider>
      )}
    </ListActionDialog>
  );
};

FormItemsDialog.propTypes = {
  formEntry: PropTypes.instanceOf(Entry),
  closeDialog: PropTypes.func,
};

export default FormItemsDialog;
