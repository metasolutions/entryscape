import { Box } from '@mui/material';
import PropTypes from 'prop-types';
import './FieldsLabel.scss';

export const FieldsLabel = ({
  alignItems = 'center',
  label,
  id,
  children,
  onClick,
}) => {
  return (
    <Box
      alignItems={alignItems}
      className="escoFieldsLabel"
      id={id}
      onClick={onClick}
    >
      <span className="escoFieldsLabel__label">{label}</span>
      {children}
    </Box>
  );
};

FieldsLabel.propTypes = {
  alignItems: PropTypes.string,
  label: PropTypes.string,
  id: PropTypes.string,
  children: PropTypes.node,
  onClick: PropTypes.func,
};

export default FieldsLabel;
