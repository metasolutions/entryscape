import PropTypes from 'prop-types';
import {
  nlsBundlesPropType,
  useTranslation,
} from 'commons/hooks/useTranslation';
import { FieldEditor as DefaultFieldGroupEditor } from '../FieldEditor';

export const Form = ({
  nlsBundles,
  fields,
  size,
  FieldGroupEditor = DefaultFieldGroupEditor,
  editorProps = {},
}) => {
  const translate = useTranslation(nlsBundles);

  return (
    <form autoComplete="off">
      {fields.map(({ name, labelNlsKey, ...fieldProps }) => {
        return (
          <FieldGroupEditor
            key={name || fieldProps.property}
            size={size}
            translate={translate}
            nlsBundles={nlsBundles}
            label={translate(labelNlsKey)}
            name={name}
            {...editorProps}
            {...fieldProps}
          />
        );
      })}
    </form>
  );
};

Form.propTypes = {
  nlsBundles: nlsBundlesPropType,
  fields: PropTypes.arrayOf(PropTypes.shape({})),
  size: PropTypes.string,
  FieldGroupEditor: PropTypes.func,
  editorProps: PropTypes.shape({}),
};
