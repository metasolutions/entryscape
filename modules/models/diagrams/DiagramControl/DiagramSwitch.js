import React, { useState } from 'react';
import PropTypes from 'prop-types';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import ListItemText from '@mui/material/ListItemText';
import ListItemIcon from '@mui/material/ListItemIcon';
import Divider from '@mui/material/Divider';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import AddIcon from '@mui/icons-material/Add';
import { useESContext } from 'commons/hooks/useESContext';
import { withListModelProvider } from 'commons/components/ListView/hooks/useListModel';
import CreateDiagramDialog from 'models/diagrams/dialogs/CreateDiagramDialog';
import useAddIgnore from 'commons/errors/hooks/useAddIgnore';
import { GENERIC_PROBLEM } from 'commons/errors/utils/async';
import { getDiagramLabel } from 'models/diagrams/utils/diagram';
import { DEFAULT_DIAGRAM_ID } from 'models/diagrams/utils/definitions';
import { useTranslation } from 'commons/hooks/useTranslation';
import esmoDiagramsNLS from 'models/nls/esmoDiagrams.nls';
import DiagramOption from './DiagramOption';
import useGetDiagramEntries from '../hooks/useGetDiagramEntries';
import {
  SET_DIAGRAM_ENTRY_ID,
  useDiagramContext,
} from '../hooks/useDiagramContext';

const DiagramSwitch = ({ onEdit }) => {
  const translate = useTranslation([esmoDiagramsNLS]);
  const { context } = useESContext();
  const [createDiagramDialogOpen, setCreateDiagramDialogOpen] = useState(false);
  useAddIgnore('getEntry', GENERIC_PROBLEM, false);
  const { diagramEntry, diagramEntryId, dispatch } = useDiagramContext();
  const { diagramEntries } = useGetDiagramEntries(context);
  const [anchorEl, setAnchorEl] = useState(null);
  const handlePopoverOpen = (event) => setAnchorEl(event.currentTarget);
  const handlePopoverClose = () => setAnchorEl(null);
  const open = Boolean(anchorEl);
  const switchDiagram = (event) => {
    setAnchorEl(null);
    dispatch({
      type: SET_DIAGRAM_ENTRY_ID,
      diagramEntryId: event.currentTarget.getAttribute('value'),
    });
  };

  const defaultDiagramEntry = diagramEntries.find(
    (entry) => entry.getId() === DEFAULT_DIAGRAM_ID
  );

  return (
    <>
      <Button
        id="diagram-switch-heading"
        className="esmoDiagramControl__headingButton"
        aria-controls={open ? 'diagram-menu' : undefined}
        aria-haspopup="true"
        aria-expanded={open ? 'true' : undefined}
        variant="text"
        disableElevation
        onClick={handlePopoverOpen}
        endIcon={<KeyboardArrowDownIcon />}
      >
        <Typography className="esmoDiagramControl__heading" variant="h1">
          {getDiagramLabel(
            diagramEntry,
            translate,
            diagramEntryId === DEFAULT_DIAGRAM_ID
          )}
        </Typography>
      </Button>
      <Menu
        id="diagram-menu"
        anchorEl={anchorEl}
        open={open}
        MenuListProps={{
          'aria-labelledby': 'diagram-switch-heading',
          role: 'listbox',
        }}
        slotProps={{
          paper: {
            style: {
              maxHeight: 500,
            },
          },
        }}
        onClose={handlePopoverClose}
      >
        <MenuItem
          selected={diagramEntryId === DEFAULT_DIAGRAM_ID}
          value={DEFAULT_DIAGRAM_ID}
          onClick={switchDiagram}
        >
          <div style={{ display: 'flex', flexDirection: 'column' }}>
            <DiagramOption diagramEntry={defaultDiagramEntry} defaultDiagram />
          </div>
        </MenuItem>
        {diagramEntries
          .filter((entry) => entry.getId() !== DEFAULT_DIAGRAM_ID)
          .map((entry) => (
            <MenuItem
              key={`entry-${entry.getId()}`}
              selected={entry.getId() === diagramEntryId}
              value={entry.getId()}
              onClick={switchDiagram}
            >
              <DiagramOption diagramEntry={entry} />
            </MenuItem>
          ))}
        <Divider />
        <MenuItem onClick={() => setCreateDiagramDialogOpen(true)}>
          <ListItemIcon>
            <AddIcon fontSize="medium" />
          </ListItemIcon>
          <ListItemText>{translate('createNewDiagram')}</ListItemText>
        </MenuItem>
      </Menu>
      {createDiagramDialogOpen ? (
        <CreateDiagramDialog
          closeDialog={() => setCreateDiagramDialogOpen(false)}
          onEntryEdit={(newEntry) => {
            dispatch({
              type: SET_DIAGRAM_ENTRY_ID,
              diagramEntryId: newEntry.getId(),
            });
            onEdit();
          }}
        />
      ) : null}
    </>
  );
};
DiagramSwitch.propTypes = {
  onEdit: PropTypes.func,
};

export default withListModelProvider(DiagramSwitch);
