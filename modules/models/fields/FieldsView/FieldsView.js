import { useCallback } from 'react';
import { getPathFromViewName } from 'commons/util/site';
import { useESContext } from 'commons/hooks/useESContext';
import { RDF_TYPE_FIELD } from 'models/utils/ns';
import { FIELDS_FORMS_INFO } from 'models/utils/fieldDefinitions';
import { entrystore } from 'commons/store';
import { applyQueryParams as applyDefaultQueryParams } from 'commons/util/solr';
import {
  withListModelProviderAndLocation,
  withListRerender,
  listPropsPropType,
} from 'commons/components/ListView';
import {
  EntryListView,
  useSolrQuery,
  TITLE_COLUMN,
  MODIFIED_COLUMN,
  PUBLIC_STATUS_COLUMN,
  ACTIONS_GROUP_COLUMN,
} from 'commons/components/EntryListView';
import {
  LIST_ACTION_EDIT,
  LIST_ACTION_INFO,
} from 'commons/components/EntryListView/actions';
import { getLabel } from 'commons/util/rdfUtils';
import useSearchFilters from 'commons/components/filters/hooks/useSearchFilters';
import FieldsLinkedDataBrowserDialog from 'commons/components/FieldsLinkedDataBrowserDialog';
import { withEditorProvider } from 'commons/components/forms/editors';
import { getPurpose } from '../../utils/metadata';
import { listActions, nlsBundles, filters } from './actions';
import EditFieldDialog from '../dialogs/EditFieldDialog';
import editSections from '../utils/fieldEditors';

const FieldsView = ({ listProps }) => {
  const { context } = useESContext();

  const createQuery = useCallback(
    () => entrystore.newSolrQuery().rdfType(RDF_TYPE_FIELD).context(context),
    [context]
  );

  const { applyFilters, hasFilters, ...filterProps } =
    useSearchFilters(filters);

  const applyQueryParams = useCallback(
    (query, params) => {
      applyDefaultQueryParams(query, params);
      applyFilters(query);
    },
    [applyFilters]
  );

  const queryResults = useSolrQuery({ createQuery, applyQueryParams });

  return (
    <EntryListView
      {...queryResults}
      {...listProps}
      nlsBundles={nlsBundles}
      includeFilters={hasFilters}
      filtersProps={filterProps}
      listActions={listActions}
      getListItemProps={({ entry }) => ({
        to: getPathFromViewName('models__fields__field', {
          contextId: context.getId(),
          entryId: entry.getId(),
        }),
      })}
      columns={[
        {
          ...PUBLIC_STATUS_COLUMN,
          getProps: ({ entry, translate }) => ({
            entry,
            publicTooltip: translate('publicTitle'),
            privateTooltip: translate('privateTitle'),
          }),
        },
        {
          ...TITLE_COLUMN,
          xs: 8,
          getProps: ({ entry }) => ({
            primary: getLabel(entry),
            secondary: getPurpose({ entry }),
          }),
        },
        MODIFIED_COLUMN,
        {
          ...ACTIONS_GROUP_COLUMN,
          actions: [
            {
              ...LIST_ACTION_INFO,
              nlsBundles,
              Dialog: FieldsLinkedDataBrowserDialog,
              fields: FIELDS_FORMS_INFO,
            },
            {
              ...LIST_ACTION_EDIT,
              nlsBundles,
              Dialog: withListRerender(
                withEditorProvider(EditFieldDialog, editSections),
                'onEntryEdit'
              ),
            },
          ],
        },
      ]}
    />
  );
};

FieldsView.propTypes = {
  listProps: listPropsPropType,
};

export default withListModelProviderAndLocation(FieldsView);
