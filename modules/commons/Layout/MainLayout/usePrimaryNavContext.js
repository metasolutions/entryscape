import { useContext, createContext } from 'react';

export const PrimaryNavContext = createContext({});

export const usePrimaryNavContext = () => {
  const primaryNavContext = useContext(PrimaryNavContext);

  if (primaryNavContext === undefined) {
    throw new Error(
      'usePrimaryNavContext must be used within a ContextProvider'
    );
  }

  return primaryNavContext;
};
