import PropTypes from 'prop-types';
import { Entry } from '@entryscape/entrystore-js';
import ListActionDialog from 'commons/components/common/dialogs/ListActionDialog';
import {
  Grid,
  Typography,
  List,
  ListItem,
  ListItemText,
  Link,
  TextField,
  InputAdornment,
  IconButton,
} from '@mui/material';
import Tooltip from 'commons/components/common/Tooltip';
import {
  Edit as EditIcon,
  Save as SaveIcon,
  Close as CloseIcon,
} from '@mui/icons-material';
import ContentWrapper from 'commons/components/common/ContentWrapper';
import {
  useTranslation,
  nlsBundlesPropType,
} from 'commons/hooks/useTranslation';
import useAsync from 'commons/hooks/useAsync';
import useAsyncCallback from 'commons/hooks/useAsyncCallback';
import {
  updateAlias,
  getApiInformation,
  getExampleURL,
} from 'catalog/datasets/DatasetOverview/utils/api';
import { getPipelineResultsEntry } from 'catalog/datasets/DatasetOverview/utils/distributions';
import { useEffect, useState } from 'react';
import { isAlphaNumeric } from 'commons/util/util';
import './index.scss';

const STATUS_COLOR = {
  available: 'escaApiInfo__status-green',
  error: 'escaApiInfo__status-red',
};

const ApiInfoDialog = ({
  entry: distributionEntry,
  nlsBundles,
  closeDialog,
}) => {
  const defaultHelperText = 'apiAliasNamePlaceholder';
  const t = useTranslation(nlsBundles);
  const [pipelineResultsEntry] = useAsyncCallback(
    getPipelineResultsEntry,
    distributionEntry
  );
  const { data: apiData, runAsync } = useAsync(null);
  const [alias, setAlias] = useState(null);
  const [aliasError, setAliasError] = useState('');
  const [exampleURL, setExampleURL] = useState(null);
  const [onEditMode, setOnEditMode] = useState(false);

  useEffect(() => {
    if (!pipelineResultsEntry) return;
    runAsync(getApiInformation(pipelineResultsEntry));
  }, [pipelineResultsEntry, runAsync]);

  useEffect(() => {
    if (!apiData) return;
    const { apiAlias, columnNames } = apiData;
    if (columnNames?.length) {
      const apiExampleURL = getExampleURL(
        pipelineResultsEntry.getResourceURI(),
        apiAlias,
        columnNames[0]
      );
      setExampleURL(apiExampleURL);
    }
    if (apiAlias) {
      setAlias(apiAlias);
    }
  }, [apiData, pipelineResultsEntry]);

  useEffect(() => {
    if (!isAlphaNumeric(alias) && Boolean(alias)) {
      setAliasError('invalidAliasName');
      return;
    }
    setAliasError('');
  }, [alias]);

  const handleCancel = () => {
    if (apiData.apiAlias) {
      setAlias(apiData.apiAlias);
    } else {
      setAlias(null);
    }
    setAliasError('');
    setOnEditMode(false);
  };

  const handleChange = ({ target }) => setAlias(target.value);

  const handleSaveAlias = async () => {
    if (!alias?.length) {
      setAlias(null);
    }
    try {
      const updatedEntry = await updateAlias(pipelineResultsEntry, alias);
      if (updatedEntry) {
        const updatedExampleURL = getExampleURL(
          pipelineResultsEntry.getResourceURI(),
          alias,
          apiData.columnNames[0]
        );
        setAlias(updatedEntry.apiAlias);
        setExampleURL(updatedExampleURL);
        runAsync(getApiInformation(pipelineResultsEntry));
      }
      setOnEditMode(false);
    } catch (error) {
      setAliasError('duplicateAliasName');
      console.error(`Failed to update API alias: ${error}`);
    }
  };

  return (
    <ListActionDialog
      id="api-info"
      closeDialogButtonLabel={t('apiInfoDialogCloseLabel')}
      maxWidth="md"
      title={t('apiInfoDialogHeader')}
      closeDialog={closeDialog}
    >
      <ContentWrapper>
        {apiData ? (
          <Grid container direction="row">
            <Grid
              item
              container
              className="escaApiInfo__column"
              direction="column"
            >
              <Grid item>
                <Typography
                  className="escaApiInfo__header"
                  variant="body1"
                  component="h3"
                  gutterBottom
                >
                  {t('apiStatusLabel')}
                </Typography>
              </Grid>
              <Grid item>
                <Typography
                  className={`${
                    STATUS_COLOR[apiData.apiStatus]
                  } escaApiInfo__header`}
                  variant="body1"
                  gutterBottom
                >
                  {apiData.apiStatus
                    ? t(`apiStatus_${apiData.apiStatus}`)
                    : t('missingAPIStatus')}
                </Typography>
              </Grid>
            </Grid>
            <Grid
              item
              container
              className="escaApiInfo__column"
              direction="column"
            >
              <Grid item>
                <Typography
                  className="escaApiInfo__header"
                  variant="body1"
                  component="h3"
                  gutterBottom
                >
                  {t('apiId')}
                </Typography>
              </Grid>
              <Grid item>
                <Typography variant="body1" gutterBottom>
                  {apiData.identifier}
                </Typography>
              </Grid>
            </Grid>
            <Grid
              item
              container
              className="escaApiInfo__column"
              direction="column"
            >
              <Grid item>
                {onEditMode ? (
                  <TextField
                    label={t('apiAlias')}
                    id="concept-edit-name"
                    value={alias || ''}
                    onChange={handleChange}
                    error={Boolean(aliasError)}
                    helperText={
                      aliasError ? t(aliasError) : t(defaultHelperText)
                    }
                    InputProps={{
                      endAdornment: (
                        <InputAdornment position="end">
                          <Tooltip title={t('cancelEditAliasLabel')}>
                            <IconButton
                              aria-label={t('cancelEditAliasLabel')}
                              onClick={() => handleCancel()}
                            >
                              <CloseIcon />
                            </IconButton>
                          </Tooltip>
                          <Tooltip title={t('apiSaveAliasLabel')}>
                            <IconButton
                              aria-label={t('apiSaveAliasLabel')}
                              onClick={handleSaveAlias}
                              disabled={Boolean(aliasError)}
                            >
                              <SaveIcon />
                            </IconButton>
                          </Tooltip>
                        </InputAdornment>
                      ),
                    }}
                  />
                ) : (
                  <TextField
                    disabled
                    label={t('apiAlias')}
                    id="edit-alias"
                    value={alias || ''}
                    InputProps={{
                      endAdornment: (
                        <InputAdornment position="end">
                          <Tooltip title={t('editAliasLabel')}>
                            <IconButton
                              aria-label={t('editAliasLabel')}
                              onClick={() => setOnEditMode(true)}
                            >
                              <EditIcon />
                            </IconButton>
                          </Tooltip>
                        </InputAdornment>
                      ),
                    }}
                  />
                )}
              </Grid>
            </Grid>
            {apiData.columnNames && exampleURL ? (
              <Grid
                item
                container
                className="escaApiInfo__column"
                direction="column"
              >
                <Grid item>
                  <Typography
                    className="escaApiInfo__header"
                    variant="body1"
                    component="h3"
                    gutterBottom
                  >
                    {t('searchExampleLabel')}
                  </Typography>
                </Grid>
                <Grid item>
                  <Typography variant="body1" gutterBottom>
                    <Link aria-live="polite" target="_blank" href={exampleURL}>
                      {exampleURL}
                    </Link>
                  </Typography>
                </Grid>
              </Grid>
            ) : null}
            <Grid container className="escaApiInfo__column" direction="column">
              <Grid item>
                <Typography
                  className="escaApiInfo__header"
                  variant="body1"
                  component="h3"
                  gutterBottom
                >
                  {t('columnLabel')}
                </Typography>
              </Grid>
              <Grid item>
                {apiData.columnNames?.length ? (
                  <List>
                    {apiData.columnNames.map((columnName) => (
                      <ListItem
                        className="escaApiInfo__apiColumnsList"
                        key={columnName}
                      >
                        <ListItemText primary={columnName} />
                      </ListItem>
                    ))}
                  </List>
                ) : (
                  <Typography variant="body1">{t('missingColumns')}</Typography>
                )}
              </Grid>
            </Grid>
          </Grid>
        ) : null}
      </ContentWrapper>
    </ListActionDialog>
  );
};

ApiInfoDialog.propTypes = {
  entry: PropTypes.instanceOf(Entry),
  nlsBundles: nlsBundlesPropType,
  closeDialog: PropTypes.func.isRequired,
};

export default ApiInfoDialog;
