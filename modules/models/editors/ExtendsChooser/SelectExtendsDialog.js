import React, { useCallback } from 'react';
import PropTypes from 'prop-types';
import { entrystore } from 'commons/store';
import { useESContext } from 'commons/hooks/useESContext';
import esmoFieldsNLS from 'models/nls/esmoFields.nls';
import esmoCommonsNLS from 'models/nls/esmoCommons.nls';
import escoListNLS from 'commons/nls/escoList.nls';
import escoDialogsNLS from 'commons/nls/escoDialogs.nls';
import {
  STATUS_FILTER,
  MODEL_FILTER,
  TYPE_FILTER,
  getModelItems,
} from 'models/utils/filterDefinitions';
import { FIELDS_FORMS_INFO } from 'models/utils/fieldDefinitions';
import { ANY_FILTER_ITEM } from 'commons/components/filters/utils/filterDefinitions';
import { SelectEntryDialog } from 'commons/components/forms/dialogs/SelectEntryDialog';
import { getLabel } from 'commons/util/rdfUtils';
import { getPurpose } from 'models/utils/metadata';
import { entryPropType } from 'commons/util/entry';

const NLS_BUNDLES = [
  escoListNLS,
  esmoFieldsNLS,
  esmoCommonsNLS,
  escoDialogsNLS,
];

const ExtendsChooser = ({
  closeDialog,
  onSelect,
  rdfType,
  typeChoices = [],
  selectedEntry,
}) => {
  const { context } = useESContext();
  const contextId = context.getId();

  const filters = [
    {
      ...MODEL_FILTER,
      initialSelection: contextId,
      defaultValue: contextId,
      loadItems: () => getModelItems(contextId),
    },
    STATUS_FILTER,
    {
      ...TYPE_FILTER,
      items: [ANY_FILTER_ITEM, ...typeChoices],
    },
  ];

  const createQuery = useCallback(() => {
    return entrystore.newSolrQuery().rdfType(rdfType);
  }, [rdfType]);

  const handleSelectEntry = (entry) => {
    onSelect(entry);
    closeDialog();
  };

  return (
    <SelectEntryDialog
      listModelProps={{ showFilters: true }}
      closeDialog={closeDialog}
      titleNlsKey="selectEntryTitle"
      filters={filters}
      createQuery={createQuery}
      selectedEntry={selectedEntry}
      onSelect={handleSelectEntry}
      nlsBundles={NLS_BUNDLES}
      infoFields={FIELDS_FORMS_INFO}
      getTitleProps={({ entry: rowEntry }) => ({
        primary: getLabel(rowEntry),
        secondary: getPurpose({ entry: rowEntry }),
        maxSecondaryLength: 50,
      })}
    />
  );
};

ExtendsChooser.propTypes = {
  closeDialog: PropTypes.func.isRequired,
  onSelect: PropTypes.func.isRequired,
  rdfType: PropTypes.string,
  typeChoices: PropTypes.arrayOf(PropTypes.shape({})),
  selectedEntry: entryPropType,
};

export default ExtendsChooser;
