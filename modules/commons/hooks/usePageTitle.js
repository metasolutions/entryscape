import { useContext } from 'react';
import { PageTitleContext } from 'commons/contexts';

const usePageTitle = () => {
  const pageTitleContext = useContext(PageTitleContext);

  if (pageTitleContext === undefined) {
    throw new Error('usePageTitle must be used within a ContextProvider');
  }

  return pageTitleContext;
};

export default usePageTitle;
