import { createContext, useContext, useReducer } from 'react';
import PropTypes from 'prop-types';

const MapContext = createContext();

export const useMapContext = () => {
  const mapContext = useContext(MapContext);
  if (mapContext === undefined)
    throw new Error('Map context must be used within context provider');
  return mapContext;
};

const mapReducer = (state, action) => {
  switch (action.type) {
    case 'CREATE_MAP': {
      const { map } = action;
      return { ...state, map };
    }
    case 'ADD_CONTROL': {
      const { control } = action;
      state.map.addControl(control);
      return { ...state };
    }
    case 'REMOVE_CONTROL': {
      const { control } = action;
      state.map.removeControl(control);
      return { ...state };
    }
    default:
      return state;
  }
};

export const MapProvider = ({ children }) => {
  const [mapState, dispatch] = useReducer(mapReducer, {});

  return (
    <MapContext.Provider value={[mapState, dispatch]} dispatch={dispatch}>
      {children}
    </MapContext.Provider>
  );
};

MapProvider.propTypes = {
  children: PropTypes.node,
};

/**
 * Wraps a component with MapProvider
 * @return {node}
 */
export const withMapProvider = (Component) => {
  return (props) => {
    return (
      <MapProvider>
        <Component {...props} />
      </MapProvider>
    );
  };
};
