import config from 'config';
import { i18n } from 'esi18n';
import { USER_ENTRY_ID_GUEST } from 'commons/util/userIds';

/**
 * @return {*}
 */
const getBaseUrl = () => {
  let baseURL = config.get('entryscape.baseURL');
  if (!baseURL) {
    baseURL = config.get('baseAppPath')
      ? `${window.location.origin}/${config.get('baseAppPath')}`
      : window.location.origin;
  }

  return baseURL.replace(/\/?$/, '/'); // make sure it ends with a /
};

/**
 * @return {string}
 */
const getAppVersion = () => window.sessionStorage.version || VERSION || 'n/a';

const getStaticUrl = () => config.get('entryscape.static.url');

const getStaticBuildPath = () => {
  const app = config.get('entryscape.static.app');
  const version = config.get('entryscape.static.version');
  return new URL(`${app}/${version}/`, getStaticUrl()).href;
};

const getThemeDefaults = () => config.get('theme.default');

const getThemeToRender = () => {
  const baseURL = getBaseUrl();
  const { appName, assetsPath, logoIcon, logoFull } = getThemeDefaults();
  let { themePath } = getThemeDefaults();

  if (config.get('theme.localTheme')) {
    themePath = new URL('theme/', baseURL).href;
  } else if (config.get('theme.localAssets')) {
    themePath = new URL('theme/assets/', baseURL).href;
  }

  return {
    appName:
      config.get('theme.appName') || config.get('theme.logo.text') || appName,
    themePath,
    assetsPath,
    logoIcon,
    logoFull,
  };
};

const getAssetsPath = () => {
  const baseURL = getBaseUrl();
  let { assetsPath } = getThemeDefaults();
  if (config.get('theme.localAssets')) {
    assetsPath = new URL('theme/assets/', baseURL).href;
  }
  return assetsPath;
};

const getLogoSource = (type = 'icon') => {
  const { logoIcon, logoFull, themePath } = getThemeToRender();

  if (type === 'icon') {
    return config.get('theme.logo.icon')
      ? themePath + config.get('theme.logo.icon')
      : logoIcon;
  }

  return config.get('theme.logo.full')
    ? themePath + config.get('theme.logo.full')
    : logoFull;
};

const getAppName = (native = true) => {
  const { appName } = native ? getThemeDefaults() : getThemeToRender();
  return appName;
};

/**
 *
 * @param scope
 * @return {null|string}
 */
const getResourceBase = (scope) => {
  const resourceBase = config.get('entrystore.uriTemplates', null);
  if (resourceBase === null) {
    return null;
  }
  return typeof resourceBase === 'object'
    ? resourceBase[scope] || resourceBase.default
    : resourceBase;
};

/**
 * @todo remove, use getEntitytypeURITemplate
 * @param rdfType
 * @return {null|string}
 */
const getEntryURITemplate = (rdfType) => {
  const type = Array.isArray(rdfType) ? rdfType[0] : rdfType;
  const resourceBase = config.get('entrystore.uriTemplates', null);
  if (resourceBase === null) {
    return null;
  }
  return typeof resourceBase === 'object'
    ? resourceBase[type] || resourceBase.default
    : resourceBase;
};

const uploadFileSizeLimit = () => config.get('fileUploadSizeLimit', 26214400); // 25 MB

// #region site config
const APP_NODE_ID = 'controlNode';

const sortByArray = (toSort, order) =>
  order
    .map((key) => toSort.find(({ name }) => name === key))
    .filter((v) => v) // filter out undefined
    .concat(toSort.filter(({ name }) => !order.includes(name))); // in case order doesn't contain all modules

/**
 * Register all active and acl permitted modules
 * @returns {Map<string, object>}
 */
const getActiveModules = (userInfo) => {
  const initialModules = config.get('site.modules') || [];
  const moduleList = config.get('site.moduleList') || [];

  const modules = initialModules
    .filter((module) => moduleList.includes(module.name))
    .filter((module) => {
      switch (module.restrictTo || module.public || '') {
        case 'adminUser':
          return userInfo.isAdmin;
        case 'adminGroup':
          return userInfo.inAdminGroup;
        case 'admin':
          return userInfo.hasAdminRights;
        case true: // case where module.public = true;
        default:
          return true;
      }
    });

  return sortByArray(modules, config.get('site.moduleList'));
};

const getActiveViews = (userInfo, module = '') => {
  const isGuest = userInfo.entryId === USER_ENTRY_ID_GUEST;
  const registeredViews = [...config.get('site.views')];
  const activeModuleNames = module
    ? [module]
    : getActiveModules(userInfo).map((mod) => mod.name);

  // find views that have a module defined but module is not registered
  return registeredViews.reduce((activeViews, viewDef) => {
    if (
      !(
        viewDef.disabled ||
        ('module' in viewDef && !activeModuleNames.includes(viewDef.module)) ||
        (viewDef.restrictTo === 'admin' && isGuest)
      )
    ) {
      activeViews.push(viewDef);
    }
    return activeViews;
  }, []);
};

// #endregion

/**
 * Retrieves the localized URL, version and approval requirement for the privacy policy
 * @return {Object}
 */
export const getPrivacyPolicyParams = () => {
  const locale = i18n.getLocale();
  const defaultLocale = config.get('locale.fallback');
  const privacyPolicyConfig = config.get('privacyPolicy');
  const { url: urlObject, version, requireApproval } = privacyPolicyConfig;
  const url = urlObject[locale] || urlObject[defaultLocale];

  return { url, version, requireApproval };
};

/**
 * Retrieves the localized URL, version and approval requirement for the terms and conditions
 * @return {Object}
 */
export const getTermsAndConditionsParams = () => {
  const locale = i18n.getLocale();
  const defaultLocale = config.get('locale.fallback');
  const termsConditionsConfig = config.get('termsAndConditions');
  const { url: urlObject, version, requireApproval } = termsConditionsConfig;
  const url = urlObject[locale] || urlObject[defaultLocale];

  return { url, version, requireApproval };
};

export default {
  getBaseUrl,
  getAppVersion,
  getStaticBuildPath,
  getAssetsPath,
  getAppName,
  getLogoSource,
  getResourceBase,
  getEntryURITemplate,
  uploadFileSizeLimit,
  getActiveViews,
  getActiveModules,
};

export {
  getActiveViews,
  getActiveModules,
  getLogoSource,
  getAppVersion,
  APP_NODE_ID,
};
