import escoListNLS from 'commons/nls/escoList.nls';
import escaCatalogSearchNLS from 'catalog/nls/escaCatalogSearch.nls';

const nlsBundles = [escoListNLS, escaCatalogSearchNLS];

export { nlsBundles };
