import config from 'config';

const FONT_NAME = 'Source Sans Pro';
const REGULAR_FONT_FILE = 'source-sans-pro-v14-latin-regular.woff2';
const REGULAR_FONT = {
  style: 'normal',
  weight: '400',
  display: 'swap',
};
const SEMIBOLD_FONT_FILE = 'source-sans-pro-v14-latin-600.woff2';
const SEMIBOLD_FONT = {
  style: 'normal',
  weight: '600',
  display: 'swap',
};

/**
 *
 * @param {string} name
 * @param {string} url
 * @param {object} descriptors
 * @returns {Promise}
 */
const loadFont = async (name, url, descriptors) => {
  const font = new FontFace(name, url, descriptors);
  try {
    await font.load();
    document.fonts.add(font);
  } catch (error) {
    console.error(`Failed to load font ${name}`, error);
  }
};

/**
 * @returns {Promise}
 */
export const loadFonts = async () => {
  let fontPath = config.get('entryscape.fontPath');
  fontPath = fontPath.endsWith('/') ? fontPath : `${fontPath}/`;
  await loadFont(
    FONT_NAME,
    `local('Source Sans Pro Regular'), local('SourceSansPro-Regular'), url(${fontPath}${REGULAR_FONT_FILE})`,
    REGULAR_FONT
  );
  await loadFont(
    FONT_NAME,
    `local('Source Sans Pro SemiBold'), local('SourceSansPro-SemiBold'), url(${fontPath}${SEMIBOLD_FONT_FILE})`,
    SEMIBOLD_FONT
  );
};
