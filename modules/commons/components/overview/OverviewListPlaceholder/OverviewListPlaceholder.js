import PropTypes from 'prop-types';
import './OverviewListPlaceholder.scss';

const OverviewListPlaceholder = ({ label }) => {
  return <div className="escoOverviewListPlaceholder">{label}</div>;
};

OverviewListPlaceholder.propTypes = {
  label: PropTypes.string,
};

export default OverviewListPlaceholder;
