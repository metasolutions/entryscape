import PropTypes from 'prop-types';
import { createContext, useContext, useReducer } from 'react';

/**
 * The TableView state
 *
 * @typedef {Object} TableViewState
 * @property {Object[]} rows
 * @property {string[]} selectionModel
 * @property {Object<string, Object[]>} validationErrors
 * @property {number} page
 * @property {number} totalCount
 * @property {string} searchQuery
 * @property {string} error
 */

/**
 * @returns {TableViewState}
 */
const getInitialState = () => {
  return {
    rows: [],
    selectionModel: [],
    validationErrors: {},
    page: 0,
    rowsPerPage: 25,
    totalCount: 0,
    searchQuery: '',
    error: '',
    refreshCount: 0,
  };
};

/**
 * Determines and returns the new table view validationErrors state.
 *
 * @param {Object<string, string[]>} previousErrors
 * @param {string} rowId
 * @param {Object} newRowErrors
 * @returns {Object<string, Object[]>}
 */
const getUpdatedValidationErrors = (previousErrors, rowId, newRowErrors) => {
  if (newRowErrors.length) {
    return { ...previousErrors, [rowId]: newRowErrors };
  }

  const previousRowErrors = previousErrors[rowId];
  if (!previousRowErrors) {
    return previousErrors;
  }

  // Row had pre-existing validation errors, but none during last validation
  // eslint-disable-next-line camelcase
  const { [rowId]: _removedError, ...updatedErrors } = previousErrors;
  return updatedErrors;
};

const tableViewReducer = (state, { type, value }) => {
  switch (type) {
    case 'LOAD_DATA':
      return {
        ...state,
        rows: value.rows,
        columns: value.columns,
        totalCount: value.totalCount,
      };
    case 'EDIT_ROW':
      return {
        ...state,
        selectionModel: [value.rowId, ...state.selectionModel],
        validationErrors: getUpdatedValidationErrors(
          state.validationErrors,
          value.rowId,
          value.rowErrors
        ),
      };
    case 'SAVE_ROW':
      return {
        ...state,
        selectionModel: state.selectionModel.filter(
          (key) => key !== value.rowId
        ),
        error: '',
        validationErrors: getUpdatedValidationErrors(
          state.validationErrors,
          value.rowId,
          []
        ),
      };
    case 'DISCARD_ROW':
      return {
        ...state,
        selectionModel: state.selectionModel.filter(
          (key) => key !== value.rowId
        ),
        validationErrors: (() => {
          // eslint-disable-next-line camelcase
          const { [value.rowId]: _removedError, ...newErrors } =
            state.validationErrors;
          return newErrors;
        })(),
        error: '',
      };
    case 'SAVE_SELECTED_ROWS':
      return { ...state, selectionModel: [], error: '' };
    case 'DISCARD_SELECTED_ROWS':
      return { ...state, selectionModel: [], validationErrors: {}, error: '' };
    case 'PAGINATE':
      return { ...state, page: value.page };
    case 'CHANGE_ROWS_PER_PAGE':
      return { ...state, rowsPerPage: value.rowsPerPage, page: 0 };
    case 'SEARCH':
      return { ...state, searchQuery: value.searchQuery, page: 0 };
    case 'CLEAR_SEARCH':
      return { ...state, searchQuery: '' };
    case 'ERROR':
      return { ...state, error: value.error };
    case 'CLEAR_ERROR':
      return { ...state, error: '' };
    case 'REFRESH_TABLE':
      return {
        ...state,
        refreshCount: state.refreshCount + 1, // increase to trigger refresh
      };
    default:
      throw new Error('Unknown action for TableView reducer');
  }
};

const TableViewContext = createContext(null);

export const TableViewContextProvider = ({ children }) => {
  const [state, dispatch] = useReducer(tableViewReducer, getInitialState());

  return (
    <TableViewContext.Provider value={[state, dispatch]}>
      {children}
    </TableViewContext.Provider>
  );
};

TableViewContextProvider.propTypes = {
  children: PropTypes.node,
};

export const useTableViewContext = () => {
  const context = useContext(TableViewContext);

  if (context === undefined) {
    throw new Error(
      'useTableViewContext must be used within a ContextProvider'
    );
  }

  return context;
};

/**
 * Wraps a component with TableViewContextProvider
 * @return {node}
 */
export const withTableViewContextProvider = (Component) => {
  return (props) => {
    return (
      <TableViewContextProvider>
        <Component {...props} />
      </TableViewContextProvider>
    );
  };
};
