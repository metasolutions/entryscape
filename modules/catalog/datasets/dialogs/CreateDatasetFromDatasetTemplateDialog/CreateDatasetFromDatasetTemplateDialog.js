import PropTypes from 'prop-types';
import { useState, useEffect } from 'react';
import { Entry } from '@entryscape/entrystore-js';
import { useESContext } from 'commons/hooks/useESContext';
import useAsync from 'commons/hooks/useAsync';
import { commitMetadata, RDF_TYPE_DATASET } from 'commons/util/entry';
import {
  createPrototypeDatasetEntry,
  updateCatalogWithDataset,
} from 'catalog/datasets/DatasetOverview/utils/datasets';
// eslint-disable-next-line max-len
import CreateEntryByTemplateDialog from 'commons/components/EntryListView/dialogs/create/CreateEntryByTemplateDialog';
import { LEVEL_MANDATORY } from 'commons/components/rdforms/LevelSelector';
import escaDatasetTemplateNLS from 'catalog/nls/escaDatasetTemplate.nls';
import escoListNLS from 'commons/nls/escoList.nls';
import { useTranslation } from 'commons/hooks/useTranslation';
import { getMetadataFromDatasetTemplate } from 'catalog/datasetTemplates/util';
import { useSnackbar } from 'commons/hooks/useSnackbar';
import useErrorHandler from 'commons/errors/hooks/useErrorHandler';

const CreateDatasetFromTemplateDialog = ({
  entry: datasetTemplateEntry,
  closeDialog,
  refreshDatasets,
  prototypeCallback = null,
  ...rest
}) => {
  const translate = useTranslation([escaDatasetTemplateNLS, escoListNLS]);
  const { context } = useESContext();
  const { data: prototypeEntry, runAsync, error } = useAsync(null);
  const [protoEntryInitialized, setProtoEntryInitialized] = useState(false);
  const [addSnackbar] = useSnackbar();
  useErrorHandler(error);

  useEffect(() => {
    runAsync(
      createPrototypeDatasetEntry(context, RDF_TYPE_DATASET).then((result) =>
        prototypeCallback ? prototypeCallback(result) : result
      )
    );
  }, [context, runAsync, prototypeCallback]);

  useEffect(() => {
    if (!datasetTemplateEntry || !prototypeEntry) {
      return;
    }

    const metadata = getMetadataFromDatasetTemplate(
      datasetTemplateEntry,
      prototypeEntry
    );

    prototypeEntry.setMetadata(metadata);
    setProtoEntryInitialized(prototypeEntry.getMetadata().isChanged());
  }, [datasetTemplateEntry, prototypeEntry]);

  const handleCreateEntry = async (datasetPrototypeEntry, graph) => {
    const newDatasetEntry = await commitMetadata(datasetPrototypeEntry, graph);
    await updateCatalogWithDataset(newDatasetEntry);

    addSnackbar({
      message: translate('createDatasetSuccess'),
    });
    return newDatasetEntry;
  };

  return protoEntryInitialized ? (
    <CreateEntryByTemplateDialog
      entry={prototypeEntry}
      createEntry={handleCreateEntry}
      editorLevel={LEVEL_MANDATORY}
      createHeaderLabel={translate('createDatasetFromTemplateHeader')}
      fromDatasetTemplate
      closeDialog={closeDialog}
      onEntryEdit={(entry) => {
        refreshDatasets();
        return entry;
      }}
      createErrorNls="createDatasetFail"
      {...rest}
    />
  ) : null;
};

CreateDatasetFromTemplateDialog.propTypes = {
  entry: PropTypes.instanceOf(Entry),
  closeDialog: PropTypes.func,
  refreshDatasets: PropTypes.func,
  prototypeCallback: PropTypes.func,
};

export default CreateDatasetFromTemplateDialog;
