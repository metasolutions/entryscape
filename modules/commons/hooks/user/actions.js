export const AUTH_LOGIN = 'AUTH_LOGIN';
export const AUTH_LOGIN_REQUEST = 'AUTH_LOGIN_REQUEST';
export const AUTH_LOGIN_SUCCESS = 'AUTH_LOGIN_SUCCESS';
export const AUTH_LOGOUT = 'AUTH_LOGOUT';
export const AUTH_LOGOUT_SUCCESS = 'AUTH_LOGOUT_SUCCESS';
export const AUTH_CURRENT_USER = 'AUTH_CURRENT_USER';
export const AUTH_CURRENT_USER_REQUEST = 'AUTH_CURRENT_USER_REQUEST';
export const AUTH_CURRENT_USER_SUCCESS = 'AUTH_CURRENT_USER_SUCCESS';
