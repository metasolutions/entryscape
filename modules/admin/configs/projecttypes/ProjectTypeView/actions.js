import ListRemoveEntryDialog from 'commons/components/EntryListView/dialogs/ListRemoveEntryDialog';
import {
  LIST_ACTION_EDIT,
  LIST_ACTION_REVISIONS,
  LIST_ACTION_REMOVE,
} from 'commons/components/EntryListView/actions';
import { ACTION_INFO_ID } from 'commons/actions/actionIds';
import { PROJECT_TYPE_ID_PREFIX } from 'admin/configs/utils/namespace';
import esadProjectTypeNLS from 'admin/nls/esadProjectType.nls';
import escoListNLS from 'commons/nls/escoList.nls';
import esadTypeNLS from 'admin/nls/esadType.nls';
import FieldsLinkedDataBrowserDialog from 'commons/components/FieldsLinkedDataBrowserDialog';
import { withListRerender } from 'commons/components/ListView';
import ProjectTypeCreateDialog from '../ProjectTypeCreateDialog';
import ProjectTypeEditDialog from '../ProjectTypeEditDialog';
import { fieldPresenters } from '../utils/projectTypePresenters';

const nlsBundles = [esadProjectTypeNLS, esadTypeNLS, escoListNLS];

const listActions = [
  {
    id: 'create',
    Dialog: ProjectTypeCreateDialog,
    labelNlsKey: 'addLabel',
    tooltipNlsKey: 'addTooltip',
  },
];

export const presenterAction = {
  id: ACTION_INFO_ID,
  Dialog: FieldsLinkedDataBrowserDialog,
  idPrefix: PROJECT_TYPE_ID_PREFIX,
  fields: fieldPresenters,
  labelNlsKey: 'infoEntry',
  nlsBundles,
};

const rowActions = [
  {
    ...LIST_ACTION_EDIT,
    Dialog: withListRerender(ProjectTypeEditDialog, 'onEdit'),
  },
  LIST_ACTION_REVISIONS,
  {
    ...LIST_ACTION_REMOVE,
    Dialog: ListRemoveEntryDialog,
  },
];

export { rowActions, nlsBundles, listActions };
