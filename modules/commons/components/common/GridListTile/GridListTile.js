import PropTypes from 'prop-types';
import { ImageListItem } from '@mui/material';

const GridListTile = ({ children, ...gridListTileProps }) => {
  return <ImageListItem {...gridListTileProps}>{children}</ImageListItem>;
};

GridListTile.propTypes = {
  children: PropTypes.node,
};

export default GridListTile;
