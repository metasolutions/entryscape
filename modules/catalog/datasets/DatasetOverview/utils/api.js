import { entrystore } from 'commons/store';
import { promiseUtil, Entry, Context } from '@entryscape/entrystore-js';
import { refreshEntry } from 'commons/util/entry';

/**
 * The index (order) of the statuses corresponds to the pipeline return status.
 * Do not change if not changed in the RowStore/EntryStore
 *
 * @type {string[]}
 */
const id2status = ['created', 'accepted', 'processing', 'available', 'error'];

/**
 * Load the RowStore data for this entry
 *
 * @param {Entry} pipelineEntry
 * @returns {Promise}
 */
export const loadRowStoreData = (pipelineEntry) => {
  return entrystore.loadViaProxy(
    pipelineEntry.getEntryInfo().getExternalMetadataURI()
  );
};

/**
 * Get api's example url
 *
 * @param {string} resultResourceURI
 * @param {string} apiAlias
 * @param {string} columnName
 * @returns {object}
 */
export const getExampleURL = (resultResourceURI, apiAlias, columnName) => {
  if (apiAlias) {
    return `${resultResourceURI.substr(
      0,
      resultResourceURI.lastIndexOf('/') + 1
    )}${apiAlias}?${columnName}=some_string_pattern`;
  }
  return `${resultResourceURI}?${columnName}=some_string_pattern`;
};

/**
 * Get api information from cached external metadata
 *
 * @param {Entry} pipelineResultsEntry
 * @returns {object}
 */
export const getApiInformation = async (pipelineResultsEntry) => {
  const resultResourceURI = pipelineResultsEntry.getResourceURI();
  const identifier = resultResourceURI.substr(
    resultResourceURI.lastIndexOf('/') + 1,
    resultResourceURI.length
  );
  const cachedExternalMetadata =
    pipelineResultsEntry.getCachedExternalMetadata();
  const columnNames = cachedExternalMetadata
    .find(null, 'store:pipelineResultColumnName')
    .map((stmt) => stmt.getValue());
  const apiAlias = cachedExternalMetadata.findFirstValue(
    null,
    'store:aliasName'
  );
  const apiStatus = cachedExternalMetadata.findFirstValue(
    null,
    'store:pipelineResultStatus'
  );
  return {
    apiStatus,
    apiAlias,
    columnNames,
    identifier,
  };
};

/**
 * Sets alias in RowStore
 *
 * @param {Context} context
 * @param {string} resultResourceURI
 * @param {string} alias
 * @returns {Promise}
 */
const setAlias = async (context, resultResourceURI, alias) => {
  const rowstorePipelineEntry = await context.getEntryById('rowstorePipeline');
  const pipeline = await rowstorePipelineEntry.getResource();
  const transformId = pipeline.getTransformForType(
    pipeline.transformTypes.ROWSTORE
  );

  const transformArguments = alias
    ? {
        action: 'setalias',
        alias,
        datasetURL: resultResourceURI,
      }
    : {
        action: 'setalias',
        datasetURL: resultResourceURI,
      };

  pipeline.setTransformArguments(transformId, {});
  pipeline.setTransformArguments(transformId, transformArguments);
  return pipeline
    .commit()
    .then(() => {
      return pipeline.execute(null, {});
    })
    .catch(() => {
      throw new Error('Alias cannot be set');
    });
};

/**
 * Updates cached external metadata with alias
 *
 * @param {Entry} pipelineResultsEntry
 * @param {string} alias
 * @returns {object}
 */
export const updateAlias = async (pipelineResultsEntry, alias) => {
  const context = pipelineResultsEntry.getContext();
  const resultResourceURI = pipelineResultsEntry.getResourceURI();
  // update rowstore
  await setAlias(context, resultResourceURI, alias);

  // udpate cached external metadata
  const extMetadata = pipelineResultsEntry.getCachedExternalMetadata();
  extMetadata.findAndRemove(resultResourceURI, 'store:aliasName');
  if (alias) {
    extMetadata.addL(resultResourceURI, 'store:aliasName', alias);
  }
  const updatedEntry =
    await pipelineResultsEntry.commitCachedExternalMetadata();
  return getApiInformation(updatedEntry);
};

/**
 *
 * @param {Entry} pipelineEntry
 * @returns {string}
 */
export const oldStatus = (pipelineEntry) =>
  pipelineEntry
    .getCachedExternalMetadata()
    .findFirstValue(
      pipelineEntry.getResourceURI(),
      'store:pipelineResultStatus'
    );

/**
 * Transform the RowStore status value to string
 *
 * @see id2status
 * @param {object} data
 * @returns {string}
 */
export const status = (data) => id2status[data.status];

/**
 * Update the cached external metadata of the entry from given data object
 *
 * @param {store/Entry} pipelineEntry
 * @param {object} data
 * @return {Promise}
 */
export const update = async (pipelineEntry, data) => {
  // get a fresh copy of the entry
  await refreshEntry(pipelineEntry);

  const newStatus = id2status[data.status];
  const cachedExternalMetadata = pipelineEntry.getCachedExternalMetadata();
  const resourceURI = pipelineEntry.getResourceURI();

  // update status
  cachedExternalMetadata.findAndRemove(
    resourceURI,
    'store:pipelineResultStatus'
  );
  cachedExternalMetadata.addL(
    resourceURI,
    'store:pipelineResultStatus',
    newStatus
  );

  // update columns
  cachedExternalMetadata.findAndRemove(
    resourceURI,
    'store:pipelineResultColumnName'
  );

  data.columnnames.forEach((col) =>
    cachedExternalMetadata.addL(
      resourceURI,
      'store:pipelineResultColumnName',
      col
    )
  );

  // update aliases
  cachedExternalMetadata.findAndRemove(resourceURI, 'store:aliasName');
  if (data.aliases && data.aliases.length > 0) {
    cachedExternalMetadata.addL(
      resourceURI,
      'store:aliasName',
      data.aliases[0]
    );
  }

  return pipelineEntry.commitCachedExternalMetadata();
};

/**
 * Synchronizes the API status values between what was returned from RowStore and what is
 * saved in the external metadata graph
 *
 * @param {string} pipelineEntryURI
 * @param {number} remainingRetries
 * @returns {Promise<string>}
 * @see update
 */
export const syncStatus = async (pipelineEntryURI, remainingRetries) => {
  const pipelineEntry = await entrystore.getEntry(pipelineEntryURI, {
    forceLoad: false,
  });
  const data = await loadRowStoreData(pipelineEntry);
  const newStatus = status(data);
  /**
   * Currently it might happen that status is 'available' but there's a delay
   * in getting column names.
   */
  if (newStatus === 'available' && data?.columnnames.length) {
    await update(pipelineEntry, data);
  }
  // Status is 'available" but still no column names available
  if (newStatus === 'available' && remainingRetries < 1) return 'error';
  return newStatus;
};

const STATUS_CHECK_REPEATS = 50;
const STATUS_CHECK_DELAY_MILLIS = 2000;

/**
 * Recursive function that returns only when the api status is 'available' or throws error.
 * Otherwise keeps looping every certain millis
 *
 * @param {Entry} pipelineEntryURI
 * @param {function} translate
 * @param {number} repeat how many times should we re-try to find the API in an available status
 * @param {number} delayMillis how many millis to delay before trying again
 * @return {Promise<String>}
 * @throws
 */
export const checkStatusOnRepeat = async (
  pipelineEntryURI,
  translate,
  repeat = STATUS_CHECK_REPEATS,
  delayMillis = STATUS_CHECK_DELAY_MILLIS
) => {
  const newStatus = await syncStatus(pipelineEntryURI, repeat);

  switch (newStatus) {
    case 'available':
      return '';
    case 'error':
      throw Error(translate('apiProgressError')); // reject();
    default:
      // retry checking the API after 'delayMillis'
      if (repeat > 0) {
        await promiseUtil.delay(delayMillis);
        return checkStatusOnRepeat(pipelineEntryURI, translate, repeat - 1);
      }
      return translate('apiProgressWarning');
  }
};
