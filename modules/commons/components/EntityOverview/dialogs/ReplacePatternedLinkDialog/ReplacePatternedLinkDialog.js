import { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Button, TextField, InputAdornment } from '@mui/material';
import ContentWrapper from 'commons/components/common/ContentWrapper';
import ListActionDialog from 'commons/components/common/dialogs/ListActionDialog';
import {
  spreadEntry,
  isResourceURIUniqueInContext,
  constructURIFromPattern,
} from 'commons/util/store';
import { entrystore } from 'commons/store';
import { entryPropType } from 'commons/util/entry';
import { useTranslation } from 'commons/hooks/useTranslation';
import { useEntityURI } from 'commons/hooks/useEntityTitleAndURI';
import eswoSpacesNLS from 'workbench/nls/eswoSpaces.nls';
import eswoReplaceDialogNLS from 'workbench/nls/eswoReplaceDialog.nls';
import { entityTypePropType } from 'commons/types/EntityType';
import { ErrorCatcher } from 'commons/errors/ErrorCatcher';
import { ErrorWithMessage } from 'commons/errors/utils/errors';

const ReplacePatternedLinkDialog = ({
  entityType,
  entry,
  closeDialog,
  onSuccess,
}) => {
  const uriPattern = entityType.get('uriPattern');
  const translate = useTranslation([eswoSpacesNLS, eswoReplaceDialogNLS]);
  const { name, handleNameChange, hasNameChangedManually } = useEntityURI({
    uriPattern,
  });
  const [isUnique, setUnique] = useState(true);
  const { context, ruri, info: entryInfo } = spreadEntry(entry);
  const [contextName, setContextName] = useState('');
  const [replaceError, setReplaceError] = useState();

  useEffect(() => {
    entry
      .getContext()
      .getEntry()
      .then((contextEntry) => {
        contextEntry.refresh(true, true).then(() => {
          setContextName(contextEntry.getEntryInfo().getName());
          //   const uriSpace = metadata.findFirstValue(ruri, 'void:uriSpace');
        }); // make sure the context data is loaded
      });
  }, [entry]);

  useEffect(() => {
    const newURI = constructURIFromPattern(uriPattern, {
      entryName: name,
      contextName,
    });

    isResourceURIUniqueInContext(newURI).then((response) => {
      setUnique(response);
    });
  }, [contextName, name, uriPattern]);

  const handleSaveForm = async () => {
    try {
      const newURI = constructURIFromPattern(uriPattern, {
        entryName: name,
        contextName,
      });

      if (isUnique) {
        // 1. update entry resource URI
        entryInfo.setResourceURI(newURI);
        await entryInfo.commit();

        // 2. update all linked entries
        await entrystore
          .newSolrQuery()
          .context(context)
          .objectUri(ruri)
          .forEach(async (linkedEntry) => {
            linkedEntry.getMetadata().replaceURI(ruri, newURI);
            await linkedEntry.commitMetadata();
          });

        entry.setRefreshNeeded();
        await entry.refresh();
        closeDialog();
        onSuccess?.();
      }
    } catch (error) {
      setReplaceError(new ErrorWithMessage(translate('saveFail'), error));
    }
  };

  const dialogActions = (
    <Button autoFocus onClick={handleSaveForm} disabled={!name || !isUnique}>
      {translate('replaceLinkFooterButton')}
    </Button>
  );

  return (
    <>
      <ListActionDialog
        id="replace-entity-uri"
        title={translate('replaceLinkHeader')}
        actions={dialogActions}
        closeDialog={closeDialog}
        maxWidth="md"
      >
        <ContentWrapper>
          <TextField
            disabled
            label={translate('replaceEntityURLCurrentLinkLabel')}
            id="replace-entity-uri-current"
            value={ruri}
          />

          <TextField
            id="replace-entity-uri-new"
            label={translate('replaceEntityURLNewLinkLabel')}
            value={name}
            onChange={(evt) => {
              handleNameChange(evt);
            }}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  {constructURIFromPattern(uriPattern, {
                    contextName,
                  })}
                </InputAdornment>
              ),
            }}
            error={hasNameChangedManually && (!name || !isUnique)}
            helperText={
              hasNameChangedManually &&
              (!name
                ? translate('newURLRequired')
                : !isUnique && translate('unavailableURLFeedback'))
            }
          />
        </ContentWrapper>
      </ListActionDialog>
      <ErrorCatcher error={replaceError} />
    </>
  );
};

ReplacePatternedLinkDialog.propTypes = {
  entityType: entityTypePropType,
  entry: entryPropType,
  closeDialog: PropTypes.func,
  onSuccess: PropTypes.func,
};

export default ReplacePatternedLinkDialog;
