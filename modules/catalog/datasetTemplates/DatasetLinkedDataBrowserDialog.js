import PropTypes from 'prop-types';
import React from 'react';
import useGetDatasetTemplateEntry from 'catalog/datasetTemplates/useGetDatasetTemplateEntry';
import { Entry } from '@entryscape/entrystore-js';
// eslint-disable-next-line max-len
import DatasetTemplateMetadataPresenter from 'commons/components/rdforms/DatasetTemplateMetadataPresenter';
// eslint-disable-next-line max-len
import LinkedDataBrowserDialog from 'commons/components/LinkedDataBrowserDialog/LinkedDataBrowserDialog';

const DatasetLinkedDataBrowserDialog = ({ entry, ...rest }) => {
  const [datasetTemplateEntry] = useGetDatasetTemplateEntry(entry);
  const renderDatasetPresenter = ({
    templateId,
    initiallyExpanded,
    stackIndex,
  }) =>
    datasetTemplateEntry && stackIndex === 0 ? (
      <DatasetTemplateMetadataPresenter
        initiallyExpanded={initiallyExpanded}
        entry={datasetTemplateEntry}
        externalTemplateId={templateId}
        alternativeClass="escoMetadataExpandableAreaButton"
        collapseClasses={{
          root: 'escoMetadataExpandableArea',
        }}
      />
    ) : null;

  return (
    <LinkedDataBrowserDialog
      {...rest}
      entry={entry}
      renderPresenter={renderDatasetPresenter}
    />
  );
};

DatasetLinkedDataBrowserDialog.propTypes = {
  entry: PropTypes.instanceOf(Entry),
};

export default DatasetLinkedDataBrowserDialog;
