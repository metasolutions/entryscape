import { namespaces as ns } from '@entryscape/rdfjson';

ns.add('rfs', 'https://rdforms.org/terms/');
ns.add('sh', 'http://www.w3.org/ns/shacl#');

export const RDF_TYPE_MODEL = 'esterms:FormsContext';
export const RDF_TYPE_NAMESPACE = 'sh:namespace';
export const RDF_TYPE_PREFIX = 'sh:PrefixDeclaration';
export const RDF_TYPE_CLASS = 'rdfs:Class';
export const RDF_TYPE_PROPERTY = 'rdf:Property';
export const RDF_TYPE_FIELD = 'rfs:Field';
export const RDF_TYPE_FORM = 'rfs:Form';
export const RDF_TYPE_BUNDLES = 'rfs:Bundle';
export const RDF_TYPE_TEXT_FIELD = 'rfs:TextField';
export const RDF_TYPE_DATATYPE_FIELD = 'rfs:DatatypeField';
export const RDF_TYPE_SELECT_FIELD = 'rfs:SelectField';
export const RDF_TYPE_LOOKUP_FIELD = 'rfs:LookupField';
export const RDF_TYPE_INLINE_FIELD = 'rfs:InlineField';
export const RDF_TYPE_SECTION_FORM = 'rfs:SectionForm';
export const RDF_TYPE_PROPERTY_FORM = 'rfs:PropertyForm';
export const RDF_TYPE_PROFILE_FORM = 'rfs:ProfileForm';
export const RDF_TYPE_OBJECT_FORM = 'rfs:ObjectForm';
export const RDF_TYPE_CONSTRAINT = 'rfs:Constraint';
export const RDF_TYPE_QUALIFIED_RESOURCE = 'rfs:QualifiedResource';
export const RDF_TYPE_DIAGRAM = 'esterms:Diagram';
export const RDF_TYPE_TEXT = 'rfs:Text';

export const RDF_PROPERTY_LABEL = 'rfs:label';
export const RDF_PROPERTY_EDIT_LABEL = 'rfs:editLabel';
export const RDF_PROPERTY_DESCRIPTION = 'rfs:description';
export const RDF_PROPERTY_EDIT_DESCRIPTION = 'rfs:editDescription';
export const RDF_PROPERTY_HELP = 'rfs:help';
export const RDF_PROPERTY_PURPOSE = 'rfs:purpose';
export const RDF_PROPERTY_SPECIFICATION = 'rfs:specification';
export const RDF_PROPERTY_PLACEHOLDER = 'rfs:placeholder';
export const RDF_PROPERTY_PROPERTY = 'rfs:property';
export const RDF_PROPERTY_MIN = 'rfs:min';
export const RDF_PROPERTY_MAX = 'rfs:max';
export const RDF_PROPERTY_PREF = 'rfs:pref';
export const RDF_PROPERTY_NODETYPE = 'rfs:nodetype';
export const RDF_PROPERTY_DATATYPE = 'rfs:datatype';
export const RDF_PROPERTY_CONSTRAINT = 'rfs:constraint';
export const RDF_PROPERTY_CONSTRAINT_PROPERTY = 'rfs:constraintProperty';
export const RDF_PROPERTY_CONSTRAINT_VALUE = 'rfs:constraintValue';
export const RDF_PROPERTY_ORDER = 'rfs:order';
export const RDF_PROPERTY_EXTENDS = 'rfs:extends';
export const RDF_PROPERTY_CHILD = 'rfs:child';
export const RDF_PROPERTY_CSS = 'rfs:cssClass';
export const RDF_PROPERTY_ATTRIBUTE = 'rfs:attribute';
export const RDF_CLASS_OPTION = 'rfs:Option';
export const RDF_PROPERTY_OPTION = 'rfs:option';
export const RDF_PROPERTY_OPTION_VALUE = 'rfs:optionValue';
export const RDF_PROPERTY_TERMINOLOGY = 'rfs:terminology';
export const RDF_PROPERTY_COLLECTION = 'rfs:collection';
export const RDF_PROPERTY_VALUE = 'rfs:value';
export const RDF_PROPERTY_STATUS = 'rfs:status';
export const RDF_PROPERTY_ITEM = 'rfs:item';
export const RDF_CLASS_EXTENSION = 'rfs:Extension';
export const RDF_PROPERTY_STATUS_UNSTABLE = 'rfs:unstable';
export const RDF_PROPERTY_STATUS_STABLE = 'rfs:stable';
export const RDF_PROPERTY_STATUS_DEPRECATED = 'rfs:deprecated';
export const RDF_PROPERTY_VALUE_TEMPLATE = 'rfs:valueTemplate';
export const RDF_PROPERTY_PATTERN = 'rfs:pattern';
export const RDF_PROPERTY_INLINE = 'rfs:inline';
export const RDF_PROPERTY_OVERRIDE_FORM_ITEMS = 'rfs:overrideFormItems';
export const RDF_PROPERTY_ROLE = 'rfs:role';
export const RDF_PROPERTY_TEXT = 'rfs:text';

export const RDF_PROPERTY_LITERAL = 'rfs:literal';
export const RDF_PROPERTY_ONLY_LITERAL = 'rfs:onlyLiteral';
export const RDF_PROPERTY_DATATYPE_LITERAL = 'rfs:datatypeLiteral';
export const RDF_PROPERTY_LANGUAGE_LITERAL = 'rfs:languageLiteral';
export const RDF_PROPERTY_RESOURCE = 'rfs:resource';
export const RDF_PROPERTY_BLANK = 'rfs:blank';
export const RDF_PROPERTY_URI = 'rfs:uri';
export const RDF_PROPERTY_LANGUAGE = 'rfs:language';
export const RDF_PROPERTY_DEPENDS_ON = 'rfs:dependsOnField';
export const RDF_PROPERTY_DEPENDS_ON_VALUE = 'rfs:dependsOnValue';

// lookup entry
export const RDF_PROPERTY_NAMESPACE_ENTRY = 'rfs:namespaceEntry';
export const RDF_PROPERTY_NAMESPACE_LOCALNAME = 'rfs:localname';
export const RDF_PROPERTY_RESOURCE_ENTRY = 'rfs:resourceEntry';

// builds
export const RDF_PROPERTY_BUILD_SELECTION = 'rfs:buildSelection';
export const RDF_PROPERTY_TARGET_LANGUAGE = 'rfs:targetLanguage';
export const RDF_PROPERTY_BUILD_INCLUDE = 'rfs:buildInclude';
export const RDF_PROPERTY_BUILD_EXCLUDE = 'rfs:buildExclude';

export const RDF_CONSTRAINT_NULL_URI = 'rfs:constraintNullUri';

// SHACL ontology
export const RDF_TYPE_SHACL = 'http://www.w3.org/ns/shacl-shacl#';
