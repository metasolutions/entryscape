const STATIC_INFRA_BRANCHES_URL =
  'https://static.infra.entryscape.com/branches.json';

/**
 * @returns {string}
 */
const getBranch = () => {
  const urlQuery = window.location.search;
  const searchParams = new URLSearchParams(urlQuery);
  return searchParams.get('branch') || 'unknown';
};

const getAvailableBranches = () =>
  fetch(STATIC_INFRA_BRANCHES_URL).then((res) => res.json());

export { getBranch, getAvailableBranches };
