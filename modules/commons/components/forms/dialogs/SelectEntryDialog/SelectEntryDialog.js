import { useCallback } from 'react';
import PropTypes from 'prop-types';
import { Button } from '@mui/material';
import { Entry } from '@entryscape/entrystore-js';
import { entrystore } from 'commons/store';
import { applyQueryParams as applyDefaultQueryParams } from 'commons/util/solr';
import {
  useTranslation,
  nlsBundlesPropType,
} from 'commons/hooks/useTranslation';
import ListActionDialog from 'commons/components/common/dialogs/ListActionDialog';
import { useESContext } from 'commons/hooks/useESContext';
import {
  EntryListView,
  useSolrQuery,
  TITLE_COLUMN,
  MODIFIED_COLUMN,
  INFO_COLUMN,
} from 'commons/components/EntryListView';
import { LIST_ACTION_INFO } from 'commons/components/EntryListView/actions';
import FieldsLinkedDataBrowserDialog from 'commons/components/FieldsLinkedDataBrowserDialog';
import {
  withListModelProvider,
  ListItemButtonAction,
} from 'commons/components/ListView';
import useSearchFilters from 'commons/components/filters/hooks/useSearchFilters';
import { PENDING } from 'commons/hooks/useAsync';
import escoEntryChooserNLS from 'commons/nls/escoEntryChooser.nls';
import { entryPropType } from 'commons/util/entry';
import LinkedDataBrowserDialog from 'commons/components/LinkedDataBrowserDialog';

const EntrySelectButton = ({
  entry,
  selectedEntry,
  onSelect,
  nlsBundles = [],
}) => {
  const selected = (() => {
    if (Array.isArray(selectedEntry)) return selectedEntry.includes(entry);
    return entry === selectedEntry;
  })();

  const translate = useTranslation([...nlsBundles, escoEntryChooserNLS]);
  return (
    <Button
      color="primary"
      disabled={selected}
      variant="text"
      onClick={() => onSelect(entry)}
    >
      {selected ? translate('selectedEntityLabel') : translate('selectEntity')}
    </Button>
  );
};

EntrySelectButton.propTypes = {
  entry: PropTypes.instanceOf(Entry).isRequired,
  selectedEntry: PropTypes.oneOfType([
    entryPropType,
    PropTypes.arrayOf(entryPropType),
  ]),
  onSelect: PropTypes.func,
  nlsBundles: nlsBundlesPropType,
};

const createDefaultQuery = (context, rdfType) => {
  const query = entrystore.newSolrQuery();
  if (context) {
    query.context(context);
  }
  if (rdfType) {
    query.rdfType(rdfType);
  }
  return query;
};

const SelectEntryDialog = ({
  closeDialog,
  titleNlsKey = 'selectEntryTitle',
  onSelect,
  selectedEntry,
  rdfType,
  infoFields,
  filters,
  placeholderProps = {},
  getTitleProps,
  createQuery: createCustomQuery,
  allContexts = false,
  nlsBundles,
}) => {
  const { context: currentContext } = useESContext();
  const context = allContexts ? null : currentContext;
  const translate = useTranslation(nlsBundles);
  const { applyFilters, isLoading, hasFilters, ...filterProps } =
    useSearchFilters(filters);
  const createQuery = useCallback(
    () =>
      createCustomQuery
        ? createCustomQuery()
        : createDefaultQuery(context, rdfType),
    [context, rdfType, createCustomQuery]
  );
  const applyQueryParams = useCallback(
    (query, params) => {
      applyDefaultQueryParams(query, params);
      applyFilters(query);
    },
    [applyFilters]
  );
  const { status, ...queryResults } = useSolrQuery({
    createQuery,
    applyQueryParams,
  });

  return (
    <ListActionDialog
      id="field-entry-chooser"
      closeDialog={closeDialog}
      closeDialogButtonLabel={translate('close')}
      title={translate(titleNlsKey)}
      fixedHeight
    >
      <EntryListView
        {...queryResults}
        status={isLoading ? PENDING : status}
        nlsBundles={nlsBundles}
        includeFilters={hasFilters}
        filtersProps={filterProps}
        viewPlaceholderProps={placeholderProps}
        columns={[
          {
            ...TITLE_COLUMN,
            xs: 8,
            headerNlsKey: 'selectEntryListHeaderLabel',
            ...(getTitleProps ? { getProps: getTitleProps } : {}),
          },
          { ...MODIFIED_COLUMN },
          {
            ...INFO_COLUMN,
            getProps: ({ entry }) => ({
              action: {
                ...LIST_ACTION_INFO,
                entry,
                Dialog: infoFields
                  ? FieldsLinkedDataBrowserDialog
                  : LinkedDataBrowserDialog,
                fields: infoFields,
                titleNlsKey: 'infoDialogHeader',
                nlsBundles,
              },
            }),
          },
          {
            id: 'select-entry',
            xs: 1,
            Component: ListItemButtonAction,
            getProps: ({ entry }) => ({
              ButtonComponent: EntrySelectButton,
              entry,
              selectedEntry,
              onSelect,
              nlsBundles,
            }),
          },
        ]}
      />
    </ListActionDialog>
  );
};

SelectEntryDialog.propTypes = {
  closeDialog: PropTypes.func,
  titleNlsKey: PropTypes.string,
  onSelect: PropTypes.func,
  createQuery: PropTypes.func,
  selectedEntry: PropTypes.oneOfType([
    entryPropType,
    PropTypes.arrayOf(entryPropType),
  ]),
  rdfType: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.arrayOf(PropTypes.string),
  ]),
  infoFields: PropTypes.arrayOf(PropTypes.shape({})),
  filters: PropTypes.arrayOf(PropTypes.shape({})),
  placeholderProps: PropTypes.shape({}),
  nlsBundles: nlsBundlesPropType,
  getTitleProps: PropTypes.func,
  allContexts: PropTypes.bool,
};

const SelectEntryDialogWithListModel = ({ listModelProps, ...props }) =>
  withListModelProvider(SelectEntryDialog, () => listModelProps || {})(props);

export { SelectEntryDialogWithListModel as SelectEntryDialog };
