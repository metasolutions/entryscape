import { IconButton, TextField, InputAdornment } from '@mui/material';
import FindInPageIcon from '@mui/icons-material/FindInPage';
import { useRef, useState, useEffect } from 'react';
import escoEntryTypeNLS from 'commons/nls/escoEntryType.nls';
import { useTranslation } from 'commons/hooks/useTranslation';
import PropTypes from 'prop-types';
import Tooltip from 'commons/components/common/Tooltip';
/**
 * Get the file name from a browsers given fake path.
 *
 * @param {HTMLElement} fileInput
 * @returns {string}
 */
export const getFileNameFromInput = (fileInput) => {
  const fullPath = fileInput.value;
  const startIndex =
    fullPath.indexOf('\\') >= 0
      ? fullPath.lastIndexOf('\\')
      : fullPath.lastIndexOf('/');
  let fileName = fullPath.substring(startIndex);
  if (fileName.indexOf('\\') === 0 || fileName.indexOf('/') === 0) {
    fileName = fileName.substring(1);
  }
  return fileName;
};

const FILE_INPUT_ID = 'file-upload-input';

const FileUpload = ({ onSelectFile, file, helperText: helperTextProp }) => {
  const [fileName, setFileName] = useState('');
  const [pristine, setPristine] = useState(true);
  const fileInput = useRef(null);

  const translate = useTranslation([escoEntryTypeNLS]);
  const helperText = helperTextProp || translate('fileRequired');

  const onChangeFile = () => {
    setFileName(getFileNameFromInput(fileInput.current));
    onSelectFile(fileInput.current);
  };

  useEffect(() => {
    if (file !== null) {
      setFileName(getFileNameFromInput(file));
    }
  }, [file]);

  // TODO: input type="file" below should not have display:none or visibility:hidden
  return (
    <label
      htmlFor={FILE_INPUT_ID}
      aria-label={translate('uploadFile')}
      style={{ position: 'relative', width: '100%' }}
    >
      <input
        ref={fileInput}
        // accept=".rdf"
        style={{ opacity: 0, position: 'absolute', zIndex: -1 }}
        id={FILE_INPUT_ID}
        onChange={onChangeFile}
        type="file"
        name="file-upload-input"
      />
      <TextField
        value={fileName}
        label={translate('uploadFile')}
        error={!pristine && !fileName}
        helperText={!pristine && !fileName && helperText}
        onBlur={() => setPristine(false)}
        inputProps={{
          'aria-label': 'file',
        }}
        sx={{ margin: 0 }}
        // eslint-disable-next-line react/jsx-no-duplicate-props
        InputProps={{
          endAdornment: (
            <InputAdornment position="end">
              <Tooltip title={translate('fileInputBrowse')}>
                <IconButton
                  aria-label={translate('fileInputBrowse')}
                  color="secondary"
                  component="span"
                  // Not sure why but button presses are simulated as click events
                  // File upload should be rewritten with a11y in mind
                  onClick={(event) => {
                    if (event.key === 'Enter' || event.key === ' ') {
                      event.preventDefault();
                      event.stopPropagation();
                      fileInput.current.click();
                    }
                  }}
                >
                  <FindInPageIcon />
                </IconButton>
              </Tooltip>
            </InputAdornment>
          ),
        }}
      />
    </label>
  );
};

FileUpload.propTypes = {
  onSelectFile: PropTypes.func.isRequired,
  file: PropTypes.instanceOf(Element),
  helperText: PropTypes.string,
};

FileUpload.defaultProps = {
  file: null,
};
export default FileUpload;
