import React from 'react';
import PropTypes from 'prop-types';
import { Entry } from '@entryscape/entrystore-js';
import EditEntryDialog from 'commons/components/entry/EditEntryDialog';

const EditDiagramDialog = ({ closeDialog, datasetEntry }) => {
  return <EditEntryDialog entry={datasetEntry} closeDialog={closeDialog} />;
};

EditDiagramDialog.propTypes = {
  closeDialog: PropTypes.func,
  datasetEntry: PropTypes.instanceOf(Entry),
};

export default EditDiagramDialog;
