import { entrystore } from 'commons/store';
import { utils as entrystoreJsUtils } from '@entryscape/entrystore-js';
import { addIgnore, removeIgnore } from 'commons/errors/utils/async';
import config from 'config';

export const handlersToIgnore = [
  'execute',
  'loadViaProxy',
  'getEntry',
  'refresh',
  'commitCachedExternalMetadata',
  'commitGraph',
];

/**
 *  Informs the global error handler to ignore the passed handlers.
 *
 * @param {string[]} handlers
 */
export const ignoreHandlers = (handlers) =>
  handlers.forEach((handler) => addIgnore(handler, true, false));

/**
 *  Informs the global error handler to stop ignoring the passed handlers.
 *
 * @param {string[]} handlers
 */
export const stopIgnoringHandlers = (handlers) =>
  handlers.forEach((handler) => removeIgnore(handler));

/**
 *
 * @param {store/Pipeline} pipelineResource
 * @param {Entry} parentDistribution
 * @returns {Promise}
 */
const addApiDistributionToParent = (pipelineResource, parentDistribution) => {
  const swaggerBaseURL = config.get('entryscape.swaggerBaseURL');
  const swaggerJsonFileURL = `${pipelineResource.getResourceURI()}/swagger`;
  const swaggerDocumentationURL = `${swaggerBaseURL}?url=${swaggerJsonFileURL}`;

  return parentDistribution
    .getContext()
    .newNamedEntry()
    .add('rdf:type', 'dcat:Distribution')
    .add('dcat:accessURL', pipelineResource.getResourceURI())
    .add('dcterms:conformsTo', swaggerJsonFileURL)
    .add('foaf:page', swaggerDocumentationURL)
    .add('dcterms:source', parentDistribution.getResourceURI())
    .addL('dcterms:format', 'application/json')
    .commit();
};

/**
 *
 * @param {string} pipelineResultEntryURI
 * @param {Entry} datasetEntry
 * @param {Entry} distributionEntry
 * @returns {Promise}
 */
export const createApiDistribution = async (
  pipelineResultEntryURI,
  datasetEntry,
  distributionEntry
) => {
  // ? Not sure why we need to check for these, was in old code
  if (!pipelineResultEntryURI || !datasetEntry) {
    const rejectionMessage = !pipelineResultEntryURI
      ? 'No API to create distribution for.' // ! nls?
      : 'No Dataset to create distribution in.';

    return Promise.reject(rejectionMessage);
  }

  const pipelineResourceEntry = await entrystore.getEntry(
    pipelineResultEntryURI
  );
  const newDistributionEntry = await addApiDistributionToParent(
    pipelineResourceEntry,
    distributionEntry
  );
  await entrystoreJsUtils.addRelation(
    datasetEntry,
    'dcat:distribution',
    newDistributionEntry
  );

  return Promise.resolve(true);
};
