import { createContext, useContext } from 'react';

/**
 * The context is necessary for two reasons. The (clipboard) graph has to persist
 * 1) on all toolkit views. For example, the catalog loaded on the "Catalog"
 * view should be the one validated or converted on the corresponding views.
 * 2) Between the (harvest) status and (toolkit) validation.
 */
const ClipboardGraphContext = createContext(['', () => {}]);

const useClipboardGraph = () => {
  const clipboardGraphContext = useContext(ClipboardGraphContext);

  if (clipboardGraphContext === undefined) {
    throw new Error('useClipboardGraph must be used within a ContextProvider');
  }

  return clipboardGraphContext;
};

export default ClipboardGraphContext;
export { useClipboardGraph };
