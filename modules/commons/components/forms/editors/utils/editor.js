import {
  BLANK,
  GROUP,
  DATATYPE_LITERAL,
  LANGUAGE_LITERAL,
  LITERAL,
  URI,
  XSD_BOOLEAN,
  ONLY_LITERAL,
} from '../../utils/nodetypes';
import { LiteralEditor } from '../LiteralEditor';
import { LanguageLiteralEditor } from '../LanguageLiteralEditor';
import { CheckboxEditor } from '../CheckboxEditor';
import { BlankEditor } from '../BlankEditor';
import { GroupEditor } from '../GroupEditor';

/**
 * Find a matching editor component based on nodetype.
 * @param {string} nodetype
 * @returns {Function|undefined}
 */
export const getEditorComponent = (nodetype, datatype) => {
  if (!nodetype) {
    throw new Error('Nodetype missing');
  }

  switch (nodetype) {
    case DATATYPE_LITERAL: {
      if (datatype === XSD_BOOLEAN) return CheckboxEditor;
      return LiteralEditor;
    }
    case GROUP: {
      return GroupEditor;
    }
    case BLANK: {
      return BlankEditor;
    }
    case URI:
    case LITERAL:
    case ONLY_LITERAL:
      return LiteralEditor;
    case LANGUAGE_LITERAL:
      return LanguageLiteralEditor;
    default:
      console.error(`No editor found for nodetype ${nodetype}`);
      break;
  }
};
