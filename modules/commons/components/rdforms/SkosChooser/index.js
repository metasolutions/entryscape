import { renderingContext } from '@entryscape/rdforms';
import show from './EntryChooserDialog';
import search from '../EntryChooser/searchChoice';
import getChoice from '../EntryChooser/getChoice';

const chooserConfiguration = {
  show,
  supportsInlineCreate() {
    return false;
  },
  search,
  getChoice,
};

renderingContext.chooserRegistry
  .itemtype('choice')
  .constraint({
    'http://www.w3.org/1999/02/22-rdf-syntax-ns#type':
      'http://www.w3.org/2004/02/skos/core#Concept',
  })
  .register(chooserConfiguration);

renderingContext.chooserRegistry
  .itemtype('choice')
  .constraint({
    'http://www.w3.org/2004/02/skos/core#inScheme': '',
  })
  .register(chooserConfiguration);

renderingContext.chooserRegistry
  .itemtype('choice')
  .constraint({
    'http://www.w3.org/1999/02/22-rdf-syntax-ns#type':
      'http://www.w3.org/2004/02/skos/core#Concept',
    'http://www.w3.org/2004/02/skos/core#inScheme': '',
  })
  .register(chooserConfiguration);
