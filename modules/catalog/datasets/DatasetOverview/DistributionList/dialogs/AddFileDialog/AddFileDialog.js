import PropTypes from 'prop-types';
import { Entry } from '@entryscape/entrystore-js';
import { useState, useMemo } from 'react';
import ListActionDialog from 'commons/components/common/dialogs/ListActionDialog';
import FileUpload from 'commons/components/common/FileUpload';
import RDFormsFieldWrapper from 'commons/components/rdforms/FieldWrapper';
import useRDFormsEditor from 'commons/components/rdforms/hooks/useRDFormsEditor';
import { useESContext } from 'commons/hooks/useESContext';
import { useTranslation } from 'commons/hooks/useTranslation';
import escoEntryTypeNLS from 'commons/nls/escoEntryType.nls';
import escaFilesListNLS from 'catalog/nls/escaFilesList.nls';
import { itemStore } from 'commons/rdforms/itemstore';
import ContentWrapper from 'commons/components/common/ContentWrapper';
import { LEVEL_OPTIONAL } from 'commons/components/rdforms/LevelSelector';
import {
  createFilePrototypeEntry,
  addFileToDistribution,
} from 'catalog/datasets/DatasetOverview/utils/distributions';
import Button from '@mui/material/Button';

const AddFileDialog = ({
  closeDialog,
  distributionEntry,
  refreshFileEntries,
  setEditedFiles,
}) => {
  const { context } = useESContext();
  const [file, setFile] = useState(null);
  const rdformTemplate = useMemo(
    () => itemStore.createTemplateFromChildren(['dcterms:title']),
    []
  );
  const t = useTranslation([escoEntryTypeNLS, escaFilesListNLS]);
  const [filePrototypeEntry] = useState(createFilePrototypeEntry(context));
  const { editor, EditorComponent, graph } = useRDFormsEditor({
    entry: filePrototypeEntry,
    includeLevel: LEVEL_OPTIONAL,
    rdformTemplate,
  });

  const handleAddFile = async () => {
    try {
      distributionEntry.setRefreshNeeded(); // make sure distribution has latest changes
      await distributionEntry.refresh();
      await addFileToDistribution(
        distributionEntry,
        filePrototypeEntry,
        file,
        graph
      );
      setEditedFiles(true);
      refreshFileEntries();
      closeDialog();
    } catch (error) {
      console.error(`Failed to add file: ${error}`);
    }
  };

  const dialogActions = (
    <Button autoFocus onClick={handleAddFile} disabled={!file}>
      {t('addButtonLabel')}
    </Button>
  );

  return (
    <ListActionDialog
      id="add-file"
      closeDialog={closeDialog}
      title={t('createHeader')}
      actions={dialogActions}
      maxWidth="md"
    >
      <ContentWrapper md={12}>
        <form autoComplete="off">
          <RDFormsFieldWrapper label={t('fileLabel')} editor>
            <FileUpload
              file={file}
              onSelectFile={(inputEl) => setFile(inputEl)}
            />
          </RDFormsFieldWrapper>
          {editor && <EditorComponent />}
        </form>
      </ContentWrapper>
    </ListActionDialog>
  );
};

AddFileDialog.propTypes = {
  closeDialog: PropTypes.func,
  distributionEntry: PropTypes.instanceOf(Entry),
  refreshFileEntries: PropTypes.func,
  setEditedFiles: PropTypes.func,
};

export default AddFileDialog;
