export const loadCanvg = async () => {
  try {
    const { Canvg } = await import(/* webpackChunkName: "canvg" */ 'canvg');
    return Canvg;
  } catch (error) {
    console.log('Failed to load canvg library');
  }
};
