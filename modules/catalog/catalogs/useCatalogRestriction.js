import config from 'config';
import { useUserState } from 'commons/hooks/useUser';
import { hasUserPermission, ADMIN_RIGHTS, PREMIUM } from 'commons/util/user';

const useCatalogRestriction = (numberOfCatalogs) => {
  const { userEntry } = useUserState();
  const isExemptFromRestrictions = hasUserPermission(userEntry, [
    PREMIUM,
    ADMIN_RIGHTS,
  ]);

  if (isExemptFromRestrictions) return { isRestricted: false };

  const catalogLimit = config.get('catalog.catalogLimit');
  const pathToCatalogLimitContent = config.get('catalog.catalogLimitDialog');
  const isRestricted =
    (catalogLimit && numberOfCatalogs >= catalogLimit) || catalogLimit === 0;

  return { isRestricted, contentPath: pathToCatalogLimitContent };
};

export default useCatalogRestriction;
