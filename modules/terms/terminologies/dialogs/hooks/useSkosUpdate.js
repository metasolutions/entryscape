import {
  STATUS_PROGRESS,
  STATUS_DONE,
  STATUS_FAILED,
} from 'commons/hooks/useTasksList';
import { promiseUtil } from '@entryscape/entrystore-js';
import { Graph, utils } from '@entryscape/rdfjson';
import { fixURIs, fixSymmetry } from '../utils/skos';

const updateConceptScheme = async (conceptSchemeEntry, graph) => {
  const conceptSchemeURI = conceptSchemeEntry.getResourceURI();
  const newGraph = utils.extract(graph, new Graph(), conceptSchemeURI);
  const oldGraph = conceptSchemeEntry.getCachedExternalMetadata();
  if (utils.fingerprint(oldGraph) !== utils.fingerprint(newGraph)) {
    conceptSchemeEntry.setCachedExternalMetadata(newGraph);
    await conceptSchemeEntry.commitCachedExternalMetadata();
  }
};

const indexExistingConcepts = async (context) => {
  const es = context.getEntryStore();
  const cache = es.getCache();
  const index = {};
  await es
    .newSolrQuery()
    .context(context)
    .rdfType('skos:Concept')
    .forEach((conceptEntry) => {
      index[conceptEntry.getResourceURI()] = {
        fingerprint: utils.fingerprint(
          conceptEntry.getCachedExternalMetadata()
        ),
        uri: conceptEntry.getURI(),
        id: conceptEntry.getId(),
      };
      cache.unCache(conceptEntry);
    });
  return index;
};

const updateConcepts = async (context, graph, updateProgress) => {
  const statements = graph.find(null, 'rdf:type', 'skos:Concept');
  const total = statements.length;
  const updateProgressPerConcept = (operation) =>
    updateProgress({ total, operation });

  const conceptIndex = await indexExistingConcepts(context);

  await promiseUtil.forEach(statements, async (stmt) => {
    const uri = stmt.getSubject();
    const occurence = conceptIndex[uri];
    const prototypeEntry = context.newLinkRef(
      uri,
      uri,
      occurence ? occurence.id : undefined
    );
    const newGraph = utils.extract(graph, new Graph(), uri);
    const newFingerprint = utils.fingerprint(newGraph);
    prototypeEntry.setCachedExternalMetadata(newGraph);
    if (occurence === undefined) {
      await prototypeEntry.commit();
      updateProgressPerConcept('added');
    } else {
      delete conceptIndex[uri];
      if (occurence.fingerprint !== newFingerprint) {
        await prototypeEntry.commitCachedExternalMetadata();
        updateProgressPerConcept('updated');
      } else {
        updateProgressPerConcept('unchanged');
      }
    }
  });

  const rest = context.getEntryStore().getREST();
  await promiseUtil.forEach(Object.keys(conceptIndex), async (uri) => {
    const occurence = conceptIndex[uri];
    await rest.del(occurence.uri);
    updateProgressPerConcept('removed');
  });
};

const skosUpdate =
  ({ t, updateTask }) =>
  async (data, conceptSchemeEntry) => {
    const { graph, conceptSchemeURI } = data;
    if (conceptSchemeEntry.getResourceURI() !== conceptSchemeURI) {
      updateTask('update', {
        status: STATUS_FAILED,
        message: t('terminologyURLHasChanged'),
      });
      return;
    }

    updateTask('update', { status: STATUS_PROGRESS });

    try {
      let treated = 0;
      let updated = 0;
      let added = 0;
      let removed = 0;
      let unchanged = 0;
      // make sure to upate the progress dialog while in a promiseUtil.forEach loop
      const updateProgress = ({ total, operation }) => {
        treated += 1;
        switch (operation) {
          case 'updated':
            updated += 1;
            break;
          case 'added':
            added += 1;
            break;
          case 'removed':
            removed += 1;
            break;
          default:
            unchanged += 1;
            break;
        }
        updateTask('update', {
          message: t('numberOfConceptsUpdated', {
            treated,
            total,
            updated,
            added,
            removed,
            unchanged,
          }),
        });
      };

      await fixURIs(data);
      await fixSymmetry(data);
      await updateConceptScheme(conceptSchemeEntry, graph);
      const context = conceptSchemeEntry.getContext();
      await updateConcepts(context, graph, updateProgress);
    } catch (err) {
      updateTask('update', { status: STATUS_FAILED, message: t(err.message) });
      return;
    }
    updateTask('update', { status: STATUS_DONE });
  };

export default skosUpdate;
