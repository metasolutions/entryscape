import useNavigate from 'commons/components/router/useNavigate';
import RemoveEntryDialog from 'commons/components/entry/RemoveEntryDialog';

const OverviewRemoveEntryDialog = (props) => {
  const { goBack } = useNavigate();
  return <RemoveEntryDialog {...props} onRemoveCallback={goBack} />;
};

export default OverviewRemoveEntryDialog;
