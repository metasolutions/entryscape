import PropTypes from 'prop-types';
import { getLabel, getFullLengthLabel } from 'commons/util/rdfUtils';
import React, { useState } from 'react';
import { ButtonGroup, Button, TextField, Typography } from '@mui/material';
import ContentWrapper from 'commons/components/common/ContentWrapper';
import {
  useTranslation,
  nlsBundlesPropType,
} from 'commons/hooks/useTranslation';
import ListActionDialog from 'commons/components/common/dialogs/ListActionDialog';
import { downloadURI, convertFileFormat } from 'commons/util/file';
import './ListDownloadEntryDialog.scss';

const formats = [
  {
    name: 'RDF/XML',
    media: 'application/rdf+xml',
  },
  {
    name: 'Turtle',
    media: 'text/turtle',
  },
  {
    name: 'N-Triples',
    media: 'application/n-triples',
  },
  {
    name: 'JSON-LD',
    media: 'application/ld+json',
  },
];

const ListDownloadEntryDialog = ({
  entry,
  closeDialog,
  profile,
  nlsBundles,
}) => {
  const [format, setFormat] = useState(formats[0].media);
  const metadataURL = new URL(entry.getEntryInfo().getMetadataURI());
  const translate = useTranslation(nlsBundles);

  if (profile) {
    metadataURL.searchParams.set('recursive', profile);
  }
  metadataURL.searchParams.set('format', format);

  const handleDownload = () => {
    const exportURL = new URL(`${metadataURL}&download`);
    downloadURI(
      decodeURIComponent(exportURL.href),
      `${getFullLengthLabel(entry)}.${convertFileFormat(format)}`
    );
  };

  const actions = (
    <Button autoFocus onClick={handleDownload}>
      {translate('downloadButton')}
    </Button>
  );

  return (
    <ListActionDialog
      id="download-entry"
      title={translate('download', { entryLabel: getLabel(entry) })}
      actions={actions}
      closeDialog={closeDialog}
      closeDialogButtonLabel={translate('close')}
    >
      <ContentWrapper md={12}>
        <p className="escoListActionDialog__info">{translate('exportText')}</p>
        <Typography
          className="escoListDownloadEntryDialog__buttonGroupHeader"
          variant="body1"
        >
          {translate('exportFormatSelect')}
        </Typography>
        <ButtonGroup
          color="primary"
          aria-label={translate('selectFormatAriaLabel')}
        >
          {formats.map(({ name, media }) => (
            <Button
              key={name}
              onClick={() => setFormat(media)}
              variant={format === media ? 'contained' : 'outlined'}
            >
              {name}
            </Button>
          ))}
        </ButtonGroup>

        <TextField
          value={decodeURIComponent(metadataURL.href)}
          readOnly
          aria-describedby="format"
          inputProps={{
            id: 'format',
            'aria-label': 'format',
          }}
        />
      </ContentWrapper>
    </ListActionDialog>
  );
};

ListDownloadEntryDialog.propTypes = {
  entry: PropTypes.shape({
    getEntryInfo: PropTypes.func,
    getId: PropTypes.func,
  }),
  closeDialog: PropTypes.func,
  profile: PropTypes.string,
  nlsBundles: nlsBundlesPropType,
};

export default ListDownloadEntryDialog;
