// IMPORTANT:
// This is an example of the most minimal possible local.js configuration for Suite.
// The actual configuration should be at entryscape/app/suite/theme/local.js
// If it's missing, you need to create the file.

// eslint-disable-next-line camelcase, no-undef
__entryscape_config = {
  entrystore: {
    repository: 'http://localhost:8080/store/',
  },
  entryscape: {},
};
