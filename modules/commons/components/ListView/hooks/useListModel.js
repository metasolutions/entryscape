import {
  createContext,
  useContext,
  useEffect,
  useReducer,
  useRef,
} from 'react';
import useLocationState from 'commons/components/router/useLocationState';
import PropTypes from 'prop-types';
import {
  SEARCH,
  CLEAR_SEARCH,
  PAGINATE,
  SORT,
  RESET,
  REFRESH,
  REFRESH_AND_CLEAR,
  RERENDER,
  CREATE,
  DELETE,
  TOGGLE_TABLEVIEW,
  TOGGLE_FILTERS,
  LIMIT,
  FILTER,
  CLEAR_FILTER,
} from 'commons/actions/actionTypes';
import withActionCallback from 'commons/actions/hocs/withActionCallback';

export * from 'commons/actions/actionTypes';

const ListModelContext = createContext();

export const LIST_MODEL_DEFAULT_VALUE = {
  page: 0,
  sort: {
    field: 'modified',
    order: 'desc',
  },
  search: '',
  refreshCount: 0,
  showTableView: false,
  showFilters: false,
  limit: 10,
  filter: {},
};

const listModelReducer = (state, action) => {
  switch (action.type) {
    case SEARCH:
      return {
        ...state,
        search: action.value,
        page: 0,
      };
    case CLEAR_SEARCH:
      return {
        ...state,
        search: '',
      };
    case FILTER:
      return {
        ...state,
        filter: {
          ...state.filter,
          ...action.filter,
        },
        refreshCount: state.refreshCount + 1, // increase to trigger new query
      };
    case CLEAR_FILTER:
      return {
        ...state,
        filter: {},
      };
    case PAGINATE:
      return {
        ...state,
        page: action.value,
      };
    case LIMIT:
      return {
        ...state,
        page: 0,
        limit: action.value,
      };
    case SORT:
      return {
        ...state,
        sort: action.value,
        page: 0,
      };
    case REFRESH:
      return {
        ...state,
        refreshCount: state.refreshCount + 1, // increase to trigger refresh
        page: 0,
      };
    case REFRESH_AND_CLEAR:
      return {
        ...state,
        refreshCount: state.refreshCount + 1,
        search: '',
        page: 0,
      };
    case RERENDER: // action to trigger rerender by making shallow copy of state
      return {
        ...state,
      };
    case CREATE:
    case DELETE:
      return {
        ...state,
        refreshCount: state.refreshCount + 1, // increase to trigger refresh
      };
    case RESET: {
      const initialState = action?.value || LIST_MODEL_DEFAULT_VALUE;
      return {
        ...initialState,
      };
    }
    case TOGGLE_TABLEVIEW: {
      return {
        ...state,
        showTableView: !state.showTableView,
      };
    }
    case TOGGLE_FILTERS: {
      return {
        ...state,
        showFilters: action.value ?? !state.showFilters,
      };
    }
    default:
      throw new Error(`${action.type} is not a valid list action type`);
  }
};

// ListModelProvider is required to use ListView. It handles the common ui
// states for list view components, for example search and sorting. The
// useListModel hook is used to access state och dispatch updates.
// Recommended way to add the provider to a list view is by the
// withListModelProvider hoc. For example like this:
// withListModelProvider(ListViewExample)
export const ListModelProvider = ({
  children,
  initialValue = LIST_MODEL_DEFAULT_VALUE,
}) => {
  const [listModel, dispatch] = useReducer(listModelReducer, initialValue);

  return (
    <ListModelContext.Provider value={[listModel, dispatch]}>
      {children}
    </ListModelContext.Provider>
  );
};

ListModelProvider.propTypes = {
  children: PropTypes.node,
  initialValue: PropTypes.shape({}),
};

// The useListModel hook is used to get the list model context,
// in order to access state and dispatch function in a list view.
export const useListModel = () => {
  const context = useContext(ListModelContext);
  if (context === undefined)
    throw new Error('ListModel context must be used within context provider');
  return context;
};

// hoc to add ListModelProvider to a list view
export const withListModelProvider =
  (Component, getInitialValue = () => ({})) =>
  (props) =>
    (
      <ListModelProvider
        initialValue={{ ...LIST_MODEL_DEFAULT_VALUE, ...getInitialValue() }}
      >
        <Component {...props} />
      </ListModelProvider>
    );

/**
 * Get initial list model state from location.
 *
 * @param {object} locationState
 * @returns {object}
 */
const getInitialValueFromLocation = (locationState) => {
  if (!locationState) return {};

  return ['search', 'page', 'limit', 'filter'].reduce((state, nextProp) => {
    if (nextProp in locationState) {
      state[nextProp] = locationState[nextProp];
    }
    return state;
  }, {});
};

const LocationState = ({ children }) => {
  const [{ search, page, limit, filter }] = useListModel();
  const { setLocationState } = useLocationState();
  const isMounting = useRef(true);

  useEffect(() => {
    if (isMounting.current) {
      isMounting.current = false;
      return;
    }
    setLocationState({ search, page, limit, filter });
  }, [search, page, filter, limit, setLocationState]);

  return children;
};

export const withListModelProviderAndLocation = (
  Component,
  getInitialValue = () => ({})
) => {
  const ListModelProviderWrapper = (props) => {
    const { locationState } = useLocationState();

    return (
      <ListModelProvider
        initialValue={{
          ...LIST_MODEL_DEFAULT_VALUE,
          ...getInitialValue(),
          ...getInitialValueFromLocation(locationState),
        }}
      >
        <LocationState>
          <Component {...props} />
        </LocationState>
      </ListModelProvider>
    );
  };

  return ListModelProviderWrapper;
};

export const withListRefresh = (Dialog, callbackName) =>
  withActionCallback(useListModel, Dialog, callbackName, REFRESH);

export const withListRerender = (Dialog, callbackName) =>
  withActionCallback(useListModel, Dialog, callbackName, RERENDER);
