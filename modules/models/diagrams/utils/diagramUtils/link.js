import { Entry } from '@entryscape/entrystore-js';
import { shapes } from 'jointjs';
import { namespaces } from '@entryscape/rdfjson';
import { getLabel } from 'commons/util/rdfUtils';
import { LABEL_TYPE_URI } from '../definitions';

const getGenericProps = (sourceId, targetId, targetMarker) => ({
  source: {
    id: sourceId,
    anchor: {
      name: 'perpendicular',
      args: {
        padding: 10,
      },
    },
  },
  target: {
    id: targetId,
    anchor: {
      name: 'perpendicular',
      args: {
        padding: 10,
      },
    },
  },
  router: {
    name: 'manhattan',
    args: {
      padding: 20,
      step: 10,
    },
  },
  connector: { name: 'rounded' },
  attrs: {
    line: {
      stroke: '#333333',
      strokeWidth: 1,
      targetMarker: targetMarker || {
        type: 'path',
        d: 'M 12 -6 0 0 12 6 0 0 Z',
      },
    },
  },
});

const maybeShorten = (str) =>
  str.length > 25 ? `${str.substring(0, 23)} …` : str;

const appendLabel = (link, label) => {
  link.appendLabel({
    attrs: {
      text: {
        text: maybeShorten(label),
        fontSize: 10,
      },
      rect: {
        fill: 'rgba(255, 255, 255, 0.7)',
        x: -4,
        width: 'calc(w+8)',
        stroke: 'rgba(255, 255, 255, 0.7)',
        strokeWidth: 1,
      },
    },
    position: { offset: 10 },
  });
};

/**
 * Updates the link label. When updating the label, both the model and the
 * coupling to the underlying svg must be updated. Thus both label and the attr
 * are set. The attr method call triggers refresh of the svg.
 *
 * @param {object} link
 * @param {string} label
 * @returns {object|undefined}
 */
const updateLabelText = (link, label) => {
  const labels = link.labels();
  if (!labels.length) return;
  labels[0].attrs.text.text = label;
  link.label(0, labels[0]);
  link.attr('text/text', label);
  return link;
};

/**
 *
 * @param {object} link
 * @returns {string|undefined}
 */
const labelLinkByEntry = (link) => {
  const { attributes } = link;
  const { propertyEntry, classEntry, itemEntry } = attributes;
  const entry = propertyEntry || classEntry || itemEntry;
  if (!entry) return;
  const label = getLabel(entry);
  return updateLabelText(link, maybeShorten(label));
};

/**
 *
 * @param {object} link
 * @param {object} namespaceStore
 * @returns {string|undefined}
 */
const labelLinkByUri = (link, namespaceStore) => {
  const { attributes } = link;
  const { property, object } = attributes;
  return updateLabelText(link, namespaceStore.shortenKnown(property || object));
};

/**
 *
 * @param {string} labelType
 * @returns {Function}
 */
export const getLinkLabelSetter = (labelType) => {
  if (labelType === LABEL_TYPE_URI) return labelLinkByUri;
  return labelLinkByEntry;
};

const createLink = (property, label, sourceId, targetId) => {
  const props = getGenericProps(sourceId, targetId);
  const link = new shapes.standard.Link(props);
  appendLabel(link, label);
  link.prop('property', property);
  return link;
};

/**
 *
 * @param {Entry} formEntry
 * @param {Entry} itemEntry
 * @param {string} property
 * @param {string} label
 * @param {string} sourceId
 * @param {string} targetId
 * @returns {object}
 */
export const createLinkFromField = (
  formEntry,
  itemEntry,
  property,
  label,
  sourceId,
  targetId
) => {
  const link = createLink(property, label, sourceId, targetId);
  link.prop('formEntry', formEntry);
  link.prop('itemEntry', itemEntry);
  return link;
};

/**
 *
 * @param {Entry} entry
 * @param {string} property
 * @param {string} object
 * @param {string} label
 * @param {string} sourceId
 * @param {string} targetId
 * @returns {object}
 */
export const createLinkFromProperty = (
  entry,
  property,
  object,
  label,
  sourceId,
  targetId
) => {
  const link = createLink(property, label, sourceId, targetId);
  link.prop({
    propertyEntry: entry,
    object,
  });
  return link;
};

/**
 *
 * @param {Entry} entry
 * @param {string} object
 * @param {string} sourceId
 * @param {string} targetId
 * @returns {object}
 */
export const createLinkForSubClassRelation = (
  entry,
  object,
  sourceId,
  targetId
) => {
  const props = getGenericProps(sourceId, targetId, {
    type: 'path',
    fill: 'white',
    d: 'M 12 -6 0 0 12 6 Z',
  });
  const link = new shapes.standard.Link(props);
  link.prop({
    property: namespaces.expand('rdfs:subClassOf'),
    subject: entry.getResourceURI(),
    object,
  });
  return link;
};
