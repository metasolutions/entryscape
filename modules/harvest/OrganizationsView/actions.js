import { ACTION_REMOVE } from 'commons/actions';
import escoListNLS from 'commons/nls/escoList.nls';
import esreHarvestNLS from 'harvest/nls/esreHarvest.nls';
import CreatePipelineDialog from 'harvest/dialogs/CreatePipelineDialog';
import RemovePipelineDialog from 'harvest/dialogs/RemovePipelineDialog';
import { getGroupWithHomeContext } from 'commons/util/store';
import { isManager } from 'commons/util/user';

const nlsBundles = [esreHarvestNLS, escoListNLS];

const listActions = [
  {
    id: 'create',
    Dialog: CreatePipelineDialog,
    labelNlsKey: 'createButton',
  },
];

const rowActions = [
  {
    ...ACTION_REMOVE,
    Dialog: RemovePipelineDialog,
    isVisible: async ({ entry, userEntry, userInfo }) => {
      if (userInfo.hasAdminRights) return true;

      const groupEntry = await getGroupWithHomeContext(entry.getContext());
      return isManager(groupEntry, userEntry);
    },
  },
];

export { listActions, rowActions, nlsBundles };
