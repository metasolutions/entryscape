import config from 'config';
import { i18n } from 'esi18n';

const getDateFormat = (localLang, configKey = 'shortDatePattern') => {
  for (let i = 0; i < config.get('locale.supported').length; i++) {
    const supportedLang = config.get('locale.supported')[i].lang;
    if (localLang === supportedLang) {
      return config.get('locale.supported')[i][configKey];
    }
  }

  return '';
};
const getMultipleDateFormats = (date) => {
  let period = 'older';
  const lang = i18n.getLocale();
  const modDateMedium = i18n.getDate(date, {
    selector: 'date',
    formatLength: 'medium',
  });
  let short = i18n.getDate(date, {
    selector: 'date',
    datePattern: getDateFormat(lang, 'shortDatePatternWithYear'),
  });
  const cd = new Date();
  const currentDateMedium = i18n.getDate(cd, {
    selector: 'date',
    formatLength: 'medium',
  });
  if (currentDateMedium === modDateMedium) {
    period = 'today';
    short = i18n.getDate(date, { selector: 'time', formatLength: 'short' });
  } else if (date.getYear() === cd.getYear()) {
    period = 'thisyear';
    short = i18n.getDate(date, {
      selector: 'date',
      datePattern: getDateFormat(lang),
    });
  }
  return {
    dateMedium: modDateMedium,
    timeMedium: i18n.getDate(date, {
      selector: 'time',
      formatLength: 'medium',
    }),
    full: i18n.getDate(date, { formatLength: 'full' }),
    short,
    period,
  };
};

const getShortDate = (date) => getMultipleDateFormats(date).short;

export default {
  getMultipleDateFormats,
  getDateFormat,
};

export { getMultipleDateFormats, getDateFormat, getShortDate };
