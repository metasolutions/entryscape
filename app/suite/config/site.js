import adminSiteConfig from 'admin/config/site';
import catalogSiteConfig from 'catalog/config/site';
import Signin from 'commons/auth/Signin';
import Signup from 'commons/auth/Signup';
import merge from 'commons/merge';
import Start from 'commons/start';
import Version from 'commons/version';
import termsSiteConfig from 'terms/config/site';
import workbenchSiteConfig from 'workbench/config/site';
import formsSiteConfig from 'models/config/site';
import searchSiteConfig from 'search/config/site';
import { SINGLE_COLUMN_LAYOUT } from 'commons/Layout';

const siteConfig = merge(
  adminSiteConfig,
  catalogSiteConfig,
  workbenchSiteConfig,
  formsSiteConfig,
  termsSiteConfig,
  searchSiteConfig,
  {
    startView: 'start', // mandatory
    signinView: 'signin',
    signupView: 'signup',
    permissionView: 'permission',
    sidebar: { wide: false, always: true, replaceTabs: true },
    modules: [
      {
        name: 'search',
        title: { en: 'Search', sv: 'Sök', de: 'Suche' },
        productName: { en: 'Search', sv: 'Sök', de: 'Suche' },
        startView: 'search__list',
        public: true,
      },
      {
        name: 'documentation',
        title: {
          en: 'Documentation',
          sv: 'Dokumentation',
          de: 'Dokumentation',
        },
        productName: {
          en: 'Documentation',
          sv: 'Dokumentation',
          de: 'Dokumentation',
        },
        faClass: 'book',
        startView: 'documentation',
        public: false,
      },
    ],
    views: [
      {
        name: 'version',
        title: { en: 'version' },
        class: Version,
        route: '/version',
        restrictTo: 'admin',
      },
      {
        name: 'signin',
        title: {
          en: 'Sign in',
          sv: 'Logga in',
          da: 'Login',
          de: 'Anmelden',
        },
        class: Signin,
        route: '/signin',
        layout: SINGLE_COLUMN_LAYOUT,
        public: true,
      },
      {
        name: 'signup',
        title: {
          en: 'Create account',
          sv: 'Skapa konto',
          de: 'Konto erstellen',
        },
        class: Signup,
        route: '/signup',
        layout: SINGLE_COLUMN_LAYOUT,
        public: true,
      },
      {
        name: 'start',
        class: Start,
        title: { en: 'Start', sv: 'Start', da: 'Start', de: 'Start' },
        route: '/start',
      },
      {
        name: 'documentation',
        title: {
          en: 'Documentation',
          sv: 'Dokumentation',
          de: 'Dokumentation',
        },
        route: 'https://docs.entryscape.com/',
      },
    ],

    moduleList: [
      'catalog',
      'terms',
      'workbench',
      'search',
      'admin',
      'documentation',
    ],
  }
);

export default siteConfig;
