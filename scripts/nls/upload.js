/* eslint-disable no-await-in-loop */
const nls = require('./nls');
const poe = require('./poeclient');
const { projects } = require('./projects');
const { logGroup, getModuleNames, delayWithProgress } = require('./utils');

let apikey;
try {
  apikey = require('./apikey');
} catch (e) {
  console.log(
    'You have to provide an API-key from POEditor and put it in the apikey.js file,' +
      ' see how it is done in apikey.js_example'
  );
  process.exit(1);
}

(async () => {
  logGroup('Uploading to POEditor projects...\n', false);
  for (const { id, paths } of projects) {
    // Retrieve terms and definitions
    let terms = [];
    let definitions = [];
    paths.forEach((path) => {
      // merge terms and definitions if there's more than one path per project
      terms = [...terms, ...nls.readTerms(false, path, false)];
      definitions = [...definitions, ...nls.readTerms(false, path, true)];
    });

    const poeAuth = poe(apikey, id);

    logGroup(getModuleNames(paths), '\n');

    // push terms to poeditor.
    await poeAuth.syncTerms(terms);

    console.log(`Uploading english translations from ${paths}`);
    await poeAuth.uploadDefinitions(definitions, 'en');

    console.log('Waiting 10 seconds due to API restrictions');
    await delayWithProgress(10000);
    process.stdout.clearLine();
    process.stdout.cursorTo(0);
    console.groupEnd();
  }
})();
