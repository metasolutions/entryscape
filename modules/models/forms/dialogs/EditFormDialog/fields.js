import * as fields from 'models/utils/fieldDefinitions';
import * as forms from 'models/utils/formDefinitions';

const editFields = [
  {
    ...forms.FORM_PROPERTY,
  },
  {
    ...forms.FORM_NODETYPE,
  },
  {
    ...fields.FIELD_STATUS,
  },
  {
    ...forms.FORM_CARDINALITY,
  },
  {
    ...fields.FIELD_LABEL,
  },
  {
    ...fields.FIELD_DESCRIPTION,
  },
  {
    ...fields.FIELD_PURPOSE,
  },
  {
    ...fields.FIELD_SPECIFICATION,
  },
  {
    ...fields.FIELD_CSS,
  },
  {
    ...fields.FIELD_EDIT_LABEL,
  },
  {
    ...fields.FIELD_EDIT_DESCRIPTION,
  },
];

export default editFields;
