import React, { useContext, useEffect } from 'react';
import PropTypes from 'prop-types';
import { entrystore } from 'commons/store';
import { ESContextContext } from 'commons/contexts';
import useAsync from 'commons/hooks/useAsync';

let esContext;
let esContextName;

/**
 * A context provider that utilizes the render props technique
 * and renders the children views only if context has been loaded
 *
 * @param {object} props
 * @param {string} props.contextId
 * @param {object} props.children
 * @returns {object}
 */
const ESContextProvider = ({ contextId, children }) => {
  const { runAsync, error, isLoading, data } = useAsync({
    data: { context: null, contextEntry: null, contextName: '' },
  });

  const ctx = entrystore.getContextById(contextId);
  esContext = ctx;
  esContextName = data?.contextName;

  useEffect(() => {
    const getContext = async () => {
      if (!contextId) {
        throw Error('Could not read the "context" parameter from the URL path');
      }
      const ctxEntry = await ctx.getEntry();
      return {
        context: ctx,
        contextEntry: ctxEntry,
        contextName: ctxEntry.getEntryInfo().getName(),
      };
    };
    runAsync(getContext());

    return () => {
      esContext = null;
      esContextName = '';
    };
  }, [contextId, ctx, runAsync]);

  return isLoading ? null : (
    <ESContextContext.Provider value={{ ...data, error }}>
      {children}
    </ESContextContext.Provider>
  );
};

ESContextProvider.propTypes = {
  contextId: PropTypes.string,
  children: PropTypes.node,
};

/**
 * Hook for ES Context
 *
 * @returns {object}
 */
function useESContext() {
  const context = useContext(ESContextContext);
  if (context === undefined) {
    throw new Error('useESContext must be used within a ContextProvider');
  }
  return context;
}

/**
 * Use this method in callbacks when useESContext cannot be used.
 *
 * @returns {object}
 */
function getESContext() {
  return esContext;
}

export const getESContextAndName = () => {
  return { context: esContext, contextName: esContextName };
};

export { ESContextProvider, useESContext, getESContext };
