import { namespaces as ns } from '@entryscape/rdfjson';
import {
  RDF_PROPERTY_OPTION,
  RDF_TYPE_SELECT_FIELD,
  RDF_PROPERTY_URI,
  RDF_PROPERTY_VALUE,
} from './ns';
import { matchesType } from './metadata';
import { convertURIValuesToLiterals, getHasURIValue } from './resource';

/**
 * Callback to detect if nodetype change causes corrupt values or requires
 * conversion. In the latter case a conversion function is also detected.
 *
 * @param {object} field
 * @param {string} value
 * @returns {Promise<{messageNls: string, conversion: Function}>}
 */
export const getConversionOnNodetypeChange = async (field, value) => {
  const graph = field.getGraph();
  const resourceURI = field.getResourceURI();
  if (!matchesType(graph, resourceURI, [RDF_TYPE_SELECT_FIELD])) return;

  // if no option value, do nothing
  const statements = graph.find(resourceURI, RDF_PROPERTY_OPTION);
  if (!statements.length) return;

  const blankNodeIds = statements.map((statement) => statement.getValue());
  const hasURI = getHasURIValue(graph, blankNodeIds, RDF_PROPERTY_VALUE);
  // Change from uri to literal. Conversion required.
  if (hasURI) {
    return {
      messageNls: 'nodetypeConversionWarning',
      converter: async () =>
        convertURIValuesToLiterals(graph, blankNodeIds, RDF_PROPERTY_VALUE),
    };
  }
  // Change from literal to uri. May cause corrupt values.
  if (value === ns.expand(RDF_PROPERTY_URI) || value === RDF_PROPERTY_URI) {
    return {
      messageNls: 'nodetypeChangeWarning',
    };
  }
};
