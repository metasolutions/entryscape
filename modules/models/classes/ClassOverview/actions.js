import {
  ACTION_EDIT,
  ACTION_REMOVE,
} from 'commons/components/overview/actions';
import ReplaceLinkDialog from 'commons/components/common/dialogs/ReplaceLinkDialog';
import { NamespaceLinkField } from 'models/editors/NamespaceLinkField';

export const sidebarActions = [
  ACTION_EDIT,
  {
    id: 'replace-uri',
    Dialog: ReplaceLinkDialog,
    action: {
      ReplaceURIField: NamespaceLinkField,
      copyOldLink: true,
    },
    labelNlsKey: 'replaceURIClass',
    isVisible: ({ entry }) => !entry.isLinkReference(),
  },
  ACTION_REMOVE,
];
