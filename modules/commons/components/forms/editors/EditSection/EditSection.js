import PropTypes from 'prop-types';
import { Typography } from '@mui/material';
import { nlsBundlesPropType } from 'commons/hooks/useTranslation';
import './EditSection.scss';
import { FieldEditor } from '../FieldEditor';

export const EditSection = ({
  label,
  property,
  name,
  editors,
  nlsBundles,
  translate,
  size,
}) => {
  return (
    <div className="escoFormEditSection">
      <Typography
        variant="h2"
        id={`fields-label-${name || property}`}
        className="escoFormEditSection__heading"
      >
        {label}
      </Typography>
      {editors.map((editorConfig) => {
        return (
          <FieldEditor
            key={editorConfig.property}
            nlsBundles={nlsBundles}
            translate={translate}
            size={size}
            {...editorConfig}
          />
        );
      })}
    </div>
  );
};

EditSection.propTypes = {
  children: PropTypes.node,
  label: PropTypes.string,
  property: PropTypes.string,
  name: PropTypes.string,
  editors: PropTypes.arrayOf(PropTypes.shape({})),
  nlsBundles: nlsBundlesPropType,
  translate: PropTypes.func,
  size: PropTypes.string,
};
