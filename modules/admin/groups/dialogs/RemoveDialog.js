import PropTypes from 'prop-types';
import {
  useTranslation,
  nlsBundlesPropType,
} from 'commons/hooks/useTranslation';
import { useMainDialog } from 'commons/hooks/useMainDialog';
import {
  AcknowledgeAction,
  ConfirmAction,
} from 'commons/components/common/dialogs/MainDialog';
import { entrystore } from 'commons/store';
import { getRenderName } from 'commons/util/rdfUtils';
import RemoveEntryDialog from 'commons/components/entry/RemoveEntryDialog';
import { entryPropType } from 'commons/util/entry';
import useNavigate from 'commons/components/router/useNavigate';

const RemoveDialog = ({
  entry,
  actionParams,
  nlsBundles,
  closeDialog,
  ...restProps
}) => {
  const { openMainDialog } = useMainDialog();
  const t = useTranslation(nlsBundles);
  const groupName = getRenderName(entry) || entry.getId();
  const { goBack } = useNavigate();

  const confirmRemoveHomeContext = async (homeContextId) => {
    try {
      const context = entrystore.getContextById(homeContextId);
      const homeContextEntry = await context.getEntry();
      if (homeContextEntry) {
        openMainDialog({
          content: t('removeHomecontext'),
          actions: (
            <ConfirmAction
              onDone={() => homeContextEntry.del()}
              affirmLabel={t('confirmRemoveHomecontext')}
              rejectLabel={t('cancelRemoveHomecontext')}
            />
          ),
        });
      }
    } catch (error) {
      console.error(error);
    }
  };

  const onRemove = async () => {
    const entryResource = entry.getResource(true);
    const homeContextId = entryResource.getHomeContext();
    if (homeContextId) {
      await confirmRemoveHomeContext(homeContextId);
    }
    try {
      await entry.del();
    } catch (error) {
      openMainDialog({
        content: t('failedRemoveGroup'),
        actions: <AcknowledgeAction onDone={closeDialog} />,
      });
      console.error(error);
    }
  };

  return (
    <RemoveEntryDialog
      {...restProps}
      closeDialog={closeDialog}
      onRemove={onRemove}
      onRemoveCallback={goBack}
      removeConfirmMessage={t('removeGroupMessage', groupName)}
    />
  );
};

RemoveDialog.propTypes = {
  entry: entryPropType,
  actionParams: PropTypes.arrayOf(PropTypes.shape({})),
  nlsBundles: nlsBundlesPropType,
  closeDialog: PropTypes.func,
};

export default RemoveDialog;
