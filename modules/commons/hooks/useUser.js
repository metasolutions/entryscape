import PropTypes from 'prop-types';
import CircularProgress from '@mui/material/CircularProgress';
import Fade from '@mui/material/Fade';
import config from 'config';
import React, { useContext, useEffect, useReducer } from 'react';
import { UserStateContext, UserDispatchContext } from 'commons/contexts';
import { getCurrentUserEntry, logout } from 'commons/util/user';
import { UNAUTHORIZED, extractProblem } from 'commons/errors/utils/async';
import { AUTH_CURRENT_USER_SUCCESS, AUTH_LOGOUT_SUCCESS } from './user/actions';
import reducer from './user/reducer';

function UserProvider({ children }) {
  /**
   * @type {[{userEntry: object, userInfo: object}, function]}
   */
  const [userState, userDispatch] = useReducer(reducer, {
    userEntry: null,
    userInfo: {},
  });

  // run only first time hook is rendered
  useEffect(() => {
    // TODO `authentification` should go. There's no application loading if that's false as it is
    if (config.get('entrystore.authentification')) {
      getCurrentUserEntry()
        .then(({ userEntry, userInfo }) => {
          userDispatch({
            type: AUTH_CURRENT_USER_SUCCESS,
            data: {
              userEntry,
              userInfo,
            },
          });
        })
        .catch((error) => {
          const problem = extractProblem(error);
          if (problem !== UNAUTHORIZED) {
            console.error(error);
            return;
          }

          // can be unauthorized if for example expired or invalid access token
          logout().then(({ userEntry, userInfo }) => {
            userDispatch({
              type: AUTH_LOGOUT_SUCCESS,
              data: {
                userEntry,
                userInfo,
              },
            });
          });
        });
    }
  }, []);

  if (!userState.userEntry) {
    // Should fix with suspense when ready
    return (
      <div
        style={{
          // ! move styles elsewhere
          display: 'flex',
          position: 'absolute',
          width: '100%',
          height: '100%',
          justifyContent: 'center',
          alignItems: 'center',
        }}
      >
        <Fade
          in={!userState.userEntry}
          style={{ transitionDelay: '800ms' }}
          unmountOnExit
        >
          <CircularProgress />
        </Fade>
      </div>
    );
  }

  return (
    <UserStateContext.Provider value={userState}>
      <UserDispatchContext.Provider value={userDispatch}>
        {children}
      </UserDispatchContext.Provider>
    </UserStateContext.Provider>
  );
}

UserProvider.propTypes = {
  children: PropTypes.node,
};

function useUserState() {
  const context = useContext(UserStateContext);
  if (context === undefined) {
    throw new Error('useUserState must be used within a UserProvider');
  }
  return context;
}

function useUserDispatch() {
  const context = useContext(UserDispatchContext);
  if (context === undefined) {
    throw new Error('useUserDispatch must be used within a UserProvider');
  }
  return context;
}

export { UserProvider, useUserState, useUserDispatch };
