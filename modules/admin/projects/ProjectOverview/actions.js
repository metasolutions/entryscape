import {
  ACTION_EDIT,
  ACTION_REVISIONS,
  ACTION_REMOVE,
} from 'commons/components/overview/actions';
import {
  Delete as RemoveIcon,
  Autorenew as ReindexIcon,
} from '@mui/icons-material';
import ReindexProjectDialog from '../ProjectsView/dialogs/ReindexProjectDialog';
import RemoveProjectDialog from '../ProjectsView/dialogs/RemoveProjectDialog';

export const sidebarActions = [
  {
    ...ACTION_EDIT,
    getProps: () => ({
      action: {
        formTemplateId: 'esc:Context',
      },
    }),
  },
  ACTION_REVISIONS,
  {
    id: 'reindex',
    Dialog: ReindexProjectDialog,
    labelNlsKey: 'projectReindex',
    getProps: () => ({
      startIcon: <ReindexIcon />,
    }),
  },
  {
    ...ACTION_REMOVE,
    Dialog: RemoveProjectDialog,
    getProps: () => ({
      startIcon: <RemoveIcon />,
    }),
  },
];
