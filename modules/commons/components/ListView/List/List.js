import PropTypes from 'prop-types';
import { List as MuiList } from '@mui/material';
import {
  SearchOff as NoSearchResultsIcon,
  WarningAmber as WarningIcon,
} from '@mui/icons-material';
import { useTranslation } from 'commons/hooks/useTranslation';
import escoListNLS from 'commons/nls/escoList.nls';
import Loader from 'commons/components/Loader';
import { REJECTED } from 'commons/hooks/useAsync';
import { useListView } from '../hooks/useListView';
import { ListPlaceholder } from '../ListPlaceholder';

// Default list placeholder
const EmptyListPlaceholder = ({
  Icon = NoSearchResultsIcon,
  label,
  search,
  className,
  variant,
}) => {
  const t = useTranslation(escoListNLS);
  return (
    <ListPlaceholder
      Icon={Icon}
      label={label || t('emptySearchResult', search)}
      className={className}
      variant={variant}
    />
  );
};

EmptyListPlaceholder.propTypes = {
  Icon: PropTypes.elementType,
  label: PropTypes.string,
  search: PropTypes.string,
  className: PropTypes.string,
  variant: PropTypes.oneOf(['compact', 'dialog', 'view']),
};

// The List component is primarily a container for list items.
// The renderPlaceholder function can be used to render a customized placeholder
// component, based on EmptyListPlaceholder component.
export const List = ({
  renderPlaceholder,
  renderRejectedPlaceholder = (RejectedPlaceholder, placeholderProps = {}) => (
    <RejectedPlaceholder {...placeholderProps} />
  ),
  renderLoader = (ListLoader) => <ListLoader />,
  children,
}) => {
  const translate = useTranslation(escoListNLS);
  const { search, size, status } = useListView();
  const isEmpty = size === 0 && status !== 'pending';
  const isPending = status === 'pending' || (status === 'idle' && size === -1);

  if (status === REJECTED) {
    return renderRejectedPlaceholder(EmptyListPlaceholder, {
      label: translate('rejectedPlaceholderLabel'),
      Icon: WarningIcon,
      variant: 'compact',
    });
  }

  if (isEmpty)
    return renderPlaceholder ? (
      renderPlaceholder(EmptyListPlaceholder)
    ) : (
      <EmptyListPlaceholder search={search} />
    );
  return <MuiList>{isPending ? renderLoader(Loader) : children}</MuiList>;
};

List.propTypes = {
  children: PropTypes.node,
  renderPlaceholder: PropTypes.func,
  renderRejectedPlaceholder: PropTypes.func,
  renderLoader: PropTypes.func,
};
