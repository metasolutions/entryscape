import PropTypes from 'prop-types';
import ClearIcon from '@mui/icons-material/Cancel';
import RemoveCircleIcon from '@mui/icons-material/RemoveCircle';
import SearchIcon from '@mui/icons-material/Search';
import { Box, IconButton } from '@mui/material';
import './FieldControl.scss';
import Tooltip from 'commons/components/common/Tooltip';
import ConditionalWrapper from 'commons/components/common/ConditionalWrapper';
import { useTranslation } from 'commons/hooks/useTranslation';
import escoRdformsNLS from 'commons/nls/escoRdforms.nls';

export const FieldControl = ({
  children,
  marginLeft = 'auto',
  ...boxProps
}) => {
  return (
    <Box className="escoFieldControl" marginLeft={marginLeft} {...boxProps}>
      {children}
    </Box>
  );
};

FieldControl.propTypes = {
  children: PropTypes.node,
  marginLeft: PropTypes.string,
};

export const FieldControlButton = ({
  children,
  disabled,
  tooltip,
  ariaLabel,
  onClick,
  ...fieldControlProps
}) => {
  return (
    <FieldControl {...fieldControlProps}>
      <ConditionalWrapper
        condition={!disabled && Boolean(tooltip)}
        wrapper={(tooltipChildren) => (
          <Tooltip title={tooltip}>{tooltipChildren}</Tooltip>
        )}
      >
        <IconButton
          aria-label={ariaLabel || tooltip}
          onClick={onClick}
          disabled={disabled}
        >
          {children}
        </IconButton>
      </ConditionalWrapper>
    </FieldControl>
  );
};

FieldControlButton.propTypes = {
  children: PropTypes.node,
  onClick: PropTypes.func,
  tooltip: PropTypes.string,
  ariaLabel: PropTypes.string,
  disabled: PropTypes.bool,
};

export const RemoveButton = (props) => {
  const t = useTranslation(escoRdformsNLS);

  return (
    <FieldControlButton tooltip={t('edit_remove')} {...props}>
      <RemoveCircleIcon />
    </FieldControlButton>
  );
};

export const ClearButton = (props) => {
  const t = useTranslation(escoRdformsNLS);

  return (
    <FieldControlButton tooltip={t('edit_clear')} {...props}>
      <ClearIcon />
    </FieldControlButton>
  );
};

export const SearchButton = ({
  marginLeft = '0',
  ...fieldControlButtonProps
}) => {
  return (
    <FieldControlButton marginLeft={marginLeft} {...fieldControlButtonProps}>
      <SearchIcon color="primary" />
    </FieldControlButton>
  );
};

SearchButton.propTypes = {
  marginLeft: PropTypes.string,
};
