import PropTypes from 'prop-types';
import React, { forwardRef } from 'react';
import { Button } from '@mui/material';
import './index.scss';

const IconButton = forwardRef(({ icon, onClick, ...props }, ref) => (
  // eslint-disable-next-line react/jsx-props-no-spreading
  <Button
    classes={{ root: 'escoIconButton' }}
    onClick={onClick}
    ref={ref}
    {...props}
  >
    <span className="escoIconButton__icon">{icon}</span>
  </Button>
));

IconButton.propTypes = {
  icon: PropTypes.node,
  onClick: PropTypes.func,
};

export default IconButton;
