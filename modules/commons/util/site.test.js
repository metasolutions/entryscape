import {
  GUEST_PERMISSION as GUEST,
  ADMIN_PERMISSION as ADMIN,
  ADV_ADMIN_PERMISSION as ADV_ADMIN,
} from 'commons/hooks/useUserPermission';
import { checkHasViewPermission } from './site';

describe('`site` utility functions', () => {
  test('checkHasViewPermission', () => {
    expect(checkHasViewPermission('', '')).toBe(true);
    expect(checkHasViewPermission(GUEST, GUEST)).toBe(true);
    expect(checkHasViewPermission(ADMIN, ADMIN)).toBe(true);
    expect(checkHasViewPermission(ADV_ADMIN, ADV_ADMIN)).toBe(true);
    expect(checkHasViewPermission(GUEST, ADMIN)).toBe(false);
    expect(checkHasViewPermission(ADMIN, ADV_ADMIN)).toBe(false);
    expect(checkHasViewPermission(ADV_ADMIN, ADMIN)).toBe(true);
  });

  test('checkHasViewPermission w/ module specific check that changes result', () => {
    const module = { checkPermission: jest.fn(() => true) };
    expect(checkHasViewPermission(ADMIN, ADV_ADMIN, module)).toBe(true);
  });
});
