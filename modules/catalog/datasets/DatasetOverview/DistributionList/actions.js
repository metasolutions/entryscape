import { Entry } from '@entryscape/entrystore-js';
// eslint-disable-next-line max-len
import ListRevisionsEntryDialog from 'commons/components/EntryListView/dialogs/ListRevisionsEntryDialog';
import {
  LIST_ACTION_INFO,
  LIST_ACTION_REMOVE,
  LIST_ACTION_REVISIONS,
} from 'commons/components/EntryListView/actions';
import escaDatasetNLS from 'catalog/nls/escaDataset.nls';
import escaManageFilesNLS from 'catalog/nls/escaManageFiles.nls';
import escoListNLS from 'commons/nls/escoList.nls';
import escoErrorsNLS from 'commons/nls/escoErrors.nls';
import { entrystore } from 'commons/store';
import { getResourceURI } from 'commons/util/entry';
import {
  isDistributionUploaded,
  isAPIDistribution,
  isFileDistributionWithoutAPI,
} from '../utils/distributions';
import StatisticsDialog from './dialogs/StatisticsDialog';
import ManageFilesDialog from './dialogs/ManageFilesDialog';
import RemoveDistributionDialog from './dialogs/RemoveDistributionDialog';
import ApiInfoDialog from './dialogs/ApiInfoDialog';
import ActivateAPIDialog from './dialogs/ActivateAPIDialog';
import RefreshAPIDialogWrapper from './dialogs/RefreshAPIDialog/RefreshAPIDialogWrapper';

const hasFilesInRepository = ({ entry: distributionEntry }) => {
  const accessURI = getResourceURI(distributionEntry, 'dcat:accessURL');
  const downloadURI = getResourceURI(distributionEntry, 'dcat:downloadURL');
  const baseURI = entrystore.getBaseURI();

  return accessURI === downloadURI && downloadURI?.indexOf(baseURI) === 0;
};

/**
 *
 * @param {object} params
 * @param {Entry} params.entry - Distribution entry
 * @param {Entry} params.datasetEntry
 * @returns {boolean}
 */
const shouldShowStatistics = ({ entry: distributionEntry, datasetEntry }) =>
  (isDistributionUploaded(distributionEntry) ||
    isAPIDistribution(distributionEntry)) &&
  datasetEntry.getContext().getEntry(true).isPublic();

/**
 *
 * @param {object} params
 * @param {Entry} params.entry - Distribution entry
 * @param {string[]} params.apiDistributionResourceURIs
 * @returns {boolean}
 */
const isActivateApiVisible = ({
  entry: distributionEntry,
  apiDistributionResourceURIs,
}) =>
  isDistributionUploaded(distributionEntry) &&
  isFileDistributionWithoutAPI(distributionEntry, apiDistributionResourceURIs);

export const nlsBundles = [
  escaDatasetNLS,
  escoListNLS,
  escaManageFilesNLS,
  escoErrorsNLS,
];

export const rowActions = [
  {
    id: 'activate-api',
    Dialog: ActivateAPIDialog,
    isVisible: isActivateApiVisible,
    labelNlsKey: 'apiActivateTitle',
  },
  {
    id: 'statistics',
    Dialog: StatisticsDialog,
    isVisible: shouldShowStatistics,
    labelNlsKey: 'seeStatistics',
  },
  { ...LIST_ACTION_REVISIONS, Dialog: ListRevisionsEntryDialog, nlsBundles },
  {
    id: 'api-info',
    Dialog: ApiInfoDialog,
    isVisible: ({ entry }) => isAPIDistribution(entry),
    labelNlsKey: 'apiDistributionTitle',
  },
  {
    id: 'refresh-api',
    Dialog: RefreshAPIDialogWrapper,
    isVisible: ({ entry }) => isAPIDistribution(entry),
    labelNlsKey: 'reGenerateAPI',
  },
  { ...LIST_ACTION_REMOVE, Dialog: RemoveDistributionDialog },
  {
    id: 'manage-files',
    Dialog: ManageFilesDialog,
    isVisible: hasFilesInRepository,
    labelNlsKey: 'manageFilesButtonTitle',
  },
];

export const presenterAction = {
  ...LIST_ACTION_INFO,
  nlsBundles,
};
