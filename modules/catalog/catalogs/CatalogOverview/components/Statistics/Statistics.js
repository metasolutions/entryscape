import { useState, useEffect } from 'react';
import { useESContext } from 'commons/hooks/useESContext';
import { useTranslation } from 'commons/hooks/useTranslation';
import escaStatisticsNLS from 'catalog/nls/escaStatistics.nls';
import escaOverviewNLS from 'catalog/nls/escaOverview.nls';
import ChartPlaceholder from 'commons/components/chart/ChartPlaceholder';
import { List } from '@mui/material';
import useAsync from 'commons/hooks/useAsync';
import Chart from 'commons/components/chart/Time';
import { getStatisticsData } from 'catalog/catalogs/CatalogOverview/util';
import ListItem from '../ListItem';
import './Statistics.scss';

const Statistics = () => {
  const [loading, setLoading] = useState(true);
  const { context } = useESContext();
  const { data: chartData, runAsync } = useAsync(null);
  const t = useTranslation([escaOverviewNLS, escaStatisticsNLS]);

  const stopLoading = () => {
    setLoading(false);
  };

  useEffect(() => {
    runAsync(getStatisticsData(context, t, stopLoading));
  }, [context, runAsync, t]);

  return (
    <div className="escaOverviewStats">
      <h2 className="escaOverviewStats__header">
        {t('statsCatalogOverviewTitle')}
      </h2>

      <List classes={{ root: 'escaOverviewStats__list' }} disablePadding>
        <ListItem
          primary={t('statsCatalogOverviewChartFileLabel')}
          secondary={chartData?.fileTotalCount ? chartData.fileTotalCount : '-'}
        />
        <ListItem
          primary={t('statsCatalogOverviewChartAPILabel')}
          secondary={chartData?.apiTotalCount ? chartData.apiTotalCount : '-'}
        />
      </List>
      {chartData?.bar.datasets?.length ? (
        <Chart
          data={chartData.bar}
          dimensions={{ height: '306px', width: '100%' }}
        />
      ) : (
        <ChartPlaceholder
          label={t('statsChartPlaceholder')}
          loading={loading}
        />
      )}
    </div>
  );
};

export default Statistics;
