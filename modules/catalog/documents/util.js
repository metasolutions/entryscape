import { types } from '@entryscape/entrystore-js';
import { entrystore } from 'commons/store';

const getImportedSpecEntries = async (context) => {
  const searchList = entrystore
    .newSolrQuery()
    .rdfType(['dcterms:Standard', 'prof:Profile'])
    .entryType([types.ET_REF, types.ET_LINKREF])
    .context(context)
    .list();
  const entries = await searchList.getEntries();
  return entries;
};

const importSpecification = async (entry, context) => {
  const remoteEntryRURI = entry.getResourceURI();
  const remoteEntryMetadata = entry.getMetadata();
  const remoteEntryMetadataURI = entry.getEntryInfo().getMetadataURI();

  try {
    const newEntry = await context
      .newLinkRef(remoteEntryRURI, remoteEntryMetadataURI)
      .commit();
    newEntry.setCachedExternalMetadata(remoteEntryMetadata);
    await newEntry.commitCachedExternalMetadata();
  } catch (error) {
    console.error(error);
  }
};

export { getImportedSpecEntries, importSpecification };
