import PropTypes from 'prop-types';
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  Button,
  TextField,
  Link as MuiLink,
} from '@mui/material';
import ContentWrapper from 'commons/components/common/ContentWrapper';
import { useTranslation } from 'commons/hooks/useTranslation';
import escoListNLS from 'commons/nls/escoList.nls';
import escoLayoutNLS from 'commons/nls/escoLayout.nls';
import { useUserState, useUserDispatch } from 'commons/hooks/useUser';
import { useState } from 'react';
import { getCurrentUserEntry, replaceName } from 'commons/util/user';
import { AUTH_CURRENT_USER_SUCCESS } from 'commons/hooks/user/actions';
import { useLanguage } from 'commons/hooks/useLanguage';
import ChangePasswordDialog from 'admin/users/UsersView/dialogs/ChangePasswordDialog';
import LanguageSelect from './LanguageSelect';

const UserSettingsDialog = ({ closeDialog }) => {
  const { userEntry, userInfo } = useUserState();
  const dispatch = useUserDispatch();
  const [firstName, setFirstName] = useState(userInfo.firstName);
  const [lastName, setLastName] = useState(userInfo.lastName);
  const [language, setLanguage] = useState(userInfo.language);
  const [passwordEntry, setPasswordEntry] = useState(null);
  const t = useTranslation([escoListNLS, escoLayoutNLS]);
  const [, setGlobalLanguage] = useLanguage();
  const [error, setError] = useState({
    firstName: '',
    lastName: '',
  });

  const handleFirstNameChange = ({ target }) => {
    setFirstName(target.value);
    if (target.value.trim().length === 1) {
      setError({ ...error, firstName: t('firstNameTooShort') });
      return;
    }
    if (!target.value.trim().length) {
      setError({ ...error, firstName: t('firstNameMissing') });
      return;
    }
    setError({ ...error, firstName: null });
  };

  const handleLastNameChange = ({ target }) => {
    setLastName(target.value);
    if (target.value.trim().length === 1) {
      setError({ ...error, lastName: t('lastNameTooShort') });
      return;
    }
    if (!target.value.trim().length) {
      setError({ ...error, lastName: t('lastNameMissing') });
      return;
    }
    setError({ ...error, lastName: null });
  };

  const handleLanguageChange = (event) => {
    setLanguage(event.target.value);
  };

  const saveUserSettings = async () => {
    try {
      if (error.firstName || error.lastName) return;
      if (language !== userInfo.language) {
        await userEntry.getResource(true).setLanguage(language);
        setGlobalLanguage(language);
      }
      // modification date has changed after setting the language above
      // need to refresh the entry before making further changes
      await userEntry.refresh(true, true);
      await replaceName(userEntry, firstName.trim(), lastName.trim());

      // update user info
      getCurrentUserEntry()
        .then((userData) => {
          dispatch({
            type: AUTH_CURRENT_USER_SUCCESS,
            data: userData,
          });
        })
        .then(closeDialog);
    } catch (errorOnSave) {
      throw Error(`Could not save user settings: ${errorOnSave}`);
    }
  };

  const handlePasswordChangeClick = (e) => {
    e.preventDefault();
    setPasswordEntry(userEntry);
  };

  return (
    <>
      <Dialog
        onClose={closeDialog}
        aria-labelledby="user-settings-dialog-title"
        open
        fullWidth
        maxWidth="md"
      >
        <DialogTitle id="user-setttings-dialog-title" onClose={closeDialog}>
          {t('settingsHeader')}
        </DialogTitle>
        <DialogContent dividers>
          <ContentWrapper>
            <TextField
              disabled
              label={t('username')}
              value={userInfo.user}
              id="user-settings-username"
            />
            <TextField
              label={t('firstname')}
              autoComplete="given-name"
              value={firstName}
              onChange={handleFirstNameChange}
              id="user-settings-firstname"
              error={Boolean(error.firstName)}
              helperText={error.firstName}
              required
            />
            <TextField
              label={t('lastname')}
              autoComplete="family-name"
              value={lastName}
              onChange={handleLastNameChange}
              id="user-settings-lastname"
              error={Boolean(error.lastName)}
              helperText={error.lastName}
              required
            />
            <LanguageSelect
              language={language}
              handleChange={handleLanguageChange}
              textFieldProps={{ variant: 'filled' }}
            />
            <div style={{ marginTop: '20px' }}>
              <MuiLink
                href="#"
                variant="body2"
                onClick={handlePasswordChangeClick}
              >
                {t('passwordLink')}
              </MuiLink>
            </div>
          </ContentWrapper>
        </DialogContent>
        <DialogActions>
          <Button onClick={closeDialog} variant="text">
            {t('cancel')}
          </Button>
          <Button onClick={saveUserSettings}>{t('saveButton')}</Button>
        </DialogActions>
      </Dialog>
      {passwordEntry ? (
        <ChangePasswordDialog
          entry={passwordEntry}
          closeDialog={() => setPasswordEntry(null)}
        />
      ) : null}
    </>
  );
};

UserSettingsDialog.propTypes = {
  closeDialog: PropTypes.func.isRequired,
};

export default UserSettingsDialog;
