import { Link } from 'react-router-dom';
import {
  ListItemText,
  ListItemActionIconButton,
  ListItemIconButton,
  ListItemIcon,
  ListItemActionsGroup,
} from 'commons/components/ListView';
import {
  OpenInNew as OpenInNewIcon,
  Edit as EditIcon,
  Info as InfoIcon,
} from '@mui/icons-material';
import { getShortModifiedDate } from 'commons/util/metadata';
import { getFullLengthLabel } from 'commons/util/rdfUtils';
import { LIST_ACTION_EDIT } from 'commons/components/EntryListView/actions';
import { ListItemActionsMenu } from '../listItems/ListItemActionsMenu';
import { ListPublishContextToggle } from '../listItems/ListPublishContextToggle';
import { ListPublishEntryToggle } from '../listItems/ListPublishEntryToggle';
import { ListPublicStatusIcon } from '../listItems/ListPublicStatusIcon';
import { ListDraftStatusIcon } from '../listItems/ListDraftStatusIcon';
import ListEditEntryDialog from '../dialogs/ListEditEntryDialog';
import EntryListItemText from '../listItems/EntryListItemText';

export const MODIFIED_SORT = 'modified';
export const MODIFIED_COLUMN = {
  id: 'modified',
  xs: 2,
  sortBy: MODIFIED_SORT,
  headerNlsKey: 'listHeaderModifiedDate',
  Component: ListItemText,
  getProps: ({ entry }) => ({ secondary: getShortModifiedDate(entry) }),
};

export const TITLE_SORT = 'title';
export const TITLE_COLUMN = {
  id: 'title',
  xs: 7,
  sortBy: false,
  headerNlsKey: 'listHeaderLabel',
  Component: EntryListItemText,
  getProps: (props) => props,
};

export const TITLE_WITH_CONTEXT_COLUMN = {
  ...TITLE_COLUMN,
  id: 'title-with-context',
  xs: 8,
  getProps: ({ entry }) => {
    return {
      primary: getFullLengthLabel(entry),
      secondary: getFullLengthLabel(entry.getContext().getEntry(true)),
    };
  },
};

export const ACTION_MENU_COLUMN = {
  id: 'action-menu',
  xs: 1,
  Component: ListItemActionsMenu,
  getProps: (props) => props,
};

export const INFO_COLUMN = {
  id: 'info',
  xs: 1,
  Component: ListItemActionIconButton,
  icon: <InfoIcon />,
};

export const TOGGLE_CONTEXT_COLUMN = {
  id: 'toggle-publish-context',
  Component: ListPublishContextToggle,
  xs: 2,
  getProps: ({ entry, translate }) => ({
    entry,
    publicTooltip: translate('publicTooltip'),
    privateTooltip: translate('privateTooltip'),
    noAccess: translate('sharingNoAccess'),
  }),
  headerNlsKey: 'listHeaderPublicLabel',
  highlightId: 'listItemPublish',
};

export const TOGGLE_ENTRY_COLUMN = {
  id: 'toggle-publish-entry',
  xs: 2,
  Component: ListPublishEntryToggle,
  headerNlsKey: 'listHeaderPublicLabel',
  getProps: ({ entry, translate }) => ({
    entry,
    publicTitle: translate('publicTitle'),
    privateTitle: translate('privateTitle'),
    publishDisabledTitle: translate('publishDisabledTitle'),
    toggleAriaLabel: translate('publishToggleAriaLabel'),
  }),
  highlightId: 'listItemPublish',
};

export const PUBLIC_STATUS_COLUMN = {
  id: 'public-status',
  xs: 1,
  headerNlsKey: 'listHeaderPublicLabel',
  Component: ListPublicStatusIcon,
  getProps: ({ entry, translate }) => ({
    entry,
    publicTooltip: translate('publicTooltip'),
    privateTooltip: translate('privateTooltip'),
  }),
};

// Used to indicate draft status where the public status/toggle columns are not used
export const DRAFT_STATUS_COLUMN = {
  id: 'draft-status',
  xs: 1,
  Component: ListDraftStatusIcon,
  getProps: ({ entry }) => ({ entry, tooltipNlsKey: 'draftTooltip' }),
};

export const SOURCE_LINK_COLUMN = {
  id: 'source-link',
  xs: 1,
  color: 'primary',
  component: Link,
  Component: ListItemIconButton,
  icon: <OpenInNewIcon />,
  target: '_blank',
  sx: { padding: 0 },
};

export const EDIT_ENTRY_COLUMN = {
  id: 'edit',
  xs: 1,
  Component: ListItemActionIconButton,
  icon: <EditIcon />,
  getProps: ({ entry, translate }) => ({
    action: {
      ...LIST_ACTION_EDIT,
      entry,
      Dialog: ListEditEntryDialog,
    },
    title: translate('editEntry'),
  }),
};

export const OVERVIEW_ACCESS_COLUMN = {
  id: 'overview-access',
  xs: 1,
  Component: ListItemIcon,
};

export const ACTIONS_GROUP_COLUMN = {
  id: 'actions-group',
  xs: 1,
  Component: ListItemActionsGroup,
  getProps: (props) => props,
};
