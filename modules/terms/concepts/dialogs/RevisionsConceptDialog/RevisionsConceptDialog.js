import PropTypes from 'prop-types';
import { Entry } from '@entryscape/entrystore-js';
import RevisionsEntryDialog from 'commons/components/entry/RevisionsEntryDialog';
import esteConceptNLS from 'terms/nls/esteConcept.nls';

const RevisionsConceptDialog = ({ entry, handleClose, revertCallback }) => {
  return (
    <RevisionsEntryDialog
      entry={entry}
      nlsBundles={[esteConceptNLS]}
      excludeProperties={[
        'skos:inScheme',
        'skos:narrower',
        'skos:broader',
        'skos:related',
        'skos:hasTopConcept',
        'skos:topConceptOf',
      ]}
      filterPredicates={{ 'skosmos:relations': true }}
      closeDialog={handleClose}
      onRevertCallback={revertCallback}
    />
  );
};

RevisionsConceptDialog.propTypes = {
  entry: PropTypes.instanceOf(Entry),
  handleClose: PropTypes.func,
  revertCallback: PropTypes.func,
};

export default RevisionsConceptDialog;
