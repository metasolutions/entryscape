import React, { useContext, useEffect } from 'react';
import PropTypes from 'prop-types';
import { entrystore } from 'commons/store';
import { EntryContext } from 'commons/contexts';
import useAsync from 'commons/hooks/useAsync';
import useHandleError from 'commons/errors/hooks/useErrorHandler';
import { useUserState } from 'commons/hooks/useUser';
import { USER_ENTRY_ID_GUEST } from 'commons/util/userIds';
import useNavigate from 'commons/components/router/useNavigate';
import { getPathFromViewName } from 'commons/util/site';

const EntryProvider = ({ contextId, entryId, children }) => {
  const { data: entry, runAsync, error } = useAsync(null);
  const { userEntry } = useUserState();
  const { navigate } = useNavigate();

  useHandleError(error);

  useEffect(() => {
    const authorizeUser = () => {
      if (!(contextId || entryId))
        throw Error('contextId or entryId is missing.');

      // preload entry
      const uri = entrystore.getEntryURI(contextId, entryId);
      return entrystore.getEntry(uri).catch((getEntryError) => {
        const isGuest = userEntry.getId() === USER_ENTRY_ID_GUEST;
        if (getEntryError.message.includes('Not authorized') && isGuest) {
          navigate(getPathFromViewName('signin'));
        } else {
          throw getEntryError;
        }
      });
    };
    runAsync(authorizeUser());
  }, [runAsync, contextId, entryId, navigate, userEntry]);

  // TODO children should not render before entry is ready and entry is applicable
  return (
    entry && (
      <EntryContext.Provider value={entry}>{children}</EntryContext.Provider>
    )
  );
};

EntryProvider.propTypes = {
  contextId: PropTypes.string,
  entryId: PropTypes.string,
  children: PropTypes.node,
};

const useEntry = () => {
  const entry = useContext(EntryContext);
  if (entry === undefined) {
    throw new Error('useEntry must be used within a ContextProvider');
  }
  return entry;
};

export { EntryProvider, useEntry };
