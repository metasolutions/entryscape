import { namespaces as ns } from '@entryscape/rdfjson';
import config from 'config';
import { getViewDefFromName } from 'commons/util/site';
// import 'commons/rdforms/linkBehaviour';

export default () => {
  ns.add('skos', 'http://www.w3.org/2004/02/skos/core#');
  ns.add('esterms', 'http://entryscape.com/terms/');
  ns.add('void', 'http://rdfs.org/ns/void#');
};

// Copy over include settings to view so secondary navigation is hidden for these views (navbar:false)
const includes2Name = {
  includeOverview: 'terminology__overview',
  includeCollections: 'terminology__collections',
};

Object.keys(includes2Name).forEach((key) => {
  if (config.get(`terms.${key}`) === false) {
    const viewDef = getViewDefFromName(includes2Name[key]);
    viewDef.navbar = false;
  }
});
