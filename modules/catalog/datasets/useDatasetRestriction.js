import config from 'config';
import { useUserState } from 'commons/hooks/useUser';
import { hasUserPermission, ADMIN_RIGHTS, PREMIUM } from 'commons/util/user';
import { isContextPremium } from 'commons/util/context';

const useDatasetRestriction = (numberOfDatasets, context) => {
  const { userEntry } = useUserState();
  const contextEntry = context.getEntry(true);
  const hasPermission = hasUserPermission(userEntry, [PREMIUM, ADMIN_RIGHTS]);
  const isExemptFromRestrictions =
    hasPermission || isContextPremium(contextEntry);

  if (isExemptFromRestrictions) return { isRestricted: false };

  const datasetLimit = config.get('catalog.datasetLimit');
  const pathToDatasetLimitContent = config.get('catalog.datasetLimitDialog');
  const isRestricted =
    (datasetLimit && numberOfDatasets >= datasetLimit) || datasetLimit === 0;

  return { isRestricted, contentPath: pathToDatasetLimitContent };
};

export default useDatasetRestriction;
