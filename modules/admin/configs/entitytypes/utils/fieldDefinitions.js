import {
  URI,
  BLANK,
  DATATYPE_LITERAL,
  LANGUAGE_LITERAL,
  LITERAL,
  GROUP,
  XSD_BOOLEAN,
} from 'commons/components/forms/editors';
import {
  addConstraintFromRdfType,
  addConstraints,
  addURIFromName,
  addStoreConfig,
} from 'admin/configs/utils/exporters';
import { CONFIG_NAMESPACE } from 'admin/configs/utils/namespace';

export const template = {
  name: 'template',
  nodetype: LITERAL,
  property: `${CONFIG_NAMESPACE}:template`,
  labelNlsKey: 'templateLabel',
  mandatory: true,
  max: 1,
};

export const label = {
  name: 'label',
  nodetype: LANGUAGE_LITERAL,
  property: 'dcterms:title',
  labelNlsKey: 'labelLabel',
  mandatory: true,
};

export const rdfType = {
  name: 'rdfType',
  nodetype: BLANK,
  property: `rfs:constraint`,
  toGraph: addConstraintFromRdfType,
  labelNlsKey: 'rdfTypeLabel',
  hidden: true,
};

export const constraints = {
  name: 'constraints',
  nodetype: BLANK,
  property: `rfs:constraint`,
  toGraph: addConstraints,
  labelNlsKey: 'constraintsLabel',
  max: 1,
};

export const description = {
  name: 'description',
  nodetype: LANGUAGE_LITERAL,
  property: 'dcterms:description',
  labelNlsKey: 'descriptionLabel',
};

export const module = {
  name: 'module',
  nodetype: LITERAL,
  property: `${CONFIG_NAMESPACE}:module`,
  choices: [
    {
      value: 'catalog',
      label: {
        en: 'Catalog',
      },
    },
    {
      value: 'workbench',
      label: {
        en: 'Workbench',
      },
    },
    {
      value: 'toolkit',
      label: {
        en: 'Toolkit',
      },
    },
    {
      value: 'terms',
      label: {
        en: 'Terms',
      },
    },
    {
      value: 'admin',
      label: {
        en: 'Admin',
      },
    },
    {
      value: 'models',
      label: {
        en: 'Models',
      },
    },
  ],
  labelNlsKey: 'moduleLabel',
};

export const includeInternal = {
  name: 'includeInternal',
  nodetype: DATATYPE_LITERAL,
  datatype: XSD_BOOLEAN,
  property: `${CONFIG_NAMESPACE}:includeInternal`,
  labelNlsKey: 'includeInternalLabel',
};

export const inlineCreation = {
  name: 'inlineCreation',
  nodetype: DATATYPE_LITERAL,
  datatype: XSD_BOOLEAN,
  property: `${CONFIG_NAMESPACE}:inlineCreation`,
  labelNlsKey: 'inlineCreationLabel',
};

export const includeFile = {
  name: 'includeFile',
  nodetype: DATATYPE_LITERAL,
  datatype: XSD_BOOLEAN,
  property: `${CONFIG_NAMESPACE}:includeFile`,
  labelNlsKey: 'includeFileLabel',
};

export const includeLink = {
  name: 'includeLink',
  nodetype: DATATYPE_LITERAL,
  datatype: XSD_BOOLEAN,
  property: `${CONFIG_NAMESPACE}:includeLink`,
  labelNlsKey: 'includeLinkLabel',
};

export const createOptions = {
  nodetype: GROUP,
  property: 'createOptions',
  direction: 'row',
  labelNlsKey: 'createOptionsLabel',
  max: 1,
};

// faClass is renamed to iconId
export const faClass = {
  name: 'faClass',
  nodetype: LITERAL,
  property: `${CONFIG_NAMESPACE}:iconId`,
  max: 1,
};

export const iconId = {
  name: 'iconId',
  nodetype: LITERAL,
  property: `${CONFIG_NAMESPACE}:iconId`,
  labelNlsKey: 'iconIdLabel',
  max: 1,
};

export const useWith = {
  name: 'useWidth',
  nodetype: URI,
  property: `${CONFIG_NAMESPACE}:useWith`,
  toGraph: addURIFromName,
  labelNlsKey: 'useWithLabel',
  max: 1,
};

export const refines = {
  name: 'refines',
  nodetype: URI,
  property: `${CONFIG_NAMESPACE}:refines`,
  toGraph: addURIFromName,
  labelNlsKey: 'refinesLabel',
  max: 1,
};

export const templateLevel = {
  name: 'templateLevel',
  nodetype: LITERAL,
  property: `${CONFIG_NAMESPACE}:templateLevel`,
  labelNlsKey: 'templateLevelLabel',
  choices: [
    {
      value: 'mandatory',
      label: {
        en: 'Mandatory',
      },
    },
    {
      value: 'recommended',
      label: {
        en: 'Recommended',
      },
    },
    {
      value: 'optional',
      label: {
        en: 'Optional',
      },
    },
  ],
  row: true,
  max: 1,
};

export const publishControl = {
  name: 'publishControl',
  nodetype: DATATYPE_LITERAL,
  datatype: XSD_BOOLEAN,
  property: `${CONFIG_NAMESPACE}:publishControl`,
  labelNlsKey: 'publishControlLabel',
};

export const searchProps = {
  name: 'searchProps',
  nodetype: URI,
  property: `${CONFIG_NAMESPACE}:searchProps`,
  labelNlsKey: 'searchPropsLabel',
};

export const allContexts = {
  name: 'allContexts',
  nodetype: DATATYPE_LITERAL,
  datatype: XSD_BOOLEAN,
  property: `${CONFIG_NAMESPACE}:allContexts`,
  labelNlsKey: 'allContextsLabel',
  max: 1,
};

export const context = {
  name: 'context',
  nodetype: URI,
  property: `${CONFIG_NAMESPACE}:context`,
  labelNlsKey: 'contextLabel',
};

export const uriPattern = {
  name: 'uriPattern',
  nodetype: LITERAL,
  property: `${CONFIG_NAMESPACE}:uriPattern`,
  labelNlsKey: 'uriPatternLabel',
  max: 1,
};

export const restrictTo = {
  name: 'restrictTo',
  nodetype: URI,
  property: `${CONFIG_NAMESPACE}:restrictTo`,
  labelNlsKey: 'restrictToLabel',
  max: 1,
};

export const global = {
  name: 'global',
  nodetype: DATATYPE_LITERAL,
  datatype: XSD_BOOLEAN,
  property: `${CONFIG_NAMESPACE}:global`,
  labelNlsKey: 'globalLabel',
};

export const main = {
  name: 'main',
  nodetype: DATATYPE_LITERAL,
  datatype: XSD_BOOLEAN,
  property: `${CONFIG_NAMESPACE}:main`,
  labelNlsKey: 'mainLabel',
  max: 1,
};

export const storeConfig = {
  name: 'storeConfig',
  nodetype: BLANK,
  property: `${CONFIG_NAMESPACE}:storeConfig`,
  labelNlsKey: 'storeConfigLabel',
  toGraph: addStoreConfig,
  max: 1,
};
