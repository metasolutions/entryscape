import PropTypes from 'prop-types';
import {
  constructURIFromPattern,
  getValueFromURIByPattern,
  isResourceURIUniqueInContext,
} from 'commons/util/store';
import { useState, useEffect, useRef } from 'react';
import esteConceptNLS from 'terms/nls/esteConcept.nls';
import { useTranslation } from 'commons/hooks/useTranslation';
import {
  Edit as EditIcon,
  Save as SaveIcon,
  Close as CloseIcon,
} from '@mui/icons-material';
import { TextField, IconButton, InputAdornment } from '@mui/material';
import { useESContext } from 'commons/hooks/useESContext';
import skosUtil from 'commons/tree/skos/util';
import { useEntityURI } from 'commons/hooks/useEntityTitleAndURI';
import Tooltip from 'commons/components/common/Tooltip';
import useAsync from 'commons/hooks/useAsync';
import { ErrorCatcher } from 'commons/errors/ErrorCatcher';
import { ErrorWithMessage } from 'commons/errors/utils/errors';
import { entryPropType } from 'commons/util/entry';
import './index.scss';

const ConceptURI = ({ entry, uriPattern, onUpdateConceptRURI }) => {
  const translate = useTranslation(esteConceptNLS);
  const { context, contextName } = useESContext();
  const { name, setName, isNameFree, handleNameChange } = useEntityURI({
    context,
    contextName,
    uriPattern,
  });
  const [isEditMode, setIsEditMode] = useState(false);
  const { runAsync, error: updateError } = useAsync();

  const conceptResourceURI = entry.getResourceURI();
  const initialNameValue = useRef(
    getValueFromURIByPattern(conceptResourceURI, uriPattern, 'entryName')
  );

  useEffect(() => {
    setName(
      getValueFromURIByPattern(entry.getResourceURI(), uriPattern, 'entryName')
    );
    setIsEditMode(false);
  }, [entry, setName, uriPattern]);

  /**
   * toggle between edit and view mode
   */
  const changeMode = () => {
    setIsEditMode(!isEditMode);
  };

  // replace resource uri and update tree/tremodel via onUpdateConceptRURI
  const updateResourceURI = async (newRURI) => {
    if (isNameFree && name !== '') {
      const oldRURI = entry.getResourceURI();
      await skosUtil.updateConceptResourceURI(entry, newRURI);
      await onUpdateConceptRURI(oldRURI, newRURI);
      setIsEditMode(false);
    } else {
      setIsEditMode(true);
    }
  };

  // check if the new ruri is available in context
  const isConceptRURIUnique = (value) => {
    const resourceURI = constructURIFromPattern(uriPattern, {
      contextName,
      entryName: value,
    });
    return isResourceURIUniqueInContext(resourceURI, context);
  };

  const onSubmitName = async () => {
    const newRURI = constructURIFromPattern(uriPattern, {
      contextName,
      entryName: name,
    });

    runAsync(
      isConceptRURIUnique(name)
        .then(isNameFree)
        .then(() => updateResourceURI(newRURI))
        .catch((error) => {
          throw new ErrorWithMessage(translate('updateConceptUriFail'), error);
        })
    );
  };

  const isInvalid =
    !isNameFree || (name === '' && name !== initialNameValue.current);

  return initialNameValue.current ? (
    <>
      <div className="esteConceptURI">
        {isEditMode ? (
          <TextField
            label={translate('URLInputLabel')}
            id="concept-edit-name"
            value={name}
            onChange={handleNameChange}
            error={!isNameFree}
            helperText={!isNameFree && translate('unavailableURLFeedback')}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  {constructURIFromPattern(uriPattern, {
                    contextName,
                  })}
                </InputAdornment>
              ),
              endAdornment: (
                <InputAdornment position="end">
                  <Tooltip title={translate('cancelEditURLButtonTitle')}>
                    <IconButton
                      aria-label={translate('cancelEditURLButtonTitle')}
                      onClick={changeMode}
                    >
                      <CloseIcon />
                    </IconButton>
                  </Tooltip>
                  <Tooltip title={translate('updateURLButtonTitle')}>
                    <IconButton
                      aria-label={translate('updateURLButtonTitle')}
                      onClick={onSubmitName}
                      disabled={isInvalid}
                    >
                      <SaveIcon />
                    </IconButton>
                  </Tooltip>
                </InputAdornment>
              ),
            }}
          />
        ) : (
          <TextField
            disabled
            label={translate('URLInputLabel')}
            id="create-terminology-name"
            value={conceptResourceURI}
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <Tooltip title={translate('editURLButtonTitle')}>
                    <IconButton
                      aria-label={translate('editURLButtonTitle')}
                      onClick={changeMode}
                    >
                      <EditIcon />
                    </IconButton>
                  </Tooltip>
                </InputAdornment>
              ),
            }}
          />
        )}
      </div>
      <ErrorCatcher error={updateError} />
    </>
  ) : null;
};

ConceptURI.propTypes = {
  entry: entryPropType,
  uriPattern: PropTypes.string,
  onUpdateConceptRURI: PropTypes.func,
};

export default ConceptURI;
