import { get, fromLonLat, toLonLat, transformExtent } from 'ol/proj';
import proj4 from 'proj4';
import { register } from 'ol/proj/proj4';
import { getTopLeft, getBottomRight } from 'ol/extent';
import { CRS_WGS84, EPSG_WEB_MERCATOR, ESPG_WGS84 } from './projections';

/**
 * @param {string} projectionCode
 * @returns {Projection}
 */
export const getProjection = (projectionCode) => {
  return get(projectionCode);
};

/**
 * @param {Coordinate[]} coordinates
 * @param {string} projectionCode
 * @returns {Coordinate[]}
 */
export const convertFromLonLat = (
  coordinates,
  projectionCode = EPSG_WEB_MERCATOR
) => {
  return coordinates.map((coordinate) =>
    fromLonLat(coordinate, projectionCode)
  );
};

/**
 * @param {Coordinate[]} coordinates
 * @param {string} projectionCode
 * @returns {Coordinate[]}
 */
export const convertToLonLat = (
  coordinates,
  projectionCode = EPSG_WEB_MERCATOR
) => {
  return coordinates.map((coordinate) => toLonLat(coordinate, projectionCode));
};

/**
 * Convert extent coordinates to lon lat
 * @param {number[]} extent
 * @param {string} projectionCode
 * @returns {number[]}
 */
export const extentToLonLat = (extent, projectionCode = EPSG_WEB_MERCATOR) => {
  const [west, north] = toLonLat(getTopLeft(extent), projectionCode);
  const [east, south] = toLonLat(getBottomRight(extent), projectionCode);
  return [west, north, east, south];
};

/**
 * Convert extent coordinates from lon lat
 * @param {number[]} extent
 * @param {string} projectionCode
 * @returns {number[]}
 */
export const extentFromLonLat = (
  [west, north, east, south],
  projectionCode
) => {
  const coordinates = convertFromLonLat(
    [
      [west, north],
      [east, south],
    ],
    projectionCode
  );
  return coordinates.flat();
};

/**
 * @param {number[]} extent
 * @param {string} sourceProjectionCode
 * @param {string} targetProjectionCode
 * @returns
 */
const transformAndValidateExtent = (
  extent,
  sourceProjectionCode,
  targetProjectionCode
) => {
  const transformedExtent = transformExtent(
    extent,
    sourceProjectionCode,
    targetProjectionCode
  );
  if (transformedExtent.every((value) => Number.isFinite(value)))
    return transformedExtent;
};

/**
 * Transforms extent to default projection
 * @param {number[]} extent
 * @param {string} projectionCode
 * @returns
 */
export const extentToProjection = (
  extent,
  projectionCode,
  targetProjectionCode = EPSG_WEB_MERCATOR
) => {
  if (projectionCode === targetProjectionCode);
  const sourceProjectionCode =
    projectionCode === CRS_WGS84 ? ESPG_WGS84 : projectionCode;

  const transformedExtent = transformAndValidateExtent(
    extent,
    sourceProjectionCode,
    targetProjectionCode
  );
  if (transformedExtent) return transformedExtent;

  const projectionExtent = getProjection(projectionCode).getExtent();
  return transformExtent(
    projectionExtent,
    sourceProjectionCode,
    targetProjectionCode
  );
};

/**
 * Register projection in OpenLayers
 * @param {object} projectionDefinition
 * @returns
 */
export const registerProjection = ({ code, definition, boundingBox }) => {
  if (getProjection(code)) return;
  proj4.defs(code, definition);
  register(proj4);
  const projection = getProjection(code);
  projection.setWorldExtent(boundingBox);
  projection.setExtent(transformExtent(boundingBox, ESPG_WGS84, code));
};

/**
 * @param {Object[]} projectionDefinitions
 * @returns
 */
export const registerProjections = (projectionDefinitions) => {
  if (!projectionDefinitions) return;
  projectionDefinitions.forEach((projectionDefinition) => {
    registerProjection(projectionDefinition);
  });
};
