import PropTypes from 'prop-types';
import { FormControlLabel, InputLabel } from '@mui/material';
import Switch from 'commons/components/common/Switch';
import escoRdformsNLS from 'commons/nls/escoRdforms.nls';
import { useTranslation } from 'commons/hooks/useTranslation';
import Tooltip from 'commons/components/common/Tooltip';
import ConditionalWrapper from 'commons/components/common/ConditionalWrapper';
import './LevelSelector.scss';

const LEVEL_MANDATORY = 'mandatory';
const LEVEL_RECOMMENDED = 'recommended';
const LEVEL_OPTIONAL = 'optional';

const LevelSelector = ({
  level = LEVEL_MANDATORY,
  onUpdateLevel,
  disabledLevels,
}) => {
  const t = useTranslation(escoRdformsNLS);

  const handleChange = (event) => {
    const toggled = event.target.name;

    if (level === toggled) {
      onUpdateLevel(
        toggled === LEVEL_RECOMMENDED ? LEVEL_MANDATORY : LEVEL_RECOMMENDED
      );
    } else if (toggled === LEVEL_OPTIONAL) {
      onUpdateLevel(LEVEL_OPTIONAL);
    } else if (level === LEVEL_MANDATORY) {
      onUpdateLevel(LEVEL_RECOMMENDED);
    } else {
      onUpdateLevel(LEVEL_MANDATORY);
    }
  };

  const recommendedChecked =
    level === LEVEL_RECOMMENDED || level === LEVEL_OPTIONAL;
  const recommendedDisabled = disabledLevels.includes(LEVEL_RECOMMENDED);
  const optionalChecked = level === LEVEL_OPTIONAL;
  const optionalDisabled = disabledLevels.includes(LEVEL_OPTIONAL);

  return (
    <div className="LevelSelector">
      <div className="LevelSelector__level">
        <InputLabel className="LevelSelector__label">
          {t('mandatoryLabel')}
        </InputLabel>
      </div>
      <div
        className={`LevelSelector__level ${
          !recommendedChecked ? 'LevelSelector__level--unchecked' : ''
        }`}
      >
        <ConditionalWrapper
          condition={recommendedDisabled}
          wrapper={(children) => (
            <Tooltip title={t('disabledRecommendedTooltip')}>
              <span>{children}</span>
            </Tooltip>
          )}
        >
          <FormControlLabel
            control={
              <Switch
                checked={recommendedChecked}
                onChange={handleChange}
                name={LEVEL_RECOMMENDED}
                color="primary"
                disabled={recommendedDisabled}
              />
            }
            label={t('recommendedLabel')}
            classes={{ label: 'LevelSelector__label' }}
          />
        </ConditionalWrapper>
      </div>
      <div
        className={`LevelSelector__level ${
          !optionalChecked ? 'LevelSelector__level--unchecked' : ''
        }`}
      >
        <ConditionalWrapper
          condition={optionalDisabled}
          wrapper={(children) => (
            <Tooltip title={t('disabledOptionalTooltip')}>
              <span>{children}</span>
            </Tooltip>
          )}
        >
          <FormControlLabel
            control={
              <Switch
                checked={optionalChecked}
                onChange={handleChange}
                name={LEVEL_OPTIONAL}
                color="primary"
                disabled={optionalDisabled}
              />
            }
            label={t('optionalLabel')}
            classes={{ label: 'LevelSelector__label' }}
          />
        </ConditionalWrapper>
      </div>
    </div>
  );
};

LevelSelector.propTypes = {
  level: PropTypes.oneOf([LEVEL_MANDATORY, LEVEL_RECOMMENDED, LEVEL_OPTIONAL]),
  onUpdateLevel: PropTypes.func.isRequired,
  disabledLevels: PropTypes.arrayOf(
    PropTypes.oneOf([LEVEL_RECOMMENDED, LEVEL_OPTIONAL])
  ),
};

export { LEVEL_MANDATORY, LEVEL_RECOMMENDED, LEVEL_OPTIONAL };
export default LevelSelector;
