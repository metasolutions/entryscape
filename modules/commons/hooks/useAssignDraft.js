import useGetMainDialog from 'commons/hooks/useGetMainDialog';
import { useTranslation } from 'commons/hooks/useTranslation';
import escoListNLS from 'commons/nls/escoList.nls';
import { getDraftsEnabled } from 'commons/types/utils/entityType';
import { setEntryDraftStatus } from 'commons/util/entry';
import { useCallback, useRef } from 'react';
import config from 'config';
import { namespaces as ns } from '@entryscape/rdfjson';

const isTitleError = (properties, error) =>
  properties.includes(error.item.getSource().property);

const hasOnlyMissingRequiredValues = (properties, errors) => {
  // when using validation errors from validate.bindingReport
  if (typeof errors[0] === 'object' && errors[0].code)
    return errors.every(
      (error) => error.code === 'min' && !isTitleError(properties, error)
    );
  return false;
};

const useAssignDraft = (isPrototype = false) => {
  const translate = useTranslation([escoListNLS]);
  const { getConfirmationDialog } = useGetMainDialog();
  const saveAsDraft = useRef(false);
  const shortProperties = config.get('rdf.labelProperties').flat();
  const properties = [...shortProperties, ...shortProperties.map(ns.expand)];

  const checkCanDraft = useCallback(
    async (validationErrors, entityType) => {
      if (validationErrors.length === 0) return true;
      const hasDraftEnabled = entityType ? getDraftsEnabled(entityType) : false;
      if (
        hasOnlyMissingRequiredValues(properties, validationErrors) &&
        hasDraftEnabled
      ) {
        const proceed = await getConfirmationDialog({
          content: translate('confirmDraftSave'),
          rejectLabel: translate('no'),
          affirmLabel: translate('yes'),
        });
        if (!proceed) return false;
        saveAsDraft.current = true;
        return true;
      }
      return false;
    },
    [translate, getConfirmationDialog]
  );

  const applyDraftChanges = useCallback(
    async (updatedEntry) => {
      if (saveAsDraft.current)
        return setEntryDraftStatus(
          updatedEntry,
          saveAsDraft.current,
          isPrototype
        );
      return updatedEntry;
    },
    [saveAsDraft, isPrototype]
  );

  return { checkCanDraft, applyDraftChanges };
};

export default useAssignDraft;
