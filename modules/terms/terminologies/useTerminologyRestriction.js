import config from 'config';
import { useUserState } from 'commons/hooks/useUser';
import { hasUserPermission, ADMIN_RIGHTS, PREMIUM } from 'commons/util/user';

const useTerminologyRestriction = (numberOfTerminologies) => {
  const { userEntry } = useUserState();
  const isExemptFromRestrictions = hasUserPermission(userEntry, [
    PREMIUM,
    ADMIN_RIGHTS,
  ]);
  if (isExemptFromRestrictions) return { isRestricted: false };

  const terminologiesLimit = config.get('terms.schemeLimit');
  const pathToSchemeLimitContent = config.get('terms.schemeLimitDialog');
  const isRestricted =
    (terminologiesLimit && numberOfTerminologies >= terminologiesLimit) ||
    terminologiesLimit === 0;

  return { isRestricted, contentPath: pathToSchemeLimitContent };
};

export default useTerminologyRestriction;
