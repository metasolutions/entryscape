import React from 'react';
import PropTypes from 'prop-types';
import {
  Autocomplete,
  Checkbox,
  InputLabel,
  MenuItem,
  Select,
  FormControl,
  FormControlLabel,
  TextField,
} from '@mui/material';
import { useUniqueId } from 'commons/hooks/useUniqueId';
import './SearchFilter.scss';
import Filter from '../utils/filter';
import { ANY, AUTOCOMPLETE, CHECKBOX } from '../utils/filterDefinitions';

const renderAutocompleteOption = (props, option) => {
  const { label, value } = option;
  return (
    <li {...props} key={label + value}>
      {label}
    </li>
  );
};

const SearchFilter = ({ onSelect, filter, disabled = false, translate }) => {
  const header = translate(filter.getHeaderNlsKey());
  const prefix = `${header.toLowerCase().replace(' ', '-')}-`;
  const labelId = useUniqueId(prefix);
  const values = filter.getItems().map(({ labelNlsKey, label, ...props }) => ({
    ...props,
    ...(label || labelNlsKey ? { label: label || translate(labelNlsKey) } : {}),
  }));

  const selected = filter.getSelected();

  const handleChange = (changeEvent) => {
    const {
      target: { value },
    } = changeEvent;
    onSelect(value);
  };

  const handleCheckboxChange = ({ target: { checked, value } }) =>
    onSelect(checked ? value : ANY);

  const handleAutocompleteChange = (event, { value }) => onSelect(value);

  const getOptionLabel = (option) => {
    if (typeof option === 'object' && option !== null) return option.label;
    const matchingItem = filter.findItem(option) || filter.getDefaultItem();
    return matchingItem.label ?? translate(matchingItem.labelNlsKey);
  };

  if (filter.getType() === AUTOCOMPLETE)
    return (
      <Autocomplete
        id={labelId}
        options={values}
        groupBy={(option) => option.type}
        value={selected}
        defaultValue={ANY}
        disablePortal
        disableClearable
        disabled={disabled}
        getOptionLabel={getOptionLabel}
        isOptionEqualToValue={(option, value) => option.value === value}
        onChange={handleAutocompleteChange}
        renderOption={renderAutocompleteOption}
        renderInput={(params) => (
          <TextField
            {...params}
            label={header}
            className="escoSearchFilter__formControl"
          />
        )}
      />
    );

  if (filter.getType() === CHECKBOX) {
    const { value } = filter.getItems().find((item) => item.value !== ANY);
    return (
      <FormControlLabel
        className="escoSearchFilter__formControl"
        control={
          <Checkbox
            checked={selected !== ANY}
            onChange={handleCheckboxChange}
            disabled={disabled}
            value={value}
          />
        }
        label={header}
      />
    );
  }

  return (
    <FormControl
      variant="filled"
      className="escoSearchFilter__formControl"
      disabled={disabled}
    >
      <InputLabel id={labelId}>{header}</InputLabel>
      <Select
        className="escoSearchFilter"
        value={selected}
        onChange={handleChange}
        inputProps={{ 'aria-labelledby': labelId }}
      >
        {values.map(({ value, label }) => (
          <MenuItem key={`${value}-${label}`} value={value}>
            {label}
          </MenuItem>
        ))}
      </Select>
    </FormControl>
  );
};

SearchFilter.propTypes = {
  onSelect: PropTypes.func.isRequired,
  filter: PropTypes.instanceOf(Filter),
  disabled: PropTypes.bool,
  translate: PropTypes.func,
};

export default SearchFilter;
