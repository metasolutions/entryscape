import { useState, useMemo } from 'react';
import PropTypes from 'prop-types';
import { Grid, IconButton, Button, Divider as MuiDivider } from '@mui/material';
import {
  Edit as EditIcon,
  Delete as DeleteIcon,
  Add,
  Add as AddIcon,
} from '@mui/icons-material';
import AddIconButton from 'commons/components/IconButton';
import Tooltip from 'commons/components/common/Tooltip';
import { useTranslation } from 'commons/hooks/useTranslation';
import esmoFieldsNLS from 'models/nls/esmoFields.nls';
import esmoCommonsNLS from 'models/nls/esmoCommons.nls';
import escoDialogsNLS from 'commons/nls/escoDialogs.nls';
import escoFormsNLS from 'commons/nls/escoForms.nls';
import {
  DraggableList,
  DraggableListItem,
} from 'commons/components/DraggableList';
import {
  EditorProvider,
  useEditorContext,
} from 'commons/components/forms/editors/EditorContext';
import {
  FIELD_LABEL,
  FIELD_DESCRIPTION,
  FIELD_VALUE,
  FIELD_EDIT_LABEL,
  FIELD_EDIT_DESCRIPTION,
} from 'models/utils/fieldDefinitions';
import { RDF_PROPERTY_LABEL } from 'models/utils/ns';
import { FieldPresenter, Value } from 'models/presenters';
import { getLocalizedValue } from 'commons/util/rdfUtils';
import { getNodetype } from 'models/fields/utils/fields';
import { getEditorComponent } from 'commons/components/forms/editors/utils/editor';
import { URI, LANGUAGE_LITERAL } from 'commons/components/forms/editors';
import {
  ActionsMenu,
  ActionsProvider,
  OpenActionsMenuButton,
} from 'commons/components/ListView';
import { getIconFromActionId } from 'commons/actions';
import { SELECT_OPTIONS_VALUE_CHOICES } from 'models/utils/choiceDefinitions';
import { URIValueEditor } from 'models/editors';
import { getPresenter } from 'commons/components/forms/presenters/utils/presenter';
import Placeholder from 'commons/components/common/Placeholder';
import AddOptionDialog from '../AddOptionDialog';
import EditOptionDialog from '../EditOptionDialog';
import {
  getOptionIdsInOrder,
  getOptionLabel,
  reorderOptions,
  removeOption,
  getDefaultOptionId,
} from '../../../utils/options';

const NLS_BUNDLES = [
  esmoFieldsNLS,
  esmoCommonsNLS,
  escoFormsNLS,
  escoDialogsNLS,
];

const NoOptionsPlaceholder = ({ label, translate, onAdd }) => {
  return (
    <Placeholder variant="view" label={label}>
      <Grid item className="escoListViewPlaceholder__children">
        <Button startIcon={<AddIcon />} onClick={onAdd}>
          {translate('addLabel')}
        </Button>
      </Grid>
    </Placeholder>
  );
};

NoOptionsPlaceholder.propTypes = {
  label: PropTypes.string,
  onAdd: PropTypes.func,
  translate: PropTypes.func,
};

const OptionsManager = ({ onChange }) => {
  const {
    graph,
    resourceURI,
    setGraph,
    entry: fieldEntry,
  } = useEditorContext();
  const [orderedOptionIds, setOrderedOptionIds] = useState(() =>
    getOptionIdsInOrder(graph, resourceURI)
  );
  const [selectedOptionId, setSelectedOptionId] = useState(orderedOptionIds[0]);
  const [defaultOptionId, setDefaultOptionId] = useState(
    getDefaultOptionId(graph, resourceURI)
  );
  const [showAddOptionDialog, setShowAddOptionDialog] = useState(false);
  const [showEditOptionDialog, setShowEditOptionDialog] = useState(false);
  const translate = useTranslation(NLS_BUNDLES);

  const getValueEditor = (nodetype) => {
    if (nodetype === URI) return URIValueEditor;
    return getEditorComponent(nodetype, null);
  };
  const getValuePresenter = (nodetype) => {
    if (nodetype === URI) return Value;
    return getPresenter({ nodetype });
  };

  const nodetype = getNodetype(fieldEntry);
  const editor = getValueEditor(nodetype, null);
  const presenter = getValuePresenter(nodetype);

  const handleRemoveOption = () => {
    removeOption(graph, resourceURI, selectedOptionId);

    const reorderedOptionIds = orderedOptionIds.filter(
      (optionId) => optionId !== selectedOptionId
    );
    setOrderedOptionIds(reorderedOptionIds);
    const newSelectedOptiondId = reorderedOptionIds.length
      ? reorderedOptionIds[reorderedOptionIds.length - 1]
      : null;
    setSelectedOptionId(newSelectedOptiondId);

    reorderOptions(graph, reorderedOptionIds);
    onChange(true);
  };

  const handleAddOption = (newGraph) => {
    setGraph(newGraph);
    const reorderedOptionIds = getOptionIdsInOrder(graph, resourceURI);
    setOrderedOptionIds(reorderedOptionIds);
    setSelectedOptionId(reorderedOptionIds[reorderedOptionIds.length - 1]);
    onChange(true);
  };

  const handleEditOption = (newGraph) => {
    setGraph(newGraph);
    onChange(true);
  };

  const onDragEnd = (reorder, source, destination) => {
    if (!destination) return; // dropped outside the list
    if (source.index !== destination.index) {
      const reorderedOptionIds = reorder(
        orderedOptionIds,
        source.index,
        destination.index
      );
      reorderOptions(graph, reorderedOptionIds);
      setOrderedOptionIds(reorderedOptionIds);
      onChange(true);
    }
  };

  const fields = useMemo(
    () => [
      {
        ...FIELD_VALUE,
        nodetype,
        max: nodetype !== LANGUAGE_LITERAL ? 1 : null,
        mandatory: true,
        nlsBundles: [esmoFieldsNLS, esmoCommonsNLS],
        editChoices: SELECT_OPTIONS_VALUE_CHOICES,
        Editor: editor,
        Presenter: presenter,
      },
      { ...FIELD_LABEL, mandatory: true },
      { ...FIELD_DESCRIPTION },
      { ...FIELD_EDIT_LABEL },
      { ...FIELD_EDIT_DESCRIPTION },
    ],
    [nodetype, editor, presenter]
  );

  const unsetDefaultAction = defaultOptionId === selectedOptionId;

  const removeCurrentDefaultOption = () => {
    graph.findAndRemove(defaultOptionId, 'esterms:default');
  };

  const handleSetDefaultOption = async () => {
    if (defaultOptionId) {
      removeCurrentDefaultOption();
    }
    if (unsetDefaultAction) {
      setDefaultOptionId(null);
      return;
    }
    setDefaultOptionId(selectedOptionId);
    graph.addD(selectedOptionId, 'esterms:default', 'true', 'xsd:boolean');
  };

  const optionsActions = [
    {
      label: translate('remove'),
      icon: <DeleteIcon />,
      onClick: handleRemoveOption,
      isVisible: true,
    },
    {
      label: translate('setDefaultLabel'),
      icon: getIconFromActionId('setDefault'),
      onClick: handleSetDefaultOption,
      isVisible: selectedOptionId !== defaultOptionId,
    },
    {
      label: translate('unsetDefaultLabel'),
      icon: getIconFromActionId('unsetDefault'),
      onClick: handleSetDefaultOption,
      isVisible: selectedOptionId === defaultOptionId,
    },
  ];

  const actionsMenuItems = optionsActions.filter(({ isVisible }) => isVisible);

  return (
    <>
      <div className="esmoTwoColView">
        <div className="escoTwoColView__Sidebar esmoList__sidebar">
          <Grid className="esmoList__sidebarHead">
            <div className="esmoSidebar__sidebarHeader">
              <div className="esmoSidebar__formDetails">
                <h2>{translate('optionsListHeader')}</h2>
              </div>
              <Tooltip title={translate('addOptionTooltip')}>
                <AddIconButton
                  onClick={() => setShowAddOptionDialog(true)}
                  icon={<Add />}
                  className="esmoSidebar__addButton"
                />
              </Tooltip>
            </div>
          </Grid>
          <MuiDivider />
          <div className="esmoList__sidebarBody">
            <DraggableList onDragEnd={onDragEnd}>
              {orderedOptionIds.map((optionId, index) => (
                <DraggableListItem
                  id={optionId}
                  index={index}
                  selected={optionId === selectedOptionId}
                  onClick={() => setSelectedOptionId(optionId)}
                  key={optionId}
                  primaryText={getOptionLabel(graph, optionId)}
                  icon={
                    optionId === defaultOptionId
                      ? getIconFromActionId('setDefault')
                      : null
                  }
                  iconTooltipLabel={translate('defaultTooltip')}
                />
              ))}
            </DraggableList>
          </div>
        </div>
        <div className="esmoFormOverview__main">
          {orderedOptionIds.length ? (
            <>
              <div className="esmoFieldOptionsDialogHeader">
                <h2 className="esmoFieldOptionsDialogHeader_heading">
                  {getLocalizedValue(graph, selectedOptionId, [
                    RDF_PROPERTY_LABEL,
                  ])}
                </h2>
                <div className="esmoFieldOptionsHeader_actions">
                  <Grid item>
                    <Tooltip title={translate('editOptionHeader')}>
                      <IconButton
                        aria-label={translate('editOptionHeader')}
                        onClick={() => setShowEditOptionDialog(true)}
                      >
                        <EditIcon />
                      </IconButton>
                    </Tooltip>
                    <ActionsProvider>
                      <OpenActionsMenuButton />
                      <ActionsMenu items={actionsMenuItems} />
                    </ActionsProvider>
                  </Grid>
                </div>
              </div>
              {fields.map((field) => (
                <FieldPresenter
                  key={field.property}
                  graph={graph}
                  resourceURI={selectedOptionId}
                  {...field}
                />
              ))}
            </>
          ) : (
            <NoOptionsPlaceholder
              label={translate('optionPlaceholder')}
              translate={translate}
              onAdd={() => setShowAddOptionDialog(true)}
            />
          )}
        </div>
      </div>
      {showAddOptionDialog && (
        <AddOptionDialog
          closeDialog={() => setShowAddOptionDialog(false)}
          onAddOption={handleAddOption}
          graph={graph}
          resourceURI={resourceURI}
          order={orderedOptionIds.length + 1}
          title={translate('addOptionHeader')}
          fields={fields}
        />
      )}
      {showEditOptionDialog && (
        <EditorProvider graph={graph} resourceURI={selectedOptionId}>
          <EditOptionDialog
            closeDialog={() => setShowEditOptionDialog(false)}
            onAccept={handleEditOption}
            fields={fields}
          />
        </EditorProvider>
      )}
    </>
  );
};

OptionsManager.propTypes = {
  onChange: PropTypes.func,
};

export default OptionsManager;
