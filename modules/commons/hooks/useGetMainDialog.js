import { useCallback } from 'react';
import { useMainDialog } from 'commons/hooks/useMainDialog';
import { useTranslation } from 'commons/hooks/useTranslation';
import {
  AcknowledgeAction,
  ConfirmAction,
} from 'commons/components/common/dialogs/MainDialog';

import escoDialogNLS from 'commons/nls/escoDialogs.nls';

const useGetMainDialog = () => {
  const { closeMainDialog, openMainDialog } = useMainDialog();
  const t = useTranslation(escoDialogNLS);

  const handlePromise = useCallback(
    (resolve, result) => {
      closeMainDialog();
      resolve(result);
    },
    [closeMainDialog]
  );

  const getConfirmationDialog = useCallback(
    ({
      rejectLabel = t('reject'),
      affirmLabel = t('confirm'),
      backdropClickPromiseResolution = false,
      ...options
    }) =>
      new Promise((resolve) => {
        openMainDialog({
          actions: (
            <ConfirmAction
              onDone={() => handlePromise(resolve, true)}
              onReject={() => handlePromise(resolve, false)}
              affirmLabel={affirmLabel}
              rejectLabel={rejectLabel}
            />
          ),
          ...options,
          onBackdropClick: () =>
            handlePromise(resolve, backdropClickPromiseResolution),
        });
      }),
    [handlePromise, openMainDialog, t]
  );

  const getAcknowledgeDialog = useCallback(
    ({ okLabel, ...options }) =>
      new Promise((resolve) => {
        openMainDialog({
          actions: (
            <AcknowledgeAction
              okLabel={okLabel}
              onDone={() => handlePromise(resolve, true)}
            />
          ),
          ...options,
          onBackdropClick: () => handlePromise(resolve, true),
        });
      }),
    [handlePromise, openMainDialog]
  );

  return { getConfirmationDialog, getAcknowledgeDialog };
};

export default useGetMainDialog;
