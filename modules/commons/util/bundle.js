import config from 'config';
import configUtil from './config';
import { isUri } from './util';

/**
 * @return {string}
 * TODO place itemstore.relativeBundlePath in commons/config.js
 */
const getThemeTemplatesPath = () =>
  config.get('itemstore.relativeBundlePath', false)
    ? ''
    : new URL('theme/templates/', configUtil.getBaseUrl()).href;

/**
 * @return {*}
 */
const getBuildTemplatesPath = () =>
  config.get('entryscape.localBuild', false)
    ? configUtil.getBaseUrl()
    : new URL('templates/', configUtil.getStaticBuildPath()).href;

/**
 * @return {string}
 */
const getDevelopmentTemplatesPath = () =>
  new URL('templates/', configUtil.getBaseUrl()).href;

/**
 * A fallback utility to fetch rdforms templates.
 * NOTE! order matters here
 *
 * @param {string} id
 * @param {string} format
 * @param {boolean} includeThemePath Used to avoid getting the theme path template for default bundles
 * @returns {string[]}
 */
export const getFallbackUrls = (id, format, includeThemePath) =>
  [
    ...(includeThemePath ? [getThemeTemplatesPath] : []),
    DEVELOPMENT ? getDevelopmentTemplatesPath : getBuildTemplatesPath,
  ].map((baseUrlFunc) => {
    const baseURL = baseUrlFunc();
    if (baseURL === '' && config.get('itemstore.relativeBundlePath', false)) {
      return `${id}.${format}`;
    }
    return new URL(`${id}.${format}`, baseURL).href;
  });

/**
 * Get an array of fallback urls where a specific bundle may be found.
 * Can also be passed a full url which would ignore the fallback mechanism.
 *
 * @param {string} id The id of the bundle or a full url
 * @param {string} [format='json']
 * @param {boolean} [includeThemePath=true]
 * @return {Array<string>}
 */
export const getFallbackBundleUrls = (
  id,
  format = 'json',
  includeThemePath = true
) => (isUri(id) ? [id] : getFallbackUrls(id, format, includeThemePath));
