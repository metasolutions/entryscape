import { Graph, namespaces } from '@entryscape/rdfjson';
import { Entry } from '@entryscape/entrystore-js';
import { LABEL_TYPE_TITLE, RDF_PROPERTY_LABEL_TYPE } from './definitions';

/**
 * Extract json for the given element from the metadata.
 *
 * @param rgraph
 * @param element
 * @returns {object|null}
 */
const getElementJSON = (rgraph, element) => {
  // class, classEntry, formEntry
  const cls = element.prop('class');
  const classEntry = element.prop('classEntry');
  const formEntry = element.prop('formEntry');
  let blankStmts;
  if (formEntry) {
    blankStmts = rgraph.find(null, 'esterms:ofFormEntry', formEntry.getURI());
  } else if (classEntry) {
    blankStmts = rgraph.find(null, 'esterms:ofClassEntry', classEntry.getURI());
  } else if (cls) {
    blankStmts = rgraph.find(null, 'esterms:ofClass', cls);
  }

  if (blankStmts.length > 0) {
    const blank = blankStmts[0].getSubject();
    const str = rgraph.findFirstValue(blank, 'esterms:graphics');
    if (str) {
      try {
        return JSON.parse(str);
        // eslint-disable-next-line no-empty
      } catch (e) {}
    }
  }
  return null;
};

/**
 * Extract json for the given link from the metadata.
 *
 * @param rgraph
 * @param link
 * @returns {object|null}
 */
const getLinkJSON = (rgraph, link) => {
  // property, subject, object, propertyEntry, fieldEntry

  const fieldEntry = link.prop('itemEntry');
  const propertyEntry = link.prop('propertyEntry');
  const subject = link.prop('subject');
  const predicate = link.prop('property');
  const object = link.prop('object');

  let blankStatements;
  if (fieldEntry) {
    blankStatements = rgraph.find(
      null,
      'esterms:ofFieldEntry',
      fieldEntry.getURI()
    );
  } else if (propertyEntry) {
    blankStatements = rgraph.find(
      null,
      'esterms:ofPropertyEntry',
      propertyEntry.getURI()
    );
  } else if (subject && predicate && object) {
    blankStatements = rgraph
      .find(null, 'esterms:ofSubject', subject)
      .filter((stmt) => {
        const blank = stmt.getSubject();
        return (
          rgraph.find(blank, 'esterms:ofPredicate', predicate).length > 0 &&
          rgraph.find(blank, 'esterms:ofObject', object).length > 0
        );
      });
  }
  if (blankStatements.length > 0) {
    const blank = blankStatements[0].getSubject();
    const str = rgraph.findFirstValue(blank, 'esterms:graphics');
    if (str) {
      try {
        return JSON.parse(str);
        // eslint-disable-next-line no-empty
      } catch (e) {}
    }
  }
  return null;
};

/**
 * Layouts the given graph according to the diagramEntry.
 *
 * @param jGraph jointJS graph
 * @param rdfGraph rdf graph
 * @returns {object}
 */
export const importLayout = (jGraph, rdfGraph) => {
  const report = {
    missingLinks: [],
    missingElements: [],
  };
  jGraph.getElements().forEach((element) => {
    const jsonObject = getElementJSON(rdfGraph, element);
    if (jsonObject) {
      element.prop('position', jsonObject.position);
    } else {
      report.missingElements.push(element);
    }
  });

  jGraph.getLinks().forEach((link) => {
    const jsonObject = getLinkJSON(rdfGraph, link);
    if (jsonObject) {
      if (jsonObject.vertices) {
        link.prop('vertices', jsonObject.vertices);
      }
    } else {
      report.missingLinks.push(link);
    }
  });
  return report;
};

/**
 * Save current layout of graph in metadata
 *
 * @param {object} jGraph
 * @param {Entry} diagramEntry
 */
export const exportLayout = (jGraph) => {
  const rdfGraph = new Graph();
  const elements = jGraph.getElements();
  const links = jGraph.getLinks();
  elements.forEach((element) => {
    const blank = rdfGraph
      .add(null, 'rdf:type', 'esterms:ResourceLayout')
      .getSubject();
    if (element.prop('class')) {
      rdfGraph.add(blank, 'esterms:ofClass', element.prop('class'));
    }
    if (element.prop('classEntry')) {
      rdfGraph.add(
        blank,
        'esterms:ofClassEntry',
        element.prop('classEntry').getURI()
      );
    }
    if (element.prop('formEntry')) {
      rdfGraph.add(
        blank,
        'esterms:ofFormEntry',
        element.prop('formEntry').getURI()
      );
    }
    rdfGraph.addL(
      blank,
      'esterms:graphics',
      JSON.stringify({ position: element.get('position') })
    );
  });
  links.forEach((link) => {
    // property, subject, object, propertyEntry, fieldEntry
    const blank = rdfGraph
      .add(null, 'rdf:type', 'esterms:StatementLayout')
      .getSubject();
    if (link.prop('itemEntry')) {
      rdfGraph.add(
        blank,
        'esterms:ofFieldEntry',
        link.prop('itemEntry').getURI()
      );
    }
    if (link.prop('propertyEntry')) {
      rdfGraph.add(
        blank,
        'esterms:ofPropertyEntry',
        link.prop('propertyEntry').getURI()
      );
    }
    if (link.prop('property')) {
      rdfGraph.add(blank, 'esterms:ofPredicate', link.prop('property'));
    }
    if (link.prop('subject')) {
      rdfGraph.add(blank, 'esterms:ofSubject', link.prop('subject'));
    }
    if (link.prop('object')) {
      rdfGraph.add(blank, 'esterms:ofObject', link.prop('object'));
    }
    const obj = {};
    const vertices = link.get('vertices');
    if (vertices) {
      obj.vertices = vertices;
    }
    rdfGraph.addL(blank, 'esterms:graphics', JSON.stringify(obj));
  });
  return rdfGraph;
};

/**
 * Extracts the diagram type from the metadata of the diagram entry.
 *
 * @param {Entry} diagramEntry
 * @returns {('class'|'form'|'mixed')}
 */
export const getDiagramTypeFromMetadata = (diagramEntry) => {
  const md = diagramEntry.getMetadata();
  const diagramResource = diagramEntry.getResourceURI();
  const styleURI =
    md.findFirstValue(diagramResource, 'esterms:diagramStyle') ||
    namespaces.expand('esterms:MixedDiagram');
  switch (namespaces.shortenKnown(styleURI)) {
    case 'esterms:ClassDiagram':
      return 'class';
    case 'esterms:FormDiagram':
      return 'form';
    default:
      return 'mixed';
  }
};

/**
 * Sets the diagram type in the metadata of the provided diagramEntry, note the
 * metadata is not committed.
 *
 * @param {Entry} diagramEntry
 * @param {('class'|'form'|'mixed')} type
 */
export const setDiagramTypeInMetadata = (diagramEntry, type) => {
  const md = diagramEntry.getMetadata();
  const diagramResource = diagramEntry.getResourceURI();
  md.findAndRemove(diagramResource, 'esterms:diagramStyle');
  switch (type) {
    case 'class':
      md.add(diagramResource, 'esterms:diagramStyle', 'esterms:ClassDiagram');
      break;
    case 'form':
      md.add(diagramResource, 'esterms:diagramStyle', 'esterms:FormDiagram');
      break;
    default: // mixed
      md.add(diagramResource, 'esterms:diagramStyle', 'esterms:MixedDiagram');
  }
};

/**
 *
 * @param {Entry} diagramEntry
 * @returns {string}
 */
export const getLabelTypeFromMetadata = (diagramEntry) => {
  const labelType = diagramEntry
    .getMetadata()
    .findFirstValue(diagramEntry.getResourceURI(), RDF_PROPERTY_LABEL_TYPE);
  return labelType || LABEL_TYPE_TITLE;
};

/**
 * Sets label type for the diagram entry.
 *
 * @param {Entry} diagramEntry
 * @param {string} labelType
 * @returns {Entry}
 */
export const setLabelTypeInMetadata = (diagramEntry, labelType) => {
  const metadata = diagramEntry.getMetadata();
  const resourceURI = diagramEntry.getResourceURI();
  metadata.findAndRemove(resourceURI, RDF_PROPERTY_LABEL_TYPE);
  metadata.addL(resourceURI, RDF_PROPERTY_LABEL_TYPE, labelType);
  return diagramEntry;
};

/**
 * Extracts the boolean if implicit classes should be included in the diagram.
 *
 * @param {Entry} diagramEntry
 * @returns {boolean}
 */
export const getImplicitClassesFromMetadata = (diagramEntry) => {
  const md = diagramEntry.getMetadata();
  const diagramResource = diagramEntry.getResourceURI();
  const implicit = md.findFirstValue(
    diagramResource,
    'esterms:inlcudeImplicitClassesInDiagram'
  );
  return implicit === undefined || implicit === 'true';
};

/**
 * Set in the metadata of the provided diagramEntry if implicit classes should be included or not.
 * Metadata is not saved by this function.
 *
 * @param {Entry} diagramEntry
 * @param {boolean} implicit
 */
export const setImplicitClassesFromMetadata = (diagramEntry, implicit) => {
  const md = diagramEntry.getMetadata();
  const diagramResource = diagramEntry.getResourceURI();
  md.findAndRemove(diagramResource, 'esterms:inlcudeImplicitClassesInDiagram');
  if (!implicit) {
    md.addD(
      diagramResource,
      'esterms:inlcudeImplicitClassesInDiagram',
      'false',
      'xsd:boolean'
    );
  }
};
