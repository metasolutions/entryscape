import { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { graphPropType } from 'commons/util/entry';
import { Chip } from '@mui/material';
import { Presenter as RDFormsPresenter } from '@entryscape/rdforms/renderers/react';
import { useTemplateContext } from 'models/rdforms/TemplateProvider';
import './PresenterItem.scss';

/**
 * Wrapper component for presenter items,
 * both rdforms and other components.
 */
export const PresenterItem = ({ children }) => (
  <div className="esmoPresenterItem">{children}</div>
);

PresenterItem.propTypes = {
  children: PropTypes.node,
};

/**
 * Hook to get rdforms component by template property
 */
export const useRdformsPresenterItem = ({ template, graph, resourceURI }) => {
  if (!graph || !template || !resourceURI) return null;

  const presenter = new RDFormsPresenter({
    graph,
    template,
    resource: resourceURI,
    compact: false,
  });

  return presenter?.domNode.component;
};

/**
 * Presenter component that uses rdforms
 */
export const RdformsPresenterItem = ({ property }) => {
  const { entry, getTemplate } = useTemplateContext();

  const PresenterComponent = useRdformsPresenterItem({
    property,
    template: getTemplate(property),
    graph: entry?.getMetadata(),
    resourceURI: entry?.getResourceURI(),
  });

  if (!PresenterComponent) return null;

  return (
    <PresenterItem>
      <PresenterComponent />
    </PresenterItem>
  );
};

RdformsPresenterItem.propTypes = {
  property: PropTypes.string,
};

/**
 * Extract values by metadata property
 */
const usePresenterItemValues = ({ property }) => {
  const { entry } = useTemplateContext();
  const [values, setValues] = useState(null);

  useEffect(() => {
    if (!entry) return;

    const metadata = entry.getMetadata();
    const statements = metadata.find(entry.getResourceURI(), property);
    if (statements?.length) {
      setValues(statements.map((statement) => statement.getValue()));
    }
  }, [entry, property]);

  return values;
};

export const PresenterItemChip = ({ label }) => (
  <span className="esmoPresenterItem__chip">
    <Chip label={label} />
  </span>
);

PresenterItemChip.propTypes = {
  label: PropTypes.string,
};

/**
 * When rdforms component can't be used,
 * this can be used together with PresenterItemValues
 */
export const PresenterItemLabel = ({ label = '' }) => (
  <div className="esmoPresenterItem__label">{label}</div>
);

PresenterItemLabel.propTypes = {
  label: PropTypes.string,
};

/**
 * Presenter component for use cases when rdforms component
 * can't be used, when customization is needed.
 */
export const PresenterItemValues = ({ property, Component }) => {
  const values = usePresenterItemValues({ property });

  if (!values?.length) return null;

  return (
    <div>
      {values.map((value, index) => (
        // eslint-disable-next-line react/no-array-index-key
        <Component key={index} label={value} />
      ))}
    </div>
  );
};

PresenterItemValues.propTypes = {
  property: PropTypes.string,
  Component: PropTypes.func,
};

/**
 * Auxilary function for preview presentation
 */
const createRDFormsPresesenter = ({ graph, template, resourceURI }) => {
  if (!graph || !template || !resourceURI) return;

  const presenter = new RDFormsPresenter({
    graph,
    template,
    resource: resourceURI,
    compact: false,
  });
  return presenter.domNode.component;
};

/**
 * Presents template and graph in preview
 */
export const PreviewPresenterItem = ({ graph, resourceURI, template }) => {
  const PreviewPresenterComponent = createRDFormsPresesenter({
    graph,
    template,
    resourceURI,
  });
  return PreviewPresenterComponent ? <PreviewPresenterComponent /> : null;
};

PreviewPresenterItem.propTypes = {
  graph: graphPropType,
  resourceURI: PropTypes.string,
  template: PropTypes.shape({}),
};
