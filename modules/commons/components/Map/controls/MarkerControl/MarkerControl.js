import { useState } from 'react';
import PropTypes from 'prop-types';
import { Place as MarkerIcon } from '@mui/icons-material';
import { useTranslation } from 'commons/hooks/useTranslation';
import escoMapNls from 'commons/nls/escoMap.nls';
import { useMapContext } from 'commons/components/Map/hooks/useMapContext';
import { useOpenlayers } from 'commons/components/Map/hooks/useOpenlayers';
import { ControlButton } from '../ControlButton';
import { useDrawMode } from '../DrawControlGroup';

export const MARKER_ACTION = 'marker';

export const MarkerControl = ({ onDrawEnd }) => {
  const translate = useTranslation(escoMapNls);
  const [drawMode, setDrawMode] = useDrawMode();
  const [{ map }] = useMapContext();
  const mapUtils = useOpenlayers();
  const [drawInteraction, setDrawInteraction] = useState(null);
  const isActive = drawMode === MARKER_ACTION;

  const activateDraw = () => {
    const vectorLayer = mapUtils.createVectorLayer();
    const interaction = mapUtils.createDrawInteraction(
      vectorLayer.getSource(),
      'point'
    );
    setDrawInteraction(interaction);

    map.addInteraction(interaction);

    interaction.on('drawabort', () => {
      setDrawMode(null);
    });
    interaction.on('drawend', ({ feature }) => {
      setDrawMode(null);
      onDrawEnd({
        feature,
        type: 'point',
      });
    });
  };

  const handleClick = (event) => {
    event.stopPropagation();
    if (isActive) return;
    setDrawMode(MARKER_ACTION);
    activateDraw();
  };

  if (drawInteraction && !isActive) {
    map.removeInteraction(drawInteraction);
    setDrawInteraction(null);
  }

  return (
    <ControlButton
      ariaLabel="marker"
      active={isActive}
      onClick={handleClick}
      Icon={MarkerIcon}
      title={translate('markerEditTitle')}
    />
  );
};

MarkerControl.propTypes = {
  onDrawEnd: PropTypes.func,
};
