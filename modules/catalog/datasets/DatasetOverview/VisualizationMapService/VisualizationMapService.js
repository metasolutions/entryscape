import { useEffect } from 'react';
import {
  Map,
  useMapContext as useOpenlayersMapContext,
} from 'commons/components/Map';
import PropTypes from 'prop-types';

const VisualizationMapService = ({
  onViewExtentChange,
  mapOptions,
  children,
  height = '60vh',
  ...rest
}) => {
  const [{ map }] = useOpenlayersMapContext();

  // Add listener for extent change on panning and zooming
  useEffect(() => {
    if (!map) return;
    map.getView().fit(mapOptions.initialView);

    if (!onViewExtentChange) return;

    map.on('moveend', onViewExtentChange);

    return () => {
      map.un('moveend', onViewExtentChange);
    };
  }, [map, onViewExtentChange, mapOptions]);

  return (
    <Map height={height} mapOptions={mapOptions} {...rest}>
      {children}
    </Map>
  );
};

export default VisualizationMapService;

VisualizationMapService.propTypes = {
  onViewExtentChange: PropTypes.func,
  mapOptions: PropTypes.shape({
    initialView: PropTypes.arrayOf(PropTypes.number),
  }),
  height: PropTypes.string,
  children: PropTypes.node,
};
