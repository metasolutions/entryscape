const createVariants = require('parallel-webpack').createVariants;
const { merge } = require('webpack-merge');
const webpack = require('webpack');
const getCommonConfig = require('./webpack.common');
const path = require('path');
const TerserPlugin = require('terser-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

let app = '';
process.argv.forEach((arg) => {
  if (arg.startsWith('--app=')) {
    app = arg.replace('--app=', '');
  }
});

/** ********** CONFIGURATION *********** */

// Those options will be mixed into every variant
// and passed to the `createConfig` callback.
const baseOptions = {
  // preferredDevTool: process.env.DEVTOOL || 'eval'
};

// This object defines the potential option variants
// the key of the object is used as the option name, its value must be an array
// which contains all potential values of your build.
const variants = {
  app: app ? [app] : ['suite', 'registry'],
  debug: [true, false],
};

const createConfig = ({ app, debug }) => {
  const { ENTRYSCAPE_VERSION, ENTRYSCAPE_STATIC } = process.env;
  const optimization = {};

  if (debug) {
    optimization.minimizer = [new TerserPlugin()];
  }

  const showNLSWarnings = false; // @todo (argv && argv['nls-warnings']) || false;

  const commonConfig = getCommonConfig({ app, showNLSWarnings });
  return merge(commonConfig, {
    mode: debug ? 'development' : 'production',
    entry: [
      path.join(__dirname, 'modules', 'commons', 'util', 'onLoadProd.js'),
      path.join(__dirname, 'app', app, 'index.js'),
    ],
    devtool: debug ? 'inline-cheap-module-source-map' : false,
    output: {
      chunkFilename: '[name].[contenthash].js',
      filename: `app.${debug ? 'debug.js' : 'js'}`,
      clean: {
        // prevent removing build files with multiple targets
        keep(asset) {
          const keepFileName = debug ? 'app.js' : 'app.debug.js';
          return asset.includes(keepFileName);
        },
      },
    },
    plugins: [
      new webpack.DefinePlugin({
        DEVELOPMENT: false,
      }),
      new HtmlWebpackPlugin({
        // Also generate a test.html
        filename: 'index.html',
        // lodash template instead of html to avoid conflict with raw loader
        template: path.join(__dirname, 'app', 'suite', 'index.ejs'),
        minify: false,
        inject: false,
        identifier: ENTRYSCAPE_VERSION,
        source: `${ENTRYSCAPE_STATIC}/${app}/${ENTRYSCAPE_VERSION}/index.html`, // @todo @valentino path.join
        static: `${ENTRYSCAPE_STATIC}/${app}/${ENTRYSCAPE_VERSION}`,
      }),
    ],
    optimization,
  });
};

module.exports = createVariants(baseOptions, variants, createConfig);
