import PropTypes from 'prop-types';
import Button from '@mui/material/Button';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormGroup from '@mui/material/FormGroup';
import FormLabel from '@mui/material/FormLabel';
import Radio from '@mui/material/Radio';
import RadioGroup from '@mui/material/RadioGroup';
import Switch from '@mui/material/Switch';
import TextField from '@mui/material/TextField';
import escaEmbedNLS from 'catalog/nls/escaEmbed.nls';
import EmbedPreviewDialog from 'catalog/catalogs/dialogs/EmbedPreviewDialog';
import ListActionDialog from 'commons/components/common/dialogs/ListActionDialog';
import { useTranslation } from 'commons/hooks/useTranslation';
import { getLabel } from 'commons/util/rdfUtils';
import React, { useState } from 'react';
import config from 'config';
import './index.scss';

const EMBED_URL =
  'https://static.cdn.entryscape.com/embed/catalog/latest/embed.js';
// TODO @valentino get the static URL from config

const FONTAWESOME_PATH = 'libs/fontawesome/css/font-awesome.min.css';

/**
 * extract protocol + hostname from url
 * @param {string} url
 * @returns {string}
 */
const getBaseUrl = (url) => url.match(/(^https?:\/\/[^/?#]+)(?:[/?#]|$)/i)?.[1];

/**
 *
 * @returns {string}
 */
const getFontawesomeUrl = () => {
  const staticUrl = config.get('entryscape.static.url');
  return `${staticUrl}${FONTAWESOME_PATH}`;
};

const EmbedDialog = ({ entry, closeDialog }) => {
  const [isTargetBlank, setTargetBlank] = useState(true);
  const [openPreview, setOpenPreview] = useState(false);
  const [theme, setTheme] = useState('FA');
  const entryId = entry.getId();
  const ctxId = entry.getContext().getId();
  const repobase = getBaseUrl(config.get('entrystore.repository'));

  // TODO unecessary! just set it to either and offer a copy to clipboard
  const handleTargetChange = (event) => {
    setTargetBlank(event.target.checked);
  };

  const handleThemeChange = (event) => {
    setTheme(event.target.value);
  };

  const t = useTranslation(escaEmbedNLS);

  const themeStyles = [
    { value: 'FA', label: t('faLabel') },
    { value: 'IMG', label: t('imgLabel') },
  ];

  const actions = (
    <Button autoFocus onClick={() => setOpenPreview(true)}>
      {t('previewButton')}
    </Button>
  );

  const scriptAttributes = [
    `src="${EMBED_URL}"`,
    `data-entry-id="${entryId}"`,
    `data-context-id="${ctxId}"`,
    `data-theme-style="${theme}"`,
    `data-repository="${repobase}"`,
    `data-target="${isTargetBlank ? '_blank' : '_self'}"`,
    'data-ignore-auth="true"',
    'data-ignore-fa-css="true"',
  ].join(' ');
  const embedScript = `<script ${scriptAttributes}></script>`;

  return (
    <>
      <ListActionDialog
        closeDialog={closeDialog}
        title={`${t('embedHeader')} ${getLabel(entry)}`}
        id="edit-embed"
        actions={actions}
      >
        <FormGroup
          row
          key="embed-themeFormats"
          className="escaEmbedDialog__themeFormats"
        >
          <FormLabel
            component="legend"
            className="escaEmbedDialog__formatLabel"
          >
            {t('themeFormats')}
          </FormLabel>
          <RadioGroup
            aria-label={t('themeStyle')}
            name="themeStyle"
            value={theme}
            onChange={handleThemeChange}
            className="escaEmbedDialog__radioGroup"
          >
            {themeStyles.map(({ value, label }, index) => (
              <FormControlLabel
                key={value}
                value={value}
                control={<Radio />}
                label={label}
                autoFocus={index === 0}
              />
            ))}
          </RadioGroup>
        </FormGroup>
        <FormGroup row key="embed-openToTarget">
          <FormControlLabel
            classes={{ root: 'escaEmbedDialog__switch' }}
            control={
              <Switch
                checked={isTargetBlank}
                onChange={handleTargetChange}
                name="checked"
                color="primary"
              />
            }
            label={t('chkNewTab')}
            labelPlacement="start"
          />
        </FormGroup>
        <FormGroup row key="embed-snippetCode">
          <FormLabel
            component="legend"
            className="escaEmbedDialog__snippetLabel"
          >
            {t('snippet')}
          </FormLabel>
          <TextField
            id="filled-multiline-static"
            multiline
            rows={10}
            disabled
            value={embedScript}
          />
        </FormGroup>
      </ListActionDialog>
      <EmbedPreviewDialog
        open={openPreview}
        onClose={() => setOpenPreview(false)}
        embedScript={embedScript}
        fontawesomeUrl={theme === 'FA' ? getFontawesomeUrl() : null}
      />
    </>
  );
};

EmbedDialog.propTypes = {
  entry: PropTypes.shape({
    getId: PropTypes.func,
    getContext: PropTypes.func,
  }),
  closeDialog: PropTypes.func,
};

export default EmbedDialog;
