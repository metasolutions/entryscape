import PropTypes from 'prop-types';
import { useState, useRef, useEffect, useMemo, useCallback } from 'react';
import { Link, List, ListItem, Typography } from '@mui/material';
import { Binding } from '@entryscape/rdforms';
import { useIntersect } from 'commons/components/rdforms/RdformsOutline/useIntersect';
import { useTranslation } from 'commons/hooks/useTranslation';
import escoRdformsNLS from 'commons/nls/escoRdforms.nls';
import { getNavigationItemsFromEditor } from 'commons/util/rdfUtils';
import SearchInput from 'commons/components/SearchInput';
import {
  getOutlineItemClasses,
  highlightElement,
  scrollToElement,
  moveFocusToLabel,
  isActive,
  isExcludedPredicate,
} from './util';
import './RdformsOutline.scss';

const RdformsOutline = ({
  editor,
  root,
  errors = [],
  searchProps,
  filterPredicates,
}) => {
  const translate = useTranslation(escoRdformsNLS);
  const { searchQuery, onSearchQueryChange, clearSearch } = searchProps;
  const listItemsRef = useRef();
  const [observedVisibility, setObservedElements] = useIntersect(root);
  /**
   * hiddenBindingsMask keeps track of all items visible in the outline,
   * including dependencies. For example, "Resource type" in edit/create
   * (Geographical) dataset has the dependency "Spatial data service type" which
   * will only be shown on the form if type "spatial data service" is selected.
   */
  const [hiddenBindingsMask, setHiddenBindingsMask] = useState([]);
  const [formItems, setFormItems] = useState([]);
  const [errorIds, setErrorIds] = useState([]);

  const outlineItems = useMemo(
    () => getNavigationItemsFromEditor(editor).filter((item) => item.id),
    [editor]
  );

  // TODO: `time   period` wouldn't return any results
  const queryMatchingItems = useMemo(
    () =>
      outlineItems.filter(({ label }) =>
        label?.toLowerCase().includes(searchQuery.trim())
      ),
    [outlineItems, searchQuery]
  );

  //  Bindings/fields whose dependencies are not ok are hidden
  const updateHiddenBindings = useCallback(() => {
    const newMask = queryMatchingItems.map(({ binding }) => {
      if (isExcludedPredicate(binding.getPredicate(), filterPredicates))
        return true;
      if (!binding.getItem().getDeps()) return false;
      if (binding.isValid()) return false;
      return !binding.getCardinalityTracker().isDepsOk();
    });

    setHiddenBindingsMask(newMask);
  }, [queryMatchingItems, filterPredicates]);

  const updateErrorIds = useCallback(
    (changedBinding) => {
      const changedItemId = changedBinding.getItem()._internalId;
      const updatedErrorIds = errorIds.filter((id) => id !== changedItemId);

      setErrorIds(updatedErrorIds);
    },
    [errorIds]
  );

  /**
   * Listens to cardinalityTracker changes to
   * a) handle items with dependencies
   * b) update the outline error state
   *
   * @param {Binding} changedBinding - The binding that has been edited
   */
  const cardinalityChangeListener = useCallback(
    (changedBinding) => {
      updateHiddenBindings();
      updateErrorIds(changedBinding);
    },
    [updateErrorIds, updateHiddenBindings]
  );

  useEffect(() => {
    if (!errors.length) return;

    const errorItemIds = errors.map((error) => error.item._internalId);
    setErrorIds(errorItemIds);
  }, [errors]);

  /**
   * Creates a visibity mask for the outline items. This is necessary to
   * correctly show and hide rdforms' dependent fields.
   */
  useEffect(() => {
    updateHiddenBindings();

    // Needed for cleanup below, cause CardinalityTracker.removeListener expects
    // the same exact function
    const listeners = outlineItems.map(({ binding }) => {
      const listener = () => cardinalityChangeListener(binding);
      binding.getCardinalityTracker().addListener(listener);
      return listener;
    });

    return () =>
      outlineItems.forEach(({ binding }, index) =>
        binding.getCardinalityTracker().removeListener(listeners[index])
      );
  }, [cardinalityChangeListener, outlineItems, updateHiddenBindings]);

  /**
   * Sets the form items visible in the actual outline and the dom elements to
   * be observed by the intersection observer.
   */
  useEffect(() => {
    // Use the mask to filter out dependent items that shouldn't be visible.
    const items = queryMatchingItems.filter(
      (_item, index) => !hiddenBindingsMask[index]
    );

    const formInputElements = items
      .map(({ id }) => document.getElementById(id))
      .filter((element) => element);

    setObservedElements(formInputElements);
    setFormItems(items);
  }, [queryMatchingItems, setObservedElements, hiddenBindingsMask]);

  const handleLinkClick = (event, id) => {
    event.preventDefault();
    scrollToElement(id, root);
    highlightElement(id);
  };

  const handleKeyDown = (event, id) => {
    if (event.key !== 'Enter') return;

    scrollToElement(id, root);
    highlightElement(id);
    // Timeout is necessary, moving the focus interrupts the scrolling behaviour
    setTimeout(() => moveFocusToLabel(id), 600);
  };

  return (
    <div className="escoRdformsOutline">
      {
        // using outlineItems here cause formItems change depending on query
        outlineItems.length > 0 || searchQuery ? (
          <SearchInput
            query={searchQuery}
            onChange={onSearchQueryChange}
            clear={clearSearch}
            placeholder={translate('fieldOutlineSearchPlaceholder')}
            clearTooltip={translate('fieldOutlineClearTooltip')}
          />
        ) : null
      }
      {
        // eslint-disable-next-line no-nested-ternary
        formItems.length ? (
          <nav className="escoRdformsOutline__navigation">
            <List ref={listItemsRef} dense>
              {formItems.map(({ id, label, editLabel, indented, binding }) => {
                const isInvalid = errorIds.includes(
                  binding.getItem()._internalId
                );
                return (
                  <ListItem
                    id={`${id}_outline`}
                    key={`rdformsOutline-listItem-${id}-${label}`}
                    classes={{
                      root: getOutlineItemClasses(
                        isActive(id, observedVisibility),
                        indented,
                        isInvalid
                      ),
                    }}
                  >
                    <Link
                      key={`rdformsOutline-link-${id}`}
                      href={`#${id}`}
                      noWrap
                      onClick={(event) => handleLinkClick(event, id)}
                      onKeyDown={(event) => handleKeyDown(event, id)}
                    >
                      {editLabel || label}
                    </Link>
                  </ListItem>
                );
              })}
            </List>
          </nav>
        ) : searchQuery ? (
          <div className="escoRdformsOutline__searchPlaceholder" role="status">
            {translate('fieldOutlineNoSearchResults')}
          </div>
        ) : null
      }
    </div>
  );
};

RdformsOutline.propTypes = {
  editor: PropTypes.shape({}),
  root: PropTypes.string,
  errors: PropTypes.arrayOf(PropTypes.shape({})),
  searchProps: PropTypes.shape({
    searchQuery: PropTypes.string,
    onSearchQueryChange: PropTypes.func,
    clearSearch: PropTypes.func,
  }),
  filterPredicates: PropTypes.shape({}),
};

// This component is unused right now but will be useful once we incorporate more
// stuff in the sidebar.
const RdformsOutlineHeader = ({ children }) => (
  <Typography variant="h1" className="escoRdformsOutline__header">
    {children}
  </Typography>
);

RdformsOutlineHeader.propTypes = {
  children: PropTypes.node,
};

export default RdformsOutline;
