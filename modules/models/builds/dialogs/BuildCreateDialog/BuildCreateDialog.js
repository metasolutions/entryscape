import PropTypes from 'prop-types';
import { useEffect, useMemo, useState } from 'react';
import ListActionDialog from 'commons/components/common/dialogs/ListActionDialog';
import {
  useTranslation,
  nlsBundlesPropType,
} from 'commons/hooks/useTranslation';
import escoListNLS from 'commons/nls/escoList.nls';
import esmoBuildsNLS from 'models/nls/esmoBuilds.nls';
import esmoCommonsNLS from 'models/nls/esmoCommons.nls';
import escoDialogsNLS from 'commons/nls/escoDialogs.nls';
import { useListModel, REFRESH } from 'commons/components/ListView';
import ContentWrapper from 'commons/components/common/ContentWrapper';
import { Button } from '@mui/material';
import {
  EditorProvider,
  FieldEditor,
  useEditorContext,
} from 'commons/components/forms/editors';
import { useESContext } from 'commons/hooks/useESContext';
import useAsync from 'commons/hooks/useAsync';
import { RDF_TYPE_BUNDLES } from 'models/utils/ns';
import {
  buildSelectionField,
  dependenciesField,
} from 'models/builds/utils/fieldDefinitions';
import { useSnackbar } from 'commons/hooks/useSnackbar';
import { ErrorCatcher } from 'commons/errors/ErrorCatcher';
import { ErrorWithMessage } from 'commons/errors/utils/errors';
import { useConfirmCloseAction } from 'commons/components/EntryListView/dialogs/create/handlers';
import {
  addBuildIncludeAndExclude,
  addBuildSelection,
  getDefaultSelection,
  getDependentContexts,
} from '../../utils/dependencies';
import { generateJsonBuild } from '../../utils/build';
import { fields } from './actions';

const NLS_BUNDLES = [
  esmoBuildsNLS,
  esmoCommonsNLS,
  escoListNLS,
  escoDialogsNLS,
];
const { Editor: DependencyEditor } = dependenciesField;
const { Editor: BuildSelectionEditor, ...selectionField } = buildSelectionField;

const BuildCreateDialog = ({ closeDialog }) => {
  const [, dispatch] = useListModel();
  const [createError, setCreateError] = useState();
  const translate = useTranslation(NLS_BUNDLES);
  const { context: currentContext } = useESContext();
  const [canSubmit, setCanSubmit] = useState(false);
  const {
    entry: prototypeEntry,
    graph,
    resourceURI,
    validate,
  } = useEditorContext();
  const [selectedFormsAndFields, setSelectedFormsAndFields] = useState();
  const [selectionError, setSelectionError] = useState(null);
  const [buildItems, setBuildItems] = useState([]);
  const [excludedContexts, setExcludedContexts] = useState([]);
  const [dependentContexts, setDependentContexts] = useState([]);
  const { runAsync: runGetDefaultSelection, status: selectionStatus } =
    useAsync();
  const [addSnackbar] = useSnackbar();
  const confirmClose = useConfirmCloseAction(closeDialog);

  useEffect(() => {
    if (!graph) return;
    graph.onChange = () => setCanSubmit(true);
  }, [graph, resourceURI]);

  useEffect(() => {
    if (selectedFormsAndFields) return;
    runGetDefaultSelection(
      getDefaultSelection(currentContext)
        .then(({ selection, items }) => {
          setSelectedFormsAndFields(selection);
          setBuildItems(items);
          setExcludedContexts([]);
          setDependentContexts(getDependentContexts(items, currentContext));
        })
        .catch((error) => {
          console.error(error);
        })
    );
  }, [runGetDefaultSelection, currentContext, selectedFormsAndFields]);

  const validateSelection = (selection) => {
    const errorMessage = selection?.length ? '' : translate('selectionError');
    setSelectionError(errorMessage);
    return errorMessage;
  };

  const handleSelectionChange = (newFormsAndFieldsSeletion, newBuildItems) => {
    setSelectedFormsAndFields(newFormsAndFieldsSeletion);
    setBuildItems(newBuildItems);
    validateSelection(newFormsAndFieldsSeletion);

    // always refresh dependencies when selection has changed
    setDependentContexts(getDependentContexts(newBuildItems, currentContext));
    setExcludedContexts([]);
  };

  const handleDependencyChange = (context, exclude) => {
    if (exclude) {
      if (excludedContexts.includes(context)) return;
      setExcludedContexts([...excludedContexts, context]);
      return;
    }
    setExcludedContexts(
      excludedContexts.filter((includedContext) => includedContext !== context)
    );
  };

  const createBuild = async () => {
    if (!graph) return;
    const errors = [
      ...validate(),
      validateSelection(selectedFormsAndFields),
    ].filter((error) => error);
    if (errors.length) {
      return;
    }

    addBuildSelection(graph, resourceURI, selectedFormsAndFields);

    addBuildIncludeAndExclude(
      graph,
      resourceURI,
      dependentContexts,
      excludedContexts
    );

    prototypeEntry.setMetadata(graph);

    try {
      const jsonBuild = await generateJsonBuild(buildItems, excludedContexts);
      const entry = await prototypeEntry.commit();
      await entry.getResource(true).put(JSON.stringify(jsonBuild, null, 2));
      dispatch({ type: REFRESH });
      addSnackbar({ message: translate('createBuildSuccess') });
      closeDialog();
    } catch (error) {
      setCreateError(
        new ErrorWithMessage(translate('createResourceFail'), error)
      );
    }
  };

  const actions = (
    <Button onClick={() => createBuild()} disabled={!canSubmit}>
      {translate('createButtonLabel')}
    </Button>
  );

  return (
    <>
      <ListActionDialog
        id="create-build-dialog"
        title={translate('createBundleHeader')}
        actions={actions}
        closeDialog={() => confirmClose(graph.isChanged())}
        fixedHeight
      >
        <ContentWrapper>
          <form noValidate autoComplete="off">
            {fields.map(({ labelNlsKey, ...fieldProps }) => (
              <FieldEditor
                key={fieldProps.property}
                label={translate(labelNlsKey)}
                size="small"
                nlsBundles={NLS_BUNDLES}
                {...fieldProps}
              />
            ))}
            <BuildSelectionEditor
              label={translate(buildSelectionField.labelNlsKey)}
              error={selectionError}
              selection={selectedFormsAndFields}
              buildItems={buildItems}
              onChange={handleSelectionChange}
              status={selectionStatus}
              disabled={selectionStatus !== 'resolved'}
              {...selectionField}
            />
            <DependencyEditor
              buildItems={buildItems}
              currentContext={currentContext}
              excludedContexts={excludedContexts}
              onChange={handleDependencyChange}
              buildItemsStatus={selectionStatus}
              label={translate(dependenciesField.labelNlsKey)}
            />
          </form>
        </ContentWrapper>
      </ListActionDialog>
      <ErrorCatcher error={createError} />
    </>
  );
};

BuildCreateDialog.propTypes = {
  closeDialog: PropTypes.func.isRequired,
  nlsBundles: nlsBundlesPropType,
};

const BuildCreateDialogWithEditorContext = (props) => {
  const { context } = useESContext();

  const prototypeEntry = useMemo(() => {
    return context
      .newEntry()
      .add('rdf:type', RDF_TYPE_BUNDLES)
      .add('rdf:type', 'prof:ResourceDescriptor')
      .add('prof:hasRole', 'http://www.w3.org/ns/dx/prof/role/schema');
  }, [context]);

  return (
    <EditorProvider entry={prototypeEntry}>
      <BuildCreateDialog {...props} />
    </EditorProvider>
  );
};

export default BuildCreateDialogWithEditorContext;
