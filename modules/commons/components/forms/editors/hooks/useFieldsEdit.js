import { useEffect, useState, useRef } from 'react';
import { useLanguage } from 'commons/hooks/useLanguage';
import { FieldGroup } from '../utils/FieldGroup';
import {
  ADD_FIELD,
  CHANGE,
  REFRESH,
  REMOVE_FIELD,
  REMOVE_FIELD_GROUP,
} from '../EditorContext';

const defaultDispatch = () => {};

/**
 *
 * @param {Object} editOptions
 * @returns {Object}
 */
export const useFieldsEdit = ({
  property,
  nodetype,
  datatype,
  graph,
  dispatch = defaultDispatch,
  onChange,
  resourceURI,
  validate,
  mandatory,
  insertEmptyField = true,
  isFormType = false,
  resetOnFormType = false,
  FieldGroupClass = FieldGroup,
  ...props
}) => {
  const [language] = useLanguage();
  const [fieldGroup] = useState(
    new FieldGroupClass({
      graph,
      datatype,
      resourceURI,
      property,
      nodetype,
      language,
      validate,
      mandatory,
      insertEmptyField,
      resetOnFormType,
      ...props,
    })
  );
  const isFieldGroupAdded = useRef(false);

  useEffect(() => {
    if (isFieldGroupAdded.current) return;
    isFieldGroupAdded.current = true;
    dispatch({
      type: ADD_FIELD,
      field: fieldGroup,
      ...(isFormType
        ? { formType: fieldGroup.getFields()[0]?.getValue() }
        : {}),
    });
  }, [dispatch, fieldGroup, isFormType]);

  const addField = () => {
    const field = fieldGroup.addField();
    dispatch({ type: REFRESH });
    return field;
  };

  const removeField = (field) => {
    fieldGroup.removeField(field);
    if (onChange) {
      onChange(field);
    }
    dispatch({ type: REMOVE_FIELD });
  };

  const updateValue = (field, value) => {
    field.setValue(value);
    fieldGroup.validate();
    if (onChange) {
      onChange(field, value);
    }
    dispatch({
      type: CHANGE,
      ...(isFormType ? { formType: value, value } : { value }),
    });
  };

  const updateLanguage = (field, newLanguage) => {
    field.setLanguage(newLanguage);
    fieldGroup.validate();
    dispatch({ type: CHANGE });
  };

  // Make sure to remove the field group if no longer visible, to prevent it
  // from being validated.
  useEffect(() => {
    return () => {
      if (resetOnFormType) {
        dispatch({ type: REMOVE_FIELD_GROUP, field: fieldGroup });
      }
    };
  }, [resetOnFormType, dispatch, fieldGroup]);

  return {
    addField,
    fields: fieldGroup.getFields(),
    fieldGroup,
    id: fieldGroup.id,
    labelId: fieldGroup.labelId,
    removeField,
    updateValue,
    updateLanguage,
    error: fieldGroup.error,
  };
};
