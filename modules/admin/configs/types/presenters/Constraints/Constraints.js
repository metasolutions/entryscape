import PropTypes from 'prop-types';
import { graphPropType } from 'commons/util/entry';
import { Graph } from '@entryscape/rdfjson';
import escoFormsNLS from 'commons/nls/escoForms.nls';
import { useTranslation } from 'commons/hooks/useTranslation';
import { Fields, FieldsLabel } from 'commons/components/forms/presenters';
import {
  RDF_PROPERTY_CONSTRAINT_PROPERTY,
  RDF_PROPERTY_CONSTRAINT_VALUE,
  RDF_PROPERTY_ORDER,
  RDF_PROPERTY_CONSTRAINT,
} from 'models/utils/ns';
import './Constraints.scss';

/**
 * @param {Graph} graph
 * @param {string} resourceURI
 * @returns {object}
 */
const getConstraints = (graph, resourceURI) => {
  const statements = graph.find(resourceURI, RDF_PROPERTY_CONSTRAINT);

  const constraints = statements.map((statement) => {
    const blankNodeId = statement.getValue();

    // constraint property
    const constraintProperty = graph.findFirstValue(
      blankNodeId,
      RDF_PROPERTY_CONSTRAINT_PROPERTY
    );
    const valueStatements = graph.find(
      blankNodeId,
      RDF_PROPERTY_CONSTRAINT_VALUE
    );

    // constraint values
    const constraintValues = valueStatements
      .map((valueStatement) => {
        const valueBlankNodeId = valueStatement.getValue();
        const order = graph.findFirstValue(
          valueBlankNodeId,
          RDF_PROPERTY_ORDER
        );
        const value = graph.findFirstValue(valueBlankNodeId, 'rdf:value');
        return { value, order };
      })
      .sort(
        ({ order: firstOrder }, { order: secondOrder }) =>
          firstOrder - secondOrder
      )
      .map(({ value }) => value)
      .join(', ');
    return { property: constraintProperty, values: constraintValues };
  });

  return constraints;
};

const Constraint = ({ property, values }) => {
  const translate = useTranslation(escoFormsNLS);

  return (
    <div className="esadConstraint">
      <FieldsLabel
        alignItems="start"
        label={`${translate('constraintPropertyLabel')}: `}
      >
        <span className="esadConstraint__value">{property}</span>
      </FieldsLabel>
      <FieldsLabel
        alignItems="start"
        label={`${translate('constraintObjectLabel')}: `}
      >
        <span className="esadConstraint__value">{values}</span>
      </FieldsLabel>
    </div>
  );
};

Constraint.propTypes = {
  property: PropTypes.string,
  values: PropTypes.oneOfType([PropTypes.string, PropTypes.array]),
};

export const Constraints = ({
  graph,
  resourceURI,
  children,
  ...fieldsProps
}) => {
  const constraints = getConstraints(graph, resourceURI);
  return constraints ? (
    // eslint-disable-next-line react/jsx-props-no-spreading
    <Fields {...fieldsProps}>
      {constraints.map(({ property, values }) => {
        return (
          <Constraint key={property} property={property} values={values} />
        );
      })}
      {children}
    </Fields>
  ) : null;
};

Constraints.propTypes = {
  graph: graphPropType,
  resourceURI: PropTypes.string,
  children: PropTypes.node,
};
