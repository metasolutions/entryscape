import * as ns from 'models/utils/ns';
import RfsField from './RfsField';

const SELECT_FIELD_PROPERTIES = [
  ns.RDF_PROPERTY_OPTION,
  ns.RDF_PROPERTY_DEPENDS_ON,
];

const SELECT_FIELD_COMPATIBLE_TYPES = [ns.RDF_TYPE_LOOKUP_FIELD];

const SELECT_FIELD_NODETYPES = [ns.RDF_PROPERTY_URI];

class RfsSelectField extends RfsField {
  constructor(props) {
    super({
      ...props,
      rdfType: ns.RDF_TYPE_SELECT_FIELD,
      nlsKeyLabel: 'selectFieldLabel',
      properties: SELECT_FIELD_PROPERTIES,
      compatibleTypes: SELECT_FIELD_COMPATIBLE_TYPES,
      nodetypes: SELECT_FIELD_NODETYPES,
    });
  }
}

export default RfsSelectField;
