import PropTypes from 'prop-types';
// eslint-disable-next-line max-len
import ListImageViewInfoDialog from 'commons/components/EntryListView/dialogs/ListImageViewInfoDialog';
import LinkedDataBrowserDialog from 'commons/components/LinkedDataBrowserDialog';
import { entityTypePropType } from 'commons/types/EntityType';

const ListContentViewerDialog = (actionsProps) => {
  const { entityTypeDef, entityType } = actionsProps;
  let hasImageContentViewer = false;

  // TODO https://metasolutions.atlassian.net/browse/ES-2826
  // only use entity type instance eventually
  const contentViewers = entityType
    ? entityType.get('contentviewers')
    : entityTypeDef.contentviewers;

  if (contentViewers) {
    const imageContentViewer = contentViewers.filter(
      (contentViewer) =>
        contentViewer.name === 'imageview' || contentViewer === 'imageview'
    );
    hasImageContentViewer = imageContentViewer.length > 0;
  }

  return (
    <>
      {hasImageContentViewer ? (
        <ListImageViewInfoDialog showReferences {...actionsProps} />
      ) : (
        <LinkedDataBrowserDialog {...actionsProps} />
      )}
    </>
  );
};

ListContentViewerDialog.propTypes = {
  actionsProps: PropTypes.shape({
    entityType: entityTypePropType,
    entityTypeDef: PropTypes.shape({}),
  }),
};

export default ListContentViewerDialog;
