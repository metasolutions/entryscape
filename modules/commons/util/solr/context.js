import { entrystore } from 'commons/store';
import { types } from '@entryscape/entrystore-js';
import {
  getUserEntryParentGroupsRURIs,
  getEntriesByTypeForContextIds,
} from '../context';
import { simpleFetchEntries } from './entry';

const mapEntriesToIds = (entries) => entries.map((entry) => entry.getId());

/**
 * Provides a query and an execution plan (fetching the actual entries)
 * @param {*} userEntry
 * @param {*} rdfType
 * @param {*} contextType
 * @returns {[SolrQuery , function]}
 */
const getContextQueryForUser = (userEntry, rdfType, contextType) => {
  // #region Query formulator
  const groupRURIs = getUserEntryParentGroupsRURIs(userEntry);

  const query = entrystore
    .newSolrQuery()
    .graphType(types.GT_CONTEXT)
    .resourceWrite(groupRURIs);

  if (contextType) {
    query.rdfType(contextType);
  }
  // #endregion

  // #region Query execution function
  /**
   *
   * @param {SolrQuery} solrQuery
   * @returns {Promise.<{entries: Entry[], size: number}>}
   */
  const fetchContextEntries = async (solrQuery) => {
    const searchList = solrQuery.list();
    const entries = await searchList
      .getAllEntries()
      .then(mapEntriesToIds)
      .then((contextIds) => getEntriesByTypeForContextIds(contextIds, rdfType));

    return { entries, size: searchList.getSize() };
  };
  // #endregion

  return [query, fetchContextEntries];
};

/**
 *
 * rdfType refers to rdf:type
 * @param {{rdfType: 'string', contextType: 'string', graphType: 'string'}} param0
 * @param {{userEntry: Entry, userInfo: object}} param1
 * @param store
 */
const getSearchQuery = (
  { rdfType, contextType, graphType },
  { userEntry, userInfo },
  store = entrystore
) => {
  /**
   * Users with non admin priviledges have a slightly more complicated
   * query and execution as first we need to fetch the contexts
   * with the right acl. See getContextQueryForUser
   */
  if (!userInfo.hasAdminRights) {
    return getContextQueryForUser(userEntry, rdfType, contextType);
  }

  const query = store.newSolrQuery();
  if (rdfType) {
    query.rdfType(rdfType);
  } else if (graphType === types.GT_PIPELINE) {
    query.graphType(types.GT_PIPELINE);
  } else {
    query.graphType(types.GT_CONTEXT).rdfType(contextType);
  }

  return [query, simpleFetchEntries];
};

const getFetchContextEntries = (rdfType) => {
  const fetchContextEntries = async (solrQuery) => {
    const searchList = solrQuery.list();
    const entries = await searchList
      .getAllEntries()
      .then(mapEntriesToIds)
      .then((contextIds) => getEntriesByTypeForContextIds(contextIds, rdfType));

    return { entries, size: searchList.getSize() };
  };

  return fetchContextEntries;
};

export const getSearchQueryForUser = (
  { userEntry, contextType },
  store = entrystore
) => {
  const groupRURIs = getUserEntryParentGroupsRURIs(userEntry);

  const query = store
    .newSolrQuery()
    .graphType(types.GT_CONTEXT)
    .resourceWrite(groupRURIs);

  if (contextType) {
    query.rdfType(contextType);
  }
  return query;
};

export const getSearchQueryForAdmin = (
  { rdfType, contextType, graphType },
  store = entrystore
) => {
  const query = store.newSolrQuery();
  if (rdfType) {
    query.rdfType(rdfType);
  } else if (graphType === types.GT_PIPELINE) {
    query.graphType(types.GT_PIPELINE);
  } else {
    query.graphType(types.GT_CONTEXT).rdfType(contextType);
  }
  return query;
};

export const getContextSearchQuery = (
  { rdfType, contextType, graphType },
  { userEntry, userInfo },
  store
) => {
  const isAdmin = userInfo.hasAdminRights;
  const getQuery = isAdmin ? getSearchQueryForAdmin : getSearchQueryForUser;
  const runQuery = isAdmin
    ? simpleFetchEntries
    : getFetchContextEntries(rdfType);

  const createQuery = () => {
    return getQuery({ rdfType, contextType, graphType, userEntry }, store);
  };

  return { createQuery, runQuery };
};

export { getSearchQuery };
