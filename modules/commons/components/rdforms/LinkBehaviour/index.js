import { system } from '@entryscape/rdforms';
import Lookup from 'commons/types/Lookup';
// eslint-disable-next-line max-len
import ListContentViewerDialog from 'commons/components/EntryListView/dialogs/ListContentViewerDialog';
import { entrystoreUtil as storeutil } from 'commons/store';
import React, { useEffect, useState } from 'react';
import OpenInNewIcon from '@mui/icons-material/OpenInNew';
import { getQueryConfiguration } from 'commons/components/rdforms/EntryChooser/util';
import './linkBehaviour.css';

const init = (onLinkClick = null) => {
  system.attachLinkBehaviour = (node, binding) => {
    const item = binding.getItem();
    const { remoteStoreUtil } = getQueryConfiguration(item);

    if (item.getType() === 'text') {
      return {
        target: '_blank',
      };
    }

    if (item.getType() !== 'choice') {
      return undefined;
    }

    // external link icon (component) will be removed if entry is found.
    const choice = binding.getChoice();
    let ExternalLinkComponent;
    if (!item.hasStaticChoices() || item.hasStyle('externalLink')) {
      const oldLoad = choice?.load;
      let promise = null;

      if (oldLoad) {
        promise = new Promise((success) => oldLoad(success));
        choice.load = (onSuccess) => promise.then(onSuccess);
      }

      ExternalLinkComponent = () => {
        const [externalLink, setExternalLink] = useState(
          choice.entry ? false : item.hasStyle('externalLink')
        );
        useEffect(() => {
          if (!promise) return;

          promise.then(() => {
            setExternalLink(!choice.entry);
          });
        }, []);

        return externalLink ? (
          <OpenInNewIcon className="rdformsExternalLinkIcon" />
        ) : null;
      };
    }

    return {
      component: ExternalLinkComponent ? <ExternalLinkComponent /> : null,
      target: '_blank',
      onClick: (e) => {
        if (choice.entry) {
          e.preventDefault();
          e.stopPropagation();

          if (onLinkClick) {
            onLinkClick({ entry: choice.entry });
            return;
          }
          const conf = Lookup.getEntityTypeByConstraints(
            item.getConstraints()
          ).get();
          if (conf) {
            const showInfoDialog = (entry) => {
              let dialogComponent;
              const closeDialog = () => {
                node.removeChild(dialogComponent);
              };
              dialogComponent = (
                <ListContentViewerDialog
                  entry={entry}
                  entityTypeDef={conf}
                  closeDialog={closeDialog}
                />
              );
              node.appendChild(dialogComponent);
            };
            storeutil
              .getEntryByResourceURI(binding.getValue())
              .then(showInfoDialog)
              .catch((error) => {
                console.error(error);
                if (!remoteStoreUtil) return;

                remoteStoreUtil
                  .getEntryByResourceURI(binding.getValue())
                  .then(showInfoDialog);
              });
          }
        }
      },
    };
  };

  // Add an external link icon
  system.attachExternalLinkBehaviour = () => {
    return {
      component: <OpenInNewIcon className="rdformsExternalLinkIcon" />,
    };
  };
};

init();

export default init;
