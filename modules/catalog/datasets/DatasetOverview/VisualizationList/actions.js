import {
  LIST_ACTION_REMOVE,
  LIST_ACTION_INFO,
} from 'commons/components/EntryListView/actions';
import escoListNLS from 'commons/nls/escoList.nls';
import escaDatasetNLS from 'catalog/nls/escaDataset.nls';
import escaVisualizationNLS from 'catalog/nls/escaVisualization.nls';
import escoGeoMapNLS from 'commons/nls/escoGeoMap.nls';
import escoErrorsNLS from 'commons/nls/escoErrors.nls';
import RemoveVisualizationDialog from '../dialogs/RemoveVisualizationDialog';
import PreviewVisualizationDialog from '../dialogs/PreviewVisualizationDialog';

export const nlsBundles = [
  escoListNLS,
  escaDatasetNLS,
  escaVisualizationNLS,
  escoGeoMapNLS,
  escoErrorsNLS,
];

export const rowActions = [
  {
    id: 'preview',
    Dialog: PreviewVisualizationDialog,
    isVisible: () => true,
    nlsKeyLabel: 'vizDialogPreview',
  },
  {
    ...LIST_ACTION_REMOVE,
    Dialog: RemoveVisualizationDialog,
    nlsKeyLabel: 'remove',
  },
];

export const presenterAction = {
  ...LIST_ACTION_INFO,
  nlsBundles,
};
