import PropTypes from 'prop-types';
import { InputBase, InputAdornment, IconButton } from '@mui/material';
import Tooltip from 'commons/components/common/Tooltip';
import { Search as SearchIcon, Clear as ClearIcon } from '@mui/icons-material';
import { makeStyles } from '@mui/styles';
import escoListNLS from 'commons/nls/escoList.nls';
import { useTranslation } from 'commons/hooks/useTranslation';
import { CLEAR_SEARCH } from '../../hooks/useListModel';
import './ListSearchField.scss';

const placeholderStyles = {
  opacity: 1,
};

// currently seems hard to fix by pure css solution
const useStyles = makeStyles({
  input: {
    '&::-webkit-input-placeholder': placeholderStyles,
    '&::-moz-placeholder': placeholderStyles,
    '&:-ms-input-placeholder': placeholderStyles, // Firefox 19+
    '&::-ms-input-placeholder': placeholderStyles, // IE 11
  },
});

export const ListSearchField = ({
  disabled = false,
  placeholder,
  search,
  dispatch,
}) => {
  const t = useTranslation([escoListNLS]);
  const classes = useStyles();

  return (
    <div
      className="escoListViewSearchField"
      data-highlight-id="listViewSearchField"
    >
      <InputBase
        disabled={disabled}
        value={search}
        classes={{
          root: 'escoListViewSearchField__input',
          focused: 'escoListViewSearchField__input--focused',
          input: classes.input,
        }}
        placeholder={placeholder}
        onChange={({ target }) =>
          dispatch({ type: 'SEARCH', value: target.value })
        }
        fullWidth
        inputProps={{ 'aria-label': 'Search' }}
        startAdornment={
          <InputAdornment
            position="start"
            classes={{
              root: `escoListViewSearchField__adornment${
                disabled ? '--disabled' : ''
              }`,
            }}
          >
            <SearchIcon />
          </InputAdornment>
        }
        endAdornment={
          search ? (
            <InputAdornment position="end">
              <Tooltip title={t('clear')}>
                <IconButton
                  aria-label={t('clear')}
                  onClick={() => dispatch({ type: CLEAR_SEARCH })}
                  onMouseDown={(event) => event.preventDefault()}
                  color="secondary"
                >
                  <ClearIcon />
                </IconButton>
              </Tooltip>
            </InputAdornment>
          ) : null
        }
      />
    </div>
  );
};

ListSearchField.propTypes = {
  placeholder: PropTypes.string,
  disabled: PropTypes.bool,
  search: PropTypes.string,
  dispatch: PropTypes.func,
};
