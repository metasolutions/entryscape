import React, { useState } from 'react';
import { Button, TextField } from '@mui/material';
import ContentWrapper from 'commons/components/common/ContentWrapper';
import PropTypes from 'prop-types';
import ListActionDialog from 'commons/components/common/dialogs/ListActionDialog';
import { spreadEntry, checkURIUniquenessScope } from 'commons/util/store';
import { entrystore } from 'commons/store';
import { useTranslation } from 'commons/hooks/useTranslation';
import escoDialogsNLS from 'commons/nls/escoDialogs.nls';
import { isUri } from 'commons/util/util';
import { entityTypePropType } from 'commons/types/EntityType';
import { EntityType } from 'entitytype-lookup';
import { ErrorCatcher } from 'commons/errors/ErrorCatcher';
import { ErrorWithMessage } from 'commons/errors/utils/errors';

const ReplaceLinkDialog = ({
  entry,
  entityType,
  closeDialog,
  onLinkChangeSuccess,
  copyOldLink,
  ReplaceURIField = TextField,
}) => {
  const translate = useTranslation([escoDialogsNLS]);
  // TODO https://metasolutions.atlassian.net/browse/ES-2825
  // only use entity type instance eventually
  const entityTypeConfig =
    entityType instanceof EntityType ? entityType.get() : entityType;
  const [buttonDisabled, setButtonIsDisabled] = useState(true);
  const { context, ruri, info: entryInfo } = spreadEntry(entry);
  const [newURI, setNewURI] = useState(copyOldLink ? ruri : '');
  const [errorMessage, setErrorMessage] = useState('');
  const [replaceError, setReplaceError] = useState();

  const handleNewLinkUpdate = (evt) => {
    const updatedURI = evt.target.value.trim();
    setNewURI(updatedURI);

    if (updatedURI && isUri(updatedURI)) {
      if (updatedURI === ruri) {
        setButtonIsDisabled(true);
        setErrorMessage('sameOldLink');
      } else {
        setButtonIsDisabled(false);
        setErrorMessage('');
      }
    } else if (updatedURI === '') {
      setButtonIsDisabled(true);
      setErrorMessage('newLinkRequired');
    } else {
      setButtonIsDisabled(true);
      setErrorMessage('invalidNewLink');
    }
  };

  const handleSaveForm = async () => {
    try {
      let isNewURIValid = true;
      if (entityTypeConfig?.uniqueURIScope) {
        isNewURIValid = await checkURIUniquenessScope(
          entityTypeConfig.uniqueURIScope,
          newURI,
          context,
          entityTypeConfig.rdfType
        );
      }
      if (isNewURIValid) {
        // 1. update entry resource URI
        entryInfo.setResourceURI(newURI);
        await entryInfo.commit();

        // 2. update all linked entries
        await entrystore
          .newSolrQuery()
          .context(context)
          .objectUri(ruri)
          .forEach(async (linkedEntry) => {
            linkedEntry.getMetadata().replaceURI(ruri, newURI);
            await linkedEntry.commitMetadata();
          });

        entry.setRefreshNeeded();
        await entry.refresh();
        closeDialog();
        onLinkChangeSuccess?.();
      } else {
        setButtonIsDisabled(true);
        setErrorMessage('unavailableNewLink');
        throw Error('URL already exists');
      }
    } catch (error) {
      setReplaceError(new ErrorWithMessage(translate('saveFail'), error));
    }
  };

  const dialogActions = (
    <Button autoFocus onClick={handleSaveForm} disabled={buttonDisabled}>
      {translate('replaceLinkFooterButton')}
    </Button>
  );

  return (
    <>
      <ListActionDialog
        id="replace-entity-uri"
        title={translate('replaceLinkHeader')}
        actions={dialogActions}
        closeDialog={closeDialog}
      >
        <ContentWrapper md={8}>
          <TextField
            disabled
            label={translate('replaceEntityURLCurrentLinkLabel')}
            id="replace-entity-uri-current"
            value={ruri}
          />
          <ReplaceURIField
            label={translate('replaceEntityURLNewLinkLabel')}
            id="replace-entity-uri-new"
            value={newURI}
            onChange={handleNewLinkUpdate}
            error={!!errorMessage}
            helperText={!!errorMessage && translate(errorMessage)}
          />
        </ContentWrapper>
      </ListActionDialog>
      <ErrorCatcher error={replaceError} />
    </>
  );
};

ReplaceLinkDialog.propTypes = {
  entry: PropTypes.shape().isRequired,
  entityType: PropTypes.oneOfType([PropTypes.shape(), entityTypePropType]),
  closeDialog: PropTypes.func,
  onLinkChangeSuccess: PropTypes.func,
  ReplaceURIField: PropTypes.func,
  copyOldLink: PropTypes.bool,
};

export default ReplaceLinkDialog;
