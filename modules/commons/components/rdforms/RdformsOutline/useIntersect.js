import { useState, useEffect } from 'react';

export const useIntersect = (
  rootId,
  contentClassName = 'escoRDFormsDialogWrapper__content'
) => {
  const [observedElements, setObservedElements] = useState([]);
  const [observedVisibility, setObservedVisibility] = useState(new Map());

  useEffect(() => {
    if (!observedElements?.length) {
      if (observedVisibility.size > 0) {
        setObservedVisibility(new Map()); // Reset the visible state
      }
      return;
    }
    // Reset the visible state
    const clearedVisibleState = new Map();
    observedElements.forEach((element) => {
      clearedVisibleState.set(element.id, false);
    });
    setObservedVisibility(clearedVisibleState);

    const root = document.getElementById(rootId);
    const { offsetTop } = document.getElementsByClassName(contentClassName)[0];
    const options = {
      root,
      rootMargin: `-${offsetTop}px 0px -72px 0px`, // Hardcoded bottom section of 72px
      threshold: 0.5,
    };

    const currentObserver = new IntersectionObserver(
      (intersectionObserverEntries) => {
        setObservedVisibility((currentVisibleState) => {
          let somethingChanged = false;
          // Lets clone the map
          const newVisibleState = new Map();
          currentVisibleState.forEach((value, key) => {
            newVisibleState.set(key, value);
          });
          // Update the visible state if something changed
          intersectionObserverEntries.forEach(({ isIntersecting, target }) => {
            if (
              target.id &&
              newVisibleState.has(target.id) &&
              newVisibleState.get(target.id) !== isIntersecting
            ) {
              newVisibleState.set(target.id, isIntersecting);
              somethingChanged = true;
            }
          });
          // Don't force a rerender if nothing changed.
          return somethingChanged ? newVisibleState : currentVisibleState;
        });
      },
      options
    );

    observedElements.forEach((element) => currentObserver.observe(element));
    return () => currentObserver.disconnect();
  }, [observedElements]);

  return [observedVisibility, setObservedElements];
};
