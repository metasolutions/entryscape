import RfsItem from 'models/utils/RfsItem';
import { RDF_TYPE_FORM } from 'models/utils/ns';
import { RFS_FORMS_COMMON_PROPERTIES } from './rfsForms';

class RfsForm extends RfsItem {
  constructor(props) {
    super({
      ...props,
      baseRdfType: RDF_TYPE_FORM,
      commonProperties: RFS_FORMS_COMMON_PROPERTIES,
    });
  }
}

export default RfsForm;
