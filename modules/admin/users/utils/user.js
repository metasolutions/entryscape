import { spreadEntry } from 'commons/util/store';
import { Entry } from '@entryscape/entrystore-js';

/**
 *
 * @param {Entry} entry
 */
export const replaceNameWithFirstAndLastName = (entry) => {
  const { metadata, ruri } = spreadEntry(entry);
  const firstname = metadata.findFirstValue(ruri, 'foaf:givenName');
  const lastname = metadata.findFirstValue(ruri, 'foaf:familyName');
  metadata.findAndRemove(ruri, 'foaf:name');
  metadata.addL(ruri, 'foaf:name', `${firstname} ${lastname}`);
};
