import eswoSpacesNLS from 'workbench/nls/eswoSpaces.nls';
import eswoBenchNLS from 'workbench/nls/eswoBench.nls';
import escoPlaceholderNLS from 'commons/nls/escoPlaceholder.nls';
import escoListNLS from 'commons/nls/escoList.nls';
import { localize } from 'commons/locale';
import CreateEntityDialog from 'commons/components/EntityOverview/dialogs/CreateEntityDialog';

// TODO there's too many nls for the actions. fix up
const nlsBundles = [
  eswoBenchNLS,
  eswoSpacesNLS,
  escoPlaceholderNLS,
  escoListNLS,
];

const listActions = [
  {
    id: 'create',
    Dialog: CreateEntityDialog,
    labelNlsKey: 'createButton',
    tooltipNlsKey: 'createButtonWithName',
    getNLSParam: ({ entityType }) => localize(entityType.get('label')),
  },
];

export { nlsBundles, listActions };
