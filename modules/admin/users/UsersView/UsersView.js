import { useCallback } from 'react';
import { types } from '@entryscape/entrystore-js';
import PersonAddDisabledIcon from '@mui/icons-material/PersonAddDisabled';
import {
  EntryListView,
  useSolrQuery,
  TITLE_COLUMN,
  TITLE_SORT,
  MODIFIED_COLUMN,
  ACTIONS_GROUP_COLUMN,
} from 'commons/components/EntryListView';
import {
  LIST_ACTION_INFO,
  LIST_ACTION_EDIT,
} from 'commons/components/EntryListView/actions';
import {
  withListModelProviderAndLocation,
  listPropsPropType,
} from 'commons/components/ListView';
import { useUserState } from 'commons/hooks/useUser';
import { getSearchQuery } from 'commons/util/solr/entry';
import { getUserRenderName, getIsUserDisabled } from 'commons/util/user';
import { useTranslation } from 'commons/hooks/useTranslation';
import { applyTitleOrUsernameParams } from 'commons/util/solr';
import { getPathFromViewName } from 'commons/util/site';
import LinkedDataBrowserDialog from 'commons/components/LinkedDataBrowserDialog';
import { listActions, nlsBundles } from './actions';

const UsersList = ({ listProps }) => {
  const { userEntry, userInfo } = useUserState();

  const createQuery = useCallback(() => {
    const [query] = getSearchQuery(
      {
        entryType: types.ET_LOCAL,
        graphType: types.GT_USER,
        resourceType: 'InformationResource',
      },
      { userEntry, userInfo }
    );
    return query;
  }, [userEntry, userInfo]);

  const queryResults = useSolrQuery({
    createQuery,
    applyQueryParams: applyTitleOrUsernameParams,
  });

  const translate = useTranslation(nlsBundles);
  const entryTypeName = translate('createEntryName');

  return (
    <EntryListView
      {...queryResults}
      {...listProps}
      nlsBundles={nlsBundles}
      viewPlaceholderProps={{
        label: translate('emptyMessageWithName', entryTypeName),
      }}
      listActions={listActions}
      getListItemProps={({ entry }) => ({
        to: getPathFromViewName('admin__users__user', {
          entryId: entry.getId(),
          contextId: '_principals',
        }),
      })}
      columns={[
        {
          ...TITLE_COLUMN,
          xs: 9,
          sortBy: TITLE_SORT,
          direction: 'row',
          getProps: ({ entry }) => ({
            primary: getUserRenderName(entry),
            secondary: getIsUserDisabled(entry) ? (
              <PersonAddDisabledIcon
                fontSize="small"
                title={translate('userStatusDisabled')}
              />
            ) : null,
          }),
        },
        MODIFIED_COLUMN,
        {
          ...ACTIONS_GROUP_COLUMN,
          actions: [
            {
              ...LIST_ACTION_INFO,
              formTemplateId: 'esc:User',
              Dialog: LinkedDataBrowserDialog,
            },
            {
              ...LIST_ACTION_EDIT,
              formTemplateId: 'esc:User',
            },
          ],
        },
      ]}
    />
  );
};

UsersList.propTypes = {
  listProps: listPropsPropType,
};

export default withListModelProviderAndLocation(UsersList);
