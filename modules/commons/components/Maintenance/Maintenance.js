import config from 'config';
import { StyledEngineProvider, ThemeProvider } from '@mui/material/styles';
import { Link, Typography } from '@mui/material';
import { useTranslation } from 'commons/hooks/useTranslation';
import escoErrorsNLS from 'commons/nls/escoErrors.nls';
import customTheme from 'commons/theme/mui/mui-theme';
import Logo from 'commons/nav/components/Logo';
import { i18n } from 'esi18n';
import './Maintenance.scss';

const Maintenance = () => {
  const t = useTranslation(escoErrorsNLS);
  const contactEmail = config.get('entryscape.maintenance.email');
  const locale = i18n.getLocale();
  const defaultLocale = config.get('locale.fallback');
  const { message } = config.get('entryscape.maintenance.ongoing');
  const localizedMessage = message[locale] || message[defaultLocale];

  const themePath = config.get('theme.default.themePath');
  const imagePath = new URL('images/maintenance.svg', themePath).href;

  return (
    <div className="escoMaintenance">
      <div className="escoMaintenance__bar">
        <Logo type="full" className="escoMaintenance__logo" />
      </div>
      <div className="escoMaintenance__body">
        <img
          className="escoMaintenance__image"
          src={imagePath}
          alt={t('maintenanceImageAlt')}
        />
        <Typography className="escoMaintenance__header" variant="h1">
          {t('maintenanceHeader')}
        </Typography>
        <Typography className="escoMaintenance__message">
          {localizedMessage}
        </Typography>
        <Typography className="escoMaintenance__subtitle">
          {t('maintenanceContact')}
          <Link href={`mailto:${contactEmail}`}>{contactEmail}</Link>.
        </Typography>
        <img
          className="escoMaintenance__brand"
          src={
            new URL(
              'images/es-logo-grayscale.svg',
              config.get('theme.default.themePath')
            ).href
          }
          alt="EntryScape logo"
        />
      </div>
    </div>
  );
};

const withStyles = (Component) => (props) =>
  (
    <StyledEngineProvider injectFirst>
      <ThemeProvider theme={customTheme}>
        <Component {...props} />
      </ThemeProvider>
    </StyledEngineProvider>
  );

export default withStyles(Maintenance);
