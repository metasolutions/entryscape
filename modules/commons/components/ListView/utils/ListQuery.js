import config from 'config';
import { getModifiedDate } from 'commons/util/metadata';

/**
 * Utility class for handling list items client side, including searching,
 * sorting and paging.
 */
export class ListQuery {
  constructor({ items, rowsPerPage = 10, minimumSearchLength }) {
    this._items = items || [];
    this._result = [...this._items];
    this._rowsPerPage = rowsPerPage;
    this._minimumSearchLength =
      minimumSearchLength || config.get('minimumSearchLength');
  }

  search(searchString, searchFields) {
    if (searchString === null || searchString === undefined) return this;
    if (searchString.trim().length < this._minimumSearchLength) {
      this._result = [...this._items];
      return this;
    }
    this._result = this._items.filter((item) =>
      searchFields.some((searchField) =>
        item[searchField].toLowerCase().includes(searchString.toLowerCase())
      )
    );
    return this;
  }

  sort(sort) {
    if (!sort) return this;
    const { field, order } = sort;

    this._result.sort((firstItem, secondItem) => {
      const compareValues =
        field === 'modified'
          ? [
              getModifiedDate(firstItem.entry),
              getModifiedDate(secondItem.entry),
            ]
          : [firstItem[field].toLowerCase(), secondItem[field].toLowerCase()];

      if (order !== 'asc') {
        compareValues.reverse();
      }
      const [firstValue, secondValue] = compareValues;
      return firstValue <= secondValue ? -1 : 1;
    });
    return this;
  }

  getSize() {
    return this._result.length;
  }

  restore() {
    this._result = [...this._items];
  }

  getResult(page) {
    if (page === undefined || page === null) return this._result;
    if (!this._rowsPerPage) {
      throw new Error('rowsPerPage is required when getting page');
    }

    const startIndex = this._rowsPerPage * page;
    return this._result.slice(startIndex, startIndex + this._rowsPerPage);
  }

  setItems(items) {
    this._items = items;
    this._result = this._items;
  }
}
