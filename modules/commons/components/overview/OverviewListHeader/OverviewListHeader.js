import PropTypes from 'prop-types';
import { useState } from 'react';
import { Entry } from '@entryscape/entrystore-js';
import { Grid, Typography } from '@mui/material';
import {
  Add as AddIcon,
  Help as HelpIcon,
  WarningAmber as WarningIcon,
} from '@mui/icons-material';
import IconButton from 'commons/components/IconButton';
import Tooltip from 'commons/components/common/Tooltip';
import { nlsBundlesPropType } from 'commons/hooks/useTranslation';
import './OverviewListHeader.scss';
import ErrorBoundary from 'commons/errors/ErrorBoundary';
import SnackbarFallbackComponent from 'commons/errors/SnackbarFallbackComponent';

export const OverviewListAction = ({
  action,
  actionParams,
  actionIcon = <AddIcon />,
  nlsBundles,
}) => {
  const [showActionDialog, setShowActionDialog] = useState(false);
  const {
    Dialog: ActionDialog = null,
    tooltip = '',
    // the plus button can become disabled now according to the max number of entities
    disabled = false,
    highlightId,
  } = action;

  return (
    <>
      <Tooltip title={tooltip}>
        {/* span is added to allow rendering tooltip even if button is disabled */}
        <span>
          <IconButton
            aria-label={tooltip}
            icon={actionIcon}
            onClick={() => setShowActionDialog(true)}
            disabled={disabled}
            data-highlight-id={highlightId}
          />
        </span>
      </Tooltip>
      {showActionDialog && ActionDialog ? (
        <ErrorBoundary
          onError={() => setShowActionDialog(false)}
          FallbackComponent={SnackbarFallbackComponent}
        >
          <ActionDialog
            key="overview-list-action-open"
            actionParams={actionParams}
            isOpen
            closeDialog={() => setShowActionDialog(false)}
            nlsBundles={nlsBundles}
          />
        </ErrorBoundary>
      ) : null}
    </>
  );
};

OverviewListAction.propTypes = {
  action: PropTypes.shape({
    Dialog: PropTypes.func,
    tooltip: PropTypes.string,
    disabled: PropTypes.bool,
    highlightId: PropTypes.string,
  }),
  actionParams: PropTypes.oneOfType([
    PropTypes.arrayOf(
      PropTypes.oneOfType([
        PropTypes.instanceOf(Entry),
        PropTypes.func,
        PropTypes.array,
        PropTypes.shape({}),
      ])
    ),
    PropTypes.shape({}),
  ]),
  actionIcon: PropTypes.element,
  nlsBundles: nlsBundlesPropType,
};

export const OverviewListHeader = ({
  heading,
  action,
  actionParams,
  actionIcon,
  nlsBundles,
  helperText = '',
  warningText = '',
  highlightId = '',
}) => (
  <Grid
    container
    item
    alignItems="center"
    classes={{ item: 'escoOverviewListHeader' }}
  >
    <Grid item className="escoOverviewListHeader__headingContainer">
      <Typography display="inline" variant="h2" data-highlight-id={highlightId}>
        {heading}
      </Typography>
      {helperText ? (
        <Tooltip title={helperText}>
          <HelpIcon className="escoOverviewListHeader__helperIcon" />
        </Tooltip>
      ) : null}
      {warningText ? (
        <Tooltip title={warningText}>
          <WarningIcon className="escoOverviewListHeader__helperIcon" />
        </Tooltip>
      ) : null}
    </Grid>
    {action ? (
      <Grid item className="escoOverviewListHeader__actionContainer">
        <OverviewListAction
          action={action}
          actionParams={actionParams}
          actionIcon={actionIcon}
          nlsBundles={nlsBundles}
        />
      </Grid>
    ) : null}
  </Grid>
);

OverviewListHeader.propTypes = {
  heading: PropTypes.string,
  action: PropTypes.shape({
    Dialog: PropTypes.func,
    tooltip: PropTypes.string,
  }),
  actionParams: PropTypes.oneOfType([
    PropTypes.arrayOf(
      PropTypes.oneOfType([
        PropTypes.instanceOf(Entry),
        PropTypes.func,
        PropTypes.array,
        PropTypes.shape({}),
      ])
    ),
    PropTypes.shape({}),
  ]),
  actionIcon: PropTypes.element,
  nlsBundles: nlsBundlesPropType,
  helperText: PropTypes.string,
  warningText: PropTypes.string,
  highlightId: PropTypes.string,
};
