import PropTypes from 'prop-types';
import { useUniqueURIScopeContext } from 'commons/hooks/useUniqueURIScope';
import escoEntryTypeNLS from 'commons/nls/escoEntryType.nls';
import { useTranslation } from 'commons/hooks/useTranslation';
import LinkField from './LinkField';

// This component has access to uniqueURIScope context
const UniqueLinkField = ({
  mimicsRdformsStyle,
  inputLabel,
  link,
  handleLinkInputChange,
  hasError,
  helperText,
}) => {
  const { isURIUnique, setIsURIUnique } = useUniqueURIScopeContext();
  const translation = useTranslation(escoEntryTypeNLS);
  return (
    <LinkField
      mimicsRdformsStyle={mimicsRdformsStyle}
      inputLabel={inputLabel}
      link={link}
      handleLinkInputChange={(event) => {
        handleLinkInputChange(event);
        setIsURIUnique(true);
      }}
      hasError={hasError || !isURIUnique}
      helperText={
        isURIUnique ? helperText : translation('linkPatternUnavailableURL')
      }
    />
  );
};

UniqueLinkField.propTypes = {
  mimicsRdformsStyle: PropTypes.bool,
  inputLabel: PropTypes.string,
  link: PropTypes.string,
  handleLinkInputChange: PropTypes.func,
  hasError: PropTypes.bool,
  helperText: PropTypes.string,
};
export default UniqueLinkField;
