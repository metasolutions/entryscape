import React, { useState, useCallback } from 'react';
import PropTypes from 'prop-types';
import {
  Alert,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  TextField,
} from '@mui/material';
import escoSigninNLS from 'commons/nls/escoSignin.nls';
import LoadingButton from 'commons/components/LoadingButton';
import { useTranslation } from 'commons/hooks/useTranslation';
import { validateEmail } from 'commons/util/util';

const SecondStageLoginDialog = ({ isOpen, close, url }) => {
  const [email, setEmail] = useState('');
  const [errors, setErrors] = useState({});
  const [hasEmail, setHasEmail] = useState(false);

  const translate = useTranslation(escoSigninNLS);

  const validate = useCallback(() => {
    if (!email) {
      return { email: translate('emailRequired') };
    }

    if (!validateEmail(email)) {
      return { email: translate('signupInvalidEmail') };
    }

    return { email: null };
  }, [translate, email]);

  const handleEmailChange = (event) => {
    setEmail(event.target.value);
    setHasEmail(true);
  };

  const handleSubmit = () => {
    if (!email) {
      setErrors(() => ({
        ...validate(),
      }));
      return;
    }

    if (validateEmail(email)) {
      try {
        const params = new URLSearchParams({ username: email });
        window.location.href = `${url}?${params.toString()}`;
      } catch (error) {
        setErrors({ sso: error.message });
      }
      close();
    } else {
      setErrors((currentErrors) => ({
        ...currentErrors,
        ...validate(),
      }));
    }
  };

  return (
    <Dialog
      id="second-stage-login-dialog"
      open={isOpen}
      onClose={close}
      aria-label="second-stage-login-dialog"
      maxWidth="sm"
      fullWidth
    >
      <DialogContent>
        <TextField
          autoFocus
          required
          id="email"
          label={translate('email')}
          name="email"
          autoComplete="email"
          value={email}
          error={!!errors.email}
          helperText={errors.email}
          onChange={handleEmailChange}
        />
        {errors.response && (
          <Alert severity="error" style={{ marginTop: '12px' }}>
            {errors.response}
          </Alert>
        )}
        <DialogActions>
          <Button
            style={{ marginRight: '12px' }}
            variant="text"
            onClick={close}
          >
            {translate('cancel')}
          </Button>
          <LoadingButton disabled={!hasEmail} onClick={handleSubmit}>
            {translate('next')}
          </LoadingButton>
        </DialogActions>
      </DialogContent>
    </Dialog>
  );
};

SecondStageLoginDialog.propTypes = {
  isOpen: PropTypes.bool,
  close: PropTypes.func,
  url: PropTypes.string,
};

export default SecondStageLoginDialog;
