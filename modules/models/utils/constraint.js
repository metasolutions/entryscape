import * as ns from 'models/utils/ns';
import { Entry } from '@entryscape/entrystore-js';
import { Graph } from '@entryscape/rdfjson';
import { getOrIgnoreEntry } from 'commons/util/entry';
import { OrderedFieldGroup } from 'commons/components/forms/editors/utils/OrderedFieldGroup';
import { Blank } from 'commons/components/forms/editors';
import { FieldGroup } from 'commons/components/forms/editors/utils/FieldGroup';
import { findGroupsByResourceURI } from 'commons/components/forms/editors/utils/editorContext';
import {
  RESOURCE,
  NAMESPACE_ENTRY,
  RESOURCE_ENTRY,
  getURIEditorType,
  getNamespaceEntryValue,
  getResourceEntryValue,
} from './metadata';

export const MISSING_PROPERTY = 'missingPropertyError';
export const CLASS_ENTRY = 'classEntry';
export const USE_TERMINOLOGY = 'useTerminology';
export const ADVANCED_CONSTRAINT = 'advancedConstraint';

export const MAIN_EDIT_MODE_CHOICES = [
  {
    labelNlsKey: 'selectClassLabel',
    value: CLASS_ENTRY,
  },
  {
    labelNlsKey: 'selectTerminologyLabel',
    value: USE_TERMINOLOGY,
    constrainedTo: [ns.RDF_TYPE_LOOKUP_FIELD],
  },
  {
    labelNlsKey: 'advancedLabel',
    value: ADVANCED_CONSTRAINT,
  },
];

/**
 *
 * @param {Graph} graph
 * @param {object} field
 * @returns {Array<(string|undefined)>}
 */
export const findConstraint = (graph, field) => {
  const blankNodeId = field.getBlankNodeId();
  const property = graph.findFirstValue(
    blankNodeId,
    ns.RDF_PROPERTY_CONSTRAINT_PROPERTY
  );
  const value = graph.findFirstValue(
    blankNodeId,
    ns.RDF_PROPERTY_CONSTRAINT_VALUE
  );
  return [property, value];
};

/**
 *
 * @param {string} errorText
 * @param {string[]} errors
 * @returns {string[]}
 */
export const getErrorTexts = (errorText, errors) => {
  if (!errorText) return ['', ''];
  return errors.map((error) => (error ? errorText : ' '));
};

/**
 * @param {Graph} graph
 * @param {string} resourceURI
 * @returns {string|undefined}
 */
export const getConstraintType = (graph, resourceURI) => {
  const [statement] = graph.find(resourceURI, ns.RDF_PROPERTY_CONSTRAINT);
  if (!statement) return;

  const blankNodeId = statement.getValue();
  const isAdvanced =
    graph.find(blankNodeId, 'rdf:type', ns.RDF_TYPE_CONSTRAINT).length > 0;

  if (isAdvanced) return ADVANCED_CONSTRAINT;
  if (graph.findFirstValue(blankNodeId, ns.RDF_PROPERTY_TERMINOLOGY))
    return USE_TERMINOLOGY;
  return CLASS_ENTRY;
};

/**
 * @param {Graph} graph
 * @param {string} resourceURI
 * @returns {Promise<string>}
 */
export const getConstraintProperty = (graph, resourceURI) => {
  const propertyType = getURIEditorType(
    graph,
    resourceURI,
    ns.RDF_PROPERTY_CONSTRAINT_PROPERTY,
    RESOURCE_ENTRY
  );

  if (propertyType === RESOURCE)
    return Promise.resolve(
      graph.findFirstValue(resourceURI, ns.RDF_PROPERTY_CONSTRAINT_PROPERTY)
    );

  if (propertyType === NAMESPACE_ENTRY) {
    return getNamespaceEntryValue(
      graph,
      resourceURI,
      ns.RDF_PROPERTY_CONSTRAINT_PROPERTY
    );
  }

  return getResourceEntryValue(
    graph,
    resourceURI,
    ns.RDF_PROPERTY_CONSTRAINT_PROPERTY
  );
};

/**
 * @param {Graph} graph
 * @param {string} resourceURI
 * @returns {Promise<string>}
 */
const getConstraintValue = async (graph, resourceURI) => {
  const valueType = getURIEditorType(
    graph,
    resourceURI,
    'rdf:value',
    NAMESPACE_ENTRY
  );

  if (valueType === RESOURCE)
    return Promise.resolve(graph.findFirstValue(resourceURI, 'rdf:value'));

  if (valueType === RESOURCE_ENTRY) {
    return getResourceEntryValue(graph, resourceURI, 'rdf:value');
  }

  return getNamespaceEntryValue(graph, resourceURI, 'rdf:value');
};

/**
 * @param {Graph} graph
 * @param {string} resourceURI
 * @returns {object|undefined}
 */
export const getAdvancedConstraints = async (graph, resourceURI) => {
  const statements = graph.find(resourceURI, ns.RDF_PROPERTY_CONSTRAINT);

  if (!statements.length) return;

  const constraints = await Promise.all(
    statements.map(async (statement) => {
      const blankNodeId = statement.getValue();

      const constraintProperty = await getConstraintProperty(
        graph,
        blankNodeId
      );

      const valueStatements = graph.find(
        blankNodeId,
        ns.RDF_PROPERTY_CONSTRAINT_VALUE
      );

      // constraint values
      const constraintValuesPromises = valueStatements.map(
        async (valueStatement) => {
          const valueBlankNodeId = valueStatement.getValue();
          const order = graph.findFirstValue(
            valueBlankNodeId,
            ns.RDF_PROPERTY_ORDER
          );
          const constraintValue = await getConstraintValue(
            graph,
            valueBlankNodeId
          );
          return { constraintValue, order };
        }
      );
      const constraintValues = await Promise.all(constraintValuesPromises).then(
        (values) => {
          return values
            .sort(({ order: firstOrder }, { order: secondOrder }) =>
              firstOrder < secondOrder ? -1 : 1
            )
            .map(({ constraintValue }) => constraintValue);
        }
      );

      return [constraintProperty, constraintValues];
    })
  );

  return Object.fromEntries(constraints);
};

const getResourceURIForResourceEntry = async (graph, resourceURI, property) => {
  const blankNodeId = graph.findFirstValue(
    resourceURI,
    ns.RDF_PROPERTY_CONSTRAINT
  );

  if (!blankNodeId) return '';

  // constraint value
  const entryURI = graph.findFirstValue(blankNodeId, property);
  const entry = await getOrIgnoreEntry(entryURI);
  if (!entry) return '';
  return entry.getResourceURI();
};

/**
 * @param {Graph} graph
 * @param {string} resourceURI
 * @returns {object|undefined}
 */
export const getClassEntryConstraint = async (graph, resourceURI) => {
  const constraintValue = await getResourceURIForResourceEntry(
    graph,
    resourceURI,
    ns.RDF_PROPERTY_RESOURCE_ENTRY
  );

  return {
    'http://www.w3.org/1999/02/22-rdf-syntax-ns#type': constraintValue,
  };
};

/**
 *
 * @param {Graph} graph
 * @param {string} resourceURI
 * @returns {Promise<object>}
 */
const getTerminologyConstraint = async (graph, resourceURI) => {
  const constraintValue = await getResourceURIForResourceEntry(
    graph,
    resourceURI,
    ns.RDF_PROPERTY_TERMINOLOGY
  );
  return {
    'http://www.w3.org/2004/02/skos/core#inScheme': constraintValue,
    'http://www.w3.org/1999/02/22-rdf-syntax-ns#type':
      'http://www.w3.org/2004/02/skos/core#Concept',
  };
};

/**
 * @param {Graph} graph
 * @param {string} resourceURI
 * @returns {object|undefined}
 */
export const getConstraints = (graph, resourceURI) => {
  const constraintType = getConstraintType(graph, resourceURI);
  if (!constraintType) return;
  if (constraintType === ADVANCED_CONSTRAINT)
    return Promise.resolve(getAdvancedConstraints(graph, resourceURI));
  if (constraintType === USE_TERMINOLOGY)
    return getTerminologyConstraint(graph, resourceURI);
  return getClassEntryConstraint(graph, resourceURI);
};

/**
 * Wrapper for getConstraints using entry and optional resourceURI
 *
 * @param {Entry} entry
 * @param {string} resourceURI
 * @returns {object|undefined}
 */
export const extractConstraints = (entry, resourceURI) => {
  return getConstraints(
    entry.getMetadata(),
    resourceURI || entry.getResourceURI()
  );
};

/**
 * Find all dependent field groups for a ordered value group, in a field group
 * set.
 *
 * @param {Set} fieldSet
 * @param {FieldGroup} constraintGroup
 * @returns {FieldGroup[]}
 */
const getOrderedValueGroups = (fieldSet, constraintGroup) => {
  const constraintValue = constraintGroup.getFields()[0];
  const bnode = constraintValue.getBlank().getBlankNodeId();

  // one value group for one constraint order/value pair
  const [valueBlankGroup] = findGroupsByResourceURI(fieldSet, bnode);

  const valueGroups = [valueBlankGroup];

  // A value group can only have one field value (rdf:value)
  const valueField = valueBlankGroup.getFields()[0];

  // A field value can either be a literal or blank node. If it's a blank node,
  // the dependent groups are detected.
  if (!(valueField instanceof Blank)) return valueGroups;
  const groups = findGroupsByResourceURI(fieldSet, valueField.getBlankNodeId());
  valueGroups.push(...groups);
  return valueGroups;
};

/**
 * Find all dependent field groups for a constraint group, in a field group
 * set.
 *
 * @param {Set} fieldSet
 * @param {FieldGroup} constraintGroup
 * @returns {FieldGroup[]}
 */
const getPropertyGroups = (fieldSet, constraintGroup) => {
  const constraintPropertyField = constraintGroup.getFields()[0];
  // A field value can either be a literal or blank node. If it's a blank node,
  // the dependent groups are detected.
  if (!(constraintPropertyField instanceof Blank)) return [];
  const groups = findGroupsByResourceURI(
    fieldSet,
    constraintPropertyField.getBlankNodeId()
  );
  return groups;
};

/**
 * Find all dependent field groups for a constraint blank node, in a field group
 * set.
 *
 * @param {Set} fieldSet
 * @param {string} constraintBlankNodeId
 * @returns {FieldGroup[]}
 */
export const findConstraintFieldGroups = (fieldSet, constraintBlankNodeId) => {
  // Each constraint has a property and value. Find the groups for these.
  const constraintGroups = findGroupsByResourceURI(
    fieldSet,
    constraintBlankNodeId
  );

  const allGroups = [...constraintGroups];
  // get the dependencies for the constraint groups
  constraintGroups.forEach((constraintGroup) => {
    // get all groups dependent on the value constraint
    if (constraintGroup instanceof OrderedFieldGroup) {
      const valueGroups = getOrderedValueGroups(fieldSet, constraintGroup);
      allGroups.push(...valueGroups);
      return;
    }
    // get all groups dependent on the property constraint
    const propertyGroups = getPropertyGroups(fieldSet, constraintGroup);
    allGroups.push(...propertyGroups);
  });
  return allGroups;
};
