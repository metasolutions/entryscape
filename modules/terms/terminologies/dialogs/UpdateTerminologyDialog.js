import { useCallback, useEffect } from 'react';
import PropTypes from 'prop-types';
import esteImportNLS from 'terms/nls/esteImport.nls';
import esteSchemeNLS from 'terms/nls/esteScheme.nls';
import esteTerminologyNLS from 'terms/nls/esteTerminology.nls';
import { Button } from '@mui/material';
import ContentWrapper from 'commons/components/common/ContentWrapper';
import EntryImport from 'commons/components/entry/Import';
import useLinkOrFile from 'commons/components/entry/Import/useLinkOrFile';
import { useTranslation } from 'commons/hooks/useTranslation';
import ProgressList from 'commons/components/common/ProgressList';
import { useTasksList, STATUS_FAILED } from 'commons/hooks/useTasksList';
import { useMainDialog } from 'commons/hooks/useMainDialog';
import { AcknowledgeAction } from 'commons/components/common/dialogs/MainDialog';
import { updateContextTitle } from 'commons/util/context';
import ListActionDialog from 'commons/components/common/dialogs/ListActionDialog';
import useSkosUpdate from 'terms/terminologies/dialogs/hooks/useSkosUpdate';
import useSkosLoad from 'terms/terminologies/dialogs/hooks/useSkosLoad';
import { Entry } from '@entryscape/entrystore-js';
import { useOverviewModel, REFRESH } from 'commons/components/overview';
import { TASKS_UPDATE_TERMINOLOGY } from '../tasks';

const UpdateTerminologyDialog = ({ entry, closeDialog }) => {
  const [, dispatch] = useOverviewModel();
  const {
    link,
    file,
    isLink,
    setIsLink,
    handleLinkChange,
    handleFileChange,
    buttonEnabled,
    handleTabChange,
    linkIsValid,
  } = useLinkOrFile(); // the one to be created by the import
  const { tasks, updateTask, allTasksDone, resetTasks } = useTasksList([
    ...TASKS_UPDATE_TERMINOLOGY,
  ]);
  const {
    open: isMainDialogOpen,
    openMainDialog,
    setDialogContent,
    closeMainDialog,
    setDialogProps,
  } = useMainDialog();
  const nlsBundles = [esteSchemeNLS, esteTerminologyNLS, esteImportNLS];
  const t = useTranslation(nlsBundles);
  const hasAnyTaskFailed = tasks.some((task) => task.status === STATUS_FAILED);

  const closeDialogs = useCallback(() => {
    closeMainDialog();
    closeDialog();
  }, [closeMainDialog, closeDialog]);

  useEffect(() => {
    setDialogContent({
      title: t('updateTerminology'),
      content: <ProgressList tasks={tasks} nlsBundles={[esteImportNLS]} />,
      actions: (
        <AcknowledgeAction
          onDone={closeDialogs}
          isDisabled={!allTasksDone && !hasAnyTaskFailed}
        />
      ),
    });

    if (hasAnyTaskFailed || allTasksDone)
      setDialogProps({
        ignoreBackdropClick: false,
        disableEscapeKeyDown: false,
      });

    if (allTasksDone) dispatch({ type: REFRESH });
  }, [
    tasks,
    t,
    allTasksDone,
    closeDialogs,
    setDialogContent,
    setDialogProps,
    hasAnyTaskFailed,
    dispatch,
  ]);

  useEffect(() => {
    if (!isMainDialogOpen && hasAnyTaskFailed) {
      resetTasks();
    }
  }, [hasAnyTaskFailed, isMainDialogOpen, resetTasks, tasks]);

  const skosLoad = useSkosLoad({ t, updateTask, isLink, link, file });
  const skosUpdate = useSkosUpdate({ t, updateTask });

  const process = async () => {
    openMainDialog({
      title: t('updateTerminology'),
      content: <ProgressList tasks={tasks} nlsBundles={nlsBundles} />,
      actions: null,
      dialogProps: {
        ignoreBackdropClick: true,
        disableEscapeKeyDown: true,
      },
    });
    const data = await skosLoad();
    if (data) {
      await skosUpdate(data, entry);
      updateContextTitle(entry);
    }
  };

  const actions = (
    <Button onClick={process} disabled={!buttonEnabled}>
      {t('updateButtonLabel')}
    </Button>
  );
  return (
    <ListActionDialog
      id="update-terminology"
      closeDialog={closeDialog}
      title={t('updateTerminology')}
      actions={actions}
      maxWidth="md"
    >
      <ContentWrapper>
        <EntryImport
          link={link}
          file={file}
          handleLinkChange={handleLinkChange}
          setIsLink={setIsLink}
          handleFileChange={handleFileChange}
          validURI={linkIsValid}
          handleTabChange={handleTabChange}
        />
      </ContentWrapper>
    </ListActionDialog>
  );
};

UpdateTerminologyDialog.propTypes = {
  closeDialog: PropTypes.func,
  entry: PropTypes.instanceOf(Entry),
};
export default UpdateTerminologyDialog;
