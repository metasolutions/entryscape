import { useEntry } from 'commons/hooks/useEntry';
import { getLabel } from 'commons/util/rdfUtils';
import { getViewDefFromName } from 'commons/util/site';
import escoListNLS from 'commons/nls/escoList.nls';
import { useTranslation } from 'commons/hooks/useTranslation';
import useDocumentTitle from 'commons/hooks/useDocumentTitle';
import usePageTitle from 'commons/hooks/usePageTitle';
import useGetContributors from 'commons/hooks/useGetContributors';
import escoOverviewNLS from 'commons/nls/escoOverview.nls';
import esadContextNLS from 'admin/nls/esadContext.nls';
import esadProjectTypeNLS from 'admin/nls/esadProjectType.nls';
import esadTypeNLS from 'admin/nls/esadType.nls';
import {
  ACTION_INFO_WITH_ICON,
  DESCRIPTION_UPDATED,
} from 'commons/components/overview/actions';
import Overview from 'commons/components/overview/Overview';
import {
  useOverviewModel,
  withOverviewModelProvider,
} from 'commons/components/overview';
import FieldsLinkedDataBrowserDialog from 'commons/components/FieldsLinkedDataBrowserDialog';
import { ListViewContext } from 'commons/components/ListView/hooks/useListView';
import { localize } from 'commons/locale';
import { sidebarActions } from './actions';
import { fieldPresenters } from '../utils/projectTypePresenters';

const nlsBundles = [
  escoListNLS,
  escoOverviewNLS,
  esadContextNLS,
  esadProjectTypeNLS,
  esadTypeNLS,
];

const ProjectTypeOverview = () => {
  const entry = useEntry();
  const t = useTranslation(nlsBundles);
  const [{ refreshCount }] = useOverviewModel();
  const { contributors } = useGetContributors(entry, refreshCount);
  const [pageTitle] = usePageTitle();
  const viewDefinition = getViewDefFromName('admin__projecttypes__projecttype');
  const viewDefinitionTitle = localize(viewDefinition.title);

  const descriptionItems = [
    DESCRIPTION_UPDATED,
    {
      id: 'edited',
      labelNlsKey: 'editedByLabel',
      getValues: () => contributors,
    },
  ];

  useDocumentTitle(`${pageTitle} - ${viewDefinitionTitle} ${getLabel(entry)}`);

  return (
    <ListViewContext.Provider value={{ nlsBundles }}>
      <Overview
        backLabel={t('backTitle')}
        headerAction={{
          ...ACTION_INFO_WITH_ICON,
          Dialog: FieldsLinkedDataBrowserDialog,
          fields: fieldPresenters,
        }}
        entry={entry}
        nlsBundles={nlsBundles}
        descriptionItems={descriptionItems}
        sidebarActions={sidebarActions}
        returnTo="/admin/projecttypes"
      />
    </ListViewContext.Provider>
  );
};

export default withOverviewModelProvider(ProjectTypeOverview);
