import PropTypes from 'prop-types';
import { useMemo } from 'react';
import { useTranslation } from 'commons/hooks/useTranslation';
import esmoFieldNLS from 'models/nls/esmoFields.nls';
import esmoCommonsNLS from 'models/nls/esmoCommons.nls';
import escoDialogsNLS from 'commons/nls/escoDialogs.nls';
import { useESContext } from 'commons/hooks/useESContext';
import escoListNLS from 'commons/nls/escoList.nls';
import {
  FIELD_TITLE,
  FIELD_PURPOSE,
  FIELD_TYPE,
  FIELD_INLINE,
  FIELD_NODETYPE,
  FIELD_PROPERTY,
  FIELD_DATATYPE,
} from 'models/utils/fieldDefinitions';
import CreateDialog from 'models/components/CreateDialog';
import { EditorProvider } from 'commons/components/forms/editors';
import * as ns from 'models/utils/ns';
import { copyTitleToLabel } from 'models/utils/metadata';
import { useSnackbar } from 'commons/hooks/useSnackbar';

const NLS_BUNDLES = [escoListNLS, esmoFieldNLS, esmoCommonsNLS, escoDialogsNLS];

/**
 * Adds default field metadata before commit
 *
 * @param {Graph} graph
 * @param {string} resourceURI
 * @returns {Graph}
 */
const addFieldMetadata = (graph, resourceURI) => {
  graph.add(resourceURI, 'rdf:type', ns.RDF_TYPE_FIELD);

  graph.add(
    resourceURI,
    ns.RDF_PROPERTY_STATUS,
    ns.RDF_PROPERTY_STATUS_UNSTABLE
  );

  copyTitleToLabel(graph, resourceURI);

  if (graph.find(resourceURI, 'rdf:type', ns.RDF_TYPE_DATATYPE_FIELD).length) {
    graph.add(
      resourceURI,
      ns.RDF_PROPERTY_NODETYPE,
      ns.RDF_PROPERTY_DATATYPE_LITERAL
    );
  }

  if (graph.find(resourceURI, 'rdf:type', ns.RDF_TYPE_LOOKUP_FIELD).length) {
    graph.add(resourceURI, ns.RDF_PROPERTY_NODETYPE, ns.RDF_PROPERTY_URI);
  }

  graph.addD(resourceURI, ns.RDF_PROPERTY_MIN, '0', 'xsd:integer');

  return graph;
};

const CreateFieldDialog = ({ closeDialog }) => {
  const translate = useTranslation(NLS_BUNDLES);
  const { context } = useESContext();
  const [addSnackbar] = useSnackbar();

  const prototypeEntry = useMemo(() => {
    return context.newEntry();
  }, [context]);

  return (
    <EditorProvider entry={prototypeEntry}>
      <CreateDialog
        title={translate('createFieldTitle')}
        closeDialog={closeDialog}
        fields={[
          { ...FIELD_TYPE },
          {
            ...FIELD_INLINE,
            dialogProps: {
              ...FIELD_INLINE.dialogProps,
              placeholderProps: {
                label: translate('emptyObjectFormListWarning'),
              },
            },
          },
          { ...FIELD_DATATYPE },
          { ...FIELD_TITLE },
          { ...FIELD_PURPOSE },
          { ...FIELD_NODETYPE },
          { ...FIELD_PROPERTY },
        ]}
        beforeCreate={addFieldMetadata}
        nlsBundles={NLS_BUNDLES}
        onEditEntry={() => addSnackbar({ message: translate('createSuccess') })}
      />
    </EditorProvider>
  );
};

CreateFieldDialog.propTypes = {
  closeDialog: PropTypes.func.isRequired,
};

export default CreateFieldDialog;
