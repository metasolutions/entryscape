import { useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { entrystore } from 'commons/store';
import { hasAdminRights, isAdmin } from 'commons/util/user';
import { getGroupWithHomeContext } from 'commons/util/store';
import useAsync from 'commons/hooks/useAsync';

export const GUEST_PERMISSION = 'guest';
export const ADMIN_PERMISSION = 'admin';
export const ADV_ADMIN_PERMISSION = 'advancedAdmin';

const useUserPermission = (userEntry) => {
  // Should eventually wrap the ViewWrapper component with the context provider instead.
  // That way we would be able to access the context through the hook anywhere.
  const { contextId } = useParams();
  const { data: groupEntry, runAsync } = useAsync(null);
  const hasUserAdminRights = hasAdminRights(userEntry);
  const canAdminister = hasUserAdminRights || groupEntry?.canAdministerEntry();

  useEffect(() => {
    const context = contextId && entrystore.getContextById(contextId);

    if (!context || hasUserAdminRights) return;

    runAsync(getGroupWithHomeContext(context));
  }, [hasUserAdminRights, contextId, runAsync]);

  if (isAdmin(userEntry)) return ADV_ADMIN_PERMISSION;
  if (canAdminister) return ADMIN_PERMISSION;
  return GUEST_PERMISSION;
};

export default useUserPermission;
