import PropTypes from 'prop-types';
import { useCallback } from 'react';
import { localizeLabel } from 'commons/locale';
import { useEditorContext } from '../EditorContext';
import { getEditorComponent } from '../utils/editor';

const defaultOnChange = () => {};

export const FieldEditor = ({
  Editor,
  isVisible = () => true,
  blankNodeId,
  onChange = defaultOnChange,
  label,
  labelNlsKey,
  translate,
  ...editorProps
}) => {
  const { graph, resourceURI, dispatch } = useEditorContext();
  const EditorComponent =
    Editor || getEditorComponent(editorProps.nodetype, editorProps.datatype);

  const handleDispatch = useCallback(
    (action) => {
      dispatch(action);
      onChange(action);
    },
    [dispatch, onChange]
  );

  if (!graph) return null;
  if (!isVisible(graph, resourceURI) || !EditorComponent) return null;

  return (
    <EditorComponent
      key={editorProps.property}
      graph={graph}
      resourceURI={blankNodeId || resourceURI}
      {...editorProps}
      translate={translate}
      label={localizeLabel(label) || translate(labelNlsKey)}
      dispatch={handleDispatch}
    />
  );
};

FieldEditor.propTypes = {
  label: PropTypes.oneOfType([PropTypes.string, PropTypes.shape({})]),
  Editor: PropTypes.func,
  isVisible: PropTypes.func,
  blankNodeId: PropTypes.string,
  onChange: PropTypes.func,
  labelNlsKey: PropTypes.string,
  translate: PropTypes.func,
};
