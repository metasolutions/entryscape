import { Group } from '@entryscape/entrystore-js';
import {
  LEVEL_MANDATORY,
  LEVEL_OPTIONAL,
  LEVEL_RECOMMENDED,
} from '../../commons/components/rdforms/LevelSelector/LevelSelector';

/**
 * @param {Array} disabledLevels
 * @returns {string}
 */
export const getLevelFromDisabledLevels = (disabledLevels) => {
  if (
    disabledLevels.includes(LEVEL_RECOMMENDED) &&
    disabledLevels.includes(LEVEL_OPTIONAL)
  )
    return LEVEL_MANDATORY;
  if (
    disabledLevels.includes(LEVEL_OPTIONAL) &&
    !disabledLevels.includes(LEVEL_RECOMMENDED)
  )
    return LEVEL_RECOMMENDED;
  return LEVEL_OPTIONAL;
};

/**
 *
 * @param {object|undefined} cardinality
 * @returns {string}
 */
export const getLevelFromCardinality = (cardinality) => {
  if (!cardinality || cardinality.min === 0 || cardinality.pref === 0)
    return LEVEL_OPTIONAL;
  if (cardinality.min === 1) return LEVEL_MANDATORY;
  if (cardinality.pref === 1) return LEVEL_RECOMMENDED;
  return LEVEL_OPTIONAL;
};

/**
 *
 * @param {Group} template
 * @returns {Array | undefined}
 */
export const getLevels = (template) => {
  const children = template?.getChildren();
  const levels = children?.map((item) => {
    const { cardinality } = item._source;
    return getLevelFromCardinality(cardinality);
  });
  return levels;
};
