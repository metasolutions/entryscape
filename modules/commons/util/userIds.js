// move to @entryscape/entrysotre.js
export const USER_ENTRY_ID_GUEST = '_guest';
export const USER_ENTRY_ID_ADMIN = '_admin';
export const USER_ENTRY_ID_ADMINS = '_admins';
export const USERS_ENTRY_ID = '_users';
export const CONTEXT_ENTRY_ID_PRINCIPALS = '_principals';
export const CONTEXT_CONTEXTS = '_contexts';
export const CONTEXT_PRINCIPALS = '_principals';
