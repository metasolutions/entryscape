import { useEffect } from 'react';
import PropTypes from 'prop-types';
import Button from '@mui/material/Button';
import ContentWrapper from 'commons/components/common/ContentWrapper';
import ListActionDialog from 'commons/components/common/dialogs/ListActionDialog';
import useAsync from 'commons/hooks/useAsync';
import { entryPropType } from 'commons/util/entry';
import {
  useTranslation,
  nlsBundlesPropType,
} from 'commons/hooks/useTranslation';

const DefaultNamespaceDialog = ({
  closeDialog,
  entry,
  currentDefaultEntry,
  nlsBundles,
  onConfirm,
}) => {
  const { error, runAsync } = useAsync();
  const translate = useTranslation(nlsBundles);

  const unsetDefaultAction = currentDefaultEntry === entry;

  const removeCurrentDefault = () => {
    const currentDefaultEntryGraph = currentDefaultEntry.getMetadata();
    currentDefaultEntryGraph.findAndRemove(null, 'esterms:default');
    currentDefaultEntry.setMetadata(currentDefaultEntryGraph);
  };

  const setDefaultNamespace = async () => {
    if (currentDefaultEntry) {
      removeCurrentDefault();
      await currentDefaultEntry.commitMetadata();
    }
    if (unsetDefaultAction) return;

    const graph = entry.getMetadata();
    const resourceURI = entry.getResourceURI();
    graph.addD(resourceURI, 'esterms:default', 'true', 'xsd:boolean');
    entry.setMetadata(graph);
    return entry.commitMetadata();
  };

  const handleConfirm = () => {
    runAsync(
      setDefaultNamespace().then(() => {
        closeDialog();
        onConfirm();
      })
    );
  };

  useEffect(() => {
    if (error) {
      console.error(error);
      closeDialog();
    }
  }, [error, closeDialog]);

  const actions = (
    <Button autoFocus onClick={handleConfirm}>
      {unsetDefaultAction
        ? translate('unsetDefaultLabel')
        : translate('setDefaultLabel')}
    </Button>
  );
  return (
    <ListActionDialog
      id="set-default-entry"
      closeDialog={closeDialog}
      actions={actions}
      maxWidth="xs"
    >
      <ContentWrapper xs={10}>
        {unsetDefaultAction
          ? translate('confirmUnsetDefaultMessage')
          : translate('confirmSetDefaultMessage')}
      </ContentWrapper>
    </ListActionDialog>
  );
};

DefaultNamespaceDialog.propTypes = {
  closeDialog: PropTypes.func,
  onConfirm: PropTypes.func,
  entry: entryPropType,
  currentDefaultEntry: entryPropType,
  nlsBundles: nlsBundlesPropType,
};

export default DefaultNamespaceDialog;
