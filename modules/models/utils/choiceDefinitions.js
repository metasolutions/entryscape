import * as ns from 'models/utils/ns';
import {
  NAMESPACE_ENTRY,
  RESOURCE,
  RESOURCE_ENTRY,
} from 'models/utils/metadata';

export const FIELD_STATUS_CHOICES = [
  {
    labelNlsKey: 'unstableLabel',
    value: ns.RDF_PROPERTY_STATUS_UNSTABLE,
  },
  {
    labelNlsKey: 'stableLabel',
    value: ns.RDF_PROPERTY_STATUS_STABLE,
  },
  {
    labelNlsKey: 'deprecatedLabel',
    value: ns.RDF_PROPERTY_STATUS_DEPRECATED,
  },
];

export const TEXT_FIELD_CHOICE = {
  labelNlsKey: 'textFieldLabel',
  value: ns.RDF_TYPE_TEXT_FIELD,
};
export const DATATYPE_FIELD_CHOICE = {
  labelNlsKey: 'dataTypeLabel',
  value: ns.RDF_TYPE_DATATYPE_FIELD,
};
export const SELECT_FIELD_CHOICE = {
  labelNlsKey: 'selectFieldLabel',
  value: ns.RDF_TYPE_SELECT_FIELD,
};
export const LOOKUP_FIELD_CHOICE = {
  labelNlsKey: 'lookupFieldLabel',
  value: ns.RDF_TYPE_LOOKUP_FIELD,
};
export const INLINE_FIELD_CHOICE = {
  labelNlsKey: 'inlineFieldLabel',
  value: ns.RDF_TYPE_INLINE_FIELD,
};

export const FIELD_TYPE_CHOICES = [
  TEXT_FIELD_CHOICE,
  DATATYPE_FIELD_CHOICE,
  SELECT_FIELD_CHOICE,
  LOOKUP_FIELD_CHOICE,
  INLINE_FIELD_CHOICE,
];

export const PROFILE_FORM_CHOICE = {
  labelNlsKey: 'profileFormLabel',
  value: ns.RDF_TYPE_PROFILE_FORM,
};
export const SECTION_FORM_CHOICE = {
  labelNlsKey: 'sectionFormLabel',
  value: ns.RDF_TYPE_SECTION_FORM,
};
export const OBJECT_FORM_CHOICE = {
  labelNlsKey: 'objectFormLabel',
  value: ns.RDF_TYPE_OBJECT_FORM,
};
// deprecated
export const PROPERTY_FORM_CHOICE = {
  labelNlsKey: 'propertyFormLabel',
  value: ns.RDF_TYPE_PROPERTY_FORM,
};
export const FORM_TYPE_CHOICES = [
  PROFILE_FORM_CHOICE,
  SECTION_FORM_CHOICE,
  OBJECT_FORM_CHOICE,
];

export const FIELD_NODETYPE_CHOICES = [
  {
    value: ns.RDF_PROPERTY_LITERAL,
    label: { en: 'Literal' },
    constrainedTo: [ns.RDF_TYPE_TEXT_FIELD, ns.RDF_TYPE_SELECT_FIELD],
  },
  {
    value: ns.RDF_PROPERTY_ONLY_LITERAL,
    label: { en: 'Only literal' },
    constrainedTo: [ns.RDF_TYPE_TEXT_FIELD, ns.RDF_TYPE_SELECT_FIELD],
  },
  {
    value: ns.RDF_PROPERTY_LANGUAGE_LITERAL,
    label: { en: 'Language literal' },
    constrainedTo: [ns.RDF_TYPE_TEXT_FIELD, ns.RDF_TYPE_SELECT_FIELD],
  },
  {
    value: ns.RDF_PROPERTY_URI,
    label: { en: 'URI' },
    constrainedTo: [
      ns.RDF_TYPE_TEXT_FIELD,
      ns.RDF_TYPE_LOOKUP_FIELD,
      ns.RDF_TYPE_SELECT_FIELD,
    ],
  },
];

export const FORM_NODETYPE_CHOICES = [
  {
    label: 'URI',
    value: ns.RDF_PROPERTY_URI,
  },
  {
    label: 'Blank',
    value: ns.RDF_PROPERTY_BLANK,
  },
  {
    label: 'Resource',
    value: ns.RDF_PROPERTY_RESOURCE,
    constrainedTo: [ns.RDF_TYPE_OBJECT_FORM, ns.RDF_TYPE_PROFILE_FORM],
  },
];

export const PROPERTY_EDIT_MODE_CHOICES = [
  {
    labelNlsKey: 'selectPropertyLabel',
    value: RESOURCE_ENTRY,
  },
  {
    labelNlsKey: 'namespacePropertyLabel',
    value: NAMESPACE_ENTRY,
  },
  {
    labelNlsKey: 'manualPropertyLabel',
    value: RESOURCE,
  },
];

export const SELECT_OPTIONS_VALUE_CHOICES = [
  {
    labelNlsKey: 'namespacePropertyLabel',
    value: NAMESPACE_ENTRY,
  },
  {
    labelNlsKey: 'manualPropertyLabel',
    value: RESOURCE,
  },
];

export const CONSTRAINT_VALUE_EDIT_CHOICES = [
  {
    labelNlsKey: 'namespacePropertyLabel',
    value: NAMESPACE_ENTRY,
  },
  {
    labelNlsKey: 'manualPropertyLabel',
    value: RESOURCE,
  },
  {
    labelNlsKey: 'selectClassLabel',
    value: RESOURCE_ENTRY,
  },
];
