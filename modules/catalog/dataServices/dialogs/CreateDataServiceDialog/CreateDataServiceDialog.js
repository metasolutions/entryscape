import PropTypes from 'prop-types';
import { useEffect } from 'react';
import { useESContext } from 'commons/hooks/useESContext';
import useAsync from 'commons/hooks/useAsync';
import { commitMetadata, RDF_TYPE_DATASERVICE } from 'commons/util/entry';
import { createPrototypeDataServiceEntry } from 'catalog/dataServices/utils';
import { updateCatalogWithDataService } from 'catalog/datasets/DatasetOverview/utils/dataServices';
import { LEVEL_MANDATORY } from 'commons/components/rdforms/LevelSelector';
// eslint-disable-next-line max-len
import CreateEntryByTemplateDialog from 'commons/components/EntryListView/dialogs/create/CreateEntryByTemplateDialog';
import useHandleError from 'commons/errors/hooks/useErrorHandler';
import { useTranslation } from 'commons/hooks/useTranslation';
import escaServicesNLS from 'catalog/nls/escaServices.nls';
import { useSnackbar } from 'commons/hooks/useSnackbar';

const CreateDataServiceDialog = ({ onCreate, ...props }) => {
  const translate = useTranslation(escaServicesNLS);

  const { context } = useESContext();
  const { data: prototypeEntry, runAsync, error } = useAsync(null);
  const [addSnackbar] = useSnackbar();

  useHandleError(error);

  useEffect(() => {
    runAsync(createPrototypeDataServiceEntry(context, RDF_TYPE_DATASERVICE));
  }, [context, runAsync]);

  const handleCreateEntry = async (dataServicePrototypeEntry, graph) => {
    const dataServiceEntry = await commitMetadata(
      dataServicePrototypeEntry,
      graph
    );
    await updateCatalogWithDataService(dataServiceEntry);
    onCreate();
    addSnackbar({ message: translate('createSuccessMessage') });
    return dataServiceEntry;
  };

  return prototypeEntry ? (
    <CreateEntryByTemplateDialog
      // eslint-disable-next-line react/jsx-props-no-spreading
      {...props}
      entry={prototypeEntry}
      createEntry={handleCreateEntry}
      editorLevel={LEVEL_MANDATORY}
      createHeaderLabel={translate('createHeader')}
    />
  ) : null;
};

CreateDataServiceDialog.propTypes = {
  onCreate: PropTypes.func,
};

export default CreateDataServiceDialog;
