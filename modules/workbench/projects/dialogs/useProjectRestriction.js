import config from 'config';
import { useUserState } from 'commons/hooks/useUser';
import { hasUserPermission, ADMIN_RIGHTS, PREMIUM } from 'commons/util/user';

const useProjectRestriction = (numberOfProjects) => {
  const { userEntry } = useUserState();
  const isExemptFromRestrictions = hasUserPermission(userEntry, [
    PREMIUM,
    ADMIN_RIGHTS,
  ]);
  if (isExemptFromRestrictions) return { isRestricted: false };

  const projectLimit = config.get('workbench.projectLimit');
  const pathToProjectLimitContent = config.get('workbench.projectLimitDialog');
  const isRestricted =
    (projectLimit && numberOfProjects >= projectLimit) || projectLimit === 0;

  return { isRestricted, contentPath: pathToProjectLimitContent };
};

export default useProjectRestriction;
