import { hasAdminRights } from 'commons/util/user';
import { getUniqueId } from 'commons/hooks/useUniqueId';
import config from 'config';

export const getTransformOptions = () =>
  config
    .get('registry.transforms')
    .map(({ name, ...props }) => ({ name, type: name, ...props }));

export const labelOverrides = {
  masterType: 'primary types',
  slaveTypes: 'supportive types',
};

export const CHANGE_TITLE = 'change-title';
export const CHANGE_DESCRIPTION = 'change-description';
export const CHANGE_PROJECT_TYPE = 'change-project-type';
export const CHANGE_USERNAME = 'change-username';
export const CHANGE_PSI = 'change-psi';
export const CHANGE_ORG_ID = 'change-org-id';
export const CHANGE_SOURCE = 'change-source';
export const CHANGE_PUBLIC = 'change-public';
export const ADD_TRANSFORM = 'add-transform';
export const REMOVE_TRANSFORM = 'remove-transform';
export const CHANGE_TRANSFORM_TYPE = 'change-transform-type';
export const UPDATE_TRANSFORM_ARGUMENT = 'update-transform-argument';

export const initialState = {
  title: '',
  description: '',
  username: '',
  psi: '',
  orgId: '',
  sourceUrl: '',
  isPublic: false,
  projectType: '',
  transforms: [],
  errors: {},
};

export const reducer = (state, action) => {
  switch (action.type) {
    case CHANGE_PROJECT_TYPE:
      return { ...state, projectType: action.projectType };
    case CHANGE_TITLE:
      return {
        ...state,
        title: action.title,
        errors: { ...state.errors, ...action.errors },
      };
    case CHANGE_DESCRIPTION:
      return { ...state, description: action.description };
    case CHANGE_USERNAME:
      return {
        ...state,
        username: action.username,
        errors: { ...state.errors, ...action.errors },
      };
    case CHANGE_PSI:
      return {
        ...state,
        psi: action.psi,
        errors: { ...state.errors, ...action.errors },
      };
    case CHANGE_ORG_ID:
      return {
        ...state,
        orgId: action.orgId,
        errors: { ...state.errors, ...action.errors },
      };
    case CHANGE_SOURCE:
      return {
        ...state,
        sourceUrl: action.sourceUrl,
        errors: { ...state.errors, ...action.errors },
      };
    case CHANGE_PUBLIC:
      if (!action.isPublic) {
        state.oldPsiFields = {
          psi: state.psi,
          orgId: state.orgId,
        };
        state.oldPsiErrors = {
          psi: state.errors.psi,
          orgId: state.errors.orgId,
        };
      }
      return {
        ...state,
        isPublic: action.isPublic,
        // Reinit psi fields when public is toggled off
        ...(!action.isPublic
          ? {
              psi: '',
              orgId: '',
            }
          : state.oldPsiFields || {}),
        errors: {
          ...state.errors,
          ...(!action.isPublic
            ? { ...state.errors, psi: '', orgId: '' }
            : state.oldPsiErrors || {}),
        },
      };
    case ADD_TRANSFORM:
      return {
        ...state,
        transforms: [
          ...(state.transforms || []),
          { type: '', id: getUniqueId('transform') },
        ],
      };
    case REMOVE_TRANSFORM:
      return {
        ...state,
        transforms: state.transforms.filter(
          (transform) => transform.id !== action.transformId
        ),
      };
    case CHANGE_TRANSFORM_TYPE: {
      const updatedTransform = {
        id: action.transformId,
        ...getTransformOptions().find(
          (transform) => transform.type === action.newType
        ),
      };
      return {
        ...state,
        transforms: state.transforms.map((transform) =>
          transform.id === action.transformId ? updatedTransform : transform
        ),
      };
    }
    case UPDATE_TRANSFORM_ARGUMENT: {
      return {
        ...state,
        transforms: state.transforms.map((transform) =>
          transform.id === action.transformId
            ? {
                ...transform,
                args: { ...transform.args, [action.key]: action.newValue },
              }
            : transform
        ),
      };
    }
    default:
      throw new Error(`${action.type} is not a valid action type`);
  }
};

const getRequiredFields = (userEntry, isPublic, isDcat) => {
  const requiredFields = ['title'];
  if (isDcat) requiredFields.push('sourceUrl');
  if (isPublic) requiredFields.push('psi', 'orgId');
  if (hasAdminRights(userEntry)) requiredFields.push('username');
  return requiredFields;
};

export const validateForm = (data, userEntry, isDcat) => {
  const hasErrors = Object.values(data.errors).some((value) => value);
  const requiredFields = getRequiredFields(userEntry, data.isPublic, isDcat);
  const requiredFieldsFilled = Object.keys(data)
    .filter((dataField) => requiredFields.includes(dataField))
    .every((dataValue) => data[dataValue]);

  return !hasErrors && requiredFieldsFilled;
};
