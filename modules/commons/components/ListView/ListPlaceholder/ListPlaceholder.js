import PropTypes from 'prop-types';
import { Typography } from '@mui/material';
import EmptyListIcon from '@mui/icons-material/List';
import { useTranslation } from 'commons/hooks/useTranslation';
import Placeholder from 'commons/components/common/Placeholder';
import { useListView } from '../hooks/useListView';
import './ListPlaceholder.scss';

export const ListPlaceholder = ({
  Icon = EmptyListIcon,
  label,
  children,
  className = '',
  variant = 'view',
}) => {
  const { nlsBundles } = useListView();
  const t = useTranslation(nlsBundles);

  return (
    <Placeholder
      variant={variant}
      label={
        <Typography className="escoListViewPlaceholder__text" variant="body1">
          {label || t('emptyListWarning')}
        </Typography>
      }
      icon={<Icon classes={{ root: 'escoListViewPlaceholder__icon' }} />}
      className={className}
    >
      {children}
    </Placeholder>
  );
};

ListPlaceholder.propTypes = {
  Icon: PropTypes.elementType,
  label: PropTypes.string,
  className: PropTypes.string,
  children: PropTypes.node,
  variant: PropTypes.oneOf(['compact', 'dialog', 'list']),
};
