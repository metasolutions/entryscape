import { useEffect, useRef } from 'react';
import useDebounce from 'commons/hooks/useDebounce';
import useAsync from 'commons/hooks/useAsync';
import sleep from 'commons/util/sleep';

const initialState = {
  entries: [],
  size: -1,
  searchQueryString: '',
};

export const SOLR_DELAY = 1500;

/**
 * Hook that runs provided fetchEntries function.
 *
 * @param {{fetchEntries: Function, delay: number, refreshCount: number, wait: boolean}} options
 * @returns {object}
 */
export const useFetchEntries = ({
  fetchEntries,
  delay = 300,
  refreshCount,
  wait = false,
}) => {
  const {
    data: entriesResults,
    status,
    error,
    runAsync,
  } = useAsync({
    data: initialState,
  });

  const debouncedFetchEntries = useDebounce(
    (callback) => runAsync(callback()),
    delay
  );
  const initialFetch = useRef(true);
  const previousRefreshCount = useRef(0);

  useEffect(() => {
    if (wait) return;
    /* 
      To trigger refresh, refreshCount is increased after refresh,
      create or delete.
      Entries will be fetched using a delay because of solr reindexing. Since
      the delay is for a single fetch and with a specific delay, debounce is not
      used.     
    */
    if (refreshCount !== previousRefreshCount.current) {
      previousRefreshCount.current = refreshCount;

      runAsync(
        (async () => {
          await sleep(SOLR_DELAY);
          return fetchEntries();
        })()
      );
      return;
    }

    /*
      Initial fetch should not have a delay or use debounce, since it's a single
      fetch and no typing is being made at this point.
     */
    if (initialFetch.current) {
      runAsync(fetchEntries());
      initialFetch.current = false;
      return;
    }
    /*
      Debounce queries to prevent multiple queries while typing during search. 
     */
    debouncedFetchEntries(fetchEntries);
  }, [fetchEntries, debouncedFetchEntries, refreshCount, runAsync, wait]);

  return { ...entriesResults, status, error };
};
