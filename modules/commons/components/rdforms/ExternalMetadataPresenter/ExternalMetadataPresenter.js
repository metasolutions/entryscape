import PropTypes from 'prop-types';
import { Entry } from '@entryscape/entrystore-js';
import useRDFormsPresenter from 'commons/components/rdforms/hooks/useRDFormsPresenter';
import escoPresentExpandableNLS from 'commons/nls/escoPresentExpandable.nls';
import ExpandButton from 'commons/components/buttons/ExpandButton';

const ExternalMetadataPresenter = ({ entry, externalTemplateId, ...rest }) => {
  const initiallyExpanded = entry.getMetadata().isEmpty();
  const externalMetadata = entry.getCachedExternalMetadata();
  const resourceURI = entry.getResourceURI();
  const { PresenterComponent } = useRDFormsPresenter(
    externalMetadata,
    resourceURI,
    externalTemplateId
  );

  return (
    <ExpandButton
      initiallyExpanded={initiallyExpanded}
      PresenterComponent={PresenterComponent}
      nlsBundles={[escoPresentExpandableNLS]}
      {...rest}
    />
  );
};

ExternalMetadataPresenter.propTypes = {
  entry: PropTypes.instanceOf(Entry),
  externalTemplateId: PropTypes.string,
};

export default ExternalMetadataPresenter;
