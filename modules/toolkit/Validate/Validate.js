import { useState, useEffect, useMemo } from 'react';
import { isEmpty } from 'lodash-es';
import useNavigate from 'commons/components/router/useNavigate';
import { CircularProgress, Typography } from '@mui/material';
import { EntryInfo } from '@entryscape/entrystore-js';
import { useClipboardGraph } from 'commons/contexts/ClipboardGraphContext';
import esreReportNLS from 'toolkit/nls/esreReport.nls';
import esreSourceNLS from 'toolkit/nls/esreSource.nls';
import AlertDialog from 'commons/components/common/dialogs/AlertDialog';
import { useTranslation } from 'commons/hooks/useTranslation';
import { validate } from '@entryscape/rdforms';
import { itemStore } from 'commons/rdforms/itemstore';
import { namespaces as ns } from '@entryscape/rdfjson';
import ExpandableListItem from 'commons/components/common/ExpandableListItem';
import withLocationChangeListener from 'toolkit/LocationChangeListener';
import ClassReport, {
  ClassReportHeaderSecondary,
} from 'toolkit/Validate/ClassReport';
import config from 'config';
import { useUserState } from 'commons/hooks/useUser';
import Lookup from 'commons/types/Lookup';
import { filterOnRestrictions } from 'commons/types/utils/restrictions';
import { localize } from 'commons/locale';
import './Validate.scss';

const sortByLabel = (entityTypes) => {
  return [...entityTypes].sort((a, b) => {
    const labelA = localize(a.get('label')).toLowerCase();
    const labelB = localize(b.get('label')).toLowerCase();
    if (labelA < labelB) {
      return -1;
    }
    if (labelA > labelB) {
      return 1;
    }
    return 0; // if names are equal
  });
};

/**
 * Gets available entitypes for a given project type. If no project type is
 * used, available entity types will be used instead.
 *
 * @param {string} projectTypeId
 * @param {object} userInfo
 * @returns {Promise<object[]>}
 */
const getAvailableEntityTypes = (projectTypeId, userInfo) => {
  const entityTypesFromProjectType = Lookup.getProjectType(projectTypeId)
    ?.getPrimary()
    .map((enityTypeId) => Lookup.get(enityTypeId));
  const availableEntityTypes =
    entityTypesFromProjectType || Lookup.getEntityTypes();

  return sortByLabel(
    filterOnRestrictions(availableEntityTypes, userInfo, 'register', false)
  );
};

/**
 *  Get the mandatory validation types from config.
 *
 * @returns {string[]}
 */
const getMandatoryTypes = () =>
  config
    .get('registry.mandatoryValidationTypes')
    .map((mandatoryType) => ns.expand(mandatoryType));

/**
 *
 * @param {EntryInfo} userInfo
 * @returns {object} A map having the types as keys and their templates as values.
 */
const getType2Template = (userInfo) => {
  const type2Template = {};
  const entityTypes = getAvailableEntityTypes(null, userInfo);

  entityTypes.forEach((entityType) => {
    entityType.getRdfType().forEach((rdfType) => {
      const templateId = entityType.get('template');
      if (!templateId) return;
      const template = itemStore.getItem(templateId);
      if (!template) return;
      type2Template[ns.expand(rdfType)] = template;
    });
  });

  return type2Template;
};

const regroup = (objects, prop) => {
  const group = {};
  objects.forEach((obj) => {
    group[obj[prop]] = group[obj[prop]] || [];
    group[obj[prop]].push(obj);
  });
  return group;
};

const NLS_BUNDLES = [esreReportNLS, esreSourceNLS];

const Validate = () => {
  const [showAlert, setShowAlert] = useState(false);
  const [report, setReport] = useState(null);
  const { navigate } = useNavigate();
  const translate = useTranslation(NLS_BUNDLES);
  const [clipboardGraph] = useClipboardGraph();
  const { userInfo } = useUserState();
  const type2Template = useMemo(() => getType2Template(userInfo), [userInfo]);

  useEffect(() => {
    if (!clipboardGraph || clipboardGraph.isEmpty()) {
      setShowAlert(true);
      return;
    }

    const validationReport = validate.graphReport(
      clipboardGraph,
      type2Template,
      getMandatoryTypes()
    );

    const groupedReportResources = validationReport?.resources
      ? regroup(validationReport.resources, 'type')
      : null;

    setReport(groupedReportResources);
  }, [clipboardGraph, type2Template]);

  const titleElement = (
    <Typography variant="h2" classes={{ root: 'esreValidate__header' }}>
      {translate('validationReport')}
    </Typography>
  );

  const hasSource = !showAlert;
  if (!hasSource)
    return (
      <>
        {titleElement}
        <AlertDialog
          open={showAlert}
          handleClose={() => {
            setShowAlert(false);
            navigate('/toolkit/catalog');
          }}
          buttonLabel={translate('noRDFProceed')}
        >
          {translate('noRDF')}
        </AlertDialog>
      </>
    );

  const reportHasResources = report && !isEmpty(report);
  if (!reportHasResources)
    console.error(
      'No resources found, check the configuration for any missing bundles and/or entity types.'
    );

  if (!report) {
    return (
      <>
        {titleElement}
        <div className="esreValidate__loading">
          <CircularProgress disableShrink />
        </div>
      </>
    );
  }

  return (
    <>
      {titleElement}
      {reportHasResources ? (
        Object.keys(report).map((type) => {
          const { localname: localNameType, pretty: prettyType } =
            ns.nsify(type);
          return (
            <ExpandableListItem
              primary={translate('instancesHeader', {
                nr: report[type].length,
                class: prettyType,
              })}
              secondary={
                <ClassReportHeaderSecondary resources={report[type]} />
              }
              key={type}
            >
              <ClassReport
                type={localNameType}
                graph={clipboardGraph}
                resources={report[type]}
                template={type2Template[type]}
              />
            </ExpandableListItem>
          );
        })
      ) : (
        <p>{translate('placeholder')}</p>
      )}

      <AlertDialog
        open={showAlert}
        handleClose={() => {
          setShowAlert(false);
          navigate('/toolkit/catalog');
        }}
        buttonLabel={translate('noRDFProceed')}
      >
        {translate('noRDF')}
      </AlertDialog>
    </>
  );
};

export default withLocationChangeListener(Validate);
