import * as ns from 'models/utils/ns';
import RfsForm from './RfsForm';

const OBJECT_FORM_PROPERTIES = [
  ns.RDF_PROPERTY_NODETYPE,
  ns.RDF_PROPERTY_CONSTRAINT,
  ns.RDF_PROPERTY_PATTERN,
];

const OBJECT_FORM_COMPATIBLE_TYPES = [ns.RDF_TYPE_PROFILE_FORM];

const OBJECT_FORM_NODETYPES = [
  ns.RDF_PROPERTY_RESOURCE,
  ns.RDF_PROPERTY_URI,
  ns.RDF_PROPERTY_BLANK,
];

class RfsObjectForm extends RfsForm {
  constructor(props) {
    super({
      ...props,
      rdfType: ns.RDF_TYPE_OBJECT_FORM,
      nlsKeyLabel: 'objectFormLabel',
      properties: OBJECT_FORM_PROPERTIES,
      compatibleTypes: OBJECT_FORM_COMPATIBLE_TYPES,
      nodetypes: OBJECT_FORM_NODETYPES,
    });
  }
}

export default RfsObjectForm;
