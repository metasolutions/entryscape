import { useState, useCallback, useEffect } from 'react';
import PropTypes from 'prop-types';
import esteImportNLS from 'terms/nls/esteImport.nls';
import esteSchemeNLS from 'terms/nls/esteScheme.nls';
import esteTerminologyNLS from 'terms/nls/esteTerminology.nls';
import { Button, FormGroup, FormControlLabel, Checkbox } from '@mui/material';
import ContentWrapper from 'commons/components/common/ContentWrapper';
import EntryImport from 'commons/components/entry/Import';
import useLinkOrFile from 'commons/components/entry/Import/useLinkOrFile';
import { useTranslation } from 'commons/hooks/useTranslation';
import ProgressList from 'commons/components/common/ProgressList';
import { useTasksList, STATUS_FAILED } from 'commons/hooks/useTasksList';
import { useMainDialog } from 'commons/hooks/useMainDialog';
import ConfirmationDialog from 'commons/components/common/dialogs/ConfirmationDialog';
import { AcknowledgeAction } from 'commons/components/common/dialogs/MainDialog';
import { updateContextTitle } from 'commons/util/context';
import { useUserState } from 'commons/hooks/useUser';
import ListActionDialog from 'commons/components/common/dialogs/ListActionDialog';
import { CREATE, useListModel } from 'commons/components/ListView';
import useSkosImport from 'terms/terminologies/dialogs/hooks/useSkosImport';
import useSkosLoad from 'terms/terminologies/dialogs/hooks/useSkosLoad';
import { TASKS_IMPORT_TERMINOLOGY } from '../tasks';

const ImportTerminologyDialog = ({ closeDialog }) => {
  const { userEntry, userInfo } = useUserState();
  const [, dispatch] = useListModel();
  const [conceptSchemeEntry, setConceptSchemeEntry] = useState(null);
  const [external, setExternal] = useState(false);
  const [showConfirmation, setShowConfirmation] = useState(false);
  const [confirmationProps, setConfirmationProps] = useState({});
  const {
    link,
    file,
    isLink,
    setIsLink,
    handleLinkChange,
    handleFileChange,
    buttonEnabled,
    handleTabChange,
    linkIsValid,
  } = useLinkOrFile(); // the one to be created by the import
  const { tasks, updateTask, allTasksDone, resetTasks } = useTasksList([
    ...TASKS_IMPORT_TERMINOLOGY,
  ]);
  const {
    open: isMainDialogOpen,
    openMainDialog,
    setDialogContent,
    closeMainDialog,
    setDialogProps,
  } = useMainDialog();
  const nlsBundles = [esteSchemeNLS, esteTerminologyNLS, esteImportNLS];
  const t = useTranslation(nlsBundles);
  const hasAnyTaskFailed = tasks.some((task) => task.status === STATUS_FAILED);

  const closeDialogs = useCallback(() => {
    closeMainDialog();
    closeDialog();

    // if all fine, more admin work for the newly scheme entry and notify the list
    if (allTasksDone) {
      updateContextTitle(conceptSchemeEntry).then(() => {
        userEntry.setRefreshNeeded();
        userEntry.refresh();
      });
    }
  }, [
    closeMainDialog,
    closeDialog,
    allTasksDone,
    conceptSchemeEntry,
    userEntry,
  ]);

  useEffect(() => {
    setDialogContent({
      title: t('importTerminology'),
      content: <ProgressList tasks={tasks} nlsBundles={[esteImportNLS]} />,
      actions: (
        <AcknowledgeAction
          onDone={closeDialogs}
          isDisabled={!allTasksDone && !hasAnyTaskFailed}
        />
      ),
    });

    if (hasAnyTaskFailed || allTasksDone)
      setDialogProps({
        ignoreBackdropClick: false,
        disableEscapeKeyDown: false,
      });

    if (allTasksDone) dispatch({ type: CREATE });
  }, [
    tasks,
    t,
    allTasksDone,
    closeDialogs,
    setDialogContent,
    setDialogProps,
    hasAnyTaskFailed,
    dispatch,
  ]);

  useEffect(() => {
    if (!isMainDialogOpen && hasAnyTaskFailed) {
      resetTasks();
    }
  }, [hasAnyTaskFailed, isMainDialogOpen, resetTasks, tasks]);

  const getConfirmationDialog = useCallback(
    () =>
      new Promise((resolve) => {
        setShowConfirmation(true);
        setConfirmationProps({
          onDone: (result) => resolve(result),
          onReject: (result) => {
            closeDialogs();
            resolve(result);
          },
        });
      }),
    [closeDialogs]
  );

  const skosLoad = useSkosLoad({
    t,
    updateTask,
    isLink,
    link,
    file,
    getConfirmationDialog,
    external,
  });
  const skosImport = useSkosImport({ t, updateTask, external, userInfo });

  const process = async () => {
    openMainDialog({
      title: t('importTerminology'),
      content: <ProgressList tasks={tasks} nlsBundles={nlsBundles} />,
      actions: null,
      dialogProps: {
        ignoreBackdropClick: true,
        disableEscapeKeyDown: true,
      },
    });
    const data = await skosLoad();
    if (data) {
      const newEntry = await skosImport(data);
      if (newEntry) setConceptSchemeEntry(newEntry);
    }
  };

  const actions = (
    <Button onClick={process} disabled={!buttonEnabled}>
      {t('importButtonLabel')}
    </Button>
  );

  return (
    <>
      <ListActionDialog
        id="import-terminology"
        closeDialog={closeDialog}
        title={t('importTerminology')}
        actions={actions}
        maxWidth="md"
      >
        <ContentWrapper>
          <EntryImport
            link={link}
            file={file}
            handleLinkChange={handleLinkChange}
            setIsLink={setIsLink}
            handleFileChange={handleFileChange}
            validURI={linkIsValid}
            handleTabChange={handleTabChange}
            fileUploadProps={{ helperText: t('fileRequired') }}
          />
          <FormGroup>
            <FormControlLabel
              control={
                <Checkbox
                  checked={external}
                  onChange={(event) => {
                    setExternal(event.target.checked);
                  }}
                />
              }
              label={t('importAsEnhancedCheckboxLabel')}
              title={t('importAsEnhancedTooltip')}
            />
          </FormGroup>
        </ContentWrapper>
      </ListActionDialog>
      <ConfirmationDialog
        open={showConfirmation}
        handleClose={() => setShowConfirmation(false)}
        affirmLabel={t('confirm')}
        rejectLabel={t('reject')}
        {...confirmationProps}
      >
        {t('confirmLargeImport')}
      </ConfirmationDialog>
    </>
  );
};

ImportTerminologyDialog.propTypes = {
  closeDialog: PropTypes.func,
};
export default ImportTerminologyDialog;
