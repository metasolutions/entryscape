import PropTypes from 'prop-types';
import { useState } from 'react';
import { Grid } from '@mui/material';
import { useTranslation } from 'commons/hooks/useTranslation';
import esmoCommonsNLS from 'models/nls/esmoCommons.nls';
import {
  Field,
  Fields,
  SearchButton,
  TextField,
  useEditorContext,
} from 'commons/components/forms/editors';
import { getTitle } from 'commons/util/metadata';
import ExtendsChooser from '../ExtendsChooser';

export const ExtendsEditor = ({
  property,
  label,
  rdfType,
  choices,
  mandatory,
}) => {
  const { resourceURI, graph } = useEditorContext();
  const [selectedExtensionField, setSelectedExtensionField] = useState('');

  const [showExtendsDialog, setShowExtendsDialog] = useState(false);
  const t = useTranslation(esmoCommonsNLS);

  const onSelect = (entry) => {
    graph.findAndRemove(resourceURI, property);
    graph.add(resourceURI, property, entry.getResourceURI());
    setSelectedExtensionField(entry);
  };

  const onClear = () => {
    graph.findAndRemove(resourceURI, property);
    setSelectedExtensionField('');
  };

  return (
    <Fields
      required
      labelId="extend-field"
      label={label}
      addField={() => {}}
      mandatory={mandatory}
      max={1}
    >
      <Field size={1} disabled={!selectedExtensionField} onClear={onClear}>
        <TextField
          disabled
          required
          xs={8}
          inputProps={{ 'aria-labelledby': 'extend-field' }}
          size="small"
          onChange={() => {}}
          value={selectedExtensionField ? getTitle(selectedExtensionField) : ''}
        />
        <Grid item xs={4}>
          <SearchButton
            tooltip={t('selectExtendsTooltip')}
            onClick={() => setShowExtendsDialog(true)}
          />
        </Grid>
      </Field>
      {showExtendsDialog ? (
        <ExtendsChooser
          closeDialog={() => {
            setShowExtendsDialog(false);
          }}
          onSelect={onSelect}
          rdfType={rdfType}
          typeChoices={choices}
          selectedEntry={selectedExtensionField || null}
        />
      ) : null}
    </Fields>
  );
};

ExtendsEditor.propTypes = {
  property: PropTypes.string,
  label: PropTypes.string,
  rdfType: PropTypes.string,
  choices: PropTypes.arrayOf(PropTypes.shape({})),
  mandatory: PropTypes.bool,
};
