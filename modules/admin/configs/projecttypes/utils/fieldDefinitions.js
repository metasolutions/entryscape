import {
  LANGUAGE_LITERAL,
  LITERAL,
  SelectEditor,
  URI,
} from 'commons/components/forms/editors';
import {
  CONFIG_NAMESPACE,
  CONFIG_CONTEXT,
  ENTITY_TYPE_ID_PREFIX,
} from 'admin/configs/utils/namespace';
import { entrystoreUtil } from 'commons/store';
import { getLabel } from 'commons/util/rdfUtils';
import { nameToURI } from 'commons/types/utils/uri';
import { getInstanceAndBuiltinEntityTypes } from 'admin/configs/entitytypes/utils/entityTypeQuery';

export const label = {
  name: 'label',
  nodetype: LANGUAGE_LITERAL,
  property: 'dcterms:title',
  labelNlsKey: 'labelLabel',
  mandatory: true,
};

export const description = {
  name: 'description',
  nodetype: LANGUAGE_LITERAL,
  property: 'dcterms:description',
  labelNlsKey: 'descriptionLabel',
};

export const module = {
  name: 'module',
  nodetype: LITERAL,
  property: `${CONFIG_NAMESPACE}:module`,
  choices: [
    {
      value: 'catalog',
      label: {
        en: 'Catalog',
      },
    },
    {
      value: 'workbench',
      label: {
        en: 'Workbench',
      },
    },
    {
      value: 'toolkit',
      label: {
        en: 'Toolkit',
      },
    },
    {
      value: 'terms',
      label: {
        en: 'Terms',
      },
    },
    {
      value: 'admin',
      label: {
        en: 'Admin',
      },
    },
    {
      value: 'models',
      label: {
        en: 'Models',
      },
    },
  ],
  Editor: SelectEditor,
  labelNlsKey: 'moduleLabel',
};

const getValueFromEntityTypes = async ({ graph, resourceURI, property }) => {
  const selectedResourceURIs = graph
    .find(resourceURI, property)
    .map((statement) => statement.getValue());
  const entries = await entrystoreUtil.loadEntriesByResourceURIs(
    selectedResourceURIs,
    null,
    true
  );
  const instanceAndBuiltinEntityTypes = getInstanceAndBuiltinEntityTypes();
  return entries
    .map((entry, index) => {
      if (entry) {
        const entryLabel = getLabel(entry);
        if (entryLabel) {
          return entryLabel;
        }
      }
      const found = instanceAndBuiltinEntityTypes.find(
        (entityType) =>
          nameToURI(
            `${ENTITY_TYPE_ID_PREFIX}-${entityType.id}`,
            CONFIG_CONTEXT
          ) === selectedResourceURIs[index]
      );
      if (found) {
        return found.label;
      }
      return selectedResourceURIs[index];
    })
    .join(', ');
};

export const defaults = {
  name: 'default-et',
  nodetype: URI,
  property: `${CONFIG_NAMESPACE}:default`,
  labelNlsKey: 'defaultEntityTypesLabel',
  mandatory: true,
  direct: false,
  getValue: getValueFromEntityTypes,
};

export const optional = {
  name: 'optional-et',
  nodetype: URI,
  property: `${CONFIG_NAMESPACE}:optional`,
  labelNlsKey: 'optioinalEntityTypesLabel',
  direct: false,
  getValue: getValueFromEntityTypes,
};
