const webpack = require('webpack');
const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const CspHtmlWebpackPlugin = require('csp-html-webpack-plugin');

/**
 *
 * Get the version from the package.json.
 * We have to do some processing on the version string since the building process is based on branch names rather than the package.json version
 * @todo fix in bitbuckets-pipelines.yml
 * @type {string}
 */
let version = require('./package.json').version;
let isDev = false;

if (version.endsWith('-dev')) {
  version = 'latest';
  isDev = true;
} else if (version.endsWith('-rc')) {
  version = version.replace('-rc', '');
  isDev = true;
}
const ENTRYSCAPE_STATIC = `https://static.${isDev ? '' : 'cdn.'}entryscape.com`;
process.env.ENTRYSCAPE_VERSION = version;
process.env.ENTRYSCAPE_STATIC = ENTRYSCAPE_STATIC;

const locales = ['de', 'sv', 'it', 'fr'];
const momentLocaleRegExp = RegExp(
  locales.reduce(
    (accum, locale, i) =>
      i === 0 ? `${accum}${locale}` : `${accum}|${locale}`,
    ''
  )
);

module.exports = ({ app = 'suite', showNLSWarnings = false }) => ({
  output: {
    filename: 'app.js',
    path: path.join(__dirname, 'app', app, 'dist'),
    chunkFilename: '[name].[contenthash].js',
    library: `EntryScape${app.charAt(0).toUpperCase() + app.substring(1)}`, // e.g EntryScapeSuite
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude:
          /node_modules\/(?!(esi18n|@entryscape|entitytype-lookup|rdforms-shacl)\/).*/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              presets: [
                [
                  '@babel/preset-env',
                  {
                    useBuiltIns: 'usage',
                    corejs: 3,
                    shippedProposals: true,
                    targets: { chrome: 55, edge: 15 },
                  },
                ],
                ['@babel/preset-react'],
              ],
              plugins: [
                '@babel/plugin-transform-object-rest-spread',
                '@babel/plugin-transform-class-properties',
                '@babel/plugin-syntax-dynamic-import',
                ['@babel/plugin-transform-react-jsx', {}],
              ],
            },
          },
          {
            loader: 'ifdef-loader',
            options: {
              BLOCKS: false,
            },
          },
        ],
      },
      {
        test: /\.js$/,
        resolve: {
          fullySpecified: false, // Avoid having to specify js-extension in imports
        },
      },
      {
        test: /\.nls$/,
        use: [
          {
            loader: 'nls-loader',
            options: {
              context: path.join(__dirname, 'app', app),
              showNLSWarnings,
              locales,
            },
          },
        ],
      },
      {
        test: /\.(s*)css$/,
        use: ['style-loader', 'css-loader', 'sass-loader'],
      },
      {
        test: /\.less$/,
        use: [
          'style-loader',
          'css-loader',
          {
            loader: 'less-loader',
            options: {
              lessOptions: {
                strictMath: true,
              },
            },
          },
        ],
      },
      {
        test: /\.html$/,
        use: ['raw-loader'],
      },
      {
        test: /\.(gif|png|jpe?g)$/i,
        type: 'asset/resource',
      },
      {
        test: /.+flag-icon-css.+\.svg$/,
        type: 'asset/resource',
        generator: {
          filename: 'flags/[folder][name].[ext]',
        },
      },
      {
        test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
        type: 'asset/resource',
        exclude: /.+flag-icon-css.+\.svg$/,
        generator: {
          filename: 'fonts/[name].[ext]',
        },
      },
    ],
  },
  plugins: [
    new webpack.DefinePlugin({
      VERSION: JSON.stringify(version),
      ENTRYSCAPE_STATIC: JSON.stringify(ENTRYSCAPE_STATIC),
    }),
    new webpack.ProvidePlugin({
      React: 'react',
      $: 'jquery',
      jQuery: 'jquery',
      jquery: 'jquery',
    }),
    new CopyWebpackPlugin({
      patterns: [
        {
          from: path.join(__dirname, 'templates', '**/*.json'),
          to: 'templates/[name][ext]', // uses output as default path
        },
        {
          from: path.join(__dirname, 'app', app, 'assets'),
          to: 'assets', // uses output as default path
        },
      ],
    }),
    new webpack.ContextReplacementPlugin(
      /moment[/\\]locale$/,
      momentLocaleRegExp
    ),
    new CspHtmlWebpackPlugin(
      {
        'script-src': ["'self'", '*'],
        'style-src': ["'unsafe-inline'", "'self'", '*'],
        'worker-src': ['blob:'],
      },
      {
        nonceEnabled: {
          'script-src': false,
        },
      }
    ),
  ],
  resolve: {
    mainFields: ['module', 'browser', 'main'],
    mainFiles: ['index'],
    alias: {
      jquery: path.join(__dirname, 'node_modules', 'jquery'),
      commons: path.join(__dirname, 'modules', 'commons'),
      catalog: path.join(__dirname, 'modules', 'catalog'),
      admin: path.join(__dirname, 'modules', 'admin'),
      terms: path.join(__dirname, 'modules', 'terms'),
      workbench: path.join(__dirname, 'modules', 'workbench'),
      toolkit: path.join(__dirname, 'modules', 'toolkit'),
      harvest: path.join(__dirname, 'modules', 'harvest'),
      status: path.join(__dirname, 'modules', 'status'),
      suite: path.join(__dirname, 'app', 'suite'),
      registry: path.join(__dirname, 'app', 'registry'),
      config: path.join(__dirname, 'app', app, 'config', 'config'),
      models: path.join(__dirname, 'modules', 'models'),
      search: path.join(__dirname, 'modules', 'search'),
      theme: path.join(__dirname, 'app', app, 'theme'),
      templates: path.join(__dirname, 'templates'),
      '@test-utils': path.join(__dirname, 'test', 'utils'), // prevent eslint warning
    },
  },
  stats: {
    preset: 'minimal',
    colors: true,
    warnings: false,
    errorDetails: true,
    timings: true,
  },
});
