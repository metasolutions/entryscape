import PropTypes from 'prop-types';
import { Chip } from '@mui/material';
import {
  nlsBundlesPropType,
  useTranslation,
} from 'commons/hooks/useTranslation';
import './Tag.scss';

const Tag = ({ nlsBundles = [], isVisible, labelNlsKey, ...props }) => {
  const translate = useTranslation(nlsBundles);
  return (
    <Chip
      label={translate(labelNlsKey)}
      classes={{ root: 'escoTag' }}
      size="small"
      {...props}
    />
  );
};

Tag.propTypes = {
  nlsBundles: nlsBundlesPropType,
  isVisible: PropTypes.func,
  labelNlsKey: PropTypes.string,
};

export default Tag;
