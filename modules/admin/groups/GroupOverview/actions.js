import {
  ACTION_EDIT,
  ACTION_REVISIONS,
  ACTION_REMOVE,
} from 'commons/components/overview/actions';
import { Group as GroupIcon, Delete as RemoveIcon } from '@mui/icons-material';
import RemoveDialog from '../dialogs/RemoveDialog';
import MembersDialog from '../dialogs/MembersDialog';

export const sidebarActions = [
  {
    ...ACTION_EDIT,
    getProps: () => ({
      action: {
        formTemplateId: 'esc:Group',
      },
    }),
  },
  ACTION_REVISIONS,
  {
    id: 'members',
    Dialog: MembersDialog,
    labelNlsKey: 'memberList',
    getProps: () => ({
      startIcon: <GroupIcon />,
    }),
  },
  {
    ...ACTION_REMOVE,
    Dialog: RemoveDialog,
    getProps: () => ({
      startIcon: <RemoveIcon />,
    }),
  },
];
