import PropTypes from 'prop-types';
import Button from '@mui/material/Button';
import ContentWrapper from 'commons/components/common/ContentWrapper';
import ListActionDialog from 'commons/components/common/dialogs/ListActionDialog';
import escoListNLS from 'commons/nls/escoList.nls';
import escaPreparations from 'catalog/nls/escaPreparations.nls';
import { useTranslation } from 'commons/hooks/useTranslation';

const ConfirmationDialog = ({
  buttonLabel,
  dialogConfirmationQuestionNLS,
  closeDialog,
  buttonAction,
}) => {
  const t = useTranslation([escoListNLS, escaPreparations]);

  const actions = (
    <Button autoFocus onClick={buttonAction}>
      {t(buttonLabel)}
    </Button>
  );

  return (
    <ListActionDialog
      id="remove-entry"
      closeDialog={closeDialog}
      actions={actions}
      maxWidth="xs"
    >
      <ContentWrapper xs={10}>
        <div>{t(dialogConfirmationQuestionNLS)}</div>
      </ContentWrapper>
    </ListActionDialog>
  );
};

ConfirmationDialog.propTypes = {
  buttonLabel: PropTypes.string,
  dialogConfirmationQuestionNLS: PropTypes.string,
  closeDialog: PropTypes.func,
  buttonAction: PropTypes.func,
};

export default ConfirmationDialog;
