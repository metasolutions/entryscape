import { useEffect, useState, useRef, useCallback } from 'react';
import { Divider, Typography, Grid } from '@mui/material';
import {
  shouldUseUriPattern,
  createEntry,
  createEntryFromEntityType,
} from 'commons/util/store';
import Tree from 'commons/tree/Tree';
import { entrystoreUtil } from 'commons/store';
import { useESContext } from 'commons/hooks/useESContext';
import Lookup from 'commons/types/Lookup';
import jquery from 'jquery';
import esteConceptNLS from 'terms/nls/esteConcept.nls';
import { i18n } from 'esi18n';
import skosUtil from 'commons/tree/skos/util';
import { useMainDialog } from 'commons/hooks/useMainDialog';
import {
  ActionsMenu,
  ActionsProvider,
  OpenActionsMenuButton,
} from 'commons/components/ListView';
import {
  AcknowledgeAction,
  ConfirmAction,
} from 'commons/components/common/dialogs/MainDialog';
import { useTranslation } from 'commons/hooks/useTranslation';
import escoListNLS from 'commons/nls/escoList.nls';
import { removeConcept, isEnhancedTerminology } from 'terms/utils';
import {
  Bookmark as RevisionsIcon,
  Delete as RemoveIcon,
} from '@mui/icons-material';
import { getLabel, getLocalMetadata } from 'commons/util/rdfUtils';
import { refreshEntry, checkEntriesHaveConflict } from 'commons/util/entry';
import Placeholder from 'commons/components/common/Placeholder';
import 'commons/components/rdforms/SkosChooser';
import useAsync from 'commons/hooks/useAsync';
import { useSnackbar } from 'commons/hooks/useSnackbar';
import { ErrorCatcher } from 'commons/errors/ErrorCatcher';
import { ErrorWithMessage } from 'commons/errors/utils/errors';
import CreateConceptDialog from '../dialogs/CreateConceptDialog';
import EditConceptDialog from '../dialogs/EditConceptDialog';
import ConceptRevisionsDialog from '../dialogs/RevisionsConceptDialog';
import PresentConcept from '../PresentConcept';
import ConceptsInfoIcon from './ConceptsInfoIcon';
import './ConceptsView.scss';

const nlsBundles = [esteConceptNLS, escoListNLS];

const ConceptsView = () => {
  // TODO make into a custom hook. Use only when absolutely necessary
  // entries are mutable and as such it's hard to play ball with react's immutable
  // state principle. So, we fake a state update
  const [, updateState] = useState();
  const forceUpdate = useCallback(() => updateState({}), []);

  const { context, contextName, contextEntry } = useESContext();
  const [conceptTree, setConceptTree] = useState(null);
  const [conceptSchemeEntry, setConceptSchemeEntry] = useState(null);
  const uriSpace = conceptSchemeEntry
    ?.getMetadata()
    .findFirstValue(null, 'void:uriSpace');
  const [selectedConceptEntry, setSelectedConceptEntry] = useState(null);
  const [uriPattern, setURIPattern] = useState('');
  const { openMainDialog } = useMainDialog();
  const [openRevisionsDialog, setOpenRevisionsDialog] = useState(false);
  const conceptTreeRef = useRef(null);
  const { data: entityType, runAsync } = useAsync();
  const translate = useTranslation(nlsBundles);
  const readOnly = contextEntry ? isEnhancedTerminology(contextEntry) : false;
  const [addSnackbar] = useSnackbar();
  const [deleteError, setDeleteError] = useState();

  // import dependencies
  useEffect(() => {
    import(/* webpackChunkName: "jstree" */ 'jstree');
    import(
      // eslint-disable-next-line max-len
      /* webpackChunkName: "jstree-bootstrap-theme" */ 'jstree-bootstrap-theme/dist/themes/proton/style.min.css'
    );
  }, []);

  /**
   * Get entitity type for concept constraint. First lookup
   * if matching entity type is saved in project type.
   */
  useEffect(() => {
    runAsync(
      Lookup.getProjectTypeInUse(context).then((projectType) =>
        Lookup.getEntityTypeByConstraints(
          {
            'skos:inScheme': null,
            'rdf:type': 'skos:Concept',
          },
          projectType
        )
      )
    );
  }, [runAsync, context]);

  const isMoveAllowed = useCallback(
    async (child, from, to) =>
      checkEntriesHaveConflict([child, from, to]).then((hasConflict) => {
        if (!hasConflict) return true;

        const reloadPage = () => window.location.reload();
        openMainDialog({
          content: translate('concurrentConflictMessage'),
          actions: (
            <AcknowledgeAction
              onDone={reloadPage}
              okLabel={translate('reloadButton')}
            />
          ),
        });
        return false;
      }),
    [openMainDialog, translate]
  );

  const disallowedSiblingMove = useCallback(() => {
    openMainDialog({
      content: translate('cannotReorderTerm'),
      actions: <AcknowledgeAction />,
    });
  }, [openMainDialog, translate]);

  // load concept scheme entry
  useEffect(() => {
    if (conceptSchemeEntry) return;

    entrystoreUtil
      .getEntryByType('skos:ConceptScheme', context)
      .then(setConceptSchemeEntry);
  }, [conceptSchemeEntry, context]);

  // check if the terminology has uri pattern (set in local.js)
  useEffect(() => {
    if (!entityType) return;
    if (shouldUseUriPattern(entityType, conceptSchemeEntry)) {
      setURIPattern(entityType.uriPattern);
    }
  }, [conceptSchemeEntry, entityType]);

  // create jsTree
  useEffect(() => {
    if (!conceptSchemeEntry) return;

    setConceptTree(
      new Tree({
        context,
        domNode: conceptTreeRef.current,
        readOnly,
        isMoveAllowed,
        disallowedSiblingMove,
        locale: i18n.getLocale(),
      })
    );
  }, [
    conceptSchemeEntry,
    context,
    disallowedSiblingMove,
    isMoveAllowed,
    readOnly,
  ]);

  // updated selected entry
  const selectNodeListener = useCallback(
    (node, { node: selectedNode }) => {
      conceptTree
        .getTreeModel()
        .getEntry(selectedNode)
        .then(setSelectedConceptEntry);
    },
    [conceptTree]
  );

  // render jsTree and listen for jstree selects
  useEffect(() => {
    /**
     * @param {Event} event
     */
    function handleDeselect(event) {
      if (event.target !== this && event.target.tagName !== 'LI') return;

      const selectedNode = jquery(conceptTree.domNode).jstree(
        'get_selected',
        true
      )[0];
      jquery(conceptTree.domNode).jstree('deselect_node', selectedNode);
      setSelectedConceptEntry(null);
      document.activeElement.blur();
    }

    if (!conceptTree) return;

    conceptTree.show(conceptSchemeEntry);
    jquery(conceptTree.domNode).on('select_node.jstree', selectNodeListener);
    jquery(conceptTree.domNode).on('ready.jstree', () => {
      if (selectedConceptEntry) {
        conceptTree
          .getTree()
          .select_node(selectedConceptEntry.getResourceURI());
      }
    });
    jquery(conceptTree.domNode).on('click', handleDeselect);

    return () => {
      if (conceptTree?.domNode) {
        jquery(conceptTree.domNode).off('select_node.jstree');
      }
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [
    conceptSchemeEntry,
    conceptTree,
    selectNodeListener,
    // selectedConceptEntry, // TODO include this. It re-renders the tree on selection, not ideal
    translate,
  ]);

  // create the concept entry and add skos mappings
  const handleConceptCreate = async (label, alias) => {
    const defLang = i18n.getLocale();
    if (label === '' || label == null) return;

    let prototypeEntry;
    if (uriSpace || uriPattern) {
      prototypeEntry = createEntryFromEntityType(
        context,
        entityType,
        {
          contextName,
          entryName: alias,
        },
        uriSpace
      );
    } else {
      prototypeEntry = createEntry(context, 'skos:Concept');
    }
    const tree = conceptTree.getTree();
    const model = conceptTree.getTreeModel();
    const selNode = conceptTree.getSelectedNode();
    const conceptRURI = prototypeEntry.getResourceURI();
    const metadata = prototypeEntry.getMetadata();
    let lang;
    if (typeof defLang === 'string' && defLang !== '') {
      lang = defLang;
    }

    skosUtil.addNewConceptStmts({
      schemeRURI: conceptSchemeEntry.getResourceURI(),
      isRoot: !selNode,
      conceptRURI,
      metadata,
      label,
      lang,
    });

    await model.createEntry(
      prototypeEntry,
      selNode || model.getRootNode(),
      tree
    );
  };

  const refreshSelectedConceptTreeLabel = () => {
    conceptTree.getTree().set_text(
      { id: selectedConceptEntry.getResourceURI() }, // mocks a jsTree node obj
      getLabel(selectedConceptEntry)
    );
  };

  const revertCallback = () => {
    setOpenRevisionsDialog(false);
    refreshSelectedConceptTreeLabel();
  };

  const handleConceptUpdate = async (graph) => {
    // If needed remove statements from cached external graph before saving.
    const localGraph = getLocalMetadata(graph, selectedConceptEntry);
    return selectedConceptEntry
      .setMetadata(localGraph)
      .commitMetadata()
      .then(() => refreshSelectedConceptTreeLabel()) // update the text of the jstree selected node
      .then(forceUpdate); // update the editor
  };

  const handleConceptDelete = useCallback(() => {
    if (skosUtil.hasChildrenOrRelationsConcepts(selectedConceptEntry)) {
      openMainDialog({
        content: translate('cannotRemoveTermTree'),
        actions: <AcknowledgeAction />,
      });
    } else {
      const onAccept = () =>
        // delete in entrystore
        // not adding new error handling here for now
        removeConcept(selectedConceptEntry, context, conceptTree.getTreeModel())
          // delete in jsTree
          .then(() => conceptTree.deleteNode())
          .then(() => setSelectedConceptEntry(null))
          .then(() =>
            addSnackbar({ message: translate('removeConceptSuccess') })
          )
          .catch((error) => {
            setDeleteError(
              new ErrorWithMessage(translate('removeConceptFail'), error)
            );
          });

      openMainDialog({
        content: translate(
          'confirmRemoveTerm',
          getLabel(selectedConceptEntry) || selectedConceptEntry.getId()
        ),
        actions: (
          <ConfirmAction
            affirmLabel={translate('removeConceptButtonLabel')}
            rejectLabel={translate('cancel')}
            onDone={onAccept}
          />
        ),
      });
    }
  }, [
    selectedConceptEntry,
    openMainDialog,
    translate,
    context,
    conceptTree,
    addSnackbar,
  ]);

  const handleConceptRevision = () => setOpenRevisionsDialog(true);

  const onUpdateConceptRURI = (oldRURI, newRURI) =>
    refreshEntry(selectedConceptEntry) // needed?
      // update the tree and tree model
      .then(() => {
        conceptTree.getTreeModel().updateNodeRURI(oldRURI, newRURI);
        conceptTree.getTree().set_id({ id: oldRURI }, newRURI);
        conceptTree.getTree().refresh_node({ id: newRURI });
      })
      // update the editor
      .then(forceUpdate);

  return (
    <>
      <div className="escoTwoColView esteConcepts__TwoColView">
        <div className="escoTwoColView__Sidebar esteConcepts__sidebar">
          <div className="esteConcepts__sidebarHead">
            <CreateConceptDialog
              selectedConceptEntry={selectedConceptEntry}
              conceptSchemeEntry={conceptSchemeEntry}
              uriPattern={uriPattern}
              createConcept={handleConceptCreate}
              readOnly={readOnly}
            />
          </div>
          <Divider />
          <div className="esteConcepts__sidebarBody" ref={conceptTreeRef} />
        </div>
        <div className="escoTwoColView__Main esteConcepts__main">
          <Grid
            className="esteConcepts__Grid"
            container
            justifyContent="flex-end"
            alignItems="center"
          >
            <Grid className="esteConcepts__Grid" item xs={10}>
              {selectedConceptEntry ? (
                <>
                  <div className="esteConcepts__editorHeader">
                    <Typography
                      className="esteConcepts__editorTitle"
                      variant="h1"
                    >
                      {getLabel(selectedConceptEntry)}
                    </Typography>

                    <div className="esteConcepts__editorActions">
                      <EditConceptDialog
                        conceptEntry={selectedConceptEntry}
                        onEdit={handleConceptUpdate}
                      />
                      <ConceptsInfoIcon
                        entry={selectedConceptEntry}
                        nlsBundles={nlsBundles}
                      />
                      <ActionsProvider>
                        <OpenActionsMenuButton />
                        <ActionsMenu
                          items={[
                            {
                              label: translate('revisions'),
                              icon: <RevisionsIcon />,
                              onClick: handleConceptRevision,
                            },
                            ...(!readOnly
                              ? [
                                  {
                                    label: translate('remove'),
                                    icon: <RemoveIcon />,
                                    onClick: handleConceptDelete,
                                  },
                                ]
                              : []),
                          ]}
                        />
                      </ActionsProvider>
                    </div>
                  </div>
                  <Divider />
                  <div className="esteConcepts__editorPresenter">
                    <PresentConcept
                      entry={selectedConceptEntry}
                      onUpdateConceptRURI={onUpdateConceptRURI}
                      uriPattern={uriPattern}
                      uriSpace={uriSpace}
                      readOnly={readOnly}
                    />
                  </div>
                  {openRevisionsDialog && (
                    <ConceptRevisionsDialog
                      entry={selectedConceptEntry}
                      handleClose={() => setOpenRevisionsDialog(false)}
                      revertCallback={revertCallback}
                    />
                  )}
                </>
              ) : (
                <Placeholder
                  label={translate('emptyTreeWarning')}
                  variant="view"
                />
              )}
            </Grid>
          </Grid>
        </div>
      </div>
      <ErrorCatcher error={deleteError} />
    </>
  );
};

export default ConceptsView;
