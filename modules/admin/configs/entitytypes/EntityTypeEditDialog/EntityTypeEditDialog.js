import PropTypes from 'prop-types';
import TypeEditDialog from 'admin/configs/types/TypeEditDialog';
import { EditorProvider } from 'commons/components/forms/editors';
import { Entry } from '@entryscape/entrystore-js';
import { useTranslation } from 'commons/hooks/useTranslation';
import { useListView } from 'commons/components/ListView';
import EntityTypeForm from '../EntityTypeForm';

const EntityTypeEditDialog = ({
  closeDialog,
  entry,
  entityTypeEntries,
  onEdit,
}) => {
  const { nlsBundles } = useListView();
  const t = useTranslation(nlsBundles);

  return (
    <EditorProvider entry={entry}>
      <TypeEditDialog
        title={t('editHeader')}
        closeDialog={closeDialog}
        nlsBundles={nlsBundles}
        onEdit={onEdit}
      >
        <EntityTypeForm
          entityTypeEntries={entityTypeEntries}
          nlsBundles={nlsBundles}
        />
      </TypeEditDialog>
    </EditorProvider>
  );
};

EntityTypeEditDialog.propTypes = {
  closeDialog: PropTypes.func.isRequired,
  onEdit: PropTypes.func,
  entry: PropTypes.instanceOf(Entry),
  entityTypeEntries: PropTypes.arrayOf(PropTypes.instanceOf(Entry)),
};

export default EntityTypeEditDialog;
