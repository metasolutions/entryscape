import PropTypes from 'prop-types';
import { Entry } from '@entryscape/entrystore-js';
import { useState, useEffect } from 'react';
import {
  useTranslation,
  nlsBundlesPropType,
} from 'commons/hooks/useTranslation';
import { useMainDialog } from 'commons/hooks/useMainDialog';
import ListRemoveEntryDialog from 'commons/components/EntryListView/dialogs/ListRemoveEntryDialog';
import { AcknowledgeAction } from 'commons/components/common/dialogs/MainDialog';

const removeUserFromGroup = async (groupEntry, userEntry) => {
  await groupEntry.getResource(true).removeEntry(userEntry);
};

const RemoveUserDialog = ({
  entry: groupEntry,
  userEntry,
  updateList,
  nlsBundles,
  closeDialog,
  ...restProps
}) => {
  const [isUserManager, setIsUserManager] = useState(false);
  const { openMainDialog } = useMainDialog();
  const t = useTranslation(nlsBundles);

  useEffect(() => {
    const isManager = groupEntry
      .getEntryInfo()
      .getACL()
      .admin.includes(userEntry.getResourceURI());

    if (isManager) {
      setIsUserManager(true);
      openMainDialog({
        content: t('removeManager'),
        actions: <AcknowledgeAction onDone={closeDialog} />,
      });
    }
  }, [isUserManager]);

  const onRemove = async () => {
    await removeUserFromGroup(groupEntry, userEntry);
    await userEntry.setRefreshNeeded(true);
    await userEntry.refresh();
    updateList();
  };

  return (
    <>
      {!isUserManager && (
        <ListRemoveEntryDialog
          {...restProps}
          onRemove={onRemove}
          closeDialog={closeDialog}
          removeConfirmMessage={t('removeGroupFromUser')}
        />
      )}
    </>
  );
};

RemoveUserDialog.propTypes = {
  entry: PropTypes.instanceOf(Entry),
  closeDialog: PropTypes.func,
  nlsBundles: nlsBundlesPropType,
  actions: PropTypes.arrayOf(PropTypes.shape({})),
  userEntry: PropTypes.instanceOf(Entry),
  updateList: PropTypes.func,
  userGroupIds: PropTypes.arrayOf(PropTypes.string),
};

export default RemoveUserDialog;
