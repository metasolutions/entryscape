import * as ns from 'models/utils/ns';
import RfsField from './RfsField';

const LOOKUP_FIELD_PROPERTIES = [
  ns.RDF_PROPERTY_CONSTRAINT,
  ns.RDF_PROPERTY_PATTERN,
];

const LOOKUP_FIELD_COMPATIBLE_TYPES = [ns.RDF_TYPE_SELECT_FIELD];

const LOOKUP_FIELD_NODETYPES = [ns.RDF_PROPERTY_URI];

class RfsLookupField extends RfsField {
  constructor(props) {
    super({
      ...props,
      rdfType: ns.RDF_TYPE_LOOKUP_FIELD,
      nlsKeyLabel: 'lookupFieldLabel',
      properties: LOOKUP_FIELD_PROPERTIES,
      compatibleTypes: LOOKUP_FIELD_COMPATIBLE_TYPES,
      nodetypes: LOOKUP_FIELD_NODETYPES,
    });
  }
}

export default RfsLookupField;
