import PropTypes from 'prop-types';
import { entryPropType, graphPropType } from 'commons/util/entry';
import { useState, useEffect } from 'react';
import { Button } from '@mui/material';
import {
  Editor as RDFormsEditor,
  validate,
} from '@entryscape/rdforms/renderers/react';
import { useTranslation } from 'commons/hooks/useTranslation';
import escoTableViewNLS from 'commons/nls/escoTableView.nls';
import {
  RdformsDialogFormWrapper,
  RdformsStickyDialogContent,
  RdformsDialogContent,
} from 'commons/components/rdforms/RdformsDialogFormWrapper';
import ListActionDialog from 'commons/components/common/dialogs/ListActionDialog';
import LevelSelector, {
  LEVEL_RECOMMENDED,
} from 'commons/components/rdforms/LevelSelector';
import useLevelProfile from 'commons/components/rdforms/hooks/useLevelProfile';
import useGetDatasetTemplateEntry from 'catalog/datasetTemplates/useGetDatasetTemplateEntry';
import { useFormErrorMessage } from 'commons/components/rdforms/hooks/useRDFormsValidation';
import DatasetTemplateMetadataPresenter from 'commons/components/rdforms/DatasetTemplateMetadataPresenter';
import DialogTwoColumnLayout, {
  PrimaryColumn,
  SecondaryColumn,
} from 'commons/components/common/dialogs/DialogTwoColumnLayout';
import RdformsOutline from 'commons/components/rdforms/RdformsOutline';
import useRdformsOutlineSearch from 'commons/components/rdforms/RdformsOutline/useRdformsOutlineSearch';
import useTableViewRowActions from '../../useTableViewRowActions';
import { useTableViewContext } from '../../TableViewContext';
import { useHandleEditRowContext } from '../../HandleEditRowContext';

const TableViewEditDialog = ({
  row,
  closeDialog,
  editorLevel = LEVEL_RECOMMENDED,
  filterPredicates,
  ...rest // ListActionDialog props
}) => {
  const {
    entry,
    graph,
    template,
    actions: { parent: binding },
  } = row;
  const t = useTranslation(escoTableViewNLS);
  const handleEditRow = useHandleEditRowContext();
  const [, dispatch] = useTableViewContext();
  const { handleSaveRow } = useTableViewRowActions(dispatch);
  const [datasetTemplateEntry] = useGetDatasetTemplateEntry(entry);
  const [editor, setEditor] = useState(null);
  const EditorComponent = editor?.domNode.component;
  const getFormErrorMessage = useFormErrorMessage();
  const [formError, setFormError] = useState(null);
  const [errors, setErrors] = useState([]);
  const outlineSearchProps = useRdformsOutlineSearch();
  const alert = formError
    ? { message: formError, setMessage: setFormError }
    : undefined;
  // eslint-disable-next-line react/prop-types
  const templateId = template.getId();
  const [level, setLevel] = useState(editorLevel);
  const disabledLevels = useLevelProfile(templateId);
  const [hasChanges, setHasChanges] = useState(false);

  useEffect(() => {
    try {
      setEditor(
        new RDFormsEditor({
          binding,
          includeLevel: level,
          compact: false,
          filterPredicates,
        })
      );
    } catch (error) {
      console.error('bypassing rdforms issues, caused by: ', error);
    }
  }, [level, filterPredicates, binding]);

  useEffect(() => {
    if (graph) {
      graph.onChange = () => {
        setHasChanges(true);
      };
    }
  }, [graph]);

  // Validate on init to highlight errors in both the form and outline
  useEffect(() => {
    if (!editor) return;

    const { errors: validationErrors } = validate.bindingReport(editor.binding);
    setErrors(validationErrors);
  }, [editor]);

  const handleClose = () => {
    closeDialog();
    if (graph.isChanged()) {
      handleEditRow(row);
    }
  };

  const saveEntry = () => {
    const { errors: validationErrors } = validate.bindingReport(editor.binding);
    if (!validationErrors.length) {
      handleSaveRow(row, handleClose);
      return;
    }

    const errorMessage = getFormErrorMessage(validationErrors);
    setFormError(errorMessage);
    setErrors(validationErrors);
    outlineSearchProps.clearSearch();
    // Needed to update TableView state in case the dialog is closed
    dispatch({
      type: 'EDIT_ROW',
      value: { rowId: row.id, rowErrors: validationErrors },
    });
  };

  const actions = (
    <Button disabled={!hasChanges} onClick={saveEntry}>
      {t('editDialogSaveButton')}
    </Button>
  );

  return (
    <ListActionDialog
      title={t('editDialogHeader')}
      actions={actions}
      closeDialog={handleClose}
      closeDialogButtonLabel={t('editDialogCloseButton')}
      alert={alert}
      id="tableview-edit-entry-dialog"
      {...rest}
    >
      <DialogTwoColumnLayout>
        <SecondaryColumn>
          {editor ? (
            <RdformsOutline
              editor={editor}
              root="tableview-edit-entry-dialog"
              errors={errors}
              searchProps={outlineSearchProps}
            />
          ) : null}
        </SecondaryColumn>
        <PrimaryColumn>
          <RdformsDialogFormWrapper>
            <RdformsStickyDialogContent>
              <LevelSelector
                level={level}
                onUpdateLevel={setLevel}
                disabledLevels={disabledLevels}
              />
            </RdformsStickyDialogContent>
            <RdformsDialogContent>
              {datasetTemplateEntry ? (
                <DatasetTemplateMetadataPresenter
                  entry={datasetTemplateEntry}
                  externalTemplateId={templateId}
                />
              ) : null}
              {editor ? <EditorComponent /> : null}
            </RdformsDialogContent>
          </RdformsDialogFormWrapper>
        </PrimaryColumn>
      </DialogTwoColumnLayout>
    </ListActionDialog>
  );
};

TableViewEditDialog.propTypes = {
  row: PropTypes.shape({
    id: PropTypes.string,
    graph: graphPropType,
    entry: entryPropType,
    template: PropTypes.shape({}),
    actions: PropTypes.shape({ parent: PropTypes.shape({}) }), // GroupBinding
  }),
  closeDialog: PropTypes.func,
  editorLevel: PropTypes.string,
  filterPredicates: PropTypes.shape(),
};

export default TableViewEditDialog;
