const { EntryStore } = require('@entryscape/entrystore-js');
const { BASE_URI } = require('@test-utils/constants');

const store = new EntryStore(BASE_URI);

module.exports = {
  entrystore: store,
};
