import PropTypes from 'prop-types';
import Button from '@mui/material/Button';
import AddIcon from '@mui/icons-material/Add';

export const AddFieldButton = ({ disabled, onClick, title, label }) => {
  return (
    <Button
      onClick={onClick}
      aria-label={title}
      title={title}
      disabled={disabled}
      variant="text"
      color="primary"
      startIcon={<AddIcon />}
    >
      {label}
    </Button>
  );
};

AddFieldButton.propTypes = {
  disabled: PropTypes.bool,
  onClick: PropTypes.func,
  title: PropTypes.string,
  label: PropTypes.string,
};

export default AddFieldButton;
