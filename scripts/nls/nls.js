const fs = require('fs');
const pathUtil = require('path');

const loadJSON = (path) => JSON.parse(fs.readFileSync(path));
/**
 *
 * @param {string} path
 * @param {object} obj
 * @returns {undefined}
 */
const writeJSON = (path, obj) =>
  fs.writeFileSync(path, JSON.stringify(obj || {}, null, '  '));
/**
 *
 * @param {string} path
 * @returns {string[]}
 */
const findFiles = (path) => {
  const arr = [];
  fs.readdirSync(path).forEach((p) => {
    const filename = pathUtil.join(path, p);
    if (!fs.lstatSync(filename).isDirectory()) {
      arr.push(filename);
    }
  });

  return arr;
};

// @todo this should be done in one pass
const readTerms = (isRoot, path, includeDefs) => {
  const nlsFiles = findFiles(path);
  // log(`Found ${nlsFiles.length} nls files`);
  const res = [];
  nlsFiles.forEach((contextPath) => {
    const obj = loadJSON(contextPath);
    // extract the filename without the path and .nls
    const context = pathUtil.basename(contextPath, '.nls');

    const root = isRoot ? obj : obj.root;
    Object.keys(root).forEach((term) => {
      const definition = root[term];
      if (includeDefs) {
        res.push({ term, context, definition });
      } else {
        res.push({ term, context });
      }
    });
  });
  return res;
};

/**
 *
 * @param {object[]} terms
 * @returns {string[]}
 */
const getContexts = (terms) => {
  const contexts = {};
  const arr = [];
  for (let t = 0; t < terms.length; t++) {
    const c = terms[t].context;
    if (!contexts[c]) {
      contexts[c] = true;
      arr.push(c);
    }
  }
  return arr;
};

/**
 *
 * @param {object[]} terms
 * @returns {object}
 */
const getContextGroupedTerms = (terms) => {
  const grouped = {};
  for (let i = 0; i < terms.length; i++) {
    const term = terms[i];
    const context = term.context;
    if (context) {
      if (!grouped[context]) {
        grouped[context] = {};
      }
      grouped[context][term.term] = term.definition;
    }
  }
  return grouped;
};

/**
 *
 * @param {object} terms
 * @returns {object}
 */
const delNull = (terms) => {
  Object.keys(terms).forEach((k) => {
    if (terms[k] === null) {
      delete terms[k];
    }
  });
  return terms;
};

const createDirectory = (directory) => {
  try {
    if (!fs.statSync(directory).isDirectory()) {
      throw new Error('not a directory');
    }
  } catch (e) {
    fs.mkdirSync(directory);
  }
};

/**
 * Since (nls) path and context is not a 1-to-1 relationship in ./project.js this
 * provides a way to map contexts (in POEditor) with (nls) file names locally.
 * Same behaviour happens on upload.
 *
 * @param path
 * @returns {String[]}
 */
const getRelevantContextsForPath = (path) =>
  findFiles(path).map((contextPath) => pathUtil.basename(contextPath, '.nls'));

/**
 *
 * @param {string} path
 * @param {string} lang
 * @param {object[]} terms
 * @param {string[]} contexts
 */
const writeLang = (path, lang, terms, contexts) => {
  const grouped = getContextGroupedTerms(terms);
  const relevantContexts = getRelevantContextsForPath(path);
  createDirectory(pathUtil.join(path, lang));

  // write to files by context
  contexts
    .filter((context) => relevantContexts.includes(context))
    .forEach((context) => {
      writeJSON(
        pathUtil.join(path, lang, `${context}.nls`),
        delNull(grouped[context])
      );
    });
};

/**
 *
 * @param {string[]} _langs
 * @param {object[]} terms
 * @param {string[]} paths
 */
const writeTranslations = (_langs, terms, paths) => {
  const langs = _langs.slice(0);
  const enIdx = langs.indexOf('en');
  const enTerms = terms[enIdx];
  terms.splice(enIdx, 1);
  langs.splice(enIdx, 1);
  const contexts = getContexts(enTerms);
  langs.forEach((lang, idx) => {
    paths.forEach((path) => {
      writeLang(path, lang, terms[idx], contexts);
    });
  });
};

module.exports = {
  readTerms,
  writeTranslations,
};
