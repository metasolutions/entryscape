import React from 'react';
import PropTypes from 'prop-types';
import { Entry } from '@entryscape/entrystore-js';
import EditEntryDialog from 'commons/components/entry/EditEntryDialog';
import 'commons/components/rdforms/GeoChooser';

const EditDatasetDialog = ({ closeDialog, datasetEntry }) => {
  return <EditEntryDialog entry={datasetEntry} closeDialog={closeDialog} />;
};

EditDatasetDialog.propTypes = {
  closeDialog: PropTypes.func,
  datasetEntry: PropTypes.instanceOf(Entry),
};

export default EditDatasetDialog;
