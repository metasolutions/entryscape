import { useEffect, useRef } from 'react';
import useDebounce from 'commons/hooks/useDebounce';
import { escapeSolrStrings } from 'commons/util/solr';
import useAsync from 'commons/hooks/useAsync';
import sleep from 'commons/util/sleep';

/**
 *
 * @param {{ query: object, execQuery: function, applyParams: function, search: string, sort: string, page: number }}
 * @returns
 */
const fetchEntries = ({
  // query and state data
  query,
  searchQueryString,
  sort,
  page,
  limit,

  // execution strategy
  execQuery,
  applyParams,
}) => {
  applyParams({ query, search: searchQueryString, sort, page, limit });

  return execQuery(query, page).then((result) => {
    return {
      ...result,
      searchQueryString,
    };
  });
};

const initialState = {
  entries: [],
  size: -1,
  searchQueryString: '',
};

const SOLR_DELAY = 1500;
/**
 * Custom hook to fetch entries with for a certain query and filters
 * @param {{query: SolrQuery, execQuery: function, applyParams: function, search: string, sort: string, page: number}}
 * @returns {Promise.{entries: Entry[], size: number}}
 */
const useSolrQueryEntries = (
  {
    query,
    search,
    sort,
    page,
    limit,
    minimumSearchLength = 3,
    delay = 300,
    execQuery,
    applyParams,
  },
  queryCount
) => {
  const { data: entriesResults, status, runAsync } = useAsync({
    data: initialState,
  });
  /**
   * The search string that will be used in the solr query.
   * If search is less than minimumSearchLength, no search
   * should be done.
   */
  const searchQueryString =
    search.trim().length < minimumSearchLength ? '' : escapeSolrStrings(search);
  const debouncedFetchEntries = useDebounce(
    (options) => runAsync(fetchEntries(options)),
    delay
  );
  const initialFetch = useRef(true);
  const previousQueryCount = useRef(0);

  useEffect(() => {
    const fetchOptions = {
      query,
      searchQueryString,
      sort,
      page,
      limit,
      execQuery,
      applyParams,
    };

    /* 
      To trigger refresh, queryCount is increased after refresh,
      create or delete.
      Entries will be fetched using a delay because of solr reindexing.      
    */
    if (queryCount !== previousQueryCount.current) {
      previousQueryCount.current = queryCount;

      runAsync(
        (async () => {
          await sleep(SOLR_DELAY);
          return fetchEntries(fetchOptions);
        })()
      );
      return;
    }

    // Initial fetch should not have a delay or be debounced
    if (initialFetch.current) {
      runAsync(fetchEntries(fetchOptions));
      initialFetch.current = false;
      return;
    }
    // Debounce queries to prevent multiple queries while typing
    debouncedFetchEntries(fetchOptions);
  }, [
    page,
    searchQueryString,
    sort,
    limit,
    minimumSearchLength,
    applyParams,
    debouncedFetchEntries,
    runAsync,
    queryCount,
  ]);

  return [entriesResults, status];
};

export default useSolrQueryEntries;
export { SOLR_DELAY };
