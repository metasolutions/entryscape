import { entrystore } from 'commons/store';
import { isAdmin } from 'commons/util/user';
import { Graph } from '@entryscape/rdfjson';
import { Pipeline, Entry } from '@entryscape/entrystore-js';
import { RDF_PROPERTY_PROJECTTYPE } from 'entitytype-lookup';
import Lookup from 'commons/types/Lookup';
import config from 'config';
import { getPipelineEntryAndResource } from './data';

/**
 * Just a helper function to avoid duplicate code
 *
 * @param {Entry} userEntry
 * @param {Graph} md
 * @param {string} ruri
 * @param {object} data
 * @param {string} type
 * @param {string[]} tags
 */
const addToMetadata = (userEntry, md, ruri, data, type = '', tags = []) => {
  const { title, description, username, psi, orgId } = data;
  const isAdministrator = isAdmin(userEntry);

  md.addL(ruri, 'dcterms:title', title);
  if (description) {
    md.addL(ruri, 'dcterms:description', description);
  }

  if (psi) {
    md.addL(ruri, 'dcterms:subject', 'psi');
    md.add(ruri, 'foaf:page', psi);
  }

  tags.forEach((tag) => md.addL(ruri, 'dcterms:subject', tag));

  if (orgId) {
    md.addL(ruri, 'dcterms:identifier', orgId);
  }

  if (username) {
    md.add(ruri, 'foaf:mbox', `mailto:${username}`);
  } else if (!isAdministrator) {
    const res = userEntry.getResource(true);
    md.add(ruri, 'foaf:mbox', `mailto:${res.getSource().name}`);
  }

  if (type) {
    md.add(ruri, 'rdf:type', type);
  }
};

/**
 * Sets new metadata for a pipeline
 *
 * @todo @valentino not good that we are coldly dropping the previous metadata. Re-write!
 * @param {Entry} entry
 * @param {Entry} userEntry
 * @param {object} data
 * @param {string} type
 * @param {string[]} tags
 * @see this.addToMetadata
 * @returns {Entry}
 */
const setEntryMetadata = (entry, userEntry, data, type = '', tags) => {
  const md = new Graph();
  addToMetadata(userEntry, md, entry.getResourceURI(), data, type, tags);
  const updatedEntry = entry.setMetadata(md);
  return updatedEntry;
};

/**
 * Adds transforms to the pipeline resource
 *
 * @param {Resource} resource
 * @param {object} data
 * @param {boolean} isDcat
 */
const addTransforms = (resource, data, isDcat) => {
  const { title, psi, sourceUrl, transforms } = data;
  const psiSource = psi
    ? `${psi.replace(/\/$/, '')}/${config.get('registry.psidataPath')}`
    : '';

  if (isDcat) {
    const dcatTransforms = [
      { type: 'empty', args: {} },
      { type: 'fetch', args: { source: sourceUrl } },
      { type: 'validate', args: { profile: 'dcat_ap_se' } },
      { type: 'merge', args: title ? { title } : {} },
    ];
    const checkTransform = { type: 'check', args: { source: psiSource } };
    if (psi) dcatTransforms.splice(1, 0, checkTransform);
    dcatTransforms.forEach(({ type, args }) =>
      resource.addTransform(type, args)
    );

    return;
  }

  [
    { type: 'empty', args: {} },
    ...transforms.filter((transform) => transform.type),
  ].forEach(({ type, args }) => resource.addTransform(type, args));
};

export const createContextAndGroup = (
  userEntry,
  userInfo,
  data,
  replaceUserErrorMessage,
  tags
) =>
  entrystore.createGroupAndContext().then(async (groupEntry) => {
    const homeContextId = groupEntry.getResource(true).getHomeContext();
    const homeContext = entrystore.getContextById(homeContextId);
    const contextEntry = await homeContext.getEntry();

    try {
      const { username } = data;

      if (username) {
        await replaceUser(
          userEntry,
          userInfo,
          groupEntry,
          contextEntry,
          username
        );
        groupEntry.setRefreshNeeded();
        await groupEntry.refresh();
      }

      // Set group title
      const { title } = data;
      groupEntry
        .getMetadata()
        .addL(groupEntry.getResourceURI(), 'foaf:name', title);
      await groupEntry.commitMetadata();
    } catch (e) {
      throw Error(replaceUserErrorMessage);
    }

    // Add title to the context metadata
    try {
      const { title, projectType } = data;
      contextEntry.addL('dcterms:title', title);
      await contextEntry.commitMetadata();

      // Make the context a esterms:CatalogContext
      const homeContextEntryInfo = contextEntry.getEntryInfo();
      homeContextEntryInfo
        .getGraph()
        .add(
          contextEntry.getResourceURI(),
          'rdf:type',
          'esterms:CatalogContext'
        );

      // TODO remove when entrystore is changed so groups have read access to
      // homecontext metadata by default.
      // Start fix with missing metadata rights on context for group
      const acl = homeContextEntryInfo.getACL(true);
      acl.mread.push(groupEntry.getId());
      acl.mwrite.push(groupEntry.getId());
      homeContextEntryInfo.setACL(acl);
      // End fix

      if (projectType) {
        homeContextEntryInfo
          .getGraph()
          .add(
            contextEntry.getResourceURI(),
            RDF_PROPERTY_PROJECTTYPE,
            projectType
          );
      }

      homeContextEntryInfo.commit();
    } catch (e) {
      throw Error(
        'There was some error while trying to set the home context title'
      );
    }

    const protoPipeline = homeContext.newPipeline();
    const isDcat = tags.includes('dcat');
    try {
      // Create PipelineEntry and add metadata
      const pipelineResource = protoPipeline.getResource();
      addTransforms(pipelineResource, data, isDcat);
      const pmd = protoPipeline.getMetadata();
      addToMetadata(
        userEntry,
        pmd,
        pipelineResource.getResourceURI(),
        data,
        null,
        tags
      );
      await protoPipeline.commit();
    } catch (e) {
      console.error(e);
      throw Error(
        'There was some error while trying to create the pipeline entry/resource'
      );
    }
  });

export const updatePipelineEntry = async (
  entry,
  userEntry,
  userInfo,
  data,
  replaceUserErrorMessage,
  tags
) => {
  const context = entry.getContext();
  const contextEntry = await context.getEntry();
  const isDcat = tags.includes('dcat');

  try {
    // upate the metadata of the pipeline entry
    const updatedPipelineEntry = setEntryMetadata(
      entry,
      userEntry,
      data,
      null,
      tags
    );
    await updatedPipelineEntry.commitMetadata();
  } catch (e) {
    throw Error('There was some error while trying to update the Pipeline');
  }

  let groupEntry = null;
  try {
    groupEntry = await context.getHomeContextOf(); // group or user (TODO check)
  } catch (e) {
    // No user or group that has this context as home context
  }

  // update owner user
  try {
    const { username } = data;
    if (username) {
      await replaceUser(
        userEntry,
        userInfo,
        groupEntry,
        contextEntry,
        username
      );
      groupEntry.setRefreshNeeded();
      await groupEntry.refresh();
    }
  } catch (e) {
    throw Error(replaceUserErrorMessage);
  }

  try {
    // replace the pipeline resource
    // 1. remove all transforms
    // 2. add new ones according to the data
    const pipelineResource = await entry.getResource();
    pipelineResource
      .getTransforms()
      .forEach(pipelineResource.removeTransform, pipelineResource);
    addTransforms(pipelineResource, data, isDcat);
    await pipelineResource.commit();
  } catch (e) {
    throw Error('There was some error while trying to edit pipeline resource');
  }

  try {
    // Set group title
    const { title } = data;
    const groupMd = new Graph();
    groupMd.addL(groupEntry.getResourceURI(), 'foaf:name', title);
    groupEntry.setMetadata(groupMd);
    await groupEntry.commitMetadata();
  } catch (e) {
    throw Error(
      'There was some error while trying to edit the group of the pipeline'
    );
  }

  try {
    // Add title to the context metadata
    const { title } = data;
    contextEntry.setRefreshNeeded(true);
    await contextEntry.refresh();
    contextEntry
      .getMetadata()
      .addL(contextEntry.getResourceURI(), 'dcterms:title', title);
    await contextEntry.commitMetadata();
  } catch (e) {
    throw Error(
      'There was some error while trying to edit the home context of the' +
        ' pipeline'
    );
  }
};

/**
 * Get the value of the `source` argument for some known transforms
 *
 * @param {Pipeline} pipeline
 * @returns {string}
 */
const getSourceUrl = (pipeline) => {
  let sourceUrl = '';
  const transformsWithSource = ['fetch', 'fetchCKAN', 'fetchCSW'];
  pipeline.getTransforms().forEach((id) => {
    const type = pipeline.getTransformType(id);
    if (transformsWithSource.includes(type))
      sourceUrl = pipeline.getTransformArguments(id).source;
  });

  return sourceUrl;
};

/**
 * @param {Entry} entry
 * @param {boolean} loadResource
 * @returns {Promise|object|null}
 */
export const getPipelineMetadata = async (entry, loadResource = false) => {
  const metadata = entry.getMetadata();
  const resourceURI = entry.getResourceURI();
  const title = metadata.findFirstValue(resourceURI, 'dcterms:title') || '';
  const description =
    metadata.findFirstValue(resourceURI, 'dcterms:description') || '';
  const psi = metadata.findFirstValue(resourceURI, 'foaf:page') || '';
  const organizationId =
    metadata.findFirstValue(resourceURI, 'dcterms:identifier') || '';
  const username =
    metadata.findFirstValue(resourceURI, 'foaf:mbox')?.replace('mailto:', '') ||
    '';
  const projectType = await Lookup.getProjectTypeInUse(entry.getContext());
  const metadataObject = {
    title,
    description,
    username,
    psi,
    orgId: organizationId,
    ...(projectType ? { projectType: projectType.getId() } : {}),
  };
  if (!loadResource) return metadataObject;

  return entry.getResource().then((pipelineResource) => {
    const sourceUrl = getSourceUrl(pipelineResource);
    metadataObject.sourceUrl = sourceUrl;
    return metadataObject;
  });
};

/**
 * If the user with a certain username does not exist :
 *  - create user
 *  - add acl for group
 *  - remove current user from all entries' acl in group and add the new user to them
 *
 * If the user with a certain username exists then return the user entry
 *
 * @param userEntry
 * @param userInfo
 * @param groupEntry
 * @param contextEntry
 * @param username
 */
const replaceUser = async (
  userEntry,
  userInfo,
  groupEntry,
  contextEntry,
  username
) => {
  if (userInfo.user === username) {
    return;
  }

  let newUserEntry;
  const data = await entrystore
    .getREST()
    .get(`${entrystore.getBaseURI()}_principals?entryname=${username}`);
  if (data.length > 0) {
    // return existing userEntry
    const userEntryId = data[0];
    newUserEntry = await entrystore
      .getContextById('_principals')
      .getEntryById(userEntryId);
  } else {
    // Create new userEntry, and make sure its metadata is public
    const newUser = entrystore.newUser(username);
    const newUserEntryInfo = newUser.getEntryInfo();
    const newUserACL = newUserEntryInfo.getACL(true);
    newUserACL.mread.push('_users');
    newUserACL.mwrite.push('_newId');
    newUserACL.rwrite.push('_newId');
    newUserEntryInfo.setACL(newUserACL);
    newUserEntry = await newUser.commit();
  }

  // add new user to rwrite of the group
  makeOwner(groupEntry, newUserEntry);
  makeOwner(contextEntry, newUserEntry);

  // remove current user from all entries' acl in group, and add the new user to all of them
  const entryIds = await groupEntry.getResource(true).getAllEntryIds();
  entryIds.splice(entryIds.indexOf(userEntry.getId()), 1);
  entryIds.push(newUserEntry.getId());
  await groupEntry.getResource(true).setAllEntryIds(entryIds);
};

const makeOwner = async (entryToOwn, userEntry) => {
  // add new user to rwrite of the group
  const entryInfo = entryToOwn.getEntryInfo();
  const acl = entryInfo.getACL(true);
  const { admin } = acl;

  if (!admin.includes(userEntry.getId())) {
    admin.push(userEntry.getId());
    entryInfo.setACL(acl);

    await entryInfo.commit();
  }
};

/**
 *
 * @param {Pipeline} pipelineResource
 * @returns {object[]}
 */
export const getTransformsData = (pipelineResource) => {
  if (!pipelineResource) return [];

  return pipelineResource.getTransforms().map((id) => {
    const type = pipelineResource.getTransformType(id);
    const priority = pipelineResource.getPriority(id);
    const args = pipelineResource.getTransformArguments(id);
    return { id, type, priority, args };
  });
};

export const fetchOrganizationData = async (contextId) => {
  const { pipelineEntry, pipelineResource } = await getPipelineEntryAndResource(
    contextId
  );
  const fetchedData = await getPipelineMetadata(pipelineEntry, true);
  // eslint-disable-next-line no-unused-vars
  const [_empty, ...transforms] = getTransformsData(pipelineResource);
  const entityType = await Lookup.inUse(pipelineEntry);

  return {
    pipelineEntry,
    organizationData: { ...fetchedData, transforms },
    entityType,
  };
};
