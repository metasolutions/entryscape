import PropTypes from 'prop-types';
import { getLocalizedValue } from 'commons/util/rdfUtils';
import { Fields } from '../Fields';
import { Field } from '../Field';
import { Text } from '../Text';
import { Language } from '../Language';

export const LanguageLiteral = ({
  graph,
  resourceURI,
  property,
  inline,
  children,
  ...fieldsProps
}) => {
  const statements = graph.find(resourceURI, property);

  return statements.length ? (
    <Fields inline={inline} {...fieldsProps}>
      {inline ? (
        <Field inline>
          <Text text={getLocalizedValue(graph, resourceURI, [property])} />
        </Field>
      ) : (
        statements.map((statement, index) => {
          return (
            // eslint-disable-next-line react/no-array-index-key
            <Field key={index}>
              <Text text={statement.getValue()} />
              <Language language={statement.getLanguage()} />
            </Field>
          );
        })
      )}
      {children}
    </Fields>
  ) : null;
};

LanguageLiteral.propTypes = {
  graph: PropTypes.shape({ find: PropTypes.func }),
  resourceURI: PropTypes.string,
  property: PropTypes.string,
  inline: PropTypes.bool,
  children: PropTypes.node,
};
