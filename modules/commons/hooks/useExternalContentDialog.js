import { useEffect } from 'react';
import { getContentHTML } from 'commons/util/htmlUtil';
import { renderHtmlString } from 'commons/util/reactUtils';
import { i18n } from 'esi18n';
import { useTranslation } from 'commons/hooks/useTranslation';
import escoErrors from 'commons/nls/escoErrors.nls';
import { useMainDialog } from 'commons/hooks/useMainDialog';
import { AcknowledgeAction } from 'commons/components/common/dialogs/MainDialog';
import useAsync from './useAsync';

const useExternalContentDialog = ({
  open,
  contentPath,
  callback,
  title = '',
  actions = null,
}) => {
  const { openMainDialog } = useMainDialog();
  const t = useTranslation(escoErrors);
  const { runAsync } = useAsync(null);

  useEffect(() => {
    if (!open) return;
    if (!contentPath)
      throw Error('No content path given for external content dialog');

    runAsync(
      getContentHTML(`${contentPath}_${i18n.getLocale()}.html`)
        .catch(() => getContentHTML(`${contentPath}.html`))
        .then(
          (html) =>
            openMainDialog({
              title,
              content: renderHtmlString(html),
              actions: actions || <AcknowledgeAction onDone={callback} />,
            }),
          () =>
            openMainDialog({
              title,
              content: t('restrictionFallback'),
              actions: actions || <AcknowledgeAction onDone={callback} />,
            })
        )
        .then(callback)
    );
  }, [
    actions,
    callback,
    openMainDialog,
    open,
    t,
    contentPath,
    title,
    runAsync,
  ]);
};

export default useExternalContentDialog;
