import { renderingContext } from '@entryscape/rdforms';
import {
  getHierarchy,
  getGeonameId,
} from 'commons/components/rdforms/GeonamesChooser/util';
import show from './showDialog';

/**
 * Retrieves current location data
 * @param {string} value
 * @returns {Promise}
 */
const getCurrentLocationData = async (value) => {
  const id = await getGeonameId(value);
  const hierarchy = await getHierarchy(id);
  const currentLocation = hierarchy.filter((item) => item.geonameId === id);
  return currentLocation;
};

const cachedLocationData = {};

const chooserConfiguration = {
  show,
  supportsInlineCreate() {
    return false;
  },
  getChoice: (_item, value) => {
    if (cachedLocationData.value) {
      return cachedLocationData.value;
    }
    const locationData = {
      value,
      load: (onSuccess) => {
        if (locationData._promise) {
          locationData._promise.then(onSuccess);
          return locationData._promise;
        }
        locationData._promise = new Promise((resolve, reject) => {
          getCurrentLocationData(value).then(([data]) => {
            locationData.label = { en: data.name };
            delete locationData.load;
            delete locationData._promise;
            resolve(locationData);
          });
        }).then(onSuccess);
        return locationData._promise;
      },
    };
    cachedLocationData.value = locationData;
    return locationData;
  },
};

renderingContext.chooserRegistry
  .style('geonames')
  .register(chooserConfiguration);
