import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { TextField } from '@mui/material';
import ContentWrapper from 'commons/components/common/ContentWrapper';
import esadUserNLS from 'admin/nls/esadUser.nls';
import { useTranslation } from 'commons/hooks/useTranslation';
import { isUsernameInUse } from 'commons/util/user';
import ListActionDialog from 'commons/components/common/dialogs/ListActionDialog';
import { ErrorCatcher } from 'commons/errors/ErrorCatcher';
import { ErrorWithMessage } from 'commons/errors/utils/errors';
import useAsync, { PENDING } from 'commons/hooks/useAsync';
import LoadingButton from 'commons/components/LoadingButton';

const ChangeUsernameDialog = ({ entry, closeDialog, onCreate }) => {
  const [username, setUsername] = useState('');
  const [newUsername, setNewUsername] = useState('');
  const [hasUsernameError, setUsernameError] = useState(false);
  const { runAsync, error: changeError, status } = useAsync();
  const translate = useTranslation(esadUserNLS);

  useEffect(() => {
    entry.getResource().then((userEntry) => {
      setUsername(userEntry.getName());
    });
  }, []);

  const handleUsernameChange = (e) => {
    setNewUsername(e.target.value); // update UI
    isUsernameInUse(e.target.value).then(setUsernameError); // check if new username is already used
    // TODO use some debounce hook
  };

  const saveNewUsername = () => {
    return entry
      .getResource()
      .then((user) => user.setName(newUsername))
      .then(onCreate)
      .then(closeDialog)
      .catch((error) => {
        throw new ErrorWithMessage(translate('changeUsernameFail'), error);
      });
  };

  const actions = (
    <LoadingButton
      loading={status === PENDING}
      onClick={() => runAsync(saveNewUsername())}
      disabled={
        username === newUsername || newUsername.length < 2 || hasUsernameError
      }
    >
      {translate('changeUsernameButton') /* TODO nls too long for this UI */}
    </LoadingButton>
  );

  return (
    <>
      <ListActionDialog
        id="change-username"
        title={translate('changeUsernameHeader')}
        actions={actions}
        closeDialog={closeDialog}
        maxWidth="sm"
      >
        <ContentWrapper>
          <form noValidate>
            <TextField
              id="current-username"
              label={translate('currentUsernameLabel')}
              type="email"
              disabled
              value={username}
            />
            <TextField
              error={hasUsernameError}
              helperText={hasUsernameError ? translate('usernameTaken') : ''}
              autoFocus
              id="new-username"
              label={translate('changeUsernameLabel')}
              type="email"
              value={newUsername}
              onChange={handleUsernameChange}
            />
          </form>
        </ContentWrapper>
      </ListActionDialog>
      <ErrorCatcher error={changeError} />
    </>
  );
};

ChangeUsernameDialog.propTypes = {
  entry: PropTypes.shape(),
  closeDialog: PropTypes.func.isRequired,
  onCreate: PropTypes.func,
};

export default ChangeUsernameDialog;
