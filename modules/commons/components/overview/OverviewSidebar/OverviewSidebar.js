import PropTypes from 'prop-types';
import { List } from '@mui/material';
import './OverviewSidebar.scss';

const OverviewSidebar = ({ children }) => {
  return (
    <div className="escoOverview__sidebar">
      <List>{children}</List>
    </div>
  );
};

OverviewSidebar.propTypes = {
  children: PropTypes.node.isRequired,
};

export default OverviewSidebar;
