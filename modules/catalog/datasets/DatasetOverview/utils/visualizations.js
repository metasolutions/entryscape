import { Context, Entry } from '@entryscape/entrystore-js';
import { entrystoreUtil } from 'commons/store';
import { createEntry, spreadEntry } from 'commons/util/store';
import { getResourceURI } from 'commons/util/entry';
import { truncate } from 'commons/util/util';
import { i18n } from 'esi18n';
import config from 'config';
import { getLabel } from 'commons/util/rdfUtils';
import { fetchCsv } from 'commons/util/fetchCsv';
import {
  filterFileDistributionEntries,
  getCsvFileEntries,
  getDistributionFileRURIs,
  getDistributions,
  getDistributionTitle,
  isWMSDistribution,
} from './distributions';
import { CSV_VALID_FORMATS } from './csv';

const ES_VISUALIZATION_TERM = 'http://entryscape.com/terms/visualization/';

const namespaces = {
  Visualization: `${ES_VISUALIZATION_TERM}Visualization`,
  xAxis: `${ES_VISUALIZATION_TERM}xAxis`,
  yAxis: `${ES_VISUALIZATION_TERM}yAxis`,
  operation: `${ES_VISUALIZATION_TERM}operation`,
  chartType: `${ES_VISUALIZATION_TERM}chartType`,
  encoding: `${ES_VISUALIZATION_TERM}encoding`,
  columns: `${ES_VISUALIZATION_TERM}columns`,
  layers: `${ES_VISUALIZATION_TERM}layers`,
  extent: `${ES_VISUALIZATION_TERM}extent`,
  wmsBaseURI: `${ES_VISUALIZATION_TERM}wmsBaseURI`,
  basemap: `${ES_VISUALIZATION_TERM}basemap`,
  initialView: `${ES_VISUALIZATION_TERM}initialView`,
  legendControl: `${ES_VISUALIZATION_TERM}legendControl`,
  scaleLineControl: `${ES_VISUALIZATION_TERM}scaleLineControl`,
  featureTitle: `${ES_VISUALIZATION_TERM}featureTitle`,
};

const DCAT_ACCESSURL = 'dcat:accessURL';

export const WMS_FORMAT = 'wms';
export const CSV_FORMAT = 'csv';

const DEFAULT_WMS_VISUALIZATION_STATE = {
  chartType: WMS_FORMAT,
  layers: [],
  extent: [],
  wmsBaseURI: '',
  sourceURI: '',
  basemap: 'true',
  initialView: [],
  legendControl: 'false',
  scaleLineControl: 'false',
};

const DEFAULT_CSV_VISUALIZATION_STATE = {
  chartType: 'bar',
  encoding: 'UTF-8',
  xField: '',
  yField: '',
  columns: [],
  operation: 'none',
  sourceURI: '',
  featureTitle: '',
  scaleLineControl: '',
};

const defaultStateMap = {
  wms: DEFAULT_WMS_VISUALIZATION_STATE,
  csv: DEFAULT_CSV_VISUALIZATION_STATE,
};

export const getDefaultTitles = () => [
  { title: '', lang: i18n.getLocale() || config.get('locale.fallback') },
];

/**
 *
 * @param {string} format
 * @returns {object}
 */
export const getDefaultFieldsState = (format) => {
  if (format && !(format in defaultStateMap)) {
    throw new Error(`Unknown visuzalization format: ${format}`);
  }
  return {
    ...(defaultStateMap[format] || {}),
    titles: getDefaultTitles(),
  };
};

const statementToTitleItem = (statement) => ({
  title: statement.getValue(),
  lang: statement.getLanguage(),
});

/**
 *
 * @param {Entry} visualizationEntry
 * @returns {string} chartType
 */
export const getChartType = (visualizationEntry) => {
  const resourceURI = visualizationEntry.getResourceURI();
  const metadata = visualizationEntry.getMetadata();
  return metadata.findFirstValue(resourceURI, namespaces.chartType);
};

/**
 *
 * @param {Entry} visualizationEntry
 * @returns {string}
 */
export const getVisualizationTitle = (visualizationEntry) => {
  const metadata = visualizationEntry.getMetadata();
  const resourceURI = visualizationEntry.getResourceURI();
  const currentLang = i18n.getLocale();
  const titles = metadata
    .find(resourceURI, 'dcterms:title')
    .map(statementToTitleItem);

  const title =
    titles.find(({ lang }) => currentLang === lang)?.title || titles[0].title;
  return truncate(title);
};

/**
 *
 * @param {Entry} datasetEntry
 * @returns {Promise.<Entry[]>}
 */
export const getVisualizations = (datasetEntry) => {
  const { metadata, ruri: resourceURI } = spreadEntry(datasetEntry);
  const statements = metadata.find(resourceURI, 'schema:diagram');

  const resourceURIs = statements.map((statement) => statement.getValue());
  return entrystoreUtil.loadEntriesByResourceURIs(
    resourceURIs,
    datasetEntry.getContext(),
    true
  );
};

/**
 *
 * @param {Entry} fileEntry
 * @param {Entry} datasetEntry
 * @returns {Promise.<Entry[]>}
 */
export const getVisualizationsForFile = async (fileEntry, datasetEntry) => {
  const visualizationEntries = await getVisualizations(datasetEntry);
  const fileResourceURI = fileEntry.getResourceURI();
  return visualizationEntries.filter(
    (visualizationEntry) =>
      visualizationEntry
        .getMetadata()
        .findFirstValue(
          visualizationEntry.getResourceURI(),
          'dcterms:source'
        ) === fileResourceURI
  );
};

/**
 *
 * @param {Context} context
 * @returns {Entry}
 */
export const createPrototypeEntry = (context) => {
  const prototypeEntry = createEntry(context, namespaces.Visualization);
  prototypeEntry.add('rdf:type', namespaces.Visualization);
  return prototypeEntry;
};

/**
 *
 * @param {Entry} datasetEntry
 * @param {Entry} visualisationPrototypeEntry
 * @returns {Promise<Entry>}
 */
export const createVisualization = async (
  datasetEntry,
  visualisationPrototypeEntry
) => {
  return visualisationPrototypeEntry
    .commit()
    .then(async (visualizationEntry) => {
      const metadata = datasetEntry.getMetadata();
      metadata.add(
        datasetEntry.getResourceURI(),
        'schema:diagram',
        visualizationEntry.getResourceURI()
      );
      await datasetEntry.commitMetadata();
      return datasetEntry;
    });
};

/**
 *
 * @param {Entry} visualizationEntry
 * @param {object} fieldsState
 * @returns {visualizationEntry}
 */
export const updateVisualizationMetadata = (
  visualizationEntry,
  fieldsState
) => {
  const { metadata, ruri: resourceURI } = spreadEntry(visualizationEntry);
  const {
    chartType,
    encoding,
    xField,
    yField,
    operation,
    titles,
    columns,
    layers,
    extent,
    wmsBaseURI,
    sourceURI,
    basemap,
    initialView,
    legendControl,
    scaleLineControl,
    featureTitle,
  } = fieldsState;
  // mandatory for all chartTypes
  metadata.findAndRemove(resourceURI, 'dcterms:title');
  titles.forEach(({ title, lang }) => {
    metadata.addL(resourceURI, 'dcterms:title', title, lang);
  });

  metadata.findAndRemove(resourceURI, 'dcterms:source');
  metadata.add(resourceURI, 'dcterms:source', sourceURI);

  metadata.findAndRemove(resourceURI, namespaces.chartType);
  metadata.addL(resourceURI, namespaces.chartType, chartType);
  // specific to different chartTypes

  metadata.findAndRemove(resourceURI, namespaces.encoding);
  metadata.findAndRemove(resourceURI, namespaces.xAxis);
  metadata.findAndRemove(resourceURI, namespaces.yAxis);
  metadata.findAndRemove(resourceURI, namespaces.columns);
  metadata.findAndRemove(resourceURI, namespaces.operation);
  metadata.findAndRemove(resourceURI, namespaces.layers);
  metadata.findAndRemove(resourceURI, namespaces.basemap);
  metadata.findAndRemove(resourceURI, namespaces.extent);
  metadata.findAndRemove(resourceURI, namespaces.wmsBaseURI);
  metadata.findAndRemove(resourceURI, namespaces.initialView);
  metadata.findAndRemove(resourceURI, namespaces.legendControl);
  metadata.findAndRemove(resourceURI, namespaces.scaleLineControl);
  metadata.findAndRemove(resourceURI, namespaces.featureTitle);

  if (chartType === WMS_FORMAT) {
    try {
      const stringifiedLayers = JSON.stringify(layers);
      const stringifiedExtent = JSON.stringify(extent);
      const stringifiedInitialView = JSON.stringify(initialView);

      metadata.addL(resourceURI, namespaces.layers, stringifiedLayers);

      metadata.addL(resourceURI, namespaces.extent, stringifiedExtent);

      metadata.addL(
        resourceURI,
        namespaces.initialView,
        stringifiedInitialView
      );

      metadata.addL(resourceURI, namespaces.wmsBaseURI, wmsBaseURI);
      metadata.addD(resourceURI, namespaces.basemap, basemap, 'xsd:boolean');

      metadata.addD(
        resourceURI,
        namespaces.legendControl,
        legendControl,
        'xsd:boolean'
      );

      metadata.addD(
        resourceURI,
        namespaces.scaleLineControl,
        scaleLineControl,
        'xsd:boolean'
      );
    } catch (error) {
      throw Error(`Unable to save layers: ${error}`);
    }
  }

  if (chartType !== WMS_FORMAT) {
    metadata.addL(resourceURI, namespaces.encoding, encoding);
  }

  if (chartType === 'table') {
    try {
      const stringifiedColumns = JSON.stringify(columns);
      metadata.addL(resourceURI, namespaces.columns, stringifiedColumns);
    } catch (error) {
      throw Error(`Unable to save column fields: ${error}`);
    }
  }

  if (chartType === 'line' || chartType === 'map') {
    metadata.addL(resourceURI, namespaces.xAxis, xField);
    metadata.addL(resourceURI, namespaces.yAxis, yField);
  }

  if (chartType === 'map') {
    metadata.addL(resourceURI, namespaces.featureTitle, featureTitle);
    metadata.addL(resourceURI, namespaces.scaleLineControl, scaleLineControl);

    const stringifiedInitialView = JSON.stringify(initialView);
    metadata.addL(resourceURI, namespaces.initialView, stringifiedInitialView);
  }

  if (chartType === 'bar') {
    metadata.addL(resourceURI, namespaces.xAxis, xField);

    if (yField) {
      metadata.addL(resourceURI, namespaces.yAxis, yField);
    }

    if (operation) {
      metadata.addL(resourceURI, namespaces.operation, operation);
    }
  }
  return visualizationEntry;
};

/**
 * Gets the visualization source URI
 *
 * @param {Entry} visualizationEntry
 * @returns {string|undefined}
 */
export const getVisualizationSourceURI = (visualizationEntry) => {
  if (!visualizationEntry) return;
  const resourceURI = visualizationEntry.getResourceURI();
  const metadata = visualizationEntry.getMetadata();
  return metadata.findFirstValue(resourceURI, 'dcterms:source');
};

/**
 * Get wms visualization fields
 *
 * @param {Entry} visualizationEntry
 * @returns {object}
 */
const getWmsFieldsFromMetadata = (visualizationEntry) => {
  const resourceURI = visualizationEntry.getResourceURI();
  const metadata = visualizationEntry.getMetadata();
  const titles = metadata
    .find(resourceURI, 'dcterms:title')
    .map(statementToTitleItem);
  const sourceURI = metadata.findFirstValue(resourceURI, 'dcterms:source');

  const layersSettings = metadata.findFirstValue(
    resourceURI,
    namespaces.layers
  );
  const mapExtent = metadata.findFirstValue(resourceURI, namespaces.extent);
  const mapInitialView = metadata.findFirstValue(
    resourceURI,
    namespaces.initialView
  );

  const wmsFields = {
    chartType: DEFAULT_WMS_VISUALIZATION_STATE.chartType,
    titles,
    layers: DEFAULT_WMS_VISUALIZATION_STATE.layers,
    extent: DEFAULT_WMS_VISUALIZATION_STATE.extent,
    wmsBaseURI:
      metadata.findFirstValue(resourceURI, namespaces.wmsBaseURI) ||
      DEFAULT_WMS_VISUALIZATION_STATE.wmsBaseURI,
    sourceURI,
    basemap:
      metadata.findFirstValue(resourceURI, namespaces.basemap) ||
      DEFAULT_WMS_VISUALIZATION_STATE.basemap,
    initialView: DEFAULT_WMS_VISUALIZATION_STATE.initialView,
    legendControl:
      metadata.findFirstValue(resourceURI, namespaces.legendControl) ||
      DEFAULT_WMS_VISUALIZATION_STATE.legendControl,
    scaleLineControl:
      metadata.findFirstValue(resourceURI, namespaces.scaleLineControl) ||
      DEFAULT_WMS_VISUALIZATION_STATE.scaleLineControl,
  };

  try {
    const layers = JSON.parse(layersSettings);
    const extent = JSON.parse(mapExtent);
    const initialView = JSON.parse(mapInitialView);
    return {
      ...wmsFields,
      layers,
      extent,
      initialView,
    };
  } catch (error) {
    console.log(error);
    return wmsFields;
  }
};

/**
 * Get csv visualization fields
 *
 * @param {Entry} visualizationEntry
 * @returns {object}
 */
const getCsvFieldsFromMetadata = (visualizationEntry) => {
  const resourceURI = visualizationEntry.getResourceURI();
  const metadata = visualizationEntry.getMetadata();
  const chartType = metadata.findFirstValue(resourceURI, namespaces.chartType);
  const titles = metadata
    .find(resourceURI, 'dcterms:title')
    .map(statementToTitleItem);
  const sourceURI = metadata.findFirstValue(resourceURI, 'dcterms:source');

  /**
   * For existing visualizationEntries:
   * If changing from wms to csv, set chartType to the default value
   */
  const csvFields = {
    chartType:
      chartType === WMS_FORMAT
        ? DEFAULT_WMS_VISUALIZATION_STATE.chartType
        : chartType,
    titles,
    sourceURI,
    encoding:
      (
        metadata.findFirstValue(resourceURI, namespaces.encoding) ||
        DEFAULT_WMS_VISUALIZATION_STATE.encoding
      ).toUpperCase() || 'UTF-8',
    xField:
      metadata.findFirstValue(resourceURI, namespaces.xAxis) ||
      DEFAULT_WMS_VISUALIZATION_STATE.xField,
    yField:
      metadata.findFirstValue(resourceURI, namespaces.yAxis) ||
      DEFAULT_WMS_VISUALIZATION_STATE.yField,
    operation:
      metadata.findFirstValue(resourceURI, namespaces.operation) ||
      DEFAULT_WMS_VISUALIZATION_STATE.operation,
    columns: DEFAULT_WMS_VISUALIZATION_STATE.columns,
    featureTitle:
      metadata.findFirstValue(resourceURI, namespaces.featureTitle) ||
      DEFAULT_WMS_VISUALIZATION_STATE.featureTitle,
    scaleLineControl:
      metadata.findFirstValue(resourceURI, namespaces.scaleLineControl) ||
      DEFAULT_WMS_VISUALIZATION_STATE.scaleLineControl,
  };

  const columnFields = metadata.findFirstValue(resourceURI, namespaces.columns);

  if (chartType === 'table') {
    try {
      const columns = JSON.parse(columnFields);
      return { ...csvFields, columns };
    } catch (error) {
      console.error(error);
      return {
        ...csvFields,
        columns: DEFAULT_WMS_VISUALIZATION_STATE.columns,
      };
    }
  }

  if (chartType === 'map') {
    try {
      const mapInitialView = metadata.findFirstValue(
        resourceURI,
        namespaces.initialView
      );
      const initialView = JSON.parse(mapInitialView);
      return { ...csvFields, initialView };
    } catch (error) {
      console.error(error);
      return csvFields;
    }
  }

  return csvFields;
};

/**
 * Get the type of visualization
 *
 * @param {Entry} visualizationEntry
 * @returns {string|undefined}
 */
export const getVisualizationType = (visualizationEntry) => {
  if (!visualizationEntry) return;
  const resourceURI = visualizationEntry.getResourceURI();
  const metadata = visualizationEntry.getMetadata();
  return metadata.findFirstValue(resourceURI, namespaces.chartType);
};

/**
 *
 * @param {Entry} datasetEntry
 * @param {Entry} visualizationEntry
 * @returns {Promise}
 */
export const removeVisualization = async (datasetEntry, visualizationEntry) => {
  const datasetResourceURI = datasetEntry.getResourceURI();
  const visualizationResourceURI = visualizationEntry.getResourceURI();
  datasetEntry
    .getMetadata()
    .findAndRemove(
      datasetResourceURI,
      'schema:diagram',
      visualizationResourceURI
    );
  await datasetEntry.commitMetadata();
  return visualizationEntry.del();
};

/**
 *
 * @param {Entry[]} visualizationsEntries
 * @param {Entry} datasetEntry
 * @returns {Promise}
 */
export const removeVisualizationsEntriesFromDataset = async (
  visualizationsEntries,
  datasetEntry
) => {
  const { metadata, ruri } = spreadEntry(datasetEntry);

  visualizationsEntries
    .map((visualizationEntry) => visualizationEntry.getResourceURI())
    .forEach((visualizationResourceURI) => {
      metadata.findAndRemove(ruri, 'schema:diagram', visualizationResourceURI);
    });

  await datasetEntry.commitMetadata();

  return Promise.all(
    visualizationsEntries.map((visualizationEntry) => visualizationEntry.del())
  );
};
/**
 *
 * @param {Entry} visualizationEntry
 * @returns {Promise}
 */
const getFileEntryFromVisualization = async (visualizationEntry) => {
  const { metadata, ruri: resourceURI } = spreadEntry(visualizationEntry);
  const fileEntryResourceURI = metadata.findFirstValue(
    resourceURI,
    'dcterms:source'
  );
  if (fileEntryResourceURI.toLowerCase().includes(WMS_FORMAT)) return;
  return entrystoreUtil.getEntryByResourceURI(fileEntryResourceURI);
};

/**
 * Gets the distribution related to a wms visualization
 *
 * @param {Entry} visualizationEntry
 * @param {Entry[]} distributions
 * @returns {Entry}
 */
const getRelatedWmsDistribution = (visualizationEntry, distributions) => {
  const visualizationSourceURL = getResourceURI(
    visualizationEntry,
    'dcterms:source'
  );
  const relatedDistribution = distributions.filter((distributionEntry) => {
    const distributionAccessURL = getResourceURI(
      distributionEntry,
      DCAT_ACCESSURL
    );
    return distributionAccessURL === visualizationSourceURL;
  });

  return relatedDistribution[0];
};

/**
 *
 * @param {string} field
 * @param {string[]} fields
 * @returns {boolean}
 */
const checkFieldMismatch = (field, fields) => {
  return Boolean(field && !fields.includes(field));
};

/**
 * Retrieves distribution entries compatible with visualizations.
 *
 * @param {Entry} datasetEntry
 * @returns {Promise<object>}
 */
const getDistributionsForVisualizations = async (datasetEntry) => {
  const includeWMSDistributions = config.get(
    'catalog.includeMapServiceVisualizations'
  );
  const distributionEntries = await getDistributions(datasetEntry);
  const csvDistributionEntries = filterFileDistributionEntries(
    distributionEntries,
    CSV_VALID_FORMATS
  );
  const wmsDistributionEntries = includeWMSDistributions
    ? distributionEntries.filter((distributionEntry) =>
        isWMSDistribution(distributionEntry)
      )
    : [];

  return { csvDistributionEntries, wmsDistributionEntries };
};

/**
 * Gets a wms source type info
 *
 * @param {Entry} distributionEntry
 * @returns {object}
 */
export const getWmsChoice = (distributionEntry) => ({
  name:
    getLabel(distributionEntry) ||
    getResourceURI(distributionEntry, 'dcat:accessURL'),
  sourceURI: getResourceURI(distributionEntry, 'dcat:accessURL'),
  format: WMS_FORMAT,
});

/**
 * Gets a list of csv type source
 *
 * @param {Entry} distributionEntry
 * @param {object[]} fileEntries
 * @param {Function} translate
 * @returns {object[]}
 */
const getCsvChoices = (distributionEntry, fileEntries, translate) => {
  const fileResourceURIs = getDistributionFileRURIs(distributionEntry);
  const distributionTitle = getDistributionTitle(distributionEntry, translate);
  const csvSourceItems = [];
  fileEntries.forEach((fileEntry) => {
    if (fileResourceURIs.includes(fileEntry.getResourceURI())) {
      const csvSourceItem = {
        name: `${getLabel(fileEntry)} - ${distributionTitle}`,
        sourceURI: fileEntry.getResourceURI(),
        format: CSV_FORMAT,
        fileEntry,
      };
      csvSourceItems.push(csvSourceItem);
    }
  });
  return csvSourceItems;
};

/**
 *
 * @param {object} distributionsByType
 * @param {Entry[]} fileEntries
 * @param {Function} translate
 * @returns {object[]}
 */
const getSourceChoices = (distributionsByType, fileEntries, translate) => {
  const sourceChoices = [];
  const { csvDistributionEntries, wmsDistributionEntries } =
    distributionsByType;

  if (csvDistributionEntries) {
    const csvSources = csvDistributionEntries
      .map((csvDistributionEntry) =>
        getCsvChoices(csvDistributionEntry, fileEntries, translate)
      )
      .flat();
    sourceChoices.push(...csvSources);
  }
  if (wmsDistributionEntries) {
    const wmsChoices = wmsDistributionEntries.map((distributionEntry) =>
      getWmsChoice(distributionEntry)
    );
    sourceChoices.push(...wmsChoices);
  }
  return sourceChoices;
};

/**
 * Find selected choice from uri
 *
 * @param {string} selectedURI
 * @param {object[]} sourceChoices
 * @returns {object}
 */
export const getSelectedSourceChoice = (selectedURI, sourceChoices) => {
  return sourceChoices.find((choice) => choice.sourceURI === selectedURI);
};

/**
 * Find source from visualization entry and get matching source choice
 *
 * @param {Entry} visualizationEntry
 * @param {object[]} sourceChoices
 * @returns {object}
 */
const getInitialSelection = (visualizationEntry, sourceChoices) => {
  if (!visualizationEntry) return {};
  const sourceURI = getVisualizationSourceURI(visualizationEntry);
  const selectedSourceChoice = getSelectedSourceChoice(
    sourceURI,
    sourceChoices
  );
  return selectedSourceChoice;
};

/**
 * Get initial state from visualization metadata
 *
 * @param {string} format
 * @param {Entry} visualizationEntry
 * @returns {object}
 */
const getFieldsFromMetadata = (format, visualizationEntry) => {
  if (format === 'csv') return getCsvFieldsFromMetadata(visualizationEntry);
  if (format === 'wms') return getWmsFieldsFromMetadata(visualizationEntry);
  return getDefaultFieldsState();
};

/**
 * Load initial fields state and distribution sources
 *
 * @param {Entry} datasetEntry
 * @param {Entry} visualizationEntry
 * @param {Function} translate
 * @returns {Promise<object>}
 */
export const getVisualizationData = async (
  datasetEntry,
  visualizationEntry,
  translate
) => {
  let errors;
  const distributionEntriesByType =
    await getDistributionsForVisualizations(datasetEntry);
  const csvFileEntries = await getCsvFileEntries(datasetEntry);
  const sourceChoices = getSourceChoices(
    distributionEntriesByType,
    csvFileEntries,
    translate
  );

  const selectedSourceChoice = getInitialSelection(
    visualizationEntry,
    sourceChoices
  );

  if (visualizationEntry && !selectedSourceChoice) {
    errors = { sourceURI: translate('loadSourceError') };
  }

  const initialFields = getFieldsFromMetadata(
    selectedSourceChoice?.format,
    visualizationEntry,
    true
  );

  return {
    errors,
    sourceChoices,
    selectedSourceChoice,
    initialFields,
  };
};

/**
 * Validate csv to make sure there are no broken fields due to changes in the
 * source.
 *
 * @param {object} fieldsState
 * @param {object} csvData
 * @returns {object|undefined}
 */
export const validateCsvFields = (fieldsState, csvData) => {
  const validationFieldNames = ['xField', 'yField'];
  const fields = csvData.meta?.fields || [];

  let errors;
  for (const fieldName of validationFieldNames) {
    const fieldMismatch = checkFieldMismatch(fieldsState[fieldName], fields);
    if (fieldMismatch) {
      if (!errors) {
        errors = {};
      }
      errors[fieldName] = 'Field mismatch';
    }
  }
  return errors;
};

/**
 *
 * @param {Entry} datasetEntry
 * @param {Entry} visualizationEntry
 * @returns {Promise<object>}
 */
export const getPreviewData = async (datasetEntry, visualizationEntry) => {
  const format =
    getVisualizationType(visualizationEntry) === WMS_FORMAT
      ? WMS_FORMAT
      : CSV_FORMAT;
  const fields = getFieldsFromMetadata(format, visualizationEntry);
  const previewData = { format, fields };

  // handle wms
  if (format === WMS_FORMAT) {
    const distributions = await getDistributions(datasetEntry);
    const wmsDistribution = getRelatedWmsDistribution(
      visualizationEntry,
      distributions
    );
    if (!wmsDistribution) {
      previewData.errors = { sourceURI: 'loadSourceError' };
      console.log(previewData.errors);
    }
    return previewData;
  }
  // handle csv file
  const { encoding } = fields;
  const fileEntry = await getFileEntryFromVisualization(visualizationEntry);
  const csvData = await fetchCsv(fileEntry.getResourceURI(), {
    encoding,
  });
  const errors = validateCsvFields(fields, csvData);
  if (errors) {
    previewData.errors = errors;
    console.log(errors);
  }
  previewData.csvData = csvData;
  previewData.csvFields = csvData.meta.fields;
  previewData.fileEntry = fileEntry;
  return previewData;
};
