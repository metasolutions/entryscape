import {
  ACTION_EDIT,
  ACTION_REVISIONS,
} from 'commons/components/overview/actions';
import { LIST_ACTION_SHARING_SETTINGS } from 'commons/components/EntryListView/actions';
import { hasAdminRights } from 'commons/util/user';
import Lookup from 'commons/types/Lookup';
import { withOverviewRefresh } from 'commons/components/overview/hooks/useOverviewModel';
import { withListModelProvider } from 'commons/components/ListView';
import { shouldUseUriPattern } from 'commons/util/store';
import { canWriteMetadata } from 'commons/util/entry';
import RemoveContextDialog from 'commons/components/context/RemoveContextDialog';
import UpdateProjectURI from 'workbench/projects/dialogs/UpdateURI';
import ConfigureEntityTypesDialog from 'workbench/projects/dialogs/ConfigureEntityTypes';
import EditProject from 'workbench/projects/dialogs/EditProject';

const ACTION_EDIT_CONTEXT_NAME = 'edit-name';
const ACTION_CONFIGURE_ENTITYTYPES = 'configure-entitytypes';

const WrapperEditDialog = withListModelProvider(EditProject);

const canEditContextMetadata = (entry, userEntry) => {
  if (hasAdminRights(userEntry)) return true;
  return entry.canWriteMetadata();
};

const hasEntityTypeUriPattern = (entry) => {
  const mainEntityType = Lookup.getMainEntityType('workbench');
  return mainEntityType ? shouldUseUriPattern(mainEntityType, entry) : false;
};

const shouldProjectUseUriPattern = (entry) =>
  hasEntityTypeUriPattern(entry) && canWriteMetadata(entry);

const sidebarActions = [
  {
    ...ACTION_EDIT,
    Dialog: withOverviewRefresh(WrapperEditDialog, 'onChange'),
    isVisible: ({ entry, userEntry }) =>
      canEditContextMetadata(entry, userEntry),
  },
  {
    ...ACTION_REVISIONS,
    isVisible: ({ entry, userEntry }) =>
      canEditContextMetadata(entry, userEntry),
  },
  {
    id: ACTION_EDIT_CONTEXT_NAME,
    Dialog: withOverviewRefresh(UpdateProjectURI, 'onChange'),
    isVisible: ({ entry }) => shouldProjectUseUriPattern(entry),
    labelNlsKey: 'editProjectNameTitle',
  },
  {
    id: ACTION_CONFIGURE_ENTITYTYPES,
    Dialog: withOverviewRefresh(ConfigureEntityTypesDialog, 'onChange'),
    isVisible: ({ entry }) => canWriteMetadata(entry),
    labelNlsKey: 'workspaceConfigure',
  },
  LIST_ACTION_SHARING_SETTINGS,
  {
    id: 'remove',
    Dialog: RemoveContextDialog,
    labelNlsKey: 'removeButtonLabel',
    isVisible: ({ entry, userEntry }) =>
      canEditContextMetadata(entry, userEntry),
  },
];

export { sidebarActions };
