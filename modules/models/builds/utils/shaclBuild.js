import { ItemStore } from '@entryscape/rdforms';
import { Graph } from '@entryscape/rdfjson';
import { Context } from '@entryscape/entrystore-js';
import toShacl from 'rdforms-shacl/src/rdforms2shacl';
import URIManager from 'rdforms-shacl/src/URIManager';
import {
  loadContextEntries,
  mapResourceURIToId,
  getIncludedBuildItems,
  getIncludedNamespaces,
  createRDFormsTemplates,
} from './build';

/**
 * Create rdforms template for each dependency item
 *
 * @param {object[]} templates
 * @param {object} namespaces
 * @returns {Promise<object[]>}
 */
const createShapes = async (templates, namespaces) => {
  const itemstore = new ItemStore();
  itemstore.registerBundle({ source: { templates, namespaces } });
  const uriManager = new URIManager('http://example.com/test');
  return toShacl({ itemstore, uriManager, graph: new Graph(), flatten: true });
};

/**
 * Generates a template file compatible with rdforms
 *
 * @param {object[]} buildItems
 * @param {Context[]} excludedContexts
 * @returns {Promise<object>}
 */
export const generateShapes = async (buildItems, excludedContexts = []) => {
  const includedBuildItems = excludedContexts
    ? getIncludedBuildItems(buildItems, excludedContexts)
    : buildItems;
  await loadContextEntries(buildItems); // required to generate rdforms ids based on context name
  const resourceURIToIdMap = mapResourceURIToId(buildItems);
  const templates = await createRDFormsTemplates(
    includedBuildItems,
    resourceURIToIdMap,
    excludedContexts
  );
  const ns = await getIncludedNamespaces(includedBuildItems, excludedContexts);
  const graph = createShapes(templates, ns);

  return graph;
};
