/* eslint-disable max-len */

const INFO_HELP_LINK = {
  label: {
    en: 'Detailed information',
    sv: 'Detaljerad information',
    de: 'Detaillierte informationen',
  },
  links: {
    en: 'https://docs.entryscape.com/content/en/catalog/detailed_information',
    sv: 'https://docs.entryscape.com/sv/catalog/detaljerad_information',
    de: 'https://docs.entryscape.com/de/catalog/information',
  },
};

const INFO_LISTVIEW_HELP_LINK = {
  ...INFO_HELP_LINK,
  highlightId: 'listActionInfo',
};

const INFO_OVERVIEW_HELP_LINK = {
  ...INFO_HELP_LINK,
  highlightId: 'overviewActionInfo',
};

const DOWNLOAD_HELP_LINK = {
  label: {
    en: 'Download',
    sv: 'Ladda ner',
    de: 'Download',
  },
  links: {
    en: 'https://docs.entryscape.com/content/en/catalog/datamaintenance#download',
    sv: 'https://docs.entryscape.com/content/sv/catalog/forvaltning#download',
    de: 'https://docs.entryscape.com/content/de/catalog/datenpflege#download',
  },
};

const CATALOGS_DATASETS_PUBLISH_HELP_LINK = {
  label: {
    en: 'Publishing catalogs and datasets',
    sv: 'Publicera kataloger och datamängder',
    de: 'Veröffentlichung von Katalogen und Datensätzen',
  },
  links: {
    en: 'https://docs.entryscape.com/content/en/catalog/publishing#publishing-catalogs-and-datasets',
    sv: 'https://docs.entryscape.com/content/sv/catalog/publicering#publishing-catalogs-and-datasets',
    de: 'https://docs.entryscape.com/content/de/catalog/veroeffentlichen#publishing-catalogs-and-datasets',
  },
  highlightId: 'listItemPublish',
};

export const CATALOGS_LISTVIEW_HELP = [
  {
    label: {
      en: 'Catalogs',
      sv: 'Kataloger',
      de: 'Kataloge',
    },
    links: {
      en: 'https://docs.entryscape.com/content/en/catalog#overview',
      sv: 'https://docs.entryscape.com/content/sv/catalog#overview',
      de: 'https://docs.entryscape.com/content/de/catalog#overview',
    },
  },
  {
    label: {
      en: 'Create a new catalog',
      sv: 'Skapa en katalog',
      de: 'Erstellen Sie einen neuen Katalog',
    },
    links: {
      en: 'https://docs.entryscape.com/en/catalog#create-a-new-catalog',
      sv: 'https://docs.entryscape.com/sv/catalog#create-a-new-catalog',
      de: 'https://docs.entryscape.com/de/catalog#create-a-new-catalog',
    },
    highlightId: 'listActionCreate',
  },
  {
    label: {
      en: 'Enter more information about the catalog',
      sv: 'Fyll i mer information om katalogen',
      de: 'Geben Sie weitere Informationen zum Katalog ein',
    },
    links: {
      en: 'https://docs.entryscape.com/en/catalog/#enter-more-information-about-the-catalog',
      sv: 'https://docs.entryscape.com/sv/catalog#enter-more-information-about-the-catalog',
      de: 'https://docs.entryscape.com/de/catalog#enter-more-information-about-the-catalog',
    },
    highlightId: 'listActionEdit',
  },
  CATALOGS_DATASETS_PUBLISH_HELP_LINK,
  INFO_LISTVIEW_HELP_LINK,
];

export const CATALOGS_OVERVIEW_HELP = [
  {
    label: {
      en: 'Catalogs',
      sv: 'Kataloger',
      de: 'Kataloge',
    },
    links: {
      en: 'https://docs.entryscape.com/content/en/catalog#overview',
      sv: 'https://docs.entryscape.com/content/sv/catalog#overview',
      de: 'https://docs.entryscape.com/content/de/catalog#overview',
    },
  },
  {
    label: {
      en: 'Permissions and sharing settings',
      sv: 'Behörigheter och delningsinställningar',
      de: 'Berechtigungen und Freigabeeinstellungen',
    },
    links: {
      en: 'https://docs.entryscape.com/en/catalog#permissions-and-sharing-settings',
      sv: 'https://docs.entryscape.com/sv/catalog#permissions-and-sharing-settings',
      de: 'https://docs.entryscape.com/de/catalog#permissions-and-sharing-settings',
    },
    highlightId: 'overviewActionSharing',
  },
  {
    label: {
      en: 'Enter more information about the catalog',
      sv: 'Fyll i mer information om katalogen',
      de: 'Geben Sie weitere Informationen zum Katalog ein',
    },
    links: {
      en: 'https://docs.entryscape.com/en/catalog/#enter-more-information-about-the-catalog',
      sv: 'https://docs.entryscape.com/sv/catalog#enter-more-information-about-the-catalog',
      de: 'https://docs.entryscape.com/de/catalog#enter-more-information-about-the-catalog',
    },
    highlightId: 'overviewActionEdit',
  },
  { ...DOWNLOAD_HELP_LINK, highlightId: 'overviewActionDownload' },
  {
    label: {
      en: 'Embedding',
      sv: 'Inbäddning',
      de: 'Einbetten',
    },
    links: {
      en: 'https://docs.entryscape.com/content/en/catalog/publishing#embedding',
      sv: 'https://docs.entryscape.com/content/sv/catalog/publicering#embedding',
      de: 'https://docs.entryscape.com/content/de/catalog/veroeffentlichen#embedding',
    },
    highlightId: 'overviewActionEmbed',
  },
  INFO_OVERVIEW_HELP_LINK,
  {
    label: {
      en: 'Remove catalog',
      sv: 'Ta bort katalog',
      de: 'Katalog entfernen',
    },
    links: {
      en: 'https://docs.entryscape.com/en/catalog#remove-catalog',
      sv: 'https://docs.entryscape.com/sv/catalog#remove-catalog',
      de: 'https://docs.entryscape.com/de/catalog#remove-catalog',
    },
    highlightId: 'overviewActionRemove',
  },
];

const DATASET_SERIES_HELP_LINK = {
  label: {
    en: 'Dataset series',
    sv: 'Datamängdsserier',
    de: 'Datensatzserien',
  },
  links: {
    en: 'https://docs.entryscape.com/content/en/catalog/datasets#dataset-series',
    sv: 'https://docs.entryscape.com/content/sv/catalog/datamangder#dataset-series',
    de: 'https://docs.entryscape.com/content/de/catalog/datensaetze#dataset-series',
  },
};

const LINK_DOCUMENT_HELP_LINK = {
  label: {
    en: 'Link a document to a dataset or distribution',
    sv: 'Länka till dokument från en datamängd eller distribution',
    de: 'Ein Dokument mit einem Datensatz oder einer Distribution verknüpfen',
  },
  links: {
    en: 'https://docs.entryscape.com/content/en/catalog/documents#link-a-document-to-a-dataset-or-distribution',
    sv: 'https://docs.entryscape.com/content/sv/catalog/dokument#link-a-document-to-a-dataset-or-distribution',
    de: 'https://docs.entryscape.com/content/de/catalog/dokumente#link-a-document-to-a-dataset-or-distribution',
  },
};

export const DATASETS_LISTVIEW_HELP = [
  {
    label: {
      en: 'Create a dataset from scratch',
      sv: 'Skapa datamängd från grunden',
      de: 'Einen Datensatz von Grund auf neu erstellen',
    },
    links: {
      en: 'https://docs.entryscape.com/en/catalog/datasets#create-a-dataset-from-scratch',
      sv: 'https://docs.entryscape.com/sv/catalog/datamangder#create-a-dataset-from-scratch',
      de: 'https://docs.entryscape.com/de/catalog/datensaetze#create-a-dataset-from-scratch',
    },
    highlightId: 'listActionCreate',
  },
  {
    label: {
      en: 'Create dataset from template',
      sv: 'Skapa datamängd från mall',
      de: 'Datensatz aus Vorlage erstellen',
    },
    links: {
      en: 'https://docs.entryscape.com/en/catalog/datasets#create-dataset-from-template',
      sv: 'https://docs.entryscape.com/sv/catalog/datamangder#create-dataset-from-template',
      de: 'https://docs.entryscape.com/de/catalog/datensaetze#create-dataset-from-template',
    },
    highlightId: 'listActionCreate',
  },
  {
    label: {
      en: 'Describing a dataset',
      sv: 'Beskriva datamängd',
      de: 'Einen Datensatz beschreiben',
    },
    links: {
      en: 'https://docs.entryscape.com/en/catalog/datasets#describing-a-dataset',
      sv: 'https://docs.entryscape.com/sv/catalog/datamangder#describing-a-dataset',
      de: 'https://docs.entryscape.com/de/catalog/datensaetze#describing-a-dataset',
    },
    highlightId: 'listActionEdit',
  },
  DATASET_SERIES_HELP_LINK,
  { ...LINK_DOCUMENT_HELP_LINK, highlightId: 'listActionEdit' },
  {
    label: {
      en: 'Create contact point',
      sv: 'Skapa kontaktuppgift',
      de: 'Kontaktstelle erstellen',
    },
    links: {
      en: 'https://docs.entryscape.com/en/catalog/datasets#create-contact-point',
      sv: 'https://docs.entryscape.com/sv/catalog/datamangder#create-contact-point',
      de: 'https://docs.entryscape.com/de/catalog/datensaetze#create-contact-point',
    },
    highlightId: 'listActionEdit',
  },
  {
    label: {
      en: 'Search for dataset',
      sv: 'Sök datamängd',
      de: 'Nach Datensatz suchen',
    },
    links: {
      en: 'https://docs.entryscape.com/en/catalog/datasets#search-for-dataset',
      sv: 'https://docs.entryscape.com/sv/catalog/datamangder#search-for-dataset',
      de: 'https://docs.entryscape.com/de/catalog/datensaetze#search-for-dataset',
    },
    highlightId: 'listViewSearchField',
  },
  {
    label: {
      en: 'Table view and list view',
      sv: 'Tabellvy och listvy',
      de: 'Tabellenansicht und Listenansicht',
    },
    links: {
      en: 'https://docs.entryscape.com/en/catalog/datasets#table-view-and-list-view',
      sv: 'https://docs.entryscape.com/sv/catalog/datamangder#table-view-and-list-view',
      de: 'https://docs.entryscape.com/de/catalog/datensaetze#table-view-and-list-view',
    },
    highlightId: 'tableViewButton',
  },
];

export const DATASETS_OVERVIEW_HELP = [
  {
    label: {
      en: 'Describing a dataset',
      sv: 'Beskriva datamängd',
      de: 'Einen Datensatz beschreiben',
    },
    links: {
      en: 'https://docs.entryscape.com/en/catalog/datasets#describing-a-dataset',
      sv: 'https://docs.entryscape.com/sv/catalog/datamangder#describing-a-dataset',
      de: 'https://docs.entryscape.com/de/catalog/datensaetze#describing-a-dataset',
    },
    highlightId: 'overviewActionEdit',
  },
  DATASET_SERIES_HELP_LINK,
  { ...LINK_DOCUMENT_HELP_LINK, highlightId: 'overviewActionEdit' },
  {
    label: {
      en: 'Create contact point',
      sv: 'Skapa kontaktuppgift',
      de: 'Kontaktstelle erstellen',
    },
    links: {
      en: 'https://docs.entryscape.com/en/catalog/datasets#create-contact-point',
      sv: 'https://docs.entryscape.com/sv/catalog/datamangder#create-contact-point',
      de: 'https://docs.entryscape.com/de/catalog/datensaetze#create-contact-point',
    },
    highlightId: 'overviewActionEdit',
  },
  { ...CATALOGS_DATASETS_PUBLISH_HELP_LINK, highlightId: 'overviewPublish' },
  INFO_OVERVIEW_HELP_LINK,
  {
    label: {
      en: 'Copy dataset',
      sv: 'Kopiera datamängd',
      de: 'Datensatz kopieren',
    },
    links: {
      en: 'https://docs.entryscape.com/en/catalog/datasets#copy-dataset',
      sv: 'https://docs.entryscape.com/sv/catalog/datamangder#copy-dataset',
      de: 'https://docs.entryscape.com/de/catalog/datensaetze#copy-dataset',
    },
    highlightId: 'overviewActionCopy',
  },
  {
    label: {
      en: 'Remove dataset',
      sv: 'Ta bort datamängd',
      de: 'Datensatz entfernen',
    },
    links: {
      en: 'https://docs.entryscape.com/en/catalog/datasets#remove-dataset',
      sv: 'https://docs.entryscape.com/sv/catalog/datamangder#remove-dataset',
      de: 'https://docs.entryscape.com/de/catalog/datensaetze#remove-dataset',
    },
    highlightId: 'overviewActionRemove',
  },
  {
    label: {
      en: 'Distributions',
      sv: 'Distributioner',
      de: 'Distributionen',
    },
    links: {
      en: 'https://docs.entryscape.com/content/en/catalog/distributions#overview',
      sv: 'https://docs.entryscape.com/content/sv/catalog/distributioner#overview',
      de: 'https://docs.entryscape.com/content/de/catalog/distributionen#overview',
    },
    highlightId: 'distributionsListHeader',
  },
  {
    label: {
      en: 'Create distribution',
      sv: 'Skapa distribution',
      de: 'Distribution erstellen',
    },
    links: {
      en: 'https://docs.entryscape.com/content/en/catalog/distributions#create-distribution',
      sv: 'https://docs.entryscape.com/content/sv/catalog/distributioner#create-distribution',
      de: 'https://docs.entryscape.com/content/de/catalog/distributionen#create-distribution',
    },
    highlightId: 'distributionsListCreate',
  },
  {
    label: {
      en: 'Upload a file',
      sv: 'Ladda upp fil',
      de: 'Datei hochladen',
    },
    links: {
      en: 'https://docs.entryscape.com/content/en/catalog/distributions#upload-a-file',
      sv: 'https://docs.entryscape.com/content/sv/catalog/distributioner#upload-a-file',
      de: 'https://docs.entryscape.com/content/de/catalog/distributionen#upload-a-file',
    },
    highlightId: 'distributionsListCreate',
  },
  {
    label: {
      en: 'Remove, replace or update file',
      sv: 'Ta bort, ersätta eller uppdatera fil',
      de: 'Dateien entfernen, ersetzen und aktualisieren',
    },
    links: {
      en: 'https://docs.entryscape.com/content/en/catalog/distributions#remove-replace-or-update-file',
      sv: 'https://docs.entryscape.com/content/sv/catalog/distributioner#remove-replace-or-update-file',
      de: 'https://docs.entryscape.com/content/de/catalog/distributionen#remove-replace-or-update-file',
    },
  },
  {
    label: {
      en: 'Create an API using tabular data (CSV)',
      sv: 'Skapa ett API från tabelldata (CSV)',
      de: 'Erstellen einer API mithilfe von Tabellendaten (CSV)',
    },
    links: {
      en: 'https://docs.entryscape.com/content/en/catalog/distributions#create-an-api-using-tabular-data',
      sv: 'https://docs.entryscape.com/content/sv/catalog/distributioner#create-an-api-using-tabular-data',
      de: 'https://docs.entryscape.com/content/de/catalog/distributionen#create-an-api-using-tabular-data',
    },
  },
  {
    label: {
      en: 'Activate API for a distribution',
      sv: 'Aktivera API för en distribution',
      de: 'API für eine Distribution aktivieren',
    },
    links: {
      en: 'https://docs.entryscape.com/content/en/catalog/distributions#activate-api-for-a-distribution',
      sv: 'https://docs.entryscape.com/content/sv/catalog/distributioner#activate-api-for-a-distribution',
      de: 'https://docs.entryscape.com/content/de/catalog/distributionen#activate-api-for-a-distribution',
    },
  },
  {
    label: {
      en: 'Link to external API:s or files',
      sv: 'Länka till externa API:n eller filer',
      de: 'Link zu externen APIs oder Dateien',
    },
    links: {
      en: 'https://docs.entryscape.com/content/en/catalog/distributions#link-to-external-apis-or-files',
      sv: 'https://docs.entryscape.com/content/sv/catalog/distributioner#link-to-external-apis-or-files',
      de: 'https://docs.entryscape.com/content/de/catalog/distributionen#link-to-external-apis-or-files',
    },
  },
  DOWNLOAD_HELP_LINK,
  {
    label: {
      en: 'Visualizations',
      sv: 'Visualiseringar',
      de: 'Visualisierungen',
    },
    links: {
      en: 'https://docs.entryscape.com/content/en/catalog/distributions#visualizations',
      sv: 'https://docs.entryscape.com/content/sv/catalog/distributioner#visualizations',
      de: 'https://docs.entryscape.com/content/de/catalog/distributionen#visualizations',
    },
    highlightId: 'visualizationsListHeader',
  },
];

const DATA_SERVICES_OVERVIEW_HELP_LINK = {
  label: {
    en: 'Data services',
    sv: 'Datatjänster',
    de: 'Datendienste',
  },
  links: {
    en: 'https://docs.entryscape.com/content/en/catalog/dataservices#overview',
    sv: 'https://docs.entryscape.com/content/sv/catalog/datatjanster#overview',
    de: 'https://docs.entryscape.com/content/de/catalog/datendienste#overview',
  },
};

export const DATA_SERVICES_LISTVIEW_HELP = [
  DATA_SERVICES_OVERVIEW_HELP_LINK,
  {
    label: {
      en: 'Create data service',
      sv: 'Skapa datatjänst',
      de: 'Datendienst erstellen',
    },
    links: {
      en: 'https://docs.entryscape.com/content/en/catalog/dataservices#create-data-service',
      sv: 'https://docs.entryscape.com/content/sv/catalog/datatjanster#create-data-service',
      de: 'https://docs.entryscape.com/content/de/catalog/datendienste#create-data-service',
    },
    highlightId: 'listActionCreate',
  },
  INFO_LISTVIEW_HELP_LINK,
  {
    label: {
      en: 'Publish data service',
      sv: 'Publicera datatjänst',
      de: 'Datendienst veröffentlichen',
    },
    links: {
      en: 'https://docs.entryscape.com/content/en/catalog/dataservices#publish-data-service',
      sv: 'https://docs.entryscape.com/content/sv/catalog/datatjanster#publish-data-service',
      de: 'https://docs.entryscape.com/content/de/catalog/datendienste#publish-data-service',
    },
    highlightId: 'listItemPublish',
  },
];

export const DATA_SERVICES_OVERVIEW_HELP = [
  DATA_SERVICES_OVERVIEW_HELP_LINK,
  {
    label: {
      en: 'Edit or remove data service',
      sv: 'Redigera eller ta bort datatjänst',
      de: 'Datendienst bearbeiten oder entfernen',
    },
    links: {
      en: 'https://docs.entryscape.com/content/en/catalog/dataservices#edit-or-remove-data-service',
      sv: 'https://docs.entryscape.com/content/sv/catalog/datatjanster#edit-or-remove-data-service',
      de: 'https://docs.entryscape.com/content/de/catalog/datendienste#edit-or-remove-data-service',
    },
    highlightId: 'overviewActionEdit',
  },
  INFO_OVERVIEW_HELP_LINK,
];

const SUGGESTIONS_OVERVIEW_HELP_LINK = {
  label: {
    en: 'Suggestions for datasets',
    sv: 'Förslag på datamängder',
    de: 'Vorschläge für Datensätze',
  },
  links: {
    en: 'https://docs.entryscape.com/content/en/catalog/suggestions#suggestions-for-datasets',
    sv: 'https://docs.entryscape.com/content/sv/catalog/forslag#suggestions-for-datasets',
    de: 'https://docs.entryscape.com/content/de/catalog/vorschlaege#suggestions-for-datasets',
  },
};

const SUGGESTIONS_CHECKLIST_HELP_LINK = {
  label: {
    en: 'Fulfilling the requirements checklist',
    sv: 'Klarmarkera förslag genom checklista',
    de: 'Checkliste für die Erfüllung der Anforderungen',
  },
  links: {
    en: 'https://docs.entryscape.com/content/en/catalog/suggestions#fulfilling-the-requirements-checklist',
    sv: 'https://docs.entryscape.com/content/sv/catalog/forslag#fulfilling-the-requirements-checklist',
    de: 'https://docs.entryscape.com/content/de/catalog/vorschlaege#fulfilling-the-requirements-checklist',
  },
  highlightId: 'listItemChecklist',
};

const SUGGESTIONS_COMMENTS_HELP_LINK = {
  label: {
    en: 'Comments',
    sv: 'Kommentarer',
    de: 'Kommentare',
  },
  links: {
    en: 'https://docs.entryscape.com/content/en/catalog/suggestions#comments',
    sv: 'https://docs.entryscape.com/content/sv/catalog/forslag#comments',
    de: 'https://docs.entryscape.com/content/de/catalog/vorschlaege#comments',
  },
  highlightId: 'listItemComments',
};

export const SUGGESTIONS_LISTVIEW_HELP = [
  SUGGESTIONS_OVERVIEW_HELP_LINK,
  {
    label: {
      en: 'Create suggestion',
      sv: 'Skapa förslag',
      de: 'Vorschlag erstellen',
    },
    links: {
      en: 'https://docs.entryscape.com/content/en/catalog/suggestions#create-suggestion',
      sv: 'https://docs.entryscape.com/content/sv/catalog/forslag#create-suggestion',
      de: 'https://docs.entryscape.com/content/de/catalog/vorschlaege#create-suggestion',
    },
    highlightId: 'listActionCreate',
  },
  SUGGESTIONS_CHECKLIST_HELP_LINK,
  SUGGESTIONS_COMMENTS_HELP_LINK,
];
export const SUGGESTIONS_OVERVIEW_HELP = [
  SUGGESTIONS_OVERVIEW_HELP_LINK,
  {
    ...SUGGESTIONS_CHECKLIST_HELP_LINK,
    highlightId: 'overviewActionChecklist',
  },
  {
    ...SUGGESTIONS_COMMENTS_HELP_LINK,
    highlightId: 'overviewButtonComments',
  },
  {
    label: {
      en: 'Create a dataset from a suggestion',
      sv: 'Skapa datamängd av ett förslag',
      de: 'Erstellen Sie einen Datensatz aus einem Vorschlag',
    },
    links: {
      en: 'https://docs.entryscape.com/content/en/catalog/suggestions#create-a-dataset-from-a-suggestion',
      sv: 'https://docs.entryscape.com/content/sv/catalog/forslag#create-a-dataset-from-a-suggestion',
      de: 'https://docs.entryscape.com/content/de/catalog/vorschlaege#create-a-dataset-from-a-suggestion',
    },
    highlightId: 'suggestionsCreateDatasetButton',
  },
  {
    label: {
      en: 'Archive or remove suggestions',
      sv: 'Arkivera eller ta bort förslag',
      de: 'Vorschläge archivieren oder entfernen',
    },
    links: {
      en: 'https://docs.entryscape.com/content/en/catalog/suggestions#archive-or-remove-suggestions',
      sv: 'https://docs.entryscape.com/content/sv/catalog/forslag#archive-or-remove-suggestions',
      de: 'https://docs.entryscape.com/content/de/catalog/vorschlaege#archive-or-remove-suggestions',
    },
    highlightId: 'overviewActionArchive', // TODO https://metasolutions.atlassian.net/browse/ES-3688
  },
];

const SHOWCASES_OVERVIEW_HELP_LINK = {
  label: {
    en: 'Showcases',
    sv: 'Showcases',
    de: 'Showcases',
  },
  links: {
    en: 'https://docs.entryscape.com/content/en/catalog/showcases#overview',
    sv: 'https://docs.entryscape.com/content/sv/catalog/showcases#overview',
    de: 'https://docs.entryscape.com/content/de/catalog/showcases#overview',
  },
};

export const SHOWCASES_LISTVIEW_HELP = [
  SHOWCASES_OVERVIEW_HELP_LINK,
  {
    label: {
      en: 'Create showcase',
      sv: 'Skapa showcase',
      de: 'Showcase erstellen',
    },
    links: {
      en: 'https://docs.entryscape.com/content/en/catalog/showcases#create-showcase',
      sv: 'https://docs.entryscape.com/content/sv/catalog/showcases#create-showcase',
      de: 'https://docs.entryscape.com/content/de/catalog/showcases#create-showcase',
    },
    highlightId: 'listActionCreate',
  },
  {
    label: {
      en: 'Publish showcase',
      sv: 'Publicera showcase',
      de: 'Showcase veröffentlichen',
    },
    links: {
      en: 'https://docs.entryscape.com/content/en/catalog/showcases#publish-showcase',
      sv: 'https://docs.entryscape.com/content/sv/catalog/showcases#publish-showcase',
      de: 'https://docs.entryscape.com/content/de/catalog/showcases#publish-showcase',
    },
    highlightId: 'listItemPublish',
  },
];

export const SHOWCASES_OVERVIEW_HELP = [
  SHOWCASES_OVERVIEW_HELP_LINK,
  {
    label: {
      en: 'Edit or remove showcase',
      sv: 'Redigera eller ta bort',
      de: 'Showcase bearbeiten oder entfernen',
    },
    links: {
      en: 'https://docs.entryscape.com/content/en/catalog/showcases#edit-or-remove-showcase',
      sv: 'https://docs.entryscape.com/content/sv/catalog/showcases#edit-or-remove-showcase',
      de: 'https://docs.entryscape.com/content/de/catalog/showcases#edit-or-remove-showcase',
    },
    highlightId: 'overviewActionEdit',
  },
  INFO_OVERVIEW_HELP_LINK,
];

const IDEAS_OVERVIEW_HELP_LINK = {
  label: {
    en: 'Ideas',
    sv: 'Idéer',
    de: 'Ideen',
  },
  links: {
    en: 'https://docs.entryscape.com/content/en/catalog/ideas#overview',
    sv: 'https://docs.entryscape.com/content/sv/catalog/ideer#overview',
    de: 'https://docs.entryscape.com/content/de/catalog/ideen#overview',
  },
};

export const IDEAS_LISTVIEW_HELP = [
  IDEAS_OVERVIEW_HELP_LINK,
  {
    label: {
      en: 'Create idea',
      sv: 'Skapa idé',
      de: 'Idee erstellen',
    },
    links: {
      en: 'https://docs.entryscape.com/content/en/catalog/ideas#create-idea',
      sv: 'https://docs.entryscape.com/content/sv/catalog/ideer#create-idea',
      de: 'https://docs.entryscape.com/content/de/catalog/ideen#create-idea',
    },
    highlightId: 'listActionCreate',
  },
  {
    label: {
      en: 'Publish idea',
      sv: 'Publicera idé',
      de: 'Idee veröffentlichen',
    },
    links: {
      en: 'https://docs.entryscape.com/content/en/catalog/ideas#publish-idea',
      sv: 'https://docs.entryscape.com/content/sv/catalog/ideer#publish-idea',
      de: 'https://docs.entryscape.com/content/de/catalog/ideen#publish-idea',
    },
    highlightId: 'listItemPublish',
  },
  INFO_LISTVIEW_HELP_LINK,
];

export const IDEAS_OVERVIEW_HELP = [
  IDEAS_OVERVIEW_HELP_LINK,
  {
    label: {
      en: 'Edit or remove idea',
      sv: 'Redigera eller ta bort idé',
      de: 'Idee bearbeiten oder entfernen',
    },
    links: {
      en: 'https://docs.entryscape.com/content/en/catalog/ideas#edit-or-remove-idea',
      sv: 'https://docs.entryscape.com/content/sv/catalog/ideer#edit-or-remove-idea',
      de: 'https://docs.entryscape.com/content/de/catalog/ideen#edit-or-remove-idea',
    },
    highlightId: 'overviewActionEdit',
  },
  INFO_OVERVIEW_HELP_LINK,
];

const DOCUMENTS_OVERVIEW_HELP_LINK = {
  label: {
    en: 'Documents',
    sv: 'Dokument',
    de: 'Dokumente',
  },
  links: {
    en: 'https://docs.entryscape.com/content/en/catalog/documents#overview',
    sv: 'https://docs.entryscape.com/content/sv/catalog/dokument#overview',
    de: 'https://docs.entryscape.com/content/de/catalog/dokumente#overview',
  },
};

export const DOCUMENTS_LISTVIEW_HELP = [
  DOCUMENTS_OVERVIEW_HELP_LINK,
  {
    label: {
      en: 'Import external specification',
      sv: 'Importera extern specifikation',
      de: 'Externe Spezifikation importieren',
    },
    links: {
      en: 'https://docs.entryscape.com/content/en/catalog/documents#import-external-specification',
      sv: 'https://docs.entryscape.com/content/sv/catalog/dokument#import-external-specification',
      de: 'https://docs.entryscape.com/content/de/catalog/dokumente#import-external-specification',
    },
    highlightId: 'listActionImport',
  },
  {
    label: {
      en: 'Create document',
      sv: 'Skapa dokument',
      de: 'Dokument erstellen',
    },
    links: {
      en: 'https://docs.entryscape.com/content/en/catalog/documents#create-document',
      sv: 'https://docs.entryscape.com/content/sv/catalog/dokument#create-document',
      de: 'https://docs.entryscape.com/content/de/catalog/dokumente#create-document',
    },
    highlightId: 'listActionCreate',
  },
  LINK_DOCUMENT_HELP_LINK,
  INFO_LISTVIEW_HELP_LINK,
];
export const DOCUMENTS_OVERVIEW_HELP = [
  DOCUMENTS_OVERVIEW_HELP_LINK,
  {
    label: {
      en: 'Edit, replace or remove document',
      sv: 'Redigera, ersätt eller ta bort dokument',
      de: 'Dokument bearbeiten, ersetzen oder entfernen',
    },
    links: {
      en: 'https://docs.entryscape.com/content/en/catalog/documents#edit-replace-or-remove-document',
      sv: 'https://docs.entryscape.com/content/sv/catalog/dokument#edit-replace-or-remove-document',
      de: 'https://docs.entryscape.com/content/de/catalog/dokumente#edit-replace-or-remove-document',
    },
    highlightId: 'overviewActionEdit',
  },
  LINK_DOCUMENT_HELP_LINK,
  {
    label: {
      en: 'Download document',
      sv: 'Ladda ner',
      de: 'Dokument herunterladen',
    },
    links: {
      en: 'https://docs.entryscape.com/content/en/catalog/documents#download-document',
      sv: 'https://docs.entryscape.com/content/sv/catalog/dokument#download-document',
      de: 'https://docs.entryscape.com/content/de/catalog/dokumente#download-document',
    },
    highlightId: 'overviewActionDownload',
  },
  INFO_OVERVIEW_HELP_LINK,
];

export const STATISTICS_VIEW_HELP = [
  {
    label: {
      en: 'Statistics',
      sv: 'Statistik',
      de: 'Statistik',
    },
    links: {
      en: 'https://docs.entryscape.com/content/en/catalog/statistics#overview',
      sv: 'https://docs.entryscape.com/content/sv/catalog/statistik#overview',
      de: 'https://docs.entryscape.com/content/de/catalog/statistik#overview',
    },
  },
];

const PUBLISHERS_CONTACTS_OVERVIEW_HELP_LINK = {
  label: {
    en: 'Overview',
    sv: 'Översikt',
    de: 'Überblick',
  },
  links: {
    en: 'https://docs.entryscape.com/content/en/catalog/publishers_contacts#overview',
    sv: 'https://docs.entryscape.com/content/sv/catalog/organisationer_kontakter#overview',
    de: 'https://docs.entryscape.com/content/de/catalog/organisationen#overview',
  },
};

const PUBLISHERS_HELP_LINK = {
  label: {
    en: 'Publishers',
    sv: 'Organisationer',
    de: 'Herausgeber',
  },
  links: {
    en: 'https://docs.entryscape.com/content/en/catalog/publishers_contacts#publishers',
    sv: 'https://docs.entryscape.com/content/sv/catalog/organisationer_kontakter#publishers',
    de: 'https://docs.entryscape.com/content/de/catalog/organisationen#publishers',
  },
};

export const PUBLISHERS_OVERVIEW_HELP = [
  PUBLISHERS_CONTACTS_OVERVIEW_HELP_LINK,
  PUBLISHERS_HELP_LINK,
  INFO_OVERVIEW_HELP_LINK,
];

export const PUBLISHERS_LISTVIEW_HELP = [
  PUBLISHERS_CONTACTS_OVERVIEW_HELP_LINK,
  PUBLISHERS_HELP_LINK,
  INFO_LISTVIEW_HELP_LINK,
];

const CONTACTS_HELP_LINK = {
  label: {
    en: 'Contacts',
    sv: 'Kontakter',
    de: 'Kontakte',
  },
  links: {
    en: 'https://docs.entryscape.com/content/en/catalog/publishers_contacts#contacts',
    sv: 'https://docs.entryscape.com/content/sv/catalog/organisationer_kontakter#contacts',
    de: 'https://docs.entryscape.com/content/de/catalog/organisationen#contacts',
  },
};

export const CONTACTS_OVERVIEW_HELP = [
  PUBLISHERS_CONTACTS_OVERVIEW_HELP_LINK,
  CONTACTS_HELP_LINK,
  INFO_OVERVIEW_HELP_LINK,
];
export const CONTACTS_LISTVIEW_HELP = [
  PUBLISHERS_CONTACTS_OVERVIEW_HELP_LINK,
  CONTACTS_HELP_LINK,
  INFO_LISTVIEW_HELP_LINK,
];
