import config from 'config';

/**
 * Returns the actual translated string or template that will be rendered.
 *
 * Translation string definition can have the following format:
 * `"listHeaderCommentsLabel": "Comments~Comments column header for list items in datasets view"`
 * The part left of the colon `:` is the `term`, and the part to its right is the `definition`.
 * The definition can also be split between two parts like in the example above.
 * The actual definition left of the tilde symbol `~`, and an explanation/clarification about its
 * usage to the right.
 *
 * @param {string} localizedString
 * @returns {string}
 */
export const getNlsDefinition = (localizedString) =>
  localizedString.split('~')[0];

/**
 *  Gets the localized string or NLS template from the provided bundle
 *  If NLS profile configuration is provided tries to using the appropriate profile key and
 *  then falls back to the default.
 *
 * @param {string} key - NLS key
 * @param {object} bundle - NLS bundle that contains the key
 * @returns {string} - String or NLS template
 */
export const getLocalizedString = (key, bundle) => {
  const nlsProfile = config.get('theme.nlsProfile');
  if (!nlsProfile) return bundle[key];

  const profileKey = `${key}__${nlsProfile}`;
  return bundle[profileKey] || bundle[key];
};
