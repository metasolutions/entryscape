import { RDF_TYPE_DATASET_SERIES } from 'commons/util/entry';
import CreateDatasetDialog from '../CreateDatasetDialog';

const CreateDatasetSeries = (props) => {
  return (
    <CreateDatasetDialog
      rdfType={RDF_TYPE_DATASET_SERIES}
      nlsKeys={{
        dialogHeader: 'createDatasetSeriesHeader',
        createSuccess: 'createDatasetSeriesSuccess',
        createFailure: 'createDatasetSeriesFail',
      }}
      {...props}
    />
  );
};

export default CreateDatasetSeries;
