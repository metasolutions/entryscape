import React, { useCallback, useEffect, useState } from 'react';
import { namespaces } from '@entryscape/rdfjson';
import { Grid, Paper, Typography } from '@mui/material';
import {
  timeChartBar,
  pieChartOptions,
  barChartOptions,
} from 'status/Overview/chartOptions';
import BarChart from 'commons/components/chart/Bar';
import DoughnutChart from 'commons/components/chart/Doughnut';
import TimeChart from 'commons/components/chart/Time';
import { entrystore } from 'commons/store';
import esreVisualizationNLS from 'status/nls/esreVisualization.nls';
import escoChartNLS from 'commons/nls/escoChart.nls';
import { useTranslation } from 'commons/hooks/useTranslation';
import config from 'config';
import 'status/Overview/index.scss';

namespaces.add('st', 'http://entrystore.org/terms/statistics#');

const extract = (idx, stat) => {
  const _idx = idx || {};
  const md = stat.getMetadata();
  const s = stat.getResourceURI();
  const base = 'http://entrystore.org/terms/statistics#datasets_in_context_';
  md.find(s).forEach((stmt) => {
    const p = stmt.getPredicate();
    if (p.indexOf(base) === 0) {
      const ctxt = p.substr(base.length);
      _idx[ctxt] = _idx[ctxt] || { d: [], t: 0 };
      const a = _idx[ctxt];
      const v = parseInt(stmt.getValue(), 10);
      if (a.d.length > 0) {
        a.d.push(v - a.t);
      } else {
        a.d.push(v);
      }
      a.t = v;
    }
  });
  return _idx;
};

/**
 *
 * @return {Promise<store/Entry|null>}
 */
const getCatalogStatisticsEntries = () => {
  return entrystore
    .newSolrQuery()
    .rdfType('st:CatalogStatistics')
    .limit(30)
    .context(entrystore.getContextById('catalogstatistics'))
    .list()
    .getEntries();
};

const getLastMonthData = (entries) => {
  const today = new Date();
  return entries.map((entry, idx) => {
    const ruri = entry.getResourceURI();
    const psiAmount = entry
      .getMetadata()
      .findFirstValue(ruri, 'st:psiDatasetCount');
    const otherAmount = entry
      .getMetadata()
      .findFirstValue(ruri, 'st:otherDatasetCount');

    const date = new Date(today);
    date.setDate(today.getDate() - idx);

    return {
      x: date,
      y: parseInt(psiAmount, 10) + parseInt(otherAmount, 10),
    };
  });
};

const getMainStatistics = (entry) => {
  const md = entry.getMetadata();
  const s = entry.getResourceURI();
  const psiDCount = md.findFirstValue(s, 'st:psiDatasetCount');
  const otherDCount = md.findFirstValue(s, 'st:otherDatasetCount');
  const datasetCount = parseInt(psiDCount, 10) + parseInt(otherDCount, 10);
  const idx = extract({}, entry);
  const organizationCount = Object.keys(idx).length;

  return { datasetCount, organizationCount };
};

const useGetPartitions = () => {
  const translate = useTranslation([esreVisualizationNLS]);

  /**
   *
   * @param entry
   * @return {{datasets: {data: number[]}[], labels: *[]}}
   */
  const getPartitions = useCallback(
    (entry) => {
      const metadata = entry.getMetadata();
      const resourceURI = entry.getResourceURI();
      const psiPage = metadata.findFirstValue(resourceURI, 'st:psiPage');
      const psiPageAndDcat = metadata.findFirstValue(
        resourceURI,
        'st:psiPageAndDcat'
      );
      const psiDcat = metadata.findFirstValue(resourceURI, 'st:psiDcat');
      const psiFailed = metadata.findFirstValue(resourceURI, 'st:psiFailed');

      const labels = [
        translate('dcatAndPSILegend', psiPageAndDcat),
        translate('onlyPSILegend', psiPage),
        translate('onlyDcatLegend', psiDcat),
        translate('neitherLegend', psiFailed),
      ];

      return {
        labels,
        datasets: [
          {
            data: [
              parseInt(psiPageAndDcat, 10),
              parseInt(psiPage, 10),
              parseInt(psiDcat, 10),
              parseInt(psiFailed, 10),
            ],
          },
        ],
      };
    },
    [translate]
  );

  return getPartitions;
};

const useGetPublicOrganizations = () => {
  const translate = useTranslation([esreVisualizationNLS]);

  const getPublicOrganizations = useCallback(
    (organizationsEntries) => {
      if (organizationsEntries.length < 3) {
        return Promise.resolve([]);
      }
      const arr = organizationsEntries.slice(organizationsEntries.length - 3);
      const idx = {};
      arr.forEach(extract.bind(this, idx));

      const ctxt2label = {};
      return entrystore
        .newSolrQuery()
        .literalProperty('storepr:merge', 'true')
        .tagLiteral('latest')
        .list()
        .forEach((entry) => {
          ctxt2label[entry.getContext().getId()] = entry
            .getMetadata()
            .findFirstValue(null, 'dcterms:title');
        })
        .then(() => {
          const organizations = [];
          Object.keys(idx).forEach((key) => {
            const l = ctxt2label[key];
            // If no label found, the organization has no datasets
            // in the latest index => ignore.
            if (l) {
              const { t } = idx[key];
              organizations.push({ l: `${l} (${t})`, t, d: idx[key].d });
            }
          });

          organizations.sort((organization1, organization2) => {
            if (organization1.t > organization2.t) return -1;
            if (organization1.t < organization2.t) return 1;
            return 0;
          });

          const max = 20;
          const labels = organizations.map((o) => o.l).slice(0, max);

          return {
            datasets: [
              {
                barThickness: 7,
                label: translate('datasetsPerOrg'),
                data: organizations.map((organization) => organization?.t || 0),
              },
            ],
            labels,
          };
        });
    },
    [translate]
  );

  return getPublicOrganizations;
};

const GROWTH_CHART_ID = 'growthChart';
const STATUS_CHART_ID = 'statusChart';
const RANKING_CHART_ID = 'rankingChart';
const CHARTS = [GROWTH_CHART_ID, STATUS_CHART_ID, RANKING_CHART_ID];

const StatusOverview = () => {
  const t = useTranslation([esreVisualizationNLS, escoChartNLS]);
  const getPartitions = useGetPartitions();
  const getPublicOrganizations = useGetPublicOrganizations();
  const [datasetsCount, setDatasetsCount] = useState(0);
  const [organizationsCount, setOrganizationsCount] = useState(0);
  const [lastMonthData, setLastMonthData] = useState({});
  const [publicOrganizations, setPublichOrganizations] = useState({});
  const [partitions, setPartitions] = useState({});
  const excludedCharts = config.get('status.excludeCharts');
  const visibleCharts = CHARTS.filter(
    (chartId) => !excludedCharts.includes(chartId)
  );

  useEffect(() => {
    getCatalogStatisticsEntries().then((entries) => {
      if (entries.length > 0) {
        const { datasetCount, organizationCount } = getMainStatistics(
          entries[0]
        );
        setDatasetsCount(datasetCount);
        setOrganizationsCount(organizationCount);
        setLastMonthData({
          datasets: [
            {
              label: t('datasetCount'),
              data: getLastMonthData(entries),
              fill: false,
            },
          ],
        });
        setPartitions(getPartitions(entries[0]));
        entries.reverse(); // earliest to latest
        getPublicOrganizations(entries).then(setPublichOrganizations);
      }
    });
  }, [getPartitions, getPublicOrganizations, t]);

  return (
    <Grid
      container
      direction="column"
      justifyContent="flex-start"
      alignItems="flex-start"
      spacing={3}
    >
      <Grid item container direction="row" spacing={2} xs={12}>
        <Grid item classes={{ root: 'esreStatusCard' }}>
          <Typography className="esreStatusCard__value esreStatusCard--background-color">
            {datasetsCount}
          </Typography>
          <Typography className="esreStatusCard__label esreStatusCard--background-color">
            {t('datasetsTitle')}
          </Typography>
        </Grid>
        <Grid item classes={{ root: 'esreStatusCard' }}>
          <Typography className="esreStatusCard__value esreStatusCard--background-color">
            {organizationsCount}
          </Typography>
          <Typography className="esreStatusCard__label esreStatusCard--background-color">
            {t('organizationsTitle')}
          </Typography>
        </Grid>
      </Grid>
      <Grid item container spacing={3} direction="row" xs={12}>
        {visibleCharts.includes(GROWTH_CHART_ID) ? (
          <Grid
            item
            xs={12}
            xl={visibleCharts.includes(STATUS_CHART_ID) ? 6 : 12}
          >
            <Paper classes={{ root: 'esreStatusCard__charts' }} elevation={0}>
              <Typography variant="h2" className="esreStatusCard__header">
                {t('datasetGrowth')}
              </Typography>
              {lastMonthData.datasets &&
              lastMonthData.datasets[0].data.length ? (
                <TimeChart
                  data={lastMonthData}
                  type="line"
                  offset={false}
                  timeUnit="day"
                  dimensions={{ height: '400px' }}
                  options={timeChartBar}
                />
              ) : (
                <Typography>{t('noDataMessage')}</Typography>
              )}
            </Paper>
          </Grid>
        ) : null}
        {visibleCharts.includes(STATUS_CHART_ID) ? (
          <Grid
            item
            xs={12}
            xl={visibleCharts.includes(GROWTH_CHART_ID) ? 6 : 12}
          >
            <Paper classes={{ root: 'esreStatusCard__charts' }} elevation={0}>
              <Typography variant="h2" className="esreStatusCard__header">
                {t('orgStatus')}
              </Typography>
              {partitions.datasets && partitions.datasets[0].data.length ? (
                <DoughnutChart
                  data={partitions}
                  type="pie"
                  labelFontSize={13}
                  dimensions={{ height: `calc(100% - 45px)` }}
                  options={pieChartOptions}
                />
              ) : (
                <Typography>{t('noDataMessage')}</Typography>
              )}
            </Paper>
          </Grid>
        ) : null}
      </Grid>
      {visibleCharts.includes(RANKING_CHART_ID) ? (
        <Grid container item spacing={3} xs={12}>
          <Grid item xs={12}>
            <Paper classes={{ root: 'esreStatusCard__charts' }} elevation={0}>
              <Typography variant="h2" className="esreStatusCard__header">
                {t('topOrganizations')}
              </Typography>
              {publicOrganizations.datasets &&
              publicOrganizations.datasets[0].data.length ? (
                <BarChart
                  data={publicOrganizations}
                  type="horizontalBar"
                  dimensions={{ height: '800px' }}
                  options={barChartOptions}
                />
              ) : (
                <Typography>{t('noDataMessage')}</Typography>
              )}
            </Paper>
          </Grid>
        </Grid>
      ) : null}
    </Grid>
  );
};

export default StatusOverview;
