import { useCallback } from 'react';
import CollectionsIcon from '@mui/icons-material/Collections';
import { Avatar } from '@mui/material';
import { useESContext } from 'commons/hooks/useESContext';
import { RDF_TYPE_COLLECTION } from 'commons/util/entry';
import { entrystore } from 'commons/store';
import { spreadEntry } from 'commons/util/store';
import { useTranslation } from 'commons/hooks/useTranslation';
import esteCollectionNLS from 'terms/nls/esteCollection.nls';
import { isEnhancedTerminology } from 'terms/utils';
import { getPathFromViewName } from 'commons/util/site';
import {
  EntryListView,
  useSolrQuery,
  TITLE_COLUMN,
  MODIFIED_COLUMN,
  ACTIONS_GROUP_COLUMN,
} from 'commons/components/EntryListView';
import {
  LIST_ACTION_EDIT,
  LIST_ACTION_INFO,
} from 'commons/components/EntryListView/actions';
import {
  withListModelProviderAndLocation,
  ListItemIcon,
  listPropsPropType,
} from 'commons/components/ListView';
import { listActions, nlsBundles } from '../actions';

const getMemberLength = (entry) => {
  const { metadata, ruri } = spreadEntry(entry);
  return metadata.find(ruri, 'skos:member').length;
};

const CollectionsView = ({ listProps }) => {
  const translate = useTranslation(esteCollectionNLS);
  const { context, contextEntry } = useESContext();

  const disabledListActions = contextEntry
    ? isEnhancedTerminology(contextEntry)
    : false;

  const updatedListActions = listActions.map((action) =>
    action.id === 'create' && disabledListActions
      ? {
          ...action,
          tooltipNlsKey: 'disabledActionTooltip',
        }
      : action
  );

  const createQuery = useCallback(
    () =>
      entrystore.newSolrQuery().rdfType(RDF_TYPE_COLLECTION).context(context),
    [context]
  );
  const queryResults = useSolrQuery({ createQuery });

  return (
    <EntryListView
      {...queryResults}
      {...listProps}
      nlsBundles={nlsBundles}
      listActions={updatedListActions}
      listActionsProps={{
        nlsBundles,
        disabled: disabledListActions,
      }}
      getListItemProps={({ entry }) => ({
        to: getPathFromViewName('terminology__collections__collection', {
          contextId: entry.getContext().getId(),
          entryId: entry.getId(),
        }),
      })}
      viewPlaceholderProps={{ Icon: CollectionsIcon }}
      columns={[
        {
          id: 'concept-count',
          xs: 1,
          Component: ListItemIcon,
          justifyContent: 'flex-start',
          tooltip: translate('conceptNumberTooltip'),
          getProps: ({ entry }) => ({
            icon: <Avatar>{getMemberLength(entry)}</Avatar>,
          }),
        },
        { ...TITLE_COLUMN, xs: 8 },
        MODIFIED_COLUMN,
        {
          ...ACTIONS_GROUP_COLUMN,
          actions: [LIST_ACTION_INFO, LIST_ACTION_EDIT],
        },
      ]}
    />
  );
};

CollectionsView.propTypes = {
  listProps: listPropsPropType,
};

export default withListModelProviderAndLocation(CollectionsView);
