import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import { useTranslation } from 'commons/hooks/useTranslation';
import esmoDiagramsNLS from 'models/nls/esmoDiagrams.nls';
import escoDialogsNLS from 'commons/nls/escoDialogs.nls';
import DiagramControl from '../DiagramControl';
import './DiagramDialog.scss';
import { useDiagramContext } from '../hooks/useDiagramContext';

const NLS_BUNDLES = [esmoDiagramsNLS, escoDialogsNLS];

const DiagramDialog = ({ open, diagramUtils, onEdit, onUnMaximize }) => {
  const translate = useTranslation(NLS_BUNDLES);
  const { diagramAdapter } = useDiagramContext();
  const [paper, setPaper] = useState();
  const [node, setNode] = useState();

  useEffect(() => {
    if (node) {
      const newPaper = diagramAdapter.createPaper(node);
      newPaper.unfreeze();
      setPaper(newPaper);
    }
  }, [node, diagramAdapter]);

  return (
    <Dialog
      fullScreen
      open={open}
      onClose={onUnMaximize}
      aria-labelledby="diagram-switch-heading"
    >
      <DiagramControl
        diagramUtils={diagramUtils}
        paper={paper}
        onEdit={onEdit}
      />
      <div className="esmoDiagram__fullCanvas" ref={setNode} />
      <Button
        onClick={onUnMaximize}
        variant="text"
        autoFocus
        sx={{ position: 'absolute', bottom: '5px', right: '15px' }}
      >
        {translate('close')}
      </Button>
    </Dialog>
  );
};

DiagramDialog.propTypes = {
  open: PropTypes.bool,
  graph: PropTypes.shape({}),
  onEdit: PropTypes.func,
  onUnMaximize: PropTypes.func,
  diagramUtils: PropTypes.shape({
    createPaper: PropTypes.func,
    fitContent: PropTypes.func,
  }),
};

export default DiagramDialog;
