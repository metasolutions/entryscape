import { useState } from 'react';
import PropTypes from 'prop-types';
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
} from '@mui/material';
import escoEntryChooser from 'commons/nls/escoEntryChooser.nls';
import escoDialogNLS from 'commons/nls/escoDialogs.nls';
import { useTranslation } from 'commons/hooks/useTranslation';
import EntrySelectList from '../EntrySelectList';
import './index.scss';

const EntryChooserDialog = ({ binding, onClose, onSelect }) => {
  const translate = useTranslation([escoDialogNLS, escoEntryChooser]);
  const itemLabel = binding.getItem().getLabel();
  const [open, setOpen] = useState(true);

  const onDialogClose = () => {
    setOpen(false);
    setTimeout(onClose, 500);
  };

  return (
    <Dialog
      fullWidth
      maxWidth="lg"
      open={open}
      onClose={onClose}
      classes={{
        paper: 'escoEntryChooserDialog',
      }}
    >
      <DialogTitle onClose={onClose}>
        {translate('searchForHeader', itemLabel)}
      </DialogTitle>
      <DialogContent>
        <EntrySelectList
          binding={binding}
          onClose={onDialogClose}
          onSelect={onSelect}
        />
      </DialogContent>
      <DialogActions>
        <Button color="primary" onClick={onClose} autoFocus>
          {translate('close')}
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export const showDialog = (binding, onSelect, field) => {
  let child;
  const onClose = () => field.removeChild(child);
  child = (
    <EntryChooserDialog
      key="entrychooser"
      binding={binding}
      onClose={onClose}
      onSelect={onSelect}
    />
  );
  field.appendChild(child);
};

EntryChooserDialog.propTypes = {
  binding: PropTypes.shape({ getItem: PropTypes.func }),
  onSelect: PropTypes.func,
  onClose: PropTypes.func,
};
