import { mergeDatasetColors } from 'commons/components/chart/Chart';

const isValue = (value) => value || value === 0;

const removeEmptyLastLine = (data) => {
  const lastItem = data[data.length - 1];

  const lastItemHasValue = Object.keys(lastItem).some((key) =>
    isValue(lastItem[key])
  );

  if (lastItemHasValue) return data;
  return data.slice(0, data.length - 1);
};

const sumByX = (xyData) =>
  xyData.reduce((currentSum, { x, y }) => {
    if (!(x in currentSum)) {
      currentSum[x] = 0;
    }
    currentSum[x] += parseFloat(y);
    return currentSum;
  }, {});

const sum = (xyData) =>
  Object.entries(sumByX(xyData)).map(([x, y]) => ({ x, y }));

const countByX = (xyData) =>
  xyData.reduce((currentCount, { x }) => {
    if (!(x in currentCount)) {
      currentCount[x] = 0;
    }
    currentCount[x] += 1;
    return currentCount;
  }, {});

const count = (xyData) =>
  Object.entries(countByX(xyData)).map(([x, y]) => ({ x, y }));

const operations = {
  sum,
  count,
};

const transformData = (data, operation) => {
  if (!operation || operation === 'none') return data;
  if (operation in operations) return operations[operation](data);
  throw new Error(`Operation ${operation} is currently not supported`);
};

const getXyData = (data, xField, yField) => {
  const csvData = data.data || data.csv.data;
  return csvData.map(({ [xField]: x, [yField]: y }) => ({ x, y }));
};

export const getChartData = ({
  data: csvData,
  xField,
  yField,
  operation,
  type,
}) => {
  if (!csvData) return;
  const xyData = transformData(
    removeEmptyLastLine(getXyData(csvData, xField, yField)),
    operation
  );

  const labels = xyData.map(({ x }) => (isValue(x) ? x : 'No label'));
  const data = xyData.map(({ y }) => y);
  const colors =
    type === 'pie'
      ? [
          {
            backgroundColor: data.map(
              (_, index) => `hsl(${(index / data.length) * 360}, 50%, 50%)`
            ),
          },
        ]
      : [];

  return {
    datasets: mergeDatasetColors([{ data }], colors),
    labels,
  };
};

/**
 * Parses and validates string values into coordinate numbers
 *
 * @param {string[]} values
 * @returns {number[]|undefined}
 */
const getCoordinates = (values) => {
  const coordinates = values
    .map((value) => parseFloat(value))
    .filter(Number.isFinite);
  if (coordinates.length !== 2) return;
  return coordinates;
};

/**
 *
 * @param {{data: object, xField: string, yField: string}} mapDataParams
 * @returns {object[]|undefined}
 */
export const getMapData = ({ data, xField, yField }) => {
  if (!data) return;
  const { data: csvData } = data;

  // extract coordinate values
  const mapData = csvData
    .map((row) => {
      const coordinates = getCoordinates([row[xField], row[yField]]);
      if (!coordinates) return;
      return {
        geometry: coordinates,
        ...row,
      };
    })
    .filter((row) => row);
  return mapData;
};
