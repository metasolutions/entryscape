import { getFilterPredicatesFromRelations } from './relations';

describe('getFilterPredicatesFromRelations', () => {
  test('should return undefined when entityType is undefined', () => {
    const entityType = undefined;
    const result = getFilterPredicatesFromRelations(entityType);
    expect(result).toBeUndefined();
  });

  test('should return undefined when entityType has no relations', () => {
    const entityType = {
      get: () => undefined,
    };
    const result = getFilterPredicatesFromRelations(entityType);
    expect(result).toBeUndefined();
  });

  test('should return undefined when entityType has an empty relations array', () => {
    const entityType = {
      get: () => [],
    };
    const result = getFilterPredicatesFromRelations(entityType);
    expect(result).toBeUndefined();
  });

  test('should return an object with properties based on relations', () => {
    const entityType = {
      get: () => [
        {
          entityType: 'imagelink',
          type: 'composition',
          property: 'dcterms:relation',
        },
      ],
    };
    const result = getFilterPredicatesFromRelations(entityType);

    // Check if the returned object has the expected properties
    expect(result).toEqual({
      'dcterms:relation': true,
    });
  });
});
