import Papa from 'papaparse';
import { getCatalogLabel } from 'catalog/utils/react/dcat';
import { getStatisticsItems, getStatisticItemsRelevantEntries } from './data';
import timeRange from './timeRange';

const { serializeSelection } = timeRange;

const RESOURCE_TYPE_ALL = 'all';

/**
 *
 * @param timeRangeValue
 * @returns {Promise<{filename: string, data: *}>}
 */
export const getCSVData = async (context, timeRangeValue) => {
  try {
    // gets only the resource URIs of downloads and the counts
    const statisticItems = await getStatisticsItems(
      context,
      RESOURCE_TYPE_ALL,
      timeRangeValue
    );

    // enrich the raw statistics data with relevant dataset/distribution entries titles
    const statisticItemsEnriched = await getStatisticItemsRelevantEntries(
      context,
      statisticItems
    );

    return Papa.unparse(statisticItemsEnriched);
  } catch (error) {
    console.error(error);
    throw new Error(`Failed exporting CSV file caused by: ${error}`);
  }
};

export const getFileName = async (context, timeRangeValue) => {
  const catalogLabel = await getCatalogLabel(context);
  const date = serializeSelection(timeRangeValue);
  return `Statistics - ${catalogLabel} - ${date}.csv`;
};
