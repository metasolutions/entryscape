import { useState, useEffect } from 'react';
import useNavigate from 'commons/components/router/useNavigate';
import { Typography } from '@mui/material';
import { entrystore } from 'commons/store';
import {
  useListQuery,
  withListModelProvider,
} from 'commons/components/ListView';
import {
  EntryListView,
  MODIFIED_COLUMN,
  TITLE_COLUMN,
  INFO_COLUMN,
} from 'commons/components/EntryListView';
import { LIST_ACTION_INFO } from 'commons/components/EntryListView/actions';
import { entriesToListQueryItems } from 'commons/components/ListView/utils/listQueryUtils';
import { useClipboardGraph } from 'commons/contexts/ClipboardGraphContext';
import escoListNLS from 'commons/nls/escoList.nls';
import esreSourceNLS from 'toolkit/nls/esreSource.nls';
import escaDatasetNLS from 'catalog/nls/escaDataset.nls';
import AlertDialog from 'commons/components/common/dialogs/AlertDialog';
import { useTranslation } from 'commons/hooks/useTranslation';
import config from 'config';
import { PENDING, RESOLVED } from 'commons/hooks/useAsync';
import LinkedDataBrowserDialog from 'commons/components/LinkedDataBrowserDialog';
import withLocationChangeListener from '../../LocationChangeListener';

const nlsBundles = [esreSourceNLS, escoListNLS, escaDatasetNLS];

const ExploreView = () => {
  const { navigate } = useNavigate();
  const translate = useTranslation(nlsBundles);
  const [clipboardGraph] = useClipboardGraph();
  const [datasetEntries, setDatasetEntries] = useState(null);
  const clipboardGraphIsValid = clipboardGraph && !clipboardGraph.isEmpty();

  const { result: datasetItems, size } = useListQuery({
    items: datasetEntries ? entriesToListQueryItems(datasetEntries) : null,
  });

  useEffect(() => {
    if (!clipboardGraphIsValid) return;

    const tempContext = entrystore.getContextById('__temporary');
    const entries = clipboardGraph
      .find(null, 'rdf:type', 'dcat:Dataset')
      .map((statement) => {
        const uri = statement.getSubject();
        const datasetEntry = tempContext.newLink(uri);
        datasetEntry.setMetadata(clipboardGraph);
        return datasetEntry;
      });
    setDatasetEntries(entries);
  }, [clipboardGraph, clipboardGraphIsValid]);

  const columns = [
    { ...TITLE_COLUMN, xs: 8 },
    { ...MODIFIED_COLUMN, sortBy: false, xs: 3 },
    {
      ...INFO_COLUMN,
      getProps: ({ entry, translate: t }) => ({
        action: {
          ...LIST_ACTION_INFO,
          entry,
          nlsBundles,
          Dialog: LinkedDataBrowserDialog,
          formTemplateId: config.get('catalog.datasetTemplateId'),
        },
        title: t('infoEntry'),
      }),
    },
  ];

  return (
    <>
      <Typography variant="h2">{translate('exploreHeader')}</Typography>
      {clipboardGraphIsValid ? (
        <EntryListView
          entries={datasetItems.map(({ entry }) => entry)}
          status={datasetEntries ? RESOLVED : PENDING}
          size={size}
          columns={columns}
          nlsBundles={nlsBundles}
          includeDefaultListActions={false}
          getListItemProps={({ entry }) => ({
            action: {
              ...LIST_ACTION_INFO,
              entry,
              formTemplateId: config.get('catalog.datasetTemplateId'),
              maxWidth: 'md',
              nlsBundles,
              Dialog: LinkedDataBrowserDialog,
            },
          })}
        />
      ) : (
        <AlertDialog
          open
          handleClose={() => navigate('/toolkit/catalog')}
          buttonLabel={translate('noRDFProceed')}
        >
          {translate('noRDF')}
        </AlertDialog>
      )}
    </>
  );
};

export default withListModelProvider(withLocationChangeListener(ExploreView));
