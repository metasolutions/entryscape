import { useEffect } from 'react';
import PropTypes from 'prop-types';
import { Entry } from '@entryscape/entrystore-js';
import {
  useTranslation,
  nlsBundlesPropType,
} from 'commons/hooks/useTranslation';
import useGetMainDialog from 'commons/hooks/useGetMainDialog';
import ListRemoveEntryDialog from 'commons/components/EntryListView/dialogs/ListRemoveEntryDialog';
import useAsync from 'commons/hooks/useAsync';

const removeUserFromGroup = (groupEntry, userEntry) =>
  groupEntry
    .getResource(true)
    .removeEntry(userEntry)
    .then(() => {
      userEntry.setRefreshNeeded();
      userEntry.refresh();
    });

const RemoveUserDialog = ({
  entry: userEntry,
  groupEntry,
  updateList,
  nlsBundles,
  ...restProps
}) => {
  const { closeDialog } = restProps;
  const { getAcknowledgeDialog } = useGetMainDialog();
  const { runAsync } = useAsync();
  const t = useTranslation(nlsBundles);
  const isManager = groupEntry
    .getEntryInfo()
    .getACL()
    .admin.includes(userEntry.getResourceURI());

  useEffect(() => {
    if (isManager) {
      runAsync(
        getAcknowledgeDialog({
          content: t('removeManager'),
        }).then(() => closeDialog())
      );
    }
  }, [isManager, closeDialog, t, getAcknowledgeDialog, runAsync]);

  const handleRemove = async () => {
    runAsync(
      removeUserFromGroup(groupEntry, userEntry).then(() => updateList())
    );
  };

  return (
    <>
      {!isManager && (
        <ListRemoveEntryDialog
          // eslint-disable-next-line react/jsx-props-no-spreading
          {...restProps}
          entry={userEntry}
          closeDialog={closeDialog}
          onRemove={handleRemove}
          removeConfirmMessage={t('removeMemberFromGroup')}
        />
      )}
    </>
  );
};

RemoveUserDialog.propTypes = {
  entry: PropTypes.instanceOf(Entry),
  closeDialog: PropTypes.func,
  groupEntry: PropTypes.instanceOf(Entry),
  ignoreEntries: PropTypes.arrayOf(PropTypes.instanceOf(Entry)),
  updateList: PropTypes.func,
  nlsBundles: nlsBundlesPropType,
};

export default RemoveUserDialog;
