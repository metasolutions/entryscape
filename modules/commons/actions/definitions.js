import {
  canWriteMetadata,
  canAdministerEntry,
  hasMetadataRevisions,
  canReadMetadata,
} from 'commons/util/entry';
import {
  ACTION_EDIT_ID,
  ACTION_INFO_ID,
  ACTION_REMOVE_ID,
  ACTION_REVISIONS_ID,
  ACTION_SHARING_SETTINGS_ID,
  ACTION_DOWNLOAD_ID,
} from './actionIds';

export const ACTION_EDIT = {
  id: ACTION_EDIT_ID,
  isVisible: ({ entry }) => canWriteMetadata(entry),
  labelNlsKey: 'editEntry',
};

export const ACTION_INFO = {
  id: ACTION_INFO_ID,
  isVisible: ({ entry }) => canReadMetadata(entry),
  labelNlsKey: 'infoEntry',
};

export const ACTION_REMOVE = {
  id: ACTION_REMOVE_ID,
  isVisible: ({ entry }) => canAdministerEntry(entry),
  labelNlsKey: 'removeEntryLabel',
};

export const ACTION_REVISIONS = {
  id: ACTION_REVISIONS_ID,
  isVisible: ({ entry }) => hasMetadataRevisions(entry),
  labelNlsKey: 'versionsLabel',
};

export const ACTION_SHARING_SETTINGS = {
  id: ACTION_SHARING_SETTINGS_ID,
  labelNlsKey: 'sharingSettingsLabel',
};

export const ACTION_DOWNLOAD = {
  id: ACTION_DOWNLOAD_ID,
  labelNlsKey: 'downloadLabel',
};
