import PropTypes from 'prop-types';
import React from 'react';
import { getPresenter } from 'commons/components/forms/presenters';
import { entryPropType, graphPropType } from 'commons/util/entry';
import {
  useTranslation,
  nlsBundlesPropType,
} from 'commons/hooks/useTranslation';
import { localizeLabel } from 'commons/locale';
import LinkedDataBrowserDialog from 'commons/components/LinkedDataBrowserDialog';

const translateLabel = ({ labelNlsKey, label }, translate) => {
  const localizedLabel = localizeLabel(label);
  if (localizedLabel === null && labelNlsKey) return translate(labelNlsKey);
  return localizedLabel;
};

export const renderFieldsPresenter = ({
  entry,
  graph,
  resourceURI,
  fields,
  translate,
}) => {
  return (
    <>
      {fields.map((field) => {
        const Presenter = getPresenter(field);
        return Presenter ? (
          <Presenter
            key={field.name || field.property}
            graph={graph || entry?.getMetadata?.()}
            resourceURI={resourceURI || entry?.getResourceURI?.()}
            entry={entry}
            {...field}
            label={translateLabel(field, translate)}
            translate={translate}
          />
        ) : null;
      })}
    </>
  );
};

const FieldsLinkedDataBrowserDialog = ({
  entry,
  fields,
  nlsBundles,
  graph,
  resourceURI,
  ...rest
}) => {
  const translate = useTranslation(nlsBundles);

  const renderPresenter = ({ entry: currentEntry, stackIndex }) =>
    stackIndex === 0
      ? renderFieldsPresenter({
          graph,
          resourceURI,
          entry: currentEntry,
          fields,
          translate,
        })
      : null;

  return (
    <LinkedDataBrowserDialog
      {...rest}
      entry={entry}
      renderPresenter={renderPresenter}
    />
  );
};

FieldsLinkedDataBrowserDialog.propTypes = {
  entry: entryPropType,
  fields: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  nlsBundles: nlsBundlesPropType,
  graph: graphPropType,
  resourceURI: PropTypes.string,
};

export default FieldsLinkedDataBrowserDialog;
