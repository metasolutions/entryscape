import * as ns from 'models/utils/ns';
import { ANY_FILTER_ITEM } from 'commons/components/filters/utils/filterDefinitions';
import { TYPE_FILTER } from 'models/utils/filterDefinitions';
import BuildCreateDialog from '../dialogs/BuildCreateDialog';
import ShaclCreateDialog from '../dialogs/ShaclCreateDialog';
import CreateDiagramDialog from '../dialogs/CreateDiagramDialog';
import CreateRDFSDialog from '../dialogs/CreateRDFSDialog';
import CreateResourceDialog from '../dialogs/CreateResourceDialog';

export const createActions = [
  {
    id: 'rdforms-bundles',
    labelNlsKey: 'rdformsBundleLabel',
    action: {
      Dialog: BuildCreateDialog,
    },
  },
  {
    id: 'shacl',
    labelNlsKey: 'shaclLabel',
    action: {
      Dialog: ShaclCreateDialog,
    },
  },
  {
    id: 'diagrams',
    labelNlsKey: 'diagramResourceLabel',
    action: {
      Dialog: CreateDiagramDialog,
    },
  },
  {
    id: 'rdfs',
    labelNlsKey: 'rdfsResourceLabel',
    action: {
      Dialog: CreateRDFSDialog,
    },
  },
  {
    id: 'resource',
    labelNlsKey: 'resourceLabel',
    action: {
      Dialog: CreateResourceDialog,
    },
  },
];

const RESOURCE_TYPE_CHOICES = [
  ANY_FILTER_ITEM,
  { labelNlsKey: 'diagramLabel', value: 'http://purl.org/dc/dcmitype/Image' },
  { labelNlsKey: 'rdfFormsBundleLabel', value: ns.RDF_TYPE_BUNDLES },
  { labelNlsKey: 'rdfsResourceLabel', value: 'owl:Ontology' },
];

export const filters = [
  {
    ...TYPE_FILTER,
    items: RESOURCE_TYPE_CHOICES,
    id: 'resources-filter',
  },
];
