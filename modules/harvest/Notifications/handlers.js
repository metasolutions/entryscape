/**
 * Get the correct options for the selected type of notification
 *
 * Types in notification options can be:
 *  - strict
 *  - lax
 *  - off
 *
 * Values are int ranging from 0 to the total number of notification types
 *
 * @param {array} notificationOptions - Notification types and values
 * @param {number} value - The selected input value
 * @return {object} The option whose value matches the selected input value
 */
const getOptionByValue = (notificationOptions, value) => {
  const opt = notificationOptions.filter((option) => option.value === value);
  return opt[0];
};

/**
 * Get the option that matches the type
 *
 * Types in notification options can be:
 *  - strict
 *  - lax
 *  - off
 * Values are int ranging from 0 to the total number of notification types
 *
 * @param {array} notificationOptions - Notification types and values
 * @param {number} type - The model type
 * @return {object} The option whose value matches the selected input value
 */
const getOptionByType = (notificationOptions, type) => {
  const opt = notificationOptions.filter((option) => option.type === type);
  return opt[0];
};

export { getOptionByValue, getOptionByType };
