/* eslint-disable max-len */
import { DEFAULT_ASSETS_PATH, STATIC_PATH } from 'commons/webpack/paths';

export default {
  /**
   * Appends to window.location.origin to form the baseURL
   *
   * @type {string}
   */
  baseAppPath: '',

  entryscape: {
    // baseURL: '', if set bypasses window.location.origin
    // localBuild: false, to be set in local.js
    // static: { url, app, version }, to be set in local.js

    /**
     * @type {string|string[]}
     */
    extras: [],
    swaggerBaseURL: 'https://swagger.entryscape.com/',
    fontPath: `${STATIC_PATH}/assets/fonts/`,
    maintenance: {
      email: 'support@entryscape.com',
      ongoing: {
        enabled: false,
        message: {
          en: 'EntryScape is offline for maintenance. Please check back later.',
          de: 'EntryScape ist wegen Wartungsarbeiten offline. Bitte versuchen Sie es später erneut.',
          sv: 'EntryScape är offline för underhåll. Vänligen försök igen senare.',
        },
      },
      // planned: {
      //   start: '2022-08-02 02:00',
      //   end: '2022-08-02 08:00',
      //   message: {
      //     en: 'Example planned maintenance warning message in EN',
      //     sv: 'Example planned maintenance warning message in SV',
      //   },
      // },
    },
    support: {
      enabled: true,
      url: 'https://support.entryscape.com',
      label: {
        en: 'Contact support',
        sv: 'Kontakta supporten',
        de: 'Support kontaktieren',
      },
    },
    community: {
      enabled: true,
      url: 'https://community.entryscape.com',
      label: {
        en: 'User forum',
        sv: 'Användarforum',
        de: 'Benutzerforum',
      },
    },
  },
  entrystore: {
    // repository: 'http://localhost:8080/store/', to be set in local.js

    /**
     * The ID corresponding to the premium group. Members of the premium group
     * are exempt from restrictions. If there is no existing group with the ID,
     * it is created upon using any premium action (upgrade/downgrade).
     *
     * @type {string}
     */
    premiumGroupId: '',

    /**
     * Premium contexts are exempt from restrictions. If true the upgrade/downgrade
     * actions are available on the admin projects list items.
     *
     * @type {boolean}
     */
    premiumContexts: false,

    /**
     * @type {boolean}
     */
    internalsignin: true,

    /**
     * @typedef {object} SSOConfig
     * @property {boolean} enabled - Support for SSO
     * @property {string} message - Custom message
     * @property {string} path - Either 'cas' or 'saml'
     * @type {SSOConfig}
     */
    sso: null,

    /**
     * Configuration for the password validation rules. These rules apply in any case of creating or changing a password via the frontend.
     *
     * @property {object} password - Password configuration
     * @property {boolean} password.uppercase - Whether an uppercase character is required
     * @property {boolean} password.lowercase - Whether a lowercase character is required
     * @property {boolean} password.symbol - Whether a symbol is required
     * @property {boolean} password.number - Whether a number is required
     * @property {RegExp[]} password.custom - An array of regular expressions to match against
     * @property {object} password.message - The error message in NLS object form, this message appears whatever the validation error may be
     * @property {number} password.minLength - The minimum password length
     */
    password: {
      uppercase: true,
      lowercase: true,
      number: true,
      symbol: false,
      minLength: 10,
      message: {
        en: 'The password must be at least 10 characters long and contain a number as well as both lower case and upper case characters.',
        de: 'Das Kennwort muss mindestens 10 Zeichen lang sein und sowohl mindestens eine Zahl als auch Klein- und Großbuchstaben enthalten.',
        sv: 'Lösenordet måste vara minst 10 tecken långt och innehålla minst en siffra samt både gemener och versaler.',
      },
    },

    /**
     * @type {boolean}
     */
    authentification: true,

    /**
     * @type {string}
     */
    defaultSolrQuery: 'title',

    /**
     * @type {number}
     */
    defaultSolrLimit: 50,
  },
  rdf: {
    /**
     * @type {object}
     */
    namespaces: {
      /**
       * @memberof namespaces
       * @type {string}
       */
      store: 'http://entrystore.org/terms/',
      esterms: 'http://entryscape.com/terms/',
      /**
       * @memberof namespaces
       * @type {string}
       */
      adms: 'http://www.w3.org/ns/adms#',
      /**
       * @memberof namespaces
       * @type {string}
       */
      wdrs: 'http://www.w3.org/2007/05/powder-s#',
    },
    /**
     * @type {(string | string[])[]}
     */
    labelProperties: [
      'foaf:name',
      ['foaf:firstName', 'foaf:lastName'],
      ['foaf:givenName', 'foaf:familyName'],
      'vcard:fn',
      'skos:prefLabel',
      'dcterms:title',
      'dc:title',
      'rdfs:label',
      'skos:altLabel',
      'vcard:hasEmail',
      'schema:name',
    ],
    /**
     * @type {string[]}
     */
    descriptionProperties: ['http://purl.org/dc/terms/description'],
  },
  /**
   * @type {Array}
   */
  contentviewers: [],

  // reCaptchaSiteKey: '', to be set in local.js

  /**
   * @type {object}
   */
  theme: {
    /**
     * @memberof theme
     * @type {boolean}
     */
    oneRowNavbar: false,

    /**
     * @memberof theme
     * @type {boolean}
     */
    localTheme: false,

    /**
     * @memberof theme
     * @type {boolean}
     */
    localAssets: false,

    /**
     * @memberof theme
     * @type {string}
     */
    commonRestrictionTextPath: '',

    /**
     * @memberof theme
     * @type {object}
     */
    default: {
      /**
       * @memberof default
       * @type {string}
       */
      appName: 'EntryScape',

      /**
       * @memberof default
       * @type {string}
       */
      logoIcon: new URL('images/es-logo-icon.svg', DEFAULT_ASSETS_PATH).href,

      /**
       * @memberof default
       * @type {string}
       */
      logoFull: new URL('images/es-logo-full.svg', DEFAULT_ASSETS_PATH).href,

      /**
       * @memberof default
       * @type {string}
       */
      themePath: DEFAULT_ASSETS_PATH,

      /**
       * @memberof default
       * @type {string}
       */
      assetsPath: DEFAULT_ASSETS_PATH,
    },
    /**
     * @memberof theme
     * @type {{}}
     */
    footer: {},
    /**
     * External links are available at the bottom of the primary navigation drawer.
     * Each external link object must have a `uri` and `label` property. Both can
     * be either a string or an object with locale keys and localized values.
     *
     * @memberof theme
     * @type {object[]}
     */
    externalLinks: [
      {
        label: {
          en: 'Privacy policy',
          sv: 'Integritetspolicy',
          de: 'Datenschutz\u00ADerklärung',
        },
        uri: {
          en: 'https://entryscape.com/en/privacypolicy',
          sv: 'https://entryscape.com/sv/integritetspolicy',
          de: 'https://entryscape.com/de/datenschutzerklarung',
        },
      },
    ],
    /**
     * An NLS profile enhances the translation mechanism to check for
     * profile-specific strings, and then fall-back to defaults.
     * E.g. with nlsProfile 'dcat', the first check will be for the key
     * 'mergeAdded__dcat' and fall-back to the default 'mergeAdded'.
     */
    nlsProfile: '',
  },
  /**
   * @type {object}
   */
  locale: {
    /**
     * @memberof locale
     * @type {string}
     */
    fallback: 'en',

    /**
     * @memberof locale
     * @typedef {{lang: string, flag: string, label: string, labelEn: string, shortDatePattern: string}} SupportedObj
     * @type {SupportedObj[]}
     */
    supported: [
      {
        lang: 'de',
        flag: 'de',
        label: 'Deutsch',
        labelEn: 'German',
        shortDatePattern: 'DD. MMM',
        shortDatePatternWithYear: 'DD. MMM, YYYY',
      },
      {
        lang: 'en',
        flag: 'gb',
        label: 'English',
        labelEn: 'English',
        shortDatePattern: 'MMM DD',
        shortDatePatternWithYear: 'MMM DD, YYYY',
      },
      {
        lang: 'sv',
        flag: 'se',
        label: 'Svenska',
        labelEn: 'Swedish',
        shortDatePattern: 'DD MMM',
        shortDatePatternWithYear: 'DD MMM, YYYY',
      },
    ],
    // NLSOverrides: [], to be set in local.js
  },
  itemstore: {
    /**
     * @type {string[]}
     */
    defaultBundles: [
      'skos',
      'dcterms',
      'foaf',
      'vcard',
      'adms',
      'dcat2',
      'datasetseries',
      'prof',
      'esc',
      'rdfs',
    ],

    /**
     * @type {Array}
     */
    bundles: [],

    /**
     * The type definition is just an example as for different languages, there are varying translation options.
     * Note: Maybe languages should be moved from config as these values aren't really being configured.
     *
     * @typedef {{en: string, bg: string, sv: string, de: string}} LanguageObj
     * @type {{value: string, label: LanguageObj}}
     */
    languages: [
      { value: '', label: { en: '' } },
      {
        value: 'bg',
        label: {
          en: 'Bulgarian',
          bg: 'български',
          sv: 'bulgariska',
          de: 'Bulgarisch',
        },
      },
      {
        value: 'cs',
        label: {
          en: 'Czech',
          cs: 'čeština',
          sv: 'tjeckiska',
          de: 'Tschechisch',
        },
      },
      {
        value: 'da',
        label: { en: 'Danish', da: 'dansk', sv: 'danska', de: 'Dänisch' },
      },
      { value: 'de', label: { en: 'German', de: 'Deutsch', sv: 'tyska' } },
      {
        value: 'el',
        label: {
          en: 'Greek',
          el: 'ελληνικά',
          sv: 'grekiska',
          de: 'Griechisch',
        },
      },
      { value: 'en', label: { en: 'English', sv: 'engelska', de: 'Englisch' } },
      {
        value: 'es',
        label: { en: 'Spanish', es: 'Español', sv: 'spanska', de: 'Spanisch' },
      },
      {
        value: 'et',
        label: {
          en: 'Estonian',
          et: 'Eesti keel',
          sv: 'estniska',
          de: 'Estnisch',
        },
      },
      {
        value: 'fi',
        label: { en: 'Finnish', fi: 'Suomi', sv: 'finska', de: 'Finnisch' },
      },
      {
        value: 'fr',
        label: {
          en: 'French',
          fr: 'Français',
          sv: 'franska',
          de: 'Französisch',
        },
      },
      {
        value: 'ga',
        label: { en: 'Irish', ga: 'Gaeilge', sv: 'iriska', de: 'Irisch' },
      },
      {
        value: 'hr',
        label: {
          en: 'Croatian',
          hr: 'Hrvatski',
          sv: 'kroatiska',
          de: 'Kroatisch',
        },
      },
      {
        value: 'hu',
        label: {
          en: 'Hungarian',
          hu: 'Magyar',
          sv: 'ungerska',
          de: 'Ungarisch',
        },
      },
      {
        value: 'it',
        label: {
          en: 'Italian',
          it: 'Italiano',
          sv: 'italienska',
          de: 'Italienisch',
        },
      },
      {
        value: 'lt',
        label: {
          en: 'Lithuanian',
          lt: 'Lietuvių kalba',
          sv: 'litauiska',
          de: 'Litauisch',
        },
      },
      {
        value: 'lv',
        label: {
          en: 'Latvian',
          lv: 'Latviešu valoda',
          sv: 'lettiska',
          de: 'Lettisch',
        },
      },
      {
        value: 'mt',
        label: {
          en: 'Maltese',
          mt: 'Malti',
          sv: 'maltesiska',
          de: 'Maltesisch',
        },
      },
      {
        value: 'nb',
        label: {
          en: 'Norwegian (bokmål)',
          nb: 'norsk bokmål',
          no: 'norsk bokmål',
          nn: 'norsk bokmål',
          sv: 'norska (bokmål)',
          de: 'Norwegisch (Buchsprache)',
        },
      },
      {
        value: 'nl',
        label: {
          en: 'Dutch',
          nl: 'Nederlands',
          sv: 'nederländska',
          de: 'Niederländisch',
        },
      },
      {
        value: 'nn',
        label: {
          en: 'Norwegian (nynorsk)',
          nb: 'norsk nynorsk',
          no: 'norsk nynorsk',
          nn: 'norsk nynorsk',
          sv: 'norska (nynorska)',
          de: 'Norwegisch (Neu)',
        },
      },
      {
        value: 'pl',
        label: { en: 'Polish', pl: 'Polski', sv: 'polska', de: 'Polnisch' },
      },
      {
        value: 'pt',
        label: {
          en: 'Portuguese',
          pt: 'Português',
          sv: 'portugisiska',
          de: 'Portugiesisch',
        },
      },
      {
        value: 'ro',
        label: {
          en: 'Romanian',
          ro: 'Română',
          sv: 'rumänska',
          de: 'Rumänisch',
        },
      },
      {
        value: 'no',
        label: {
          en: 'Norwegian',
          no: 'norsk',
          nb: 'norsk',
          nn: 'norsk nynorsk',
          sv: 'norska',
          de: 'Norwegisch',
        },
      },
      {
        value: 'sk',
        label: {
          en: 'Slovak',
          sk: 'Slovenčina',
          sv: 'slovakiska',
          de: 'Slowakisch',
        },
      },
      {
        value: 'sl',
        label: {
          en: 'Slovenian',
          sl: 'Slovenščina',
          sv: 'slovenska',
          de: 'Slowenisch',
        },
      },
      {
        value: 'sv',
        label: { en: 'Swedish', sv: 'svenska', de: 'Schwedisch' },
      },
    ],

    /**
     * @type {Array}
     */
    appendLanguages: [],

    /**
     * @type {Array}
     */
    choosers: [],

    /**
     * @type {string}
     */
    geonamesStart: 6295630,

    /**
     * @type {object} geonamesAPI
     * {string} geonamesAPI.labelProperty - should be 'toponymName' or 'name'
     */
    geonamesAPI: {
      uriPrefix: 'https://sws.geonames.org/',
      hierarchyURL: `http://api.geonames.org/hierarchyJSON?userName=metasolutions&lang=en&geonameId=`,
      childrenURL: `http://api.geonames.org/childrenJSON?userName=metasolutions&lang=en&geonameId=`,
      geonameURL: `http://api.geonames.org/getJSON?userName=metasolutions&lang=en&geonameId=`,
      labelProperty: 'toponymName',
    },
  },

  /**
   * @type {{}}
   */
  entrychooser: {},

  /**
   * @type {object} privacyPolicyConfiguration
   * {string} privacyPolicyConfiguration.url
   * {string} privacyPolicyConfiguration.version
   * {boolean} privacyPolicyConfiguration.requireApproval - Determines whether the dialog should come up for approval
   */
  privacyPolicy: {
    url: {
      en: 'https://entryscape.com/privacypolicy',
      sv: 'https://entryscape.com/integritetspolicy',
      de: 'https://entryscape.com/datenschutzerklarung',
    },
    version: null,
    requireApproval: false,
  },

  /**
   * @type {object} termsAndConditionsConfiguration
   * {string} termsAndConditionsConfiguration.url
   * {string} termsAndConditionsConfiguration.version
   * {boolean} termsAndConditionsConfiguration.requireApproval - Determines whether the dialog should come up for approval
   */
  termsAndConditions: {
    url: {
      en: 'https://entryscape.com/termsconditions',
      sv: 'https://entryscape.com/allmannavillkor',
      de: 'https://entryscape.com/geschaftsbedingungen',
    },
    version: null,
    requireApproval: false,
  },

  /**
   * Specifies the minimum number of chars that must be entered for search.
   *
   * @type {string}
   */
  minimumSearchLength: 3,

  /**
   * Determines whether the help button and the corresponding drawer with documentation
   * links are available. Note that their visibility also depends on `help links`
   * being available for a particular view.
   *
   * @type {boolean}
   */
  includeHelp: true,
};
