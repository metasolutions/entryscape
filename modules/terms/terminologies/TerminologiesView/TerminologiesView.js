import { useMemo } from 'react';
import { useUserState } from 'commons/hooks/useUser';
import { getContextSearchQuery } from 'commons/util/solr/context';
import {
  CONTEXT_TYPE_TERMINOLOGY,
  canEditContextMetadata,
} from 'commons/util/context';
import { RDF_TYPE_TERMINOLOGY } from 'commons/util/entry';
import { getPathFromViewName } from 'commons/util/site';
import config from 'config';
import {
  withListModelProviderAndLocation,
  listPropsPropType,
} from 'commons/components/ListView';
import {
  EntryListView,
  useSolrQuery,
  MODIFIED_COLUMN,
  TITLE_COLUMN,
  TOGGLE_CONTEXT_COLUMN,
  ACTIONS_GROUP_COLUMN,
} from 'commons/components/EntryListView';
import {
  LIST_ACTION_EDIT,
  LIST_ACTION_INFO,
} from 'commons/components/EntryListView/actions';
import { nlsBundles, listActions } from './actions';

const TerminologiesView = ({ listProps }) => {
  const { userEntry, userInfo } = useUserState();

  const queryParams = useMemo(() => {
    const queryTypeParams = {
      rdfType: RDF_TYPE_TERMINOLOGY,
      contextType: CONTEXT_TYPE_TERMINOLOGY,
    };

    return getContextSearchQuery(queryTypeParams, {
      userEntry,
      userInfo,
    });
  }, [userEntry, userInfo]);

  const queryResults = useSolrQuery(queryParams);
  const terminologyStartView = config.get('terms.startView');

  const publishingRestrictionPath = config.get(
    'terms.disallowSchemePublishingDialog'
  );

  return (
    <EntryListView
      {...listProps}
      nlsBundles={nlsBundles}
      listActions={listActions}
      listActionsProps={{
        userEntry,
        userInfo,
        numberOfTerminologies: queryResults.size,
      }}
      getListItemProps={({ entry }) => ({
        to: getPathFromViewName(terminologyStartView, {
          contextId: entry.getContext().getId(),
        }),
      })}
      columns={[
        {
          ...TOGGLE_CONTEXT_COLUMN,
          getProps: ({ entry, translate }) => ({
            entry,
            publicTooltip: translate('publicSchemeTitle'),
            privateTooltip: translate('privateSchemeTitle'),
            noAccess: translate('schemeSharingNoAccess'),
          }),
          restrictionPath: publishingRestrictionPath,
        },
        { ...TITLE_COLUMN, xs: 7 },
        MODIFIED_COLUMN,
        {
          ...ACTIONS_GROUP_COLUMN,
          actions: [
            LIST_ACTION_INFO,
            {
              ...LIST_ACTION_EDIT,
              isVisible: ({ entry }) =>
                canEditContextMetadata(entry, userEntry),
            },
          ],
        },
      ]}
      {...queryResults}
    />
  );
};

TerminologiesView.propTypes = {
  listProps: listPropsPropType,
};

export default withListModelProviderAndLocation(TerminologiesView);
