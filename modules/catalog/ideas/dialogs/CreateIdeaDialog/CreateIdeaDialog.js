import PropTypes from 'prop-types';
import { Context, Entry } from '@entryscape/entrystore-js';
import { createEntry } from 'commons/util/store';
// eslint-disable-next-line max-len
import CreateEntryByTemplateDialog from 'commons/components/EntryListView/dialogs/create/CreateEntryByTemplateDialog';
import { useESContext } from 'commons/hooks/useESContext';
import { RDF_TYPE_IDEA } from 'commons/util/entry';
import { LEVEL_MANDATORY } from 'commons/components/rdforms/LevelSelector';
import { useTranslation } from 'commons/hooks/useTranslation';
import escaIdeasNLS from 'catalog/nls/escaIdeas.nls';
import { useSnackbar } from 'commons/hooks/useSnackbar';
import { useState } from 'react';

/**
 *
 * @param {Context} context
 * @returns {Entry}
 */
const createPrototypeEntry = (context) => {
  const prototypeEntry = createEntry(context, RDF_TYPE_IDEA);
  prototypeEntry
    .getMetadata()
    .add(prototypeEntry.getResourceURI(), 'rdf:type', RDF_TYPE_IDEA);
  return prototypeEntry;
};

const CreateIdeaDialog = ({ onCreate, ...props }) => {
  const translate = useTranslation(escaIdeasNLS);
  const [addSnackbar] = useSnackbar();

  const { context } = useESContext();
  const [prototypeEntry] = useState(() => createPrototypeEntry(context));

  return (
    <CreateEntryByTemplateDialog
      {...props}
      entry={prototypeEntry}
      editorLevel={LEVEL_MANDATORY}
      createHeaderLabel={translate('createHeader')}
      onEntryEdit={() => {
        onCreate();
        addSnackbar({ message: translate('createIdeaSuccess') });
      }}
    />
  );
};

CreateIdeaDialog.propTypes = { onCreate: PropTypes.func };

export default CreateIdeaDialog;
