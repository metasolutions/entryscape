import { useReducer, useCallback, useLayoutEffect, useRef } from 'react';
import PropTypes from 'prop-types';

export const IDLE = 'idle';
export const PENDING = 'pending';
export const RESOLVED = 'resolved';
export const REJECTED = 'rejected';

/**
 *
 * @param {Function} dispatch
 * @returns {Function}
 */
function useSafeDispatch(dispatch) {
  const mounted = useRef(false);

  useLayoutEffect(() => {
    mounted.current = true;
    return () => {
      mounted.current = false;
    };
  }, []);

  return useCallback(
    (...args) => (mounted.current ? dispatch(...args) : undefined),
    [dispatch]
  );
}

/**
 *
 * @param {{data: any}} state
 * @param {object} action
 * @returns {object}
 */
function asyncReducer({ data }, action) {
  switch (action.type) {
    case PENDING: {
      return { status: PENDING, data, error: null };
    }
    case RESOLVED: {
      return { status: RESOLVED, data: action.data, error: null };
    }
    case REJECTED: {
      return { status: REJECTED, data, error: action.error };
    }
    default: {
      throw new Error(`Unhandled action type: ${action.type}`);
    }
  }
}

const defaultState = {
  status: IDLE,
  data: null,
  error: null,
};

/**
 *
 * @param {object} initialState
 * @returns {object}
 */
function useAsync(initialState) {
  const [state, unsafeDispatch] = useReducer(asyncReducer, {
    ...defaultState,
    ...initialState,
  });

  const dispatch = useSafeDispatch(unsafeDispatch);

  const runAsync = useCallback(
    (promise) => {
      dispatch({ type: PENDING });
      promise.then(
        (data) => {
          dispatch({ type: RESOLVED, data });
        },
        (error) => {
          dispatch({ type: REJECTED, error });
        }
      );
    },
    [dispatch]
  );

  const { data, error, status } = state;

  const isLoading = status === PENDING || status === IDLE;
  const isPending = status === PENDING;

  return {
    error,
    status,
    isLoading,
    isPending,
    data,
    runAsync,
  };
}

export const getAsyncStatus = (statuses) => {
  if (statuses.includes(REJECTED)) return REJECTED;
  if (statuses.includes(PENDING)) return PENDING;
  if (statuses.every((status) => status === RESOLVED)) return RESOLVED;
  if (statuses.includes(RESOLVED)) return PENDING; // at least one resolved but waiting for rest to resolve
  return IDLE;
};

export const asyncStatusPropType = PropTypes.oneOf([
  IDLE,
  PENDING,
  RESOLVED,
  REJECTED,
]);

export default useAsync;
