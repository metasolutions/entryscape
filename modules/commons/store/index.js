/* eslint-disable import/no-mutable-exports */
import { EntryStore, EntryStoreUtil } from '@entryscape/entrystore-js';
import config from 'config';
import registry from 'commons/registry';

let entrystore;
let entrystoreUtil;

const init = () => {
  entrystore = new EntryStore(config.get('entrystore.repository'));
  registry.set('entrystore', entrystore);
  entrystore.getREST().disableJSONP();

  entrystoreUtil = new EntryStoreUtil(entrystore);
  registry.set('entrystoreutil', entrystoreUtil);

  return entrystore;
};

/**
 * Initializes a store.
 *
 * @param {string} endpoint
 * @returns {EntryStore}
 */
const initializeStore = (endpoint) => {
  const store = new EntryStore(endpoint);
  store.getREST().disableJSONP();
  store.getREST().disableCredentials();
  return store;
};

/**
 * Initializes a store's utils.
 *
 * @param {EntryStore} store
 * @returns {EntryStoreUtil}
 */
const initializeStoreUtil = (store) => new EntryStoreUtil(store);

export default init;
export { entrystore, entrystoreUtil, initializeStore, initializeStoreUtil };
