import escaPublishersNLS from 'catalog/nls/escaPublishers.nls';
import escoListNLS from 'commons/nls/escoList.nls';
import { withListRefresh } from 'commons/components/ListView';
import CreatePublisherDialog from '../dialogs/CreatePublisher';

const nlsBundles = [escaPublishersNLS, escoListNLS];

const listActions = [
  {
    id: 'create',
    Dialog: withListRefresh(CreatePublisherDialog, 'onCreate'),
    isVisible: () => true,
    labelNlsKey: 'createButtonLabel',
    tooltipNlsKey: 'actionTooltipPublisher',
  },
];

export { listActions, nlsBundles };
