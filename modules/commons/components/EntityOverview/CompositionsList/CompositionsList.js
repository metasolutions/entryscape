import PropTypes from 'prop-types';
import { useCallback, useEffect } from 'react';
import { Entry } from '@entryscape/entrystore-js';
import {
  ListItemOpenMenuButton,
  List,
  ListItem,
  ActionsMenu,
  ListItemText,
  ListView,
  withListModelProvider,
  useListQuery,
  ListPagination,
  ListItemAction,
  ListItemActionsGroup,
} from 'commons/components/ListView';
import { getIconFromActionId } from 'commons/actions';
import {
  LIST_ACTION_EDIT,
  LIST_ACTION_INFO,
} from 'commons/components/EntryListView/actions';
import {
  useTranslation,
  nlsBundlesPropType,
} from 'commons/hooks/useTranslation';
import escoListNLS from 'commons/nls/escoList.nls';
import { OverviewListHeader } from 'commons/components/overview/OverviewListHeader';
import { OverviewListPlaceholder } from 'commons/components/overview';
import { localize } from 'commons/locale';
import { useESContext } from 'commons/hooks/useESContext';
import { getTitle, getShortModifiedDate } from 'commons/util/metadata';
import useAsync, { RESOLVED } from 'commons/hooks/useAsync';
import LinkedDataBrowserDialog from 'commons/components/LinkedDataBrowserDialog';
import { getSubEntities } from 'commons/types/utils/relations';
import { Edit as EditIcon, Info as InfoIcon } from '@mui/icons-material';
import { useSnackbar } from 'commons/hooks/useSnackbar';
import { entityTypePropType } from 'commons/types/EntityType';
import { isDraftEntry } from 'commons/util/entry';
import DraftIcon from '@mui/icons-material/StickyNote2';
import Tooltip from 'commons/components/common/Tooltip';
import CreateEntityDialog from '../dialogs/CreateEntityDialog';
import { actions } from './actions';

const SEARCH_FIELDS = ['label'];
const ROWS_PER_PAGE = 10;

const CompositionsList = ({ relation, entityType, entry, nlsBundles = [] }) => {
  const translate = useTranslation([escoListNLS, ...nlsBundles]);
  const { context, contextName } = useESContext();
  const { data: subEntities, runAsync, status } = useAsync({ data: [] });
  const [addSnackbar] = useSnackbar();

  const refreshSubEntities = useCallback(
    () => runAsync(getSubEntities(entry, relation, context, entityType)),
    [entry, runAsync, relation, context, entityType]
  );

  const afterCreateSubEntry = (newSubEntry) => {
    const metadata = entry.getMetadata();
    metadata.add(
      entry.getResourceURI(),
      relation.property,
      newSubEntry.getResourceURI()
    );
    entry.commitMetadata().then(refreshSubEntities);
  };

  useEffect(() => {
    refreshSubEntities();
  }, [refreshSubEntities]);

  const { result: subItems, size } = useListQuery({
    items:
      status === RESOLVED
        ? subEntities.map((subEntityEntry) => ({
            entry: subEntityEntry,
            label: getTitle(subEntityEntry),
          }))
        : null,
    searchFields: SEARCH_FIELDS,
  });

  const label = relation.label || entityType.get('label');
  const actionParams = {
    entityType,
    // would normally be refreshSubEntities, not working right now
    afterCreateEntry: afterCreateSubEntry,
    context,
    contextName,
    label,
    nlsBundles,
  };
  const isCreationAllowed =
    !('max' in relation) || relation.max > subEntities.length;

  return (
    <>
      <OverviewListHeader
        heading={localize(label)}
        nlsBundles={nlsBundles}
        action={{
          Dialog: CreateEntityDialog,
          tooltip: isCreationAllowed
            ? translate('addRelationTitle')
            : translate('maxEntitiesTooltip', relation.max),
          disabled: !isCreationAllowed,
        }}
        actionParams={actionParams}
      />
      <ListView
        size={size}
        nlsBundles={nlsBundles}
        renderPlaceholder={() => (
          <OverviewListPlaceholder
            label={translate('relationsListPlaceholder')}
          />
        )}
        // avoid rejected status, since default error handling is not needed here
        // todo: fix error handling
        status={status === 'rejected' ? 'resolved' : status}
      >
        <List renderLoader={(Loader) => <Loader height="10vh" />}>
          {subItems.map(({ entry: subEntity, label: subEntityLabel }) => {
            const actionsMenuItems = actions
              .filter(({ isVisible }) =>
                isVisible
                  ? isVisible({
                      entry: subEntity,
                      ...actionParams,
                    })
                  : true
              )
              .map((action) => ({
                label: translate(action.labelNlsKey),
                icon: getIconFromActionId(action.id),
                action: {
                  ...action,
                  entry: subEntity,
                  parent: entry,
                  relation,
                  refresh: refreshSubEntities,
                  ...(action.getProps
                    ? action.getProps({ translate, addSnackbar })
                    : {}),
                },
              }));

            return (
              <ListItem key={subEntity.getId()}>
                <ListItemText
                  xs={5}
                  primary={
                    subEntityLabel ||
                    translate('unnamedEntity', subEntity.getId())
                  }
                />
                <ListItemAction xs={1}>
                  {isDraftEntry(subEntity) ? (
                    <Tooltip title={translate('draftTooltip')}>
                      <DraftIcon color="primary" />
                    </Tooltip>
                  ) : null}
                </ListItemAction>
                <ListItemText
                  xs={3}
                  secondary={getShortModifiedDate(subEntity)}
                />
                <ListItemActionsGroup
                  xs={2}
                  justifyContent="end"
                  actions={[
                    {
                      ...LIST_ACTION_INFO,
                      Dialog: LinkedDataBrowserDialog,
                      getProps: () => ({
                        nlsKeyLabel: 'typeInfoDialogTitle',
                        entry: subEntity,
                        entityType,
                        icon: <InfoIcon />,
                        title: translate('infoEntry'),
                      }),
                    },
                    {
                      ...LIST_ACTION_EDIT,
                      getProps: () => ({
                        entry: subEntity,
                        parentEntry: entry,
                        icon: <EditIcon />,
                        title: translate('editEntry'),
                      }),
                    },
                  ]}
                />
                <ListItemOpenMenuButton xs={1} />
                <ActionsMenu items={actionsMenuItems} />
              </ListItem>
            );
          })}
        </List>
        {size > ROWS_PER_PAGE ? <ListPagination /> : null}
      </ListView>
    </>
  );
};
CompositionsList.propTypes = {
  relation: PropTypes.shape({
    entityType: PropTypes.string,
    property: PropTypes.string,
    type: PropTypes.string,
    max: PropTypes.number,
    label: PropTypes.shape({}),
  }),
  entityType: entityTypePropType,
  entry: PropTypes.instanceOf(Entry),
  nlsBundles: nlsBundlesPropType,
};

export default withListModelProvider(CompositionsList, () => ({
  limit: ROWS_PER_PAGE,
}));
