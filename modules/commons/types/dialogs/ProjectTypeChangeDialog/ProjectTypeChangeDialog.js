import PropTypes from 'prop-types';
import { Context } from '@entryscape/entrystore-js';
import Lookup from 'commons/types/Lookup';
import ContentWrapper from 'commons/components/common/ContentWrapper';
import { useState, useEffect } from 'react';
import {
  nlsBundlesPropType,
  useTranslation,
} from 'commons/hooks/useTranslation';
import { useUserState } from 'commons/hooks/useUser';
import ListActionDialog from 'commons/components/common/dialogs/ListActionDialog';
import { spreadEntry } from 'commons/util/store';
import useAsync, { RESOLVED, PENDING } from 'commons/hooks/useAsync';
import ProjectTypeChooser from 'commons/types/ProjectTypeChooser';
import { RDF_PROPERTY_PROJECTTYPE } from 'entitytype-lookup';
import { ErrorCatcher } from 'commons/errors/ErrorCatcher';
import { ErrorWithMessage } from 'commons/errors/utils/errors';
import LoadingButton from 'commons/components/LoadingButton';

/**
 *
 * @param {Context} context
 * @param {string} projectTypeId
 * @returns {Promise}
 */
const udpateProjectType = async (context, projectTypeId) => {
  const contextEntry = await context.getEntry();
  const { ruri, info } = spreadEntry(contextEntry);
  const graph = info.getGraph();
  graph.findAndRemove(ruri, RDF_PROPERTY_PROJECTTYPE);

  if (projectTypeId) {
    graph.add(ruri, RDF_PROPERTY_PROJECTTYPE, projectTypeId);
  }
  return info.commit();
};

const ProjectTypeChangeDialog = ({
  context,
  closeDialog,
  nlsBundles,
  onChange,
}) => {
  const { userInfo } = useUserState();
  const [selectedProjectTypeId, setSelectedProjectTypeId] = useState('');
  const translate = useTranslation(nlsBundles);

  const {
    runAsync: runAsyncSave,
    error: saveError,
    status: saveStatus,
  } = useAsync();
  const {
    runAsync,
    data: savedProjectTypeId,
    status,
    error: loadError,
  } = useAsync({
    data: '',
  });

  const hasChanges =
    status === RESOLVED && savedProjectTypeId !== selectedProjectTypeId;

  useEffect(() => {
    if (!context) return;
    runAsync(
      Lookup.getProjectTypeInUse(context)
        .then(async (projectType) => {
          const projectTypeId = projectType?.getId() || '';
          if (projectTypeId) {
            setSelectedProjectTypeId(projectTypeId);
          }
          return projectTypeId;
        })
        .catch((error) => {
          throw new ErrorWithMessage(translate('loadFail'), error);
        })
    );
  }, [context, runAsync, userInfo, translate]);

  const onChangeProjectType = async (projectTypeId) => {
    setSelectedProjectTypeId(projectTypeId);
  };

  const handleSaveAction = () => {
    runAsyncSave(
      udpateProjectType(context, selectedProjectTypeId)
        .then(() => {
          closeDialog();
          onChange();
        })
        .catch((error) => {
          throw new ErrorWithMessage(translate('saveFail'), error);
        })
    );
  };

  const actions = (
    <LoadingButton
      loading={saveStatus === PENDING}
      autoFocus
      disabled={!hasChanges}
      onClick={handleSaveAction}
    >
      {translate('saveButton')}
    </LoadingButton>
  );

  return (
    <>
      <ListActionDialog
        closeDialog={closeDialog}
        id="change-projecttype"
        title={translate('changeProjectTypeLabel')}
        actions={actions}
        maxWidth="sm"
      >
        <ContentWrapper xs={10}>
          <ProjectTypeChooser
            onChangeProjectType={onChangeProjectType}
            selectedId={selectedProjectTypeId}
            userInfo={userInfo}
          />
        </ContentWrapper>
      </ListActionDialog>
      <ErrorCatcher error={loadError || saveError} />
    </>
  );
};

ProjectTypeChangeDialog.propTypes = {
  context: PropTypes.instanceOf(Context),
  closeDialog: PropTypes.func,
  nlsBundles: nlsBundlesPropType,
  onChange: PropTypes.func,
};

export default ProjectTypeChangeDialog;
