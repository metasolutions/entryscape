import { entrystoreUtil as esUtil } from 'commons/store';
import { namespaces as ns } from '@entryscape/rdfjson';
import { Entry } from '@entryscape/entrystore-js';
import { spreadEntry } from 'commons/util/store';
import { EntityType } from 'entitytype-lookup';

/**
 * Matches rdf types in entry with rdf types for entity type. This can be useful
 * if a specific rdf type is used to filter on entries with multiple rdf types.
 *
 * @param {Entry} entry
 * @param {EntityType} entityType
 * @returns {boolean}
 */
const matchEntityTypeRdfType = (entry, entityType) => {
  const resourceURI = entry.getResourceURI();
  const metadata = entry.getMetadata();
  const sourceRdfTypes = metadata
    .find(resourceURI, 'rdf:type')
    .map((statement) => statement.getValue());
  const entityTypeRdfTypes = entityType.getRdfType();

  for (const rdfType of entityTypeRdfTypes) {
    if (sourceRdfTypes.includes(ns.expand(rdfType))) return true;
  }
  return false;
};

/**
 * Retrieves all sub entities related to given entry
 *
 * @param {object} entry
 * @param {object} relation
 * @param {object} context
 * @param {EntityType} entityType
 * @returns {Promise<object[]>}
 */
export const getSubEntities = async (entry, relation, context, entityType) => {
  const metadata = entry.getMetadata();
  const subEntityURIs = metadata
    .find(entry.getResourceURI(), relation.property)
    .map((statement) => statement.getValue());
  const subEntries = await esUtil.loadEntriesByResourceURIs(
    subEntityURIs,
    context,
    true // accept missing, in case of broken references
  );
  if (!entityType) return subEntries;
  return subEntries.filter((subEntry) => {
    return subEntry && matchEntityTypeRdfType(subEntry, entityType);
  });
};

/**
 *
 * @param {Entry} parentEntry
 * @param {Entry} connectedEntry
 * @param {string} property
 * @returns {Promise}
 */
export const removeRelationFromParent = (
  parentEntry,
  connectedEntry,
  property
) => {
  const { metadata: parentMetadata, ruri: parentResourceURI } =
    spreadEntry(parentEntry);

  parentMetadata.findAndRemove(
    parentResourceURI,
    property,
    connectedEntry.getResourceURI()
  );
  return parentEntry.commitMetadata();
};

/**
 *
 * @param {Entry} parentEntry
 * @param {Entry} connectedEntry
 * @param {string} property
 * @returns {Promise}
 */
export const addRelationToParent = (parentEntry, connectedEntry, property) => {
  parentEntry
    .getMetadata()
    .add(
      parentEntry.getResourceURI(),
      property,
      connectedEntry.getResourceURI()
    );

  return parentEntry.commitMetadata();
};

/**
 * Removes relations/references pointing to an entry and then the entry itself.
 *
 * @param {Entry} referredEntry
 * @param {Entry[]} referenceEntries
 * @param {Function} callback
 * @returns {Promise}
 */
export const removeEntryAndRelations = async (
  referredEntry,
  referenceEntries,
  callback
) => {
  return Promise.all(
    referenceEntries.map(
      // removing referredEntry afterwards so relation property is irrelevant
      (referenceEntry) =>
        removeRelationFromParent(referenceEntry, referredEntry, null)
    )
  )
    .then(() => referredEntry.del())
    .then(callback);
};

/**
 * @param {EntityType} entityType
 * @returns {object|undefined}
 */
export const getFilterPredicatesFromRelations = (entityType) => {
  const relations = entityType?.get('relations');
  if (!relations?.length) return;
  return relations.reduce((filterPredicates, { property }) => {
    filterPredicates[property] = true;
    return filterPredicates;
  }, {});
};
