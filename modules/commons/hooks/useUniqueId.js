import { useState } from 'react';

/**
 * Generates a unique id with an optional prefix
 * @param {string} prefix
 * @returns {string}
 */
export const getUniqueId = (prefix = '') =>
  `${prefix}${Date.now().toString(36)}${Math.random()
    .toString(36)
    .substring(2, 7)}`;

/**
 * Simple hook to generate a unique id with an optional prefix
 * @param {string} prefix
 * @returns {string}
 */
export const useUniqueId = (prefix) => {
  const [uniqueId] = useState(() => getUniqueId(prefix));
  return uniqueId;
};
