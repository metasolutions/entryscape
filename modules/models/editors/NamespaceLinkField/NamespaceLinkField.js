import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Box, FormHelperText } from '@mui/material';
import useLinkOrFile from 'commons/components/entry/Import/useLinkOrFile';
import escoEntryTypeNLS from 'commons/nls/escoEntryType.nls';
import esmoCommonsNLS from 'models/nls/esmoCommons.nls';
import escoFormsNLS from 'commons/nls/escoForms.nls';
import { isUri } from 'commons/util/util';
import { useESContext } from 'commons/hooks/useESContext';
import { useTranslation } from 'commons/hooks/useTranslation';
import { entryPropType } from 'commons/util/entry';
// eslint-disable-next-line import/no-named-as-default
import useGetNamespaceEntries from 'models/namespaces/hooks/useGetNamespaceEntries';
import { getNamespaceEntryChoices } from 'models/utils/namespaceChoices';
import SelectEditMode from 'models/components/SelectEditMode';
import {
  Field,
  Fields,
  Select,
  TextField,
} from 'commons/components/forms/editors';
import { useUniqueId } from 'commons/hooks/useUniqueId';
import { extractNamespaceParts } from 'models/namespaces/utils/namespaceMetadata';

const NAMESPACE_MODE = 'namespace';
const MANUAL_MODE = 'manual';

const EDIT_MODE_CHOICES = [
  {
    labelNlsKey: 'namespacePropertyLabel',
    value: NAMESPACE_MODE,
  },
  {
    labelNlsKey: 'manualPropertyLabel',
    value: MANUAL_MODE,
  },
];

const NLS_BUNDLES = [escoEntryTypeNLS, esmoCommonsNLS, escoFormsNLS];

const NamespaceField = ({
  onChange,
  error,
  helperText,
  labelId,
  initialNamespace = '',
  initialLocalname = '',
  namespaceEntries,
}) => {
  const translate = useTranslation(NLS_BUNDLES);
  const [namespace, setNamespace] = useState(initialNamespace);
  const [localname, setLocalname] = useState(initialLocalname);
  const isFullUri = Boolean(namespace && localname);
  const hasLinkError = Boolean(error && helperText && isFullUri);

  const handleNamespaceChange = (newValue) => {
    const uri = newValue && localname ? `${newValue}${localname}` : '';
    setNamespace(newValue);
    onChange({ target: { value: uri } });
  };

  const handleLocalnameChange = (newValue) => {
    const uri = namespace && newValue ? `${namespace}${newValue}` : '';
    setLocalname(newValue);
    onChange({ target: { value: uri } });
  };

  const getMissingValueError = (value) => {
    if (!error || isFullUri) return {};
    if (value) return { helperText: ' ' }; // workaround to avoid alignment problem
    return { error, helperText: translate('missingValueError') };
  };

  return (
    <>
      <Field size={0} spacing={2}>
        <Select
          xs={4}
          labelId={labelId}
          value={namespace}
          choices={getNamespaceEntryChoices(namespaceEntries, false)}
          onChange={({ target }) => handleNamespaceChange(target.value)}
          label={translate('namespaceLabel')}
          error={hasLinkError}
          {...getMissingValueError(namespace)}
        />
        <TextField
          xs={8}
          onChange={({ target }) => handleLocalnameChange(target.value)}
          value={localname}
          label={translate('localNameLabel')}
          inputProps={{ 'aria-labelledby': labelId }}
          error={hasLinkError}
          {...getMissingValueError(localname)}
        />
      </Field>
      {hasLinkError ? (
        <FormHelperText error>{helperText}</FormHelperText>
      ) : null}
    </>
  );
};

NamespaceField.propTypes = {
  onChange: PropTypes.func,
  error: PropTypes.bool,
  helperText: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
  labelId: PropTypes.string,
  initialNamespace: PropTypes.string,
  initialLocalname: PropTypes.string,
  namespaceEntries: PropTypes.arrayOf(entryPropType),
};

const ManualField = ({ labelId, ...props }) => {
  return (
    <Field size={0}>
      <TextField
        xs={12}
        inputProps={{ 'aria-labelledby': labelId }}
        {...props}
      />
    </Field>
  );
};

ManualField.propTypes = {
  labelId: PropTypes.string,
};

const NamespaceLinkField = ({
  label,
  value,
  namespaceEntries,
  onChange,
  onEditModeChange = () => {},
  error,
  helperText,
  mandatory = true,
  sx = {
    marginTop: '16px',
  },
}) => {
  const [{ namespace, localname, uri }] = useState(
    extractNamespaceParts(value, namespaceEntries)
  );
  const [editMode, setEditMode] = useState(
    namespace ? NAMESPACE_MODE : MANUAL_MODE
  );
  const labelId = useUniqueId('namespace-edit-');

  const LinkField = editMode === NAMESPACE_MODE ? NamespaceField : ManualField;

  const handleEditModeChange = (editModeValue) => {
    setEditMode(editModeValue);
    onEditModeChange(editModeValue);
    onChange({ target: { value: uri } }); // use old value on edit mode change
  };

  return (
    <Box sx={sx}>
      <Fields labelId={labelId} label={label} mandatory={mandatory} max={1}>
        <SelectEditMode
          value={editMode}
          choices={EDIT_MODE_CHOICES}
          nlsBundles={esmoCommonsNLS}
          onChange={handleEditModeChange}
        />
        <LinkField
          {...(editMode === NAMESPACE_MODE
            ? {
                initialNamespace: namespace,
                initialLocalname: localname,
                namespaceEntries,
              }
            : {})}
          value={value}
          onChange={onChange}
          error={error}
          helperText={helperText}
          labelId={labelId}
        />
      </Fields>
    </Box>
  );
};

NamespaceLinkField.propTypes = {
  onChange: PropTypes.func,
  onEditModeChange: PropTypes.func,
  error: PropTypes.bool,
  helperText: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
  value: PropTypes.string,
  label: PropTypes.string,
  mandatory: PropTypes.bool,
  sx: PropTypes.shape({}),
  namespaceEntries: PropTypes.arrayOf(entryPropType),
};

const NamespaceLinkFieldWithNamespaceEntries = (props) => {
  const { context } = useESContext();
  const { namespaceEntries, isLoading } = useGetNamespaceEntries(context);

  if (isLoading) return null;

  return <NamespaceLinkField {...props} namespaceEntries={namespaceEntries} />;
};

export { NamespaceLinkFieldWithNamespaceEntries as NamespaceLinkField };

export const CreateNamespaceLinkField = ({
  setCreateEntryParams,
  setExtraFieldIsValid,
}) => {
  const { link, setLink } = useLinkOrFile();
  const [pristine, setPristine] = useState(true);
  const translate = useTranslation(NLS_BUNDLES);
  const linkIsValid = link && isUri(link);

  const handleLinkChange = ({ target }) => {
    setPristine(false);
    const { value: newLink } = target;
    setLink(newLink);
    setCreateEntryParams({ link: newLink });
    setExtraFieldIsValid(newLink && isUri(newLink));
  };

  const getHelperText = () => {
    if (pristine) return '';
    if (!link) {
      return translate('linkRequired');
    }

    if (!isUri(link)) {
      return translate('linkNotValid');
    }
    return '';
  };

  const handleEditModeChange = () => {
    setPristine(true);
  };

  return (
    <NamespaceLinkFieldWithNamespaceEntries
      label={translate('webAddressLabel')}
      value={link}
      onChange={handleLinkChange}
      onEditModeChange={handleEditModeChange}
      error={!pristine && !linkIsValid}
      helperText={getHelperText()}
      sx={{ paddingRight: '55px' }}
    />
  );
};

CreateNamespaceLinkField.propTypes = {
  setCreateEntryParams: PropTypes.func,
  setExtraFieldIsValid: PropTypes.func,
};
