import { useCallback, useState } from 'react';
import PropTypes from 'prop-types';
import { Entry, types } from '@entryscape/entrystore-js';
import ContentWrapper from 'commons/components/common/ContentWrapper';
import ListActionDialog from 'commons/components/common/dialogs/ListActionDialog';
import {
  nlsBundlesPropType,
  useTranslation,
} from 'commons/hooks/useTranslation';
import {
  EntryListView,
  useSolrQuery,
  TITLE_COLUMN,
  TITLE_SORT,
  MODIFIED_COLUMN,
} from 'commons/components/EntryListView';
import {
  ListItemButton,
  ListModelProvider,
  useListView,
} from 'commons/components/ListView';
import { useUserState } from 'commons/hooks/useUser';
import { getSearchQuery } from 'commons/util/solr/entry';
import PersonAddDisabledIcon from '@mui/icons-material/PersonAddDisabled';
import { getUserRenderName, getIsUserDisabled } from 'commons/util/user';
import {
  applyTitleOrUsernameParams,
  excludeGuestAndAdmin,
} from 'commons/util/solr';
import { ErrorCatcher } from 'commons/errors/ErrorCatcher';
import { ErrorWithMessage } from 'commons/errors/utils/errors';

/**
 *
 * @param {object} query
 * @param {object} queryOptions
 * @returns {object}
 */
const applyUsersListQueryParams = (query, queryOptions) => {
  applyTitleOrUsernameParams(query, queryOptions);
  excludeGuestAndAdmin(query);
  return query;
};

const UsersList = ({
  groupEntry,
  ignoreEntries = [],
  updateList,
  closeDialog,
  nlsBundles,
}) => {
  const { userEntry, userInfo } = useUserState();
  const [addError, setAddError] = useState();

  const createQuery = useCallback(() => {
    const [query] = getSearchQuery(
      {
        entryType: types.ET_LOCAL,
        graphType: types.GT_USER,
        resourceType: 'InformationResource',
      },
      { userEntry, userInfo }
    );
    return query;
  }, [userEntry, userInfo]);

  const { entries, ...queryResults } = useSolrQuery({
    createQuery,
    applyQueryParams: applyUsersListQueryParams,
  });

  const translate = useTranslation(nlsBundles);
  const handleAddUserToGroup = (entry) => {
    const groupList = groupEntry.getResource(true);
    groupList
      .addEntry(entry)
      .then(() => {
        entry.setRefreshNeeded();
        entry.refresh();
      })
      .then(closeDialog)
      .then(updateList)
      .catch((error) => {
        const fullError = new Error(
          `Could not add user ${entry.getId()} to group ${groupEntry.getId()}: Error: ${error}`
        );
        setAddError(new ErrorWithMessage(translate('addUserFail'), fullError));
      });
  };

  const getSelectEntryProps = ({ entry: listUserEntry }) => {
    const ignoreIds = ignoreEntries.map((entry) => entry.getId());
    const disabled = ignoreIds.includes(listUserEntry.getId());
    return {
      onClick: () => handleAddUserToGroup(listUserEntry),
      disabled,
      label: disabled
        ? translate('addedEntityTypeLabel')
        : translate('addEntityTypeLabel'),
    };
  };

  return (
    <>
      <EntryListView
        // eslint-disable-next-line react/jsx-props-no-spreading
        {...queryResults}
        entries={entries}
        nlsBundles={nlsBundles}
        columns={[
          {
            ...TITLE_COLUMN,
            xs: 9,
            sortBy: TITLE_SORT,
            direction: 'row',
            getProps: ({ entry }) => ({
              primary: getUserRenderName(entry),
              secondary: getIsUserDisabled(entry) ? (
                <PersonAddDisabledIcon
                  fontSize="small"
                  title={translate('userStatusDisabled')}
                />
              ) : null,
            }),
          },
          MODIFIED_COLUMN,
          {
            id: 'select-entry',
            xs: 1,
            Component: ListItemButton,
            getProps: getSelectEntryProps,
          },
        ]}
      />
      <ErrorCatcher error={addError} />
    </>
  );
};

UsersList.propTypes = {
  groupEntry: PropTypes.instanceOf(Entry),
  ignoreEntries: PropTypes.arrayOf(PropTypes.instanceOf(Entry)),
  updateList: PropTypes.func,
  closeDialog: PropTypes.func,
  nlsBundles: nlsBundlesPropType,
};

const AddUserToGroupDialog = ({
  entry,
  closeDialog,
  groupEntry,
  ignoreEntries,
  updateList,
}) => {
  const { nlsBundles } = useListView();
  const translate = useTranslation(nlsBundles);

  return (
    <ListActionDialog
      id="select-user-dialog"
      closeDialog={closeDialog}
      title={translate('selectUser')}
      fixedHeight
    >
      <ContentWrapper>
        <ListModelProvider>
          <UsersList
            entry={entry}
            closeDialog={closeDialog}
            groupEntry={groupEntry}
            updateList={updateList}
            ignoreEntries={ignoreEntries}
            nlsBundles={nlsBundles}
          />
        </ListModelProvider>
      </ContentWrapper>
    </ListActionDialog>
  );
};

AddUserToGroupDialog.propTypes = {
  entry: PropTypes.instanceOf(Entry),
  closeDialog: PropTypes.func,
  groupEntry: PropTypes.instanceOf(Entry),
  ignoreEntries: PropTypes.arrayOf(PropTypes.instanceOf(Entry)),
  updateList: PropTypes.func,
};

export default AddUserToGroupDialog;
