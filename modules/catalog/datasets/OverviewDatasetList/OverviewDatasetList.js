import { useCallback, useEffect, useState } from 'react';
import { getPathFromViewName } from 'commons/util/site';
import { entrystoreUtil } from 'commons/store';
import { entryPropType } from 'commons/util/entry';
import escaDataset from 'catalog/nls/escaDataset.nls';
import escoListNLS from 'commons/nls/escoList.nls';
import { withListModelProvider } from 'commons/components/ListView';
import {
  EntryListView,
  TITLE_COLUMN,
  MODIFIED_COLUMN,
  ACTIONS_GROUP_COLUMN,
} from 'commons/components/EntryListView';
import { LIST_ACTION_INFO } from 'commons/components/EntryListView/actions';
import { useTranslation } from 'commons/hooks/useTranslation';
import { OverviewListHeader } from 'commons/components/overview/OverviewListHeader';
import {
  Search as SearchIcon,
  LinkOff as DisconnectIcon,
} from '@mui/icons-material';
import DisconnectDialog from 'commons/components/EntityOverview/dialogs/DisconnectDialog';
import { getListQuery } from 'catalog/datasets/queries';
import useAsync from 'commons/hooks/useAsync';
import SearchOrCreateDialog from 'commons/components/EntityOverview/dialogs/SearchOrCreateDialog';
import Lookup from 'commons/types/Lookup';
import PropTypes from 'prop-types';
import { OverviewListPlaceholder } from 'commons/components/overview';

const nlsBundles = [escaDataset, escoListNLS];

const sourceRelation = {
  property: 'dcterms:source',
};

const DatasetSearchDialog = ({ actionParams, ...rest }) => {
  const datasetEntityType = Lookup.getByName('dataset');
  return (
    <SearchOrCreateDialog
      {...rest}
      actionParams={{
        ...actionParams,
        disableCreateActions: true,
        getQuery: getListQuery,
        relation: sourceRelation,
        entityType: datasetEntityType,
        connectButtonLabel: 'linkButtonLabel',
        connectedButtonLabel: 'linkedButtonLabel',
      }}
    />
  );
};

DatasetSearchDialog.propTypes = {
  actionParams: PropTypes.shape({}),
};

const OverviewDatasetList = ({ entry }) => {
  const translate = useTranslation(nlsBundles);
  const { runAsync, data: entries, status } = useAsync({ data: [] });
  const [refresh, setRefresh] = useState(false);

  useEffect(() => {
    const loadEntries = async () => {
      const resourceURIs = entry
        .getMetadata()
        .find(null, 'dcterms:source')
        .map((statement) => statement.getValue());

      return entrystoreUtil.loadEntriesByResourceURIs(
        resourceURIs,
        entry.getContext()
      );
    };

    runAsync(loadEntries());
  }, [entry, runAsync, refresh]);

  const refreshDatasets = () => setRefresh((value) => !value);
  const onCreate = useCallback(
    async (datasetEntry) => {
      entry
        .getMetadata()
        .add(
          entry.getResourceURI(),
          'dcterms:source',
          datasetEntry.getResourceURI()
        );
      await entry.commitMetadata();
      return datasetEntry;
    },
    [entry]
  );

  return (
    <>
      <OverviewListHeader
        heading={translate('datasetsHeading')}
        nlsBundles={nlsBundles}
        action={{
          Dialog: DatasetSearchDialog,
          tooltip: translate('addDatasetTitle'),
        }}
        actionIcon={<SearchIcon />}
        actionParams={{
          numberOfDatasets: entries?.length || 0,
          refreshSubEntities: refreshDatasets,
          onCreate,
        }}
      />
      <EntryListView
        entries={entries}
        size={entries?.length || 0}
        status={status}
        nlsBundles={nlsBundles}
        getListItemProps={({ entry: datasetEntry }) => ({
          to: getPathFromViewName('catalog__datasets__dataset', {
            contextId: entry.getContext().getId(),
            entryId: datasetEntry.getId(),
          }),
        })}
        includeDefaultListActions={false}
        includeHeader={false}
        includePagination={false}
        renderViewPlaceholder={() => (
          <OverviewListPlaceholder
            label={translate('datasetsListPlaceholder')}
          />
        )}
        columns={[
          {
            ...TITLE_COLUMN,
            xs: 8,
          },
          MODIFIED_COLUMN,
          {
            ...ACTIONS_GROUP_COLUMN,
            actions: [
              LIST_ACTION_INFO,
              {
                id: 'disconnect',
                icon: <DisconnectIcon />,
                labelNlsKey: 'disconnectLabel',
                Dialog: DisconnectDialog,
                getProps: ({ entry: datasetEntry }) => ({
                  id: 'disconnect',
                  entry: datasetEntry,
                  parentEntry: entry,
                  relation: { property: 'dcterms:source' },
                  refresh: refreshDatasets,
                }),
              },
            ],
          },
        ]}
      />
    </>
  );
};

OverviewDatasetList.propTypes = {
  entry: entryPropType,
};

export default withListModelProvider(OverviewDatasetList);
