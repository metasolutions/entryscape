import PropTypes from 'prop-types';
import { useState, useEffect, useMemo } from 'react';
import { types } from '@entryscape/entrystore-js';
import Switch from 'commons/components/common/Switch';
import Tooltip from 'commons/components/common/Tooltip';
import { entrystore } from 'commons/store';
import { getContextSearchQuery } from 'commons/util/solr/context';
import { CONTEXT_TYPE_CATALOG } from 'commons/util/context';
import { useTranslation } from 'commons/hooks/useTranslation';
import { useUserState } from 'commons/hooks/useUser';
import { getPathFromViewName } from 'commons/util/site';
import {
  EntryListView,
  useSolrQuery,
  TOGGLE_ENTRY_COLUMN,
  TITLE_COLUMN,
  MODIFIED_COLUMN,
  ACTION_MENU_COLUMN,
  INFO_COLUMN,
} from 'commons/components/EntryListView';
import { LIST_ACTION_INFO } from 'commons/components/EntryListView/actions';
import {
  withListModelProviderAndLocation,
  ListItemAction,
  useListQuery,
  listPropsPropType,
} from 'commons/components/ListView';
import { getLabel } from 'commons/util/rdfUtils';
// eslint-disable-next-line max-len
import OrganizationLinkedDataBrowserDialog from 'status/Organizations/OrganizationLinkedDataBrowserDialog';
import { USER_ENTRY_ID_GUEST } from 'commons/util/userIds';
import { listActions, rowActions, nlsBundles } from './actions';

const SEARCH_FIELDS = ['label'];

const Toggle = ({ pipelineEntry, xs }) => {
  const [contextIsPublic, setContextIsPublic] = useState(false);
  const [canAdminister, setCanAdminister] = useState(false);
  const translate = useTranslation(nlsBundles);

  useEffect(() => {
    pipelineEntry
      .getContext()
      .getEntry()
      .then((contextEntry) => {
        setContextIsPublic(contextEntry.isPublic());
        setCanAdminister(contextEntry.canAdministerEntry());
      });
  }, [pipelineEntry]);

  const handleSwitchChange = () => {
    const context = pipelineEntry.getContext();

    entrystore
      .getEntry(context.getEntryURI(), { forceLoad: true })
      .then((contextEntry) => {
        const entryInfo = contextEntry.getEntryInfo();
        const acl = entryInfo.getACL(true);
        acl.rread = acl.rread || [];
        acl.mread = acl.mread || [];

        if (contextIsPublic) {
          acl.rread.splice(acl.rread.indexOf(USER_ENTRY_ID_GUEST), 1);
          acl.mread.splice(acl.mread.indexOf(USER_ENTRY_ID_GUEST), 1);
        } else {
          acl.rread.push(USER_ENTRY_ID_GUEST);
          acl.mread.push(USER_ENTRY_ID_GUEST);
        }

        entryInfo.setACL(acl);
        entryInfo.commit().then(setContextIsPublic(!contextIsPublic));
      });
  };

  return (
    <ListItemAction xs={xs}>
      <Tooltip
        title={
          contextIsPublic
            ? translate('publicHarvestTitle')
            : translate('privateHarvestTitle')
        }
      >
        <span>
          <Switch
            disabled={!canAdminister}
            checked={contextIsPublic}
            onChange={handleSwitchChange}
            name="checked"
            inputProps={{ 'aria-label': translate('orgToggleAriaLabel') }}
          />
        </span>
      </Tooltip>
    </ListItemAction>
  );
};

Toggle.propTypes = {
  pipelineEntry: PropTypes.shape().isRequired,
  xs: PropTypes.number,
};

const getListItemProps = ({ entry }) => ({
  to: getPathFromViewName('harvest__org__status', {
    contextId: entry.getContext().getId(),
  }),
});

const getColumns = (userEntry, userInfo, listProps = {}) => {
  const { excludeActions = [] } = listProps;
  return [
    {
      ...TOGGLE_ENTRY_COLUMN,
      Component: Toggle,
      getProps: ({ entry }) => ({ pipelineEntry: entry }),
      xs: 2,
    },
    { ...TITLE_COLUMN, xs: 6 },
    MODIFIED_COLUMN,
    {
      ...INFO_COLUMN,
      getProps: ({ entry, translate: translateProp }) => ({
        action: {
          ...LIST_ACTION_INFO,
          entry,
          nlsBundles,
          Dialog: OrganizationLinkedDataBrowserDialog,
        },
        title: translateProp('infoEntry'),
      }),
    },
    {
      ...ACTION_MENU_COLUMN,
      getProps: ({ entry }) => {
        return {
          entry,
          actions: rowActions,
          userEntry,
          userInfo,
          excludeActions,
        };
      },
    },
  ];
};

const NonAdminList = ({ listProps }) => {
  const { userEntry, userInfo } = useUserState();
  const queryParams = useMemo(() => {
    const queryTypeParams = {
      rdfType: 'store:Pipeline',
      contextType: CONTEXT_TYPE_CATALOG,
    };
    return getContextSearchQuery(queryTypeParams, {
      userEntry,
      userInfo,
    });
  }, [userEntry, userInfo]);
  const { entries, ...queryResults } = useSolrQuery(queryParams);
  const { result: itemsInPage = [] } = useListQuery({
    items: entries.map((entry) => ({
      entry,
      label: getLabel(entry),
    })),
    searchFields: SEARCH_FIELDS,
  });

  return (
    <EntryListView
      entries={itemsInPage.map(({ entry }) => entry)}
      {...queryResults}
      {...listProps}
      nlsBundles={nlsBundles}
      listActions={listActions}
      getListItemProps={getListItemProps}
      columns={getColumns(userEntry, userInfo, listProps)}
    />
  );
};

NonAdminList.propTypes = {
  listProps: listPropsPropType,
};

const AdminList = ({ listProps }) => {
  const { userEntry, userInfo } = useUserState();
  const queryParams = useMemo(() => {
    const queryTypeParams = { graphType: types.GT_PIPELINE };
    return getContextSearchQuery(queryTypeParams, {
      userEntry,
      userInfo,
    });
  }, [userEntry, userInfo]);
  const { entries, ...queryResults } = useSolrQuery(queryParams);

  return (
    <EntryListView
      entries={entries}
      {...queryResults}
      {...listProps}
      nlsBundles={nlsBundles}
      listActions={listActions}
      getListItemProps={getListItemProps}
      columns={getColumns(userEntry, userInfo, listProps)}
    />
  );
};

AdminList.propTypes = {
  listProps: listPropsPropType,
};

const OrganizationsView = ({ listProps }) => {
  const {
    userInfo: { hasAdminRights },
  } = useUserState();

  // IMPORTANT: necessary cause we are searching for pipelines in two ways. First by graphtype
  // (searched for in entry information) and second by rdftype (searched for in metadata). The
  // code could probably be harmonized by using graphtype in both cases, but then the utility
  // methods (getSearchQuery, getEntriesByTypeForContextIds) already in place could not be used
  // which would force us to write more code.
  return hasAdminRights ? (
    <AdminList listProps={listProps} />
  ) : (
    <NonAdminList listProps={listProps} />
  );
};

OrganizationsView.propTypes = {
  listProps: listPropsPropType,
};

export default withListModelProviderAndLocation(OrganizationsView);
