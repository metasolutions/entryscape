import { mergeWith } from 'lodash-es';

/**
 * Performs a deep merge according to the following rules:
 * 1. Objects are merged
 * 2. Arrays are concatenated, with one important exception. All elements with
 *    the name key and same name value will be merged.
 * 3. Object keys prefixed with '!' will override the original values with
 *    corresponding unprefixed keys.
 *
 * See unit tests for examples of expected behaviour according to above rules.
 *
 * @param {Array} configs
 * @returns {Array}
 * @see lodash-es/mergeWith
 */
const merge = (...configs) => {
  const customizer = (objValue, srcValue, key, object) => {
    // override if key starts with '!'
    if (key.startsWith('!')) {
      const originalKey = key.substring(1);
      object[originalKey] = srcValue; // Update the corresponding unprefixed key
      return;
    }

    // elements in an array are either merged or concatenated
    if (Array.isArray(objValue) && Array.isArray(srcValue)) {
      // merge objects with same name key
      if (srcValue.some((item) => typeof item === 'object' && item.name)) {
        const mergeResult = [...objValue];
        const srcValueMap = new Map(srcValue.map((item) => [item.name, item]));

        mergeResult.forEach((item, index) => {
          const existingName = item.name;
          if (existingName && srcValueMap.has(existingName)) {
            const overrideValue = srcValueMap.get(existingName);
            mergeResult[index] = merge(item, overrideValue);
            srcValueMap.delete(existingName);
          }
        });
        // srcValueMap contains the values that are concatenated rather than merged
        return [...mergeResult, ...srcValueMap.values()];
      }

      // the default is to concatenate
      return [...objValue, ...srcValue];
    }

    // returning undefined for other cases, which will trigger default merge
  };

  const merged = mergeWith(...configs, customizer);
  return merged;
};

export default merge;
