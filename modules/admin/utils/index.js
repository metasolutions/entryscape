import { entrystore } from 'commons/store';
import { USERS_ENTRY_ID } from 'commons/util/userIds';
import { applyQueryParams } from 'commons/util/solr';
import { getSubviewsOfView } from 'commons/util/site';
import config from 'config';

/**
 *
 * @param {object} query
 * @param {object} queryParams
 * @returns {undefined}
 */
export const applyUsersGroupParams = (query, queryParams) => {
  applyQueryParams(query, queryParams);
  query.uri(entrystore.getEntryURI('_principals', USERS_ENTRY_ID), 'not');
};

/**
 *
 * @returns {object[]}
 */
export const getSecondaryNavViewsForAdmin = () => {
  return getSubviewsOfView('admin');
};

/**
 * Admin module-specific view permission check
 *
 * @param {string} userPermission
 * @param {string} restrictTo
 * @param {boolean} hasPermission - non-module-specific checks result
 * @returns {boolean}
 */
export const checkPermission = (userPermission, restrictTo, hasPermission) => {
  if (userPermission === 'admin' && restrictTo === 'advancedAdmin') {
    return config.get('admin.advancedAdminVisibleForAdminGroup');
  }

  return hasPermission;
};
