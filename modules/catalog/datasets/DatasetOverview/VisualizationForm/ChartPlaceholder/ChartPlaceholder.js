import PropTypes from 'prop-types';
import Typography from '@mui/material/Typography';
import { BarChart as BarChartIcon, Map as MapIcon } from '@mui/icons-material';

const ChartPlaceholder = ({ label, labelClass = '', chartType }) => {
  return (
    <div className="escaChartPlaceHolder">
      {chartType === 'wms' ? (
        <MapIcon fontSize="large" />
      ) : (
        <BarChartIcon fontSize="large" />
      )}
      <Typography classes={{ root: `escaChartPlaceHolderLabel ${labelClass}` }}>
        {label}
      </Typography>
    </div>
  );
};

ChartPlaceholder.propTypes = {
  label: PropTypes.string,
  labelClass: PropTypes.string,
  chartType: PropTypes.string,
};

export default ChartPlaceholder;
