import { AddToPhotos as ExtendsIcon } from '@mui/icons-material';
import ExtendDialog from 'models/components/ExtendDialog';
import * as ns from 'models/utils/ns';
import { FORM_EXTENDS, FORM_FORM_ITEMS } from 'models/utils/formDefinitions';
import { FIELD_TITLE } from 'models/utils/fieldDefinitions';
import { ExtendsEditor } from 'models/editors/ExtendsEditor';
import escoListNLS from 'commons/nls/escoList.nls';
import esmoFormsNLS from 'models/nls/esmoForms.nls';
import esmoCommonsNLS from 'models/nls/esmoCommons.nls';
import escoDialogsNLS from 'commons/nls/escoDialogs.nls';
import { TYPE_FILTER } from 'models/utils/filterDefinitions';
import {
  FORM_TYPE_CHOICES,
  PROPERTY_FORM_CHOICE,
} from 'models/utils/choiceDefinitions';
import { ANY_FILTER_ITEM } from 'commons/components/filters/utils/filterDefinitions';
import CreateFormDialog from '../dialogs/CreateFormDialog';
import { copyExtendedFormMetadata } from '../utils/util';

export const nlsBundles = [
  esmoFormsNLS,
  esmoCommonsNLS,
  escoListNLS,
  escoDialogsNLS,
];

const EXTEND_FIELDS = [
  { ...FIELD_TITLE },
  { ...FORM_EXTENDS, Editor: ExtendsEditor, rdfType: ns.RDF_TYPE_FORM },
  FORM_FORM_ITEMS,
];

export const listActions = [
  {
    id: 'extend',
    Dialog: ExtendDialog,
    beforeCreate: copyExtendedFormMetadata,
    labelNlsKey: 'extend',
    tooltipNlsKey: 'extendFormTitle',
    icon: <ExtendsIcon />,
    fields: EXTEND_FIELDS,
    headerNlsKey: 'extendFormTitle',
    nlsBundles,
  },
  {
    id: 'create',
    Dialog: CreateFormDialog,
    labelNlsKey: 'createButtonLabel',
    tooltipNlsKey: 'createFormTitle',
  },
];

export const filters = [
  {
    ...TYPE_FILTER,
    items: [ANY_FILTER_ITEM, ...FORM_TYPE_CHOICES, PROPERTY_FORM_CHOICE],
    id: 'forms-filter',
  },
];
