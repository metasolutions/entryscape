import { Entry } from '@entryscape/entrystore-js';

/**
 *
 * @param {object} fetchResults
 * @returns {object} fetch plus callback results
 */
export const getContextEntriesCallback = async (fetchResults) => {
  const { entries, ...results } = fetchResults;
  const callbackResults = await Promise.allSettled(
    entries.map((entry) => entry.getContext().getEntry())
  ).then((settledPromises) =>
    settledPromises.map(({ value: contextEntry }, index) => ({
      entry: entries[index],
      contextEntry,
    }))
  );

  return { entries, callbackResults, ...results };
};

/**
 * Simplifies handling the results in case the 'original' callback might depend
 * on configuration.
 *
 * @param {object} fetchResults
 * @param {Entry[]} fetchResults.entries
 * @returns {object} fetch results including objects w/ `entry` as callback results
 */
export const getItemsCallback = ({ entries, ...results }) => ({
  entries,
  callbackResults: entries.map((entry) => ({ entry })),
  ...results,
});
