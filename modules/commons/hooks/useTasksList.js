import { useState, useCallback } from 'react';

export const STATUS_IDLE = 'idle';
export const STATUS_PROGRESS = 'progress';
export const STATUS_DONE = 'done';
export const STATUS_FAILED = 'failed';

export const useTasksList = (initialTasks = []) => {
  const [tasks, setTasks] = useState(initialTasks);

  const updateTask = useCallback((taskId, data) => {
    // Setting state like that to avoid having `tasks` as a dependency, since it's
    // always a new array and would cause an infinite loop
    setTasks((existingTasks) =>
      existingTasks.map((task) =>
        task.id === taskId ? { ...task, ...data } : task
      )
    );
  }, []);

  const allTasksDone = tasks.every(({ status }) => status === STATUS_DONE);

  const resetTasks = useCallback(() => {
    setTasks(initialTasks);
  }, [initialTasks]);

  return { tasks, updateTask, allTasksDone, resetTasks };
};

export const useRunTasks = (initialTasks = []) => {
  const { updateTask, ...rest } = useTasksList(initialTasks);

  const updateTaskStatus = useCallback(
    (taskId, status, message) => {
      updateTask(taskId, { status, ...(message ? { message } : {}) });
    },
    [updateTask]
  );

  const updateTaskMessage = useCallback(
    (taskId, message) => {
      updateTask(taskId, { message });
    },
    [updateTask]
  );

  const runTask = useCallback(
    async (taskId, taskPromise) => {
      try {
        updateTaskStatus(taskId, STATUS_PROGRESS);
        const taskResult = await Promise.resolve(taskPromise);
        updateTaskStatus(taskId, STATUS_DONE);
        return taskResult;
      } catch (error) {
        updateTaskStatus(taskId, STATUS_FAILED, error.message);
        throw error;
      }
    },
    [updateTaskStatus]
  );

  return {
    ...rest,
    updateTask,
    updateTaskMessage,
    runTask,
  };
};
