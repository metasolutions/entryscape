import PropTypes from 'prop-types';
import { localize } from 'commons/locale';
import { useState } from 'react';
import {
  Button,
  Card,
  CardHeader,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  FormControl,
  FormLabel,
  Typography,
  Radio,
} from '@mui/material';
import escoRdformsNLS from 'commons/nls/escoRdforms.nls';
import { useTranslation } from 'commons/hooks/useTranslation';
import ContentWrapper from 'commons/components/common/ContentWrapper';
import InformationBlock from 'commons/components/InformationBlock';
import './ProfileChooserDialog.scss';

const ProfileChooserDialog = ({
  entityTypes,
  selectedId: preselectedId,
  onChangeEntityType,
  open,
  onClose,
}) => {
  const t = useTranslation(escoRdformsNLS);
  const [selectedId, setSelectedId] = useState(preselectedId);

  const handleChange = ({ target: { value } }) => setSelectedId(value);

  const handleSwitch = () => {
    const selectedType = entityTypes.find(
      (type) => type.getId() === selectedId
    );
    onChangeEntityType(selectedType);
    onClose();
  };

  const handleClose = () => {
    onClose();
    setSelectedId(preselectedId);
  };

  const handleCardClick = (value) => setSelectedId(value);

  return (
    <Dialog open={open} onClose={handleClose}>
      <DialogTitle>{t('entityChooserDialogTitle')}</DialogTitle>
      <DialogContent dividers>
        <ContentWrapper>
          <FormControl fullWidth>
            <FormLabel id="entitytypes-radio-group-label">
              {t('entityChooserLabel')}
            </FormLabel>
            {entityTypes.map((entityType) => {
              const isSelected = selectedId === entityType.getId();
              const id = entityType.getId();
              const label = entityType.label();
              const description = entityType.description();

              return (
                <Card
                  key={`card-${id}`}
                  classes={{
                    root: isSelected
                      ? 'escoProfileChooser__card escoProfileChooser__card--selected'
                      : 'escoProfileChooser__card',
                  }}
                  style={{ cursor: 'pointer' }}
                  onClick={() => handleCardClick(id)}
                >
                  <CardHeader
                    avatar={
                      <Radio
                        checked={isSelected}
                        onChange={handleChange}
                        value={id}
                        name="profile-radio-buttons"
                        inputProps={{ 'aria-label': localize(label) }}
                      />
                    }
                    disableTypography
                    title={
                      <Typography
                        variant="h4"
                        gutterBottom={Boolean(description)}
                      >
                        {localize(label)}
                      </Typography>
                    }
                    subheader={
                      <Typography variant="body2">
                        {localize(description)}
                      </Typography>
                    }
                  />
                </Card>
              );
            })}
          </FormControl>
          <InformationBlock
            className="escoProfileChooser__infoBlock"
            info={t('entityChooserInfo')}
          />
        </ContentWrapper>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose} variant="text">
          {t('entityChooserCancel')}
        </Button>
        <Button onClick={handleSwitch} disabled={selectedId === preselectedId}>
          {t('entityChooserSwitch')}
        </Button>
      </DialogActions>
    </Dialog>
  );
};

ProfileChooserDialog.propTypes = {
  entityTypes: PropTypes.arrayOf(PropTypes.shape({})),
  selectedId: PropTypes.string,
  onChangeEntityType: PropTypes.func,
  open: PropTypes.bool,
  onClose: PropTypes.func,
};

export default ProfileChooserDialog;
