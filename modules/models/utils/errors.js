export class CircularError extends Error {
  constructor(message) {
    super(message);
    this.name = 'CircularError';
  }
}
