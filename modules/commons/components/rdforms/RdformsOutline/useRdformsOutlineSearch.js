import { useState } from 'react';

const useRdformsOutlineSearch = () => {
  const [searchQuery, setSearchQuery] = useState('');

  const clearSearch = () => setSearchQuery('');

  const onSearchQueryChange = (event) => {
    setSearchQuery(event.target.value.toLowerCase());
  };

  return { searchQuery, onSearchQueryChange, clearSearch };
};

export default useRdformsOutlineSearch;
