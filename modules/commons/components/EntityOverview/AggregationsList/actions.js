import { canWriteMetadata } from 'commons/util/entry';
import {
  canReplaceEntityLink,
  canReplaceEntityFile,
  canDownloadEntity,
} from 'commons/util/store';
import {
  LIST_ACTION_REMOVE,
  LIST_ACTION_REVISIONS,
} from 'commons/components/EntryListView/actions';
import DownloadResourceDialog from 'commons/components/common/dialogs/DownloadResourceDialog';
import ReplaceLinkDialog from 'commons/components/common/dialogs/ReplaceLinkDialog';
import ReplaceEntityFileDialog from '../dialogs/ReplaceEntityFileDialog';
import DisconnectDialog from '../dialogs/DisconnectDialog';
import RemoveAggregationDialog from '../dialogs/RemoveAggregationDialog';

export const actions = [
  {
    id: 'replace-link',
    Dialog: ReplaceLinkDialog,
    isVisible: ({ entry }) => canReplaceEntityLink(entry),
    labelNlsKey: 'replaceLinkMenu',
    getProps: ({ translate, addSnackbar }) => ({
      onLinkChangeSuccess: () =>
        addSnackbar({ message: translate('linkChangeSnackbar') }),
    }),
  },
  {
    id: 'replace-file',
    isVisible: ({ entry }) => canReplaceEntityFile(entry),
    labelNlsKey: 'replaceFileButtonLabel',
    Dialog: ReplaceEntityFileDialog,
    getProps: ({ translate, addSnackbar }) => ({
      onSuccess: () =>
        addSnackbar({ message: translate('fileChangeSnackbar') }),
    }),
  },
  {
    id: 'disconnect',
    Dialog: DisconnectDialog,
    isVisible: ({ entry }) => canWriteMetadata(entry),
    labelNlsKey: 'disconnectMenuItemLabel',
  },
  LIST_ACTION_REVISIONS,
  {
    id: 'download',
    isVisible: ({ entry }) => canDownloadEntity(entry),
    labelNlsKey: 'downloadButton',
    Dialog: DownloadResourceDialog,
  },
  { ...LIST_ACTION_REMOVE, Dialog: RemoveAggregationDialog },
];
