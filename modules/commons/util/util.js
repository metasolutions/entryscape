import isUrl from 'is-url';

/**
 *
 * @param {string} value
 * @returns {boolean}
 */
export const validateFileURI = (value) => {
  const trimmedValue = value.trim();
  if (trimmedValue.indexOf(' ') > 0) return false;

  try {
    const url = new URL(trimmedValue);
    const { protocol: protocolWithColon, host, pathname } = url;
    const protocol = protocolWithColon.slice(0, -1); // removes `:` from the end

    if (protocol !== 'file') return false;
    if (!host && trimmedValue.indexOf('///') < 1) return false;
    if (!pathname) return false;
    return true;
  } catch (_error) {
    return false;
  }
};

/**
 * Capitalizes first letter of string
 *
 * @param {string} str
 * @returns {string}
 */
export const capitalizeFirstLetter = (str) => {
  if (!str) return '';
  return str[0].toUpperCase() + str.substring(1);
};

export const isUri = (stringToCheck) => isUrl(stringToCheck);

/**
 * Checks if email is valid
 *
 * @param {string} email
 * @returns {boolean}
 */
export const validateEmail = (email) => {
  const regex =
    /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)+$/;
  return regex.test(email);
};

export const isInputTextValid = (text) => text && text.trim().length;

/**
 * Checks if text entered is alphaNumeric
 *
 * @param {string} text
 * @returns {boolean}
 */
export const isAlphaNumeric = (text) => {
  const regex = /^[0-9a-zA-Z]+$/;
  return regex.test(text);
};

/**
 * Removes whitespace
 *
 * @param {string} value
 * @returns {(string)}
 */

export const removeWhitespace = (value) => {
  if (!value?.trim()) return '';
  return value;
};

export const isExternalLink = (url) => {
  const anchor = document.createElement('a');
  anchor.href = url;

  // Check empty hostname for IE11
  if (anchor.hostname === '') {
    return false;
  }

  return anchor.hostname !== window.location.hostname;
};

/**
 * Converts bytes to mega bytes.
 * NOTE! there is no sanity check for the give param 'bytes'
 *
 * @param {number} bytes
 * @returns {number}
 */
export const convertBytesToMBytes = (bytes) =>
  Number(parseFloat(bytes / 1048576).toFixed(2));

export const LIST_PAGE_SIZE_SMALL = 5;
export const LIST_PAGE_SIZE_MEDIUM = 20;
export const LIST_PAGE_SIZE_LARGE = 50;

export const getRandomInt = (max) =>
  Math.floor(Math.random() * Math.floor(max));

const REPLACEMENT_DICTIONARY = {
  de: [
    ['ä', 'ae'],
    ['Ä', 'AE'],
    ['å', 'a'],
    ['Å', 'A'],
    ['ö', 'oe'],
    ['Ö', 'OE'],
    ['ü', 'ue'],
    ['Ü', 'UE'],
    ['ß', 'ss'],
  ],
  sv: [
    ['ä', 'a'],
    ['Ä', 'A'],
    ['å', 'a'],
    ['Å', 'A'],
    ['ö', 'o'],
    ['Ö', 'O'],
    ['ü', 'ue'],
    ['Ü', 'UE'],
    ['ß', 'ss'],
  ],
};

/**
 * Function for replacing German/Swedish special character
 *
 * @param {string} value
 * @param {string} language
 * @returns {string}
 */
export const replaceSpecialChar = (value, language) => {
  let replacedValue = value;
  let lang = language.toLowerCase();

  if (!REPLACEMENT_DICTIONARY.hasOwnProperty(lang)) {
    lang = 'sv';
  }

  REPLACEMENT_DICTIONARY[lang].forEach(([originalChar, replacedChar]) => {
    const regOriginalChar = new RegExp(originalChar, 'g');
    replacedValue = replacedValue.replace(regOriginalChar, replacedChar);
  });

  return replacedValue;
};

/**
 * Truncates a string if its length exceeds the specified max length, adding
 * ellipses at the end.
 *
 * @param {string} string
 * @param {number} maxLength
 * @returns {string}
 */
export const truncate = (string, maxLength = 160) => {
  if (!string) return '';
  if (string.length <= maxLength) return string;
  return `${string.substring(0, maxLength).trimEnd()}...`;
};

/**
 * Truncates label and generates a truncated tooltip
 *
 * @param {string} label
 * @param {number} labelMaxLength
 * @param {number} tooltipMaxLength
 * @returns {Array<string>}
 */
export const truncateLabel = (
  label,
  labelMaxLength = 70,
  tooltipMaxLength = 140
) => {
  const needsTruncation = label?.length > labelMaxLength;
  if (!needsTruncation) return [label, ''];
  return [truncate(label, labelMaxLength), truncate(label, tooltipMaxLength)];
};
