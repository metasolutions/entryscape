export const ACTION_CREATE_ID = 'create';

export const ACTION_EDIT_ID = 'edit';

export const ACTION_INFO_ID = 'info';

export const ACTION_REMOVE_ID = 'remove';

export const ACTION_REVISIONS_ID = 'revisions';

export const ACTION_SHARING_SETTINGS_ID = 'sharing-settings';

export const ACTION_DOWNLOAD_ID = 'download';

export const ACTION_IMPORT_ID = 'import';

export const ACTION_MIGRATE_ID = 'migrate';
