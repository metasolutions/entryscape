import { PENDING, REJECTED } from 'commons/hooks/useAsync';
import Pagination from 'commons/components/Pagination';
import PropTypes from 'prop-types';
import { PAGINATE, useListModel, LIMIT } from '../hooks/useListModel';
import { useListView } from '../hooks/useListView';

export const ListPagination = ({
  rowsPerPageOptions = [3, 5, 10, 15, 20],
  ...rest
}) => {
  const [{ page, limit }, dispatch] = useListModel();
  const { size, status } = useListView();

  const handleChangeRowsPerPage = (event) => {
    dispatch({ type: LIMIT, value: parseInt(event.target.value, 10) });
  };

  if (
    size < 1 ||
    status === PENDING ||
    status === REJECTED ||
    typeof page !== 'number'
  )
    return null;

  return (
    <Pagination
      {...rest}
      rowsPerPage={limit}
      onRowsPerPageChange={handleChangeRowsPerPage}
      count={size}
      page={page}
      rowsPerPageOptions={rowsPerPageOptions}
      onPageChange={(_, newPage) =>
        dispatch({ type: PAGINATE, value: newPage })
      }
    />
  );
};

ListPagination.propTypes = {
  rowsPerPageOptions: PropTypes.arrayOf(PropTypes.number),
};
