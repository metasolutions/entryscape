import { useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import Dialog from '@mui/material/Dialog';
// eslint-disable-next-line import/no-extraneous-dependencies
import rspecs from 'rdforms-specs';
import { namespaces as ns } from '@entryscape/rdfjson';
import { Entry } from '@entryscape/entrystore-js';
import useAsync from 'commons/hooks/useAsync';
import './PreviewSpecDialog.scss';
import { Button } from '@mui/material';
import {
  nlsBundlesPropType,
  useTranslation,
} from 'commons/hooks/useTranslation';
import esmoCommonsNls from 'models/nls/esmoCommons.nls';
import ErrorBoundary from 'commons/errors/ErrorBoundary';
import ErrorPlaceholder from 'commons/errors/ErrorPlaceholder';
import useErrorHandler from 'commons/errors/hooks/useErrorHandler';
import { RDF_TYPE_PROFILE_FORM, RDF_TYPE_OBJECT_FORM } from 'models/utils/ns';
import { getSpecificationResources } from './utils/preview';

/**
 * Find top level form ids for an rdf type. A form is considered top level if
 * it's not extended by any other form within the same bundle.
 *
 * @param {object[]} templates
 * @param {string} rdfType
 * @returns {string[]}
 */
const findTopLevelFormIds = (templates, rdfType) => {
  const formIds = [];
  const extendedFormIds = [];
  templates
    .filter((template) => {
      const { about } = template;
      if (!about) {
        console.log(
          'No about property detected for the group. Please create a new bundle and try again.'
        );
        return false;
      }
      return about.rdfType.includes(ns.expand(rdfType));
    })
    .forEach((template) => {
      const { extends: extendedId, id } = template;
      if (extendedId) {
        extendedFormIds.push(extendedId);
      }
      formIds.push(id);
    });
  return formIds.filter((id) => !extendedFormIds.includes(id));
};

/**
 *
 * @param {object[]} templates
 * @returns {object}
 */
const findMainAndSupportive = (templates) => {
  const main = findTopLevelFormIds(templates, RDF_TYPE_PROFILE_FORM);
  const supportive = findTopLevelFormIds(templates, RDF_TYPE_OBJECT_FORM);

  return { main, supportive };
};

const PreviewFallback = () => {
  const translate = useTranslation(esmoCommonsNls);

  return (
    <ErrorPlaceholder
      message={translate('previewLoadError')}
      header={translate('previewErrorHeading')}
    />
  );
};

const Preview = ({ entry }) => {
  const navRef = useRef();
  const mainRef = useRef();
  const {
    runAsync,
    data: { image },
    error,
  } = useAsync({ data: {} });
  useErrorHandler(error);

  useEffect(() => {
    const initRspecs = (resources) => {
      const { bundle } = resources;
      const templates = bundle.templates || [];
      const { main, supportive } = findMainAndSupportive(templates);

      rspecs(
        {
          bundle,
          main,
          introduction: entry
            .getMetadata()
            .findFirstValue(null, 'dcterms:description'),
          supportive,
        },
        navRef.current,
        mainRef.current
      );

      return resources;
    };
    runAsync(getSpecificationResources(entry).then(initRspecs));
  }, [entry, runAsync]);

  return (
    <div className="esmoSpecificationPreview">
      <div className="rdforms-specs">
        <div className="head">
          <h1 className="title">
            {entry.getMetadata().findFirstValue(null, 'dcterms:title')}
          </h1>
          <hr title="separator" />
        </div>
        {image ? <img width="800" src={image} alt="resource" /> : null}
        <div ref={navRef} />
        <div ref={mainRef} />
      </div>
    </div>
  );
};

Preview.propTypes = {
  entry: PropTypes.instanceOf(Entry),
};

const PreviewDialog = ({ entry, closeDialog, nlsBundles }) => {
  const translate = useTranslation(nlsBundles);
  return (
    <Dialog fullScreen open>
      <ErrorBoundary FallbackComponent={PreviewFallback}>
        <Preview entry={entry} />
      </ErrorBoundary>
      <Button
        onClick={closeDialog}
        variant="contained"
        autoFocus
        sx={{ position: 'fixed', bottom: '5px', right: '15px' }}
      >
        {translate('close')}
      </Button>
    </Dialog>
  );
};

PreviewDialog.propTypes = {
  entry: PropTypes.instanceOf(Entry),
  closeDialog: PropTypes.func,
  nlsBundles: nlsBundlesPropType,
};

export default PreviewDialog;
