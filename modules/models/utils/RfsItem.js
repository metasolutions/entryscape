import { namespaces as ns, utils } from '@entryscape/rdfjson';
import { isType } from 'commons/util/metadata';
import { RDF_PROPERTY_NODETYPE } from './ns';

class RfsItem {
  constructor({
    graph,
    resourceURI,
    baseRdfType,
    rdfType,
    commonProperties,
    properties = [],
    nodetypes,
    compatibleTypes,
    nlsKeyLabel,
  }) {
    this._graph = graph;
    this._resourceURI = resourceURI;
    this._baseRdfType = baseRdfType;
    this._rdfType = rdfType;
    this._commonProperties = commonProperties;
    this._properties = properties;
    this._nodetypes = nodetypes;
    this._compatibleTypes = compatibleTypes;
    this._nlsKeyLabel = nlsKeyLabel;
  }

  getRdfType() {
    return this._rdfType;
  }

  getBaseRdfType() {
    return this._baseRdfType;
  }

  getProperties() {
    return this._properties;
  }

  getCommonProperties() {
    return this._commonProperties;
  }

  getAllProperties() {
    return [...this.getCommonProperties(), ...this.getProperties()];
  }

  getNodetypes(expand) {
    if (expand) return this._nodetypes.map((nodetype) => ns.expand(nodetype));
    return this._nodetypes;
  }

  getCompatibleTypes() {
    return this._compatibleTypes;
  }

  validateIsType() {
    return isType(this._graph, this._resourceURI, this.getRdfType());
  }

  validateType() {
    if (this.validateIsType()) return;
    this._graph.findAndRemove(this._resourceURI, 'rdf:type');
    this._graph.add(this._resourceURI, 'rdf:type', this.getBaseRdfType());
    this._graph.add(this._resourceURI, 'rdf:type', this.getRdfType());
  }

  validateNodetype() {
    const nodetype = this._graph.findFirstValue(
      this._resourceURI,
      RDF_PROPERTY_NODETYPE
    );
    const nodetypes = this.getNodetypes(true);
    // compotatible nodetypes
    if (!nodetypes && !nodetype) return;
    if (nodetypes?.includes(nodetype)) return;

    // non compatible nodetypes
    this._graph.findAndRemove(this._resourceURI, RDF_PROPERTY_NODETYPE);
    if (!nodetypes) return;
    this._graph.add(this._resourceURI, RDF_PROPERTY_NODETYPE, nodetypes[0]);
  }

  validateProperties(properties) {
    properties?.forEach((property) => {
      if (this._graph.findFirstValue(this._resourceURI, property)) {
        this.removePropertyValue(property);
      }
    });
    this.validateType();
    this.validateNodetype();
  }

  removePropertyValue(property) {
    const statements = this._graph.find(this._resourceURI, property);
    statements.forEach((statement) => {
      this._graph.remove(statement);
      // remove entire subgraph if bnode
      if (statement.getType() === 'bnode') {
        utils.remove(this._graph, statement.getValue());
      }
    });
  }

  getPropertiesWithValues() {
    return this.getProperties().filter((property) => {
      return this._graph.findFirstValue(this._resourceURI, property);
    });
  }

  getNlsKeyLabel() {
    return this._nlsKeyLabel;
  }
}

export default RfsItem;
