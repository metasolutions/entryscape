import React, { useEffect, useState } from 'react';
import useNavigate from 'commons/components/router/useNavigate';
import { Button, Typography } from '@mui/material';
import { Add, Close } from '@mui/icons-material';
import { useTranslation } from 'commons/hooks/useTranslation';
import { useSnackbar } from 'commons/hooks/useSnackbar';
import AlertDialog from 'commons/components/common/dialogs/AlertDialog';
import esreMergeNLS from 'toolkit/nls/esreMerge.nls';
import esreSourceNLS from 'toolkit/nls/esreSource.nls';
import esreCatalogDetectNLS from 'toolkit/nls/esreCatalogDetect.nls';
import UploadDialog from 'toolkit/Catalog/UploadDialog';
import withLocationChangeListener from 'toolkit/LocationChangeListener';
import useMainCatalog from './hooks/useMainCatalog';
import './index.scss';
import { MergeCard, MergeCardContent, MergeCardHeader } from './MergeCard';
import { MergeList, MergeListItem } from './MergeList';
import useCatalogList from './hooks/useCatalogList';
import mergeCatalogs from './utils/mergeScript';

const Merge = () => {
  const { navigate } = useNavigate();
  const t = useTranslation([esreMergeNLS, esreSourceNLS, esreCatalogDetectNLS]);
  const [showAlert, setShowAlert] = useState(false);
  const [alertError, setAlertError] = useState('');
  const [showUploadDialog, setShowUploadDialog] = useState(false);
  const [countByType, mainCatalogError, mainCatalog, setMainCatalog] =
    useMainCatalog();
  const { addCatalogDetect, catalogDetectList, dispatch } = useCatalogList();
  const [addSnackbar] = useSnackbar();

  useEffect(() => {
    if (mainCatalogError) {
      setAlertError(mainCatalogError);
      setShowAlert(true);
    }
  }, [mainCatalogError]);

  const addCatalog = (catalog) => {
    try {
      addCatalogDetect(catalog);
    } catch (error) {
      const message = t(...error.message.split(','));
      setAlertError(message);
      setShowAlert(true);
    }
  };

  const mergeCatalogsWithMainCatalog = () => {
    const catalogList = catalogDetectList.map((catalogDetect) =>
      catalogDetect.getCatalog()
    );
    const mergedCatalog = mergeCatalogs(mainCatalog, catalogList);
    dispatch({ type: 'clear' });
    setMainCatalog(mergedCatalog);
    addSnackbar({ message: t('catalogMerged') });
  };

  const handleAlertClose = () => {
    setShowAlert(false);
    if (mainCatalogError) navigate('/toolkit/catalog');
  };

  return (
    <>
      <Typography variant="h2" className="esreMerge__header">
        {t('mergeHeader')}
      </Typography>
      {countByType && (
        <>
          <div>
            <MergeCard>
              <MergeCardHeader title={t('mainCatalogHeader')} />
              <MergeCardContent>
                <MergeList
                  title={`${t('catalogSource')} ${t('mainCatalogHeader')}`}
                >
                  <MergeListItem value={t('catalogStatus', countByType)} />
                </MergeList>
              </MergeCardContent>
            </MergeCard>
            <MergeCard>
              <MergeCardHeader
                title={t('mergeCatalogListHeader')}
                actionButtonProps={{
                  label: t('addMergeCatalog'),
                  startIcon: <Add />,
                  onClick: () => setShowUploadDialog(true),
                }}
              />
              <MergeCardContent>
                {catalogDetectList.length ? (
                  <MergeList
                    title={`${t('catalogSource')} ${t('mainCatalogHeader')}`}
                  >
                    {catalogDetectList.map((catalogDetect) => (
                      <MergeListItem
                        key={catalogDetect.id}
                        value={t(
                          'catalogStatus',
                          catalogDetect.getCountByType()
                        )}
                        actionButtonProps={{
                          onClick: () =>
                            dispatch({
                              type: 'remove',
                              catalogDetect,
                            }),
                          'aria-label': t('removeCatalogLabel'),
                          icon: <Close />,
                        }}
                      />
                    ))}
                  </MergeList>
                ) : (
                  <Typography className="esreMergeList__emptyListText">
                    {t('noCatalogAddedToMerge')}
                  </Typography>
                )}
              </MergeCardContent>
            </MergeCard>
          </div>
          <div className="esreMerge__actions">
            <Button
              className="esreMerge__button"
              onClick={mergeCatalogsWithMainCatalog}
              variant={catalogDetectList.length ? 'contained' : 'outlined'}
              disabled={!catalogDetectList.length}
            >
              {t('mergeCatalogsButtonIntoMain')}
            </Button>
          </div>
        </>
      )}
      <AlertDialog
        open={showAlert}
        handleClose={handleAlertClose}
        buttonLabel={t('noRDFProceed')}
      >
        {alertError}
      </AlertDialog>
      <UploadDialog
        open={showUploadDialog}
        setOpen={setShowUploadDialog}
        setGraph={addCatalog}
        setRDFXML={() => {}}
        setEditorTab={() => {}}
      />
    </>
  );
};

export default withLocationChangeListener(Merge);
