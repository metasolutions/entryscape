import PropTypes from 'prop-types';
import TextField from '@mui/material/TextField';
import RDFormsFieldWrapper from 'commons/components/rdforms/FieldWrapper';

const LinkField = ({
  mimicsRdformsStyle,
  inputLabel,
  link,
  handleLinkInputChange,
  hasError,
  helperText,
  placeholder,
}) => {
  return (
    <>
      {mimicsRdformsStyle ? (
        <RDFormsFieldWrapper label={inputLabel} editor>
          <TextField
            value={link}
            onChange={handleLinkInputChange}
            error={hasError}
            helperText={helperText}
            placeholder={placeholder}
            inputProps={{ 'aria-label': inputLabel }}
            required
          />
        </RDFormsFieldWrapper>
      ) : (
        <TextField
          label={inputLabel}
          value={link}
          onChange={handleLinkInputChange}
          error={hasError}
          helperText={helperText}
          placeholder={placeholder}
          required
        />
      )}
    </>
  );
};

LinkField.propTypes = {
  mimicsRdformsStyle: PropTypes.bool,
  inputLabel: PropTypes.string,
  link: PropTypes.string,
  handleLinkInputChange: PropTypes.func,
  hasError: PropTypes.bool,
  helperText: PropTypes.string,
  placeholder: PropTypes.string,
};

export default LinkField;
