import { getPathFromViewName } from 'commons/util/site';
import { findNonCompositionEntityTypes } from 'entitytype-lookup';
import {
  DESCRIPTION_UPDATED,
  DESCRIPTION_CREATED,
  DESCRIPTION_PROJECT_TYPE,
} from 'commons/components/overview/actions';
import Lookup from 'commons/types/Lookup';
import { localize } from 'commons/locale';
import {
  getPrimaryEntityTypes,
  getEntitytypeCountQuery,
} from '../../utils/entitytypes';

const getRowActions = (entityTypes, context) => {
  const rowActionsPromises = entityTypes.map(async (entityType) => {
    const queryCount = await getEntitytypeCountQuery(context, entityType);
    const name = entityType.get('name');
    const link = getPathFromViewName('workbench__entities__type', {
      contextId: context.getId(),
      entityName: name,
    });

    return {
      getProps: () => ({
        label: localize(entityType.label()),
        labelPrefix: queryCount,
        to: link,
        isPrimary: entityType.get('main'),
      }),
    };
  });

  return Promise.all(rowActionsPromises);
};

/**
 * Custom getOverviewData function for Workbench
 *
 * @param {Entry} entry
 * @param {Context} context
 * @param {function} translate
 * @param {object} userInfo
 * @returns {Promise<object>}
 */

const getWorkbenchOverviewData = async (
  entry,
  context,
  translate,
  userInfo
) => {
  const entityTypes = await getPrimaryEntityTypes(context, userInfo);
  const visibleEntityTypes = findNonCompositionEntityTypes(entityTypes);
  const rowActions = await getRowActions(visibleEntityTypes, context);
  const entitiesTotalCount = rowActions.reduce(
    (acc, stat) => acc + Number(stat.getProps().labelPrefix),
    0
  );
  const projectType = await Lookup.getProjectTypeInUse(context);

  const data = {
    entry,
    entityTypes: visibleEntityTypes,
    rowActions,
    entitiesTotalCount,
  };

  data.descriptionItems = [
    DESCRIPTION_CREATED,
    DESCRIPTION_UPDATED,
    {
      ...DESCRIPTION_PROJECT_TYPE,
      getValues: () =>
        projectType
          ? [projectType.source.name]
          : [translate('defaultProjectType')],
    },
    {
      id: 'configuredTypes',
      labelNlsKey: 'configuredEntitiesLabel',
      getValues: () => [visibleEntityTypes.length.toString()],
    },
    {
      id: 'configuredTypesCount',
      labelNlsKey: 'entitiesCountLabel',
      getValues: () => [entitiesTotalCount.toString()],
    },
  ];
  data.entityTypes = visibleEntityTypes;

  return data;
};

export default getWorkbenchOverviewData;
