import PropTypes from 'prop-types';
import TypeCreateDialog from 'admin/configs/types/TypeCreateDialog';
import { EditorProvider } from 'commons/components/forms/editors';
import { useMemo } from 'react';
import { Entry } from '@entryscape/entrystore-js';
import { useTranslation } from 'commons/hooks/useTranslation';
import esadTypeNLS from 'admin/nls/esadType.nls';
import esadEntityTypeNLS from 'admin/nls/esadEntityType.nls';
import {
  RDF_CLASS_CUSTOM_ENTITY_TYPE,
  RDF_CLASS_ENTITY_TYPE,
} from 'admin/configs/utils/namespace';
import { getConfigContext } from 'admin/configs/utils/configQuery';
import EntityTypeForm from '../EntityTypeForm';
import { editorFields } from '../utils/editorFields';

const NLS_BUNDLES = [esadTypeNLS, esadEntityTypeNLS];

const CreateEntityTypeDialog = ({ closeDialog, entityTypeEntries }) => {
  const t = useTranslation(NLS_BUNDLES);
  const context = getConfigContext();

  const prototypeEntry = useMemo(() => {
    return context.newEntry();
  }, [context]);

  return (
    <EditorProvider entry={prototypeEntry}>
      <TypeCreateDialog
        title={t('createHeader')}
        closeDialog={closeDialog}
        rdfTypes={[RDF_CLASS_ENTITY_TYPE, RDF_CLASS_CUSTOM_ENTITY_TYPE]}
        nlsBundles={NLS_BUNDLES}
        fields={editorFields}
      >
        <EntityTypeForm
          entityTypeEntries={entityTypeEntries}
          nlsBundles={NLS_BUNDLES}
        />
      </TypeCreateDialog>
    </EditorProvider>
  );
};

CreateEntityTypeDialog.propTypes = {
  closeDialog: PropTypes.func.isRequired,
  entityTypeEntries: PropTypes.arrayOf(PropTypes.instanceOf(Entry)),
};

export default CreateEntityTypeDialog;
