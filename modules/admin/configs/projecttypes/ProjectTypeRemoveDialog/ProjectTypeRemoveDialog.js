import RemoveEntryDialog from 'commons/components/entry/RemoveEntryDialog';
import useNavigate from 'commons/components/router/useNavigate';

import { getPathFromViewName } from 'commons/util/site';

const ProjectTypeRemoveDialog = (props) => {
  const { navigate } = useNavigate();

  return (
    <RemoveEntryDialog
      {...props}
      onRemoveCallback={() =>
        navigate(getPathFromViewName('admin__projecttypes'))
      }
    />
  );
};

export default ProjectTypeRemoveDialog;
