import escaDocumentsNLS from 'catalog/nls/escaDocuments.nls';

export const IMPORTED_SPEC_TAG = {
  id: 'importedSpecificationTag',
  labelNlsKey: 'remoteSpecIconTooltip',
  nlsBundles: escaDocumentsNLS,
  isVisible: (entry) => entry.isLinkReference(),
};
