import { useTranslation } from 'commons/hooks/useTranslation';
import esmoCommonsNls from 'models/nls/esmoCommons.nls';
import PropTypes from 'prop-types';
import { graphPropType } from 'commons/util/entry';
import { Extends, hasValue } from 'models/components/Extends';

/**
 * Shows extended values if the field is extendable and if the field has no
 * overridden value
 */
export const FieldPresenter = ({
  graph,
  resourceURI,
  extendable,
  labelNlsKey,
  Presenter,
  isVisible = () => true,
  ...fieldProps
}) => {
  const translate = useTranslation(esmoCommonsNls);

  if (!isVisible(graph, resourceURI)) return null;

  return !hasValue(graph, resourceURI, fieldProps.property) ? (
    <Extends
      key={fieldProps.property}
      graph={graph}
      resourceURI={resourceURI}
      labelNlsKey={labelNlsKey}
      Component={FieldPresenter}
      Presenter={Presenter}
      {...fieldProps}
    />
  ) : (
    <Presenter
      key={fieldProps.property}
      graph={graph}
      resourceURI={resourceURI}
      label={labelNlsKey ? translate(labelNlsKey) : ''}
      translate={translate}
      {...fieldProps}
    />
  );
};

FieldPresenter.propTypes = {
  graph: graphPropType,
  resourceURI: PropTypes.string,
  extendable: PropTypes.bool,
  labelNlsKey: PropTypes.string,
  Presenter: PropTypes.func,
  isVisible: PropTypes.func,
};
