import PropTypes from 'prop-types';
import {
  Assessment,
  Map,
  ShowChart,
  TableChart,
  PieChart,
} from '@mui/icons-material';
import { Entry } from '@entryscape/entrystore-js';
import {
  ListItemOpenMenuButton,
  List,
  ListItem,
  ActionsMenu,
  ListItemText,
  ListView,
  ListItemIcon,
  withListModelProvider,
  ListItemActionsGroup,
} from 'commons/components/ListView';
import { getIconFromActionId } from 'commons/actions';
import { useTranslation } from 'commons/hooks/useTranslation';
import escaVisualizationNLS from 'catalog/nls/escaVisualization.nls';
import escaDatasetNLS from 'catalog/nls/escaDataset.nls';
import escoListNLS from 'commons/nls/escoList.nls';
import esmoCommonsNLS from 'models/nls/esmoCommons.nls';
import { getModifiedDate } from 'commons/util/metadata';
import { getShortDate } from 'commons/util/date';
import { useVisualizations } from 'catalog/datasets/DatasetOverview/hooks/useVisualizations';
import { OverviewListHeader } from 'commons/components/overview/OverviewListHeader';
import { OverviewListPlaceholder } from 'commons/components/overview';
import {
  LIST_ACTION_INFO,
  LIST_ACTION_EDIT,
} from 'commons/components/EntryListView/actions';
import VisualizationLDBDialog from '../dialogs/VisualizationLDBDialog';
import EditVisualizationDialog from '../dialogs/EditVisualizationDialog';
import { getChartType, getVisualizationTitle } from '../utils/visualizations';
import { rowActions, presenterAction } from './actions';
import CreateVisualizationDialog from '../dialogs/CreateVisualizationDialog';

export const ChartIcon = ({ type }) => {
  if (type === 'bar') return <Assessment />;
  if (type === 'pie') return <PieChart />;
  if (type === 'line') return <ShowChart />;
  if (type === 'table') return <TableChart />;
  return <Map />;
};
ChartIcon.propTypes = {
  type: PropTypes.string,
};

const nlsBundles = [
  escoListNLS,
  escaVisualizationNLS,
  escaDatasetNLS,
  esmoCommonsNLS,
];

const VisualizationList = ({ entry: datasetEntry }) => {
  const { visualizations, refreshVisualizations, status } =
    useVisualizations(datasetEntry);
  const translate = useTranslation(nlsBundles);

  return (
    <>
      <OverviewListHeader
        heading={translate('visualizationsTitle')}
        action={{
          Dialog: CreateVisualizationDialog,
          tooltip: translate('createViz'),
        }}
        actionParams={[datasetEntry, refreshVisualizations]}
        highlightId="visualizationsListHeader"
      />
      <ListView
        size={visualizations?.length ?? -1}
        nlsBundles={[escaVisualizationNLS, escoListNLS]}
        renderPlaceholder={() => (
          <OverviewListPlaceholder
            label={translate('visualizationListPlaceholder')}
          />
        )}
        status={status}
      >
        <List renderLoader={(Loader) => <Loader height="10vh" />}>
          {visualizations &&
            visualizations.map((visualizationEntry) => {
              const actionParams = {
                visualizationEntry,
                refreshVisualizations,
              };

              const actionsMenuItems = rowActions
                .filter(({ isVisible }) =>
                  isVisible ? isVisible({ entry: datasetEntry }) : true
                )
                .map((rowAction) => ({
                  action: {
                    ...rowAction,
                    entry: datasetEntry,
                    ...actionParams,
                  },
                  icon: getIconFromActionId(rowAction.id),
                  label: translate(rowAction.nlsKeyLabel),
                }));
              return (
                <ListItem key={visualizationEntry.getId()}>
                  <ListItemIcon
                    Icon={
                      ChartIcon({ type: getChartType(visualizationEntry) }).type
                    }
                  />
                  <ListItemText
                    xs={6}
                    primary={
                      getVisualizationTitle(visualizationEntry) ||
                      translate('untitled')
                    }
                  />
                  <ListItemText
                    xs={2}
                    secondary={getShortDate(
                      getModifiedDate(visualizationEntry)
                    )}
                  />
                  <ListItemActionsGroup
                    xs={2}
                    justifyContent="end"
                    actions={[
                      {
                        ...LIST_ACTION_INFO,
                        Dialog: VisualizationLDBDialog,
                        getProps: () => ({
                          entry: visualizationEntry,
                          titleNlsKey: 'infoDialogHeader',
                          title: translate(presenterAction.labelNlsKey),
                        }),
                      },
                      {
                        ...LIST_ACTION_EDIT,
                        Dialog: EditVisualizationDialog,
                        getProps: () => ({
                          visualizationEntry,
                          entry: datasetEntry,
                          refreshVisualizations,
                          title: translate('editEntry'),
                        }),
                      },
                    ]}
                  />
                  <ListItemOpenMenuButton />
                  <ActionsMenu items={actionsMenuItems} />
                </ListItem>
              );
            })}
        </List>
      </ListView>
    </>
  );
};
VisualizationList.propTypes = {
  entry: PropTypes.instanceOf(Entry),
};

export default withListModelProvider(VisualizationList);
