import { entrystore } from 'commons/store';
import { getLabel } from 'commons/util/rdfUtils';
import { getMultipleDateFormats } from 'commons/util/date';
import { spreadEntry } from 'commons/util/store';
import { utils } from '@entryscape/rdfjson';

/**
 * Checks if user is deleted
 *
 * @param {any} user
 * @returns {boolean}
 */
export const isDeletedUser = (user) => user === undefined || user === null;

/**
 * Gets revisions from entry and loads a (author) user entry for each revision
 *
 * @param {store/Entry} entry
 * @returns {Promise<Array<{revision}} entry
 */
export const loadRevisions = async (entry) => {
  const revisions = entry.getEntryInfo().getMetadataRevisions();
  await Promise.all(
    revisions.map((revision) =>
      entrystore
        .getEntry(revision.by)
        .then((userEntry) => {
          revision.user = userEntry;
        })
        .catch(() => {
          revision.user = null;
        })
    )
  );

  return revisions;
};

/**
 *
 * @param {revision} param0
 * @param {store/Entry} entry
 * @returns {boolean}
 */
export const isCurrentRevision = ({ uri }, entry) =>
  entry.getEntryInfo().getGraph().findFirstValue(uri, 'owl:sameAs');

/**
 *
 * @param {{time, user, by, rev}} revision
 * @param {store/Entry} entry
 */
export const getRevisionRenderName = (revision, entry, translate) => {
  const { time, user } = revision;

  const name = isDeletedUser(user)
    ? translate('deletedUserRevision')
    : getLabel(user) ||
      user.getEntryInfo().getName() ||
      (user.getResource(true) && user.getResource(true).getName());
  const date = getMultipleDateFormats(time).timeMedium;

  const currentMessage = isCurrentRevision(revision, entry)
    ? translate('currentRevision')
    : '';

  return name
    ? `${date}, ${name} ${currentMessage}`
    : i18n.renderNLSTemplate(translate('noUserNameRevision'), {
        datetime: date,
        id: user.getId(),
      }) + currentMessage;
};

/**
 *
 * @param {Graph} graph1
 * @param {Graph} graph2
 * @param {string[]} excludeProperties
 */
export const isSimilar = (graph1, graph2, excludeProperties) => {
  if (!(graph1 && graph2)) return false;
  const fingerprint1 = utils.fingerprint(graph1, excludeProperties);
  const fingerprint2 = utils.fingerprint(graph2, excludeProperties);
  return fingerprint1 === fingerprint2;
};

/**
 *
 * @param {*} graph1
 * @param {*} graph2
 * @param {*} excludeProperties
 */
const preserveProps = (graph1, graph2, excludeProperties = []) => {
  excludeProperties.forEach((p) => {
    graph2.findAndRemove(null, p);
    graph1.find(null, p).forEach((stmt) => {
      graph2.add(stmt);
    });
  });
};

/**
 *
 * @param {rdfjson/Graph} graph1
 * @param {rdfjson/Graph} graph2
 * @param {boolean}
 */
export const checkHasExcludeDiff = (graph1, graph2, excludeProperties = []) => {
  if (!graph1 && graph2) return false;
  let diff = false;
  if (graph1 && graph2) {
    excludeProperties.forEach((p) => {
      graph2.find(null, p).forEach((stmt) => {
        if (
          graph1.find(stmt.getSubject(), stmt.getPredicate(), stmt.getObject())
            .length === 0
        ) {
          diff = true;
        }
      });
      graph1.find(null, p).forEach((stmt) => {
        if (
          graph2.find(stmt.getSubject(), stmt.getPredicate(), stmt.getObject())
            .length === 0
        ) {
          diff = true;
        }
      });
    });
  }

  return diff;
};

/**
 *
 * @param {*} entry
 * @param {*} graph
 */
export const getRevertExceptionsNLSKey = (entry, graph, excludeProperties) => {
  const { metadata } = spreadEntry(entry);
  if (!checkHasExcludeDiff(metadata, graph, excludeProperties)) return '';
  preserveProps(metadata, graph, excludeProperties);
  return 'distRevertExcludeMessage';
};

export const revertToGraph = async (entry, graph) => {
  const clonedGraph = graph.clone();

  //   confirm(b.revertMessage + re, b.revertConfirm, b.revertReject).then(() => {
  entry.setRefreshNeeded(true);
  await entry.refresh();
  entry.setMetadata(clonedGraph);
  return entry.commitMetadata();
  //   this.versionList.search();
  //   this.list.rowMetadataUpdated(this.row);
  //   });
};

/**
 *
 * @param {{ uri: string}} revision
 * @param {*} entry
 * @returns {Promise<rdfjson/Graph} revision graph
 */
export const getRevisionGraph = ({ uri }, entry) =>
  entry.getEntryInfo().getMetadataRevisionGraph(uri);

/**
 *
 * @param {revision} param0
 * @param {store/Entry} entry
 * @returns {Promise<rdfjson/Graph|null>} the previous revision graph
 */
export const getPreviousRevisionGraph = ({ uri: currentRevURI }, entry) => {
  const revisions = entry.getEntryInfo().getMetadataRevisions();
  const currentRevisionIdx = revisions.findIndex(
    ({ uri }) => uri === currentRevURI
  );
  const prevRevision = revisions[currentRevisionIdx + 1];

  return Promise.resolve(
    prevRevision ? getRevisionGraph(prevRevision, entry) : null
  );
};
