import PropTypes from 'prop-types';
import { graphPropType } from 'commons/util/entry';
import escoFormsNLS from 'commons/nls/escoForms.nls';
import { useTranslation } from 'commons/hooks/useTranslation';
import { getConstraints } from 'models/utils/constraint';
import useAsync from 'commons/hooks/useAsync';
import { useEffect } from 'react';
import { Fields, FieldsLabel } from 'commons/components/forms/presenters';
import './Constraints.scss';

export const Constraint = ({ property, values }) => {
  const translate = useTranslation(escoFormsNLS);

  return (
    <div className="esmoConstraint">
      <FieldsLabel
        alignItems="start"
        label={`${translate('constraintPropertyLabel')}: `}
      >
        <span className="esmoConstraint__value">{property}</span>
      </FieldsLabel>
      <FieldsLabel
        alignItems="start"
        label={`${translate('constraintObjectLabel')}: `}
      >
        <span className="esmoConstraint__value">{values}</span>
      </FieldsLabel>
    </div>
  );
};

Constraint.propTypes = {
  property: PropTypes.string,
  values: PropTypes.oneOfType([PropTypes.string, PropTypes.array]),
};

export const Constraints = ({
  graph,
  resourceURI,
  property,
  inline,
  children,
  ...fieldsProps
}) => {
  const { runAsync, data: constraints, status } = useAsync();

  useEffect(() => {
    runAsync(getConstraints(graph, resourceURI));
  }, [graph, resourceURI, runAsync]);

  return status === 'resolved' && constraints ? (
    <Fields {...fieldsProps}>
      {Object.entries(constraints).map(([constraintProperty, values]) => {
        return (
          <Constraint
            key={constraintProperty}
            property={constraintProperty}
            values={Array.isArray(values) ? values.join(',') : values}
          />
        );
      })}
      {children}
    </Fields>
  ) : null;
};

Constraints.propTypes = {
  graph: graphPropType,
  resourceURI: PropTypes.string,
  property: PropTypes.string,
  inline: PropTypes.bool,
  children: PropTypes.node,
};
