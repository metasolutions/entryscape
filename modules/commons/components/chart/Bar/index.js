import React, { useEffect, useRef, useState } from 'react';
import { i18n } from 'esi18n';
import escoChartNLS from 'commons/nls/escoChart.nls';
import { COLOURS_BAR_CHART } from '../colors';
import '../style.css';

export default ({
  data,
  colors = [],
  dimensions = {},
  type = 'bar',
  options = {},
}) => {
  const { width = '100%', height = '100%' } = dimensions;
  const chartContainer = useRef(null);
  const [chartInstance, setChartInstance] = useState(null);

  useEffect(() => {
    if (chartContainer && chartContainer.current) {
      import(/* webpackChunkName: "chart-js" */ 'chart.js')
        .then((Chart) => Chart.default)
        .then((Chart) => {
          setChartInstance(
            new Chart(chartContainer.current, { type, options })
          );
        });
    }
  }, [chartContainer]);

  const dataExists =
    chartInstance &&
    data &&
    data.datasets &&
    data.datasets.length > 0 &&
    data.datasets[0].data.length > 0;
  if (dataExists) {
    // update chart data
    chartInstance.data = {
      labels: [],
      ...data,
    };

    // merge colours to apply
    const colorsToApply = [...colors, ...COLOURS_BAR_CHART];

    // update chart colors and axes and re-render
    if (chartInstance.data.datasets) {
      const colorOptionsCount = colorsToApply.length;
      chartInstance.data.datasets.forEach((dataset, idx) => {
        const colorIdx = idx % colorOptionsCount; // repeat colors
        Object.assign(chartInstance.data.datasets[idx], { ...colorsToApply[colorIdx] }); // shallow is fine
      });

      chartInstance.update();
    }
  }

  const containerStyle = {
    position: 'relative',
    height: `${height}`,
    width: `${width}`,
  };

  const escoChart = i18n.getLocalization(escoChartNLS);
  return (
    <div className="chart-container" style={containerStyle}>
      <div className="no-data" className={dataExists ? 'escoChart__hidden' : ''}>{escoChart.noDataMessage}</div>
      <canvas aria-label="Statistics chart" role="img" className={dataExists ? '': 'escoChart__hidden'} ref={chartContainer} />
    </div>
  );
};
