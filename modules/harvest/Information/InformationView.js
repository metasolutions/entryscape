import { useESContext } from 'commons/hooks/useESContext';
import Information from './index';

const InformationView = () => {
  const { context } = useESContext();

  return <Information contextId={context.getId()} />;
};

export default InformationView;
