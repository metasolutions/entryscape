import PropTypes from 'prop-types';
import { getModuleLayoutPropsFunction } from 'commons/util/module';
import { useUserState } from 'commons/hooks/useUser';
import { SingleColumnLayout } from './SingleColumnLayout';
import { AsyncLayoutWrapper } from './AsyncLayoutWrapper';

export const MAIN_LAYOUT = 'main-layout';
export const SINGLE_COLUMN_LAYOUT = 'single-column-layout';

export const Layout = ({ layout = MAIN_LAYOUT, children, path, viewName }) => {
  const { userInfo } = useUserState();

  if (layout === SINGLE_COLUMN_LAYOUT)
    return <SingleColumnLayout>{children}</SingleColumnLayout>;

  return (
    <AsyncLayoutWrapper
      getLayoutProps={getModuleLayoutPropsFunction(userInfo)}
      route={path}
      viewName={viewName}
    >
      {children}
    </AsyncLayoutWrapper>
  );
};

Layout.propTypes = {
  layout: PropTypes.string,
  children: PropTypes.node,
  path: PropTypes.string,
  viewName: PropTypes.string,
};
