import PropTypes from 'prop-types';
import { Entry } from '@entryscape/entrystore-js';
import { OverviewRemoveEntryDialog } from 'commons/components/overview';
import useCheckReferences from 'commons/hooks/references/useCheckReferences';
import escaPublishersNLS from 'catalog/nls/escaPublishers.nls';
import { RESOLVED } from 'commons/hooks/useAsync';

const RemovePublisherDialog = ({
  entry: publisherEntry,
  closeDialog,
  ...rest
}) => {
  const { references, checkReferencesStatus } = useCheckReferences({
    entry: publisherEntry,
    nlsBundles: [escaPublishersNLS],
    callback: closeDialog,
  });

  if (checkReferencesStatus !== RESOLVED || references.length !== 0)
    return null;

  return (
    <OverviewRemoveEntryDialog
      entry={publisherEntry}
      closeDialog={closeDialog}
      {...rest}
    />
  );
};

RemovePublisherDialog.propTypes = {
  entry: PropTypes.instanceOf(Entry),
  closeDialog: PropTypes.func,
};

export default RemovePublisherDialog;
