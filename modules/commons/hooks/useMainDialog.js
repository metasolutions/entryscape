import PropTypes from 'prop-types';
import React, { useContext, useState, useCallback } from 'react';
import { MainDialogContext } from 'commons/contexts';

const initialState = {
  title: '',
  content: '',
  actions: '',
  onBackdropClick: null,
};

const MainDialogProvider = ({ children }) => {
  const [open, setOpen] = useState(false);
  const [dialogContent, setDialogContent] = useState(initialState);
  const [dialogProps, setDialogProps] = useState({
    ignoreBackdropClick: false,
  });

  const openMainDialog = useCallback(
    ({
      title,
      content,
      actions,
      onBackdropClick,
      dialogProps: mainDialogProps = {},
    }) => {
      setDialogContent({ title, content, actions, onBackdropClick });
      setOpen(true);
      setDialogProps({
        ignoreBackdropClick: false,
        ...mainDialogProps,
      });
    },
    []
  );
  const { ignoreBackdropClick } = dialogProps;

  const closeMainDialog = useCallback(
    (_event, reason) => {
      if (ignoreBackdropClick && reason === 'backdropClick') return;
      setOpen(false);
      setDialogContent(initialState);
    },
    [ignoreBackdropClick]
  );

  return (
    <MainDialogContext.Provider
      value={{
        open,
        closeMainDialog,
        dialogContent,
        setDialogContent,
        openMainDialog,
        dialogProps,
        setDialogProps,
      }}
    >
      {children}
    </MainDialogContext.Provider>
  );
};

MainDialogProvider.propTypes = {
  children: PropTypes.node,
};

const useMainDialog = () => {
  const dialogState = useContext(MainDialogContext);
  if (dialogState === undefined) {
    throw new Error('useMainDialog must be used within a ContextProvider');
  }
  return dialogState;
};

export { MainDialogProvider, useMainDialog };
