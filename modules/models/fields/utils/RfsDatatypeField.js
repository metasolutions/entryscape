import * as ns from 'models/utils/ns';
import RfsField from './RfsField';

const DATATYPE_FIELD_PROPERTIES = [
  ns.RDF_PROPERTY_DATATYPE,
  ns.RDF_PROPERTY_VALUE_TEMPLATE,
  ns.RDF_PROPERTY_PATTERN,
  ns.RDF_PROPERTY_DEPENDS_ON,
];

const DATATYPE_FIELD_NODETYPES = [ns.RDF_PROPERTY_DATATYPE_LITERAL];

class RfsDatatypeField extends RfsField {
  constructor(props) {
    super({
      ...props,
      rdfType: ns.RDF_TYPE_DATATYPE_FIELD,
      nlsKeyLabel: 'dataTypeLabel',
      properties: DATATYPE_FIELD_PROPERTIES,
      compatibleTypes: [],
      nodetypes: DATATYPE_FIELD_NODETYPES,
    });
  }
}

export default RfsDatatypeField;
