import { withListRefresh } from 'commons/components/ListView';
import CreateDataServiceDialog from '../dialogs/CreateDataServiceDialog';

export const listActions = [
  {
    id: 'create',
    Dialog: withListRefresh(CreateDataServiceDialog, 'onCreate'),
    isVisible: () => true,
    labelNlsKey: 'createButtonLabel',
    tooltipNlsKey: 'actionTooltip',
    highlightId: 'listActionCreate',
  },
];
