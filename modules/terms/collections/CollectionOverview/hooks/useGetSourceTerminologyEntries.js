import { useEffect } from 'react';
import { Entry } from '@entryscape/entrystore-js';
import { entrystoreUtil } from 'commons/store';
import useAsync, { RESOLVED } from 'commons/hooks/useAsync';
import { getAllMetadata } from 'commons/util/rdfUtils';
import useRemoteStore from './useRemoteStore';

/**
 * @param {Entry[]} conceptEntries
 * @returns {string[]}
 */
const getConceptScemeRURIs = (conceptEntries) => {
  const allSchemeRURIs = conceptEntries.map((conceptEntry) => {
    const metadata = getAllMetadata(conceptEntry);
    return metadata.findFirstValue(null, 'skos:inScheme');
  });
  return [...new Set(allSchemeRURIs)];
};

/**
 *
 * @param {string[]} conceptScemeRURIs
 * @param {Entry[]} entriesPerforated
 * @param {object} remoteStoreUtil
 * @returns {Promise<Entry[]|undefined>}
 */
const getRemoteConceptSchemeEntries = async (
  conceptScemeRURIs,
  entriesPerforated,
  remoteStoreUtil
) => {
  const remoteEntriesMask = entriesPerforated.map((entry) => !entry);
  if (!remoteEntriesMask.includes(true)) return;

  const remoteRURIs = conceptScemeRURIs.filter(
    (_uri, index) => remoteEntriesMask[index]
  );
  const remoteEntries = await remoteStoreUtil.loadEntriesByResourceURIs(
    remoteRURIs
  );
  return remoteEntries;
};

/**
 * @param {Entry[]} conceptEntries
 * @param {object} remoteStoreUtil
 * @returns {Promise<Entry[]>}
 */
const getSourceTerminologyEntries = async (conceptEntries, remoteStoreUtil) => {
  const conceptScemeRURIs = getConceptScemeRURIs(conceptEntries);

  // Last param adds null holes to the returned array for entries that were not found
  // allowing to look for them on the other entrystore
  const entriesPerforated = await entrystoreUtil.loadEntriesByResourceURIs(
    conceptScemeRURIs,
    null,
    true
  );
  const localConceptSchemeEntries = entriesPerforated.filter((entry) => entry);

  if (!remoteStoreUtil) return localConceptSchemeEntries;
  // load remote store concept scheme entries
  const remoteEntries = await getRemoteConceptSchemeEntries(
    conceptScemeRURIs,
    entriesPerforated,
    remoteStoreUtil
  );
  if (!remoteEntries) return localConceptSchemeEntries;

  return [...localConceptSchemeEntries, ...remoteEntries];
};

const useGetSourceTerminologyEntries = (conceptEntries, conceptsStatus) => {
  const { remoteStoreUtil } = useRemoteStore();

  const {
    data: localAndRemoteEntries,
    runAsync,
    status,
  } = useAsync({ data: [] });

  useEffect(() => {
    if (conceptsStatus !== RESOLVED) return;
    runAsync(getSourceTerminologyEntries(conceptEntries, remoteStoreUtil));
  }, [runAsync, conceptEntries, remoteStoreUtil, conceptsStatus]);

  return [localAndRemoteEntries, status];
};

export default useGetSourceTerminologyEntries;
