// import { asyncHandler } from 'commons/errors/AsyncHandler';
import registry from 'commons/registry';
import config from 'config';
import { entrystore } from 'commons/store';
import { USERS_ENTRY_ID, CONTEXT_PRINCIPALS } from 'commons/util/userIds';

/**
 * @param {store/Context} catalogContext
 * @return {(boolean|undefined)}
 */
const isCatalogPublic = async (catalogContext) => {
  const contextEntry = await catalogContext.getEntry();
  return contextEntry.isPublic();
};

/**
 * Navigate to the parent Catalog of this dataset
 *
 * @returns {undefined}
 */
const navigateToCatalogView = (
  viewPathKey,
  withDelay = false,
  delayMillis = 2000
) => {
  const site = registry.get('siteManager');
  const state = site.getState();
  const { context } = state[state.view];
  if (withDelay) {
    // asyncHandler.openDialog(true); // TODO
    setTimeout(() => {
      // In order to avoid a slow solr re-index
      // asyncHandler.closeDialog(true); // TODO
      site.render(viewPathKey, { context });
    }, delayMillis);
  } else {
    site.render(viewPathKey, { context });
  }
};

/**
 *
 * @param {*} userEntry
 */
const isUserAllowedCatalogCreation = (userEntry, userInfo) => {
  const catalogCreationAllowGroup = config.get(
    'catalog.catalogCreationAllowedFor'
  );
  if (userInfo.hasAdminRights || catalogCreationAllowGroup === USERS_ENTRY_ID) {
    return true;
  }

  // check if the user parent groups includes config.catalogCreationAllowGroup
  return userEntry
    .getParentGroups()
    .includes(
      entrystore.getEntryURI(CONTEXT_PRINCIPALS, catalogCreationAllowGroup)
    );
};

export { isCatalogPublic, navigateToCatalogView, isUserAllowedCatalogCreation };
