import EditEntryDialog from 'commons/components/entry/EditEntryDialog';
import { withOverviewRefresh } from '../../hooks/useOverviewModel';

const OverviewEditEntryDialog = withOverviewRefresh(
  EditEntryDialog,
  'onEntryEdit'
);

export default OverviewEditEntryDialog;
