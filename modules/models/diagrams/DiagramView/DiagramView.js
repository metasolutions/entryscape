import React, { useState, useRef, useEffect } from 'react';
import { Box, CircularProgress } from '@mui/material';
import { useESContext } from 'commons/hooks/useESContext';
import useAsync, { RESOLVED } from 'commons/hooks/useAsync';
import useErrorHandler from 'commons/errors/hooks/useErrorHandler';
import { useTranslation } from 'commons/hooks/useTranslation';
import esmoDiagramsNLS from 'models/nls/esmoDiagrams.nls';
import esmoCommonsNLS from 'models/nls/esmoCommons.nls';
import escoDialogsNLS from 'commons/nls/escoDialogs.nls';
import Placeholder from 'commons/components/common/Placeholder';
import { withListModelProvider } from 'commons/components/ListView/hooks/useListModel';
import DiagramDialog from '../DiagramDialog';
import DiagramEdit from '../DiagramEdit';
import DiagramControl from '../DiagramControl';
import InfoDialog from './InfoDialog';
import { useDiagramUtils } from '../hooks/useDiagramUtils';
import './DiagramView.scss';
import { DiagramAdapter } from '../utils/diagram';
import {
  useDiagramContext,
  DiagramEntryProvider,
  SET_DIAGRAM_ADAPTER,
} from '../hooks/useDiagramContext';

const DiagramView = () => {
  const translate = useTranslation([
    esmoDiagramsNLS,
    esmoCommonsNLS,
    escoDialogsNLS,
  ]);
  const { context } = useESContext();
  const [diagramEvent, setDiagramEvent] = useState(null);
  const {
    data: diagramAdapter,
    status: diagramStatus,
    runAsync,
    error,
    isLoading,
  } = useAsync();
  const {
    diagramUtils,
    status: chunkLoadingStatus,
    error: chunkLoadingError,
  } = useDiagramUtils();
  const canvasRef = useRef();
  // '', 'max', 'edit', 'max-edit'
  const [diagramDialogState, setDiagramDialogState] = useState('');
  const { diagramEntry, dispatch } = useDiagramContext();
  useErrorHandler(error || chunkLoadingError);

  useEffect(() => {
    if (!diagramEntry || chunkLoadingStatus !== RESOLVED) return;
    const canvas = canvasRef.current;
    const newDiagramAdapter = new DiagramAdapter({
      diagramUtils,
      onClick: setDiagramEvent,
      canvas,
      context,
      diagramEntry,
      translate,
    });
    runAsync(
      newDiagramAdapter.init().then(() => {
        dispatch({
          type: SET_DIAGRAM_ADAPTER,
          diagramAdapter: newDiagramAdapter,
        });
        return newDiagramAdapter;
      })
    );
  }, [
    diagramEntry,
    runAsync,
    diagramUtils,
    chunkLoadingStatus,
    translate,
    context,
    dispatch,
  ]);

  const showPlaceholder =
    diagramStatus === RESOLVED && !diagramAdapter?.hasElements();

  return (
    <>
      <DiagramControl
        diagramUtils={diagramUtils}
        paper={diagramAdapter?.getPaper()}
        edit={diagramDialogState.endsWith('edit')}
        onMaximize={() => setDiagramDialogState('max')}
        onEdit={() => setDiagramDialogState('edit')}
      />
      {!showPlaceholder ? (
        <div className="esmoDiagram">
          <div className="esmoDiagram__canvas" ref={canvasRef} />
          {isLoading ? (
            <Box className="esmoDiagram__progress">
              <CircularProgress />
            </Box>
          ) : null}
        </div>
      ) : (
        <Placeholder label={translate('diagramPlaceholder')} />
      )}
      <InfoDialog
        diagramEvent={diagramEvent}
        closeDialog={() => setDiagramEvent(null)}
      />
      <DiagramDialog
        open={diagramDialogState === 'max'}
        diagramUtils={diagramUtils}
        onEdit={() => setDiagramDialogState('max-edit')}
        onUnMaximize={() => setDiagramDialogState('')}
      />
      <DiagramEdit
        open={diagramDialogState.endsWith('edit')}
        diagramUtils={diagramUtils}
        onDone={() =>
          setDiagramDialogState(diagramDialogState === 'max-edit' ? 'max' : '')
        }
      />
    </>
  );
};

const DiagramViewWithDiagramEntryProvider = (props) => (
  <DiagramEntryProvider>
    <DiagramView {...props} />
  </DiagramEntryProvider>
);

export default withListModelProvider(DiagramViewWithDiagramEntryProvider);
