/* eslint-disable react/jsx-props-no-spreading */
import { useMemo, useState } from 'react';
import PropTypes from 'prop-types';
import {
  ListActionButton,
  ListActions,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  ListHeader,
  ListHeaderItemText,
  ListHeaderItemSort,
  ListSearch,
  ListView,
  ListPagination,
  useListModel,
  useListQuery,
  withListModelProvider,
  ListItemIconButton,
  ListItemAction,
  useFilterListActions,
  listPropsPropType,
} from 'commons/components/ListView';
import { ACTION_MIGRATE_ID } from 'commons/actions/actionIds';
import BuiltinIcon from '@mui/icons-material/Settings';
import CustomIcon from '@mui/icons-material/Widgets';
import MigrateIcon from '@mui/icons-material/CloudUpload';
import InfoIcon from '@mui/icons-material/Info';
import escoListNLS from 'commons/nls/escoList.nls';
import esadEntityTypeNLS from 'admin/nls/esadEntityType.nls';
import esadTypeNLS from 'admin/nls/esadType.nls';
import { useTranslation } from 'commons/hooks/useTranslation';
import useAsync from 'commons/hooks/useAsync';
import {
  ENTITY_TYPE_ID_PREFIX,
  ENTITY_TYPE_BUILTIN,
} from 'admin/configs/utils/namespace';
import config from 'config';
import FieldsLinkedDataBrowserDialog from 'commons/components/FieldsLinkedDataBrowserDialog';
import { fieldPresenters } from '../utils/entityTypePresenters';
import {
  getInstanceAndBuiltinEntityTypes,
  migrateEntityTypesToStore,
} from '../utils/entityTypeQuery';
import { typeConfigToGraph } from '../../utils/exporters';

const nlsBundles = [escoListNLS, esadEntityTypeNLS, esadTypeNLS];

const SEARCH_FIELDS = ['label'];

const MigrateEntityTypesButton = (onMigrate) => {
  const { runAsync } = useAsync();
  const t = useTranslation(nlsBundles);
  if (!config.get('admin.enableTypeStoreMigration')) return null;
  return (
    <ListActionButton
      label={t('migrateToStoreLabel')}
      icon={<MigrateIcon />}
      onClick={() =>
        runAsync(migrateEntityTypesToStore().then(() => onMigrate()))
      }
      isVisible
    />
  );
};

const EntityTypeInstanceList = ({ onMigrate, listProps }) => {
  const [{ search }] = useListModel();
  const t = useTranslation(nlsBundles);
  const { status } = useAsync();
  const [selectedEntityType, setSelectedEntityType] = useState(false);

  const instanceAndBuiltinEntityTypes = useMemo(() => {
    return getInstanceAndBuiltinEntityTypes();
  }, []);

  const { result: entityTypes, size } = useListQuery({
    items: instanceAndBuiltinEntityTypes,
    searchFields: SEARCH_FIELDS,
  });

  const defaultActions = [
    {
      id: ACTION_MIGRATE_ID,
      element: <MigrateEntityTypesButton onMigrate={onMigrate} />,
    },
  ];
  const listActions = useFilterListActions({
    listActions: defaultActions,
    listActionsProps: {},
    excludeActions: listProps.excludeActions,
    nlsBundles,
  });

  return (
    <ListView
      status={status}
      search={search}
      size={size}
      nlsBundles={nlsBundles}
    >
      <ListActions>
        <ListSearch />
        {listActions.map(({ element }) => element)}
      </ListActions>
      <ListHeader>
        <ListHeaderItemText xs={1} text="" />
        <ListHeaderItemSort
          xs={11}
          sortBy="label"
          text={t('labelListHeader')}
        />
      </ListHeader>
      <List>
        {entityTypes.map((entityType) => {
          const { id, label, type } = entityType;
          const isBuiltin = type === ENTITY_TYPE_BUILTIN;
          return (
            <ListItem key={id}>
              <ListItemIcon
                Icon={isBuiltin ? BuiltinIcon : CustomIcon}
                tooltip={
                  isBuiltin ? t('builtinFilterLabel') : t('customFilterLabel')
                }
              />
              <ListItemText xs={10} primary={label} />
              <ListItemAction>
                <ListItemIconButton
                  onClick={() => setSelectedEntityType(entityType)}
                  title={t('showTypeInfoDialogTitle')}
                >
                  <InfoIcon />
                </ListItemIconButton>
              </ListItemAction>
            </ListItem>
          );
        })}
      </List>
      <ListPagination />
      {selectedEntityType ? (
        <FieldsLinkedDataBrowserDialog
          fields={fieldPresenters}
          title={t('typeInfoDialogTitle')}
          closeDialog={() => setSelectedEntityType(null)}
          nlsBundles={nlsBundles}
          {...typeConfigToGraph({
            entry: selectedEntityType.entry,
            typeConfig: selectedEntityType.config,
            idPrefix: ENTITY_TYPE_ID_PREFIX,
            fields: fieldPresenters,
          })}
        />
      ) : null}
    </ListView>
  );
};

EntityTypeInstanceList.propTypes = {
  onMigrate: PropTypes.func,
  listProps: listPropsPropType,
};

export default withListModelProvider(EntityTypeInstanceList, () => ({
  sort: { field: 'label', order: 'asc' },
}));
