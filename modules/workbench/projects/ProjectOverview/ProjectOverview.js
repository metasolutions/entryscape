import React, { useEffect, useCallback } from 'react';
import Overview from 'commons/components/overview/Overview';
import { useESContext } from 'commons/hooks/useESContext';
import { useTranslation } from 'commons/hooks/useTranslation';
import eswoOverviewNLS from 'workbench/nls/eswoOverview.nls';
import escoOverviewNLS from 'commons/nls/escoOverview.nls';
import escoListNLS from 'commons/nls/escoList.nls';
import eswoSpacesNLS from 'workbench/nls/eswoSpaces.nls';
import esteImportNLS from 'terms/nls/esteImport.nls'; // For future fix: we don't have NLS for 'uploadTask' in workbench now
import { useUserState } from 'commons/hooks/useUser';
import { useParams } from 'react-router-dom';
import useNavigate from 'commons/components/router/useNavigate';
import useAsync from 'commons/hooks/useAsync';
import {
  useOverviewModel,
  withOverviewModelProvider,
} from 'commons/components/overview';
import useErrorHandler from 'commons/errors/hooks/useErrorHandler';
import { PROJECT_LISTVIEW } from 'workbench/config/config';
import Loader from 'commons/components/Loader/Loader';
import getWorkbenchOverviewData from './workbenchOverview';
import { sidebarActions } from './actions';

const nlsBundles = [
  escoOverviewNLS,
  eswoOverviewNLS,
  escoListNLS,
  eswoSpacesNLS,
  esteImportNLS,
];

const ProjectOverview = () => {
  const navParams = useParams();
  const { context } = useESContext();
  const { userEntry, userInfo } = useUserState();
  const { data: state, runAsync, error, isLoading } = useAsync({ data: {} });
  const { goBackToListView } = useNavigate();
  const translate = useTranslation(nlsBundles);
  useErrorHandler(error);

  const [{ refreshCount }] = useOverviewModel();

  useEffect(() => {
    runAsync(
      context.getEntry().then(async (entry) => {
        return getWorkbenchOverviewData(entry, context, translate, userInfo);
      })
    );
  }, [runAsync, context, navParams, translate, userInfo, refreshCount]);

  const handleRemove = useCallback(() => {
    goBackToListView(PROJECT_LISTVIEW);
  }, [goBackToListView]);

  if (isLoading) return <Loader />;

  return (
    <div>
      <Overview
        backLabel={translate('backTitle')}
        descriptionItems={state.descriptionItems}
        entry={state.entry}
        nlsBundles={nlsBundles}
        rowActions={state.rowActions}
        sidebarActions={sidebarActions}
        sidebarProps={{ userEntry, userInfo, context, onRemove: handleRemove }}
      />
    </div>
  );
};

export default withOverviewModelProvider(ProjectOverview);
