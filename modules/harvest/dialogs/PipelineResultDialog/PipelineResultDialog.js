import PropTypes from 'prop-types';
import { useState } from 'react';
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  Button,
  AppBar,
} from '@mui/material';
import EqualizerIcon from '@mui/icons-material/Equalizer';
import InfoIcon from '@mui/icons-material/Info';
import BorderAllIcon from '@mui/icons-material/BorderAll';
import LinkIcon from '@mui/icons-material/Link';
import OpenInNewIcon from '@mui/icons-material/OpenInNew';
import Link from '@mui/material/Link';
import escoListNLS from 'commons/nls/escoList.nls';
import esrePipelineResultDialogNLS from 'harvest/nls/esrePipelineResultListDialog.nls';
import { useTranslation } from 'commons/hooks/useTranslation';
import { getLabel } from 'commons/util/rdfUtils';
import Information from 'harvest/Information';
import Status from 'harvest/Status';
import Statistics from 'harvest/Statistics';
import LinkCheckReport from 'harvest/LinkCheckReport';
import { getPathFromViewName, getViewDefFromName } from 'commons/util/site';
import Tooltip from 'commons/components/common/Tooltip';
import { Tab, Tabs, TabPanel } from 'commons/components/common/Tabs';
import { getUniqueId } from 'commons/hooks/useUniqueId';
import './PipelineResultDialog.scss';

const DirectLink = ({ href }) => {
  const t = useTranslation(esrePipelineResultDialogNLS);
  return (
    <div className="esreTabPanel__DirectLink">
      <Tooltip title={t('openSeparateReportWindowTitle')}>
        <Link className="esreDirectlink" href={href} target="_blank">
          {t('openSeparateReportWindow')}
          <OpenInNewIcon classes={{ root: 'esreOpenNewLink__Icon' }} />
        </Link>
      </Tooltip>
    </div>
  );
};

DirectLink.propTypes = {
  href: PropTypes.string.isRequired,
};

const TABS = [
  {
    id: getUniqueId(),
    Icon: EqualizerIcon,
    labelNLSKey: 'status',
    viewName: 'harvest__org__status',
    Component: Status,
  },
  {
    id: getUniqueId(),
    Icon: InfoIcon,
    labelNLSKey: 'information',
    viewName: 'harvest__org__information',
    Component: Information,
  },
  {
    id: getUniqueId(),
    Icon: BorderAllIcon,
    labelNLSKey: 'statistics',
    viewName: 'harvest__org__statistics',
    Component: Statistics,
  },
  {
    id: getUniqueId(),
    Icon: LinkIcon,
    labelNLSKey: 'linkCheckReport',
    viewName: 'harvest__org__linkCheckReport',
    Component: LinkCheckReport,
  },
];

const TabCarousel = ({ contextId }) => {
  const t = useTranslation(esrePipelineResultDialogNLS);
  const tabs = TABS.filter(({ viewName }) => {
    const { navbar } = getViewDefFromName(viewName);
    return navbar === undefined ? true : navbar;
  });
  const [value, setValue] = useState(tabs[0].id);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <div>
      <AppBar
        classes={{ root: 'esreAppBar__Tabs' }}
        position="static"
        color="default"
      >
        <Tabs
          value={value}
          onChange={handleChange}
          className="esreTabs"
          aria-label={t('tabOptions')}
        >
          {tabs.map(({ id, Icon, labelNLSKey }) => (
            <Tab
              label={t(labelNLSKey)}
              icon={<Icon classes={{ root: 'esreTab__Icon' }} />}
              id={id}
              value={id}
              key={id}
            />
          ))}
        </Tabs>
      </AppBar>

      <DialogContent classes={{ root: 'esrePipelineDialog__DialogContent' }}>
        {tabs.map(({ id, viewName, Component }) => (
          <TabPanel value={value} id={id} key={`${id}-panel`}>
            <DirectLink
              href={getPathFromViewName(viewName, {
                contextId,
              })}
            />
            <Component contextId={contextId} />
          </TabPanel>
        ))}
      </DialogContent>
    </div>
  );
};

TabCarousel.propTypes = {
  contextId: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
};

const PipelineResultDialog = ({
  entry,
  contextId,
  closeDialog,
  maxWidth = 'lg',
}) => {
  const t = useTranslation([escoListNLS, esrePipelineResultDialogNLS]);

  const actions = null;
  const title = getLabel(entry);

  return (
    <Dialog
      onClose={closeDialog}
      aria-labelledby={`${entry.getId()}-dialog-title`}
      open
      fullWidth
      maxWidth={maxWidth}
    >
      {title && (
        <div>
          <DialogTitle
            classes={{ root: 'esrePipelineDialog__DialogTitle' }}
            id={`${entry.getId()}-dialog-title`}
          >
            {title}
          </DialogTitle>
          <TabCarousel contextId={contextId} />
        </div>
      )}
      <DialogActions>
        <Button onClick={closeDialog} variant="text">
          {t('pipelineResultRowButton')}
        </Button>
        {actions}
      </DialogActions>
    </Dialog>
  );
};

PipelineResultDialog.propTypes = {
  entry: PropTypes.shape().isRequired,
  closeDialog: PropTypes.func.isRequired,
  maxWidth: PropTypes.string,
  contextId: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
};

export default PipelineResultDialog;
