import { useMemo } from 'react';
import { useUserState } from 'commons/hooks/useUser';
import { getContextSearchQuery } from 'commons/util/solr/context';
import {
  CONTEXT_TYPE_CATALOG,
  canEditContextMetadata,
} from 'commons/util/context';
import { RDF_TYPE_CATALOG } from 'commons/util/entry';
import {
  withListModelProviderAndLocation,
  withListRerender,
  listPropsPropType,
} from 'commons/components/ListView';
import {
  EntryListView,
  useSolrQuery,
  MODIFIED_COLUMN,
  TITLE_COLUMN,
  TOGGLE_CONTEXT_COLUMN,
  ACTIONS_GROUP_COLUMN,
} from 'commons/components/EntryListView';
import {
  LIST_ACTION_INFO,
  LIST_ACTION_EDIT,
} from 'commons/components/EntryListView/actions';
import { getPathFromViewName } from 'commons/util/site';
import config from 'config';
import { useTranslation } from 'commons/hooks/useTranslation';
import { getApiDistributions } from 'catalog/catalogs/utils/catalog';
import { isUserAllowedCatalogCreation } from 'catalog/utils/catalog';
import useGetMainDialog from 'commons/hooks/useGetMainDialog';
import { renderHtmlString } from 'commons/util/reactUtils';
import EditCatalog from 'catalog/catalogs/dialogs/EditCatalog';
import { nlsBundles, listActions } from './actions';

const CatalogsView = ({ listProps }) => {
  const { userEntry, userInfo } = useUserState();

  const queryParams = useMemo(() => {
    const queryTypeParams = {
      rdfType: RDF_TYPE_CATALOG,
      contextType: CONTEXT_TYPE_CATALOG,
    };

    return getContextSearchQuery(queryTypeParams, {
      userEntry,
      userInfo,
    });
  }, [userEntry, userInfo]);

  const queryResults = useSolrQuery(queryParams);

  const catalogStartView = config.get('catalog.startView');

  const translate = useTranslation(nlsBundles);

  const { getConfirmationDialog } = useGetMainDialog();
  const confirmUnpublish = async (catalogEntry) => {
    const apiDistributions = await getApiDistributions(
      catalogEntry.getContext()
    );

    if (!apiDistributions.length) {
      return true;
    }

    return getConfirmationDialog({
      content: renderHtmlString(translate('apiExistsToUnpublishCatalog')),
    });
  };

  return (
    <EntryListView
      {...listProps}
      nlsBundles={nlsBundles}
      listActions={listActions}
      listActionsProps={{
        userEntry,
        userInfo,
        numberOfCatalogs: queryResults.size,
      }}
      getListItemProps={({ entry }) => ({
        to: getPathFromViewName(catalogStartView, {
          contextId: entry.getContext().getId(),
        }),
      })}
      viewPlaceholderProps={{
        label: !isUserAllowedCatalogCreation(userEntry, userInfo)
          ? translate('emptyListWithNoAccessWarning')
          : null,
      }}
      columns={[
        {
          ...TOGGLE_CONTEXT_COLUMN,
          getProps: ({ entry }) => ({
            entry,
            publicTooltip: translate('publicCatalogTitle'),
            privateTooltip: translate('privateCatalogTitle'),
            noAccess: translate('catalogSharingNoAccess'),
          }),
          confirmUnpublish,
        },
        { ...TITLE_COLUMN, xs: 7 },
        MODIFIED_COLUMN,
        {
          ...ACTIONS_GROUP_COLUMN,
          actions: [
            LIST_ACTION_INFO,
            {
              ...LIST_ACTION_EDIT,
              isVisible: ({ entry }) =>
                canEditContextMetadata(entry, userEntry),
              Dialog: withListRerender(EditCatalog, 'onEntryEdit'),
            },
          ],
        },
      ]}
      {...queryResults}
    />
  );
};

CatalogsView.propTypes = {
  listProps: listPropsPropType,
};

export default withListModelProviderAndLocation(CatalogsView);
