import { useCallback } from 'react';
import RemoveEntryDialog from 'commons/components/entry/RemoveEntryDialog';
import { DELETE, useListModel } from 'commons/components/ListView';

const ListRemoveEntryDialog = (props) => {
  const [, dispatch] = useListModel();
  const onRemove = useCallback(() => dispatch({ type: DELETE }), [dispatch]);

  return <RemoveEntryDialog {...props} onRemoveCallback={onRemove} />;
};

export default ListRemoveEntryDialog;
