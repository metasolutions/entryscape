import PropTypes from 'prop-types';
import { Divider, Grid } from '@mui/material';
import {
  OverviewTileButtonGroup,
  OverviewMetadataList,
  OverviewMetadataListItem,
  OverviewSidebar,
  OverviewSidebarActions,
  OverviewHeader,
  OverviewDescription,
  OverviewBackButton,
  OverviewTileButton,
} from 'commons/components/overview';
import {
  nlsBundlesPropType,
  useTranslation,
} from 'commons/hooks/useTranslation';
import { getLabel } from 'commons/util/rdfUtils';
import { getDescription } from 'commons/util/metadata';
import { Entry } from '@entryscape/entrystore-js';
import escoOverviewNLS from 'commons/nls/escoOverview.nls';
import { DRAFT_TAG, tagsPropType } from 'commons/components/tags';
import Loader from 'commons/components/Loader';
import OverviewTags from '../OverviewTags';
import { ACTION_INFO_WITH_ICON } from '../actions/definitions';
import './index.scss';

export const renderTiles = ({ actions, entry, translate, nlsBundles }) =>
  actions
    .map(({ getProps = () => {}, ...actionProps }) => ({
      ...actionProps,
      componentProps: getProps({ entry, translate }),
    }))
    .filter(({ isVisible, ...props }) =>
      isVisible ? isVisible({ entry, ...props }) : true
    )
    .map(
      ({ Component = OverviewTileButton, id, componentProps, ...action }) => (
        <Component
          key={`row-button-${id}`}
          action={{
            ...action,
            entry,
            nlsBundles,
          }}
          {...componentProps}
        />
      )
    );

const Overview = ({
  descriptionItems = [],
  rowActions = [],
  sidebarActions = [],
  sidebarProps = {},
  excludeActions = [],
  headerLabel,
  headerSubLabel,
  descriptionLabel,
  backLabel = '',
  entry,
  nlsBundles = [],
  headerAction = ACTION_INFO_WITH_ICON,
  returnTo,
  tags = [],
  renderPrimaryContent,
  isLoading,
  children,
}) => {
  const translate = useTranslation([...nlsBundles, escoOverviewNLS]);
  const tagsArray = Array.isArray(tags) ? tags : [tags];

  if (isLoading) return <Loader />;

  return (
    <div className="escoTwoColView">
      <div className="escoOverview__main">
        <OverviewBackButton label={backLabel} to={returnTo} entry={entry} />

        <Grid container direction="column">
          <OverviewHeader
            entry={entry}
            label={
              headerLabel ||
              getLabel(entry) ||
              translate('unnamedEntity', entry.getId())
            }
            sublabel={headerSubLabel}
            action={{ entry, nlsBundles, ...headerAction }}
            translate={translate}
          />
          <OverviewTags entry={entry} tags={[...tagsArray, DRAFT_TAG]} />
          {renderPrimaryContent?.() || (
            <OverviewDescription
              label={descriptionLabel || getDescription(entry)}
            />
          )}

          <Divider className="escoOverview__divider" />
          {descriptionItems.length ? (
            <OverviewMetadataList cols={descriptionItems.length}>
              {descriptionItems.map(({ id, labelNlsKey, getValues }) => (
                <OverviewMetadataListItem
                  label={translate(labelNlsKey)}
                  values={getValues({ entry })}
                  key={`md-list-item-${id || labelNlsKey}`}
                />
              ))}
            </OverviewMetadataList>
          ) : null}

          {rowActions.length ? (
            <OverviewTileButtonGroup cols={rowActions.length}>
              {renderTiles({
                actions: rowActions,
                entry,
                translate,
                nlsBundles,
              })}
            </OverviewTileButtonGroup>
          ) : null}
        </Grid>
        {children}
      </div>

      <OverviewSidebar>
        <OverviewSidebarActions
          nlsBundles={nlsBundles}
          entry={entry}
          actions={sidebarActions}
          translate={translate}
          excludeActions={excludeActions}
          {...sidebarProps}
        />
      </OverviewSidebar>
    </div>
  );
};

const actionPropType = PropTypes.shape({
  id: PropTypes.string,
  Dialog: PropTypes.func,
  isVisible: PropTypes.func,
  getProps: PropTypes.func,
  labelNlsKey: PropTypes.string,
  Component: PropTypes.func,
});

const actionsPropType = PropTypes.arrayOf(actionPropType);

const descriptionItemPropType = PropTypes.shape({
  labelNlsKey: PropTypes.string,
  getValues: PropTypes.func,
});

const descriptionItemsPropType = PropTypes.arrayOf(descriptionItemPropType);

Overview.propTypes = {
  descriptionItems: descriptionItemsPropType,
  rowActions: actionsPropType,
  sidebarActions: actionsPropType,
  sidebarProps: PropTypes.shape({}),
  excludeActions: PropTypes.arrayOf(PropTypes.string),
  headerLabel: PropTypes.string,
  headerIcon: PropTypes.node,
  headerSubLabel: PropTypes.string,
  descriptionLabel: PropTypes.string,
  backLabel: PropTypes.string,
  entry: PropTypes.instanceOf(Entry),
  nlsBundles: nlsBundlesPropType,
  headerAction: actionPropType,
  returnTo: PropTypes.string,
  renderPrimaryContent: PropTypes.func,
  children: PropTypes.node,
  tags: tagsPropType,
  isLoading: PropTypes.bool,
};

export default Overview;
