import Convert from 'toolkit/Convert';
import Merge from 'toolkit/Merge';
import Catalog from 'toolkit/Catalog';
import ExploreView from 'toolkit/Explore/ExploreView';
import Validate from 'toolkit/Validate';

export default {
  modules: [
    {
      name: 'toolkit',
      productName: { en: 'Toolkit', sv: 'Verktygslåda', de: 'Toolkit' },
      faClass: 'wrench',
      title: { en: 'Toolkit', sv: 'Verktygslåda', de: 'Toolkit' },
      startView: 'toolkit__rdf__catalog', // compulsory,
      asCrumb: true,
      sidebar: true,
      text: {
        sv: 'En verktygslåda för att jobba med DCAT-AP metadata',
        en: 'A toolkit for working with DCAT-AP metadata',
        de: 'Ein Toolkit für die Arbeit mit DCAT-AP Metadaten',
      },
    },
  ],
  views: [
    {
      name: 'toolkit__rdf__catalog',
      class: Catalog,
      title: {
        en: 'Catalog',
        sv: 'Katalog',
        de: 'Katalog',
      },
      faClass: 'database',
      route: '/toolkit/catalog',
      module: 'toolkit',
      parent: 'toolkit__rdf__catalog',
      public: true,
    },
    {
      name: 'toolkit__validator__report',
      class: Validate,
      title: { en: 'Validate', sv: 'Validera', de: 'Validieren' },
      faClass: 'check-square-o',
      text: {
        sv: 'Validera dina datamängdsbeskrivningar',
        en: 'Validate your dataset descriptions',
        de: 'Validieren Sie Ihre Datensatz-Beschreibungen',
      },
      route: '/toolkit/validate',
      module: 'toolkit',
      parent: 'toolkit__rdf__catalog',
      public: true,
    },
    {
      name: 'toolkit__dcat__merge',
      class: Merge,
      faClass: 'filter',
      title: { en: 'Merge', sv: 'Slå samman', de: 'Kombinieren' },
      route: '/toolkit/merge',
      module: 'toolkit',
      parent: 'toolkit__rdf__catalog',
      public: true,
    },
    {
      name: 'toolkit__dcat__explore',
      class: ExploreView,
      faClass: 'search',
      title: { en: 'Explore', sv: 'Utforska', de: 'Untersuchen' },
      route: '/toolkit/explore',
      module: 'toolkit',
      parent: 'toolkit__rdf__catalog',
      public: true,
    },
    {
      name: 'toolkit__dcat__convert',
      class: Convert,
      faClass: 'random',
      title: { en: 'Convert', sv: 'Konver\u00ADtera', de: 'Konvertieren' },
      route: '/toolkit/convert',
      module: 'toolkit',
      parent: 'toolkit__rdf__catalog',
      public: true,
    },
  ],
};
