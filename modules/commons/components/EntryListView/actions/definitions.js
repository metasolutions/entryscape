import {
  ACTION_EDIT as BASE_ACTION_EDIT,
  ACTION_INFO as BASE_ACTION_INFO,
  ACTION_REMOVE as BASE_ACTION_REMOVE,
  ACTION_REVISIONS as BASE_ACTION_REVISIONS,
  ACTION_SHARING_SETTINGS as BASE_ACTION_SHARING_SETTINGS,
  ACTION_DOWNLOAD as BASE_ACTION_DOWNLOAD,
} from 'commons/actions';
import LinkedDataBrowserDialog from 'commons/components/LinkedDataBrowserDialog';
import ListEditEntryDialog from '../dialogs/ListEditEntryDialog';
import ListRemoveEntryDialog from '../dialogs/ListRemoveEntryDialog';
import ListRevisionsEntryDialog from '../dialogs/ListRevisionsEntryDialog';
import ListSharingSettingsDialog from '../dialogs/ListSharingSettingsDialog';
import ListDownloadEntryDialog from '../dialogs/ListDownloadEntryDialog';

export const LIST_ACTION_EDIT = {
  ...BASE_ACTION_EDIT,
  Dialog: ListEditEntryDialog,
  highlightId: 'listActionEdit',
};

export const LIST_ACTION_INFO = {
  ...BASE_ACTION_INFO,
  Dialog: LinkedDataBrowserDialog,
  highlightId: 'listActionInfo',
};

export const LIST_ACTION_REMOVE = {
  ...BASE_ACTION_REMOVE,
  Dialog: ListRemoveEntryDialog,
  highlightId: 'listActionRemove',
};

export const LIST_ACTION_REVISIONS = {
  ...BASE_ACTION_REVISIONS,
  Dialog: ListRevisionsEntryDialog,
};

export const LIST_ACTION_SHARING_SETTINGS = {
  ...BASE_ACTION_SHARING_SETTINGS,
  Dialog: ListSharingSettingsDialog,
  highlightId: 'listActionSharing',
};

export const LIST_ACTION_DOWNLOAD = {
  ...BASE_ACTION_DOWNLOAD,
  Dialog: ListDownloadEntryDialog,
};
