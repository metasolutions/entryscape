import PropTypes from 'prop-types';
import './Language.scss';

export const Language = ({ language }) => {
  return language ? (
    <span className="escoPresenterLanguage">
      <span className="escoPresenterLanguage__text">{language}</span>
    </span>
  ) : null;
};

Language.propTypes = {
  language: PropTypes.string,
};
