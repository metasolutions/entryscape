import PropTypes from 'prop-types';
import { useEffect, useState } from 'react';
import Overview from 'harvest/Statistics/tables/Overview';
import DatasetMetadata from 'harvest/Statistics/tables/DatasetMetadata';
import DistributionMetadata from 'harvest/Statistics/tables/DistributionMetadata';
import DistributionPerDataset from 'harvest/Statistics/tables/DistributionPerDataset';
import Licenses from 'harvest/Statistics/tables/Licenses';
import Formats from 'harvest/Statistics/tables/Formats';
import {
  getDatasetCount,
  getLoadStatistics,
  getPipelineEntryAndResource,
} from 'harvest/util/data';
import { getLabel } from 'commons/util/rdfUtils';
import escoStatisticsReportNLS from 'commons/nls/escoStatisticsReport.nls';
import esrePipelineResultListDialogNLS from 'harvest/nls/esrePipelineResultListDialog.nls';
import { useTranslation } from 'commons/hooks/useTranslation';
import CircularProgress from '@mui/material/CircularProgress';
import Placeholder from 'commons/components/common/Placeholder';
import usePageTitle from 'commons/hooks/usePageTitle';

const TOO_MANY_DATASETS_TO_LOAD = 2000;

const Statistics = ({ contextId }) => {
  const [statistics, setStatistics] = useState({});
  const [loading, setLoading] = useState(true);
  const [showWarning, setShowWarning] = useState(false);
  const [showNoDataMessage, setShowNoDataMessage] = useState(false);
  const [pageTitle, setPageTitle] = usePageTitle();

  const fetchData = async (continueCondition) => {
    const getDataReport = getLoadStatistics(continueCondition);
    try {
      const data = await getDataReport(contextId);
      setStatistics(data);
      setLoading(false);
      setShowWarning(false);
    } catch (error) {
      console.error(error.message);
    }
  };

  const getDataCount = async () => {
    const dataCount = await getDatasetCount(contextId);
    if (dataCount >= TOO_MANY_DATASETS_TO_LOAD) {
      setShowWarning(true);
      setLoading(true);
    }
  };

  useEffect(() => {
    const dataFetched = Object.keys(statistics);
    if (dataFetched.length) {
      const { totalDatasetCount, totalDistributionCount } = statistics;
      if (!totalDatasetCount && !totalDistributionCount) {
        setShowNoDataMessage(true);
      }
    }
  });

  useEffect(() => {
    let componentMounted = true;
    const continueCondition = () => componentMounted;
    getDataCount();
    fetchData(continueCondition);
    return () => {
      componentMounted = false;
      setShowWarning(false);
      setLoading(false);
      setShowNoDataMessage(false);
    };
  }, []);

  // Needed to fetch the page title
  useEffect(() => {
    const fetchPipelineData = async () => {
      const { pipelineEntry } = await getPipelineEntryAndResource(contextId);
      const pipelineLabel = getLabel(pipelineEntry);
      if (!pageTitle && pipelineLabel) {
        setPageTitle(pipelineLabel);
      }
    };

    fetchPipelineData();
  }, []);

  const t = useTranslation([
    esrePipelineResultListDialogNLS,
    escoStatisticsReportNLS,
  ]);

  const { totalDatasetCount, nrOfDistributionsPerDataset, licenses, formats } =
    statistics;

  return (
    <div className="esreStatistics--padding-bottom">
      {showWarning && <Placeholder label={t('loadTimeWarning')} animated />}
      {loading && (
        <div className="esreStatisics__Progress">
          <CircularProgress />
        </div>
      )}
      {totalDatasetCount > 0 && (
        <>
          <Overview stats={statistics} nlsBundle={escoStatisticsReportNLS} />
          <DatasetMetadata
            stats={statistics}
            nlsBundle={escoStatisticsReportNLS}
          />
          <DistributionMetadata
            stats={statistics}
            nlsBundle={escoStatisticsReportNLS}
          />
          <DistributionPerDataset
            datasetCount={totalDatasetCount}
            dataPerDataset={nrOfDistributionsPerDataset}
            nlsBundle={escoStatisticsReportNLS}
          />
          <Licenses
            licensesData={licenses}
            nlsBundle={escoStatisticsReportNLS}
          />
          <Formats formatData={formats} nlsBundle={escoStatisticsReportNLS} />
        </>
      )}
      {showNoDataMessage && (
        <Placeholder label={t('noStatsPlaceholder')} animated />
      )}
    </div>
  );
};

Statistics.propTypes = {
  contextId: PropTypes.string,
};

export default Statistics;
