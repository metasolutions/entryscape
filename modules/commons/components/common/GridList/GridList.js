import PropTypes from 'prop-types';
import { ImageList } from '@mui/material';

const GridList = ({ children, ...imageListProps }) => {
  return <ImageList {...imageListProps}>{children}</ImageList>;
};

GridList.propTypes = {
  children: PropTypes.node,
};

export default GridList;
