import { useEffect } from 'react';
import { useMapContext as useOpenlayersMapContext } from './useMapContext';
import { useOpenlayers } from './useOpenlayers';

const useBuiltinControl = (create, target) => {
  const [{ map }, dispatch] = useOpenlayersMapContext();
  const { status } = useOpenlayers();

  useEffect(() => {
    let control;
    if (map && status === 'resolved' && target) {
      control = create(target);
      dispatch({
        type: 'ADD_CONTROL',
        control,
      });
    }

    return () => {
      if (map && control) {
        dispatch({
          type: 'REMOVE_CONTROL',
          control,
        });
      }
    };
  }, [map, target, dispatch, create, status]);
};

export default useBuiltinControl;
