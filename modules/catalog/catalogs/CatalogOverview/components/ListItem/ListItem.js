import { ListItem as MuiListItem, ListItemText } from '@mui/material';
import './ListItem.scss';

const ListItem = (props) => {
  return (
    <MuiListItem disableGutters classes={{ root: 'escaOverviewListItem' }}>
      <ListItemText
        classes={{
          root: 'escaOverviewListItemText__root',
          primary: 'escaOverviewListItemText__primary',
          secondary: 'escaOverviewListItemText__secondary',
        }}
        {...props}
      />
    </MuiListItem>
  );
};

export default ListItem;
