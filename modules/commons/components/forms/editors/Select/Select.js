import PropTypes from 'prop-types';
import {
  FormControl,
  FormHelperText,
  Grid,
  InputLabel,
  MenuItem,
  Select as MuiSelect,
} from '@mui/material';
import { entryPropType } from 'commons/util/entry';

export const Select = ({
  xs = 8,
  disabled,
  error,
  helperText,
  label,
  labelId,
  choices,
  value,
  size,
  onChange,
}) => {
  return (
    <Grid item xs={xs}>
      <FormControl variant="filled" error={error} fullWidth>
        {label ? <InputLabel id={labelId}>{label}</InputLabel> : null}
        <MuiSelect
          disabled={disabled}
          inputProps={labelId ? { 'aria-labelledby': labelId } : null}
          value={value}
          onChange={onChange}
          size={size}
        >
          {choices.map((choice) =>
            choice === null ? (
              <MenuItem key="_none" value="_none" disabled>
                ─────
              </MenuItem>
            ) : (
              <MenuItem key={choice.id || choice.value} value={choice.value}>
                {choice.label || '\u00A0'}
              </MenuItem>
            )
          )}
        </MuiSelect>
        {helperText ? <FormHelperText>{helperText}</FormHelperText> : null}
      </FormControl>
    </Grid>
  );
};

Select.propTypes = {
  choices: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string,
      value: PropTypes.oneOfType([
        PropTypes.number,
        PropTypes.string,
        entryPropType,
      ]),
      label: PropTypes.string,
    })
  ),
  label: PropTypes.string,
  labelId: PropTypes.string,
  onChange: PropTypes.func,
  value: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.string,
    entryPropType,
  ]),
  xs: PropTypes.number,
  size: PropTypes.string,
  disabled: PropTypes.bool,
  error: PropTypes.bool,
  helperText: PropTypes.string,
};

export default Select;
