/* eslint-disable react/jsx-props-no-spreading */
import { useEffect } from 'react';
import PropTypes from 'prop-types';
import { Grid } from '@mui/material';
import {
  OverviewSection,
  useOverviewModel,
  OverviewDescription,
  overviewPropsPropType,
} from 'commons/components/overview';
import Overview from 'commons/components/overview/Overview';
import {
  ACTION_DIVIDER,
  ACTION_INFO_WITH_ICON,
  ACTION_PUBLISH,
} from 'commons/components/overview/actions';
import { withActionsProvider } from 'commons/components/ListView';
import Lookup from 'commons/types/Lookup';
import useAsync from 'commons/hooks/useAsync';
import { useEntry } from 'commons/hooks/useEntry';
import { getDescription } from 'commons/util/rdfUtils';
import { getShortModifiedDate } from 'commons/util/metadata';
import {
  useTranslation,
  nlsBundlesPropType,
} from 'commons/hooks/useTranslation';
import { useSnackbar } from 'commons/hooks/useSnackbar';
import useGetContributors from 'commons/hooks/useGetContributors';
import { COMPOSITION, AGGREGATION } from 'entitytype-lookup';
import registry from 'commons/registry';
import {
  getViewDefFromName,
  getParentPathFromViewName,
} from 'commons/util/site';
import ImagePresenter from 'commons/components/ImagePresenter';
import CompositionsList from './CompositionsList';
import AggregationList from './AggregationsList';
import { sidebarActions } from './actions';
import './EntityOverview.scss';
import LinkedDataBrowserDialog from '../LinkedDataBrowserDialog';

const EntityOverview = ({
  nlsBundles,
  actions = sidebarActions,
  getAggregationListProps,
  metadataItems = [],
  tileActions = [],
  overviewProps,
}) => {
  const translate = useTranslation(nlsBundles);
  const entry = useEntry();
  const [{ refreshCount }] = useOverviewModel();
  const { contributors } = useGetContributors(entry, refreshCount);
  const { data: entityType, runAsync } = useAsync(null);
  const [addSnackbar] = useSnackbar();

  useEffect(() => {
    if (entityType) return;
    runAsync(Lookup.inUse(entry));
  }, [entry, entityType, runAsync]);

  const defaultMetadataItems = [
    {
      id: 'last-update',
      labelNlsKey: 'lastUpdateLabel',
      getValues: () => (entry ? [getShortModifiedDate(entry)] : []),
    },
    {
      id: 'edited-by',
      labelNlsKey: 'editedByLabel',
      getValues: () => contributors,
    },
  ];
  const metadataListItems = metadataItems.length
    ? metadataItems
    : defaultMetadataItems;

  const backButtonPath = getParentPathFromViewName(registry.getCurrentView(), {
    contextId: entry.getContext().getId(),
    entityName: entityType?.get('name') || '',
  });

  const completeSidebarActions = [
    ...actions,
    ...(entityType?.get('publishControl')
      ? [ACTION_DIVIDER, ACTION_PUBLISH]
      : []),
  ];

  return (
    <Overview
      {...overviewProps}
      backLabel={translate('backButtonTooltip')}
      headerAction={{
        ...ACTION_INFO_WITH_ICON,
        entry,
        entityType,
        nlsBundles,
        Dialog: LinkedDataBrowserDialog,
      }}
      entry={entry}
      nlsBundles={nlsBundles}
      returnTo={backButtonPath}
      renderPrimaryContent={() => (
        <>
          <OverviewDescription label={getDescription(entry)} />
          <Grid item>
            {entityType?.get('contentviewers') ? (
              <ImagePresenter
                refreshKey={`${refreshCount}`}
                entityType={entityType}
                entry={entry}
              />
            ) : null}
          </Grid>
        </>
      )}
      descriptionItems={metadataListItems}
      rowActions={tileActions.length ? tileActions : []}
      sidebarActions={completeSidebarActions}
      sidebarProps={{
        entityType,
        translate,
        addSnackbar,
        parentViewName: getViewDefFromName(registry.getCurrentView()).parent,
      }}
    >
      {entityType?.get('relations')?.length > 0 ? (
        <Grid item container direction="column">
          {entityType.get('relations').map((relation) => {
            const relationEntityType = Lookup.getByName(relation.entityType);
            if (!relationEntityType) return;

            return (
              <Grid item key={`${relation.entityType} - ${relation.property}`}>
                {relation.type === COMPOSITION ? (
                  <OverviewSection>
                    <CompositionsList
                      relation={relation}
                      entityType={relationEntityType}
                      entry={entry}
                      nlsBundles={nlsBundles}
                    />
                  </OverviewSection>
                ) : null}
                {relation.type === AGGREGATION ? (
                  <OverviewSection>
                    <AggregationList
                      relation={relation}
                      entityType={relationEntityType}
                      entry={entry}
                      nlsBundles={nlsBundles}
                      {...(getAggregationListProps
                        ? getAggregationListProps({
                            entityType: relationEntityType,
                          })
                        : {})}
                    />
                  </OverviewSection>
                ) : null}
              </Grid>
            );
          })}
        </Grid>
      ) : null}
    </Overview>
  );
};

EntityOverview.propTypes = {
  actions: PropTypes.arrayOf(PropTypes.shape({})),
  nlsBundles: nlsBundlesPropType,
  getAggregationListProps: PropTypes.func,
  metadataItems: PropTypes.arrayOf(PropTypes.shape({})),
  tileActions: PropTypes.arrayOf(PropTypes.shape({})),
  overviewProps: overviewPropsPropType,
};

export default withActionsProvider(EntityOverview);
