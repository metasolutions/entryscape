import { useState } from 'react';
import { useLocation } from 'react-router-dom';
import { Button } from '@mui/material';
import { WarningAmber as WarningAmberIcon } from '@mui/icons-material';
import MaintenanceDialog from 'commons/errors/MaintenanceDialog';
import config from 'config';
import escoErrorsNLS from 'commons/nls/escoErrors.nls';
import { useTranslation } from 'commons/hooks/useTranslation';

const PlannedMaintenance = () => {
  const t = useTranslation(escoErrorsNLS);
  const plannedMaintenance = config.get('entryscape.maintenance.planned');
  const location = useLocation();
  const fromSignin = location.state?.from === config.get('site.signinView');
  const [dialogOpen, setDialogOpen] = useState(fromSignin);

  if (!plannedMaintenance) return null;

  return (
    <>
      <Button
        variant="contained"
        color="warning"
        startIcon={<WarningAmberIcon />}
        style={{ marginRight: '24px' }}
        onClick={() => setDialogOpen(true)}
      >
        {t('maintenanceButton')}
      </Button>
      {dialogOpen ? (
        <MaintenanceDialog onClose={() => setDialogOpen(false)} />
      ) : null}
    </>
  );
};

export default PlannedMaintenance;
