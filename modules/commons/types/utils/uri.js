import { entrystore } from 'commons/store';

/**
 *
 * @param {string} name
 * @param {string} baseName
 * @returns {string}
 */
export const nameToURI = (name, baseName) => {
  if (!name) {
    throw new Error('Name is required');
  }
  const base = entrystore.getResourceURI(baseName, '');
  const id = name.includes(base) ? name : `${base}${name}`;
  return id;
};

/**
 *
 * @param {Object} entityTypeConfig;
 * @returns {Object}
 */
export const normalizeEntityTypeUris = (entityTypeConfig) => {
  const baseName = 'entitytypes';
  const { id, name, refines, useWith } = entityTypeConfig;
  const normalizedEntityType = { ...entityTypeConfig };

  if (!id) {
    normalizedEntityType.id = nameToURI(name, baseName);
  }
  if (refines) {
    normalizedEntityType.refines = nameToURI(refines, baseName);
  }
  if (useWith) {
    normalizedEntityType.useWith = nameToURI(useWith, baseName);
  }
  return normalizedEntityType;
};

export const normalizeUriPattern = (uriPattern) => {
  if (!uriPattern) return uriPattern;

  const baseUrl = entrystore.getBaseURI();
  const urlObject = new URL(baseUrl);

  const domain = `${urlObject.protocol}//${urlObject.host}`;

  // eslint-disable-next-line no-template-curly-in-string
  return uriPattern.replace('${baseURI}', baseUrl).replace('${domain}', domain);
};
