import { Graph } from '@entryscape/rdfjson';
import {
  RDF_PROPERTY_DEPENDS_ON,
  RDF_PROPERTY_DEPENDS_ON_VALUE,
} from 'models/utils/ns';
import { entrystore } from 'commons/store';

/**
 *
 * @param {Graph} graph
 * @param {string} resourceURI
 * @returns {Promise<[Entry,string]>}
 */
export const getDeps = async (graph, resourceURI) => {
  const entryURI = graph.findFirstValue(resourceURI, RDF_PROPERTY_DEPENDS_ON);
  const value = graph.findFirstValue(
    resourceURI,
    RDF_PROPERTY_DEPENDS_ON_VALUE
  );
  if (!entryURI || !value) return;

  try {
    const entry = await entrystore.getEntry(entryURI);
    return [entry, value];
  } catch (error) {
    console.error(`Could not find referred deps entry with id: ${entryURI}`);
  }
};
