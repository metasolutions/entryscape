import { useEffect } from 'react';
import config from 'config';

const useDocumentTitle = (viewName) => {
  const appName = config.get('theme.appName') || 'EntryScape';
  const documentTitle = viewName ? `${viewName} - ${appName}` : appName;

  useEffect(() => {
    document.title = documentTitle;
  }, [documentTitle]);

  return documentTitle;
};

export default useDocumentTitle;
