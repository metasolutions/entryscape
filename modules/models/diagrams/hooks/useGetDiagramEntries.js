import { useEffect, useRef } from 'react';
import useAsync from 'commons/hooks/useAsync';
import { entrystore } from 'commons/store';
import { useListModel } from 'commons/components/ListView/hooks/useListModel';
import sleep from 'commons/util/sleep';

const useGetDiagramEntries = (context) => {
  const { data: diagramEntries, status, runAsync } = useAsync({ data: [] });
  const [{ refreshCount }] = useListModel();
  const initialFetch = useRef(true);

  useEffect(() => {
    const getDiagramEntries = async () => {
      if (!initialFetch.current) {
        await sleep(1500);
      } else {
        initialFetch.current = false;
      }
      return entrystore
        .newSolrQuery()
        .context(context)
        .rdfType('esterms:Diagram')
        .getEntries();
    };
    runAsync(getDiagramEntries());
  }, [context, runAsync, refreshCount]);

  return {
    status,
    diagramEntries,
  };
};

export default useGetDiagramEntries;
