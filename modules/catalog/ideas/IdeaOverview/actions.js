import {
  ACTION_EDIT,
  ACTION_REVISIONS,
  ACTION_REMOVE,
} from 'commons/components/overview/actions';

const sidebarActions = [ACTION_EDIT, ACTION_REVISIONS, ACTION_REMOVE];

export { sidebarActions };
