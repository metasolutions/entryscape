import PropTypes from 'prop-types';
import { useState, useEffect } from 'react';
import {
  Button,
  Dialog,
  DialogContent,
  Link as MuiLink,
  DialogActions,
  Typography,
} from '@mui/material';
import { useTranslation } from 'commons/hooks/useTranslation';
import escoLayoutNLS from 'commons/nls/escoLayout.nls';
import userUtil from 'commons/util/userUtil';
import { USER_ENTRY_ID_GUEST } from 'commons/util/userIds';
import {
  getPrivacyPolicyParams,
  getTermsAndConditionsParams,
} from 'commons/util/config';

function PrivacyDialog({ userEntry }) {
  const t = useTranslation(escoLayoutNLS);

  const [privacyDialogOpen, setPrivacyDialogOpen] = useState(false);
  const [privacyAccepted, setPrivacyAccepted] = useState(false);
  const [termsAccepted, setTermsAccepted] = useState(false);
  const isGuest = userEntry.getId() === USER_ENTRY_ID_GUEST;

  const {
    url: privacyPolicyURI,
    version: newestPrivacyVersion,
    requireApproval: requireApprovalPrivacyConfig,
  } = getPrivacyPolicyParams();
  const requirePrivacyApproval = Boolean(
    requireApprovalPrivacyConfig && privacyPolicyURI && newestPrivacyVersion
  );

  const {
    url: termsConditionsURI,
    version: newestTermsVersion,
    requireApproval: requireApprovalTermsConfig,
  } = getTermsAndConditionsParams();
  const requireTermsApproval = Boolean(
    requireApprovalTermsConfig && termsConditionsURI && newestTermsVersion
  );

  useEffect(() => {
    const [hasAcceptedPrivacy, hasAcceptedTerms] =
      userUtil.privacyPolicyAndTermsAccepted(
        userEntry,
        newestPrivacyVersion,
        newestTermsVersion
      );
    setPrivacyAccepted(hasAcceptedPrivacy);
    setTermsAccepted(hasAcceptedTerms);

    if (
      !isGuest &&
      ((requirePrivacyApproval && !hasAcceptedPrivacy) ||
        (requireTermsApproval && !hasAcceptedTerms))
    ) {
      setPrivacyDialogOpen(true);
    }
  }, [
    isGuest,
    newestPrivacyVersion,
    newestTermsVersion,
    requirePrivacyApproval,
    requireTermsApproval,
    userEntry,
  ]);

  const closeDialog = () => setPrivacyDialogOpen(false);

  const getButtonLabel = () => {
    let label = t('privacyPolicyAndTermsAgree');
    if (!privacyAccepted && termsAccepted) {
      label = t('privacyPolicyAgree');
    } else if (privacyAccepted && !termsAccepted) {
      label = t('termsConditionsAgree');
    }
    return label;
  };

  const updatePrivacyAndTermsStatus = () => {
    if (
      !privacyAccepted &&
      requirePrivacyApproval &&
      !termsAccepted &&
      requireTermsApproval
    ) {
      userUtil.setTermsAndPrivacy(
        userEntry,
        newestTermsVersion,
        newestPrivacyVersion
      );
    } else if (!privacyAccepted && requirePrivacyApproval) {
      userUtil.setPrivacyPolicyVersion(userEntry, newestPrivacyVersion);
    } else if (!termsAccepted && requireTermsApproval) {
      userUtil.setTermsVersion(userEntry, newestTermsVersion);
    }
  };

  const privacyPrompt = userUtil.hasAcceptedSomePrivacy(userEntry)
    ? t('privacyPolicyUpdateInner')
    : t('privacyPolicyInitialInner');

  const termsPrompt = userUtil.hasAcceptedSomeTerms(userEntry)
    ? t('termsConditionsUpdateInner')
    : t('termsConditionsInitialInner');

  return (
    <Dialog
      open={privacyDialogOpen}
      fullWidth
      maxWidth="sm"
      aria-label="privacy-dialog"
      data-testid="PrivacyDialog-id"
    >
      <DialogContent dividers>
        {!privacyAccepted && requirePrivacyApproval ? (
          <>
            <Typography variant="h6">{t('privacyPolicyHeader')}</Typography>
            <Typography
              variant="body1"
              gutterBottom
              style={{ marginBottom: '18px' }}
              data-testid="PrivacyDialog-privacy-prompt-id"
            >
              {`${privacyPrompt} `}
              <MuiLink href={privacyPolicyURI} target="_blank">
                {t('linkText')}
              </MuiLink>
              .
            </Typography>
          </>
        ) : null}

        {!termsAccepted && requireTermsApproval ? (
          <>
            <Typography variant="h6">{t('termsConditionsHeader')}</Typography>
            <Typography
              variant="body1"
              gutterBottom
              style={{ marginBottom: '18px' }}
              data-testid="PrivacyDialog-terms-prompt-id"
            >
              {`${termsPrompt} `}
              <MuiLink href={termsConditionsURI} target="_blank">
                {t('linkText')}
              </MuiLink>
              .
            </Typography>
          </>
        ) : null}

        {!privacyAccepted || !termsAccepted ? (
          <>
            <Typography
              variant="body1"
              gutterBottom
              data-testid="PrivacyDialog-privacy-terms-prompt-id"
            >
              {t('privacyAndTermsPrompt')}
            </Typography>
          </>
        ) : null}
      </DialogContent>
      <DialogActions>
        <Button
          onClick={() => {
            updatePrivacyAndTermsStatus();
            closeDialog();
          }}
          variant="text"
          data-testid="PrivacyDialog-button-id"
        >
          {getButtonLabel()}
        </Button>
      </DialogActions>
    </Dialog>
  );
}

PrivacyDialog.propTypes = {
  userEntry: PropTypes.shape().isRequired,
};

export default PrivacyDialog;
