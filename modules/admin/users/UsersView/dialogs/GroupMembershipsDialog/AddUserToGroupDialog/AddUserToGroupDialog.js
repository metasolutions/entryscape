import PropTypes from 'prop-types';
import { useCallback, useState } from 'react';
import ContentWrapper from 'commons/components/common/ContentWrapper';
import { types, Entry } from '@entryscape/entrystore-js';
import { getSearchQuery } from 'commons/util/solr/entry';
import ListActionDialog from 'commons/components/common/dialogs/ListActionDialog';
import {
  EntryListView,
  useSolrQuery,
  TITLE_COLUMN,
  MODIFIED_COLUMN,
} from 'commons/components/EntryListView';
import {
  withListModelProvider,
  ListItemButton,
} from 'commons/components/ListView';
import { getRenderName } from 'commons/util/rdfUtils';
import { useTranslation } from 'commons/hooks/useTranslation';
import { applyUsersGroupParams } from 'admin/utils';
import useErrorHandler from 'commons/errors/hooks/useErrorHandler';
import { nlsBundles } from './actions';

const entryType = types.ET_LOCAL;
const graphType = types.GT_GROUP;
const resourceType = 'InformationResource';

const GroupList = withListModelProvider(
  ({ userEntry, closeDialog, updateList, userGroupIds }) => {
    const [selectError, setSelectError] = useState();
    useErrorHandler(selectError);
    const createQuery = useCallback(() => {
      const [query] = getSearchQuery({
        entryType,
        graphType,
        resourceType,
      });
      return query;
    }, []);

    const queryResults = useSolrQuery({
      createQuery,
      applyQueryParams: applyUsersGroupParams,
    });

    const translate = useTranslation(nlsBundles);

    const handleSelectUser = async (groupEntry) =>
      groupEntry
        .getResource(true)
        .addEntry(userEntry)
        .then(() => {
          userEntry.setRefreshNeeded();
          return userEntry.refresh();
        })
        .then(() => {
          updateList();
          closeDialog();
        })
        .catch((error) => setSelectError(error));

    const getSelectEntryProps = ({ entry: groupEntry }) => {
      const disabled = Boolean(
        userGroupIds.find((userGroupId) => userGroupId === groupEntry.getId())
      );
      return {
        onClick: () => handleSelectUser(groupEntry),
        disabled,
        label: disabled
          ? translate('selectedEntityLabel')
          : translate('selectEntity'),
      };
    };

    return (
      <EntryListView
        // eslint-disable-next-line react/jsx-props-no-spreading
        {...queryResults}
        nlsBundles={nlsBundles}
        columns={[
          {
            ...TITLE_COLUMN,
            xs: 8,
            getProps: ({ entry: groupEntry }) => ({
              primary:
                getRenderName(groupEntry) ||
                translate('unnamedWorkspace', groupEntry.getId()),
            }),
          },
          { ...MODIFIED_COLUMN, xs: 3 },
          {
            id: 'select-entry',
            xs: 1,
            Component: ListItemButton,
            getProps: getSelectEntryProps,
          },
        ]}
      />
    );
  }
);

GroupList.propTypes = {
  userEntry: PropTypes.instanceOf(Entry),
  closeDialog: PropTypes.func,
  updateList: PropTypes.func,
  userGroupIds: PropTypes.arrayOf(PropTypes.string),
};

const AddUserToGroupDialog = ({
  closeDialog,
  userEntry,
  updateList,
  userGroupIds,
}) => {
  const translate = useTranslation(nlsBundles);

  return (
    <ListActionDialog
      id="select-group-dialog"
      closeDialog={closeDialog}
      title={translate('selectGroup')}
      fixedHeight
    >
      <ContentWrapper>
        <GroupList
          userEntry={userEntry}
          closeDialog={closeDialog}
          updateList={updateList}
          userGroupIds={userGroupIds}
        />
      </ContentWrapper>
    </ListActionDialog>
  );
};

AddUserToGroupDialog.propTypes = {
  closeDialog: PropTypes.func,
  userEntry: PropTypes.instanceOf(Entry),
  updateList: PropTypes.func,
  userGroupIds: PropTypes.arrayOf(PropTypes.string),
};

export default AddUserToGroupDialog;
