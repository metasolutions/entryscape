import { saveAs } from 'file-saver/FileSaver';
import { i18n } from 'esi18n';
import escoProgressTask from '../nls/escoFile.nls';
import configUtil from './config';
import { convertBytesToMBytes } from './util';

/**
 *
 * @param {File} file
 * @param {string} encoding
 * @returns {Promise}
 */
export const readFileAsText = (file, encoding = 'UTF-8') => {
  const fileSizeLimit = configUtil.uploadFileSizeLimit();
  if (file.size > fileSizeLimit) {
    return Promise.reject(
      i18n.localize(escoProgressTask, 'fileUploadSizeLimit', {
        limit: convertBytesToMBytes(fileSizeLimit),
      })
    );
  }
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.onloadend = () => resolve(reader.result);
    reader.onerror = () => reject(reader.error);
    // reader.onabort = () => callback('abort'); // TODO not supported on IE11?
    reader.readAsText(file, encoding);
  });
};

/**
 *
 * @param {string} base64StringData
 * @param {string} type
 * @returns {Blob}
 */
export const createBlob = (base64StringData, type = 'image/png') => {
  const binaryData = atob(base64StringData);

  // Create a Uint8Array from the binary data
  const uint8Array = new Uint8Array(binaryData.length);
  for (let i = 0; i < binaryData.length; i++) {
    uint8Array[i] = binaryData.charCodeAt(i);
  }

  const blob = new Blob([uint8Array], { type });

  return blob;
};

/**
 * Download file based on given URI
 *
 * @param {string} uri
 * @param {string} filename
 * @returns {Promise}
 */
export const downloadURI = (uri, filename) =>
  fetch(uri)
    .then((response) => response.blob())
    .then((blob) => saveAs(blob, filename));

const formatConversionMap = {
  'application/rdf+xml': 'rdf',
  'application/n-triples': 'nt',
  'text/turtle': 'ttl',
  'application/ld+json': 'jsonld',
  'application/json': 'json',
};

/**
 * Converts format into file extension
 *
 * @param {string} format
 * @returns {string}
 */
export const convertFileFormat = (format) =>
  formatConversionMap?.[format] || 'txt';
