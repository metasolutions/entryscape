import React, { useEffect, useState, useMemo, useCallback } from 'react';
import PropTypes from 'prop-types';
import { graphPropType } from 'commons/util/entry';
import { validate } from '@entryscape/rdforms';
import {
  Button,
  CircularProgress,
  InputBase,
  InputAdornment,
  IconButton,
  Menu,
  MenuItem,
  TablePagination,
  Typography,
} from '@mui/material';
import {
  Search as SearchIcon,
  Clear as ClearIcon,
  MoreHoriz as EllipsisIcon,
  ListAlt as ListAltIcon,
  Settings as ActionsIcon,
} from '@mui/icons-material';
import {
  DataGrid,
  GridToolbarContainer,
  GridToolbarColumnsButton,
  GridToolbarDensitySelector,
} from '@mui/x-data-grid';
import config from 'config';
import { useESContext } from 'commons/hooks/useESContext';
import {
  useTranslation,
  nlsBundlesPropType,
} from 'commons/hooks/useTranslation';
import useAsync from 'commons/hooks/useAsync';
import escoTableViewNLS from 'commons/nls/escoTableView.nls';
import CellExpand from 'commons/components/TableView/components/CellExpand';
import { itemStore } from 'commons/rdforms/itemstore';
import { entrystore } from 'commons/store';
import valueFormatter from 'commons/components/TableView/util/valueFormatter';
import { HandleEditRowContextProvider } from 'commons/components/TableView/HandleEditRowContext';
import Tooltip from 'commons/components/common/Tooltip';
import SearchFilterButton from 'commons/components/filters/SearchFiltersButton';
import SearchFilters from 'commons/components/filters/SearchFilters';
import useSearchFilters from 'commons/components/filters/hooks/useSearchFilters';
import Placeholder from 'commons/components/common/Placeholder';
import NoSearchResultsIcon from '@mui/icons-material/SearchOff';
import escoFiltersNLS from 'commons/nls/escoFilters.nls';
import { ConfirmLeaveDialog } from 'commons/components/common/dialogs/ConfirmLeaveDialog';
import TableViewEditDialog from './components/TableViewEditDialog';
import useTableViewRowActions from './useTableViewRowActions';
import {
  addRowBindings,
  loadRowChoices,
  getTableViewCellClassName,
  getTableViewRowClassName,
} from './util/rowUtils';
import {
  useTableViewContext,
  withTableViewContextProvider,
} from './TableViewContext';
import './style.scss';
import { getNoResultsNLS } from '../filters/utils/filter';
import { withListModelProvider } from '../ListView';

export const ColumnHeader = ({ name, tooltip }) => (
  <Tooltip title={tooltip || name}>
    <Typography className="escoTableView__columnHeader" color="secondary">
      {name}
    </Typography>
  </Tooltip>
);

ColumnHeader.propTypes = {
  name: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  tooltip: PropTypes.string,
};

const ActionsMenu = ({ row }) => {
  const translate = useTranslation(escoTableViewNLS);
  const [{ validationErrors }, dispatch] = useTableViewContext();
  const [anchorEl, setAnchorEl] = useState(null);
  const open = Boolean(anchorEl);
  const edited = row.graph.isChanged();
  const handleMenuClick = (event) => setAnchorEl(event.currentTarget);
  const handleMenuClose = () => setAnchorEl(null);
  const { handleSaveRow, handleDiscardRow } = useTableViewRowActions(dispatch);
  const [editDialogOpen, setEditDialogOpen] = useState(false);
  const openEditDialog = () => setEditDialogOpen(true);
  const closeEditDialog = () => setEditDialogOpen(false);

  return (
    <div className="escoTableView__rowActions">
      <Tooltip title={translate('rowMenuButtonTooltip')}>
        <IconButton
          id="row-actions-button"
          aria-label={translate('rowMenuButtonLabel')}
          edge="end"
          aria-controls="row-actions-menu"
          aria-haspopup="true"
          onClick={handleMenuClick}
          className="escoTableView__rowActionsButton"
        >
          <EllipsisIcon />
        </IconButton>
      </Tooltip>
      <Menu
        id="row-actions-menu"
        anchorEl={anchorEl}
        open={open}
        onClose={handleMenuClose}
        MenuListProps={{
          'aria-labelledby': 'row-actions-button',
        }}
      >
        <MenuItem
          onClick={() => {
            handleMenuClose();
            openEditDialog();
          }}
          key="editRowMenuItem"
        >
          {translate('editRowMenuItemLabel')}
        </MenuItem>
        {edited ? (
          <MenuItem
            onClick={() => handleSaveRow(row, handleMenuClose)}
            key="saveRowMenuItem"
            disabled={Boolean(validationErrors[row.id])}
          >
            {translate('saveRowMenuItemLabel')}
          </MenuItem>
        ) : null}
        {edited ? (
          <MenuItem
            onClick={() => handleDiscardRow(row, handleMenuClose)}
            key="discardRowMenuItem"
          >
            {translate('discardRowMenuItemLabel')}
          </MenuItem>
        ) : null}
      </Menu>
      {editDialogOpen ? (
        <TableViewEditDialog row={row} closeDialog={closeEditDialog} />
      ) : null}
    </div>
  );
};

ActionsMenu.propTypes = {
  row: PropTypes.shape({
    id: PropTypes.string,
    graph: graphPropType,
  }),
};

const extractColumns = (template, translate) => {
  const textItems = template
    .getChildren()
    .filter(
      (item) =>
        (item.getType() === 'text' || item.getType() === 'choice') &&
        !item.hasStyle('deprecated')
    );

  const commonColumnParams = {
    minWidth: 120,
    flex: 1,
    renderCell: CellExpand,
    valueFormatter,
    sortable: false,
  };

  const columns = textItems.map((item) => ({
    field: `${item._internalId}`,
    headerName: item.getLabel(),
    renderHeader: () => <ColumnHeader name={item.getLabel()} />,
    rdformsitem: item,
    hide: !(item.getCardinality().min > 0 || item.getCardinality().pref > 0), // By default selection only Mandatory & Recommemded fields
    ...commonColumnParams,
  }));
  const renderActionsMenu = (props) => <ActionsMenu {...props} />;

  return [
    {
      field: 'more',
      headerName: 'Actions',
      hideable: false,
      renderHeader: () => (
        <ColumnHeader
          name={
            <ActionsIcon
              titleAccess={translate('actionsColumnHeaderTooltip')}
            />
          }
          tooltip={translate('actionsColumnHeaderTooltip')}
        />
      ),
      width: 60,
      renderCell: renderActionsMenu,
      sortable: false,
    },
    ...columns,
  ];
};

const TableViewToolbar = () => (
  <GridToolbarContainer>
    <GridToolbarColumnsButton
      variant="outlined"
      classes={{ root: 'escoTableView__headerButtons' }}
      selected={false}
    />
    <GridToolbarDensitySelector
      variant="outlined"
      classes={{ root: 'escoTableView__headerButtons' }}
    />
  </GridToolbarContainer>
);

const TableViewPagination = () => {
  const [{ selectionModel, page, rowsPerPage, totalCount }, dispatch] =
    useTableViewContext();
  const disablePagination = Boolean(selectionModel.length);
  const translate = useTranslation(escoTableViewNLS);

  const handleChangePage = (_event, newPage) => {
    if (selectionModel.length) return;
    dispatch({ type: 'PAGINATE', value: { page: newPage } });
  };

  const handleChangeRowsPerPage = (event) => {
    dispatch({
      type: 'CHANGE_ROWS_PER_PAGE',
      value: { rowsPerPage: parseInt(event.target.value, 10), page: 0 },
    });
  };

  return (
    <TablePagination
      classes={{
        root: `escoTableView__pagination ${
          selectionModel.length ? 'escoTableView__pagination--disabled' : ''
        }`,
      }}
      backIconButtonProps={{
        disabled: disablePagination || page === 0,
      }}
      nextIconButtonProps={{
        disabled:
          disablePagination ||
          totalCount <= rowsPerPage ||
          page >= Math.floor(totalCount / rowsPerPage),
      }}
      component="div"
      count={totalCount}
      page={page}
      onPageChange={handleChangePage}
      rowsPerPage={rowsPerPage}
      onRowsPerPageChange={handleChangeRowsPerPage}
      labelRowsPerPage={translate('labelRowsPerPage')}
      SelectProps={{
        disabled: disablePagination,
        id: 'rows-per-page-select-label',
        inputProps: { 'aria-labelledby': 'rows-per-page-select-label' },
      }}
    />
  );
};

const TableViewFooter = () => {
  const translate = useTranslation(escoTableViewNLS);
  const [{ selectionModel, rows, validationErrors, error }, dispatch] =
    useTableViewContext();
  const { saveSelectedRows, discardSelectedRows } =
    useTableViewRowActions(dispatch);
  const selectedRows = rows.filter((row) => selectionModel.includes(row.id));
  const hasValidationErrors = Boolean(Object.entries(validationErrors).length);

  return (
    <GridToolbarContainer className="escoTableView__footerContainer">
      <div className="escoTableView__buttonWrapper">
        <Button
          classes={{ root: 'escoTableView__footerButton' }}
          variant="contained"
          color="primary"
          onClick={() => saveSelectedRows(selectedRows)}
          disabled={selectionModel.length === 0 || hasValidationErrors}
        >
          {translate('saveButtonLabel')}
        </Button>
        <Button
          classes={{ root: 'escoTableView__footerButton' }}
          variant="contained"
          color="primary"
          // eslint-disable-next-line no-unused-vars
          onClick={(_event) => discardSelectedRows(selectedRows)}
          disabled={selectionModel.length === 0}
        >
          {translate('discardButtonLabel')}
        </Button>
        <div className="escoTableView__errors">
          {error ? (
            <Typography className="escoTableView__errorMessage">
              {translate('errorMessage')}
            </Typography>
          ) : null}
          {hasValidationErrors ? (
            <Typography className="escoTableView__errorMessage">
              {translate('validationErrorMessage')}
            </Typography>
          ) : null}
        </div>
      </div>
      <TableViewPagination />
    </GridToolbarContainer>
  );
};

const TableViewFilters = () => {
  const translate = useTranslation(escoTableViewNLS);
  const [{ selectionModel, searchQuery = '' }, dispatch] =
    useTableViewContext();
  const disableSearch = Boolean(selectionModel.length);
  const trimmedQuery = searchQuery.trim();

  const handleSearch = (event) =>
    dispatch({ type: 'SEARCH', value: { searchQuery: event.target.value } });

  // eslint-disable-next-line no-unused-vars
  const clearSearch = (_event) => dispatch({ type: 'CLEAR_SEARCH' });

  return (
    <div className="escoTableViewSearch">
      <InputBase
        disabled={disableSearch}
        value={searchQuery}
        placeholder={translate('searchPlaceholder')}
        classes={{
          root: 'escoTableViewSearch__root',
        }}
        onChange={handleSearch}
        fullWidth
        inputProps={{ 'aria-label': 'Search' }}
        startAdornment={
          <InputAdornment
            position="start"
            classes={{
              root: `escoTableViewSearch__adornment ${
                disableSearch ? 'escoTableViewSearch__adornment--disabled' : ''
              }`,
            }}
          >
            <SearchIcon />
          </InputAdornment>
        }
        endAdornment={
          searchQuery ? (
            <InputAdornment position="end">
              <Tooltip title={translate('searchClearLabel')}>
                <IconButton
                  aria-label="searchClearLabel"
                  onClick={clearSearch}
                  onMouseDown={(e) => e.preventDefault()}
                  color="secondary"
                  disabled={disableSearch}
                >
                  <ClearIcon />
                </IconButton>
              </Tooltip>
            </InputAdornment>
          ) : null
        }
      />
      {trimmedQuery.length > 0 && trimmedQuery.length < 3 ? (
        <div className="escoTableViewSearch__statusMessage" role="status">
          {translate('searchHelperText')}
        </div>
      ) : null}
    </div>
  );
};

const NoRowsOverlay = ({ message, hide }) =>
  !hide ? (
    <Placeholder
      label={message}
      icon={<NoSearchResultsIcon sx={{ fontSize: '60px' }} />}
    />
  ) : null;

NoRowsOverlay.propTypes = { message: PropTypes.string, hide: PropTypes.bool };

const ListViewIconButton = ({ handleClick }) => {
  const translate = useTranslation(escoTableViewNLS);
  const [{ selectionModel }] = useTableViewContext();

  return (
    <Tooltip title={translate('gotoListView')}>
      <IconButton
        className="escoTableView__listViewIcon"
        onClick={handleClick}
        color="primary"
        disabled={Boolean(selectionModel.length)}
      >
        <ListAltIcon />
      </IconButton>
    </Tooltip>
  );
};

ListViewIconButton.propTypes = { handleClick: PropTypes.func };

const TableViewFilterBar = ({
  handleSwitchToList,
  includeFilters,
  hasActiveFilters,
  disableControls,
}) => {
  return (
    <div className="escoTableView__filterBar">
      <TableViewFilters />
      <div className="escoTableView__filterControls">
        {includeFilters ? (
          <SearchFilterButton
            filtersActiveBadge={hasActiveFilters}
            disabled={disableControls}
          />
        ) : null}
        <ListViewIconButton handleClick={handleSwitchToList} />
      </div>
    </div>
  );
};

TableViewFilterBar.propTypes = {
  handleSwitchToList: PropTypes.func,
  includeFilters: PropTypes.bool,
  hasActiveFilters: PropTypes.bool,
  disableControls: PropTypes.bool,
};

const TableView = ({
  setTableView,
  rdfType,
  templateId,
  filterScheme,
  nlsBundles = [],
}) => {
  const { context } = useESContext();
  const { data: queryResult, runAsync, status } = useAsync({ data: {} });
  const { runAsync: runLoadTableData } = useAsync(null);
  const filterProps = useSearchFilters(filterScheme);
  const { applyFilters, hasFilters } = filterProps;
  const hasSelectedFilter = filterProps.hasSelectedFilter();
  const translate = useTranslation([
    escoTableViewNLS,
    escoFiltersNLS,
    ...nlsBundles,
  ]);
  const template = useMemo(() => itemStore.getItem(templateId), [templateId]);
  const [
    {
      columns,
      rows,
      selectionModel,
      validationErrors,
      searchQuery = '',
      page,
      rowsPerPage,
      refreshCount,
    },
    dispatch,
  ] = useTableViewContext();
  const refreshTableView = useCallback(
    () => dispatch({ type: 'REFRESH_TABLE' }),
    [dispatch]
  );
  const minimumSearchLength = config.get('minimumSearchLength');
  const searchQueryString =
    searchQuery.trim().length < minimumSearchLength ? '' : searchQuery.trim();

  useEffect(() => {
    if (!context) return;
    const query = entrystore.newSolrQuery().rdfType(rdfType).context(context);
    if (hasFilters) applyFilters(query);

    const searchList = query
      .title(searchQueryString)
      .limit(rowsPerPage)
      .offset(page * rowsPerPage)
      .list();

    runAsync(
      searchList.getEntries(page).then((entries) => {
        const rowData = entries.map((entry) => ({
          entry,
          id: entry.getResourceURI(),
          graph: entry.getMetadata().clone(),
        }));
        return {
          rowData,
          totalCount: searchList.getSize(),
        };
      })
    );
  }, [
    context,
    template,
    page,
    searchQueryString,
    rdfType,
    dispatch,
    runAsync,
    rowsPerPage,
    applyFilters,
    hasFilters,
    refreshCount,
  ]);

  useEffect(() => {
    if (!template) return;
    const { rowData, totalCount } = queryResult;
    const getColumnsAndRows = async () => {
      const newColumns = extractColumns(template, translate);
      let newRows;
      if (rowData) {
        newRows = addRowBindings(rowData, template, newColumns);
        await loadRowChoices(newRows);
      }
      dispatch({
        type: 'LOAD_DATA',
        value: { rows: newRows, columns: newColumns, totalCount },
      });
    };
    runLoadTableData(getColumnsAndRows());
  }, [queryResult, runLoadTableData, template, dispatch, translate]);

  const isRowSelectable = (params) => params.row.graph.isChanged();

  const handleEditRow = (row) => {
    const {
      id: rowId,
      actions: { parent: binding },
    } = row;

    const { errors: rowErrors } = validate.bindingReport(binding);
    dispatch({
      type: 'EDIT_ROW',
      value: { rowId, rowErrors },
    });
  };

  const handleSwitchToList = () => {
    setTableView(false);
  };

  if (!rows || !columns) {
    return (
      <div className="escoTableView__circularProgress">
        <CircularProgress />
      </div>
    );
  }

  const overlayMessage = translate(
    getNoResultsNLS(searchQueryString, hasSelectedFilter),
    searchQueryString
  );

  return (
    <>
      <TableViewFilterBar
        includeFilters={hasFilters}
        hasActiveFilters={hasSelectedFilter}
        handleSwitchToList={handleSwitchToList}
        disableControls={Boolean(selectionModel.length)}
      />
      {hasFilters ? (
        <SearchFilters
          filterProps={filterProps}
          nlsBundles={[escoTableViewNLS, ...(nlsBundles || [])]}
          disabled={Boolean(selectionModel.length)}
          onFilterSelect={refreshTableView}
        />
      ) : null}
      <HandleEditRowContextProvider value={handleEditRow}>
        <div className="escoTableView__main">
          <DataGrid
            rows={rows}
            columns={columns}
            pagination
            pageSize={rowsPerPage}
            disableColumnMenu
            disableDensitySelector
            components={{
              Toolbar: TableViewToolbar,
              Footer: TableViewFooter,
              NoRowsOverlay,
            }}
            componentsProps={{
              noRowsOverlay: {
                message: overlayMessage,
                hide: status === 'idle' || status === 'pending',
              },
            }}
            disableColumnFilter
            disableSelectionOnClick
            isRowSelectable={isRowSelectable}
            getCellClassName={(gridCellParams) =>
              getTableViewCellClassName(gridCellParams, validationErrors)
            }
            getRowClassName={(gridRowParams) =>
              getTableViewRowClassName(
                gridRowParams,
                validationErrors,
                selectionModel
              )
            }
            localeText={{
              toolbarColumns: translate('toolbarColumns'),
              columnsPanelTextFieldLabel: translate(
                'columnsPanelTextFieldLabel'
              ),
              columnsPanelTextFieldPlaceholder: translate(
                'columnsPanelTextFieldPlaceholder'
              ),
              columnsPanelShowAllButton: translate('columnsPanelShowAllButton'),
              columnsPanelHideAllButton: translate('columnsPanelHideAllButton'),
            }}
          />
        </div>
      </HandleEditRowContextProvider>
      <ConfirmLeaveDialog shouldBlock={Boolean(selectionModel.length)} />
    </>
  );
};

TableView.propTypes = {
  setTableView: PropTypes.func,
  rdfType: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.arrayOf(PropTypes.string),
  ]),
  templateId: PropTypes.string,
  nlsBundles: nlsBundlesPropType,
  filterScheme: PropTypes.arrayOf(PropTypes.shape({})),
};

export default withListModelProvider(
  withTableViewContextProvider(TableView),
  () => ({ showFilters: true })
);
