import { useEffect, useState } from 'react';
import ListActionDialog from 'commons/components/common/dialogs/ListActionDialog';
import PropTypes from 'prop-types';
import { Button } from '@mui/material';
import { useEditorContext } from 'commons/components/forms/editors';
import useAsync from 'commons/hooks/useAsync';
import { useTranslation } from 'commons/hooks/useTranslation';
import esmoCommonsNLS from 'models/nls/esmoCommons.nls';
import esmoFieldNLS from 'models/nls/esmoFields.nls';
import escoDialogsNLS from 'commons/nls/escoDialogs.nls';
import { FIELD_ATTRIBUTE } from 'models/utils/fieldDefinitions';
import ContentWrapper from 'commons/components/common/ContentWrapper';
import { FieldEditor } from 'models/editors';
import { isExtended } from 'models/utils/extension';
import { useSnackbar, SUCCESS_EDIT } from 'commons/hooks/useSnackbar';
import { ErrorWithMessage } from 'commons/errors/utils/errors';
import { ErrorCatcher } from 'commons/errors/ErrorCatcher';
import { useConfirmCloseAction } from 'commons/components/EntryListView/dialogs/create/handlers';

const NLS_BUNDLES = [esmoCommonsNLS, esmoFieldNLS, escoDialogsNLS];

const EditAttributesDialog = ({ onSave, closeDialog }) => {
  const translate = useTranslation(NLS_BUNDLES);
  const { runAsync, error: saveError } = useAsync();
  const [hasChanges, setHasChanges] = useState(false);
  const { graph, entry: fieldEntry } = useEditorContext();
  const [addSnackbar] = useSnackbar();
  const confirmClose = useConfirmCloseAction(closeDialog);

  useEffect(() => {
    if (graph)
      graph.onChange = () => {
        setHasChanges(true);
      };
    return () => {
      if (graph) graph.onChange = () => {};
    };
  }, [graph]);

  const handleSaveForm = () => {
    runAsync(
      fieldEntry
        .setMetadata(graph)
        .commitMetadata()
        .then(() => {
          onSave();
          closeDialog();
        })
        .then(() => addSnackbar({ type: SUCCESS_EDIT }))
        .catch((error) => {
          throw new ErrorWithMessage(translate('saveEditsFail'), error);
        })
    );
  };

  const actions = (
    <Button autoFocus onClick={handleSaveForm} disabled={!hasChanges}>
      {translate('save')}
    </Button>
  );

  return (
    <>
      <ListActionDialog
        closeDialog={() => confirmClose(hasChanges)}
        id="edit-attribute"
        actions={actions}
        maxWidth="md"
        fixedHeight
        title={translate('editAttributesHeader')}
      >
        <ContentWrapper>
          <FieldEditor
            {...FIELD_ATTRIBUTE}
            translate={translate}
            isExtended={isExtended(
              fieldEntry.getMetadata(),
              fieldEntry.getResourceURI()
            )}
            label={
              isExtended(fieldEntry.getMetadata(), fieldEntry.getResourceURI())
                ? translate(FIELD_ATTRIBUTE.labelNlsKey)
                : ''
            }
          />
        </ContentWrapper>
      </ListActionDialog>
      <ErrorCatcher error={saveError} />
    </>
  );
};

EditAttributesDialog.propTypes = {
  onSave: PropTypes.func,
  closeDialog: PropTypes.func,
};

export default EditAttributesDialog;
