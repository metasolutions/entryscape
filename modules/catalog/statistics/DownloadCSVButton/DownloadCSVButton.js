import { useEffect } from 'react';
import PropTypes from 'prop-types';
import { Button } from '@mui/material';
import DownloadIcon from '@mui/icons-material/GetApp';
import Tooltip from 'commons/components/common/Tooltip';
import { useESContext } from 'commons/hooks/useESContext';
import useAsync from 'commons/hooks/useAsync';
import { useTranslation } from 'commons/hooks/useTranslation';
import { saveAs } from 'file-saver/FileSaver';
import statisticsNLS from 'catalog/nls/escaStatistics.nls';
import { getCSVData, getFileName } from '../utils/csvData';

const useDownloadCSV = (timeRange) => {
  const { context } = useESContext();
  const { error, runAsync } = useAsync();

  const downloadCSV = () => {
    runAsync(
      (async () => {
        const fileName = await getFileName(context, timeRange);
        const csvData = await getCSVData(context, timeRange);
        const blob = new Blob([csvData], {
          type: 'text/csv;charset=utf-8',
        });

        saveAs(blob, fileName, true);
      })()
    );
  };

  useEffect(() => {
    if (error) {
      console.error(error);
    }
  }, [error]);

  return { downloadCSV, error };
};

const DownloadCSVButton = ({ timeRange, disabled }) => {
  const translate = useTranslation(statisticsNLS);
  const { downloadCSV } = useDownloadCSV(timeRange);

  return (
    <Tooltip title={translate('actionsExportTooltip')}>
      <Button
        startIcon={<DownloadIcon />}
        onClick={downloadCSV}
        disabled={disabled}
      >
        {translate('actionsExport')}
      </Button>
    </Tooltip>
  );
};

DownloadCSVButton.propTypes = {
  timeRange: PropTypes.string,
  disabled: PropTypes.bool,
};

export default DownloadCSVButton;
