import PropTypes from 'prop-types';
import { Entry } from '@entryscape/entrystore-js';
import ContentWrapper from 'commons/components/common/ContentWrapper';
import ListActionDialog from 'commons/components/common/dialogs/ListActionDialog';
import escoListNLS from 'commons/nls/escoList.nls';
import {
  nlsBundlesPropType,
  useTranslation,
} from 'commons/hooks/useTranslation';
import { useSnackbar } from 'commons/hooks/useSnackbar';
import useErrorHandler from 'commons/errors/hooks/useErrorHandler';
import useAsync, { PENDING } from 'commons/hooks/useAsync';
import LoadingButton from 'commons/components/LoadingButton';

const defaultRemoveEntry = async (entry, failureMessage) => {
  try {
    await entry.del();
  } catch (error) {
    throw new Error(failureMessage);
  }
};

const RemoveEntryDialog = ({
  // actions props
  entry,
  closeDialog,

  // dialog props
  removeConfirmMessage,
  removeFailMessage,
  removeSuccessMessage,
  affirmMessage = 'removeButtonLabel',
  onRemove = defaultRemoveEntry,
  onRemoveCallback = async () => {},
  nlsBundles = escoListNLS,
}) => {
  const translate = useTranslation(nlsBundles);
  const [addSnackbar] = useSnackbar();
  const { runAsync, error, status } = useAsync();
  useErrorHandler(error);

  const handleRemove = () => {
    const successMessage =
      removeSuccessMessage || translate('removeSuccessMessage');
    const failureMessage = removeFailMessage || translate('removeFailMessage');

    return runAsync(
      onRemove(entry, failureMessage)
        .then(closeDialog)
        .then(async () => {
          await onRemoveCallback();
          addSnackbar({ message: successMessage });
        })
    );
  };

  const actions = (
    <LoadingButton
      autoFocus
      onClick={handleRemove}
      loading={status === PENDING}
    >
      {translate(affirmMessage)}
    </LoadingButton>
  );

  return (
    <ListActionDialog
      id="remove-entry"
      closeDialog={closeDialog}
      actions={actions}
      maxWidth="xs"
    >
      <ContentWrapper xs={10}>
        <div>{removeConfirmMessage || translate('removeEntryConfirm')}</div>
      </ContentWrapper>
    </ListActionDialog>
  );
};

RemoveEntryDialog.propTypes = {
  entry: PropTypes.instanceOf(Entry),
  closeDialog: PropTypes.func,
  removeConfirmMessage: PropTypes.oneOfType([PropTypes.node, PropTypes.string]),
  removeFailMessage: PropTypes.string,
  removeSuccessMessage: PropTypes.string,
  affirmMessage: PropTypes.string,
  onRemove: PropTypes.func,
  onRemoveCallback: PropTypes.func,
  nlsBundles: nlsBundlesPropType,
};

export default RemoveEntryDialog;
