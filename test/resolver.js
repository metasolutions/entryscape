/**
 * The resolver function is in this case used as a workaround to alter the main
 * property in package.json. This is needed since module setting isn't
 * respected in Jest, which would break rdforms imports.
 */
module.exports = (path, options) => {
  return options.defaultResolver(path, {
    ...options,
    // Only replace packageFilter for @entryscape libs
    ...(path.startsWith('@entryscape') && {
      packageFilter: (pkg) => {
        return {
          ...pkg,
          // Replace the value of `main` before resolving the package
          main: pkg.module || pkg.main,
        };
      },
    }),
  });
};
