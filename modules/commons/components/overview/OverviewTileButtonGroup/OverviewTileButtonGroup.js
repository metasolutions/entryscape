import PropTypes from 'prop-types';
import { Grid } from '@mui/material';
import GridList from 'commons/components/common/GridList';
import GridListTile from 'commons/components/common/GridListTile';
import { Children } from 'react';
import './OverviewTileButtonGroup.scss';

const OverviewTileButtonGroup = ({ cols, children }) => {
  const items = Children.toArray(children);
  return (
    <Grid container className="escoOverviewTileButtonGroup">
      <GridList
        cols={cols}
        gap={5}
        rowHeight="auto"
        classes={{ root: 'escoOverview__horizontalButtons' }}
      >
        {items.map((item, index) => (
          <GridListTile
            classes={{ root: 'escoOverview__tile' }}
            // eslint-disable-next-line react/no-array-index-key
            key={`overview-tile-${index}}`}
          >
            {item}
          </GridListTile>
        ))}
      </GridList>
    </Grid>
  );
};

OverviewTileButtonGroup.propTypes = {
  cols: PropTypes.number.isRequired,
  children: PropTypes.node.isRequired,
};

export default OverviewTileButtonGroup;
