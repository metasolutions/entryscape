import PropTypes from 'prop-types';
import { ListItemAction } from '../ListItemAction';
import { useActions } from '../../ActionsProvider';

// Component that associates a list item button component with an action dialog
export const ListItemButtonAction = ({
  action,
  ButtonComponent,
  children,
  justifyContent,
  xs,
  ...rest
}) => {
  const { openActionDialog } = useActions();
  const handleClick = () => openActionDialog(action);

  return (
    <ListItemAction justifyContent={justifyContent} xs={xs}>
      <ButtonComponent onClick={action && handleClick} {...rest}>
        {children}
      </ButtonComponent>
    </ListItemAction>
  );
};

ListItemButtonAction.propTypes = {
  action: PropTypes.shape({
    id: PropTypes.string,
    Dialog: PropTypes.func,
  }),
  ButtonComponent: PropTypes.elementType,
  children: PropTypes.node,
  justifyContent: PropTypes.string,
  xs: PropTypes.oneOfType([PropTypes.number, PropTypes.bool]),
};
