import PropTypes from 'prop-types';
import { Grid, IconButton, Stack } from '@mui/material';
import { useTranslation } from 'commons/hooks/useTranslation';
import Tooltip from 'commons/components/common/Tooltip';
import NoPropagation from 'commons/components/NoPropagatation';
import { getIconFromActionId } from 'commons/actions';
import { useActions } from '../../ActionsProvider';
import { useListView } from '../../hooks/useListView';
import useAsyncActions from '../../hooks/useAsyncActions';

const ActionItem = ({
  action,
  label,
  tooltip,
  disabled,
  highlightId,
  icon,
  ariaControls,
  children,
}) => {
  const { openActionDialog } = useActions();
  const handleClick = () => openActionDialog(action);

  return (
    <Tooltip title={tooltip || label}>
      <span>
        <IconButton
          aria-label={label}
          aria-controls={ariaControls}
          aria-haspopup="true"
          onClick={handleClick}
          disabled={disabled}
          data-highlight-id={highlightId}
        >
          {icon || children}
        </IconButton>
      </span>
    </Tooltip>
  );
};

ActionItem.propTypes = {
  action: PropTypes.shape({}),
  label: PropTypes.string,
  tooltip: PropTypes.string,
  disabled: PropTypes.bool,
  highlightId: PropTypes.string,
  icon: PropTypes.node,
  ariaControls: PropTypes.string,
  children: PropTypes.node,
};

export const ListItemActionsGroup = ({
  actions: initialActions,
  id,
  excludeActions = [],
  xs,
  xl,
  justifyContent = 'start',
  ...actionsParams
}) => {
  const { nlsBundles } = useListView();
  const translate = useTranslation(nlsBundles);

  const filteredActions = initialActions.filter(
    ({ id: actionId }) => !excludeActions.includes(actionId)
  );

  const mergedActions = filteredActions.map(
    ({ id: actionId, icon, highlightId, getProps, ...action }) => {
      const statefulProps = getProps ? getProps(actionsParams) : {};
      return {
        id: actionId,
        action: {
          nlsBundles,
          ...action,
          ...actionsParams,
          ...statefulProps,
        },
        icon: icon || statefulProps.icon || getIconFromActionId(actionId),
        label: translate(action.labelNlsKey),
        tooltip: action.tooltipNlsKey ? translate(action.tooltipNlsKey) : '',
        highlightId,
      };
    }
  );

  const { actions, isLoading } = useAsyncActions(mergedActions);

  return (
    <Grid
      item
      container
      justifyContent={justifyContent}
      alignItems="center"
      xs={xs}
      xl={xl}
    >
      <NoPropagation>
        <Stack direction="row" spacing={1}>
          {!isLoading
            ? actions.map((actionItem) => {
                return <ActionItem key={actionItem.id} {...actionItem} />;
              })
            : null}
        </Stack>
      </NoPropagation>
    </Grid>
  );
};

ListItemActionsGroup.propTypes = {
  actions: PropTypes.arrayOf(PropTypes.shape({})),
  id: PropTypes.string,
  actionParams: PropTypes.shape({}),
  excludeActions: PropTypes.arrayOf(PropTypes.string),
  xl: PropTypes.number,
  xs: PropTypes.number,
  justifyContent: PropTypes.string,
};
