import PropTypes from 'prop-types';
import {
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  Button,
} from '@mui/material';
import { useTranslation } from 'commons/hooks/useTranslation';
import escaDatasetTemplateNLS from 'catalog/nls/escaDatasetTemplate.nls';
import { getLabel } from 'commons/util/rdfUtils';
import { Entry } from '@entryscape/entrystore-js';
import { localize } from 'commons/locale';
import {
  CHANGE_CATEGORY,
  CHANGE_CATALOG,
  RESET,
} from 'catalog/datasetTemplates/reducer';
import {
  useListModel,
  RESET as LIST_RESET,
  PAGINATE,
  LIST_MODEL_DEFAULT_VALUE,
} from 'commons/components/ListView';
import './DatasetTemplateListFilters.scss';

const DatasetTemplateListFilters = ({
  includeCatalogFilter,
  selectedCatalogId,
  catalogEntries = [],
  categories,
  setCategories,
  selectedCategory,
  filtersDispatch,
}) => {
  const translate = useTranslation([escaDatasetTemplateNLS]);
  const [, listModelDispatch] = useListModel();

  const handleCatalogChange = (event) => {
    filtersDispatch({
      type: CHANGE_CATALOG,
      value: event.target.value,
    });
    listModelDispatch({ type: PAGINATE, value: 0 });
  };

  const handleCategoryChange = (event) => {
    filtersDispatch({
      type: CHANGE_CATEGORY,
      value: event.target.value,
    });
    listModelDispatch({ type: PAGINATE, value: 0 });
  };

  const handleClear = () => {
    if (includeCatalogFilter) setCategories([]);
    filtersDispatch({ type: RESET });
    listModelDispatch({
      type: LIST_RESET,
      value: {
        ...LIST_MODEL_DEFAULT_VALUE,
        sort: {
          field: 'title',
          order: 'asc',
        },
      },
    });
  };

  return (
    <div className="escaDatasetTemplate__filters">
      <FormControl
        variant="filled"
        margin="none"
        fullWidth
        className={`escaDatasetTemplate__formControl ${
          includeCatalogFilter
            ? 'escaDatasetTemplate__catalogFormControl'
            : 'escaDatasetTemplate__catalogFormControl--hidden'
        }`}
      >
        <InputLabel id="context-select-label">
          {translate('contextSelect')}
        </InputLabel>
        <Select
          inputProps={{ 'aria-labelledby': 'context-select-label' }}
          value={selectedCatalogId}
          onChange={handleCatalogChange}
          autoWidth
          disabled={!includeCatalogFilter}
        >
          <MenuItem value="">
            <em>{translate('selectDefault')}</em>
          </MenuItem>
          {catalogEntries.map((catalogEntry) => {
            const id = catalogEntry.getContext().getId();
            return (
              <MenuItem key={id} value={id}>
                {getLabel(catalogEntry)}
              </MenuItem>
            );
          })}
        </Select>
      </FormControl>
      <FormControl
        variant="filled"
        margin="none"
        fullWidth
        className="escaDatasetTemplate__formControl"
      >
        <InputLabel id="category-select-label">
          {translate('categorySelect')}
        </InputLabel>
        <Select
          inputProps={{ 'aria-labelledby': 'category-select-label' }}
          value={selectedCategory}
          onChange={handleCategoryChange}
          disabled={!categories.length}
        >
          <MenuItem value="">
            <em>{translate('selectDefault')}</em>
          </MenuItem>
          {categories.map(({ label, value }) => (
            <MenuItem key={value} value={value}>
              {localize(label)}
            </MenuItem>
          ))}
        </Select>
      </FormControl>

      <Button variant="text" onClick={handleClear}>
        {translate('clearFilters')}
      </Button>
    </div>
  );
};

DatasetTemplateListFilters.propTypes = {
  includeCatalogFilter: PropTypes.bool,
  selectedCatalogId: PropTypes.string,
  catalogEntries: PropTypes.arrayOf(PropTypes.instanceOf(Entry)),
  categories: PropTypes.arrayOf(
    PropTypes.shape({
      value: PropTypes.string,
      label: PropTypes.shape({}),
    })
  ),
  setCategories: PropTypes.func,
  selectedCategory: PropTypes.string,
  filtersDispatch: PropTypes.func,
};

export default DatasetTemplateListFilters;
