import RemoveEntryDialog from 'commons/components/entry/RemoveEntryDialog';
import { getPathFromViewName } from 'commons/util/site';
import { useParams } from 'react-router-dom';
import useNavigate from 'commons/components/router/useNavigate';
import PropTypes from 'prop-types';
import { Entry } from '@entryscape/entrystore-js';
import esteCollectionNLS from 'terms/nls/esteCollection.nls';
import { useTranslation } from 'commons/hooks/useTranslation';

const nlsBundles = [esteCollectionNLS];

const RemoveDialog = ({ entry, closeDialog }) => {
  const { navigate } = useNavigate();
  const viewParams = useParams();
  const collectionsPath = getPathFromViewName(
    'terminology__collections',
    viewParams
  );

  const translate = useTranslation(nlsBundles);
  const onConfirmRemove = () => entry.del();

  return (
    <RemoveEntryDialog
      entry={entry}
      onRemove={onConfirmRemove}
      onRemoveCallback={() => navigate(collectionsPath)}
      closeDialog={closeDialog}
      removeConfirmMessage={translate('removeEntryConfirm')}
    />
  );
};

RemoveDialog.propTypes = {
  entry: PropTypes.instanceOf(Entry).isRequired,
  closeDialog: PropTypes.func.isRequired,
};

export default RemoveDialog;
