import { useEffect, useRef, useState } from 'react';
import PropTypes from 'prop-types';
import { Box, IconButton, Paper, Typography } from '@mui/material';
import CloseIcon from '@mui/icons-material/Close';
import { useTranslation } from 'commons/hooks/useTranslation';
import escoMapNls from 'commons/nls/escoMap.nls';
import { useOpenlayers } from '../../hooks/useOpenlayers';
import { useMapContext } from '../../hooks/useMapContext';
import './FeatureInfo.scss';

export const FeatureInfoPopup = ({
  feature,
  maxHeight = '250px',
  titleField,
  close,
}) => {
  const translate = useTranslation(escoMapNls);
  const geometryName = feature.getGeometryName(); // ol geometry object to exclude
  const properties = feature.getProperties();
  const title = titleField && properties[titleField];
  const excludeProperties = titleField
    ? [geometryName, titleField]
    : [geometryName];

  return (
    <Paper className="escoFeatureInfoPopup" elevation={24}>
      <IconButton
        aria-label={translate('closePopupLabel')}
        size="small"
        className="escoFeatureInfoPopup__closeButton"
        onClick={close}
      >
        <CloseIcon />
      </IconButton>
      {title ? (
        <Typography variant="h2" className="escoFeatureInfoPopup__heading">
          {`${title}`}
        </Typography>
      ) : null}
      <Box className="escoFeatureInfoPopup__content" sx={{ maxHeight }}>
        <ul className="escoFeatureInfoPopupList">
          {Object.entries(properties)
            .filter(([property]) => !excludeProperties.includes(property))
            .map(([property, value]) => (
              <li key={property} className="escoFeatureInfoPopupListItem">
                <Typography
                  component="span"
                  className="escoFeatureInfoPopupListItem__property"
                >{`${property}: `}</Typography>
                <Typography
                  component="span"
                  className="escoFeatureInfoPopupListItem__value"
                >{`${value}`}</Typography>
              </li>
            ))}
        </ul>
      </Box>
    </Paper>
  );
};

FeatureInfoPopup.propTypes = {
  feature: PropTypes.shape({
    getGeometryName: PropTypes.func,
    getProperties: PropTypes.func,
  }),
  titleField: PropTypes.string,
  maxHeight: PropTypes.string,
  close: PropTypes.func,
};

export const FeatureInfo = ({ titleField, maxHeight }) => {
  const ref = useRef();
  const mapUtils = useOpenlayers();
  const [{ map }] = useMapContext();
  const [mapOverlay, setMapOverlay] = useState();
  const [selectedFeature, setSelectedFeature] = useState();

  useEffect(() => {
    const addOverlay = () => {
      const overlay = mapUtils.createOverlay({
        element: ref.current,
      });
      map.addOverlay(overlay);
      setMapOverlay(overlay);
    };

    if (!mapUtils.status === 'resolved' || !map || mapOverlay) return;

    addOverlay();
  }, [map, mapOverlay, mapUtils]);

  // effect for handling map listeners
  useEffect(() => {
    /**
     * @param {object} event
     * @returns {undefined}
     */
    function handleClick(event) {
      const feature = map.forEachFeatureAtPixel(
        event.pixel,
        (featureAtPixel) => featureAtPixel,
        { hitTolerance: 3 }
      );
      setSelectedFeature(feature);
      if (feature) {
        mapOverlay.setPosition(event.coordinate);
      }
    }

    if (!mapOverlay) return;

    map.on('click', handleClick);

    return () => {
      map.un('click', handleClick);
    };
  }, [map, mapOverlay]);

  return (
    <div ref={ref}>
      {selectedFeature ? (
        <FeatureInfoPopup
          feature={selectedFeature}
          titleField={titleField}
          maxHeight={maxHeight}
          close={() => setSelectedFeature()}
        />
      ) : null}
    </div>
  );
};

FeatureInfo.propTypes = {
  maxHeight: PropTypes.string,
  titleField: PropTypes.string,
};
