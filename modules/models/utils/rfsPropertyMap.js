import * as ns from 'models/utils/ns';

export const rfsPropertyMap = {
  'rdf:type': 'type',
  'skos:definition': 'description',
  'skos:prefLabel': 'label',
  [ns.RDF_PROPERTY_PROPERTY]: 'property',
  [ns.RDF_PROPERTY_NODETYPE]: 'nodetype',
  [ns.RDF_PROPERTY_DATATYPE]: 'datatype',
  [ns.RDF_PROPERTY_LABEL]: 'label',
  [ns.RDF_PROPERTY_DESCRIPTION]: 'description',
  [ns.RDF_PROPERTY_PURPOSE]: 'purpose',
  [ns.RDF_PROPERTY_HELP]: 'help',
  [ns.RDF_PROPERTY_PLACEHOLDER]: 'placeholder',
  [ns.RDF_PROPERTY_SPECIFICATION]: 'specification',
  [ns.RDF_PROPERTY_EDIT_LABEL]: 'editlabel',
  [ns.RDF_PROPERTY_EDIT_DESCRIPTION]: 'editdescription',
  [ns.RDF_PROPERTY_CSS]: 'cls',
  [ns.RDF_PROPERTY_ATTRIBUTE]: 'styles',
  [ns.RDF_PROPERTY_CONSTRAINT]: 'constraints',
  [ns.RDF_PROPERTY_LANGUAGE]: 'language',
  [ns.RDF_PROPERTY_EXTENDS]: 'extends',
  [ns.RDF_PROPERTY_VALUE]: 'value',
  [ns.RDF_PROPERTY_VALUE_TEMPLATE]: 'valueTemplate',
  [ns.RDF_PROPERTY_PATTERN]: 'pattern',
};

/**
 *
 * @param {string} property
 * @returns {string}
 */
export const getRfsProperty = (property) => {
  if (property in rfsPropertyMap) return rfsPropertyMap[property];
  throw new Error(`Property ${property} is not valid`);
};
