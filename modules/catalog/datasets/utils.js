import { Entry } from '@entryscape/entrystore-js';
import { RDF_TYPE_DATASET_SERIES } from 'commons/util/entry';
import { isType } from 'commons/util/metadata';

/**
 * Determines if given entry is a dataset series
 *
 * @param {Entry} entry
 * @returns {boolean}
 */
export const isDatasetSeries = (entry) =>
  isType(entry.getMetadata(), entry.getResourceURI(), RDF_TYPE_DATASET_SERIES);

/**
 * Determines if given entry is based on a dataset template
 *
 * @param {Entry} entry
 * @returns {boolean}
 */
export const isBasedOnTemplate = (entry) =>
  Boolean(
    entry
      ?.getMetadata()
      ?.findFirstValue(entry?.getResourceURI(), 'esterms:basedOn')
  );
