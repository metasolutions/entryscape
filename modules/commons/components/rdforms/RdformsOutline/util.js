import {
  canvas as backgroundColor,
  warning as warningColor,
} from 'commons/theme/mui/colors';
import { namespaces as ns } from '@entryscape/rdfjson';

/**
 * Determines the classes of an outline item depending on it
 * a) being in the intersection area or
 * b) a sub-item
 *
 * @param {boolean} active
 * @param {boolean} indented
 * @param {boolean} hasError
 * @returns {string}
 */
export const getOutlineItemClasses = (active, indented, hasError) => {
  const classes = ['escoRdformsOutline__listItem'];
  if (active) classes.push('escoRdformsOutline__listItem--active');
  if (indented) classes.push('escoRdformsOutline__listItem--indented');
  if (hasError) classes.push('escoRdformsOutline__listItem--error');
  return classes.join(' ');
};

/**
 * Highlights the DOM element matching the provided id using animation.
 *
 * @param {string} id
 * @returns {Animation}
 */
export const highlightElement = (id) =>
  document
    .getElementById(id)
    .animate(
      [{ background: warningColor.main }, { background: backgroundColor.main }],
      {
        duration: 1200,
        direction: 'normal',
      }
    );

/**
 * Scrolls to the DOM element matching the provided id, taking care to respect
 * accessibility settings for reduced motion.
 *
 * @param {string} id
 * @param {string} rootId - Root element id, corresponds to the dialog
 * @param {string} contentClassName - Classname of the content element
 */
export const scrollToElement = (
  id,
  rootId,
  contentClassName = 'escoRDFormsDialogWrapper__content'
) => {
  const anchorElement = document.getElementById(id);
  const rootElement = document.getElementById(rootId);
  const scrollableElement = rootElement.querySelector('.MuiDialogContent-root');

  const contentElement = document.getElementsByClassName(contentClassName)[0];

  const offset = contentElement.offsetTop;
  // Need special treatment for indented outline items (that correspond to
  // children of an rdformsGroup)
  const anchorOffset = anchorElement.offsetParent.className.includes(
    'rdformsGroup'
  )
    ? anchorElement.offsetParent.offsetTop + anchorElement.offsetTop
    : anchorElement.offsetTop;

  const prefersReducedMotion = window.matchMedia(
    '(prefers-reduced-motion)'
  ).matches;

  scrollableElement.scrollTo({
    top: anchorOffset - offset - 4, // scroll a few pixel above to avoid rounding errors.
    behavior: prefersReducedMotion ? 'auto' : 'smooth',
  });
};

/**
 * Moves focus to the rdforms label of the element matching the provided id.
 *
 * @param {string} id
 */
export const moveFocusToLabel = (id) => {
  const targetElement = document.getElementById(id);
  const labelElement = targetElement.getElementsByClassName('rdformsLabel')[0];
  labelElement.focus();
};

/**
 * Checks whether the element matching the provided id is in the intersection
 * area. Used to indicate the "current" element in the outline.
 *
 * @param {string} id
 * @param {Map[]} observedVisibility
 * @returns {boolean}
 */
export const isActive = (id, observedVisibility) => {
  const element = document.getElementById(id);
  const [active] = Array.from(observedVisibility.keys()).filter((key) =>
    observedVisibility.get(key)
  );
  return document.getElementById(active)?.contains(element);
};

/**
 * Checks whether a predicate is excluded through the filter predicates structure
 *
 * @param {string} predicate
 * @param {object} filterPredicates
 * @returns {boolean}
 */
export const isExcludedPredicate = (predicate, filterPredicates) => {
  if (!filterPredicates) return false;
  return Object.keys(filterPredicates).some(
    (filterPredicate) =>
      ns.expand(filterPredicate) === predicate &&
      filterPredicates[filterPredicate]
  );
};
