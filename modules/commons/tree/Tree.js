import jquery from 'jquery';
import skosUtil from './skos/util';
import TreeModel from './TreeModel';

export default class Tree {
  constructor({
    context,
    domNode,
    isMoveAllowed,
    readOnly,
    disallowedSiblingMove = () => {},
    locale = undefined,
  }) {
    this.context = context;
    this.domNode = domNode;
    this.isMoveAllowed = isMoveAllowed; // passed over to TreeModel
    this.disallowedSiblingMove = disallowedSiblingMove;

    this.jsTreeConf = {
      plugins: ['dnd', 'sort'],
      sort: (uriA, uriB) => {
        const nodeA = this.model.getNodeFromURI(uriA).text;
        const nodeB = this.model.getNodeFromURI(uriB).text;
        return nodeA.localeCompare(nodeB, locale, { numeric: true });
      },
      core: {
        multiple: false,
        check_callback: this.checkMove.bind(this),
        themes: {
          name: 'proton',
          responsive: true,
        },
        force_text: true,
      },
      dnd: {
        is_draggable(nodes) {
          return readOnly ? false : nodes[0].id !== '#';
        },
        check_while_dragging: false,
      },
    };
  }

  show(entry) {
    if (this.context && entry) {
      this.showEntry(entry);
    }
  }

  getTree() {
    return jquery(this.domNode).jstree(true);
  }

  getTreeModel() {
    return this.model;
  }

  getSelectedNode() {
    const nodeidArr = this.getTree().get_selected(true);
    if (nodeidArr.length > 0) {
      return nodeidArr[0];
    }

    return undefined; // TODO
  }

  deleteNode() {
    const node = this.getSelectedNode();
    this.getTree().delete_node(node);
  }

  refresh(entry, deep) {
    const model = this.getTreeModel();
    const node = model.getNode(entry);
    if (deep === true) {
      this.getTreeModel().refresh(entry);
      this.getTree().refresh_node(node);
    } else {
      node.text = model.getText(entry);
      this.getTree().rename_node(node, node.text);
    }
  }

  showEntry(entry) {
    if (this.model) {
      this.model.destroy();
    }

    this.model = new TreeModel({
      ...skosUtil.getSemanticProperties(),
      jsTreeConf: this.jsTreeConf,
      rootEntry: entry,
      domNode: this.domNode,
      isMoveAllowed: this.isMoveAllowed,
    });
  }

  checkMove(operation, node, nodeParent) {
    // allow anything apart from moving in the same partner, aka re-ordering
    if (operation === 'move_node' && node.parent === nodeParent.id) {
      this.disallowedSiblingMove();
      return false;
    }
    return true;
  }
}
