import eswoSpacesNLS from 'workbench/nls/eswoSpaces.nls';
import esteImportNLS from 'terms/nls/esteImport.nls'; // For future fix: we don't have NLS for 'uploadTask' in workbench now
import escoListNLS from 'commons/nls/escoList.nls';
import CreateProjectDialog from '../dialogs/CreateProject';

// TODO there's too many nls for the actions. fix up
const nlsBundles = [eswoSpacesNLS, esteImportNLS, escoListNLS];

const noRestriction = () => true;

const listActions = [
  {
    id: 'create',
    Dialog: CreateProjectDialog,
    isVisible: noRestriction,
    labelNlsKey: 'createButtonLabel',
    tooltipNlsKey: 'actionTooltip',
  },
];

export { nlsBundles, listActions };
