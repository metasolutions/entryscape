import { useState } from 'react';

function useErrorHandler(errorToHandle) {
  const [error, setError] = useState(null);
  if (errorToHandle) throw errorToHandle;
  if (error) throw error;
  return setError;
}

export default useErrorHandler;
