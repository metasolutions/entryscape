import moment from 'moment/moment';
import {
  success as successColor,
  error as errorColor,
} from 'commons/theme/mui/colors';
/**
 * Iterates over daily dataset data adding new (equal to previous day's datasets)
 * when a day is missing.
 * @param {Array.<Object>} data
 */
const addMissingData = (data) => {
  if (data.length === 0) {
    return data;
  }

  const uniqueBySubProp = (arr, prop, subProp) => {
    const tempArr = arr.map((el) => el[prop][subProp]);
    return arr.filter(
      (obj, index) => tempArr.indexOf(obj[prop][subProp]) === index
    );
  };

  const uniqueData = uniqueBySubProp(data, 'x', '_i');
  const sortedData = uniqueData.sort((a, b) => a.x.diff(b.x));

  const lastElement = sortedData[sortedData.length - 1];
  const todaysDate = moment().startOf('day');
  if (!todaysDate.isSame(lastElement.x, 'day')) {
    sortedData.push({
      x: todaysDate,
      y: lastElement.y,
      type: 'inferred',
    });
  }

  // TODO: @giorgos fix this mess - readability and only do the following for last 30 days
  // eslint-disable-next-line prefer-destructuring
  let length = sortedData.length;
  for (let i = 0; i < length - 1; i++) {
    const currentElementDate = sortedData[i].x;
    const dateAfterCurrentElement = moment(currentElementDate).add(1, 'days'); // Notice the new date is on 'd' instead of 'x'
    if (i < length) {
      const nextElementDate = sortedData[i + 1].x;
      if (!dateAfterCurrentElement.isSame(nextElementDate, 'day')) {
        const newElement = {
          x: dateAfterCurrentElement,
          y: sortedData[i].y,
          type: 'inferred',
        };
        sortedData.splice(i + 1, 0, newElement);
        length += 1;
      }
    }
  }

  return sortedData;
};

/**
 * Differentiates bar background color depending on the corresponding value.
 * @param {Array.<Object>} data
 */
const getBarColors = (data) => {
  const barColors = [];
  data.forEach((d) => {
    const color = d.type === 'inferred' ? errorColor.light : successColor.main;
    barColors.push(color);
  });

  return barColors;
};

export { addMissingData, getBarColors };
