import { useState, createContext, useContext } from 'react';
import PropTypes from 'prop-types';
import './DrawControlGroup.scss';
import { ButtonGroup } from '@mui/material';

const DrawModeContext = createContext();

export const useDrawMode = () => {
  const context = useContext(DrawModeContext);
  if (context === undefined)
    throw new Error('DrawMode context must be used within context provider');
  return context;
};

export const DrawControlGroup = ({ children }) => {
  const [drawingMode, setDrawingMode] = useState(null);

  return (
    <div className="escoMapControlGroup">
      <DrawModeContext.Provider value={[drawingMode, setDrawingMode]}>
        <ButtonGroup size="small">{children}</ButtonGroup>
      </DrawModeContext.Provider>
    </div>
  );
};

DrawControlGroup.propTypes = {
  children: PropTypes.node,
};
