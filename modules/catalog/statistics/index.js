import React, { useEffect, useCallback, useRef, useState } from 'react';
import { Typography } from '@mui/material';
import { useESContext } from 'commons/hooks/useESContext';
import useAsync from 'commons/hooks/useAsync';
import useAsyncCallback from 'commons/hooks/useAsyncCallback';
import Placeholder from 'commons/components/common/Placeholder';
import { useTranslation } from 'commons/hooks/useTranslation';
import escaStatistics from 'catalog/nls/escaStatistics.nls';
import { isCatalogPublic } from 'catalog/utils/react/catalog';
import useIsMounted from 'commons/hooks/useIsMounted';
import { TIME_RANGE_THIS_MONTH, TIME_RANGE_THIS_YEAR } from './utils/timeRange';
import { getResourceType } from './utils/distribution';
import { StatisticsList } from './StatisticsList';
import DownloadCSVButton from './DownloadCSVButton';
import PrintButton from './PrintButton';
import { getStatisticsData, filterByFileType } from './utils/data';
import StatisticsChart, { ChartFilterControls } from './StatisticsChart';

const ROWS_PER_PAGE = 5;

const StatisticsView = () => {
  const { context } = useESContext();
  const [isPublic] = useAsyncCallback(isCatalogPublic, context);
  const isInitialRequest = useRef(true);
  const [selection, setSelection] = useState({ page: 0, selectedURI: '' });
  const [paginatedData, setPaginatedData] = useState([]);
  const [chartData, setChartData] = useState();
  const [filter, setFilter] = useState({
    searchQuery: '',
    timeRange: TIME_RANGE_THIS_MONTH,
    file: true,
    documentFile: true,
    api: true,
  });
  const isMounted = useIsMounted();
  const { data: statisticsData, status, runAsync } = useAsync();
  const t = useTranslation(escaStatistics);
  const updateFilter = useCallback((filterKey, filterParam) => {
    setFilter((state) => ({ ...state, [filterKey]: filterParam }));
  }, []);

  useEffect(() => {
    if (!filter.searchQuery || filter.searchQuery.length > 2) {
      runAsync(
        getStatisticsData(
          context,
          filter.timeRange,
          getResourceType(filter.file, filter.api, filter.documentFile),
          filter.searchQuery,
          isMounted
        ).then((data) =>
          /**
           * Since there is no difference between the requests
           * for distribution files and document files,
           * the statistics data needs to be filtered.
           */
          filterByFileType(data, filter.file, filter.documentFile)
        )
      );
    }
  }, [runAsync, context, filter, isMounted]);

  /**
   * Make sure pagination is reset on filter change,
   * since changing filters results in a new request
   * being made.
   */
  useEffect(() => {
    setSelection({ selectedURI: '', page: 0 });
  }, [filter]);

  // fetch secondary time range if no data for primary time range
  useEffect(() => {
    if (!statisticsData || !isInitialRequest.current) {
      return;
    }
    isInitialRequest.current = false;

    if (!statisticsData?.length) {
      updateFilter('timeRange', TIME_RANGE_THIS_YEAR);
    }
  }, [statisticsData, updateFilter]);

  useEffect(() => {
    /**
     * Make sure fetching statistics data is resolved before
     * getting derived data, since selection change happens
     * earlier.
     */
    if (!statisticsData || status !== 'resolved') return;
    const { selectedURI, page } = selection;
    const newPaginatedData = statisticsData.slice(
      page * ROWS_PER_PAGE,
      (page + 1) * ROWS_PER_PAGE
    );

    const selectedPaginatedData =
      newPaginatedData && selectedURI
        ? newPaginatedData.filter((item) => item.uri === selectedURI)
        : newPaginatedData;

    setPaginatedData(newPaginatedData);
    setChartData(selectedPaginatedData);
  }, [selection, statisticsData, status]);

  const handleSelectionChange = (newSelection) => {
    setSelection((state) => ({ ...state, ...newSelection }));
  };

  return (
    <>
      <Typography className="escaStatistics__heading" variant="h1">
        {t('statsViewHeader')}
      </Typography>
      {isPublic ? (
        <>
          <div className="escaStatisticsActions">
            <ChartFilterControls filter={filter} updateFilter={updateFilter} />
            <div className="escaStatisticsActions_buttonGroup">
              <DownloadCSVButton
                timeRange={filter.timeRange}
                disabled={!(chartData?.length > 0)}
              />
              <PrintButton disabled={!(chartData?.length > 0)} />
            </div>
          </div>
          <StatisticsChart
            statisticsData={chartData}
            timeRange={filter.timeRange}
          />
          <StatisticsList
            paginatedData={paginatedData}
            selectedURI={selection?.selectedURI}
            count={statisticsData?.length}
            page={selection?.page}
            handleSelectionChange={handleSelectionChange}
            rowsPerPage={ROWS_PER_PAGE}
          />
        </>
      ) : (
        <Placeholder label={t('statsNotPublishedCatalog')} />
      )}
    </>
  );
};

export default StatisticsView;
