import { useState } from 'react';
import PropTypes from 'prop-types';
import { entryPropType, graphPropType } from 'commons/util/entry';
import { useESContext } from 'commons/hooks/useESContext';
import useGetNamespaceEntries from 'models/namespaces/hooks/useGetNamespaceEntries';
import {
  NAMESPACE_ENTRY,
  RESOURCE_ENTRY,
  getURIEditorType,
} from 'models/utils/metadata';
import { RDF_PROPERTY_PROPERTY, RDF_TYPE_PROPERTY } from 'models/utils/ns';
import {
  nlsBundlesPropType,
  useTranslation,
} from 'commons/hooks/useTranslation';
import { getLabel } from 'commons/util/rdfUtils';
import { NamespaceEntryEditor } from '../NamespaceEntryEditor';
import { ResourceEntryEditor } from '../ResourceEntryEditor';
import { ResourceEditor } from '../ResourceEditor';

const PropertyEditor = ({
  graph,
  resourceURI,
  namespaceEntries,
  property,
  editChoices,
  ...props
}) => {
  const translate = useTranslation(props.nlsBundles);
  const [editMode, setEditMode] = useState(
    getURIEditorType(graph, resourceURI, RDF_PROPERTY_PROPERTY, RESOURCE_ENTRY)
  );

  const getEditorComponent = () => {
    if (editMode === RESOURCE_ENTRY) return ResourceEntryEditor;
    if (editMode === NAMESPACE_ENTRY) return NamespaceEntryEditor;
    return ResourceEditor;
  };

  const EditorComponent = getEditorComponent();

  const editModeChoices = editChoices.map(({ value, labelNlsKey }) => ({
    label: translate(labelNlsKey),
    value,
  }));

  return (
    <EditorComponent
      graph={graph}
      resourceURI={resourceURI}
      onEditModeChange={setEditMode}
      editModeChoices={editModeChoices}
      property={property}
      namespaceEntries={editMode === NAMESPACE_ENTRY ? namespaceEntries : null}
      fieldsCount={1}
      {...(editMode === RESOURCE_ENTRY
        ? {
            editModeValue: RESOURCE_ENTRY,
            resourceRdfType: RDF_TYPE_PROPERTY,
            selectTooltipNlsKey: 'selectPropertyLabel',
            placeholderProps: { label: translate('noPropertiesWarning') },
            getTitleProps: ({ entry }) => ({
              primary: getLabel(entry),
              secondary: entry.getResourceURI(),
            }),
          }
        : {})}
      {...props}
    />
  );
};

PropertyEditor.propTypes = {
  property: PropTypes.string,
  graph: graphPropType,
  resourceURI: PropTypes.string,
  namespaceEntries: PropTypes.arrayOf(entryPropType),
  nlsBundles: nlsBundlesPropType,
  editChoices: PropTypes.arrayOf(PropTypes.shape({})),
};

const PropertyEditorWithGetNamespaceEntries = (props) => {
  const { context } = useESContext();
  const { namespaceEntries, status } = useGetNamespaceEntries(context);
  return status === 'resolved' ? (
    <PropertyEditor namespaceEntries={namespaceEntries} {...props} />
  ) : null;
};

export { PropertyEditorWithGetNamespaceEntries as PropertyEditor };
