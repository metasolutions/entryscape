import {
  RDF_CONSTRAINT_NULL_URI,
  RDF_TYPE_CONSTRAINT,
  RDF_PROPERTY_CONSTRAINT,
  RDF_PROPERTY_CONSTRAINT_VALUE,
  RDF_PROPERTY_CONSTRAINT_PROPERTY,
  RDF_PROPERTY_ORDER,
} from 'models/utils/ns';
import { Graph } from '@entryscape/rdfjson';
import { nameToURI } from 'commons/types/utils/uri';
import {
  DATATYPE_LITERAL,
  LANGUAGE_LITERAL,
  LITERAL,
  URI,
} from 'commons/components/forms/presenters';
import { ENTITY_TYPE_ID_PREFIX } from './namespace';

/**
 * @param {Object} fieldDefinition
 * @returns {Graph}
 */
export const addLiteral = ({ graph, resourceURI, property, value }) => {
  graph.addL(resourceURI, property, value);
  return graph;
};

/**
 * @param {Object} fieldDefinition
 * @returns {Graph}
 */
export const addURI = ({ graph, resourceURI, property, value }) => {
  graph.add(resourceURI, property, value);
  return graph;
};

/**
 * @param {Object} fieldDefinition
 * @returns {Graph}
 */
export const addDatatype = ({
  graph,
  resourceURI,
  property,
  value,
  datatype,
}) => {
  graph.addD(resourceURI, property, `${value}`, datatype);
  return graph;
};

/**
 * @param {Object} fieldDefinition
 * @returns {Graph}
 */
export const addLanguageLiteral = ({
  graph,
  resourceURI,
  property,
  value: languageValues,
}) => {
  const languageKeyValues = Object.entries(languageValues);
  for (const [lang, value] of languageKeyValues) {
    graph.add(resourceURI, property, { type: 'literal', value, lang });
  }
  return graph;
};

/**
 * @param {Object} fieldDefinition
 * @returns {Graph}
 */
export const addURIFromName = ({ graph, resourceURI, property, value }) => {
  const refinesURI = nameToURI(`${ENTITY_TYPE_ID_PREFIX}-${value}`, 'config');
  addURI({ graph, resourceURI, property, value: refinesURI });
  return graph;
};

/**
 * @param {Object} fieldDefinition
 * @returns {Graph}
 */
export const addStoreConfig = ({ graph, resourceURI, property, value }) => {
  const { label, endpoint } = value;
  const blankNodeId = graph.add(resourceURI, property).getValue();
  addLanguageLiteral({
    graph,
    resourceURI: blankNodeId,
    property: 'dcterms:title',
    value: label,
  });
  addURI({
    graph,
    resourceURI: blankNodeId,
    property: 'dcterms:source',
    value: endpoint,
  });
  return graph;
};

/**
 * @param {Object} fieldDefinition
 * @returns {Graph}
 */
const addConstraint = ({ graph, resourceURI, property, values }) => {
  const blankNodeId = graph
    .add(resourceURI, RDF_PROPERTY_CONSTRAINT)
    .getValue();

  // Add constraint property
  graph.add(blankNodeId, 'rdf:type', RDF_TYPE_CONSTRAINT);
  graph.add(blankNodeId, RDF_PROPERTY_CONSTRAINT_PROPERTY, property);

  // Add constraint values
  values.forEach((value, index) => {
    const valueBlankId = graph
      .add(blankNodeId, RDF_PROPERTY_CONSTRAINT_VALUE)
      .getValue();
    graph.add(
      valueBlankId,
      'rdf:value',
      value === null ? RDF_CONSTRAINT_NULL_URI : value // some constraints uses null to express match
    );
    graph.addD(valueBlankId, RDF_PROPERTY_ORDER, `${index + 1}`, 'xsd:integer');
  });

  return graph;
};

/**
 * @param {Object} fieldDefinition
 * @returns {Graph}
 */
export const addConstraints = ({
  graph,
  resourceURI,
  value: constraintsValue,
}) => {
  const constraintKeyValues = Object.entries(constraintsValue);
  for (const [property, value] of constraintKeyValues) {
    const values = Array.isArray(value) ? value : [value];
    addConstraint({ graph, resourceURI, property, values });
  }
  return graph;
};

export const addConstraintFromRdfType = ({ graph, resourceURI, value }) => {
  const rdfTypeValues = Array.isArray(value) ? value : [value];
  addConstraint({
    graph,
    resourceURI,
    property: 'rdf:type',
    values: rdfTypeValues,
  });
  return graph;
};

/**
 * @param {Object} fieldDefinition
 * @returns {Function|undefined}
 */
export const getExporter = (fieldDefinition = {}) => {
  const { nodetype, toGraph } = fieldDefinition;

  if (toGraph) return toGraph;
  if (!nodetype) return;

  switch (nodetype) {
    case URI:
      return addURI;
    case LITERAL:
      return addLiteral;
    case LANGUAGE_LITERAL:
      return addLanguageLiteral;
    case DATATYPE_LITERAL:
      return addDatatype;
    default:
      console.error('Unknown type for converter');
      break;
  }
};

/**
 * @param {Object} exportOptions
 * @returns {Object}
 */
export const typeConfigToGraph = ({ entry, typeConfig, fields, idPrefix }) => {
  if (!typeConfig) return {};
  const graph = entry
    ? new Graph(entry.getMetadata().exportRDFJSON())
    : new Graph();
  const resourceURI = entry
    ? entry.getResourceURI()
    : nameToURI(`${idPrefix}-${typeConfig.name}`, 'config');

  // add type configuration to graph
  const typeConfigKeyValues = Object.entries(typeConfig);
  typeConfigKeyValues.forEach(([key, value]) => {
    const fieldDefinition = fields.find(({ name }) => name === key);
    if (!fieldDefinition) return;

    const { property } = fieldDefinition;

    // don't add type config if property found in entry
    if (graph.findFirstValue(resourceURI, property)) return;

    const exportToGraph = getExporter(fieldDefinition);

    if (exportToGraph) {
      exportToGraph({ graph, resourceURI, value, ...fieldDefinition });
    }
  });

  return { graph, resourceURI };
};
