import * as ns from 'models/utils/ns';
import { Entry } from '@entryscape/entrystore-js';
import { Graph } from '@entryscape/rdfjson';
import { FORM_TYPE_CHOICES } from 'models/utils/choiceDefinitions';
import { isType } from 'commons/util/metadata';

const FORM_TYPES = [
  ns.RDF_TYPE_SECTION_FORM,
  ns.RDF_TYPE_PROPERTY_FORM,
  ns.RDF_TYPE_PROFILE_FORM,
  ns.RDF_TYPE_OBJECT_FORM,
];

/**
 * Get type of the forms
 *
 * @param {Graph} graph
 * @param {string} strresourceURIing
 * @returns {string}
 */

export const getFormType = (graph, resourceURI) =>
  FORM_TYPES.find((type) => {
    return graph.find(resourceURI, 'rdf:type', type).length;
  });

/**
 *
 * @param {Entry} entry
 * @returns {boolean}
 */
export const isForm = (entry) => {
  const graph = entry.getMetadata();
  const resourceURI = entry.getResourceURI();
  return Boolean(graph.find(resourceURI, 'rdf:type', ns.RDF_TYPE_FORM).length);
};

/**
 *
 * @param {Graph} graph
 * @param {string|null} resourceURI
 * @returns {boolean}
 */
export const isObjectForm = (graph, resourceURI) => {
  return isType(graph, resourceURI, ns.RDF_TYPE_OBJECT_FORM);
};

/**
 * Function to get label for form type.
 *
 * @param {Entry} entry
 * @param {Function} translate
 * @returns {string|undefined}
 */
export const getFormLabel = (entry, translate) => {
  if (!entry) return;
  const { labelNlsKey } =
    FORM_TYPE_CHOICES.find(({ value }) => {
      return entry.getMetadata().find(entry.getResourceURI(), 'rdf:type', value)
        .length;
    }) || {};
  return translate(labelNlsKey);
};
