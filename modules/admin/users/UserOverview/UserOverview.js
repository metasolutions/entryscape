import { useEntry } from 'commons/hooks/useEntry';
import { getLabel } from 'commons/util/rdfUtils';
import { getViewDefFromName } from 'commons/util/site';
import esadUserNLS from 'admin/nls/esadUser.nls';
import escoListNLS from 'commons/nls/escoList.nls';
import { useTranslation } from 'commons/hooks/useTranslation';
import useDocumentTitle from 'commons/hooks/useDocumentTitle';
import usePageTitle from 'commons/hooks/usePageTitle';
import useGetContributors from 'commons/hooks/useGetContributors';
import escoOverviewNLS from 'commons/nls/escoOverview.nls';
import {
  ACTION_INFO_WITH_ICON,
  DESCRIPTION_UPDATED,
} from 'commons/components/overview/actions';
import Overview from 'commons/components/overview/Overview';
import {
  withOverviewModelProvider,
  overviewPropsPropType,
  useOverviewModel,
} from 'commons/components/overview';
import LinkedDataBrowserDialog from 'commons/components/LinkedDataBrowserDialog';
import { localize } from 'commons/locale';
import { getUserRenderName } from 'commons/util/user';
import { sidebarActions } from './actions';

const nlsBundles = [esadUserNLS, escoListNLS, escoOverviewNLS];

const UserOverview = ({ overviewProps }) => {
  const entry = useEntry();
  const translate = useTranslation(nlsBundles);
  const [{ refreshCount }] = useOverviewModel();
  const { contributors } = useGetContributors(entry, refreshCount);
  const [pageTitle] = usePageTitle();
  const viewDefinition = getViewDefFromName('admin__users__user');
  const viewDefinitionTitle = localize(viewDefinition.title);

  const descriptionItems = [
    DESCRIPTION_UPDATED,
    {
      id: 'edited',
      labelNlsKey: 'editedByLabel',
      getValues: () => contributors,
    },
  ];

  useDocumentTitle(`${pageTitle} - ${viewDefinitionTitle} ${getLabel(entry)}`);

  return (
    <Overview
      {...overviewProps}
      backLabel={translate('backTitle')}
      headerAction={{
        ...ACTION_INFO_WITH_ICON,
        Dialog: LinkedDataBrowserDialog,
        formTemplateId: 'esc:User',
      }}
      entry={entry}
      nlsBundles={nlsBundles}
      descriptionItems={descriptionItems}
      sidebarActions={sidebarActions}
      returnTo="/admin/users"
      headerLabel={getLabel(entry)?.trim() || getUserRenderName(entry)}
    />
  );
};

UserOverview.propTypes = {
  overviewProps: overviewPropsPropType,
};

export default withOverviewModelProvider(UserOverview);
