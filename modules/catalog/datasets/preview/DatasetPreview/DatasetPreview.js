import { useEffect } from 'react';
import { Typography } from '@mui/material';
import PropTypes from 'prop-types';
import useRDFormsPresenter from 'commons/components/rdforms/hooks/useRDFormsPresenter';
import { getDescription, getParentCatalogEntry } from 'commons/util/metadata';
import { Entry } from '@entryscape/entrystore-js';
import { spreadEntry } from 'commons/util/store';
import { getLabel } from 'commons/util/rdfUtils';
import useAsyncCallback from 'commons/hooks/useAsyncCallback';
import useAsync from 'commons/hooks/useAsync';
import Lookup from 'commons/types/Lookup';
import useDistributions from 'catalog/datasets/DatasetOverview/hooks/useDistributions';
import {
  useVisualizations,
  withVisualizationsProvider,
} from 'catalog/datasets/DatasetOverview/hooks/useVisualizations';
import {
  getDistributionFormat,
  getDistributionTitle,
} from 'catalog/datasets/DatasetOverview/utils/distributions';
import {
  getVisualizationTitle,
  getChartType,
} from 'catalog/datasets/DatasetOverview/utils/visualizations';
import { ChartIcon } from 'catalog/datasets/DatasetOverview/VisualizationList';
// eslint-disable-next-line max-len
import PreviewVisualizationDialog from 'catalog/datasets/DatasetOverview/dialogs/PreviewVisualizationDialog';
import escaDatasetNLS from 'catalog/nls/escaDataset.nls';
import { useTranslation } from 'commons/hooks/useTranslation';
import {
  ListView,
  List,
  ListItem,
  ListItemText,
  withListModelProvider,
} from 'commons/components/ListView';
import LinkedDataBrowserDialog from 'commons/components/LinkedDataBrowserDialog';
import Info from '@mui/icons-material/Info';
import './DatasetPreview.scss';

const nlsBundles = [escaDatasetNLS];

const VisualizationIcon = ({ type }) => (
  <span className="escaDatasetPreview__listItemIcon">
    <ChartIcon type={type} />
  </span>
);

const InfoIcon = () => (
  <span className="escaDatasetPreview__listItemIcon">
    <Info />
  </span>
);

VisualizationIcon.propTypes = {
  type: PropTypes.string,
};

const PreviewSection = ({ children, title }) => {
  return (
    <div className="escaDatasetPreviewSection">
      {title ? (
        <Typography variant="h2" className="escsDatasetPreviewSection__header">
          {title}
        </Typography>
      ) : null}
      {children}
    </div>
  );
};

PreviewSection.propTypes = {
  children: PropTypes.node,
  title: PropTypes.string,
};

const CatalogsSection = ({ datasetEntry }) => {
  const [catalogEntry] = useAsyncCallback(getParentCatalogEntry, datasetEntry);
  const translate = useTranslation(nlsBundles);

  return catalogEntry ? (
    <PreviewSection title={translate('catalogListHeaderLabel')}>
      <ListView>
        <List>
          <ListItem
            action={{
              id: 'info',
              Dialog: LinkedDataBrowserDialog,
              entry: catalogEntry,
            }}
          >
            <ListItemText xs={11} primary={getLabel(catalogEntry)} />
            <ListItemText
              xs={1}
              secondary={
                <InfoIcon className="escaDatasetPreview__listItemIcon" />
              }
            />
          </ListItem>
        </List>
      </ListView>
    </PreviewSection>
  ) : null;
};

CatalogsSection.propTypes = {
  datasetEntry: PropTypes.instanceOf(Entry),
};

const DistributionsSection = withListModelProvider(({ datasetEntry }) => {
  const { distributions } = useDistributions(datasetEntry);
  const translate = useTranslation(nlsBundles);

  return distributions?.length ? (
    <PreviewSection title={translate('distributionsTitle')}>
      <ListView>
        <List>
          {distributions.map((distributionEntry) => (
            <ListItem
              key={`distr-${distributionEntry.getId()}`}
              action={{
                Dialog: LinkedDataBrowserDialog,
                entry: distributionEntry,
                parentEntry: datasetEntry,
                showReferences: false,
              }}
            >
              <ListItemText
                xs={8}
                primary={getDistributionTitle(distributionEntry, translate)}
              />
              <ListItemText
                xs={3}
                secondary={getDistributionFormat(distributionEntry, translate)}
              />
              <ListItemText
                xs={1}
                secondary={
                  <InfoIcon className="escaDatasetPreview__listItemIcon" />
                }
              />
            </ListItem>
          ))}
        </List>
      </ListView>
    </PreviewSection>
  ) : null;
});

DistributionsSection.propTypes = {
  datasetEntry: PropTypes.instanceOf(Entry),
};

const VisualizationsSection = withListModelProvider(({ datasetEntry }) => {
  const { visualizations } = useVisualizations(datasetEntry);
  const translate = useTranslation(nlsBundles);

  return visualizations?.length ? (
    <PreviewSection title={translate('visualizationsTitle')}>
      <ListView>
        <List>
          {visualizations.map((visualizationEntry) => (
            <ListItem
              key={`viz-${visualizationEntry.getId()}`}
              action={{
                Dialog: PreviewVisualizationDialog,
                entry: datasetEntry,
                visualizationEntry,
              }}
            >
              <ListItemText
                xs={11}
                primary={getVisualizationTitle(visualizationEntry, translate)}
              />
              <ListItemText
                xs={1}
                secondary={
                  <VisualizationIcon type={getChartType(visualizationEntry)} />
                }
              />
            </ListItem>
          ))}
        </List>
      </ListView>
    </PreviewSection>
  ) : null;
});

VisualizationsSection.propTypes = {
  datasetEntry: PropTypes.instanceOf(Entry),
};

const DatasetPreviewPresenter = ({ datasetEntry }) => {
  const { metadata, ruri } = spreadEntry(datasetEntry);
  const { data: templateId, runAsync } = useAsync();
  const filterPredicates = {
    'http://purl.org/dc/terms/title': true,
    'http://purl.org/dc/terms/description': true,
  };
  const { PresenterComponent } = useRDFormsPresenter(
    metadata,
    ruri,
    templateId,
    false,
    filterPredicates
  );

  useEffect(() => {
    runAsync(
      Lookup.inUse(datasetEntry).then((entityType) => {
        return datasetEntry.isLinkReference()
          ? entityType.complementaryTemplateId()
          : entityType.templateId();
      })
    );
  }, [datasetEntry, runAsync]);

  return PresenterComponent ? <PresenterComponent /> : null;
};

DatasetPreviewPresenter.propTypes = {
  datasetEntry: PropTypes.instanceOf(Entry),
};

const DatasetPreview = ({ entry }) => {
  const [description] = useAsyncCallback(getDescription, entry);

  return (
    <div className="escaDatasetPreview">
      <Typography className="escaDatasetPreview__header" variant="h1">
        {getLabel(entry)}
      </Typography>
      <PreviewSection>
        <Typography>{description}</Typography>
      </PreviewSection>
      <CatalogsSection datasetEntry={entry} />
      <DistributionsSection datasetEntry={entry} />
      <VisualizationsSection datasetEntry={entry} />
      <PreviewSection>
        <DatasetPreviewPresenter datasetEntry={entry} />
      </PreviewSection>
    </div>
  );
};

DatasetPreview.propTypes = {
  entry: PropTypes.instanceOf(Entry),
};

export default withVisualizationsProvider(DatasetPreview);
