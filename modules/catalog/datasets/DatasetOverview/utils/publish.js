import { namespaces as ns } from '@entryscape/rdfjson';
import { entrystore, entrystoreUtil } from 'commons/store';
import config from 'config';
import { hasAdminRights } from 'commons/util/user';
import { Entry } from '@entryscape/entrystore-js';
import { USERS_ENTRY_ID } from 'commons/util/userIds';
import { getDistributionFileEntries } from './distributions';

/**
 *
 * @param {Entry} entry
 * @returns {Array}
 */
const getDistributionStatements = (entry) =>
  entry.getMetadata().find(entry.getResourceURI(), 'dcat:distribution');

/**
 *
 * @param {Entry} entry
 * @returns {Promise}
 */
const getApiDistributionURIs = async (entry) => {
  const statements = getDistributionStatements(entry);

  const sourcePromises = statements.map((statement) => {
    const resourceUri = statement.getValue();
    return entrystoreUtil.getEntryByResourceURI(resourceUri).then(
      (distributionEntry) =>
        distributionEntry
          .getMetadata()
          .findFirstValue(
            distributionEntry.getResourceURI(),
            ns.expand('dcterms:source')
          ),
      () => {} // fail silently
    );
  });

  const sources = await Promise.all(sourcePromises);
  const apiDistributionURIs = sources.filter((source) => source);

  return apiDistributionURIs;
};

/**
 *
 * @param {acl} acl
 * @param {Entry} entry
 * @returns {Promise}
 */
const updateDistributionACL = async (acl, entry) => {
  const statements = getDistributionStatements(entry);
  if (statements) {
    return statements.map((statement) => {
      const resourceURI = statement.getValue();

      return entrystoreUtil
        .getEntryByResourceURI(resourceURI)
        .then(async (resourceEntry) => {
          if (acl) {
            const resourceEntryInfo = resourceEntry.getEntryInfo();
            resourceEntryInfo.setACL(acl);
            await resourceEntryInfo.commit();
            const fileEntries = await getDistributionFileEntries(resourceEntry);
            for (const fileEntry of fileEntries) {
              const entryInfo = fileEntry.getEntryInfo();
              entryInfo.setACL(acl);
              await entryInfo.commit();
            }
          }
        });
    });
  }
  return [];
};

/**
 *
 * @param {Entry} userEntry
 * @returns {boolean}
 */
export const getIsAllowedPublisher = (userEntry) => {
  const allowedFor = config.get('catalog.publishDatasetAllowedFor');
  const isAllowedUser =
    allowedFor === '_users' ||
    userEntry
      .getParentGroups()
      .includes(entrystore.getEntryURI('_principals', allowedFor));

  return isAllowedUser || hasAdminRights(userEntry);
};

/**
 * Sets and commits acl change for an entry.
 *
 * @param {Entry} entry
 * @param {Function} updateAcl - function to update the acl object
 * @param {boolean} isPrototype
 * @returns {Promise}
 */
const setAcl = async (entry, updateAcl, isPrototype = false) => {
  const entryInfo = entry.getEntryInfo();
  const acl = entryInfo.getACL(true);
  const newAcl = updateAcl(acl);
  entryInfo.setACL(newAcl);
  if (isPrototype) return;
  return Promise.all([
    entryInfo.commit(),
    updateDistributionACL(newAcl, entry),
  ]);
};

/**
 *
 * @param {Entry} entry
 * @returns {Promise<boolean>}
 */
export const publishDatasetInternally = async (entry) => {
  await setAcl(entry, (acl) => {
    acl.rread = [...acl.rread, USERS_ENTRY_ID];
    acl.mread = [...acl.mread, USERS_ENTRY_ID];
    return acl;
  });
  return true;
};

/**
 *
 * @param {Entry} entry
 * @returns {Promise<boolean>}
 */
export const unpublishDatasetInternally = async (entry) => {
  await setAcl(entry, (acl) => {
    acl.rread.splice(acl.rread.indexOf(USERS_ENTRY_ID), 1);
    acl.mread.splice(acl.mread.indexOf(USERS_ENTRY_ID), 1);
    return acl;
  });
  return false;
};

/**
 *
 * @param {Entry} entry
 * @returns {Promise<boolean>}
 */
export const publishDataset = async (entry) => {
  await setAcl(entry, () => {
    return {};
  });
  return true;
};

/**
 * Unpublish a dataset, but skip the commit if it's a prototype entry.
 *
 * @param {Entry} entry
 * @param {Entry} groupEntry
 * @param {boolean} isPrototype
 * @returns {Promise<boolean>}
 */
export const unpublishDataset = async (
  entry,
  groupEntry,
  isPrototype = false
) => {
  await setAcl(
    entry,
    (acl) => {
      const adminList = acl.admin || [];
      acl.admin = [...adminList, groupEntry.getId()];
      return acl;
    },
    isPrototype
  );
  return false;
};

/**
 *
 * @param {Entry} entry
 * @returns {Promise}
 */
export const getHasApiDistributions = async (entry) => {
  const apiDistributionURIs = await getApiDistributionURIs(entry);
  return Boolean(apiDistributionURIs.length);
};

/**
 *
 * @param {Entry} entry
 * @returns {boolean}
 */
export const getHasDistributions = (entry) =>
  Boolean(entry.getMetadata().find(null, 'dcat:distribution').length);
