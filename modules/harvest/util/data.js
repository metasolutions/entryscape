import { types, Entry, Group } from '@entryscape/entrystore-js';
import { namespaces, Graph } from '@entryscape/rdfjson';
import { entrystore } from 'commons/store';
import moment from 'moment';
import {
  Editor as RDFormsEditor,
  validate,
  engine,
} from '@entryscape/rdforms/renderers/react';
import { itemStore } from 'commons/rdforms/itemstore';
import { EntityType } from 'entitytype-lookup';
import { addMissingData } from './chart';

const STATS_VIEW_TO_BE_DELETED_TOPIC = 'registry.statistics.onremove';
const { CODES } = engine;

/**
 *
 * @param {string} contextId
 * @returns {Promise<number>}
 */
const getDatasetCount = async (contextId = null) => {
  const datasetEntriesSearchList = entrystore
    .newSolrQuery()
    .context(contextId)
    .rdfType('dcat:Dataset')
    .list();

  await datasetEntriesSearchList.getEntries();
  return datasetEntriesSearchList.getSize();
};

/**
 *
 * @param {string} contextId
 * @param {object} chartNLSBundle
 * @returns {Promise<{datasets: [{data: []}]}>}
 */
const loadDatasetsOverTime = async (contextId, chartNLSBundle) => {
  namespaces.add('st', 'http://entrystore.org/terms/statistics#');
  const statistics = await entrystore
    .newSolrQuery()
    .context('catalogstatistics')
    .rdfType('st:CatalogStatistics')
    .limit(30)
    .getEntries();

  let data = [];
  const successData = [];
  const failedData = [];

  const inContextPath =
    namespaces.expand('st:datasets_in_context_') + contextId;
  // eslint-disable-next-line array-callback-return
  statistics.map((entry) => {
    const metadata = entry.getMetadata();
    const resourceURI = entry.getResourceURI();
    const statements = metadata
      .find(resourceURI)
      .filter((stmt) => stmt.getPredicate() === inContextPath);
    let date = metadata.find(resourceURI, 'dcterms:date')[0].getValue();
    date = moment(date);

    statements.forEach((stmt) => {
      data.push({ x: date, y: stmt.getValue(), type: 'original' });
    });
  });

  data = addMissingData(data);
  // Slicing in case  addMissingData adds A LOT of data points.
  // addMissingData changes sorting order so we need the last-latest elements.

  data = data.slice(-30);

  data.forEach((item) => {
    if (item.type === 'original') {
      successData.push({ x: item.x, y: item.y, type: 'original' });
      failedData.push({ x: item.x, y: '0', type: 'inferred' });
    } else {
      successData.push({ x: item.x, y: '0', type: 'original' });
      failedData.push({ x: item.x, y: item.y, type: 'inferred' });
    }
  });

  return {
    datasets: [
      {
        label: chartNLSBundle.legendLabelSuccess,
        data: successData,
        stack: 'Stack',
      },
      {
        label: chartNLSBundle.legendLabelFailed,
        data: failedData,
        stack: 'Stack',
      },
    ],
  };
};

/**
 *
 * @param {string} contextId
 * @returns {Promise<Entry[]>}
 */
const loadJobEntries = async (contextId) => {
  const jobEntries = [];

  await entrystore
    .newSolrQuery()
    .graphType(types.GT_PIPELINERESULT)
    .context(contextId)
    .sort('created+desc')
    .forEach((entry) => {
      jobEntries.push(entry);
    });
  return jobEntries;
};

/**
 *
 * @param {string} contextId
 * @returns {Promise<{pipelineEntry: Entry[], pipelineResource: *}>}
 */
const getPipelineEntryAndResource = async (contextId) => {
  const pipelineEntry = await entrystore
    .newSolrQuery()
    .context(contextId)
    .graphType(types.GT_PIPELINE)
    .list()
    .getEntries(0)
    .then((entries) => (entries.length > 0 ? entries[0] : null));

  const pipelineResource = await pipelineEntry.getResource();
  return { pipelineEntry, pipelineResource };
};

/**
 *
 * @param {Entry} latestJobEntry
 * @param {Entry} pipelineEntry
 * @returns {boolean}
 */
const getPSIOrg = (latestJobEntry, pipelineEntry) => {
  if (latestJobEntry) {
    return (
      latestJobEntry
        .getMetadata()
        .find(null, 'dcterms:subject', { type: 'literal', value: 'psi' })
        .length > 0
    );
  }
  return (
    pipelineEntry
      .getMetadata()
      .findFirstValue(pipelineEntry.getResourceURI(), 'foaf:page') != null
  );
};

/**
 *
 * @param {Entry} entry
 * @param resource
 * @param isPSIOrg
 * @param nlsBundle
 * @returns {{data: {}, title: string}}
 */
const getPipelineData = (entry, resource, isPSIOrg, nlsBundle) => {
  const data = {};
  let title = '';
  if (entry) {
    if (isPSIOrg) {
      data[nlsBundle.orgId] =
        entry.getMetadata().findFirstValue(null, 'dcterms:identifier') ||
        nlsBundle.notFound;
      data[nlsBundle.psiPage] =
        resource.getTransformProperty('check', 'source') || nlsBundle.notFound;
    }
    data[nlsBundle.dcatAPI] =
      resource.getTransformProperty('fetch', 'source') || nlsBundle.notFound;

    // username
    const mdEntry = entry.getMetadata();
    const resourceURI = entry.getResourceURI();
    title = mdEntry.findFirstValue(null, 'dcterms:title');
    const name = mdEntry.findFirstValue(null, 'foaf:mbox');
    const psiTag =
      mdEntry.find(resourceURI, 'dcterms:subject', {
        type: 'literal',
        value: 'psi',
      }).length > 0;
    if (psiTag && name) {
      data[nlsBundle.userId] = name.substr(7);
    }
  }

  return {
    data,
    title,
  };
};

/**
 *
 * @param {string} contextId
 * @returns {object}
 */
const loadLinkCheckReport = async (contextId) => {
  const reportEntries = await entrystore
    .newSolrQuery()
    .rdfType('esterms:LinkCheckReport')
    .context(contextId)
    .limit(1)
    .getEntries(0);

  if (!reportEntries.length) return {};

  const reportEntry = reportEntries[0];
  const metadata = reportEntry.getMetadata();
  const resourceURI = reportEntry.getResourceURI();
  const resource = await reportEntry.getResource();
  const resourceContent = await resource.get();

  return {
    createdAt: metadata.findFirstValue(null, 'dcterms:created'),
    linkChecks: metadata.findFirstValue(null, 'esterms:linkChecks'),
    failedLinkChecks: metadata.findFirstValue(null, 'esterms:failedLinkChecks'),
    excludedLinkChecks: metadata.findFirstValue(
      null,
      'esterms:excludedLinkChecks'
    ),
    resource: resourceContent,
    resourceURI,
  };
};

/**
 *
 * @param {number} value
 * @param {number} total
 * @param {number} precision
 * @returns {number}
 */
const getPercentage = (value, total, precision) => {
  const multiplier = 10 ** (precision || 0);
  const percentage = (value / total) * 100;
  return Math.round(percentage * multiplier) / multiplier;
};

/**
 * Checks for the presence of a property in the metadata graph and updates the
 * provided counters.
 *
 * @param {Graph} metadata
 * @param {string} resourceURI
 * @param {string} property
 * @param {object} counters - hasProperty, multilingual, missingLanguage
 * @returns {[number, number, number ]} - updated counters
 */
const checkPresence = (metadata, resourceURI, property, counters) => {
  const languages = new Set();
  let missingLanguage = false;
  const statements = metadata.find(resourceURI, property);
  statements.forEach((statement) => {
    const language = statement.getLanguage();
    languages.add(language);
    if (!language) missingLanguage = true;
  });

  if (statements.length) counters.hasProperty += 1;
  if (languages.size > 1) counters.multilingual += 1;
  if (missingLanguage) counters.missingLanguage += 1;

  return [...Object.values(counters)];
};

/**
 * Gets an entry's validation errors and warnings.
 *
 * @param {Entry} entry
 * @param {Group} template
 * @returns {object}
 */
const getValidationResults = (entry, template) => {
  const graph = new Graph(entry.getAllMetadata().exportRDFJSON());
  const editor = new RDFormsEditor({
    graph,
    template,
    resource: entry.getResourceURI(),
  });
  const { errors, warnings } = validate.bindingReport(editor.binding);
  return { errors, warnings };
};

/**
 * Gets the number of missing mandatory fields based on the rdform editor's
 * validation results.
 *
 * @param {[]} errors
 * @returns {number}
 */
const getMissingMandatoryFieldsCount = (errors) =>
  errors.filter((error) => error.code === CODES.TOO_FEW_VALUES_MIN).length;

/**
 * Gets the number of missing recommended fields based on the rdform editor's
 * validation results.
 *
 * @param {[]} warnings
 * @returns {number}
 */
const getMissingRecommendedFieldsCount = (warnings) =>
  warnings.filter((warning) => warning.code === CODES.TOO_FEW_VALUES_PREF)
    .length;

/**
 * Calculates metadata fulfillment based on the provided counters.
 *
 * @param {number} entryCount
 * @param {number} fieldsCount
 * @param {number} missingFieldsCount
 * @returns {number}
 */
const getMetadataFulfillment = (
  entryCount,
  fieldsCount,
  missingFieldsCount
) => {
  const allFieldsCount = entryCount * fieldsCount;
  const filledFieldsCount = allFieldsCount - missingFieldsCount;
  return getPercentage(filledFieldsCount, allFieldsCount, 1);
};

/**
 * Returns the function that loads the statistics and encloses helper functions
 * and the 'continueCondition' variable that regulates whether the solr queries
 * should continue fetching or stop.
 *
 * @param {Function} continueCondition
 * @returns {Function}
 */
const getLoadStatistics = (continueCondition) => {
  /**
   * Calculates quality metrics over all contexts/orgs by default. If there's a
   * contextId only does so for that specific context.
   *
   * @param {Set<string>} relevantContextIds
   * @param {string} contextId
   * @returns {object}
   */
  const getReport = async (relevantContextIds, contextId = null) => {
    const publishers = {};
    const contactpoints = {};
    const formats = {};
    const licenses = {};
    let datasetsCount = 0;
    let hasTitle = 0;
    let multilingualTitle = 0;
    let missingLanguageOnTitle = 0;
    let hasDescription = 0;
    let multilingualDescription = 0;
    let missingLanguageOnDescription = 0;
    let hasPublisher = 0;
    let hasTheme = 0;
    let hasContactPoint = 0;
    const nrOfDistributionsPerDataset = {};

    let distributionCount = 0;
    let hasDistributionFormat = 0;
    let hasDistributionLicense = 0;
    let hasDownloadURL = 0;
    let hasAccessURL = 0;
    let hasDistributionTitle = 0;
    let multilingualDistributionTitle = 0;
    let missingLanguageOnDistributionTitle = 0;
    let hasDistributionDescription = 0;
    let multilingualDistributionDescription = 0;
    let missingLanguageOnDistributionDescription = 0;
    let lgThan5DistributionsPerDataset = 0;

    const datasetTemplate = itemStore.getItem('dcat:Dataset');
    const datasetMandatoryFieldsCount = datasetTemplate
      .getChildren()
      .filter((field) => field.getCardinality().min > 0).length;
    const datasetRecommendedFieldsCount = datasetTemplate
      .getChildren()
      .filter((field) => {
        const cardinality = field.getCardinality();
        // Some fields like 'publisher' erroneously have pref > 0 even though they are mandatory
        return cardinality.pref > 0 && !cardinality.min > 0;
      }).length;
    let datasetMissingMandatoryFieldsCount = 0;
    let datasetMissingRecommendedFields = 0;

    await entrystore
      .newSolrQuery()
      .rdfType('dcat:Dataset')
      .context(contextId)
      .publicRead(true)
      .limit(100)
      .forEach((datasetEntry) => {
        const { errors: datasetEntryErrors, warnings: datasetEntryWarnings } =
          getValidationResults(datasetEntry, datasetTemplate);
        datasetMissingMandatoryFieldsCount +=
          getMissingMandatoryFieldsCount(datasetEntryErrors);
        datasetMissingRecommendedFields +=
          getMissingRecommendedFieldsCount(datasetEntryWarnings);

        const datasetContextId = datasetEntry.getContext().getId();
        if (!relevantContextIds.has(datasetContextId)) {
          return Promise.resolve(true);
        }

        datasetsCount += 1;
        const resourceURI = datasetEntry.getResourceURI();
        const metadata = datasetEntry.getAllMetadata();

        // Check title
        [hasTitle, multilingualTitle, missingLanguageOnTitle] = checkPresence(
          metadata,
          resourceURI,
          'dcterms:title',
          {
            hasProperty: hasTitle,
            multilingual: multilingualTitle,
            missingLanguage: missingLanguageOnTitle,
          }
        );

        // Check Description
        [
          hasDescription,
          multilingualDescription,
          missingLanguageOnDescription,
        ] = checkPresence(metadata, resourceURI, 'dcterms:description', {
          hasProperty: hasDescription,
          multilingual: multilingualDescription,
          missingLanguage: missingLanguageOnDescription,
        });

        // Publishers
        const dPublishers = metadata.find(resourceURI, 'dcterms:publisher');
        if (dPublishers.length > 0) {
          hasPublisher += 1;
          dPublishers.forEach(
            // eslint-disable-next-line no-return-assign
            (p) => (publishers[p.getValue()] = datasetContextId)
          );
        }

        // ContactPoints
        const dContactPoints = metadata.find(resourceURI, 'dcat:contactPoint');
        if (dContactPoints.length > 0) {
          hasContactPoint += 1;
          dContactPoints.forEach(
            // eslint-disable-next-line no-return-assign
            (c) => (contactpoints[c.getValue()] = datasetContextId)
          );
        }

        // Theme
        const dThemes = metadata.find(resourceURI, 'dcat:theme');
        if (dThemes.length > 0) {
          hasTheme += 1;
        }

        const dists = metadata.find(resourceURI, 'dcat:distribution');
        nrOfDistributionsPerDataset[dists.length] =
          (nrOfDistributionsPerDataset[dists.length] || 0) + 1;

        return Promise.resolve(continueCondition()); // false, stops the forEach loop
      });

    const datasetsMandatoryFulfillment = getMetadataFulfillment(
      datasetsCount,
      datasetMandatoryFieldsCount,
      datasetMissingMandatoryFieldsCount
    );

    const datasetsRecommendedFulfillment = getMetadataFulfillment(
      datasetsCount,
      datasetRecommendedFieldsCount,
      datasetMissingRecommendedFields
    );

    const distributionTemplate = itemStore.getItem('dcat:Distribution');
    const distributionMandatoryFieldsCount = distributionTemplate
      .getChildren()
      .filter((field) => field.getCardinality().min > 0).length;
    const distributionRecommendedFieldsCount = distributionTemplate
      .getChildren()
      .filter((field) => {
        const cardinality = field.getCardinality();
        return cardinality.pref > 0 && !cardinality.min > 0;
      }).length;
    let distributionMissingMandatoryFieldsCount = 0;
    let distributionMissingRecommendedFieldsCount = 0;

    await entrystore
      .newSolrQuery()
      .rdfType('dcat:Distribution')
      .context(contextId)
      .publicRead(true)
      .limit(100)
      .forEach((distributionEntry) => {
        const {
          errors: distributionEntryErrors,
          warnings: distributionEntryWarnings,
        } = getValidationResults(distributionEntry, distributionTemplate);
        distributionMissingMandatoryFieldsCount +=
          getMissingMandatoryFieldsCount(distributionEntryErrors);
        distributionMissingRecommendedFieldsCount +=
          getMissingRecommendedFieldsCount(distributionEntryWarnings);

        const distributionContextId = distributionEntry.getContext().getId();
        if (!relevantContextIds.has(distributionContextId)) {
          return Promise.resolve(true);
        }

        distributionCount += 1;
        const resourceURI = distributionEntry.getResourceURI();
        const metadata = distributionEntry.getAllMetadata();

        // Licenses
        const license = metadata.findFirstValue(resourceURI, 'dcterms:license');
        if (license) {
          hasDistributionLicense += 1;
          const fURI = license.replace(/\/$/, '');
          licenses[fURI] = (licenses[fURI] || 0) + 1;
        }

        // downloadURL
        const downloadURL = metadata.find(resourceURI, 'dcat:downloadURL');
        if (downloadURL.length > 0) {
          hasDownloadURL += 1;
        }

        // accessURL
        const accessURL = metadata.find(resourceURI, 'dcat:accessURL');
        if (accessURL.length > 0) {
          hasAccessURL += 1;
        }

        // Formats
        const format = metadata.findFirstValue(resourceURI, 'dcterms:format');
        if (format) {
          hasDistributionFormat += 1;
          const mt = format.trim();
          formats[mt] = (formats[mt] || 0) + 1;
        }

        // Check title
        [
          hasDistributionTitle,
          multilingualDistributionTitle,
          missingLanguageOnDistributionTitle,
        ] = checkPresence(metadata, resourceURI, 'dcterms:title', {
          hasProperty: hasDistributionTitle,
          multilingual: multilingualDistributionTitle,
          missingLanguage: missingLanguageOnDistributionTitle,
        });

        // Check description
        [
          hasDistributionDescription,
          multilingualDistributionDescription,
          missingLanguageOnDistributionDescription,
        ] = checkPresence(metadata, resourceURI, 'dcterms:description', {
          hasProperty: hasDistributionDescription,
          multilingual: multilingualDistributionDescription,
          missingLanguage: missingLanguageOnDistributionDescription,
        });

        return Promise.resolve(continueCondition());
      });

    const distributionsMandatoryFulfillment = getMetadataFulfillment(
      distributionCount,
      distributionMandatoryFieldsCount,
      distributionMissingMandatoryFieldsCount
    );

    const distributionsRecommendedFulfillment = getMetadataFulfillment(
      distributionCount,
      distributionRecommendedFieldsCount,
      distributionMissingRecommendedFieldsCount
    );

    lgThan5DistributionsPerDataset = Object.keys(nrOfDistributionsPerDataset)
      .filter((key) => key > 5)
      .reduce((sum, d) => sum + nrOfDistributionsPerDataset[d], 0);

    return {
      totalDatasetCount: datasetsCount,
      hasTitle,
      multilingualTitle,
      missingLanguageOnTitle,
      hasDescription,
      multilingualDescription,
      missingLanguageOnDescription,
      hasPublisher,
      hasTheme,
      hasContactPoint,
      datasetsMandatoryFulfillment,
      datasetsRecommendedFulfillment,
      totalDistributionCount: distributionCount,
      hasDistributionTitle,
      multilingualDistributionTitle,
      missingLanguageOnDistributionTitle,
      hasDistributionDescription,
      multilingualDistributionDescription,
      missingLanguageOnDistributionDescription,
      hasDownloadURL,
      hasAccessURL,
      hasDistributionFormat,
      hasDistributionLicense,
      distributionsMandatoryFulfillment,
      distributionsRecommendedFulfillment,
      nrOfContactPoints: Object.keys(contactpoints).length,
      nrOfPublishers: Object.keys(publishers).length,
      nrOfDistributionsPerDataset,
      formats,
      licenses,
      lgThan5DistributionsPerDataset,
    };
  };

  /**
   * Loads dataset and distribution stats for either a single or all contexts.
   * If the number of datasets is huge returns false the first time (decided by checkNumOfDatasets).
   *
   * @param {*} contextId
   // * @param {*} checkNumOfDatasets
   * @returns  {Promise<Set>}
   */
  return async (contextId) => {
    const relevantContextIds = new Set();
    if (contextId) {
      // We only need to load label for contextId
      const contextEntry = await entrystore
        .getContextById(contextId)
        .getEntry();
      relevantContextIds.add(contextEntry.getId());
    } else {
      // We need labels for all contexts which have datasets, which we approximate
      // to be all contexts.
      await entrystore
        .newSolrQuery()
        .graphType(types.GT_CONTEXT)
        .limit(100)
        .forEach((ce) => {
          if (ce.isPublic()) {
            relevantContextIds.add(ce.getId());
          }
          return Promise.resolve(continueCondition());
        });
    }

    if (continueCondition()) {
      return getReport(relevantContextIds, contextId);
    }

    return relevantContextIds;
  };
};

/**
 *
 * @param {EntityType} entityType
 * @returns {boolean}
 */
const checkIsDcatEntityType = (entityType) => {
  return entityType?.get('tags')?.includes('dcat');
};

export {
  checkIsDcatEntityType,
  getPercentage,
  getDatasetCount,
  getLoadStatistics,
  loadDatasetsOverTime,
  loadJobEntries,
  getPipelineEntryAndResource,
  getPSIOrg,
  getPipelineData,
  loadLinkCheckReport,
  STATS_VIEW_TO_BE_DELETED_TOPIC,
};
