import { createEntry } from 'commons/util/store';
import { i18n } from 'esi18n';
import { entrystore } from 'commons/store';
import { updateContextTitle, setGroupIdForContext } from 'commons/util/context';
import { RDF_PROPERTY_PROJECTTYPE } from 'entitytype-lookup';

export const createPublisher = async (homeContext, publisher) => {
  try {
    const publisherEntry = homeContext.newNamedEntry();
    const publisherMetadata = publisherEntry.getMetadata();
    const publisherResourceRURI = publisherEntry.getResourceURI();
    const language = i18n.getLocale();
    publisherMetadata.add(publisherResourceRURI, 'rdf:type', 'foaf:Agent');
    publisherMetadata.addL(
      publisherResourceRURI,
      'foaf:name',
      publisher.value.trim(),
      language
    );
    return publisherEntry.commit();
  } catch (error) {
    throw Error(`Unable to create publisher: ${error}`);
  }
};

// Make the context a esterms:CatalogContext
const updateHomeContextEntryInfo = async (
  contextEntry,
  groupEntry,
  projectType
) => {
  try {
    const homeContextEntryInfo = contextEntry.getEntryInfo();
    const homeContextResourceURI = contextEntry.getResourceURI();
    homeContextEntryInfo
      .getGraph()
      .add(homeContextResourceURI, 'rdf:type', 'esterms:CatalogContext');

    // If project type is set, add it to entryinfo
    if (projectType) {
      homeContextEntryInfo
        .getGraph()
        .add(homeContextResourceURI, RDF_PROPERTY_PROJECTTYPE, projectType);
    }

    // access to homecontext metadata by default.
    const acl = homeContextEntryInfo.getACL(true);
    acl.mread.push(groupEntry.getId());
    homeContextEntryInfo.setACL(acl);
    await homeContextEntryInfo.commit();
  } catch (error) {
    throw Error(
      `There was some error while trying to set access to the home context metadata ${error}`
    );
  }
};

/**
 *
 * @param {Object} catalogCreateOptions
 * @returns {Promise}
 */
export const createCatalogEntry = async ({
  homeContext,
  homeContextId,
  rdfType,
  metadata,
  userEntry,
  userInfo,
  groupEntry,
  contextEntry,
  projectType,
}) => {
  try {
    const { name, description, publisherResourceRURI } = metadata;
    const prototypeEntry = createEntry(homeContext, rdfType);
    const prototypeMetadata = prototypeEntry.getMetadata();
    const prototypeResourceURI = prototypeEntry.getResourceURI();
    const language = i18n.getLocale();

    prototypeMetadata.add(prototypeResourceURI, 'rdf:type', rdfType);
    prototypeMetadata.addL(
      prototypeResourceURI,
      'dcterms:title',
      name,
      language
    );
    prototypeMetadata.addL(
      prototypeResourceURI,
      'dcterms:description',
      description,
      language
    );

    // If publisher required, add it to metadata
    if (publisherResourceRURI)
      prototypeMetadata.add(
        prototypeResourceURI,
        'dcterms:publisher',
        publisherResourceRURI
      );

    const catalogEntry = await prototypeEntry.commit();
    await updateContextTitle(catalogEntry);
    userEntry.setRefreshNeeded();
    userEntry.refresh();

    await updateHomeContextEntryInfo(contextEntry, groupEntry, projectType);
    if (!userInfo.hasAdminRights) {
      setGroupIdForContext(homeContextId, groupEntry.getId());
    }
  } catch (error) {
    throw Error(`Failed to create catalog: ${error}`);
  }
};

/**
 * Checks whether a context (catalog) has api distributions
 *
 * @param {Context} context
 * @returns {Promise}
 */
export const getApiDistributions = (context) =>
  entrystore
    .newSolrQuery()
    .context(context)
    .rdfType('dcat:Distribution')
    .uriProperty('dcterms:source', '*')
    .limit(1)
    .list()
    .getEntries();
