import PropTypes from 'prop-types';
import { graphPropType } from 'commons/util/entry';
import ArrowDownwardIcon from '@mui/icons-material/ArrowDownward';
import ArrowUpwardIcon from '@mui/icons-material/ArrowUpward';
import { Grid } from '@mui/material';
import { useTranslation } from 'commons/hooks/useTranslation';
import escoFormsNls from 'commons/nls/escoForms.nls';
import { Field } from '../Field';
import { Fields } from '../Fields';
import { Select } from '../Select';
import { FieldControlButton } from '../FieldControl';
import { expandChoices } from '../utils/choices';
import { URI } from '../../utils/nodetypes';
import { useOrderedFieldsEdit } from '../hooks/useOrderedFieldsEdit';

export const OrderedFieldsEditor = ({
  size,
  choices,
  property,
  graph,
  resourceURI,
  ...fieldsProps
}) => {
  const { addField, fieldGroup, labelId, removeField, moveField, updateValue } =
    useOrderedFieldsEdit({ property, graph, resourceURI, ...fieldsProps });
  const t = useTranslation(escoFormsNls);
  const fields = fieldGroup.getFields();

  return (
    <Fields labelId={labelId} addField={addField} {...fieldsProps}>
      {fields.map((field, index) => {
        return (
          <Field
            key={field.getId()}
            size={fields.length}
            onRemove={() => removeField(field)}
            onClear={() => updateValue(field, '')}
          >
            <Select
              labelId={labelId}
              value={field.getValue()}
              size={size}
              choices={expandChoices(choices, URI)}
              onChange={({ target }) => updateValue(field, target.value)}
            />
            <Grid item xs={4}>
              {field.getOrder() > 1 ? (
                <FieldControlButton
                  marginLeft="0"
                  tooltip={t('moveUpTooltip')}
                  onClick={() => moveField(field, index, index - 1)}
                >
                  <ArrowUpwardIcon />
                </FieldControlButton>
              ) : null}
              {field.getOrder() < fields.length ? (
                <FieldControlButton
                  marginLeft="0"
                  tooltip={t('moveDownTooltip')}
                  onClick={() => moveField(field, index, index + 1)}
                >
                  <ArrowDownwardIcon />
                </FieldControlButton>
              ) : null}
            </Grid>
          </Field>
        );
      })}
    </Fields>
  );
};

OrderedFieldsEditor.propTypes = {
  size: PropTypes.string,
  nodetype: PropTypes.string,
  choices: PropTypes.arrayOf(PropTypes.shape({})),
  property: PropTypes.string,
  graph: graphPropType,
  resourceURI: PropTypes.string,
};
