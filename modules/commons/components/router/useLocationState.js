import { useCallback, useRef } from 'react';
import { useLocation } from 'react-router-dom';
import useNavigate from 'commons/components/router/useNavigate';

const useLocationState = () => {
  const location = useLocation();
  const { navigate } = useNavigate();
  const locationStateRef = useRef(location.state);

  const setLocationState = useCallback(
    (newState) => {
      const newLocationState = { ...locationStateRef.current, ...newState };
      locationStateRef.current = newLocationState;
      navigate(location.pathname, {
        state: newLocationState,
        replace: true,
      });
    },
    [navigate, location.pathname]
  );

  const clearLocationState = useCallback(() => {
    locationStateRef.current = {};
    navigate(location.pathname, {});
  }, [navigate, location.pathname]);

  return {
    locationState: location.state,
    clearLocationState,
    setLocationState,
  };
};

export default useLocationState;
