import { utils as rdformsUtils } from '@entryscape/rdforms';
import { Graph } from '@entryscape/rdfjson';
import config from 'config';
import { truncate } from 'commons/util/util';

const INVISIBLE = 'invisible';
const INVISIBLE_GROUP = 'invisibleGroup';

/**
 * Returns the graph and resource URI whether the first param is an Entry or Graph.
 *
 * Using getAllMetadata to merge both local and external graphs, avoiding precedence
 * issues e.g. between labels. Without merging the graphs, getLabel (which uses fixEoG)
 * would return the altLabel if there was one in the local metadata while only having
 * a prefLabel on the external e.g. from an enhanced-only concept.
 *
 * @param {Entry|Graph} eog
 * @param {string} uri
 * @returns {Object}
 */
const fixEoG = (entryOrGraph, uri) => {
  const isGraph = Boolean(entryOrGraph.exportRDFJSON);
  if (isGraph) return { g: entryOrGraph, r: uri };

  // eslint-disable-next-line no-use-before-define
  return { g: getAllMetadata(entryOrGraph), r: entryOrGraph.getResourceURI() };
};

let labelProperties = [];
let descriptionProperties = [];

const setLabelProperties = (arr) => {
  labelProperties = arr;
};
const setDescriptionProperties = (arr) => {
  descriptionProperties = arr;
};

/**
 * Returns the label of an entry or graph, truncating it if necessary
 *
 * @param {Entry|Graph} entryOrGraph
 * @param {string} uri
 * @param {object} properties
 * @returns {string}
 */
const getLabel = (entryOrGraph, uri, properties = labelProperties) => {
  const { g: graph, r: resourceURI } = fixEoG(entryOrGraph, uri);
  const label = rdformsUtils.getLocalizedValue(
    rdformsUtils.getLocalizedMap(graph, resourceURI, properties)
  ).value;
  return truncate(label);
};

/**
 * Returns the label of an entry or graph, without truncation
 *
 * @param {Entry|Graph} entryOrGraph
 * @param {string} uri
 * @returns {string}
 */
const getFullLengthLabel = (
  entryOrGraph,
  uri,
  properties = labelProperties
) => {
  const { g: graph, r: resourceURI } = fixEoG(entryOrGraph, uri);
  const label = rdformsUtils.getLocalizedValue(
    rdformsUtils.getLocalizedMap(graph, resourceURI, properties)
  ).value;
  return label;
};

const getLabelMap = (eog, uri) => {
  const { g, r } = fixEoG(eog, uri);
  return rdformsUtils.getLocalizedMap(g, r, labelProperties);
};

const getDescription = (eog, uri) => {
  const { g, r } = fixEoG(eog, uri);
  return rdformsUtils.getLocalizedValue(
    rdformsUtils.getLocalizedMap(g, r, descriptionProperties)
  ).value;
};

const getDescriptionMap = (eog, uri) => {
  const { g, r } = fixEoG(eog, uri);
  return rdformsUtils.getLocalizedMap(g, r, descriptionProperties);
};

const getRenderName = (entry) =>
  getLabel(entry) || entry.getResource(true)?.getName?.();

/**
 *
 * @param {Entry|Graph} entryOrGraph
 * @param {string} uri
 * @param {string[]} properties
 * @returns {string}
 */
const getLocalizedValue = (entryOrGraph, uri, properties) => {
  const { g: graph, r: resourceURI } = fixEoG(entryOrGraph, uri);
  return rdformsUtils.getLocalizedValue(
    rdformsUtils.getLocalizedMap(graph, resourceURI, properties)
  ).value;
};

/**
 * Identifies the sub-editor corresponding to a child binding.
 * @param {Rdforms/Editor} editor
 * @param {Rdforms/Binding} childBinding
 * @returns {Rdforms/Editor}
 */
const getSubEditor = (editor, childBinding) =>
  editor._subEditors.find(
    (subEditor) => subEditor.getBinding() === childBinding.getParent()
  );

/**
 * Identifies navigation items (their ids, labels and whether they are sub-bindings)
 * given an RDForms editor.
 * @param {Rdforms/Editor} editor
 * @returns Object[]
 */
const getNavigationItemsFromEditor = (editor) => {
  if (!editor) return [];

  const filterPredicates = editor.filterPredicates || {};

  const editorBindings = editor.binding
    .getChildBindings()
    .filter((binding) => !filterPredicates[binding.getPredicate()]);

  // Removes duplicates, for some reason e.g. `Title` exists twice
  const navigationItemBindings = [
    ...new Map(
      editorBindings.map((binding) => [binding.getItem()._internalId, binding])
    ).values(),
  ];

  // Also grabs sub-bindings, e.g `Time` has children `Start` and `End`
  const navigationItems = navigationItemBindings?.reduce(
    (allBindings, binding) => {
      const item = binding.getItem();
      const isVisible = !(
        item.hasStyle(INVISIBLE) || item.hasStyle(INVISIBLE_GROUP)
      );

      if (isVisible) {
        // Add data about the main binding
        allBindings.push({
          id: editor.getLabelIndex(binding),
          label: item.getLabel(),
          editLabel: item.getEditLabel(),
          binding,
        });
      }

      if (item.getType() !== 'group') return allBindings;

      // If the main binding is a group, also add the child bindings
      const childBindings = binding
        .getChildBindings()
        .filter((childBinding) => !childBinding.getItem().hasStyle(INVISIBLE))
        .map((childBinding) => {
          // The sub-editor is necessary to correctly identify the ID of the child bindings
          // TODO label sometimes is two or more levels down, so childBinding.getItem().getLabel() can be undefined
          const subEditor = getSubEditor(editor, childBinding);
          return {
            id: subEditor.getLabelIndex(childBinding),
            label: childBinding.getItem().getLabel(),
            indented: !item.hasStyle(INVISIBLE_GROUP),
            editLabel: childBinding.getItem().getEditLabel(),
            binding: childBinding,
          };
        });
      const uniqueChildBindings = [
        ...new Map(
          childBindings.map((childBinding) => [childBinding.id, childBinding])
        ).values(),
      ];
      allBindings.push(...uniqueChildBindings);

      return allBindings;
    },
    []
  );

  return navigationItems;
};

/**
 * Gets a graph with both the local and external metadata of an entry.
 *
 * @param {Entry} entry
 * @returns {Graph}
 */
const getAllMetadata = (entry) => {
  const metadata = entry.getMetadata();

  // If the entry is NOT a reference, get the local metadata graph
  if (!entry.isReference() && !entry.isLinkReference()) return metadata;

  // If the local metadata graph is empty, get the cached external metadata graph
  if (metadata.isEmpty()) return entry.getCachedExternalMetadata();

  // If the entry has both local and cached external metadata, get a graph with both
  const graph = new Graph(metadata.exportRDFJSON());
  graph.addAll(entry.getCachedExternalMetadata(), 'external');
  return graph;
};

/**
 * To be used in conjunction with getAllMetadata. Removes the cached external
 * metadata from the graph, i.e. all statements that have named graphs given.
 *
 * @param {Graph} graph the graph with potential mix of local and external metadata
 * @param {Entry} entry optional, if provided we check if it is a link reference or reference,
 * if not we assume we do not need to clean the graph
 * @returns {Graph}
 */
const getLocalMetadata = (graph, entry) => {
  if (entry && !entry.isLinkReference() && !entry.isReference()) return graph;

  const localMetadataGraph = new Graph();
  graph.find().forEach((statement) => {
    if (!statement.getNamedGraph()) {
      localMetadataGraph.add(statement);
    }
  });

  return localMetadataGraph;
};

export {
  setLabelProperties,
  setDescriptionProperties,
  getDescription,
  getDescriptionMap,
  getLabel,
  getFullLengthLabel,
  getLabelMap,
  getLocalizedValue,
  getRenderName,
  getNavigationItemsFromEditor,
  getAllMetadata,
  getLocalMetadata,
};

export default () => {
  setLabelProperties(config.get('rdf.labelProperties'));
  setDescriptionProperties(config.get('rdf.descriptionProperties'));
};
