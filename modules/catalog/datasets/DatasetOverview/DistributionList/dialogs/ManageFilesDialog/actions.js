import { ACTION_REMOVE } from 'commons/actions';
import escaManageFilesNLS from 'catalog/nls/escaManageFiles.nls';
import escoListNLS from 'commons/nls/escoList.nls';
import escaFilesListNLS from 'catalog/nls/escaFilesList.nls';
import AddFileDialog from '../AddFileDialog';
import ReplaceFileDialog from '../ReplaceFileDialog';
import RemoveFileDialog from '../RemoveFileDialog';
import DownloadFile from '../../DownloadFile';

export const nlsBundles = [escoListNLS, escaManageFilesNLS, escaFilesListNLS];

export const listActions = [
  {
    id: 'add-file',
    Dialog: AddFileDialog,
    labelNlsKey: 'addFile',
  },
];

export const rowActions = [
  {
    id: 'replace-file',
    Dialog: ReplaceFileDialog,
    labelNlsKey: 'replaceMenu',
  },
  {
    id: 'download',
    Dialog: DownloadFile, // download does not have a dialogue
    labelNlsKey: 'downloadButtonTitle',
  },
  {
    ...ACTION_REMOVE,
    Dialog: RemoveFileDialog,
    labelNlsKey: 'removeFileLabel',
  },
];
