import {
  createContext,
  useContext,
  useState,
  useCallback,
  useMemo,
} from 'react';
import PropTypes from 'prop-types';
import { Snackbar, SnackbarContent, IconButton } from '@mui/material';
import CloseIcon from '@mui/icons-material/Close';
import SlideTransition from 'commons/components/common/SlideTransition';
import {
  error as errorColour,
  white as whiteColour,
} from 'commons/theme/mui/colors';
import { useTranslation } from 'commons/hooks/useTranslation';
import escoDialogsNLS from 'commons/nls/escoDialogs.nls';

export const SUCCESS_EDIT = 'success_edit';
export const ERROR = 'error';

const getDefaultMessageNls = (type) =>
  ({
    [SUCCESS_EDIT]: 'editSuccess',
    [ERROR]: 'defaultError',
  }[type]);

const SnackbarContext = createContext();

const useSnackbar = () => {
  const snackbarContext = useContext(SnackbarContext);

  if (snackbarContext === undefined) {
    throw new Error('useSnackbar must be used within a context provider');
  }

  return snackbarContext;
};

const DEFAULT_DURATION = 5000;
const ERROR_DURATION = 8000;
const NLS_BUNDLES = [escoDialogsNLS];

const SnackbarProvider = ({ children }) => {
  const [snacks, setSnacks] = useState([]);
  const translate = useTranslation(NLS_BUNDLES);

  /**
   * Adds a snackbar notification to the snackbar queue.
   *
   * @param {object} newSnackProps - An object containing MUI Snackbar props
   */
  const addSnackbar = useCallback(
    ({ message, type, ...restNewSnackProps }) => {
      setSnacks((previousSnacks) => [
        ...previousSnacks,
        {
          open: true,
          message: message || translate(getDefaultMessageNls(type)),
          error: type === ERROR,
          key: `${message}-${Math.random()}`,
          ...restNewSnackProps,
        },
      ]);
    },
    [translate]
  );

  const closeVisibleSnackbar = () => {
    const [snack, ...otherSnacks] = snacks;
    const closingSnack = { ...snack, open: false };
    setSnacks([closingSnack, ...otherSnacks]);
  };

  const handleClose = (_event, reason) => {
    if (reason === 'clickaway') {
      return;
    }

    closeVisibleSnackbar();
  };

  const currentSnack = snacks?.[0];
  const closeAction = (
    <IconButton
      size="small"
      aria-label="close"
      onClick={handleClose}
      style={{ color: whiteColour.main }}
    >
      <CloseIcon />
    </IconButton>
  );

  const providerValue = useMemo(() => {
    return [addSnackbar];
  }, [addSnackbar]);

  return (
    <SnackbarContext.Provider value={providerValue}>
      {children}
      {currentSnack ? (
        <Snackbar
          anchorOrigin={{ vertical: 'bottom', horizontal: 'left' }}
          style={{
            marginLeft: '120px',
          }}
          onClose={handleClose}
          TransitionProps={{
            onExited: () => {
              setSnacks(snacks.slice(1));
            },
          }}
          autoHideDuration={
            currentSnack.error ? ERROR_DURATION : DEFAULT_DURATION
          }
          TransitionComponent={SlideTransition}
          open={currentSnack.open}
          key={currentSnack.key}
        >
          <SnackbarContent
            message={currentSnack.message}
            action={closeAction}
            style={currentSnack.error ? { background: errorColour.main } : {}}
          />
        </Snackbar>
      ) : null}
    </SnackbarContext.Provider>
  );
};

SnackbarProvider.propTypes = {
  children: PropTypes.node,
};

export default useSnackbar;
export { useSnackbar, SnackbarProvider };
