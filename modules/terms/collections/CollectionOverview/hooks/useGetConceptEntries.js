import { useEffect, useState, useCallback } from 'react';
import { spreadEntry } from 'commons/util/store';
import { entrystoreUtil } from 'commons/store';
import useAsync from 'commons/hooks/useAsync';
import useRemoteStore from './useRemoteStore';

const getMemberRURIs = (collectionEntry) => {
  const { metadata: collectionMetadata, collectionRURI } =
    spreadEntry(collectionEntry);
  return collectionMetadata
    .find(collectionRURI, 'skos:member')
    .map((statement) => statement.getValue());
};

const getRemoteConceptEntries = async (
  memberRURIs,
  entriesPerforated,
  remoteStoreUtil
) => {
  const remoteEntriesMask = entriesPerforated.map((entry) => !entry);
  if (!remoteEntriesMask.includes(true)) return;

  const remoteRURIs = memberRURIs.filter(
    (_uri, index) => remoteEntriesMask[index]
  );
  const remoteEntries = await remoteStoreUtil.loadEntriesByResourceURIs(
    remoteRURIs,
    null,
    true // also accept missing on remote store, might be a concept that has been removed
  );
  return remoteEntries.filter((entry) => entry);
};

const getConceptEntries = async (collectionEntry, remoteStoreUtil) => {
  const memberRURIs = getMemberRURIs(collectionEntry);

  // Last param adds null holes to the returned array for entries that were not found
  // allowing to look for them on the other entrystore
  const entriesPerforated = await entrystoreUtil.loadEntriesByResourceURIs(
    memberRURIs,
    null,
    true
  );
  const localConceptEntries = entriesPerforated.filter((entry) => entry);

  if (!remoteStoreUtil) return localConceptEntries;
  const remoteEntries = await getRemoteConceptEntries(
    memberRURIs,
    entriesPerforated,
    remoteStoreUtil
  );
  if (!remoteEntries) return localConceptEntries;

  return [...localConceptEntries, ...remoteEntries];
};

const useGetConceptEntries = (collectionEntry) => {
  const { remoteStoreUtil } = useRemoteStore();

  const { data: conceptEntries, runAsync, status } = useAsync({ data: [] });
  const [refreshCount, setRefreshCount] = useState(0);

  useEffect(() => {
    runAsync(getConceptEntries(collectionEntry, remoteStoreUtil));
  }, [runAsync, collectionEntry, remoteStoreUtil, refreshCount]);

  const onConceptsChange = useCallback(
    () => setRefreshCount((previousCount) => previousCount + 1),
    []
  );

  return [conceptEntries, onConceptsChange, status];
};

export default useGetConceptEntries;
