import { useEffect } from 'react';
import PropTypes from 'prop-types';
import { graphPropType } from 'commons/util/entry';
import useAsync from 'commons/hooks/useAsync';
import { entrystore } from 'commons/store';
import { getPathFromViewName } from 'commons/util/site';
import { Link as LinkPresenter } from 'commons/components/forms/presenters';

const FORM_VIEW_NAME = 'models__forms__form';

export const ObjectFormLink = ({ graph, resourceURI, property, ...props }) => {
  const { data: objectFormEntry, runAsync } = useAsync();

  useEffect(() => {
    if (!graph) return;

    const getObjectFormPath = () => {
      return entrystore.getEntry(
        entrystore.getEntryURIFromURI(
          graph.findFirstValue(resourceURI, property)
        )
      );
    };

    runAsync(getObjectFormPath());
  }, [runAsync, graph, resourceURI, property]);

  if (!objectFormEntry) return null;

  const getObjectFormPath = () => {
    return getPathFromViewName(FORM_VIEW_NAME, {
      entryId: objectFormEntry.getId(),
      contextId: objectFormEntry.getContext().getId(),
    });
  };

  return (
    <LinkPresenter
      {...props}
      graph={objectFormEntry.getMetadata()}
      resourceURI={objectFormEntry.getResourceURI()}
      property="dcterms:title"
      to={getObjectFormPath(objectFormEntry)}
    />
  );
};

ObjectFormLink.propTypes = {
  graph: graphPropType,
  resourceURI: PropTypes.string,
  property: PropTypes.string,
};
