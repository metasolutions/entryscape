import CommonEntityOverview from 'commons/components/EntityOverview';
import {
  withOverviewModelProvider,
  overviewPropsPropType,
} from 'commons/components/overview';
import escoEntityOverviewNLS from 'commons/nls/escoEntityOverview.nls';
import escoOverviewNLS from 'commons/nls/escoOverview.nls';
import escoListNLS from 'commons/nls/escoList.nls';
import eswoBenchNLS from '../../nls/eswoBench.nls';
import eswoSpacesNLS from '../../nls/eswoSpaces.nls';
import { ENTITY_OVERVIEW_NAME } from '../../config/config';

const NLS_BUNDLES = [
  escoOverviewNLS,
  escoListNLS,
  eswoBenchNLS,
  eswoSpacesNLS,
  escoEntityOverviewNLS,
];

const getAggregationListProps = () => {
  return {
    to: ENTITY_OVERVIEW_NAME,
    getCreateProps: ({ relation, subEntities, translate }) => {
      const createDisabled =
        'max' in relation && relation.max <= subEntities.length;
      return {
        createDisabled,
        createDisabledTooltip: createDisabled
          ? translate('maxEntitiesTooltip', relation.max)
          : '',
      };
    },
  };
};

const EntityOverview = ({ overviewProps }) => {
  return (
    <CommonEntityOverview
      nlsBundles={NLS_BUNDLES}
      getAggregationListProps={getAggregationListProps}
      overviewProps={overviewProps}
    />
  );
};

EntityOverview.propTypes = {
  overviewProps: overviewPropsPropType,
};

export default withOverviewModelProvider(EntityOverview);
