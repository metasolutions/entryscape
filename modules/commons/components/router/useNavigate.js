import {
  getParentPathFromViewName,
  getPathFromViewName,
} from 'commons/util/site';
import { useCallback } from 'react';
import {
  useNavigate as routerUseNavigate,
  useLocation,
} from 'react-router-dom';

const useNavigate = () => {
  const navigate = routerUseNavigate();
  const location = useLocation();
  const goBack = () => navigate(-1);

  const handleNavigate = useCallback(
    async (toOrGetTo, navigateOptions) => {
      if (typeof toOrGetTo === 'function') {
        const to = await toOrGetTo();
        navigate(to, navigateOptions);
        return;
      }
      navigate(toOrGetTo, navigateOptions);
    },
    [navigate]
  );

  const goBackToListView = useCallback(
    (viewName, navigateOptions = {}) => {
      const { contextId } = navigateOptions;

      const path = contextId
        ? getParentPathFromViewName(viewName, navigateOptions)
        : getPathFromViewName(viewName);

      navigate(path, {
        ...navigateOptions,
        state: { ...location.state?.listState },
      });
    },
    [location.state?.listState, navigate]
  );

  return { navigate: handleNavigate, goBack, goBackToListView };
};

export default useNavigate;
