import * as ns from 'models/utils/ns';
import RfsForm from './RfsForm';

const SECTION_FORM_COMPATIBLE_TYPES = [
  ns.RDF_TYPE_PROFILE_FORM,
  ns.RDF_TYPE_OBJECT_FORM,
];

class RfsSectionForm extends RfsForm {
  constructor(props) {
    super({
      ...props,
      rdfType: ns.RDF_TYPE_SECTION_FORM,
      nlsKeyLabel: 'sectionFormLabel',
      properties: [],
      compatibleTypes: SECTION_FORM_COMPATIBLE_TYPES,
      nodetypes: [],
    });
  }
}

export default RfsSectionForm;
