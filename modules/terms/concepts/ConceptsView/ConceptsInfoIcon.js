import { entryPropType } from 'commons/util/entry';
import {
  useTranslation,
  nlsBundlesPropType,
} from 'commons/hooks/useTranslation';
import { withActionsProvider, useActions } from 'commons/components/ListView';
import { Tooltip, IconButton } from '@mui/material';
import Info from '@mui/icons-material/Info';
import LinkedDataBrowserDialog from 'commons/components/LinkedDataBrowserDialog';

const ConceptsInfoIcon = ({ entry, nlsBundles }) => {
  const { openActionDialog } = useActions(nlsBundles);
  const translate = useTranslation(nlsBundles);
  const action = {
    Dialog: LinkedDataBrowserDialog,
    entry,
    nlsBundles,
    icon: <Info />,
  };
  const handleActionClick = () => openActionDialog(action);

  return (
    <Tooltip title={translate('conceptInfo')}>
      <IconButton aria-label="info button" onClick={handleActionClick}>
        {action.icon}
      </IconButton>
    </Tooltip>
  );
};

ConceptsInfoIcon.propTypes = {
  entry: entryPropType,
  nlsBundles: nlsBundlesPropType,
};

export default withActionsProvider(ConceptsInfoIcon);
