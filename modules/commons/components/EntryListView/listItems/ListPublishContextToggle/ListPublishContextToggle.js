import React, { useState, useCallback } from 'react';
import PropTypes from 'prop-types';
import { Entry } from '@entryscape/entrystore-js';
import Switch from 'commons/components/common/Switch';
import Tooltip from 'commons/components/common/Tooltip';
import useContextStatus from 'commons/hooks/context/useContextStatus';
import escoListNLS from 'commons/nls/escoList.nls';
import { useTranslation } from 'commons/hooks/useTranslation';
import useGetMainDialog from 'commons/hooks/useGetMainDialog';
import useRestrictionDialog from 'commons/hooks/useRestrictionDialog';
import { useUserState } from 'commons/hooks/useUser';
import { hasUserPermission, ADMIN_RIGHTS, PREMIUM } from 'commons/util/user';
import { isContextPremium } from 'commons/util/context';
import { ListItemAction } from 'commons/components/ListView';
import { useSnackbar } from 'commons/hooks/useSnackbar';
import { isDraftEntry } from 'commons/util/entry';

/**
 * Component for publishing / unpublishing a context, to be used as part of a List.
 *
 * @returns {React.ReactElement}
 */
export const ListPublishContextToggle = ({
  entry,
  publicTooltip = '',
  privateTooltip = '',
  noAccess,
  confirmUnpublish = async () => true,
  restrictionPath = '',
  xs,
  highlightId,
}) => {
  const [isPublic, togglePublic, canAdminister] = useContextStatus(entry);
  const { getAcknowledgeDialog } = useGetMainDialog();
  const translate = useTranslation([escoListNLS]);
  const [triedPublishing, setTriedPublishing] = useState(false);
  const restrictionCallback = useCallback(() => setTriedPublishing(false), []);
  const { userEntry } = useUserState();
  const hasPermission = hasUserPermission(userEntry, [PREMIUM, ADMIN_RIGHTS]);
  const isExemptFromRestrictions = hasPermission || isContextPremium(entry);
  const isRestricted = !isExemptFromRestrictions && Boolean(restrictionPath);
  const [addSnackbar] = useSnackbar();
  useRestrictionDialog({
    open: isRestricted && triedPublishing,
    contentPath: restrictionPath,
    callback: restrictionCallback,
  });
  const handleChange = async () => {
    if (isRestricted) {
      setTriedPublishing(true);
      return;
    }
    if (!canAdminister) {
      await getAcknowledgeDialog({
        content: noAccess,
      });
      return;
    }
    if (!isPublic) {
      togglePublic();
      addSnackbar({
        message: translate('publishSnackbarMessage'),
        autoHideDuration: 1000,
      });
      return;
    }
    const proceed = await confirmUnpublish(entry);
    if (!proceed) {
      return;
    }
    togglePublic();
    addSnackbar({
      message: translate('unpublishSnackbarMessage'),
      autoHideDuration: 1000,
    });
  };

  const isDraft = isDraftEntry(entry);
  const privateTooltipWithDraft = isDraft
    ? translate('draftDisabledTitle')
    : privateTooltip;
  const tooltip = isPublic ? publicTooltip : privateTooltipWithDraft;

  return (
    <ListItemAction xs={xs}>
      <Tooltip title={tooltip}>
        <div data-highlight-id={highlightId}>
          <Switch
            checked={isPublic}
            onChange={handleChange}
            name="checked"
            inputProps={{ 'aria-label': translate('publishToggleAriaLabel') }}
            disabled={isDraft}
          />
        </div>
      </Tooltip>
    </ListItemAction>
  );
};

ListPublishContextToggle.propTypes = {
  entry: PropTypes.instanceOf(Entry),
  publicTooltip: PropTypes.string,
  privateTooltip: PropTypes.string,
  noAccess: PropTypes.string,
  confirmUnpublish: PropTypes.func,
  restrictionPath: PropTypes.string,
  xs: PropTypes.oneOfType([PropTypes.number, PropTypes.bool]),
  highlightId: PropTypes.string,
};

export default ListPublishContextToggle;
