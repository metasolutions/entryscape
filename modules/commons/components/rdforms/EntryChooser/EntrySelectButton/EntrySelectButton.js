import React from 'react';
import PropTypes from 'prop-types';
import { Entry } from '@entryscape/entrystore-js';
import Button from '@mui/material/Button';
import escoEntryChooserNLS from 'commons/nls/escoEntryChooser.nls';
import { useTranslation } from 'commons/hooks/useTranslation';

const EntrySelectButton = ({ entry, binding, onSelect }) => {
  const selected = binding.getValue() === entry.getResourceURI();
  const onClick = () => onSelect(entry);
  const translate = useTranslation(escoEntryChooserNLS);
  return (
    <Button
      color="primary"
      disabled={selected}
      variant="text"
      onClick={onClick}
    >
      {selected ? translate('selectedEntityLabel') : translate('selectEntity')}
    </Button>
  );
};

EntrySelectButton.propTypes = {
  entry: PropTypes.instanceOf(Entry).isRequired,
  binding: PropTypes.shape({ getValue: PropTypes.func }),
  onSelect: PropTypes.func,
};

export default EntrySelectButton;
