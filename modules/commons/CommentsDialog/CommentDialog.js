import { useState, useEffect, useRef, useCallback } from 'react';
import { Entry } from '@entryscape/entrystore-js';
import { useESContext } from 'commons/hooks/useESContext';
import PropTypes from 'prop-types';
import Button from '@mui/material/Button';
import { IconButton } from '@mui/material';
import Collapse from '@mui/material/Collapse';
import ReplyIcon from '@mui/icons-material/Reply';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
import TextField from '@mui/material/TextField';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import ExpandLessIcon from '@mui/icons-material/ExpandLess';
import Typography from '@mui/material/Typography';
import CircularProgress from '@mui/material/CircularProgress';
import { getTitle } from 'commons/util/metadata';
import Tooltip from 'commons/components/common/Tooltip';
import {
  useTranslation,
  nlsBundlesPropType,
} from 'commons/hooks/useTranslation';
import escoComments from 'commons/nls/escoComment.nls';
import escoDialogNLS from 'commons/nls/escoDialogs.nls';
import escaPreparations from 'catalog/nls/escaPreparations.nls';
import ContentWrapper from 'commons/components/common/ContentWrapper';
import { useSnackbar, SUCCESS_EDIT } from 'commons/hooks/useSnackbar';
import {
  ElementOnFocusProvider,
  useElementOnFocus,
} from 'commons/hooks/useElementOnFocus';
import ListActionDialog from 'commons/components/common/dialogs/ListActionDialog';
import { useUserState } from 'commons/hooks/useUser';
import { hasAdminRights } from 'commons/util/user';
import {
  addComment,
  getReplies,
  deleteCommentAndReplies,
  getCommentText,
  editComment,
  getComments,
  getReplyList,
} from './util';
import './index.scss';

const CommentForm = ({ entry, fetchComments, setIsLoading }) => {
  const [subject, setSubject] = useState('');
  const [comment, setComment] = useState('');
  const [addSnackbar] = useSnackbar();
  const t = useTranslation([escoComments, escaPreparations]);

  const clearFields = () => {
    setComment('');
    setSubject('');
  };

  return (
    <form className="escaCommentForm" noValidate autoComplete="off">
      <TextField
        required
        value={subject}
        label={t('subject')}
        id="subject-input"
        variant="filled"
        placeholder={t('subjectInput_placeholder')}
        onChange={(e) => setSubject(e.target.value)}
      />
      <TextField
        required
        value={comment}
        label={t('comment')}
        id="comment-input"
        variant="filled"
        placeholder={t('commentInput_placeholder')}
        onChange={(e) => setComment(e.target.value)}
      />
      <div className="escaCommentForm__button">
        <Button
          disabled={!subject || !comment}
          onClick={async () => {
            await addComment({ entry, comment, subject });
            setIsLoading(true);
            clearFields();
            fetchComments().then(() => {
              addSnackbar({ message: t('addedComment') });
              setIsLoading(false);
            });
          }}
        >
          {t('postComment')}
        </Button>
      </div>
    </form>
  );
};

const ReplyForm = ({ entry, setOpenComment, fetchComments, setIsLoading }) => {
  const [reply, setReply] = useState('');
  const [addSnackbar] = useSnackbar();
  const { elementToFocus } = useElementOnFocus();
  const t = useTranslation([escoComments, escaPreparations]);

  const clearFields = () => {
    setReply('');
  };

  return (
    <form className="escaCommentForm" noValidate autoComplete="off">
      <TextField
        required
        value={reply}
        label={t('comment')}
        id="reply-comment-input"
        autoFocus
        variant="filled"
        placeholder={t('commentInput_placeholder')}
        onChange={(e) => setReply(e.target.value)}
      />
      <div className="escaCommentForm__button">
        <Button
          classes={{ root: 'escaReply__cancelButton' }}
          onClick={() => {
            elementToFocus.focus();
            setOpenComment(false);
          }}
          variant="text"
        >
          {t('cancelReply')}
        </Button>

        <Button
          disabled={!reply}
          onClick={async () => {
            await addComment({ entry, comment: reply });
            setIsLoading(true);
            clearFields();
            if (setOpenComment) setOpenComment(false);
            fetchComments().then(() => {
              addSnackbar({ message: t('addedReply') });
              setIsLoading(false);
            });
          }}
        >
          {t('postComment')}
        </Button>
      </div>
    </form>
  );
};

const DisplayComment = ({ commentList, fetchComments }) => {
  const [showReply, setShowReply] = useState(true);
  const [openEditForm, setOpenEditFrom] = useState(false);
  const [editEntry, setEditEntry] = useState([]);
  const {
    userInfo: { uri: userURI },
    userEntry,
  } = useUserState();
  const canModify = useCallback((uri) => userURI === uri, [userURI]);

  const handleReply = () => {
    setShowReply(!showReply);
  };
  return commentList.map((comment) => {
    const { subject, body, author, date, noOfReply, reply, entry, authorURI } =
      comment;

    return (
      <article key={`comment-${entry.getId()}`}>
        <header className="escaComment__header">
          <Typography variant="body1" component="h3">
            {subject}
          </Typography>
        </header>
        {openEditForm && editEntry.getId() === entry.getId() ? (
          <EditForm
            entry={entry}
            setOpenEditFrom={setOpenEditFrom}
            fetchComments={fetchComments}
          />
        ) : (
          <div className="escaComment__description">
            <Typography variant="body1">{body}</Typography>
          </div>
        )}

        <div className="escaComment__footer">
          <CommentFooter
            noOfReply={noOfReply}
            showReply={showReply}
            handleReply={handleReply}
            entry={entry}
            fetchComments={fetchComments}
            setOpenEditFrom={setOpenEditFrom}
            setEditEntry={setEditEntry}
            canEdit={
              hasAdminRights(userEntry) ||
              (canModify(authorURI) && noOfReply === 0)
            }
            canDelete={hasAdminRights(userEntry) || canModify(authorURI)}
          >
            <CommentInfo author={author} date={date} />
          </CommentFooter>
          {noOfReply > 0 && (
            <Collapse in={showReply} timeout="auto">
              <DisplayReply replies={reply} fetchComments={fetchComments} />
            </Collapse>
          )}
        </div>
      </article>
    );
  });
};

const DisplayReply = ({ replies, fetchComments }) => {
  const [repliesOfReply, setRepliesOfReply] = useState([]);
  const [editEntry, setEditEntry] = useState();
  const [showReply, setShowReply] = useState(false);
  const [openEditForm, setOpenEditFrom] = useState(false);
  const {
    userInfo: { uri: userURI },
    userEntry,
  } = useUserState();
  const isAdmin = hasAdminRights(userEntry);
  const canModify = useCallback((uri) => userURI === uri, [userURI]);

  const handleReply = async (reply) => {
    setShowReply(!showReply);
    if (!showReply) {
      setRepliesOfReply(await getReplies(reply));
    } else {
      setRepliesOfReply([]);
      setEditEntry([]);
    }
  };

  return replies.map((replyItem) => {
    const { body, author, date, noOfReply, entry, authorURI } = replyItem;
    return (
      <article key={`reply-${entry.getId()}`}>
        {openEditForm && editEntry.getId() === entry.getId() ? (
          <EditForm
            entry={editEntry}
            setOpenEditFrom={setOpenEditFrom}
            fetchComments={fetchComments}
          />
        ) : (
          <div className="escaReply__description">
            <Typography variant="body1" component="h3">
              {body}
            </Typography>
          </div>
        )}
        <div className="escaReply__footer">
          <CommentFooter
            entry={entry}
            noOfReply={noOfReply}
            handleReply={handleReply}
            showReply={showReply}
            setOpenEditFrom={setOpenEditFrom}
            setEditEntry={setEditEntry}
            fetchComments={fetchComments}
            canEdit={isAdmin || (canModify(authorURI) && noOfReply === 0)}
            canDelete={isAdmin || canModify(authorURI)}
          >
            <CommentInfo author={author} date={date} />
          </CommentFooter>
          {noOfReply > 0 && editEntry?.getId() === entry.getId() && (
            <Collapse in={showReply} timeout="auto">
              {repliesOfReply.length ? (
                <DisplayReply
                  replies={repliesOfReply}
                  fetchComments={fetchComments}
                />
              ) : (
                <div className="escaComments__circularProgress">
                  <CircularProgress />
                </div>
              )}
            </Collapse>
          )}
        </div>
      </article>
    );
  });
};

const EditForm = ({ entry, setOpenEditFrom, fetchComments }) => {
  const [editedComment, setEditedComment] = useState(getCommentText(entry));
  const { elementToFocus } = useElementOnFocus();
  const [addSnackbar] = useSnackbar();

  const t = useTranslation([escoComments, escaPreparations]);
  return (
    <form className="escaComment__description">
      <TextField
        required
        id="edit-comment-input"
        value={editedComment}
        label={t('comment')}
        autoFocus
        variant="filled"
        placeholder={t('commentInput_placeholder')}
        onChange={(e) => setEditedComment(e.target.value)}
      />
      <div className="escaCommentForm__button">
        <Button
          classes={{ root: 'escaReply__cancelButton' }}
          onClick={() => {
            elementToFocus.focus();
            setOpenEditFrom(false);
          }}
          variant="text"
        >
          {t('commentEditCancel')}
        </Button>

        <Button
          onClick={() => {
            editComment(entry, editedComment, fetchComments);
            setOpenEditFrom(false);
            addSnackbar({ type: SUCCESS_EDIT });
          }}
        >
          {t('commentEditSave')}
        </Button>
      </div>
    </form>
  );
};

const CommentInfo = ({ author, date }) => {
  return (
    <div>
      <Typography className="escaCommentsFooter__author">{`${author} ${date}`}</Typography>
    </div>
  );
};

const CommentFooter = (props) => {
  const {
    entry,
    noOfReply,
    handleReply,
    setOpenEditFrom,
    setEditEntry,
    showReply,
    fetchComments,
    children,
    canEdit,
    canDelete,
  } = props;
  const [openComment, setOpenComment] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [addSnackbar] = useSnackbar();
  const { setElementToFocus } = useElementOnFocus();
  const t = useTranslation([escoComments, escaPreparations]);

  return (
    <>
      <div className="escaCommentFooter__action">
        {children}
        <div className="escaCommentFooter__actionButtons">
          {noOfReply > 0 && (
            <Button
              classes={{ root: 'escaCommentFooter__showReply' }}
              variant="text"
              onClick={() => {
                handleReply(entry);
                setEditEntry(entry);
              }}
            >
              {t('reply', noOfReply)}
              {showReply ? <ExpandLessIcon /> : <ExpandMoreIcon />}
            </Button>
          )}
          <Tooltip title={t('replyComment')}>
            <IconButton
              onClick={(event) => {
                setElementToFocus(event.currentTarget);
                setOpenComment(true);
              }}
              aria-label={t('replyComment')}
            >
              <ReplyIcon
                color="secondary"
                classes={{ root: 'escaCommentFooter__button' }}
              />
            </IconButton>
          </Tooltip>

          <IconButton
            disabled={!canEdit}
            onClick={(event) => {
              setElementToFocus(event.currentTarget);
              setOpenEditFrom(true);
              setEditEntry(entry);
            }}
            aria-label={t('editComment')}
          >
            <Tooltip title={t('editComment')}>
              <EditIcon classes={{ root: 'escaCommentFooter__button' }} />
            </Tooltip>
          </IconButton>

          <IconButton
            disabled={!canDelete}
            onClick={async () => {
              await deleteCommentAndReplies(entry, fetchComments);
              addSnackbar({ message: t('deleteCommentAction') });
            }}
            aria-label={t('deleteComment')}
          >
            <Tooltip title={t('deleteComment')}>
              <DeleteIcon classes={{ root: 'escaCommentFooter__button' }} />
            </Tooltip>
          </IconButton>
        </div>
      </div>
      {openComment && (
        <ReplyForm
          entry={entry}
          isReply
          fetchComments={fetchComments}
          setOpenComment={setOpenComment}
          setIsLoading={setIsLoading}
        />
      )}
      {isLoading && (
        <div className="escaComments__circularProgress">
          <CircularProgress />
        </div>
      )}
    </>
  );
};

const CommentsDialog = ({
  entry,
  closeDialog,
  dialogTitle = '',
  refreshCommentsCount,
  actionParams = [],
  nlsBundles = [],
}) => {
  const t = useTranslation([...nlsBundles, escoDialogNLS]);
  const { context } = useESContext();
  const [comments, setComments] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [, refreshComments] = actionParams;
  const previousNrOfComments = useRef();
  const refreshEntryComments = refreshCommentsCount || refreshComments;

  const fetchComments = useCallback(async () => {
    const replyList = await getReplyList(entry, context);
    const result = await getComments(replyList);
    setComments(result);
    return result.length;
  }, [entry, context]);

  useEffect(() => {
    fetchComments(entry).then((NrOfComments) => {
      previousNrOfComments.current = NrOfComments;
      setIsLoading(false);
    });
  }, [entry, fetchComments]);

  const handleCloseDialog = () => {
    if (previousNrOfComments.current !== comments.length) {
      refreshEntryComments();
    }
    closeDialog();
  };
  return (
    <ListActionDialog
      id="select-linked-dataset"
      closeDialog={handleCloseDialog}
      closeDialogButtonLabel={t('close')}
      title={dialogTitle || t('commentHeader', getTitle(entry))}
      fixedHeight
    >
      <CommentForm
        entry={entry}
        fetchComments={fetchComments}
        setIsLoading={setIsLoading}
      />
      {isLoading ? (
        <div className="escaComments__circularProgress">
          <CircularProgress />
        </div>
      ) : (
        <ElementOnFocusProvider>
          <DisplayComment
            commentList={comments}
            fetchComments={fetchComments}
          />
        </ElementOnFocusProvider>
      )}
      <ContentWrapper />
    </ListActionDialog>
  );
};

CommentsDialog.propTypes = {
  entry: PropTypes.instanceOf(Entry),
  closeDialog: PropTypes.func,
  dialogTitle: PropTypes.string,
  refreshCommentsCount: PropTypes.func,
  actionParams: PropTypes.arrayOf(
    PropTypes.oneOfType([PropTypes.any, PropTypes.func])
  ),
  nlsBundles: nlsBundlesPropType,
};

CommentForm.propTypes = {
  entry: PropTypes.instanceOf(Entry),
  fetchComments: PropTypes.func,
  setIsLoading: PropTypes.func,
};

ReplyForm.propTypes = {
  entry: PropTypes.instanceOf(Entry),
  setOpenComment: PropTypes.func,
  fetchComments: PropTypes.func,
  setIsLoading: PropTypes.func,
};

EditForm.propTypes = {
  entry: PropTypes.instanceOf(Entry),
  setOpenEditFrom: PropTypes.func,
  fetchComments: PropTypes.func,
};

CommentInfo.propTypes = {
  author: PropTypes.string,
  date: PropTypes.string,
};

CommentFooter.propTypes = {
  entry: PropTypes.instanceOf(Entry),
  fetchComments: PropTypes.func,
  noOfReply: PropTypes.number,
  handleReply: PropTypes.func,
  setOpenEditFrom: PropTypes.func,
  setEditEntry: PropTypes.func,
  showReply: PropTypes.bool,
  canEdit: PropTypes.bool,
  canDelete: PropTypes.bool,
  children: PropTypes.node,
};

export default CommentsDialog;
