import { useEffect, useState } from 'react';
import ListActionDialog from 'commons/components/common/dialogs/ListActionDialog';
import PropTypes from 'prop-types';
import { Button } from '@mui/material';
import { useEditorContext } from 'commons/components/forms/editors';
import { removeWhitespace } from 'commons/util/util';
import { useTranslation } from 'commons/hooks/useTranslation';
import esmoCommonsNls from 'models/nls/esmoCommons.nls';
import { FieldEditor } from 'models/editors';
import { ErrorCatcher } from 'commons/errors/ErrorCatcher';
import { ErrorWithMessage } from 'commons/errors/utils/errors';

const EditOptionDialog = ({
  onCancel = () => {},
  onAccept = () => {},
  closeDialog,
  title,
  doneLabel,
  fields,
}) => {
  const translate = useTranslation(esmoCommonsNls);
  const [isValid, setIsValid] = useState(false);
  const { graph, resourceURI } = useEditorContext();
  const [editError, setEditError] = useState();

  useEffect(() => {
    if (graph) {
      graph.onChange = () =>
        setIsValid(
          Boolean(
            fields
              .filter((field) => field.mandatory)
              .every(({ property }) =>
                removeWhitespace(graph.findFirstValue(resourceURI, property))
              )
          )
        );
    }
  }, [graph, resourceURI, fields]);

  const handleAccept = () => {
    try {
      onAccept(graph);
      closeDialog();
    } catch (error) {
      setEditError(new ErrorWithMessage(translate('saveEditsFail '), error));
    }
  };

  const handleCancel = () => {
    onCancel();
    closeDialog();
  };

  const actions = (
    <Button autoFocus onClick={handleAccept} disabled={!isValid}>
      {doneLabel || translate('doneLabel')}
    </Button>
  );

  return (
    <>
      <ListActionDialog
        closeDialog={handleCancel}
        id="edit-option-dialog"
        actions={actions}
        title={title || translate('editOptionHeader')}
      >
        {fields.map(({ labelNlsKey, ...editorProps }) => (
          <FieldEditor
            key={editorProps.property}
            size="small"
            label={labelNlsKey ? translate(labelNlsKey) : ''}
            {...editorProps}
          />
        ))}
      </ListActionDialog>
      <ErrorCatcher error={editError} />
    </>
  );
};

EditOptionDialog.propTypes = {
  closeDialog: PropTypes.func,
  onCancel: PropTypes.func,
  onAccept: PropTypes.func,
  title: PropTypes.string,
  doneLabel: PropTypes.string,
  fields: PropTypes.arrayOf(PropTypes.shape({})),
};

export default EditOptionDialog;
