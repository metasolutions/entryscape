import * as ns from 'models/utils/ns';
import RfsForm from './RfsForm';

const PROFILE_FORM_PROPERTIES = [
  ns.RDF_PROPERTY_NODETYPE,
  ns.RDF_PROPERTY_CONSTRAINT,
];

const PROFILE_FORM_COMPATIBLE_TYPES = [ns.RDF_TYPE_OBJECT_FORM];

const PROFILE_FORM_NODETYPES = [
  ns.RDF_PROPERTY_RESOURCE,
  ns.RDF_PROPERTY_URI,
  ns.RDF_PROPERTY_BLANK,
];

class RfsProfileForm extends RfsForm {
  constructor(props) {
    super({
      ...props,
      rdfType: ns.RDF_TYPE_PROFILE_FORM,
      nlsKeyLabel: 'profileFormLabel',
      properties: PROFILE_FORM_PROPERTIES,
      compatibleTypes: PROFILE_FORM_COMPATIBLE_TYPES,
      nodetypes: PROFILE_FORM_NODETYPES,
    });
  }
}

export default RfsProfileForm;
