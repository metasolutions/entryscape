import PropTypes from 'prop-types';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';

const ConfirmationDialog = ({
  children,
  open,
  handleClose,
  onDone,
  onReject,
  rejectLabel,
  affirmLabel,
}) => {
  const handleConfirm = () => {
    handleClose();
    return onDone(true);
  };

  const handleReject = () => {
    handleClose();
    return onReject(false);
  };

  return (
    <Dialog
      open={open}
      onClose={handleClose}
      aria-labelledby="confirmation-dialog-title"
      aria-describedby="confirmation-dialog-description"
      maxWidth="sm"
      fullWidth
    >
      <DialogContent>
        <DialogContentText id="confirmation-dialog-description">
          {children}
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleReject} variant="text">
          {rejectLabel}
        </Button>
        <Button onClick={handleConfirm} color="primary" autoFocus>
          {affirmLabel}
        </Button>
      </DialogActions>
    </Dialog>
  );
};

ConfirmationDialog.propTypes = {
  children: PropTypes.node,
  open: PropTypes.bool.isRequired,
  handleClose: PropTypes.func,
  affirmLabel: PropTypes.string,
  rejectLabel: PropTypes.string,
  onDone: PropTypes.func,
  onReject: PropTypes.func,
};

export default ConfirmationDialog;
