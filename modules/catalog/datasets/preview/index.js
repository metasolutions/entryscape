import { useEntry } from 'commons/hooks/useEntry';
import DatasetPreview from 'catalog/datasets/preview/DatasetPreview';

const Preview = () => {
  const entry = useEntry();

  return <DatasetPreview entry={entry} />;
};
export default Preview;
