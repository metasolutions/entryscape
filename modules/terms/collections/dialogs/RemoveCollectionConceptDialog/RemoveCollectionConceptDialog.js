import PropTypes from 'prop-types';
import { Entry } from '@entryscape/entrystore-js';
import esteCollectionNLS from 'terms/nls/esteCollection.nls';
import { useTranslation } from 'commons/hooks/useTranslation';
import { REFRESH, useListModel } from 'commons/components/ListView';
import useAsync, { PENDING } from 'commons/hooks/useAsync';
import LoadingButton from 'commons/components/LoadingButton';
import ListActionDialog from 'commons/components/common/dialogs/ListActionDialog';
import ContentWrapper from 'commons/components/common/ContentWrapper';
import useErrorHandler from 'commons/errors/hooks/useErrorHandler';
import { removeConcept } from '../../collectionUtils';

const nlsBundles = [esteCollectionNLS];

const RemoveCollectionConceptDialog = ({
  conceptEntry,
  collectionEntry,
  closeDialog,
  callback = () => {},
}) => {
  const translate = useTranslation(nlsBundles);
  const { runAsync, status, error: removeError } = useAsync(null);
  useErrorHandler(removeError);
  const [, dispatch] = useListModel();
  const onRemove = () =>
    runAsync(
      removeConcept(collectionEntry, conceptEntry)
        .then(() => dispatch({ type: REFRESH }))
        .then(callback)
        .then(closeDialog)
    );

  const actions = (
    <LoadingButton autoFocus onClick={onRemove} loading={status === PENDING}>
      {translate('collectionRemove')}
    </LoadingButton>
  );

  return (
    <>
      <ListActionDialog
        id="remove-collection-concept"
        actions={actions}
        maxWidth="xs"
        closeDialog={closeDialog}
      >
        <ContentWrapper xs={10}>
          <div>{translate('removeConceptConfirm')}</div>
        </ContentWrapper>
      </ListActionDialog>
    </>
  );
};

RemoveCollectionConceptDialog.propTypes = {
  conceptEntry: PropTypes.instanceOf(Entry).isRequired,
  collectionEntry: PropTypes.instanceOf(Entry).isRequired,
  closeDialog: PropTypes.func.isRequired,
  callback: PropTypes.func,
};

export default RemoveCollectionConceptDialog;
