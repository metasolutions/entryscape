import { useState, useRef, useEffect, useCallback } from 'react';
import PropTypes from 'prop-types';
import LayersIcon from '@mui/icons-material/Layers';
import {
  Button,
  Checkbox,
  FormControlLabel,
  Fade,
  FormGroup,
  Divider,
  Paper,
  IconButton,
} from '@mui/material';
import CloseIcon from '@mui/icons-material/Close';
import escoGeoMapNLS from 'commons/nls/escoGeoMap.nls';
import { useTranslation } from 'commons/hooks/useTranslation';
import { useMapContext } from 'commons/components/Map/hooks/useMapContext';
import Tooltip, { TOOLTIP_DELAY } from 'commons/components/common/Tooltip';
import ConditionalWrapper from 'commons/components/common/ConditionalWrapper';
import './LayerSwitcherControl.scss';

const LayerSwitcherItem = ({ autoFocus, layer, onToggleLayer = () => {} }) => {
  const labelRef = useRef();
  const [hasTooltip, setHasTooltip] = useState(false);
  /*
    Checks if ellipsis is used in order to determine if tooltip should be shown.
    Parent element must be used in the width check because a non inline element
    is required to check scrollWidth and clientWidth and the label iself must be
    inline in order for ellipsis to work.
  */
  useEffect(() => {
    const { parentElement } = labelRef.current;
    const isLabelOverflowing =
      parentElement.scrollWidth > parentElement.clientWidth;
    setHasTooltip(isLabelOverflowing);
  }, []);

  const toggleVisible = () => {
    layer.setVisible(!layer.getVisible());
    onToggleLayer(layer);
  };

  const title = layer.get('title');
  const visible = layer.getVisible();

  return (
    <ConditionalWrapper
      condition={hasTooltip}
      wrapper={(children) => (
        <Tooltip
          enterDelay={TOOLTIP_DELAY}
          enterNextDelay={TOOLTIP_DELAY}
          title={title}
        >
          {children}
        </Tooltip>
      )}
    >
      <FormControlLabel
        classes={{
          root: 'escoLayerSwitcherItem__controlLabel',
          label: 'escoLayerSwitcherItem__label',
        }}
        control={
          <Checkbox
            autoFocus={autoFocus}
            checked={visible}
            onChange={() => toggleVisible(layer)}
          />
        }
        label={<span ref={labelRef}>{title}</span>}
      />
    </ConditionalWrapper>
  );
};

LayerSwitcherItem.propTypes = {
  autoFocus: PropTypes.bool,
  onToggleLayer: PropTypes.func,
  layer: PropTypes.shape({
    getVisible: PropTypes.func,
    setVisible: PropTypes.func,
    set: PropTypes.func,
    get: PropTypes.func,
  }), // can't use instanceOf since ol is loaded dynamically
};

export const LayerSwitcherControl = ({ handleChange, layers }) => {
  const [{ map }] = useMapContext();
  const layersButton = useRef();
  const t = useTranslation(escoGeoMapNLS);

  const hasVisibleLayers = useCallback(
    () =>
      map
        .getLayers()
        .getArray()
        .slice(1)
        .some((mapLayer) => mapLayer.getVisible()),
    [map]
  );

  const [open, setOpen] = useState(!hasVisibleLayers());
  const [, setShowWarning] = useState(!hasVisibleLayers());

  useEffect(() => {
    const onLengthChange = () => {
      setShowWarning(!hasVisibleLayers());
    };
    if (!map) return;
    const collection = map.getLayers();
    collection.on('change:length', onLengthChange);
    return () => collection.un('change:length', onLengthChange);
  }, [hasVisibleLayers, map]);

  const availableLayers = map
    ?.getLayers()
    .getArray()
    .map((mapLayer) => mapLayer);

  const handleClick = () => {
    setOpen((previousOpen) => !previousOpen);
    layersButton.current.blur();
  };

  const updateLayersSetting = (mapLayer) => {
    const visualizationLayers = [...layers];
    const updatedLayersInfo = visualizationLayers.map((layer) => {
      if (layer.title === mapLayer.get('title')) {
        return {
          ...layer,
          visible: mapLayer.getVisible(),
        };
      }
      return layer;
    });
    setShowWarning(!hasVisibleLayers());
    handleChange({ layers: updatedLayersInfo });
  };

  const { 0: backgroundMap } = availableLayers;

  return (
    <div>
      <div className="escoLayerSwitcherControl">
        <Button
          ref={layersButton}
          variant="outlined"
          className="escoLayerSwitcherControl__layersButton"
          onClick={handleClick}
          startIcon={<LayersIcon />}
          aria-haspopup
          aria-expanded={open}
        >
          {t('layersButtonLabel')}
        </Button>
      </div>
      <Fade in={open} timeout={300}>
        <Paper className="escoLayerSwitcherControl__paper" square elevation={0}>
          <div className="escoLayerSwitcherControl__closeButton">
            <IconButton
              onClick={() => {
                setOpen(false);
                layersButton.current.focus();
              }}
              aria-label={t('closeLayerSwitcher')}
            >
              <CloseIcon />
            </IconButton>
          </div>
          {open ? (
            <FormGroup classes={{ root: 'escoLayerSwitcherControl__layers' }}>
              {availableLayers
                .slice(1)
                .reverse()
                .map((layer, index) => (
                  <LayerSwitcherItem
                    // eslint-disable-next-line react/no-array-index-key
                    key={`${layer.get('title')}-${index}`}
                    autoFocus={index === 0 && open}
                    layer={layer}
                    onToggleLayer={updateLayersSetting}
                  />
                ))}
              {backgroundMap && (
                <>
                  <Divider />
                  <LayerSwitcherItem
                    layer={backgroundMap}
                    onToggleLayer={() =>
                      handleChange({
                        basemap: backgroundMap.getVisible().toString(),
                      })
                    }
                  />
                </>
              )}
            </FormGroup>
          ) : null}
        </Paper>
      </Fade>
    </div>
  );
};

LayerSwitcherControl.propTypes = {
  handleChange: PropTypes.func,
  layers: PropTypes.arrayOf(PropTypes.shape()),
};
