const fetch = require('node-fetch');
const FormData = require('form-data');

const BASE_URL = 'https://api.poeditor.com/v2';
const SYNC_TERMS_URL = `${BASE_URL}/projects/sync`;
const UPLOAD_URL = `${BASE_URL}/projects/upload`;
const EXPORT_URL = `${BASE_URL}/projects/export`;

const handleResponse = async (response) => {
  const data = await response.json();
  const { status, code, message } = data.response;
  if (status !== 'success') {
    throw new Error(`${response.url}\n${status} - ${code} - ${message}`);
  }
  return data;
};

module.exports = (apiToken, projectId) => {
  /**
   * Gets the link to an exported file containing translations
   *
   * @param {string} language
   * @returns {string} URL of the exported file
   */
  const getURLForLanguage = (language) => {
    const options = {
      api_token: apiToken,
      id: projectId,
      language: language,
      type: 'json',
      filters: ['translated'],
    };
    const params = new URLSearchParams(options);
    return fetch(EXPORT_URL, {
      method: 'post',
      body: params,
    })
      .then(handleResponse)
      .then((data) => data.result.url);
  };

  /**
   *
   * @param {string} lang
   * @returns {Promise<object[]>}
   */
  const exportTermsAndDefinitions = (lang) =>
    getURLForLanguage(lang).then((url) =>
      url?.length
        ? fetch(url, {
            method: 'post',
          }).then(async (response) => {
            const { status, statusText, url } = response;
            if (status !== 200) {
              throw new Error(`${url}\n${status} - ${statusText}`);
            }
            const data = await response.text();
            return data ? JSON.parse(data) : {};
          })
        : new Promise((resolve, reject) => reject())
    );

  /**
   * Syncs local NLS keys to POEditor
   *
   * @param {object[]} terms
   * @returns {Promise}
   */
  const syncTerms = (terms) => {
    const options = {
      api_token: apiToken,
      id: projectId,
      data: JSON.stringify(terms),
    };
    const params = new URLSearchParams(options);

    return fetch(SYNC_TERMS_URL, {
      method: 'post',
      body: params,
    }).then(handleResponse);
  };

  /**
   * Uploads translations/definitions to POEditor
   *
   * @param {object[]} defs
   * @param {string} language
   * @returns {Promise}
   */
  const uploadDefinitions = (defs, language) => {
    const options = {
      api_token: apiToken,
      id: projectId,
      updating: 'translations',
      overwrite: 1,
      sync_terms: 0,
      language: language,
      fuzzy_trigger: 1,
    };
    const form = new FormData();
    for (const [key, value] of Object.entries(options)) {
      form.append(key, value);
    }
    form.append('file', JSON.stringify(defs), {
      contentType: 'application/json',
      name: 'file',
      filename: 'termsdefs.json',
    });

    return fetch(UPLOAD_URL, {
      method: 'post',
      body: form,
      headers: form.getHeaders(),
    }).then(handleResponse);
  };

  return {
    getURLForLanguage,
    exportTermsAndDefinitions,
    syncTerms,
    uploadDefinitions,
  };
};
