import PropTypes from 'prop-types';
import { useState } from 'react';
import './ControlGroup.scss';

const ControlGroup = ({
  position = 'bottom-right',
  renderControls,
  className = '',
}) => {
  const [target, setTarget] = useState(null);

  return (
    <div
      className={`escoControlGroup escoControlGroup--${position} ${className}`}
      ref={setTarget}
    >
      {renderControls(target)}
    </div>
  );
};

ControlGroup.propTypes = {
  className: PropTypes.string,
  position: PropTypes.oneOf([
    'bottom-right',
    'bottom-left',
    'top-right',
    'top-left',
  ]),
  renderControls: PropTypes.func,
};

export default ControlGroup;
