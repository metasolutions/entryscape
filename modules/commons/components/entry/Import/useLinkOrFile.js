import { useState } from 'react';
import { isUri, validateFileURI } from 'commons/util/util';
import config from 'config';

const validateLink = (url) => {
  if (!url) return false;

  const fileUriAllowed = config.get('catalog.allowFileURI');
  if (fileUriAllowed && validateFileURI(url)) return true;

  return isUri(url);
};

const useLinkOrFile = () => {
  const [link, setLink] = useState('');
  const [file, setFile] = useState(null);
  const [isLink, setIsLink] = useState(true); // Practically represents the tab
  const [buttonEnabled, setButtonEnabled] = useState(false);
  const linkIsValid = validateLink(link);

  const handleLinkChange = (evt) => {
    const newLink = evt.target.value.trimEnd();
    setLink(newLink);
    setButtonEnabled(validateLink(newLink));
    setIsLink(true);
  };

  const handleFileChange = (inputEl) => {
    setFile(inputEl);
    setButtonEnabled(!!inputEl);
    setIsLink(false);
  };

  // Tabs: 0 - link, 1 - file
  const handleTabChange = (newTab) => {
    setIsLink(newTab === 0); // Doesn't make much sense since 0 is falsy
    const stateIsValid =
      (newTab === 0 && linkIsValid) || (newTab === 1 && file);
    setButtonEnabled(stateIsValid);
  };

  return {
    link,
    setLink,
    file,
    setFile,
    handleLinkChange,
    handleFileChange,
    handleTabChange,
    linkIsValid,
    buttonEnabled,
    isLink,
    setIsLink,
  };
};

export default useLinkOrFile;
