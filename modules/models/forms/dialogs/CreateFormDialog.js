import PropTypes from 'prop-types';
import { useMemo } from 'react';
import { useTranslation } from 'commons/hooks/useTranslation';
import esmoFormsNLS from 'models/nls/esmoForms.nls';
import esmoCommonsNLS from 'models/nls/esmoCommons.nls';
import escoDialogsNLS from 'commons/nls/escoDialogs.nls';
import { useESContext } from 'commons/hooks/useESContext';
import CreateDialog from 'models/components/CreateDialog';
import { EditorProvider } from 'commons/components/forms/editors';
import { FIELD_TITLE, FIELD_PURPOSE } from 'models/utils/fieldDefinitions';
import { FORM_TYPE, FORM_PROPERTY } from 'models/utils/formDefinitions';
import * as ns from 'models/utils/ns';
import { copyTitleToLabel } from 'models/utils/metadata';
import { useSnackbar } from 'commons/hooks/useSnackbar';

const CREATE_FIELDS = [
  { ...FORM_TYPE },
  { ...FIELD_TITLE },
  { ...FIELD_PURPOSE },
  { ...FORM_PROPERTY },
];

const NLS_BUNDLES = [esmoFormsNLS, esmoCommonsNLS, escoDialogsNLS];

/**
 * Adds default form metadata before commit
 *
 * @param {Graph} graph
 * @param {string} resourceURI
 * @returns {Graph}
 */
const addFormMetadata = (graph, resourceURI) => {
  graph.add(resourceURI, 'rdf:type', ns.RDF_TYPE_FORM);

  graph.add(
    resourceURI,
    ns.RDF_PROPERTY_STATUS,
    ns.RDF_PROPERTY_STATUS_UNSTABLE
  );

  copyTitleToLabel(graph, resourceURI);

  // profile form
  if (graph.find(resourceURI, 'rdf:type', ns.RDF_TYPE_PROFILE_FORM).length) {
    graph.add(resourceURI, ns.RDF_PROPERTY_NODETYPE, ns.RDF_PROPERTY_RESOURCE);
  }

  // object form
  if (graph.find(resourceURI, 'rdf:type', ns.RDF_TYPE_OBJECT_FORM).length) {
    graph.addD(resourceURI, ns.RDF_PROPERTY_MIN, '0', 'xsd:integer');
    graph.add(resourceURI, ns.RDF_PROPERTY_NODETYPE, ns.RDF_PROPERTY_RESOURCE);
  }

  return graph;
};

const CreateFormDialog = ({ closeDialog }) => {
  const translate = useTranslation(NLS_BUNDLES);
  const { context } = useESContext();
  const [addSnackbar] = useSnackbar();

  const prototypeEntry = useMemo(() => {
    return context.newEntry();
  }, [context]);

  return (
    <EditorProvider entry={prototypeEntry}>
      <CreateDialog
        title={translate('createFormTitle')}
        closeDialog={closeDialog}
        fields={CREATE_FIELDS}
        beforeCreate={addFormMetadata}
        nlsBundles={NLS_BUNDLES}
        onEditEntry={() => addSnackbar({ message: translate('createSuccess') })}
      />
    </EditorProvider>
  );
};

CreateFormDialog.propTypes = {
  closeDialog: PropTypes.func.isRequired,
};

export default CreateFormDialog;
