import { useState } from 'react';
import PropTypes from 'prop-types';
import { graphPropType } from 'commons/util/entry';
import { EditorProvider } from 'commons/components/forms/editors';
import {
  RDF_CLASS_OPTION,
  RDF_PROPERTY_OPTION,
  RDF_PROPERTY_ORDER,
} from 'models/utils/ns';
import { useTranslation } from 'commons/hooks/useTranslation';
import esmoCommonsNls from 'models/nls/esmoCommons.nls';
import EditOptionDialog from '../EditOptionDialog';

// When adding an option, a blank node must be added for that option before it
// can be edited. If cancelling, the blank node should be removed from the graph.
const AddOptionDialog = ({
  graph,
  resourceURI,
  onAddOption,
  closeDialog,
  order,
  title,
  fields,
}) => {
  const translate = useTranslation(esmoCommonsNls);
  const [blankNodeId] = useState(
    graph.add(resourceURI, RDF_PROPERTY_OPTION, null, true).getValue()
  );

  const handleCancel = () => {
    graph.findAndRemove(resourceURI, RDF_PROPERTY_OPTION, {
      type: 'bnode',
      value: blankNodeId,
    });
  };

  const handleAccept = (newGraph) => {
    newGraph.add(blankNodeId, 'rdf:type', RDF_CLASS_OPTION);
    newGraph.addD(blankNodeId, RDF_PROPERTY_ORDER, `${order}`, 'xsd:integer');
    onAddOption(newGraph);
  };

  return (
    <EditorProvider graph={graph} resourceURI={blankNodeId}>
      <EditOptionDialog
        closeDialog={closeDialog}
        onCancel={handleCancel}
        onAccept={handleAccept}
        title={title}
        doneLabel={translate('addLabel')}
        fields={fields}
      />
    </EditorProvider>
  );
};

AddOptionDialog.propTypes = {
  graph: graphPropType,
  resourceURI: PropTypes.string,
  onAddOption: PropTypes.func,
  closeDialog: PropTypes.func,
  order: PropTypes.number,
  title: PropTypes.string,
  fields: PropTypes.arrayOf(PropTypes.shape({})),
};

export default AddOptionDialog;
