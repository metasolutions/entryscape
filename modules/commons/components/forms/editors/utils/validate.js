import { isUri } from 'commons/util/util';
import { URI } from 'commons/components/forms/utils/nodetypes';

export const REQUIRED_FIELD = 'requiredFieldError';
export const MISSING_VALUE = 'missingValueError';
export const URI_ERROR = 'uriValueError';

/**
 *
 * @param {object} field
 * @returns {string|undefined}
 */
export const validateField = (field) => {
  if (field.getNodetype() === URI) {
    const uriValue = field.getValue();
    const uriError = uriValue && !isUri(uriValue);
    if (uriError) return URI_ERROR;
  }
};

/**
 *
 * @param {Set} fieldSet
 * @returns {string[]}
 */
export const validateFieldSet = (fieldSet) => {
  const fields = Array.from(fieldSet.values());
  return fields.map((field) => field.validate?.(true)).filter((error) => error);
};
