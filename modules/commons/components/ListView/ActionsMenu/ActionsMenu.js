import PropTypes from 'prop-types';
import ListItemIcon from '@mui/material/ListItemIcon';
import {
  ListItemText,
  Menu,
  MenuItem,
  IconButton,
  Skeleton,
} from '@mui/material';
import NoPropagation from 'commons/components/NoPropagatation';
import Tooltip from 'commons/components/common/Tooltip';
import { MoreHoriz as EllipsisIcon } from '@mui/icons-material';
import { useTranslation } from 'commons/hooks/useTranslation';
import escoListNLS from 'commons/nls/escoList.nls';
import { getUniqueId } from 'commons/hooks/useUniqueId';
import { useActions } from '../ActionsProvider';

// Button component to open actions menu
export const OpenActionsMenuButton = ({ title, ...iconButtonProps }) => {
  const { openMenu, id } = useActions();
  const translate = useTranslation(escoListNLS);

  return (
    <Tooltip title={title || translate('moreOptions')}>
      <IconButton
        onClick={openMenu}
        aria-controls={id}
        aria-haspopup="true"
        // eslint-disable-next-line react/jsx-props-no-spreading
        {...iconButtonProps}
      >
        <EllipsisIcon />
      </IconButton>
    </Tooltip>
  );
};

OpenActionsMenuButton.propTypes = {
  title: PropTypes.string,
};

// ActionsMenu is usually combined with OpenActionsMenuButton to open the menu.
// Actions provider is required.
// Example:
// <OpenActionsMenuButton />
// <ActionsMenu>
//   {rowActions.map((rowAction) => (
//     <ActionsMenuItem key={rowAction.id} ...rowAction />
//   ))}
// </ActionsMenu>
export const ActionsMenu = ({ items, isLoading, menuProps = {} }) => {
  const { anchorEl, id, isOpen, closeMenu, openActionDialog } = useActions();
  const handleAction = (action) => () => openActionDialog(action);
  const handleClick = (onClick) => (event) => {
    closeMenu();
    onClick(event);
  };

  return (
    <NoPropagation>
      <Menu
        id={id}
        anchorEl={anchorEl}
        open={isOpen}
        onClose={closeMenu}
        {...menuProps}
      >
        {items.map(({ label, icon, onClick, action, id: itemId }) =>
          isLoading ? (
            <MenuItem key={action?.id || itemId || getUniqueId()}>
              <Skeleton variant="text" sx={{ fontSize: '18px' }} width="8rem" />
            </MenuItem>
          ) : (
            <MenuItem
              key={action?.id || itemId || getUniqueId()}
              onClick={onClick ? handleClick(onClick) : handleAction(action)}
            >
              {icon ? <ListItemIcon>{icon}</ListItemIcon> : null}
              <ListItemText primary={label} />
            </MenuItem>
          )
        )}
      </Menu>
    </NoPropagation>
  );
};

ActionsMenu.propTypes = {
  items: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string,
      onClick: PropTypes.func,
      action: PropTypes.shape({}),
      icon: PropTypes.node,
    })
  ),
  isLoading: PropTypes.bool,
  menuProps: PropTypes.shape({}),
};
