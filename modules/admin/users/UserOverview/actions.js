import {
  ACTION_EDIT,
  ACTION_REVISIONS,
  ACTION_REMOVE,
} from 'commons/components/overview/actions';
import {
  isUserPremium,
  isRegularUser,
  isUserDisabled,
  isUserEnabled,
  isPremiumConfigured,
  isGuest,
  isAdmin,
} from 'commons/util/user';
import {
  Group as GroupIcon,
  Delete as RemoveIcon,
  AccountCircle as UserIcon,
  LockOpen as PasswordIcon,
  PersonAddDisabled as DisableUserIcon,
  PersonAdd as EnableUserIcon,
} from '@mui/icons-material';
import { withOverviewRefresh } from 'commons/components/overview';
import { getRenderName } from 'commons/util/rdfUtils';
import ChangeUsernameDialog from '../UsersView/dialogs/ChangeUsernameDialog';
import DisableUserDialog from '../UsersView/dialogs/DisableUserDialog';
import ChangePasswordDialog from '../UsersView/dialogs/ChangePasswordDialog';
import GroupMemberships from '../UsersView/dialogs/GroupMembershipsDialog';
import UpgradeUserToPremiumDialog from '../UsersView/dialogs/UpgradeUserToPremiumDialog';
import DowngradeUserFromPremiumDialog from '../UsersView/dialogs/DowngradeUserFromPremiumDialog';
import { replaceNameWithFirstAndLastName } from '../utils/user';

const ACTION_CHANGE_USERNAME_ID = 'username';
const ACTION_CHANGE_PASSWORD_ID = 'password';
const ACTION_GROUPS_ID = 'groups';
const ACTION_ENABLE_USER_ID = 'enable-user';
const ACTION_DISABLE_USER_ID = 'disable-user';
const ACTION_MAKE_PREMIUM_ID = 'upgradeToPremium';
const ACTION_REMOVE_PREMIUM_ID = 'downgradeFromPremium';

const canChangeUserStatus = (entry) => entry.canAdministerEntry();

const canChangeUserStatusAndIsEnabled = ({ entry }) =>
  isRegularUser(entry) && canChangeUserStatus(entry) && isUserEnabled(entry);
const canChangeUserStatusAndIsDisabled = ({ entry }) =>
  isRegularUser(entry) && canChangeUserStatus(entry) && isUserDisabled(entry);

const handleUserEntryEdit = (entry) => {
  replaceNameWithFirstAndLastName(entry);
  return entry.commitMetadata();
};

export const sidebarActions = [
  {
    ...ACTION_EDIT,
    getProps: () => ({
      action: {
        formTemplateId: 'esc:User',
        onEntryEdit: handleUserEntryEdit,
      },
    }),
  },
  {
    id: ACTION_CHANGE_USERNAME_ID,
    Dialog: withOverviewRefresh(ChangeUsernameDialog, 'onCreate'),
    labelNlsKey: 'changeUsername',
    isVisible: ({ entry }) => isRegularUser(entry),
    getProps: () => ({
      startIcon: <UserIcon />,
    }),
  },
  {
    id: ACTION_CHANGE_PASSWORD_ID,
    Dialog: ChangePasswordDialog,
    labelNlsKey: 'setPassword',
    isVisible: ({ entry }) => !isGuest(entry),
    getProps: () => ({
      startIcon: <PasswordIcon />,
    }),
  },
  ACTION_REVISIONS,
  {
    id: ACTION_GROUPS_ID,
    Dialog: GroupMemberships,
    labelNlsKey: 'groupsList',
    isVisible: ({ entry }) => !isAdmin(entry),
    getProps: () => ({
      startIcon: <GroupIcon />,
    }),
  },
  {
    id: ACTION_ENABLE_USER_ID,
    Dialog: withOverviewRefresh(DisableUserDialog, 'onStatusChange'),
    isVisible: canChangeUserStatusAndIsDisabled,
    labelNlsKey: 'userStatusEnable',
    getProps: () => ({
      startIcon: <EnableUserIcon />,
    }),
  },
  {
    id: ACTION_DISABLE_USER_ID,
    Dialog: withOverviewRefresh(DisableUserDialog, 'onStatusChange'),
    isVisible: canChangeUserStatusAndIsEnabled,
    labelNlsKey: 'userStatusDisable',
    getProps: () => ({
      startIcon: <DisableUserIcon />,
    }),
  },
  {
    id: ACTION_MAKE_PREMIUM_ID,
    Dialog: UpgradeUserToPremiumDialog,
    isVisible: ({ entry }) =>
      isRegularUser(entry) && isPremiumConfigured() && !isUserPremium(entry),
    labelNlsKey: 'addToPremiumGroup',
  },
  {
    id: ACTION_REMOVE_PREMIUM_ID,
    Dialog: DowngradeUserFromPremiumDialog,
    isVisible: ({ entry }) =>
      isRegularUser(entry) && isPremiumConfigured() && isUserPremium(entry),
    labelNlsKey: 'removeFromPremiumGroup',
    tooltipNlsKey: 'removeFromPremiumGroup',
  },
  {
    ...ACTION_REMOVE,
    isVisible: ({ entry }) => isRegularUser(entry),
    getProps: ({ entry, translate }) => ({
      action: {
        removeConfirmMessage: translate(
          'removeUserMessage',
          getRenderName(entry) || entry.getId()
        ),
      },
      startIcon: <RemoveIcon />,
    }),
  },
];
