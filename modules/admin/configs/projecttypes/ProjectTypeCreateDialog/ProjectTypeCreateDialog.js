import PropTypes from 'prop-types';
import TypeCreateDialog from 'admin/configs/types/TypeCreateDialog';
import { EditorProvider } from 'commons/components/forms/editors';
import { useState, useEffect } from 'react';
import { useTranslation } from 'commons/hooks/useTranslation';
import esadTypeNLS from 'admin/nls/esadType.nls';
import esadProjectTypeNLS from 'admin/nls/esadProjectType.nls';
import { RDF_CLASS_PROJECT_TYPE } from 'admin/configs/utils/namespace';
import { getOrCreateConfigContextEntry } from 'admin/configs/utils/configQuery';
import useAsync from 'commons/hooks/useAsync';
import ProjectTypeForm from '../ProjectTypeForm';
import { editorFields } from '../utils/editorFields';

const NLS_BUNDLES = [esadTypeNLS, esadProjectTypeNLS];

const CreateProjectTypeDialog = ({ closeDialog }) => {
  const translate = useTranslation(NLS_BUNDLES);

  const [prototypeEntry, setPrototypeEntry] = useState();
  const { runAsync } = useAsync();

  useEffect(() => {
    runAsync(
      getOrCreateConfigContextEntry().then((contextEntry) => {
        const context = contextEntry.getResource(true);
        setPrototypeEntry(context.newEntry());
      })
    );
  }, [setPrototypeEntry, runAsync]);

  return prototypeEntry ? (
    <EditorProvider entry={prototypeEntry}>
      <TypeCreateDialog
        title={translate('createHeader')}
        closeDialog={closeDialog}
        rdfTypes={[RDF_CLASS_PROJECT_TYPE]}
        nlsBundles={NLS_BUNDLES}
        fields={editorFields}
      >
        <ProjectTypeForm nlsBundles={NLS_BUNDLES} />
      </TypeCreateDialog>
    </EditorProvider>
  ) : null;
};

CreateProjectTypeDialog.propTypes = {
  closeDialog: PropTypes.func.isRequired,
};

export default CreateProjectTypeDialog;
