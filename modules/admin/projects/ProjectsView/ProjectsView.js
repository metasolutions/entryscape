import { useCallback } from 'react';
import {
  EntryListView,
  useSolrQuery,
  TITLE_COLUMN,
  TITLE_SORT,
  MODIFIED_COLUMN,
  ACTIONS_GROUP_COLUMN,
} from 'commons/components/EntryListView';
import {
  LIST_ACTION_INFO,
  LIST_ACTION_EDIT,
} from 'commons/components/EntryListView/actions';
import {
  withListModelProviderAndLocation,
  listPropsPropType,
} from 'commons/components/ListView';
import { types } from '@entryscape/entrystore-js';
import { useUserState } from 'commons/hooks/useUser';
import { getSearchQuery } from 'commons/util/solr/entry';
import { getRenderName } from 'commons/util/rdfUtils';
import { useTranslation } from 'commons/hooks/useTranslation';
import { getPathFromViewName } from 'commons/util/site';
import LinkedDataBrowserDialog from 'commons/components/LinkedDataBrowserDialog';
import { listActions, nlsBundles } from './actions';

const ProjectsView = ({ listProps }) => {
  const { userEntry, userInfo } = useUserState();

  const createQuery = useCallback(() => {
    const [query] = getSearchQuery(
      {
        entryType: types.ET_LOCAL,
        graphType: types.GT_CONTEXT,
        resourceType: 'InformationResource',
      },
      { userEntry, userInfo }
    );
    return query;
  }, [userEntry, userInfo]);

  const translate = useTranslation(nlsBundles);
  const entryTypeName = translate('createEntryName');

  const queryResults = useSolrQuery({
    createQuery,
  });

  return (
    <EntryListView
      {...queryResults}
      {...listProps}
      nlsBundles={nlsBundles}
      viewPlaceholderProps={{
        label: translate('emptyMessageWithName', entryTypeName),
      }}
      listActions={listActions}
      getListItemProps={({ entry }) => ({
        to: getPathFromViewName('admin__projects__project', {
          entryId: entry.getId(),
          contextId: entry.getContext().getId(),
        }),
      })}
      columns={[
        {
          ...TITLE_COLUMN,
          xs: 9,
          sortBy: TITLE_SORT,
          getProps: ({ entry }) => ({
            primary:
              getRenderName(entry) ||
              translate('unnamedWorkspace', entry.getId()),
          }),
        },
        MODIFIED_COLUMN,
        {
          ...ACTIONS_GROUP_COLUMN,
          actions: [
            {
              ...LIST_ACTION_INFO,
              Dialog: LinkedDataBrowserDialog,
              formTemplateId: 'esc:Context',
            },
            {
              ...LIST_ACTION_EDIT,
              formTemplateId: 'esc:Context',
            },
          ],
        },
      ]}
    />
  );
};

ProjectsView.propTypes = {
  listProps: listPropsPropType,
};

export default withListModelProviderAndLocation(ProjectsView);
