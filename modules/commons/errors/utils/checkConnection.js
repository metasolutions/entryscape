import { entrystore } from 'commons/store';

const checkCountIntervalls = [2, 5, 10, 30, 60, 120, 300, 600];
let checkCountIdx = -1;
let checkCountdownTimeout = null;

export const stop = () => {
  checkCountIdx = -1;
  clearTimeout(checkCountdownTimeout);
};

export const check = (countdownCallback, onlineCallback) => {
  if (checkCountIdx >= 0) {
    return;
  }
  checkCountIdx = 0;
  clearTimeout(checkCountdownTimeout);

  let checkConnection;
  const countdown = (seconds) => {
    checkCountdownTimeout = setInterval(() => {
      const seconds_ = seconds - 1;
      if (seconds_ === 0) {
        clearTimeout(checkCountdownTimeout);
        checkConnection();
      } else {
        countdownCallback(seconds_);
      }
    }, 1000);
    countdownCallback(seconds);
  };
  const continueCountdown = () => {
    countdown(checkCountIntervalls[checkCountIdx]);
    if (checkCountIdx < checkCountIntervalls.length - 1) {
      checkCountIdx += 1;
    }
  };
  checkConnection = () => {
    entrystore
      .getREST()
      .get(`${entrystore.getBaseURI()}management/status`, 'text/plain')
      .then((res) => {
        if (res === 'UP') {
          stop();
          onlineCallback();
        } else if (
          typeof res === 'object' &&
          res.repositoryStatus === 'online'
        ) {
          continueCountdown();
        }
      }, continueCountdown);
  };
  checkConnection();
};
