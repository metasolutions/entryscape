import WMSCapabilities from 'ol/format/WMSCapabilities';
import { addIgnore } from 'commons/errors/utils/async';
import { entrystore as store } from 'commons/store';
import config from 'config';
import {
  extentToProjection,
  registerProjections,
  getProjection,
} from './projection';
import { ESPG_WGS84, CRS_WGS84, EPSG_WEB_MERCATOR } from './projections';

const LAYER_LIMIT_VISIBILITY = 3;
const DEFAULT_MAP_EXTENT = [
  -3858811.7371753007, 3192437.0539839533, 8666648.325757068, 8487560.45499612,
];
const CAPABILITIES_PARAMS = {
  request: 'getCapabilities',
  service: 'WMS',
  version: '1.3.0',
};

/**
 * Requests the WMS service metadata
 *
 * @param {string} url
 * @returns {Promise}
 */
export const getWmsCapabilities = async (url) => {
  if (url.includes('http:')) return { error: 'loadFromHttpError' };
  const parsedURL = new URL(url);

  // Add required wms parameters if missing
  const searchParamKeys = Array.from(parsedURL.searchParams.keys()).map(
    (param) => param.toLowerCase()
  );
  Object.keys(CAPABILITIES_PARAMS).forEach((param) => {
    if (searchParamKeys.includes(param)) return;
    parsedURL.searchParams.set(param, CAPABILITIES_PARAMS[param]);
  });

  const wmsURI = parsedURL.href;
  const parser = new WMSCapabilities();

  addIgnore('loadViaProxy', true, true);
  return store
    .loadViaProxy(wmsURI, 'application/xml')
    .catch((loadError) => {
      console.error(loadError);
      return 'getCapabilitiesLoadError';
    })
    .then((response) => parser.read(response))
    .catch((parseError) => {
      console.error(parseError);
      return { error: 'getCapabilitiesParseError' };
    });
};

/**
 * Converts capabtilites root layer to array of layers
 *
 * @param {object|object[]} rootLayer
 * @returns {object[]}
 */
const rootLayerToLayers = (rootLayer) => {
  if (!rootLayer) return [];
  if (!rootLayer.Layer) {
    return rootLayer.Name ? [rootLayer] : [];
  }
  return Array.isArray(rootLayer.Layer) ? rootLayer.Layer : [rootLayer.Layer];
};

/**
 * Parses wms layer configuration
 *
 * @param {object|object[]} rootLayer
 * @returns {object[]|undefined}
 */
const getWmsLayers = (rootLayer) => {
  const layers = rootLayerToLayers(rootLayer);

  return layers.map(({ Name: name, Title: title }) => ({
    title,
    name,
    visible: layers.length <= LAYER_LIMIT_VISIBILITY,
  }));
};

/**
 * Look for best matching projection in this order:
 * 1. Default projections in OpenLayers
 * 2. Other registered projections in OpenLayers
 *
 * @param {string[]} projectionCodes
 * @returns {object|undefined}
 */
const findAvailableProjection = (projectionCodes) => {
  if (!projectionCodes.length) return;

  const defaultProjectionCodes = [EPSG_WEB_MERCATOR, CRS_WGS84, ESPG_WGS84];
  const defaultProjectionCode = projectionCodes.find((projectionCode) =>
    defaultProjectionCodes.includes(projectionCode)
  );
  if (defaultProjectionCode) return defaultProjectionCode;

  return projectionCodes.find((code) => {
    return Boolean(getProjection(code));
  });
};

/**
 * Try to find extent for a boundings box with a projection that is registered
 * in OpenLayers and transform it to default projection:
 * 1. Find a matching projection registered in OpenLayers
 * 2. Get the extent and transform it to default projection.
 *
 * @param {object[]} boundingBoxes
 * @returns {number[]|undefined}
 */
export const getExtentFromBoundingBoxes = (boundingBoxes = []) => {
  const projectionCode = findAvailableProjection(
    boundingBoxes.map(({ crs }) => crs)
  );
  if (!projectionCode) return;

  // get the extent for matching projection
  const { crs, extent } = boundingBoxes.find((boundingBox) => {
    if (boundingBox.crs === projectionCode) return true;
    return false;
  });
  const axisOrientation = getProjection(crs).getAxisOrientation();
  const enuExtent =
    axisOrientation === 'neu'
      ? [extent[1], extent[0], extent[3], extent[2]]
      : extent;
  return extentToProjection(enuExtent, crs);
};

const getGeographical = (extent) => {
  return extentToProjection(extent, ESPG_WGS84);
};

/**
 * Check if any of the provided projections is a supported projection and
 * returns first match.
 *
 * @param {string[]} projectionCodes
 * @returns {string|undefined}
 */
export const checkProjectionSupport = (projectionCodes) => {
  return projectionCodes.find((projectionCode) =>
    [EPSG_WEB_MERCATOR, ESPG_WGS84, CRS_WGS84].includes(projectionCode)
  );
};

/**
 * Extracts wms layer definitions from wms getCapabilities document, compatible
 * with visualization state.
 *
 * @param {object} data
 * @returns {object}
 */
export const parseWmsGetCapabilites = (data) => {
  const wmsData = {
    extent: undefined,
    layers: [],
    wmsBaseURI: '',
    projection: null, // To be implemented
    error: '',
    basemap: 'true',
  };
  // Get url for GetMap requests
  if (data.Capability.Request) {
    const { GetMap } = data.Capability.Request;
    wmsData.wmsBaseURI = GetMap.DCPType[0].HTTP.Get.OnlineResource;
    if (!wmsData.wmsBaseURI) {
      wmsData.error = 'missingBaseURI';
      return wmsData;
    }
  }

  const rootLayer = data.Capability.Layer;
  const layers = getWmsLayers(rootLayer);
  if (!layers.length) {
    wmsData.error = 'missingLayerInformation';
    return wmsData;
  }
  wmsData.layers = layers;

  registerProjections(config.get('itemstore.projections'));

  // const extent = getExtentFromBoundingBoxes(rootLayer?.BoundingBox);
  const extent = getGeographical(rootLayer.EX_GeographicBoundingBox);
  const hasProjectionSupport = checkProjectionSupport(rootLayer.CRS);

  // TODO: this is how the check has been done, but needs to be checked. The
  // hasProjectionSupport should be required to be true, even if an extent has
  // been found. Also check redundant nls string supportedProjectionNotFound.
  if (!extent && !hasProjectionSupport) {
    wmsData.error = 'noSupportedProjectionsFound';
    return wmsData;
  }

  wmsData.extent = extent || DEFAULT_MAP_EXTENT;
  wmsData.initialView = wmsData.extent;

  return wmsData;
};

/**
 * Retreive and parse get capabilities data according to fields state
 *
 * @param {string} wmsURI
 * @returns {Promise<object>}
 */
export const getWmsData = async (wmsURI) => {
  const { error, ...capabilitesResult } = await getWmsCapabilities(wmsURI);
  if (error) return { error };

  return parseWmsGetCapabilites(capabilitesResult);
};
