import { Graph } from '@entryscape/rdfjson';
import {
  getURIEditorType,
  RESOURCE,
  NAMESPACE_ENTRY,
  getLocalnameAndNamespaceURI,
} from './metadata';

/**
 * A value is uri either if the type is uri or blank node. In the latter case a
 * blank node points to a resource entry uri.
 *
 * @param {Graph} graph
 * @param {string[]} resourceURIs
 * @param {valueProperty} valueProperty
 * @returns {boolean}
 */
export const getHasURIValue = (graph, resourceURIs, valueProperty) => {
  for (const resourceURI of resourceURIs) {
    const [statement] = graph.find(resourceURI, valueProperty);
    const type = statement.getType();
    if (type === 'uri' || type === 'bnode') return true;
  }
  return false;
};

/**
 * Converts value stored as namespace entry to literal
 *
 * @param {Graph} graph
 * @param {string} blankNodeId
 * @param {string} valueProperty
 */
const convertNamespaceEntryToLiteral = async (
  graph,
  blankNodeId,
  valueProperty
) => {
  const [statement] = graph.find(blankNodeId, valueProperty);
  const namespaceBlankNodeId = statement.getValue();
  const value = await getLocalnameAndNamespaceURI(graph, namespaceBlankNodeId);

  // clear current values
  graph.findAndRemove(blankNodeId, valueProperty);
  graph.findAndRemove(namespaceBlankNodeId);

  // add literal of the uri
  const { localname, namespaceURI } = value;
  graph.addL(blankNodeId, valueProperty, `${namespaceURI}${localname}`);
};

/**
 * Changes type from uri to literal for matching values
 *
 * @param {Graph} graph
 * @param {string[]} blankNodeId
 * @param {string} property
 */
const convertResourceToLiteral = (graph, blankNodeId, property) => {
  const uriValue = graph.findFirstValue(blankNodeId, property);
  graph.findAndRemove(blankNodeId, property);
  graph.addL(blankNodeId, property, uriValue);
};

/**
 * Converts values stored as resources either as an uri value or resource entry
 *
 * @param {Graph} graph
 * @param {string[]} blankNodeIds
 * @param {string} valueProperty
 * @returns {Promise<undefined>}
 */
export const convertURIValuesToLiterals = async (
  graph,
  blankNodeIds,
  valueProperty
) => {
  for (const blankNodeId of blankNodeIds) {
    const editorType = getURIEditorType(graph, blankNodeId, valueProperty);
    if (editorType === RESOURCE) {
      convertResourceToLiteral(graph, blankNodeId, valueProperty);
    } else if (editorType === NAMESPACE_ENTRY) {
      await convertNamespaceEntryToLiteral(graph, blankNodeId, valueProperty);
    }
  }
};
