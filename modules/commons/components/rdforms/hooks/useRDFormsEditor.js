import { Graph } from '@entryscape/rdfjson';
import { itemStore } from 'commons/rdforms/itemstore';
import { Editor as RDFormsEditor } from '@entryscape/rdforms/renderers/react';
import { refreshEntry, isNewEntry } from 'commons/util/entry';
import { useState, useEffect, useRef } from 'react';
import { LEVEL_RECOMMENDED } from 'commons/components/rdforms/LevelSelector';
import useAsync from 'commons/hooks/useAsync';

const useRDFormsEditor = ({
  entry,
  templateId,
  includeLevel = LEVEL_RECOMMENDED,
  open = false, // used to reset editor in concepts edit dialog
  filterPredicates,
  rdformTemplate,
}) => {
  const [editor, setEditor] = useState(null);
  const [graph, setGraph] = useState(null);
  const [template, setTemplate] = useState(null);
  const isOpen = useRef(open);
  const { runAsync } = useAsync(null);
  const resourceUri = entry.getResourceURI();

  useEffect(() => {
    if (!rdformTemplate && !templateId) return;
    const newTemplate = rdformTemplate || itemStore.getItem(templateId);
    if (!newTemplate) {
      console.error(`template with id ${templateId} not found`);
      return;
    }
    setTemplate(newTemplate);
  }, [rdformTemplate, templateId]);

  useEffect(() => {
    /**
     * if state change to open, graph should be initialized,
     * but not if changing from being open
     */
    if (isOpen.current && !open) return;
    if (!entry) return;
    const entryReadyPromise = isNewEntry(entry)
      ? Promise.resolve()
      : refreshEntry(entry);
    runAsync(
      entryReadyPromise.then(() => {
        const newGraph = new Graph(entry.getMetadata().exportRDFJSON());
        if (entry.isReference() || entry.isLinkReference()) {
          newGraph.addAll(entry.getCachedExternalMetadata(), 'external');
        }
        newGraph.setChanged(false);
        setGraph(newGraph);
      })
    );
  }, [entry, open, runAsync]);

  useEffect(() => {
    if (!graph || !template) return;
    try {
      setEditor(
        new RDFormsEditor({
          graph,
          includeLevel,
          resource: resourceUri,
          template,
          compact: false,
          filterPredicates,
        })
      );
    } catch (err) {
      console.error('bypassing rdforms issues, caused by: ', err);
    }
  }, [resourceUri, filterPredicates, includeLevel, template, graph]);

  useEffect(() => {
    if (open) {
      isOpen.current = true;
    }
  }, [open]);

  useEffect(() => {
    /**
     * Makes sure the editor and graph is reset when the
     * open state is used and when it's not possible
     * to rely on unmounting to reset the states.
     */
    if (isOpen.current && !open) {
      isOpen.current = false;
      setEditor(null);
      setGraph(null);
    }
  }, [entry, open]);

  return {
    EditorComponent: editor?.domNode.component,
    editor,

    /**
     * usually used in saveActions that's why its exposed
     * @type {Graph}
     */
    graph,
  };
};

export default useRDFormsEditor;
