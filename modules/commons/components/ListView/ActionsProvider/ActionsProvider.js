import { useCallback, useState, useContext, createContext } from 'react';
import PropTypes from 'prop-types';
import { useUniqueId } from 'commons/hooks/useUniqueId';
import ErrorBoundary from 'commons/errors/ErrorBoundary';
import SnackbarFallbackComponent from 'commons/errors/SnackbarFallbackComponent';

const ActionsContext = createContext();

/**
 * Hook to read context for opening and closing actions menu and action dialogs.
 *
 * @returns {object}
 */
export const useActions = () => {
  const context = useContext(ActionsContext);

  if (context === undefined) {
    throw new Error('Actions context must be used within provider');
  }

  return context;
};

// Wrapper for action dialogs to provide extra props.
const ActionDialog = ({ Dialog, ...actionProps }) => {
  const { closeActionDialog } = useActions();

  return (
    <ErrorBoundary
      FallbackComponent={SnackbarFallbackComponent}
      onError={closeActionDialog}
    >
      <Dialog closeDialog={closeActionDialog} {...actionProps} />
    </ErrorBoundary>
  );
};

ActionDialog.propTypes = {
  Dialog: PropTypes.func.isRequired,
};

/**
 * ListItemActions handles common state for action menu and action menu items,
 * primarily for open/closing menu and dialogs.
 * ActionDialog must be placed on this level in the ListItemActions, since it
 * can't be a child of the ActionsMenu which is closed when an ActionDialog is
 * opened.
 */
export const ActionsProvider = ({ children }) => {
  const [anchorEl, setAnchorEl] = useState(null);
  const [selectedAction, setSelectedAction] = useState(null);
  /*
   * Gives the list item a unqiue id, that for example can be used for
   * accessibility
   */
  const id = useUniqueId('action-');

  const openMenu = useCallback((event) => {
    event.stopPropagation();
    event.preventDefault();
    setAnchorEl(event.currentTarget);
  }, []);

  const closeMenu = useCallback(() => {
    setAnchorEl(null);
  }, []);

  const openActionDialog = useCallback(
    (action) => {
      closeMenu();
      setSelectedAction(action);
    },
    [closeMenu]
  );

  const closeActionDialog = useCallback(() => {
    setSelectedAction(null);
  }, []);

  const isOpen = Boolean(anchorEl);

  return (
    <>
      <ActionsContext.Provider
        value={{
          anchorEl,
          id,
          isOpen,
          closeMenu,
          openMenu,
          closeActionDialog,
          openActionDialog,
        }}
      >
        {children}
        {selectedAction ? <ActionDialog {...selectedAction} /> : null}
      </ActionsContext.Provider>
    </>
  );
};

ActionsProvider.propTypes = {
  id: PropTypes.string,
  children: PropTypes.node,
};

// hoc to enhance component with Actions provider
export const withActionsProvider = (Component) => (props) =>
  (
    <ActionsProvider>
      <Component {...props} />
    </ActionsProvider>
  );
