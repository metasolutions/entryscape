import PropTypes from 'prop-types';
import Table from '@mui/material/Table';
import TableHead from '@mui/material/TableHead';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import Typography from '@mui/material/Typography';
import { useTranslation } from 'commons/hooks/useTranslation';
import { getPercentage } from 'harvest/util/data';
import 'harvest/Statistics/tables/index.scss';

const DatasetMetadata = ({ stats, nlsBundle }) => {
  const { totalDatasetCount } = stats;
  const datasetData = [
    {
      name: 'hasTitle',
      nlsKey: 'title',
    },
    {
      name: 'multilingualTitle',
      nlsKey: 'titleMultilingual',
    },
    {
      name: 'missingLanguageOnTitle',
      nlsKey: 'titleNoLanguage',
    },
    {
      name: 'hasDescription',
      nlsKey: 'description',
    },
    {
      name: 'multilingualDescription',
      nlsKey: 'descriptionMultilingual',
    },
    {
      name: 'missingLanguageOnDescription',
      nlsKey: 'descriptionNoLanguage',
    },
    {
      name: 'hasPublisher',
      nlsKey: 'publisher',
    },
    {
      name: 'hasContactPoint',
      nlsKey: 'contactPoint',
    },
  ];

  const t = useTranslation(nlsBundle);

  return (
    <div>
      <Typography
        variant="h6"
        component="h2"
        className="esreStatistics__header"
      >
        {t('metadataPerDataset')}
      </Typography>
      <TableContainer component={Paper}>
        <Table size="small" aria-label={t('metadataPerDataset')}>
          <TableHead>
            <TableRow>
              <TableCell align="left" className="esreTable__th--bold">
                {t('field')}
              </TableCell>
              <TableCell className="esreTable__th--bold" align="center">
                {t('numberDatasets')}
              </TableCell>
              <TableCell className="esreTable__th--bold" align="center">
                {t('percentageDatasets')}
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {datasetData.map((data) => (
              <TableRow hover key={data.name}>
                <TableCell
                  className="esreTable__th--bold"
                  component="th"
                  scope="row"
                >
                  {t(data.nlsKey)}
                </TableCell>
                <TableCell align="center">{stats[data.name]}</TableCell>
                <TableCell align="center">
                  {getPercentage(stats[data.name], totalDatasetCount, 1)}
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </div>
  );
};

DatasetMetadata.propTypes = {
  stats: PropTypes.shape().isRequired,
  nlsBundle: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
};
export default DatasetMetadata;
