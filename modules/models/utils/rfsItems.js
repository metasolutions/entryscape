import { Graph } from '@entryscape/rdfjson';

/**
 *
 * @param {Graph} graph
 * @param {string} resourceURI
 * @param {object[]} rfsItems
 * @returns {object|undefined}
 */
export const findAndCreateRfsItem = (graph, resourceURI, rfsItems) => {
  return rfsItems
    .map((RfsField) => new RfsField({ graph, resourceURI }))
    .find((rfsField) => {
      return rfsField.validateIsType();
    });
};

/**
 *
 * @param {string} rdfType
 * @param {object} rdfTypeToRfsItem
 * @returns {object|undefined}
 */
export const findRfsItemByRdfType = (rdfType, rdfTypeToRfsItem) => {
  return rdfTypeToRfsItem[rdfType];
};
