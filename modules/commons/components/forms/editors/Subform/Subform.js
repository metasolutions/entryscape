import PropTypes from 'prop-types';
import { graphPropType } from 'commons/util/entry';
import { nlsBundlesPropType } from 'commons/hooks/useTranslation';
import { useBlankEdit } from '../hooks/useBlankEdit';
import { Fields } from '../Fields';
import { Field } from '../Field';
import { findGroupsByResourceURI } from '../utils/editorContext';
import {
  REFRESH,
  REMOVE_FIELD_GROUPS,
  useEditorContext,
} from '../EditorContext';
import './Subform.scss';

export const Subform = ({
  graph,
  resourceURI,
  rdfType,
  property,
  translate,
  dispatch,
  size,
  label,
  editors,
  placeholderNlsKey,
  ...fieldsProps
}) => {
  const {
    fieldGroup: subformGroup,
    addBlank,
    labelId,
  } = useBlankEdit({
    name: fieldsProps.name,
    graph,
    resourceURI,
    rdfType,
    property,
    dispatch,
    insertEmptyField: false,
  });
  const { fieldSet } = useEditorContext();

  const handleClear = (subform) => {
    subform.clear();
    const fieldGroups = findGroupsByResourceURI(
      fieldSet,
      subform.getBlankNodeId()
    );
    for (const fieldGroup of fieldGroups) {
      fieldGroup.clear();
      fieldGroup.setError();
    }
    dispatch({ type: REFRESH });
  };

  const handleRemove = (subform) => {
    const fieldGroups = findGroupsByResourceURI(
      fieldSet,
      subform.getBlankNodeId()
    );
    subformGroup.removeField(subform);
    dispatch({
      type: REMOVE_FIELD_GROUPS,
      fieldGroups,
    });
  };

  const handleChildChange = (subform) => {
    const fieldGroups = findGroupsByResourceURI(
      fieldSet,
      subform.getBlankNodeId()
    );
    const hasValues = fieldGroups.some((fieldGroup) => {
      return fieldGroup.hasValue();
    });
    if (!hasValues) {
      fieldGroups.forEach((fieldGroup) => {
        fieldGroup.setError();
      });
    }
    subform.setAsserted(hasValues);
  };

  const validateSubform = (validate, subform) => {
    return (required) => {
      return validate(required && subform.isAsserted());
    };
  };

  const subforms = subformGroup.getFields();

  return (
    <Fields labelId={labelId} label={label} addField={addBlank}>
      {subforms.length ? (
        subforms.map((subform) => {
          return (
            <Field
              key={subform.getId()}
              size={subforms.length}
              onClear={() => handleClear(subform)}
              onRemove={() => handleRemove(subform)}
            >
              <div className="escoSubformEditor">
                {editors.map(({ Editor, labelNlsKey, ...editorProps }) => (
                  <Editor
                    key={editorProps.name}
                    label={translate(labelNlsKey)}
                    resourceURI={subform.getBlankNodeId()}
                    graph={graph}
                    size={size}
                    onChange={() => handleChildChange(subform)}
                    dispatch={dispatch}
                    validate={(validate) => validateSubform(validate, subform)}
                    {...editorProps}
                  />
                ))}
              </div>
            </Field>
          );
        })
      ) : (
        <div className="escoSubformEditor">
          <div className="escoSubformEditor__placeholder">
            {translate(placeholderNlsKey)}
          </div>
        </div>
      )}
    </Fields>
  );
};

Subform.propTypes = {
  children: PropTypes.node,
  property: PropTypes.string,
  label: PropTypes.string,
  placeholderNlsKey: PropTypes.string,
  size: PropTypes.string,
  graph: graphPropType,
  resourceURI: PropTypes.string,
  rdfType: PropTypes.string,
  dispatch: PropTypes.func,
  editors: PropTypes.arrayOf(PropTypes.shape({})),
  nlsBundles: nlsBundlesPropType,
  translate: PropTypes.func,
};
