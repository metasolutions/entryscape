import PropTypes from 'prop-types';
import { useState } from 'react';
import { Entry } from '@entryscape/entrystore-js';
import { Checkbox } from '@mui/material';
import {
  ListItemAction,
  withListModelProvider,
} from 'commons/components/ListView';
import ListActionDialog from 'commons/components/common/dialogs/ListActionDialog';
import LinkedDataBrowserDialog from 'commons/components/LinkedDataBrowserDialog';
import { useTranslation } from 'commons/hooks/useTranslation';
import esteCollectionNLS from 'terms/nls/esteCollection.nls';
import escoListNLS from 'commons/nls/escoList.nls';
import { getFullLengthLabel, getLabel } from 'commons/util/rdfUtils';
import { truncate } from 'commons/util/util';
import { replaceConcepts } from 'terms/collections/collectionUtils';
import { TreePath } from 'commons/components/rdforms/SkosChooser/EntrySelectListItem';
import useAsync, { PENDING } from 'commons/hooks/useAsync';
import LoadingButton from 'commons/components/LoadingButton';
import { ErrorCatcher } from 'commons/errors/ErrorCatcher';
import { ErrorWithMessage } from 'commons/errors/utils/errors';
import { useConfirmCloseAction } from 'commons/components/EntryListView/dialogs/create/handlers';
import {
  EntryListView,
  INFO_COLUMN,
  MODIFIED_COLUMN,
  TITLE_COLUMN,
} from 'commons/components/EntryListView';
import { LIST_ACTION_INFO } from 'commons/components/EntryListView/actions';
import useGetConcepts from './hooks/useGetConcepts';
import './EditCollectionConceptsDialog.scss';

const NLS_BUNDLES = [esteCollectionNLS, escoListNLS];

const ROWS_PER_PAGE = 5;

const EditCollectionConceptsDialog = ({ actionParams, closeDialog }) => {
  const translate = useTranslation(NLS_BUNDLES);
  const [collectionEntry, collectionConcepts, onConceptsChange] = actionParams;

  const {
    runAsync: runSave,
    status: saveStatus,
    error: editError,
  } = useAsync();

  const confirmClose = useConfirmCloseAction(closeDialog);

  const initialRURIs = collectionConcepts.map((entry) =>
    entry.getResourceURI()
  );
  const [collectionConceptRURIs, setCollectionConceptRURIs] =
    useState(initialRURIs);
  const edited = !(
    initialRURIs.every((uri) => collectionConceptRURIs.includes(uri)) &&
    collectionConceptRURIs.length === initialRURIs.length
  );

  const { listItems, size, search, includeFilters, filtersProps, status } =
    useGetConcepts(collectionConcepts, translate);

  const handleCheckboxChange = (event, entry) => {
    const { checked } = event.target;

    if (checked) {
      setCollectionConceptRURIs((prevRURIs) => [
        ...prevRURIs,
        entry.getResourceURI(),
      ]);
      return;
    }

    setCollectionConceptRURIs((prevRURIs) =>
      prevRURIs.filter((resourceURI) => resourceURI !== entry.getResourceURI())
    );
  };

  const handleSave = () =>
    runSave(
      replaceConcepts(collectionEntry, collectionConceptRURIs)
        .then(onConceptsChange)
        .then(closeDialog)
        .catch((error) => {
          throw new ErrorWithMessage(translate('editCollectionFail'), error);
        })
    );

  const dialogActions = (
    <LoadingButton
      onClick={handleSave}
      loading={saveStatus === PENDING}
      disabled={!edited}
    >
      {translate('editConceptsSaveButton')}
    </LoadingButton>
  );

  return (
    <>
      <ListActionDialog
        closeDialog={() => confirmClose(edited)}
        actions={dialogActions}
        title={translate('editConceptsDialogTitle')}
        maxWidth="lg"
        fixedHeight
      >
        <EntryListView
          status={status}
          search={search}
          size={size}
          items={listItems}
          includeFilters={includeFilters}
          filtersProps={filtersProps}
          nlsBundles={NLS_BUNDLES}
          showPlaceholder={Boolean(search)}
          columns={[
            {
              ...TITLE_COLUMN,
              xs: 8,
              getProps: ({ entry, path }) => ({
                primary: getLabel(entry),
                secondary: <TreePath conceptEntry={entry} entries={path} />,
              }),
            },
            MODIFIED_COLUMN,
            {
              ...INFO_COLUMN,
              getProps: ({ entry }) => ({
                action: {
                  ...LIST_ACTION_INFO,
                  entry,
                  nlsBundles: NLS_BUNDLES,
                  Dialog: LinkedDataBrowserDialog,
                },
                title: translate('infoEntry'),
              }),
            },
            {
              id: 'select-concept',
              Component: ListItemAction,
              getProps: ({ entry }) => ({
                children: (
                  <Checkbox
                    checked={collectionConceptRURIs.includes(
                      entry.getResourceURI()
                    )}
                    inputProps={{
                      'aria-label': `${truncate(getFullLengthLabel(entry), 60)} checkbox`,
                    }}
                    onChange={(event) => handleCheckboxChange(event, entry)}
                  />
                ),
              }),
            },
          ]}
        />
      </ListActionDialog>
      <ErrorCatcher error={editError} />
    </>
  );
};

EditCollectionConceptsDialog.propTypes = {
  actionParams: PropTypes.arrayOf(
    PropTypes.oneOfType([
      PropTypes.instanceOf(Entry),
      PropTypes.arrayOf(PropTypes.instanceOf(Entry)),
      PropTypes.func,
    ])
  ),
  closeDialog: PropTypes.func,
};

export default withListModelProvider(EditCollectionConceptsDialog, () => ({
  limit: ROWS_PER_PAGE,
  showFilters: true,
}));
