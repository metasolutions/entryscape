import LinkedDataBrowserDialog from 'commons/components/LinkedDataBrowserDialog';
import { VisualizationsProvider } from 'catalog/datasets/DatasetOverview/hooks/useVisualizations';
import Overview from 'commons/components/overview/Overview';
import {
  withOverviewModelProvider,
  OverviewSection,
  overviewPropsPropType,
} from 'commons/components/overview';
import {
  ACTION_REMOVE,
  ACTION_INFO_WITH_ICON,
} from 'commons/components/overview/actions';
import SuggestionList from './SuggestionList';
import useDatasetOverviewCommons, {
  nlsBundles,
} from './useDatasetOverviewCommons';
import DatasetsInSeriesList from './DatasetsInSeriesList';
import { SERIES_TAG, TEMPLATE_TAG } from '../tags';

const DatasetSeriesOverview = ({ overviewProps }) => {
  const {
    entry,
    descriptionItems,
    sidebarActions: initialSidebarActions,
    rowActions,
    includeSuggestions,
    backLabel,
    translate,
    renderPrimaryContent,
  } = useDatasetOverviewCommons();

  // Use map over directly overriding the remove action to leave space for future modifications
  const sidebarActions = initialSidebarActions
    .filter((item) => item.id !== 'copy')
    .map((item) => {
      if (item.id === ACTION_REMOVE.id)
        return {
          ...item,
          action: {
            ...item.action,
            removeConfirmMessage: translate('removeDatasetSeriesQuestion'),
          },
        };
      return item;
    });

  return (
    <VisualizationsProvider datasetEntry={entry}>
      <Overview
        {...overviewProps}
        backLabel={backLabel}
        headerAction={{
          ...ACTION_INFO_WITH_ICON,
          Dialog: LinkedDataBrowserDialog,
        }}
        entry={entry}
        nlsBundles={nlsBundles}
        descriptionItems={descriptionItems}
        sidebarActions={sidebarActions}
        rowActions={rowActions}
        tags={[SERIES_TAG, TEMPLATE_TAG]}
        renderPrimaryContent={renderPrimaryContent}
      >
        <OverviewSection>
          <DatasetsInSeriesList />
        </OverviewSection>
        {includeSuggestions ? (
          <OverviewSection>
            <SuggestionList entry={entry} />
          </OverviewSection>
        ) : null}
      </Overview>
    </VisualizationsProvider>
  );
};

DatasetSeriesOverview.propTypes = {
  overviewProps: overviewPropsPropType,
};

export default withOverviewModelProvider(DatasetSeriesOverview);
