const ticksColor = '#d3d5d5';
const gridLinesColor = '#eeeeee';

const getDateRange = () => {
  const startingDate = new Date();
  startingDate.setDate(startingDate.getDate() - 7);

  return { min: startingDate, max: new Date() };
};

export const getPlaceholderOptions = () => {
  return {
    maintainAspectRatio: false,
    tooltips: {
      enabled: false,
    },
    hover: {
      mode: null,
    },
    scales: {
      xAxes: [
        {
          type: 'time',
          time: {
            unit: 'day',
          },
          offset: true,
          ticks: {
            fontColor: ticksColor,
            ...getDateRange(),
          },
          gridLines: {
            color: gridLinesColor,
          },
        },
      ],
      yAxes: [
        {
          ticks: {
            min: 0,
            max: 2,
            stepSize: 1,
            maxTicksLimit: 3,
            fontColor: ticksColor,
          },
          gridLines: {
            zeroLineColor: gridLinesColor,
            color: gridLinesColor,
          },
        },
      ],
    },
  };
};
