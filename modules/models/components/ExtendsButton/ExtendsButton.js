import { useState } from 'react';
import PropTypes from 'prop-types';
import { graphPropType } from 'commons/util/entry';
import { Button, Collapse } from '@mui/material';
import {
  ExpandLess as ExpandLessIcon,
  ExpandMore as ExpandMoreIcon,
} from '@mui/icons-material';
import { getTitle } from 'commons/util/metadata';
import Tooltip from 'commons/components/common/Tooltip';
import { useTranslation } from 'commons/hooks/useTranslation';
import esmoCommonsNls from 'models/nls/esmoCommons.nls';
import './ExtendsButton.scss';

export const ExtendsButton = ({
  graph,
  resourceURI,
  initiallyExpanded = false,
  Presenter,
  ...presenterProps
}) => {
  const t = useTranslation(esmoCommonsNls);
  const [expanded, setExpanded] = useState(initiallyExpanded);
  const expandLabel = `${t('overridesButtonLabel')} ${getTitle(
    graph,
    resourceURI
  )}`;
  const collapseLabel = `${t('overridesButtonLabel')} ${getTitle(
    graph,
    resourceURI
  )}`;
  const buttonLabel = expanded ? collapseLabel : expandLabel;

  const toggleExpand = () => {
    setExpanded((currentExpanded) => !currentExpanded);
  };

  return graph ? (
    <div className="esmoExtends">
      <Tooltip
        title={expanded ? t('hideOverriddenLabel') : t('showOverriddenLabel')}
      >
        <Button
          className="esmoExtendsButton"
          variant="text"
          endIcon={expanded ? <ExpandLessIcon /> : <ExpandMoreIcon />}
          onClick={toggleExpand}
          aria-expanded={expanded}
          aria-label={buttonLabel}
        >
          {buttonLabel}
        </Button>
      </Tooltip>
      <Collapse
        className="esmoExtends__collapse"
        classes={{
          container: 'esmoExtends__collapse',
        }}
        in={expanded}
        timeout="auto"
        unmountOnExit
      >
        <Presenter
          graph={graph}
          resourceURI={resourceURI}
          {...presenterProps}
          label=""
        />
      </Collapse>
    </div>
  ) : null;
};

ExtendsButton.propTypes = {
  graph: graphPropType,
  resourceURI: PropTypes.string,
  initiallyExpanded: PropTypes.bool,
  collapseClasses: PropTypes.shape({}),
  Presenter: PropTypes.func,
};
