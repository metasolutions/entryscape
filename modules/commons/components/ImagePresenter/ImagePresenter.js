import { useEffect } from 'react';
import PropTypes from 'prop-types';
import Alert from '@mui/material/Alert';
import { entrystore } from 'commons/store';
import { useTranslation } from 'commons/hooks/useTranslation';
import escoContentViewNLS from 'commons/nls/escoContentview.nls';
import { entityTypePropType } from 'commons/types/EntityType';
import { EntityType } from 'entitytype-lookup';
import { getLabel } from 'commons/util/rdfUtils';
import { spreadEntry } from 'commons/util/store';
import { Entry, factory } from '@entryscape/entrystore-js';
import useAsync from 'commons/hooks/useAsync';
import './ImagePresenter.scss';

const NLS_BUNDLES = [escoContentViewNLS];

/**
 *
 * @param {Entry} entry
 * @param {EntityType} entityType
 * @returns {string|null}
 */
const getImgSrc = (entry, entityType) => {
  const { metadata, ruri: uri, info } = spreadEntry(entry);
  const contentviewers = entityType.get('contentviewers');
  const imageContentViewDef = contentviewers.find((contentView) =>
    typeof contentView === 'object'
      ? contentView.name === 'imageview'
      : contentView === 'imageview'
  );

  if (imageContentViewDef.property) {
    const src = metadata.findFirstValue(null, imageContentViewDef.property);
    if (src) return src;
  } else if (entry.isLink() || info.getFormat()?.startsWith('image/')) {
    const entrystoreBaseURI = entrystore.getBaseURI();
    const isExternal = !uri.startsWith(entrystoreBaseURI);
    return isExternal ? factory.getProxyURI(entrystoreBaseURI, uri) : uri;
  }

  return null;
};

const ImagePresenter = ({ entry, entityType, refreshKey = '0' }) => {
  const { runAsync, isLoading } = useAsync();
  const translate = useTranslation(NLS_BUNDLES);

  useEffect(() => {
    const refreshEntry = async () => {
      await entry.refresh();
    };
    runAsync(refreshEntry());
  }, [runAsync, entry]);

  const imgSrc = !isLoading ? getImgSrc(entry, entityType) : null;

  if (isLoading) return null;

  return !imgSrc ? (
    <Alert className="escoImagePresenter__message" severity="error">
      {translate('noImageProvided')}
    </Alert>
  ) : (
    <img
      key={refreshKey}
      className="escoImagePresenter__image"
      src={imgSrc}
      alt={getLabel(entry)}
    />
  );
};

ImagePresenter.propTypes = {
  refreshKey: PropTypes.string,
  entry: PropTypes.instanceOf(Entry),
  entityType: entityTypePropType,
  entityTypeDef: PropTypes.shape({
    template: PropTypes.string,
    contentviewers: PropTypes.arrayOf(PropTypes.shape({})),
  }),
};

export default ImagePresenter;
