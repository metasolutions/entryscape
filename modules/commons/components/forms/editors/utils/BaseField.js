import { get, set } from 'lodash-es';
import { getUniqueId } from 'commons/hooks/useUniqueId';

export class BaseField {
  constructor({
    graph,
    validate = () => {},
    resourceURI,
    property,
    nodetype,
    mandatory,
  }) {
    // this._error;
    this._status = 'initial';
    this._id = getUniqueId('field-');
    this._graph = graph;
    this._resourceURI = resourceURI;
    this._validate = validate;
    this._property = property;
    this._nodetype = nodetype;
    this.isMandatory = mandatory;
    this._customProps = {};
  }

  getId() {
    return this._id;
  }

  getError() {
    return this._error;
  }

  setError(error) {
    this._error = error;
    const newStatus = error ? 'error' : 'ok';
    this._status = newStatus;
  }

  getStatus() {
    return this._status;
  }

  hasError() {
    return Boolean(this.getStatus() === 'error');
  }

  get(path) {
    return get(this._customProps, path);
  }

  set(path, value) {
    set(this._customProps, path, value);
  }

  getGraph() {
    return this._graph;
  }

  getResourceURI() {
    return this._resourceURI;
  }

  getProperty() {
    return this._property;
  }

  getNodetype() {
    return this._nodetype;
  }

  validate() {
    const validationError = this._validate(this);
    this.setError(validationError);
    return validationError;
  }
}
