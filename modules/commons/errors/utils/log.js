export const logError = (error) => {
  // eslint-disable-next-line no-undef
  if (DEVELOPMENT) {
    console.error(error);
  } else {
    const message = error?.name
      ? `An error occured: ${error.name}`
      : 'An unknown error occured.';
    console.error(message);
  }
};
