import { useEffect, useRef } from 'react';
import useAsync from 'commons/hooks/useAsync';
import sleep from 'commons/util/sleep';
import { getAllEntries } from '../utils/namespaceMetadata';

const SOLR_DELAY = 1500;

const useGetNamespaceEntries = (context, refreshCount = 0) => {
  const {
    isLoading,
    status,
    data: queryResults,
    runAsync,
  } = useAsync({
    data: {
      namespaceEntries: [],
      size: -1,
    },
  });

  const isMounted = useRef(false);

  useEffect(() => {
    const getNamespaceEntries = async () => {
      // refresh count is expected to be higher than 0 when a new namespace
      // entry has been created. Then a delay is needed for solr reindexing.
      if (refreshCount !== 0) {
        await sleep(SOLR_DELAY);
      }
      const namespaceEntries = await getAllEntries(context);
      return { namespaceEntries, size: namespaceEntries.length };
    };

    if (!isMounted.current) {
      runAsync(getNamespaceEntries());
      isMounted.current = true;
      return;
    }

    runAsync(getNamespaceEntries());
  }, [runAsync, context, refreshCount]);

  return { ...queryResults, status, isLoading };
};

export default useGetNamespaceEntries;
