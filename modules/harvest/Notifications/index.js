import PropTypes from 'prop-types';
import { useState, useEffect, useCallback } from 'react';
import {
  FormControlLabel,
  FormControl,
  Select,
  MenuItem,
  Typography,
  Divider,
  Button,
  InputLabel,
  FilledInput,
  FormHelperText,
} from '@mui/material';
import { OPTION_STRICT, OPTION_LAX } from 'harvest/Notifications/util/options';
import Switch from 'commons/components/common/Switch';
import esreHarvestNLS from 'harvest/nls/esreHarvest.nls';
import { useTranslation } from 'commons/hooks/useTranslation';
import EditNotificationsDialog from 'harvest/dialogs/Notifications/';
import { getOptionByType } from 'harvest/Notifications/handlers';
import { validateEmail } from 'commons/util/util';
import { useConfirmCloseAction } from 'commons/components/EntryListView/dialogs/create/handlers';
import 'harvest/Notifications/index.scss';

const NotificationsDialog = ({
  configSettings,
  dialogProps,
  open,
  onClose,
  onSave,
}) => {
  const { isEnabled, email, harvesting, validation } = configSettings;

  const {
    harvestNotificationLabel,
    validationNotificationLabel,
    harvestSelectOpts,
    validationSelectOpts,
    harvestNotificationOptions,
    validationNotificationOptions,
    harvestingCurrentSettings,
    validationCurrentSettings,
    showCurrentHarvestingSetting,
    showCurrentValidationSetting,
  } = dialogProps;
  const [helperText, setHelperText] = useState('');
  const [hasError, setHasError] = useState(false);
  const [hasChanges, setHasChanges] = useState(false);
  const [settings, setSettings] = useState({
    notificationEmail: email,
    harvesting,
    validation,
  });
  const [showHarvestingSettings, setShowHarvestingSetting] = useState(
    showCurrentHarvestingSetting
  );
  const [showValidationSettings, setShowValidationSetting] = useState(
    showCurrentValidationSetting
  );
  const [harvestNotification, setHarvestNotification] = useState(
    harvestingCurrentSettings
  );
  const [validationNotification, setValidationNotification] = useState(
    validationCurrentSettings
  );

  useEffect(() => {
    if (showHarvestingSettings && !harvestNotification) {
      const notification = getOptionByType(
        harvestNotificationOptions,
        OPTION_STRICT
      );
      setHarvestNotification(notification.value);
      setHasChanges(true);
    }
    if (showValidationSettings && !validationNotification) {
      const notification = getOptionByType(
        validationNotificationOptions,
        OPTION_LAX
      );
      setValidationNotification(notification.value);
      setHasChanges(true);
    }
  }, [showHarvestingSettings, showValidationSettings]);

  const handleEmailChange = (e) => {
    const isValid = validateEmail(e.target.value);
    if (!isValid) {
      setHelperText('Invalid email');
      setHasError(true);
    } else {
      setHelperText('');
      setHasError(false);
    }
    setSettings({ ...settings, notificationEmail: e.target.value });
    setHasChanges(e.target.value !== email);
  };

  const handleHarvestSelect = (e) => {
    setHarvestNotification(e.target.value);
    setHasChanges(true);
  };

  const handleValidationSelect = (e) => {
    setValidationNotification(e.target.value);
    setHasChanges(true);
  };

  const handleSaveForm = () => {
    const harvestNotificationOpt = showHarvestingSettings
      ? harvestNotification
      : 0;
    const validationNotificationOpt = showValidationSettings
      ? validationNotification
      : 0;

    const updatedSettings = {
      isEnabled,
      email: settings.notificationEmail,
      harvestingValue: harvestNotificationOpt,
      validationValue: validationNotificationOpt,
    };
    onSave(updatedSettings);
  };

  const t = useTranslation(esreHarvestNLS);

  const { notificationEmail } = settings;
  const isNotificationDisabled = isEnabled !== 'true';
  const emailInputProp = {
    'aria-describedby': 'helper-text',
  };

  const buttonDisabled = hasError || !hasChanges;
  const confirmCloseAction = useConfirmCloseAction(onClose);
  const confirmClose = useCallback(() => {
    confirmCloseAction(!buttonDisabled);
  }, [buttonDisabled, confirmCloseAction]);

  // This is a save button, not an edit as the nls string suggests
  const dialogAction = (
    <Button onClick={handleSaveForm} disabled={buttonDisabled}>
      {t('epEdit')}
    </Button>
  );
  return (
    <EditNotificationsDialog
      open={open}
      onClose={confirmClose}
      dialogAction={dialogAction}
    >
      <form className="esreNotifications__form" noValidate autoComplete="off">
        <FormControl margin="none">
          <InputLabel
            classes={{ root: 'esreNotifications__inputLabel' }}
            htmlFor="email"
          >
            {t('email')}
          </InputLabel>
          <FilledInput
            fullWidth
            id="email"
            type="email"
            value={notificationEmail}
            onChange={handleEmailChange}
            error={hasError}
            inputProps={hasError ? emailInputProp : {}}
          />
          {helperText && (
            <FormHelperText error={hasError} id="helper-text">
              {helperText}
            </FormHelperText>
          )}
        </FormControl>
        <Typography
          classes={{ root: 'esreNotifications__typography' }}
          variant="body1"
        >
          {t('emailAlert')}
        </Typography>
        <Typography classes={{ root: 'esreNotifications__h2' }} variant="h2">
          {t('harvestingStatusHeader')}
        </Typography>
        <Divider />
        <FormControlLabel
          classes={{ root: 'esreNotifications__formControlLabel' }}
          control={
            <Switch
              checked={showHarvestingSettings}
              onChange={() => {
                setHasChanges(
                  showHarvestingSettings === showCurrentHarvestingSetting
                );
                setShowHarvestingSetting(!showHarvestingSettings);
              }}
              name="harvestingStatus"
              color="primary"
              disabled={isNotificationDisabled}
            />
          }
          label={
            showHarvestingSettings
              ? t('notificationsOn')
              : t('notificationsOff')
          }
        />
        {showHarvestingSettings && (
          <FormControl
            classes={{ root: 'esreNotifications__formControl' }}
            variant="filled"
            margin="none"
          >
            <Typography
              classes={{ root: 'esreNotifications__label' }}
              id="harvesting-status-notification"
              variant="body1"
            >
              {harvestNotificationLabel}
            </Typography>
            <Select
              value={harvestNotification || 1}
              onChange={handleHarvestSelect}
              classes={{ select: 'esreNotifications__select' }}
              inputProps={{
                'aria-labelledby': 'harvesting-status-notification',
              }}
            >
              {harvestSelectOpts.map((opt, idx) => (
                <MenuItem key={`harvest_${opt.label}`} value={idx + 1}>
                  {opt.label}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
        )}
        <Typography classes={{ root: 'esreNotifications__h2' }} variant="h2">
          {t('dataValidationHeader')}
        </Typography>
        <Divider />
        <FormControlLabel
          classes={{ root: 'esreNotifications__formControlLabel' }}
          control={
            <Switch
              checked={showValidationSettings}
              onChange={() => {
                setHasChanges(
                  !showValidationSettings !== showCurrentValidationSetting
                );
                setShowValidationSetting(!showValidationSettings);
              }}
              name="dataValidation"
              color="primary"
              disabled={isNotificationDisabled}
            />
          }
          label={
            showValidationSettings
              ? t('notificationsOn')
              : t('notificationsOff')
          }
        />
        {showValidationSettings && (
          <FormControl
            classes={{ root: 'esreNotifications__formControl' }}
            variant="filled"
            margin="none"
          >
            <Typography
              classes={{ root: 'esreNotifications__label' }}
              id="data-validation-notification"
              variant="body1"
            >
              {validationNotificationLabel}
            </Typography>
            <Select
              value={validationNotification || 1}
              onChange={handleValidationSelect}
              classes={{ select: 'esreNotifications__select' }}
              inputProps={{
                'aria-labelledby': 'data-validation-notification',
              }}
            >
              {validationSelectOpts.map((opt, idx) => (
                <MenuItem key={`validation_${opt.label}`} value={idx + 1}>
                  {opt.label}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
        )}
      </form>
    </EditNotificationsDialog>
  );
};

NotificationsDialog.propTypes = {
  configSettings: PropTypes.shape({
    isEnabled: PropTypes.string,
    email: PropTypes.string,
    harvesting: PropTypes.string,
    validation: PropTypes.string,
  }),
  dialogProps: PropTypes.shape({
    harvestNotificationLabel: PropTypes.string,
    validationNotificationLabel: PropTypes.string,
    harvestSelectOpts: PropTypes.arrayOf(PropTypes.shape({})),
    harvestNotificationOptions: PropTypes.arrayOf(PropTypes.shape({})),
    validationNotificationOptions: PropTypes.arrayOf(PropTypes.shape({})),
    validationSelectOpts: PropTypes.arrayOf(PropTypes.shape({})),
    harvestingCurrentSettings: PropTypes.number,
    validationCurrentSettings: PropTypes.number,
    showCurrentHarvestingSetting: PropTypes.bool,
    showCurrentValidationSetting: PropTypes.bool,
  }),
  open: PropTypes.bool,
  onClose: PropTypes.func,
  onSave: PropTypes.func,
};

export default NotificationsDialog;
