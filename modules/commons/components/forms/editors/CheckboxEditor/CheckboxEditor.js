import PropTypes from 'prop-types';
import { graphPropType } from 'commons/util/entry';
import { Box, Grid, Checkbox } from '@mui/material';
import { XSD_BOOLEAN } from '../../utils/nodetypes';
import { useFieldsEdit } from '../hooks/useFieldsEdit';
import { FieldsWrapper } from '../Fields';
import { FieldsLabel } from '../FieldsLabel';

export const CheckboxEditor = ({
  name,
  property,
  nodetype,
  label,
  xs = 6,
  graph,
  resourceURI,
  initialValue,
  dispatch,
}) => {
  const { fields, labelId, updateValue } = useFieldsEdit({
    name,
    property,
    nodetype,
    graph,
    resourceURI,
    dispatch,
    initialValue,
    datatype: XSD_BOOLEAN,
  });
  const [field] = fields;

  return (
    <Grid container item xs={xs}>
      <FieldsWrapper>
        <Grid item>
          <Box display="flex" alignItems="center">
            <Checkbox
              inputProps={{ 'aria-labelledby': labelId }}
              color="primary"
              checked={field.getValue() === 'true'}
              onChange={({ target }) => updateValue(field, `${target.checked}`)}
            />
            <Box
              sx={{
                marginBottom: '-5px',
                cursor: 'pointer',
                userSelect: 'none',
              }}
            >
              <FieldsLabel
                onClick={() =>
                  updateValue(
                    field,
                    field.getValue() === 'true' ? 'false' : 'true'
                  )
                }
                label={label}
                id={labelId}
              />
            </Box>
          </Box>
        </Grid>
      </FieldsWrapper>
    </Grid>
  );
};

CheckboxEditor.propTypes = {
  name: PropTypes.string,
  label: PropTypes.string,
  nodetype: PropTypes.string.isRequired,
  property: PropTypes.string.isRequired,
  xs: PropTypes.number,
  graph: graphPropType,
  resourceURI: PropTypes.string,
  dispatch: PropTypes.func,
  initialValue: PropTypes.string,
};
