import { Context, Entry } from '@entryscape/entrystore-js';
import { Graph } from '@entryscape/rdfjson';
import { getOrIgnoreEntry } from 'commons/util/entry';
import { getNamespaceUri } from 'models/namespaces/utils/namespaceMetadata';
import * as ns from 'models/utils/ns';
import { getRfsProperty } from './rfsPropertyMap';

/**
 *
 * @param {{entry: Entry, graph: Graph, resourceURI: string}} entryOrGraphProps
 * @returns {{graph: Graph, resourceURI: string}}
 */
export const getEntryProps = (entryOrGraphProps) => {
  const { entry, graph, resourceURI } = entryOrGraphProps;
  return {
    graph: graph || entry.getMetadata(),
    resourceURI: resourceURI || entry.getResourceURI(),
  };
};

/**
 * Finds text and language value(s) of an entry predicate
 *
 * @param {Graph} graph
 * @param {string} resourceURI
 * @param {string} property
 * @returns {object|undefined}
 */
export const getLanguageLiterals = (graph, resourceURI, property) => {
  const texts = {};
  const statements = graph.find(resourceURI, property);
  for (const statement of statements) {
    texts[statement.getLanguage() ?? ''] = statement.getValue();
  }
  if (!Object.keys(texts).length) return;
  return texts;
};

/**
 *
 * @param {Graph} graph
 * @param {string} resourceURI
 * @param {string} property
 * @param {object} lang2val
 */
export const addLanguageLiterals = (graph, resourceURI, property, lang2val) => {
  for (const [language, value] of Object.entries(lang2val)) {
    graph.addL(resourceURI, property, value, language);
  }
};

export const getRfsLanguageLiterals = (graph, resourceURI, properties) => {
  return properties.reduce((rfsObject, property) => {
    const languageLiterals = getLanguageLiterals(graph, resourceURI, property);
    if (languageLiterals) {
      rfsObject[getRfsProperty(property)] = languageLiterals;
    }
    return rfsObject;
  }, {});
};

/**
 * Project name of an entry
 *
 * @param {Context} context
 * @returns {Promise<string>}
 */
export const getModelName = async (context) => {
  const contextEntry = await context.getEntry();
  const resourceURI = contextEntry.getResourceURI();
  const projectName = contextEntry
    .getMetadata()
    .findFirstValue(resourceURI, 'dcterms:title');
  return projectName;
};

/**
 * Get the most specific RDF type (field or form subclasses) as well as an appropriate NLSkey
 *
 * @param {{entry: Entry, graph: Graph, resourceURI: string}} entryOrGraphProps
 * @returns {{labelNlsKey: string, rdfType: string}|undefined}
 */
export const getNLSAndRDFType = (entryOrGraphProps) => {
  const { graph, resourceURI } = getEntryProps(entryOrGraphProps);
  if (!graph) return;
  const rdfTypes = [
    { rdfType: ns.RDF_TYPE_TEXT_FIELD, labelNlsKey: 'textFieldLabel' },
    { rdfType: ns.RDF_TYPE_DATATYPE_FIELD, labelNlsKey: 'dataTypeLabel' },
    { rdfType: ns.RDF_TYPE_SELECT_FIELD, labelNlsKey: 'selectFieldLabel' },
    { rdfType: ns.RDF_TYPE_LOOKUP_FIELD, labelNlsKey: 'lookupFieldLabel' },
    { rdfType: ns.RDF_TYPE_INLINE_FIELD, labelNlsKey: 'inlineFieldLabel' },
    { rdfType: ns.RDF_TYPE_SECTION_FORM, labelNlsKey: 'sectionFormLabel' },
    { rdfType: ns.RDF_TYPE_PROPERTY_FORM, labelNlsKey: 'propertyFormLabel' },
    { rdfType: ns.RDF_TYPE_PROFILE_FORM, labelNlsKey: 'profileFormLabel' },
    { rdfType: ns.RDF_TYPE_OBJECT_FORM, labelNlsKey: 'objectFormLabel' },
  ];
  return rdfTypes.find(
    (rdfTypeObj) =>
      graph.find(resourceURI, 'rdf:type', rdfTypeObj.rdfType).length
  );
};

/**
 * Get type of a field or form entry
 *
 * @param {{entry: Entry, graph: Graph, resourceURI: string, translate: Function}} properties
 * @returns {string|undefined}
 */
export const getType = ({ translate, ...entryOrGraphProps }) =>
  translate(getNLSAndRDFType(entryOrGraphProps)?.labelNlsKey);

/**
 *
 * @param {{entry: Entry, graph: Graph, resourceURI: string}} entryOrGraphProps
 * @returns {string}
 */
export const getPurpose = (entryOrGraphProps) => {
  const { graph, resourceURI } = getEntryProps(entryOrGraphProps);
  return graph.findFirstValue(resourceURI, ns.RDF_PROPERTY_PURPOSE) || '';
};

// TODO: move all functions related resource entries to ./resource.js
export const RESOURCE = 'resource';
export const RESOURCE_ENTRY = 'resourceEntry';
export const NAMESPACE_ENTRY = 'namespaceEntry';

/**
 * Get the localname and namespace uri from namespace entry. The combination is
 * used to build a complete uri.
 *
 * @param {Graph} graph
 * @param {string} blankNodeId
 * @returns {Promise<object>}
 */
export const getLocalnameAndNamespaceURI = async (graph, blankNodeId) => {
  const namespaceEntryURI = graph.findFirstValue(
    blankNodeId,
    ns.RDF_PROPERTY_NAMESPACE_ENTRY
  );
  const localname = graph.findFirstValue(
    blankNodeId,
    ns.RDF_PROPERTY_NAMESPACE_LOCALNAME
  );
  const namespaceEntry = await getOrIgnoreEntry(namespaceEntryURI);
  return {
    namespaceURI: namespaceEntry ? getNamespaceUri(namespaceEntry) : '',
    localname,
    fullUri: `${getNamespaceUri(namespaceEntry)}${localname}`,
  };
};

/**
 * @param {Graph} graph
 * @param {string} resourceURI
 * @param {string} property
 * @returns {Promise<string>}
 */
export const getNamespaceEntryValue = async (graph, resourceURI, property) => {
  const blankNodeId = graph.findFirstValue(resourceURI, property);

  if (!blankNodeId) return '';
  const { namespaceURI, fullUri } = await getLocalnameAndNamespaceURI(
    graph,
    blankNodeId
  );

  if (!namespaceURI) return '';
  return fullUri;
};

/**
 * @param {Graph} graph
 * @param {string} resourceURI
 * @param {string} property
 * @param {string} defaultEditMode
 * @returns {string}
 */
export const getURIEditorType = (
  graph,
  resourceURI,
  property,
  defaultEditMode
) => {
  const [statement] = graph.find(resourceURI, property);
  if (!statement) return defaultEditMode;

  const type = statement.getType();
  if (type !== 'bnode') return RESOURCE;

  const blankNodeId = statement.getValue();
  const nsEntryValue = graph.findFirstValue(
    blankNodeId,
    ns.RDF_PROPERTY_NAMESPACE_ENTRY
  );

  if (nsEntryValue) return NAMESPACE_ENTRY;
  return RESOURCE_ENTRY;
};

/**
 * @param {Graph} graph
 * @param {string} resourceURI
 * @param {string} property
 * @returns {Promise<string>}
 */
export const getResourceEntryValue = async (graph, resourceURI, property) => {
  const blankNodeId = graph.findFirstValue(resourceURI, property);

  if (!blankNodeId) return '';
  const resourceEntryURI = graph.findFirstValue(
    blankNodeId,
    ns.RDF_PROPERTY_RESOURCE_ENTRY
  );
  const resourceEntry = await getOrIgnoreEntry(resourceEntryURI);
  if (!resourceEntry) return '';
  return resourceEntry.getResourceURI();
};

/**
 * @param {Graph} graph
 * @param {string} resourceURI
 * @returns {Graph}
 */
export const copyTitleToLabel = (graph, resourceURI) => {
  graph
    .find(resourceURI, 'dcterms:title')
    .map((statement) => statement.getObject())
    .forEach((object) => {
      graph.add(resourceURI, ns.RDF_PROPERTY_LABEL, { ...object });
    });
  return graph;
};

/**
 * Utility to copy language literals for a given property from one graph to
 * another.
 *
 * @param {Graph} targetGraph
 * @param {string} targetResourceURI
 * @param {Graph} sourceGraph
 * @param {string} sourceResourceURI
 * @param {string} property
 * @returns {Graph}
 */
export const copyLanguageLiteral = (
  targetGraph,
  targetResourceURI,
  sourceGraph,
  sourceResourceURI,
  property
) => {
  const valueObjects = sourceGraph
    .find(sourceResourceURI, property)
    .map((statement) => statement.getCleanObject());
  for (const valueObject of valueObjects) {
    targetGraph.add(targetResourceURI, property, valueObject);
  }
  return targetGraph;
};

/**
 *
 * @param {Graph} targetGraph
 * @param {string} targetResourceURI
 * @param {Graph} sourceGraph
 * @param {string} sourceResourceURI
 * @returns {Graph}
 */
export const copyRdfType = (
  targetGraph,
  targetResourceURI,
  sourceGraph,
  sourceResourceURI
) => {
  const rdfTypes = sourceGraph
    .find(sourceResourceURI, 'rdf:type')
    .map((statement) => statement.getValue());
  for (const rdfType of rdfTypes) {
    targetGraph.add(targetResourceURI, 'rdf:type', rdfType);
  }
  return targetGraph;
};

export const matchesType = (graph, resourceURI, types) => {
  for (const type of types) {
    const [statement] = graph.find(resourceURI, 'rdf:type', type);
    if (statement) return true;
  }
  return false;
};

/**
 *
 * @param {Graph} graph
 * @param {string} resourceURI
 * @param {string} property
 * @returns {string[]}
 */
export const getBlankNodeIds = (graph, resourceURI, property) => {
  const blankNodeIds = graph
    .find(resourceURI, property)
    .map((statement) => statement.getValue());
  return blankNodeIds;
};

export const applyEntryProps = (fn, entry, ...args) => {
  return fn(entry.getMetadata(), entry.getResourceURI(), ...args);
};
