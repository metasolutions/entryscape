import PropTypes from 'prop-types';
import { AlertTitle, Alert as MuiAlert } from '@mui/material';
import {
  Error as ErrorIcon,
  Info as InfoIcon,
  CheckCircle as SuccessIcon,
  Warning as WarningIcon,
} from '@mui/icons-material';
import './Alert.scss';

const Alert = ({ children, severity = 'info', title, ...props }) => {
  return (
    <MuiAlert
      className={`escoAlert escoAlert--${severity}`}
      iconMapping={{
        error: <ErrorIcon fontSize="inherit" />,
        info: <InfoIcon fontSize="inherit" color="primary" />,
        success: <SuccessIcon fontSize="inherit" />,
        warning: <WarningIcon fontSize="inherit" />,
      }}
      severity={severity}
      {...props}
    >
      {title ? <AlertTitle>{title}</AlertTitle> : null}
      {children}
    </MuiAlert>
  );
};

Alert.propTypes = {
  children: PropTypes.node,
  severity: PropTypes.string,
  title: PropTypes.string,
};

export default Alert;
