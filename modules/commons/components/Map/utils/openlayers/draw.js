import Draw, { createBox } from 'ol/interaction/Draw';

/**
 * Get proper geometry function and geometry type for a given draw type.
 * @param {Object} drawType
 * @returns
 */
const getDrawTypeOptions = (drawType) => {
  switch (drawType) {
    case 'box':
      return {
        geometryFunction: createBox(),
        type: 'Circle',
      };
    case 'point':
      return {
        type: 'Point',
      };
    default:
      throw new Error(`Unknown draw type ${drawType}`);
  }
};

/**
 * @param {VectorSource} source
 * @param {string} interactionType
 * @returns {Draw}
 */
export const createDrawInteraction = (source, interactionType) => {
  const { type, geometryFunction } = getDrawTypeOptions(interactionType);
  return new Draw({
    source,
    type,
    geometryFunction,
  });
};
