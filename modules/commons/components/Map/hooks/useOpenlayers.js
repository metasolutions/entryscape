import { useEffect } from 'react';
import useAsync from 'commons/hooks/useAsync';

const loadOpenlayersUtils = () =>
  import(/* webpackChunkName: "openlayers" */ '../utils/openlayers');

export const useOpenlayers = () => {
  const { data: openlayersUtils, status, error, runAsync } = useAsync();

  useEffect(() => {
    if (status !== 'idle') return;
    runAsync(loadOpenlayersUtils());
  }, [status, runAsync]);

  return { ...openlayersUtils, status, error };
};
