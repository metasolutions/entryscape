import { useCallback } from 'react';
import Lookup from 'commons/types/Lookup';
import { useESContext } from 'commons/hooks/useESContext';
import { RDF_TYPE_IDEA } from 'commons/util/entry';
import { entrystore } from 'commons/store';
import { getPathFromViewName } from 'commons/util/site';
import TableView from 'commons/components/TableView';
import {
  EntryListView,
  useSolrQuery,
  TITLE_COLUMN,
  MODIFIED_COLUMN,
  ACTIONS_GROUP_COLUMN,
  TOGGLE_ENTRY_COLUMN,
} from 'commons/components/EntryListView';
import {
  LIST_ACTION_INFO,
  LIST_ACTION_EDIT,
} from 'commons/components/EntryListView/actions';
import {
  useListModel,
  TOGGLE_TABLEVIEW,
  listPropsPropType,
  withListModelProviderAndLocation,
} from 'commons/components/ListView';
import { listActions, nlsBundles } from './actions';

const IdeasView = ({ listProps }) => {
  const [{ showTableView }, dispatch] = useListModel();
  const { context } = useESContext();

  const createQuery = useCallback(
    () => entrystore.newSolrQuery().rdfType(RDF_TYPE_IDEA).context(context),
    [context]
  );
  const queryResults = useSolrQuery({ createQuery });

  return showTableView ? (
    <TableView
      setTableView={() => dispatch({ type: TOGGLE_TABLEVIEW })}
      rdfType={RDF_TYPE_IDEA}
      templateId={Lookup.getByName('datasetIdea').complementaryTemplateId()}
    />
  ) : (
    <EntryListView
      {...queryResults}
      {...listProps}
      nlsBundles={nlsBundles}
      getListItemProps={({ entry }) => ({
        to: getPathFromViewName('catalog__ideas__idea', {
          contextId: context.getId(),
          entryId: entry.getId(),
        }),
      })}
      includeTableView
      listActions={listActions}
      listActionsProps={{ nlsBundles }}
      columns={[
        {
          ...TOGGLE_ENTRY_COLUMN,
          getProps: ({ entry, translate }) => ({
            entry,
            publicTitle: translate('publicIdeaTitle'),
            privateTitle: translate('privateIdeaTitle'),
            publishDisabledTitle: translate('publishDisabledTitle'),
            toggleAriaLabel: translate('publishToggleAriaLabel'),
          }),
        },
        { ...TITLE_COLUMN, xs: 7 },
        MODIFIED_COLUMN,
        {
          ...ACTIONS_GROUP_COLUMN,
          actions: [LIST_ACTION_INFO, LIST_ACTION_EDIT],
        },
      ]}
    />
  );
};

IdeasView.propTypes = {
  listProps: listPropsPropType,
};

export default withListModelProviderAndLocation(IdeasView);
