import PropTypes from 'prop-types';
import { useEffect, useState } from 'react';
import { FormControlLabel, TextField, Checkbox } from '@mui/material';
import ContentWrapper from 'commons/components/common/ContentWrapper';
import esadGroupNLS from 'admin/nls/esadGroup.nls';
import ListActionDialog from 'commons/components/common/dialogs/ListActionDialog';
import { useTranslation } from 'commons/hooks/useTranslation';
import { CREATE, useListModel } from 'commons/components/ListView';
import { removeWhitespace } from 'commons/util/util';
import { useSnackbar } from 'commons/hooks/useSnackbar';
import useAsync, { PENDING } from 'commons/hooks/useAsync';
import LoadingButton from 'commons/components/LoadingButton';
import { ErrorCatcher } from 'commons/errors/ErrorCatcher';
import { ErrorWithMessage } from 'commons/errors/utils/errors';
import { useConfirmCloseAction } from 'commons/components/EntryListView/dialogs/create/handlers';
import { groupNameSearch, entryIdSearch, createGroupEntry } from './handler';

const CreateDialog = ({ closeDialog }) => {
  const [, dispatch] = useListModel();
  const { runAsync, status, error: createError } = useAsync();
  const [pristine, setPristine] = useState(true);
  const [timer, setTimer] = useState(null);
  const [formError, setFormError] = useState(null);
  const [groupName, setGroupName] = useState({
    value: '',
    error: null,
    isValid: true,
  });
  const [entryId, setEntryId] = useState({
    value: '',
    error: null,
    isValid: true,
  });
  const [fullGroupName, setFullGroupName] = useState({
    value: '',
    error: null,
  });
  const [showAdvancedOptions, setShowAdvancedOptions] = useState(false);
  const [buttonDisabled, setButtonDisabled] = useState(true);
  const [addSnackbar] = useSnackbar();
  const translate = useTranslation(esadGroupNLS);
  const confirmClose = useConfirmCloseAction(closeDialog);
  const close = () => confirmClose(!pristine);

  const handleChange = (state, set) => (e) => {
    set({
      ...state,
      value: removeWhitespace(e.target.value),
    });
    if (e.target.id === 'fullGroupName') {
      setPristine(false);
    }
  };

  useEffect(() => {
    if (groupName.value) {
      if (timer) {
        clearTimeout(timer);
        setTimer(null);
      }
      const t = setTimeout(
        () =>
          groupNameSearch(groupName.value).then((result) => {
            setGroupName((state) => ({
              ...state,
              isValid: !result,
              error: result,
            }));
          }),
        300
      );
      setTimer(t);
    }
    setGroupName((state) => ({ ...state, isValid: true, error: null }));
  }, [groupName.value]);

  useEffect(() => {
    if (entryId.value) {
      if (timer) {
        clearTimeout(timer);
        setTimer(null);
      }
      const t = setTimeout(
        () =>
          entryIdSearch(entryId.value).then((result) => {
            setEntryId((state) => ({
              ...state,
              isValid: !result,
              error: result,
            }));
          }),
        300
      );
      setTimer(t);
    }
    setEntryId((state) => ({ ...state, isValid: true, error: null }));
  }, [entryId.value]);

  useEffect(() => {
    if (fullGroupName.value && !pristine) {
      if (groupName.isValid && entryId.isValid) {
        setButtonDisabled(false);
      } else {
        setButtonDisabled(true);
      }
    } else {
      setButtonDisabled(true);
    }
  }, [fullGroupName.value, groupName.isValid, entryId.isValid]);

  const handleShowAdvancedOptions = () => {
    setShowAdvancedOptions(!showAdvancedOptions);
    setGroupName((state) => ({ ...state, value: '', isValid: true }));
    setEntryId((state) => ({ ...state, value: '', isValid: true }));
  };
  const handleSaveForm = async () => {
    if (groupName.isValid && entryId.isValid && fullGroupName.value) {
      const { value: name } = groupName;
      const { value: id } = entryId;
      const { value: fullName } = fullGroupName;

      return createGroupEntry({
        name,
        id,
        fullName,
      })
        .then(closeDialog)
        .then(() => {
          setPristine(true);
          clearTimeout(timer);
          setTimer(null);
        })
        .then(() => dispatch({ type: CREATE }))
        .then(() => addSnackbar({ message: translate('createGroupSuccess') }))
        .catch((err) => {
          throw new ErrorWithMessage(translate('createGroupFail'), err);
        });
    }
  };

  const dialogActions = (
    <LoadingButton
      autoFocus
      onClick={() => runAsync(handleSaveForm())}
      loading={status === PENDING}
      disabled={buttonDisabled}
    >
      {translate('createGroupButton')}
    </LoadingButton>
  );

  return (
    <>
      <ListActionDialog
        id="create-entry"
        title={translate('createGroupHeader')}
        actions={dialogActions}
        closeDialog={close}
        alert={
          formError
            ? {
                message: translate(formError),
                setMessage: setFormError,
              }
            : undefined
        }
        maxWidth="md"
      >
        <ContentWrapper>
          <form autoComplete="off">
            {showAdvancedOptions && (
              <>
                <TextField
                  id="groupName"
                  label={translate('createGroupname')}
                  value={groupName.value}
                  placeholder={translate('createGroupnamePlaceholder')}
                  onChange={handleChange(groupName, setGroupName)}
                  error={!groupName.isValid}
                  helperText={
                    groupName.error && translate(`${groupName.error}`)
                  }
                />
                <TextField
                  id="entryId"
                  label={translate('createEntryId')}
                  value={entryId.value}
                  placeholder={translate('createEntryIdPlaceholder')}
                  onChange={handleChange(entryId, setEntryId)}
                  error={!entryId.isValid}
                  helperText={entryId.error && translate(`${entryId.error}`)}
                />
              </>
            )}

            <TextField
              id="fullGroupName"
              label={translate('createFullname')}
              value={fullGroupName.value}
              placeholder={translate('createFullnamePlaceholder')}
              onChange={handleChange(fullGroupName, setFullGroupName)}
              error={!pristine && !fullGroupName.value}
              helperText={!pristine && !fullGroupName.value && 'Name required'}
              required
            />
            <FormControlLabel
              control={
                <Checkbox
                  checked={showAdvancedOptions}
                  onChange={handleShowAdvancedOptions}
                  inputProps={{
                    'aria-label': translate('advancedOptionsCheckbox'),
                  }}
                />
              }
              label={translate('advancedOptionsCheckbox')}
            />
          </form>
        </ContentWrapper>
      </ListActionDialog>
      <ErrorCatcher error={createError} />
    </>
  );
};

CreateDialog.propTypes = {
  closeDialog: PropTypes.func.isRequired,
};

export default CreateDialog;
