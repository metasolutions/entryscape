import PropTypes from 'prop-types';
import { Entry } from '@entryscape/entrystore-js';
import {
  useTranslation,
  nlsBundlesPropType,
} from 'commons/hooks/useTranslation';
import { useMainDialog } from 'commons/hooks/useMainDialog';
import {
  AcknowledgeAction,
  ConfirmAction,
} from 'commons/components/common/dialogs/MainDialog';
import { entrystore } from 'commons/store';
import { getRenderName } from 'commons/util/rdfUtils';
import { getGroupWithHomeContext } from 'commons/util/store';
import useNavigate from 'commons/components/router/useNavigate';
import RemoveEntryDialog from 'commons/components/entry/RemoveEntryDialog';

const RemoveDialog = ({ entry, nlsBundles, closeDialog, ...restProps }) => {
  const { openMainDialog } = useMainDialog();
  const translate = useTranslation(nlsBundles);
  const projectName = getRenderName(entry) || entry.getId();
  const { goBack } = useNavigate();

  const confirmRemoveAssociatedGroup = async (groupEntry) => {
    openMainDialog({
      content: translate('confirmGroupInProject'),
      actions: (
        <ConfirmAction
          onDone={() => groupEntry.del()}
          affirmLabel={translate('confirmRemove')}
          rejectLabel={translate('cancelRemove')}
        />
      ),
    });
  };

  const onRemove = async () => {
    try {
      const entryId = entry.getId();
      const context = entrystore.getContextById(entryId);
      const groupEntry = await getGroupWithHomeContext(context);
      if (groupEntry) {
        await confirmRemoveAssociatedGroup(groupEntry);
      }
      await entry.del();
    } catch (error) {
      openMainDialog({
        content: translate('failedRemoveContext'),
        actions: <AcknowledgeAction onDone={closeDialog} />,
      });
      console.error(error);
    }
  };

  return (
    <RemoveEntryDialog
      {...restProps}
      closeDialog={closeDialog}
      onRemove={onRemove}
      onRemoveCallback={goBack}
      removeConfirmMessage={translate('removeContextMessage', projectName)}
    />
  );
};

RemoveDialog.propTypes = {
  entry: PropTypes.instanceOf(Entry),
  nlsBundles: nlsBundlesPropType,
  closeDialog: PropTypes.func,
};

export default RemoveDialog;
