import escoListNLS from 'commons/nls/escoList.nls';
import escaPreparations from 'catalog/nls/escaPreparations.nls';
import escaDatasetTemplateNLS from 'catalog/nls/escaDatasetTemplate.nls';
import escaDatasetNLS from 'catalog/nls/escaDataset.nls';
import { withListRefresh } from 'commons/components/ListView';
import CreateSuggestionDialog from '../dialogs/CreateSuggestionDialog';

export const nlsBundles = [
  escaPreparations,
  escaDatasetTemplateNLS,
  escaDatasetNLS,
  escoListNLS,
];

export const listActions = [
  {
    id: 'create',
    Dialog: withListRefresh(CreateSuggestionDialog, 'onCreate'),
    labelNlsKey: 'createButtonLabel',
    tooltipNlsKey: 'actionTooltipSuggestion',
    nlsBundles,
    highlightId: 'listActionCreate',
  },
];
