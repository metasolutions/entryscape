import PropTypes from 'prop-types';
import { Grid, Typography } from '@mui/material';
import {
  Select,
  TextField,
  RemoveButton,
  AddFieldButton,
} from 'commons/components/forms/editors';
import { Group } from 'commons/components/forms/editors/Group';
import { useTranslation } from 'commons/hooks/useTranslation';
import esreHarvestNLS from 'harvest/nls/esreHarvest.nls';
import {
  CHANGE_TRANSFORM_TYPE,
  UPDATE_TRANSFORM_ARGUMENT,
  REMOVE_TRANSFORM,
  ADD_TRANSFORM,
  getTransformOptions,
  labelOverrides,
} from '../PipelineFormModel';
import './TransformsForm.scss';

const Transform = ({ id, type, args = {}, dispatch }) => {
  const translate = useTranslation([esreHarvestNLS]);
  const argumentPairs = Object.entries(args);

  const handleChangeType = ({ target }) =>
    dispatch({
      type: CHANGE_TRANSFORM_TYPE,
      transformId: id,
      newType: target.value,
    });

  const handleUpdateArgument = ({ target }, key) => {
    dispatch({
      type: UPDATE_TRANSFORM_ARGUMENT,
      transformId: id,
      key,
      newValue: target.value,
    });
  };

  return (
    <div className="esreTransformsForm" key={`transform-${id}`}>
      <Group
        direction="row"
        style={{ padding: '16px 12px 16px 24px' }} // override escoFormGroup padding
      >
        <Select
          className="esreTransformsForm__typeSelect"
          inputProps={{ 'aria-labelledby': 'transform-select-label' }}
          labelId="transform-select"
          label={translate('typeSelectLabel')}
          value={type}
          onChange={handleChangeType}
          choices={getTransformOptions().map(({ type: transformType }) => ({
            label: transformType,
            value: transformType,
          }))}
          xs={6}
        />
        {argumentPairs.length ? (
          <Grid item xs={12}>
            <Typography
              variant="h3"
              gutterBottom
              className="esreTransformsForm__header"
            >
              {translate('transformArguments')}
            </Typography>
            {argumentPairs.map(([key, value]) => (
              <TextField
                key={`argument-${key}`}
                label={labelOverrides[key] || key}
                InputLabelProps={{
                  shrink: true,
                }}
                value={value}
                onChange={(event) => handleUpdateArgument(event, key)}
                margin="dense"
              />
            ))}
          </Grid>
        ) : null}
      </Group>
      <RemoveButton
        onClick={() => dispatch({ type: REMOVE_TRANSFORM, transformId: id })}
      />
    </div>
  );
};

Transform.propTypes = {
  id: PropTypes.string,
  type: PropTypes.string,
  args: PropTypes.shape({}),
  dispatch: PropTypes.func,
};

const TransformsForm = ({ transforms = [], dispatch }) => {
  const translate = useTranslation(esreHarvestNLS);

  return (
    <>
      <Typography
        variant="h3"
        gutterBottom
        className="esreTransformsForm__header"
      >
        {translate('transformsHeader')}
      </Typography>
      <form noValidate autoComplete="off">
        {
          // eslint-disable-next-line no-unused-vars
          transforms.map(({ id, type, priority: _, args }) => (
            <Transform
              id={id}
              type={type}
              args={args}
              dispatch={dispatch}
              key={id}
            />
          ))
        }
      </form>
      <AddFieldButton
        label={translate('addTransformLabel')}
        title={translate('addTransformTitle')}
        onClick={() => dispatch({ type: ADD_TRANSFORM })}
      />
    </>
  );
};

TransformsForm.propTypes = {
  transforms: PropTypes.arrayOf(PropTypes.shape({})),
  dispatch: PropTypes.func,
};

export default TransformsForm;
