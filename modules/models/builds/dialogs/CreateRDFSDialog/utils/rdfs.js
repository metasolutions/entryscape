import { Context } from '@entryscape/entrystore-js';
import { Graph } from '@entryscape/rdfjson';
import {
  RDF_TYPE_CLASS,
  RDF_TYPE_NAMESPACE,
  RDF_TYPE_PREFIX,
  RDF_TYPE_PROPERTY,
} from 'models/utils/ns';

const ES_PROPERTY_DEFAULT = 'http://entryscape.com/terms/default';
const RDFS_PROPERTY_DEFINED_BY = 'rdfs:isDefinedBy';

/**
 *
 * @param {Context} context
 * @param {Graph} resourceGraph
 * @param {string} rdfType
 * @param {string} ontologyURI
 * @returns {Promise<string|undefined>}
 */
export const copyMetadataToResource = (
  context,
  resourceGraph,
  rdfType,
  ontologyURI
) => {
  const entrystore = context.getEntryStore();

  return entrystore
    .newSolrQuery()
    .context(context)
    .rdfType(rdfType)
    .forEach((classEntry) => {
      resourceGraph.addAll(classEntry.getMetadata());
      resourceGraph.add(
        classEntry.getResourceURI(),
        RDFS_PROPERTY_DEFINED_BY,
        ontologyURI
      );
    });
};

/**
 *
 * @param {Context} context
 * @returns {Promise<string|undefined>}
 */
const findURIFromNamespace = async (context) => {
  const entrystore = context.getEntryStore();
  const prefixes = await entrystore
    .newSolrQuery()
    .context(context)
    .rdfType(RDF_TYPE_PREFIX)
    .literalProperty(ES_PROPERTY_DEFAULT, 'true')
    .limit(1)
    .getEntries();
  if (prefixes.length > 0) {
    return prefixes[0].getMetadata().findFirstValue(null, RDF_TYPE_NAMESPACE);
  }
};

/**
 *
 * @param {Context} context
 * @returns {Promise<string|undefined>}
 */
const findURIFromClassOrProperty = async (context) => {
  const entrystore = context.getEntryStore();
  const classOrProperties = await entrystore
    .newSolrQuery()
    .context(context)
    .rdfType([RDF_TYPE_CLASS, RDF_TYPE_PROPERTY])
    .limit(1)
    .getEntries();
  if (classOrProperties.length > 0) {
    const resourceURI = classOrProperties[0].getResourceURI();
    const index =
      resourceURI.lastIndexOf('/') > resourceURI.lastIndexOf('#')
        ? resourceURI.lastIndexOf('/')
        : resourceURI.lastIndexOf('#');
    return resourceURI.substr(0, index + 1);
  }
};

/**
 *
 * @param {Context} context
 * @returns {Promise<string>}
 */
export const findOntologyURI = async (context) => {
  const uriFromNamespace = await findURIFromNamespace(context);
  if (uriFromNamespace) return uriFromNamespace;

  const uriFromClassOrProperty = await findURIFromClassOrProperty(context);
  if (uriFromClassOrProperty) return uriFromClassOrProperty;

  return `${context.getEntryStore().getBaseUrl()}${context.getId()}`;
};
