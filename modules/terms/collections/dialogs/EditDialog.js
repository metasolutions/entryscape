import ListEditEntryDialog from 'commons/components/EntryListView/dialogs/ListEditEntryDialog';
import { LEVEL_RECOMMENDED } from 'commons/components/rdforms/LevelSelector';

const EditDialog = (actionsProps) => {
  return (
    <ListEditEntryDialog
      {...actionsProps}
      formTemplateId="skosmos:conceptScheme"
      templateLevel={LEVEL_RECOMMENDED}
    />
  );
};

export default EditDialog;
