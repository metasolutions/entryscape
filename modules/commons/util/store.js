import { entrystore as es, entrystoreUtil } from 'commons/store';
import config from 'config';
import { EntityType } from 'entitytype-lookup';
import { template } from 'commons/util/template';
import {
  Entry,
  EntryInfo,
  PrototypeEntry,
  Context,
  terms,
} from '@entryscape/entrystore-js';
import { Graph } from '@entryscape/rdfjson';
import { utils } from '@entryscape/rdforms';
import configUtil from './config';

const ENTITYTYPE_CONTEXT_SCOPE = 'entitytypeContextScope';
const ENTITTYPE_REPOSITORY_SCOPE = 'entitytypeRepositoryScope';
const CONTEXT_SCOPE = 'contextScope';
const REPOSITORY_SCOPE = 'repositoryScope';

/**
 * Because Contexts are created opportunistically we need to load the entry
 * to make sure we can access the context entry info, e.g for the alias in this case
 *
 * @param {Context} context
 * @returns {Promise<string>}
 */
const getContextName = async (context) => {
  const contextEntry = await context.getEntry();
  return contextEntry.getEntryInfo().getName();
};

/**
 *
 * @param entryId
 * @param contextId
 * @param uriPattern
 * @param entryName
 * @param contextName
 * @param uuid
 * @param resourceURI
 * @returns {string|*}
 */
const constructURIFromPattern = (
  uriPattern,
  { entryId, contextId, entryName, contextName, uuid, resourceURI } = {}
) => {
  const uri = template(uriPattern, {
    contextId: encodeURIComponent(contextId),
    entryId: encodeURIComponent(entryId),
    contextName: encodeURIComponent(contextName || ''),
    entryName: encodeURIComponent(entryName || ''),
    uuid,
    resourceURI: encodeURIComponent(resourceURI || ''),
  });
  return (!contextName || !contextId) && uri.endsWith('//')
    ? uri.substring(0, uri.length - 1)
    : uri;
};

/**
 * For each variable in uriTemplate identify it's path position.
 * E.g
 *  uriTemplate => example.com/x/${y}/${z}
 *  returns => { y: 1, z: 2 }
 *
 * @param uriTemplate
 * @returns {Map<string, number>}
 */
const getVariableNamesAndPositionFromTemplate = (uriTemplate) => {
  const varCharStart = '$%7B'; // ${
  const varCharEnd = '%7D'; // }

  const segments = new URL(uriTemplate).pathname.split('/').splice(1);

  const variables = new Map();
  segments.forEach((segment, idx) => {
    if (segment.startsWith(varCharStart)) {
      const varName = segment.replace(varCharStart, '').replace(varCharEnd, '');
      variables.set(varName, idx);
    }
  });

  return variables;
};

/**
 * Checks whether the static parts of the provided URI and pattern match
 *
 * @param {string} uri
 * @param {string} uriPattern
 * @returns {boolean}
 */
export const checkUriAndPatternMatch = (uri, uriPattern) => {
  if (!uriPattern) return false;
  const uriPatternAsURL = new URL(uriPattern);
  const uriAsURL = new URL(uri);
  return uriPatternAsURL.origin === uriAsURL.origin;
};

/**
 * Matches a uri against a uriPattern and returns the value of variableName or all variable values.
 *
 * @param {string} uri
 * @param {string} uriPattern
 * @param {string} variableName
 * @returns {string|Map<string, number>}
 * @throws
 */
const getValueFromURIByPattern = (uri, uriPattern, variableName = '') => {
  // get all variable names as they are occurring in the URI template
  const variableNamesMap = getVariableNamesAndPositionFromTemplate(uriPattern);

  const uriAsURL = new URL(uri);
  if (!checkUriAndPatternMatch(uri, uriPattern)) {
    console.error(`Could not match uri ${uri} with uri pattern ${uriPattern}`);
    return null;
  }

  // get all 'segments' in the pathname
  const segments = uriAsURL.pathname.split('/').splice(1);

  const variableValuesMap = new Map();

  variableNamesMap.forEach((idx, name) => {
    variableValuesMap.set(name, segments[idx]);
  });

  return variableName ? variableValuesMap.get(variableName) : variableValuesMap;
};

/**
 *
 * @param {Context} context
 * @param {string} rdfType
 * @param {object} entryParams
 * @param {string} entryParams.entryName
 * @param {string} entryParams.contextName
 * @param {string} entryParams.entryId
 * @param {string} entryParams.contextId
 * @returns {PrototypeEntry}
 */
const createEntry = (
  context,
  rdfType,
  { entryId, contextId, entryName, contextName } = {}
) => {
  let resourceURI;
  if (rdfType) {
    const entryURITemplate = configUtil.getEntryURITemplate(rdfType);
    if (entryURITemplate) {
      resourceURI = template(entryURITemplate, {
        entryId: entryId || '_newId',
        contextId: contextId || context.getId(),
        entryName,
        contextName,
        uuid: utils.generateUUID(),
      });
    }
  }

  return resourceURI ? context.newLink(resourceURI) : context.newNamedEntry();
};

/**
 *
 * @param {Context} context
 * @param {EntityType} entityType
 * @param {object} entryParams
 * @param {string} entryParams.entryId
 * @param {string} entryParams.contextId
 * @param {string} entryParams.entryName
 * @param {string} entryParams.contextName
 * @param {string} entryParams.uuid
 * @param {string} uriSpace
 * @returns {PrototypeEntry}
 */
const createEntryFromEntityType = (
  context,
  entityType,
  { entryId, contextId, entryName, contextName, uuid },
  uriSpace
) => {
  let resourceURI;

  if (entityType.uriPattern) {
    resourceURI = template(entityType.uriPattern, {
      entryId: entryId || '_newId',
      contextId: contextId || context.getId(),
      entryName,
      contextName,
      uuid, // @todo needed?
    });
  }

  if (uriSpace) {
    resourceURI = template(`${uriSpace}/\${entryName}`, { entryName });
  }

  return resourceURI ? context.newLink(resourceURI) : context.newNamedEntry();
};

/**
 *
 * @param {Context} context
 * @returns {Promise<Entry>}
 */
const createRowstorePipeline = (context) => {
  const pipelinePrototypeEntry = context.newPipeline('rowstorePipeline');
  const pipelineResource = pipelinePrototypeEntry.getResource();
  pipelineResource.addTransform(pipelineResource.transformTypes.ROWSTORE, {});
  return pipelinePrototypeEntry.commit();
};

/**
 *
 * @param {Entry} entry
 * @returns {Object.<{metadata: Graph, info: EntryInfo, ruri: string, context: Context}>}
 */
const spreadEntry = (entry) => ({
  allMetadata: entry.getAllMetadata(),
  metadata: entry.getMetadata(),
  info: entry.getEntryInfo(),
  ruri: entry.getResourceURI(),
  context: entry.getContext(),
});

/**
 * Check that the alias is not already in use.
 * Only a response status code 404 fulfills the above criteria.
 *
 * @param {string} alias
 * @returns {Promise<boolean>}
 */
const isNameFreeToUse = async (alias) => {
  const repository = config.get('entrystore.repository');
  const checkIfAliasIsFreeURL = `${repository}${
    repository.endsWith('/') ? '' : '/'
  }${alias}`;
  const res = await fetch(checkIfAliasIsFreeURL, { method: 'HEAD' });

  return res.status === 404;
};

/**
 * Check if URI in the entire repository
 *
 * @param uri
 * @returns {Promise<boolean>}
 */
const isResourceUniqueInRepository = async (uri) => {
  const entries = await entrystoreUtil
    .getEntryListByResourceURI(uri)
    .getAllEntries();

  return !entries.length;
};
/**
 *
 * @param uri
 * @param context
 * @returns {Promise<boolean>}
 */
const isResourceURIUniqueInContext = async (uri, context = null) => {
  let isUnique = true;
  const entries = await entrystoreUtil
    .getEntryListByResourceURI(uri)
    .getAllEntries();

  if (entries.length > 0) {
    // entry with given resource uri exists already
    isUnique = context
      ? !entries.some((entry) => entry.getContext() === context)
      : false;
  }

  return isUnique;
};

/**
 * Check if URI is unique within entity types and context
 *
 * @param uri
 * @param context
 * @param rdfTypes
 * @returns {Promise<boolean>}
 */
const isUniqueWithinEntityTypes = async (uri, context = null, rdfTypes) => {
  const searchList = es
    .newSolrQuery()
    .rdfType(rdfTypes)
    .context(context)
    .list();
  const entries = await searchList.getEntries();
  return !entries?.some((entry) => entry.getResourceURI() === uri);
};

/**
 * Check if URI is unique based on URI config scope
 *
 * @param URIUniquenessscope
 * @param uri
 * @param context
 * @param rdfTypes
 * @returns {Promise<boolean>}
 */
const checkURIUniquenessScope = async (
  URIUniquenessscope,
  uri,
  context = null,
  rdfType = null
) => {
  switch (URIUniquenessscope) {
    case ENTITYTYPE_CONTEXT_SCOPE:
      return isUniqueWithinEntityTypes(uri, context, rdfType);
    case ENTITTYPE_REPOSITORY_SCOPE:
      return isUniqueWithinEntityTypes(uri, null, rdfType);
    case CONTEXT_SCOPE:
      return isResourceURIUniqueInContext(uri, context);
    case REPOSITORY_SCOPE:
      return isResourceUniqueInRepository(uri);
    default:
      return true;
  }
};

/**
 * Check if the configuration and the main entry in context (e.g ConceptScheme for terms)
 * indicate if a uri pattern should be used.
 *
 * @param {EntityType} entityType
 * @param {Entry} mainEntryInContext
 * @returns {boolean}
 */
const shouldUseUriPattern = (entityType = {}, mainEntryInContext = null) => {
  if (entityType?.uriPattern) {
    if (mainEntryInContext) {
      const { metadata, ruri } = spreadEntry(mainEntryInContext);

      /** @type boolean */
      return !!metadata.findFirstValue(ruri, 'void:uriSpace');
    }
    return true; // depend only on the entityType configuration, i.e creation step
  }

  return false;
};

/* eslint-disable */

/**
 *
 * @param context
 * @returns {Promise<object>}
 */
const getGroupWithHomeContext = async (context) => {
  try {
    const contextEntry = await context.getEntry();
    const stmts = contextEntry
      .getReferrersGraph()
      .find(null, terms.homeContext);
    if (!stmts || stmts.length === 0) return null;
    const groupResourceURI = stmts[0].getSubject();
    const groupEntry = await es.getEntry(
      es.getEntryURIFromURI(groupResourceURI)
    );
    return groupEntry;
  } catch (error) {
    console.error(error);
    return null;
  }
};

const createEntityTypeEntryFromLink = ({
  context,
  prototypeEntry,
  graph,
  uriPattern,
  link,
}) => {
  // check if uri value is valid
  if (!link) {
    return uriPattern
      ? Promise.reject(-1) // throwing an error since the uri name need to be set by user
      : prototypeEntry.commit(); // creating a named resource
  }
  if (uriPattern) {
    // check if uri exists but has no localname
    const localName = getValueFromURIByPattern(link, uriPattern, 'entryName');
    if (!localName) {
      return Promise.reject(-1);
    }
  }
  // replace graph ruri with the given uri
  graph.replaceURI(prototypeEntry.getResourceURI(), link);
  return context.newLink(link).setMetadata(graph).commit();
};

const createEntityTypeEntryFromFile = async ({ context, graph, file }) => {
  const newEntry = context.newEntry();
  const entry = await newEntry.setMetadata(graph).commit();
  if (file) {
    // check if file was given
    await entry.getResource(true).putFile(file);
  }
  return entry; // Just make sure we return the entry.
};

// if entry is a local entry then can replace file
const canReplaceEntityFile = (entry) =>
  entry.isLocal() && entry.isInformationResource();

// if entry uses a uri pattern or is a link then can replace link
const canReplaceEntityLink = (entry, entityTypeDef) =>
  entry.isLink() && !shouldUseUriPattern(entityTypeDef);

const canDownloadEntity = (entry) =>
  !(entry.isLink() || (entry.isLocal() && !entry.isInformationResource()));

/* eslint-enable */

export {
  createEntry,
  createEntryFromEntityType,
  createRowstorePipeline,
  spreadEntry,
  isNameFreeToUse,
  isResourceUniqueInRepository,
  isResourceURIUniqueInContext,
  isUniqueWithinEntityTypes,
  checkURIUniquenessScope,
  getContextName,
  getValueFromURIByPattern,
  constructURIFromPattern,
  shouldUseUriPattern,
  getGroupWithHomeContext,
  createEntityTypeEntryFromLink,
  createEntityTypeEntryFromFile,
  canReplaceEntityFile,
  canReplaceEntityLink,
  canDownloadEntity,
};
