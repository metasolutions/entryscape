import { useEffect, useRef, useState } from 'react';
import PropTypes from 'prop-types';
import { Entry } from '@entryscape/entrystore-js';
import { CircularProgress, Typography } from '@mui/material';
import ListActionDialog from 'commons/components/common/dialogs/ListActionDialog';
import ContentWrapper from 'commons/components/common/ContentWrapper';
import {
  getDistributionFileEntries,
  isAPIDistribution,
} from 'catalog/datasets/DatasetOverview/utils/distributions';
import { useTranslation } from 'commons/hooks/useTranslation';
import escaStatisticsNLS from 'catalog/nls/escaStatistics.nls';
import useAsync from 'commons/hooks/useAsync';
import TimeChart from 'commons/components/chart/Time';
import {
  getMultiDatasetChartData,
  hasChartData,
} from 'catalog/statistics/utils/data';
import {
  TIME_RANGE_THIS_MONTH,
  TIME_RANGE_THIS_YEAR,
} from 'catalog/statistics/utils/timeRange';
import { TimeRangeControl } from 'catalog/statistics/StatisticsChart';
import { useESContext } from 'commons/hooks/useESContext';
import './StatisticsDialog.scss';

const useEntriesForStatistics = (distributionEntry) => {
  const apiDistributionEntry = isAPIDistribution(distributionEntry)
    ? [distributionEntry]
    : null;
  const { data: entries, error, runAsync } = useAsync(apiDistributionEntry);

  useEffect(() => {
    if (!entries) {
      runAsync(getDistributionFileEntries(distributionEntry));
    }
  }, [entries, distributionEntry, runAsync]);

  return { entries, error };
};

const StatisticsChart = ({ chartData, status }) => {
  const translate = useTranslation(escaStatisticsNLS);
  const hasNoChartData = !hasChartData(chartData);

  return (
    <div className="escaStatisticsChart">
      {status === 'idle' || status === 'pending' ? (
        <div className="escaStatisticsChart__loading">
          <CircularProgress />
        </div>
      ) : null}
      {hasNoChartData ? (
        <Typography className="escaStatisticsChart__label" variant="h2">
          {translate('statsChartPlaceholder')}
        </Typography>
      ) : null}
      <div
        className={
          hasNoChartData
            ? 'escaStatisticsChart__chart escaStatisticsChart__chart--empty'
            : 'escaStatisticsChart__chart'
        }
      >
        <TimeChart data={chartData} dimensions={{ height: '300px' }} />
      </div>
    </div>
  );
};

StatisticsChart.propTypes = {
  chartData: PropTypes.shape({}),
  status: PropTypes.string.isRequired,
};

const StatisticsDialog = ({ entry: distributionEntry, closeDialog }) => {
  const { context } = useESContext();
  const [timeRange, setTimeRange] = useState(TIME_RANGE_THIS_MONTH);
  const isInitialRequest = useRef(true);
  const translate = useTranslation(escaStatisticsNLS);
  const { entries } = useEntriesForStatistics(distributionEntry);
  const { data: chartData, status, runAsync } = useAsync();

  useEffect(() => {
    if (!entries) return;
    runAsync(getMultiDatasetChartData(entries, context, timeRange));
  }, [entries, context, timeRange, runAsync]);

  // fetch secondary time range if no data for primary time range
  useEffect(() => {
    if (!chartData || !isInitialRequest.current) {
      return;
    }
    isInitialRequest.current = false;
    if (!hasChartData(chartData)) {
      setTimeRange(TIME_RANGE_THIS_YEAR);
    }
  }, [chartData]);

  return (
    <ListActionDialog
      id="distribution-statistics"
      closeDialogButtonLabel={translate('statsDialogFooter')}
      maxWidth="md"
      title={translate('statsDialogTitle')}
      closeDialog={closeDialog}
    >
      <ContentWrapper>
        <div className="escaStatisticsDialogChartControls">
          <TimeRangeControl
            timeRangeValue={timeRange}
            updateTimeRange={(timeRangeValue) => setTimeRange(timeRangeValue)}
          />
        </div>
        <StatisticsChart status={status} chartData={chartData} />
      </ContentWrapper>
    </ListActionDialog>
  );
};

StatisticsDialog.propTypes = {
  entry: PropTypes.instanceOf(Entry),
  closeDialog: PropTypes.func.isRequired,
};

export default StatisticsDialog;
