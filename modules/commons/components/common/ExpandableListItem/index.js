import { useState } from 'react';
import PropTypes from 'prop-types';
import {
  KeyboardArrowDown as ArrowDownIcon,
  KeyboardArrowRight as ArrowRightIcon,
} from '@mui/icons-material';
import { Accordion, AccordionSummary, AccordionDetails } from '@mui/material';
import './index.scss';

const ExpandableListItem = ({
  children,
  primary,
  secondary,
  accordionProps = {},
}) => {
  const [expanded, setExpanded] = useState(false);

  return (
    <Accordion
      className="escoExpandableListItem"
      elevation={0}
      square
      expanded={expanded}
      onChange={() => setExpanded(!expanded)}
      {...accordionProps}
    >
      <AccordionSummary
        classes={{
          root: 'escoExpandableListItemHeader',
          expandIconWrapper: 'escoExpandableListeItemHeader__toggle',
        }}
        expandIcon={expanded ? <ArrowDownIcon /> : <ArrowRightIcon />}
        aria-controls="expandable-list-item-content"
        id="expandable-list-item-header"
      >
        <div className="escoExpandableListeItemHeader__primary">{primary}</div>
        <div className="escoExpandableListeItemHeader__secondary">
          {secondary}
        </div>
      </AccordionSummary>
      <AccordionDetails
        classes={{
          root: 'escoExpandableListItem__details',
        }}
      >
        {children}
      </AccordionDetails>
    </Accordion>
  );
};

ExpandableListItem.propTypes = {
  children: PropTypes.node,
  primary: PropTypes.node,
  secondary: PropTypes.node,
  accordionProps: PropTypes.shape({}),
};

export default ExpandableListItem;
