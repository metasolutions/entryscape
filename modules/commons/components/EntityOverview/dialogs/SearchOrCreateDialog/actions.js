import escoDialogsNLS from 'commons/nls/escoDialogs.nls';
import escoPlaceholderNLS from 'commons/nls/escoPlaceholder.nls';
import CreateEntityDialog from '../CreateEntityDialog';

export const listAction = {
  id: 'create',
  Dialog: CreateEntityDialog,
  labelNlsKey: 'createButtonLabel',
  tooltipNlsKey: 'createButtonTooltip',
};

export const nlsBundles = [escoDialogsNLS, escoPlaceholderNLS];
