import { IconButton } from '@mui/material';
import Tooltip from 'commons/components/common/Tooltip';
import { NavigateBefore } from '@mui/icons-material';
import useNavigate from 'commons/components/router/useNavigate';
import PropTypes from 'prop-types';
import registry from 'commons/registry';
import './OverviewBackButton.scss';

const OverviewBackButton = ({ label, to, entry }) => {
  const { navigate, goBackToListView } = useNavigate();
  const handleNavigation = () => {
    if (to) {
      navigate(to);
    } else {
      goBackToListView(registry.getCurrentView(), {
        contextId: entry.getContext().getId(),
      });
    }
  };

  return (
    <Tooltip title={label}>
      <IconButton
        aria-label={label}
        onClick={handleNavigation}
        className="escoOverviewBackButton"
      >
        <NavigateBefore />
      </IconButton>
    </Tooltip>
  );
};

OverviewBackButton.propTypes = {
  label: PropTypes.string.isRequired,
  to: PropTypes.string,
  entry: PropTypes.shape({
    getContext: PropTypes.func,
  }),
};

export default OverviewBackButton;
