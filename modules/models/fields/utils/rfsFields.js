import * as ns from 'models/utils/ns';
import RfsTextField from './RfsTextField';
import RfsDatatypeField from './RfsDatatypeField';
import RfsInlineField from './RfsInlineField';
import RfsLookupField from './RfsLookupField';
import RfsSelectField from './RfsSelectField';

export const RFS_FIELDS_COMMON_PROPERTIES = [
  'dcterms:title',
  ns.RDF_PROPERTY_PROPERTY,
  ns.RDF_PROPERTY_STATUS,
  ns.RDF_PROPERTY_PREF,
  ns.RDF_PROPERTY_MIN,
  ns.RDF_PROPERTY_MAX,
  ns.RDF_PROPERTY_LABEL,
  ns.RDF_PROPERTY_DESCRIPTION,
  ns.RDF_PROPERTY_PURPOSE,
  ns.RDF_PROPERTY_PLACEHOLDER,
  ns.RDF_PROPERTY_EDIT_LABEL,
  ns.RDF_PROPERTY_EDIT_DESCRIPTION,
  ns.RDF_PROPERTY_CSS,
  ns.RDF_PROPERTY_ATTRIBUTE,
];

export const RDF_TYPE_TO_RFS_FIELD = {
  [ns.RDF_TYPE_TEXT_FIELD]: RfsTextField,
  [ns.RDF_TYPE_DATATYPE_FIELD]: RfsDatatypeField,
  [ns.RDF_TYPE_INLINE_FIELD]: RfsInlineField,
  [ns.RDF_TYPE_LOOKUP_FIELD]: RfsLookupField,
  [ns.RDF_TYPE_SELECT_FIELD]: RfsSelectField,
};

export const RFS_FIELDS = [
  RfsTextField,
  RfsDatatypeField,
  RfsInlineField,
  RfsLookupField,
  RfsSelectField,
];
