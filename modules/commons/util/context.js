import config from 'config';
import { entrystore as es, entrystoreUtil as esu } from 'commons/store';
import {
  CONTEXT_CONTEXTS,
  CONTEXT_PRINCIPALS,
  USERS_ENTRY_ID,
  USER_ENTRY_ID_GUEST,
} from 'commons/util/userIds';
import { i18n } from 'esi18n';
import { getLabel, getAllMetadata } from 'commons/util/rdfUtils';
import { RDF_PROPERTY_PROJECTTYPE } from 'entitytype-lookup';
import {
  CATALOG_MODULE,
  MODELS_MODULE,
  TERMS_MODULE,
  WORKBENCH_MODULE,
} from 'commons/util/moduleDefinitions';
import { hasAdminRights as isAdmin } from './user';
import { RDF_TYPE_CATALOG, RDF_TYPE_TERMINOLOGY, getEntryLabel } from './entry';
import {
  getGroupWithHomeContext,
  spreadEntry,
  constructURIFromPattern,
} from './store';

const NAMESPACE_ESTERMS = 'http://entryscape.com/terms/'; // TODO one place for namespaces
const CONTEXT_TYPE_CATALOG = `${NAMESPACE_ESTERMS}CatalogContext`;
const CONTEXT_TYPE_TERMINOLOGY = `${NAMESPACE_ESTERMS}TerminologyContext`;
const CONTEXT_TYPE_WORKBENCH = `${NAMESPACE_ESTERMS}WorkbenchContext`;
const CONTEXT_TYPE_MODELS = `${NAMESPACE_ESTERMS}FormsContext`;
const CONTEXT_TYPE_CONTEXT_GENERIC = `${NAMESPACE_ESTERMS}Context`;
const CONTEXT_TYPE_CONFIG = `${NAMESPACE_ESTERMS}config`;

/**
 * @type {Map}
 * TODO check if still used
 */
const contextId2groupId = new Map();

/**
 *
 * @param {*} contextId
 * @param {*} groupId
 * TODO check if still used
 */
const setGroupIdForContext = (contextId, groupId) => {
  contextId2groupId.set(contextId, groupId);
};

/**
 *
 * @param {store/Context} context
 * @return {Promise.<store/Entry>} A dcat:Catalog entry
 * @throws
 */
const getDCATCatalogInContext = (context) =>
  es
    .newSolrQuery()
    .context(context)
    .rdfType(RDF_TYPE_CATALOG)
    .getEntries(0)
    .then((entries) => {
      const [entry, ...moreCatalogs] = entries;
      if (!entry) {
        throw Error(
          `Could not find a dcat:Catalog entry in context ${context.getId()}`
        );
      }

      if (moreCatalogs.length > 0) {
        throw Error(
          `Found more than one dcat:Catalog entry in context ${context.getId()}`
        );
      }
      return entry;
    });
/**
 *
 * @param {*} contextId
 * @returns {Promise.<array.<{value: string, label: string}>>}
 */
const getPublishersByContext = (contextId = '') =>
  es
    .newSolrQuery()
    .context(contextId)
    .rdfType(['foaf:Agent', 'foaf:Person', 'foaf:Organization'])
    .list()
    .getAllEntries()
    .then((entries) =>
      entries.map((entry) => ({
        value: entry.getResourceURI(),
        label: getLabel(entry),
      }))
    );
/**
 *
 * @param {*} contextId
 * TODO check if still used
 */
const getGroupIdFromContextId = (contextId) => contextId2groupId.get(contextId);

/**
 *
 * @param {*} contextEntry
 * @param {*} direct
 * TODO check if still used
 */
const canAdministerGroupEntry = (contextEntry, direct = true) => {
  // TODO if (!registry.get('hasAdminRights')) {
  const contextId = contextEntry.getContext().getId();
  const groupURI = es.getEntryURI(
    '_principals',
    getGroupIdFromContextId(contextId)
  );

  // Works with direct = true for Context Entry List since it has already loaded all relevant groups
  const groupEntry = es.getEntry(groupURI, { direct });
  return groupEntry.canAdministerEntry();
  //   }

  //   return true;
};

// #region --- CONTEXT utilities ---

/**
 * @throws
 */
const getUserEntryParentGroupsRURIs = (userEntry) =>
  [
    ...userEntry.getParentGroups(),
    es.getEntryURI(CONTEXT_PRINCIPALS, USERS_ENTRY_ID), // store/_principals/entry/_users
  ].map((groupURI) =>
    es.getResourceURI(es.getContextId(groupURI), es.getEntryId(groupURI))
  );

/**
 * For each contextId get a specific entry type in each respective context.
 * E.g for a context of type esterms:Catalog get a single (first solr matched)
 * entry of type dcat:Catalog
 *
 * Returns a flat array of all entries retrieved per context
 * @param {*} contextIds
 * @param {string} typeValue
 * @param {(entry|graph|context)} typeName
 * @returns {Entry[]}
 */
const getEntriesByTypeForContextIds = (contextIds, entryType) => {
  /**
   * Convenience function to fetch an entry in a context or a context entry
   *
   * @param {*} contextId
   * @return {Promise.<Entry>}
   */
  const getEntryByTypeInContext = (contextId) =>
    entryType
      ? esu.getEntryByType(entryType, es.getContextById(contextId))
      : es.getEntry(es.getEntryURI(CONTEXT_CONTEXTS, contextId));

  return (
    /**
     * * Promise.allSettled is used here as opposed to Promise.all
     * given that getEntryByType (@see get) throws and we don't want to reject
     * the whole promise but rather ignore the failed ones.
     */
    Promise.allSettled(contextIds.map(getEntryByTypeInContext))

      // Filter out rejected promises  get only the promises' resolved values
      .then((promises) => promises.filter((promise) => 'value' in promise))
      .then((promises) => promises.map((promise) => promise.value))

      // [[1, 2], [3], [4]] => [1, 2, 3, 4]
      .then((entriesArr) => entriesArr.flat())
      .catch((err) => {
        console.error(err);
        console.log('busted!');
      })
  );
};

const contextTypes = [
  {
    name: 'catalogContext',
    rdfType: CONTEXT_TYPE_CATALOG,
    entryType: RDF_TYPE_CATALOG,
    module: CATALOG_MODULE,
  },
  {
    name: 'terminologyContext',
    rdfType: CONTEXT_TYPE_TERMINOLOGY,
    entryType: RDF_TYPE_TERMINOLOGY,
    module: TERMS_MODULE,
  },
  {
    name: 'workbenchContext',
    // note there's no entryType here because
    // we don't know the entry types!! dynamic entry types
    rdfType: CONTEXT_TYPE_WORKBENCH,
    module: WORKBENCH_MODULE,
  },
  {
    name: 'modelContext',
    // note there's no entryType here because
    // we don't know the entry types!! dynamic entry types
    rdfType: CONTEXT_TYPE_MODELS,
    module: MODELS_MODULE,
  },
  {
    name: 'context',
    rdfType: CONTEXT_TYPE_CONTEXT_GENERIC,
  },
];
// #region --- CONTEXT utilities ---

/**
 * Most of the times suffices to do entry.getContext() to get the context of an entry.
 * However in very few cases the entry itself is a context and so getting the context of the context
 * would yield the _contexts context which is probably not what we'd want anywhere in the app.
 * This would happen for workbench (project) contexts.
 *
 * @param {Entry} entry
 * @returns {Context|Resource}
 */
const getContextFromEntry = (entry) =>
  entry.isContext() ? entry.getResource(true) : entry.getContext();

/**
 *
 * @param {*} entry
 * @param {*} forceLoad
 */
const loadContextEntry = (entry, direct = false) =>
  getContextFromEntry(entry).getEntry(direct); // TODO check if forceLoad vs direct stills holds

// TODO there's an extra check for catalog, has apis...
const setContextPublic = async (contextEntry, isPublic) => {
  if (!contextEntry.canAdministerEntry()) {
    // acknowledge(this.nlsSpecificBundle[this.nlsContextSharingNoAccess]);
    return Promise.reject();
  }

  const contextEntryInfo = contextEntry.getEntryInfo();
  const acl = contextEntryInfo.getACL(true);
  acl.rread = acl.rread || [];
  acl.mread = acl.mread || [];

  // adds/remove the _guest user from the resource read (rread) permission,
  // essentially sets the entry to public/not public
  if (isPublic) {
    acl.rread.push(USER_ENTRY_ID_GUEST);
    acl.mread.push(USER_ENTRY_ID_GUEST);
  } else {
    acl.rread.splice(acl.rread.indexOf(USER_ENTRY_ID_GUEST), 1);
    acl.mread.splice(acl.mread.indexOf(USER_ENTRY_ID_GUEST), 1);
  }

  contextEntryInfo.setACL(acl);
  return contextEntryInfo.commit();
};

/**
 *
 * @param {store/Entry} entry
 * @returns {Promise.<boolean>}
 */
const getIsContextPublic = async (entry, direct = false) => {
  const contextEntry = await loadContextEntry(entry, direct);
  return contextEntry.isPublic();
};

/**
 *
 * @param {store/Entry} entry
 * @param {store/Entry} userEntry
 * @returns {Promise.<void>}
 * @throws
 */
const deleteContext = async (entry, userEntry) => {
  const context = getContextFromEntry(entry);

  try {
    const groupForContext = await getGroupWithHomeContext(context);

    // ? perhaps unecessary and just `entry` would do here
    const contextEntry = await context.getEntry();

    // delete the context entry
    await contextEntry.del();

    // delete the group for this context
    await groupForContext.del();

    /**
     * The user was in the ACL of the context and group just removed
     * Hence, the userEntry's inv. rel. cache would contain information
     * about being in those other ACLs so a user refresh is needed
     * @see commons/util/solr/context:getSearchQuery
     */
    userEntry.setRefreshNeeded();
    await userEntry.refresh();
  } catch (err) {
    console.error(err);
    throw Error(err);
  }
};

/**
 * Update the group name, but only if there is a single group
 * that have the context as homeContext
 * @param {Entry} contextEntry
 */
const updateGroupNameForContext = async (contextEntry) => {
  const homeContexts = contextEntry.getReferrers('store:homeContext');
  if (homeContexts.length === 1) {
    const groupURI = es.getEntryURIFromURI(homeContexts[0]);
    const groupEntry = await es.getEntry(groupURI);
    const { metadata, ruri } = spreadEntry(groupEntry);
    groupEntry.getMetadata();
    metadata.findAndRemove(null, 'foaf:name');
    metadata.addL(ruri, 'foaf:name', getEntryLabel(contextEntry));
    await groupEntry.commitMetadata();
    groupEntry.setRefreshNeeded(true);
  }
};

/**
 *
 * @param {Entry} entry
 */
const updateContextTitle = async (entry) => {
  let contextEntry = entry;

  // @see getContextFromEntry, usually catalog or concepcheme entries
  if (!entry.isContext()) {
    const context = getContextFromEntry(entry);

    // Must force load context entry since it's modification date is
    // updated whenever a containing entry is changed.
    contextEntry = await es.getEntry(context.getEntryURI(), {
      forceLoad: true,
    });

    // Update the context title (from local or cached external metadata)
    const allMetadata = getAllMetadata(entry);
    const { metadata, ruri } = spreadEntry(contextEntry);
    metadata.findAndRemove(null, 'dcterms:title');
    allMetadata
      .find(entry.getResourceURI(), 'dcterms:title')
      .forEach((statement) => {
        metadata.addL(
          ruri,
          'dcterms:title',
          statement.getValue(),
          statement.getLanguage()
        );
      });
    await contextEntry.commitMetadata();
    contextEntry.setRefreshNeeded(true);
  }

  return updateGroupNameForContext(contextEntry);
};

/**
 *
 * Creates the necessary entries for a new context
 *  - STEP 1: create group and context
 *  - STEP 2: add additional metadata on the context entry
 *  - STEP 3: administrative step, set context type and ACL fix
 *  - STEP 4: add project type if provided
 *
 */
const createContext = async ({
  title,
  description,
  alias,
  entityType,
  contextType,
  hasAdminRights,
  projectType,
}) => {
  if (!title) {
    console.error('Title is missing.');
  }

  // STEP 1: create group and context
  let groupEntry;
  let context;
  try {
    groupEntry = await es.createGroupAndContext(alias);
    const resource = await groupEntry.getResource();
    const homeContext = resource.getHomeContext();
    context = es.getContextById(homeContext);
  } catch (err) {
    console.error('Could not create group and context entry');
    throw Error(err);
  }

  // STEP 2:
  const contextEntry = await context.getEntry();
  const { metadata, ruri } = spreadEntry(contextEntry);
  const lang = i18n.getLocale();
  metadata.addL(ruri, 'dcterms:title', title, lang);
  if (description) {
    metadata.addL(ruri, 'dcterms:description', description, lang);
  }
  if (alias && entityType && entityType.uriPattern) {
    const uriSpace = constructURIFromPattern(entityType.uriPattern, {
      contextName: alias,
    });
    metadata.add(ruri, 'void:uriSpace', uriSpace);
  }
  const ctxEntry = await contextEntry.commitMetadata();

  // step 3
  const hcEntryInfo = ctxEntry.getEntryInfo();
  hcEntryInfo
    .getGraph()
    .add(ctxEntry.getResourceURI(), 'rdf:type', contextType); // E.g. 'esterms:WorkbenchContext'
  // TODO remove when entrystore is changed so groups have read access to
  // homecontext metadata by default.
  // Start fix with missing metadata rights on context for group
  const acl = hcEntryInfo.getACL(true);
  acl.mread.push(groupEntry.getId());
  hcEntryInfo.setACL(acl);
  // End fix

  // step 4
  if (projectType) {
    hcEntryInfo
      .getGraph()
      .add(ctxEntry.getResourceURI(), RDF_PROPERTY_PROJECTTYPE, projectType);
  }

  await hcEntryInfo.commit();

  // STEP 4: Update the UI and refresh entry
  if (!hasAdminRights) {
    setGroupIdForContext(context.getId(), groupEntry.getId());
  }

  return ctxEntry;
};

/**
 *  Upgrades a context to premium.
 *
 * @param {Entry} contextEntry
 * @returns {Promise}
 */
const upgradeContextToPremium = async (contextEntry) => {
  contextEntry.setRefreshNeeded();
  await contextEntry.refresh();

  const entryInfo = contextEntry.getEntryInfo();
  const graph = entryInfo.getGraph();
  graph.addL(entryInfo.getResourceURI(), 'store:premium', 'premium');
  return entryInfo.commit();
};

/**
 * Downgrades a context from premium.
 *
 * @param {Entry} contextEntry
 * @returns {Promise}
 */
const downgradeContextFromPremium = async (contextEntry) => {
  contextEntry.setRefreshNeeded();
  await contextEntry.refresh();

  const entryInfo = contextEntry.getEntryInfo();
  const graph = entryInfo.getGraph();
  graph.findAndRemove(contextEntry.getResourceURI(), 'store:premium');
  return entryInfo.commit();
};

const premiumContextsEnabled = () => config.get('entrystore.premiumContexts');

/**
 * Checks whether a context is premium.
 *
 * @param {Entry} contextEntry
 * @returns {boolean}
 */
const isContextPremium = (contextEntry) => {
  if (!premiumContextsEnabled()) return false;

  const premiumValue = contextEntry
    .getEntryInfo()
    .getGraph()
    .findFirstValue(null, 'store:premium');
  return premiumValue === 'premium';
};

/**
 * Checks whether a user can edit the metadata of a context.
 *
 * @param {Entry} entry
 * @param {Entry} userEntry
 * @param {boolean} [direct=false]
 * @returns {boolean}
 */
const canEditContextMetadata = (entry, userEntry, direct = false) => {
  if (isAdmin(userEntry)) return true;
  const context = entry.getContext();
  if (direct) return context.getEntry(true).canWriteMetadata();
  return context
    .getEntry()
    .then((contextEntry) => contextEntry.canWriteMetadata());
};

export {
  getUserEntryParentGroupsRURIs,
  getEntriesByTypeForContextIds,
  canAdministerGroupEntry,
  contextTypes,
  setContextPublic,
  getIsContextPublic,
  setGroupIdForContext,
  getDCATCatalogInContext,
  getPublishersByContext,
  getGroupIdFromContextId,
  loadContextEntry,
  getContextFromEntry,
  createContext,
  deleteContext,
  updateContextTitle,
  upgradeContextToPremium,
  downgradeContextFromPremium,
  premiumContextsEnabled,
  isContextPremium,
  canEditContextMetadata,
  CONTEXT_TYPE_CATALOG,
  CONTEXT_TYPE_TERMINOLOGY,
  CONTEXT_TYPE_WORKBENCH,
  CONTEXT_TYPE_MODELS,
  CONTEXT_TYPE_CONTEXT_GENERIC,
  CONTEXT_TYPE_CONFIG,
};
