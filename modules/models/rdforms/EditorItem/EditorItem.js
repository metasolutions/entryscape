import { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { graphPropType } from 'commons/util/entry';
import { Editor as RDFormsEditor } from '@entryscape/rdforms/renderers/react';
import { getIncludeLevel } from 'models/utils/cardinality';

export const useRdformsEditorItem = (graph, resourceURI, template, level) => {
  const [rdformsItem, setRdformsItem] = useState(null);

  useEffect(() => {
    if (!resourceURI || !graph) {
      return;
    }
    const includeLevel = level || getIncludeLevel(resourceURI, graph);
    const editor = new RDFormsEditor({
      graph,
      template,
      resource: resourceURI,
      compact: false,
      includeLevel,
    });
    setRdformsItem(editor);
  }, [graph, template, resourceURI, level]);

  if (rdformsItem) {
    return rdformsItem.domNode.component;
  }
};

export const RdformsEditorItem = ({ graph, resourceURI, template, level }) => {
  const EditorComponent = useRdformsEditorItem(
    graph,
    resourceURI,
    template,
    level
  );
  if (!EditorComponent) {
    return null;
  }
  return <EditorComponent />;
};

RdformsEditorItem.propTypes = {
  graph: graphPropType,
  resourceURI: PropTypes.string,
  template: PropTypes.shape({}),
  level: PropTypes.string,
};
