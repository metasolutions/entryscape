import { validateFileURI } from './util';

describe('`util` utility functions', () => {
  test('invalid file URIs', () => {
    expect(validateFileURI('invalid')).toBe(false);
    expect(validateFileURI('http://example.com')).toBe(false);
    expect(validateFileURI('invalid://example')).toBe(false);
  });

  test('valid file URI with file extension', () => {
    expect(validateFileURI('file:///path/filename.extension')).toBe(true);
  });

  test('valid file URI with directory', () => {
    expect(validateFileURI('file://path/')).toBe(true);
  });

  test('valid file URI without host', () => {
    expect(validateFileURI('file:///foo.txt')).toBe(true);
  });
});
