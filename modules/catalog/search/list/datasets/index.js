import PropTypes from 'prop-types';
import { useCallback } from 'react';
import { RDF_TYPE_DATASET } from 'commons/util/entry';
import Typography from '@mui/material/Typography';
import { useTranslation } from 'commons/hooks/useTranslation';
import { entrystore } from 'commons/store';
import { useUserState } from 'commons/hooks/useUser';
import { withListModelProvider } from 'commons/components/ListView';
import {
  EntryListView,
  useSolrQuery,
  TITLE_COLUMN,
  MODIFIED_COLUMN,
  INFO_COLUMN,
} from 'commons/components/EntryListView';
import DatasetInfoDialog from 'catalog/search/list/datasets/dialogs/DatasetInfoDialog';
import { LIST_ACTION_INFO } from 'commons/components/EntryListView/actions';
import { nlsBundles } from './actions';
import './index.scss';

const getDatasetsQuery = (context, userInfo) => {
  const query = entrystore.newSolrQuery().rdfType(RDF_TYPE_DATASET);

  if (context) {
    query.context(context);
  }

  if (userInfo.id === '_guest') {
    query.publicRead(true);
  }

  return query;
};

const DatasetsList = ({ context = null, showFilters }) => {
  const { userInfo } = useUserState();
  const t = useTranslation(nlsBundles);

  const createQuery = useCallback(
    () => getDatasetsQuery(context, userInfo),
    [context, userInfo]
  );

  const { entries, ...queryResults } = useSolrQuery({ createQuery });

  return (
    <>
      {context && entries.length ? (
        <Typography variant="h4" classes={{ h4: 'escaDatasetsList__heading' }}>
          {t('datasetsHeading')}
        </Typography>
      ) : null}
      <EntryListView
        // eslint-disable-next-line react/jsx-props-no-spreading
        {...queryResults}
        entries={entries}
        nlsBundles={nlsBundles}
        getListItemProps={({ entry }) => ({
          action: { Dialog: DatasetInfoDialog, entry },
        })}
        includeDefaultListActions={showFilters}
        columns={[
          {
            ...TITLE_COLUMN,
            headerNlsKey: 'headerTitleLabel',
            xs: 8,
          },
          MODIFIED_COLUMN,
          {
            ...INFO_COLUMN,
            xs: 2,
            getProps: ({ entry }) => ({
              action: {
                ...LIST_ACTION_INFO,
                entry,
                nlsBundles,
              },
            }),
          },
        ]}
      />
    </>
  );
};

DatasetsList.propTypes = {
  showFilters: PropTypes.bool,
  context: PropTypes.string,
};

export default withListModelProvider(DatasetsList);
