import {
  ACTION_EDIT,
  ACTION_REVISIONS,
  ACTION_REMOVE,
} from 'commons/components/overview/actions';
import RemoveContactDialog from '../dialogs/RemoveContactDialog';

const sidebarActions = [
  ACTION_EDIT,
  ACTION_REVISIONS,
  { ...ACTION_REMOVE, Dialog: RemoveContactDialog },
];

export { sidebarActions };
