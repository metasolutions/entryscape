import PropTypes from 'prop-types';
import Tooltip from 'commons/components/common/Tooltip';
import { Button } from '@mui/material';
import { ListItemAction } from '../ListItemAction';

export const ListItemButton = ({
  xs,
  justifyContent,
  variant = 'text',
  label,
  tooltip,
  ...buttonProps
}) => {
  return (
    <ListItemAction justifyContent={justifyContent} xs={xs}>
      <Tooltip title={tooltip}>
        <Button variant={variant} {...buttonProps}>
          {label}
        </Button>
      </Tooltip>
    </ListItemAction>
  );
};

ListItemButton.propTypes = {
  xs: PropTypes.number,
  justifyContent: PropTypes.string,
  variant: PropTypes.string,
  label: PropTypes.string,
  tooltip: PropTypes.string,
};
