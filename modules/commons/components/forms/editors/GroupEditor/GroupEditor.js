import PropTypes from 'prop-types';
import { Fields } from '../Fields';
import { FieldEditor } from '../FieldEditor';
import { Group } from '../Group';

export const GroupEditor = ({
  direction,
  editors,
  children,
  translate,
  ...fieldsProps
}) => {
  const labelId = `fields-label-${fieldsProps.name}`;

  if (!editors) {
    throw new Error('A group editor must have editors');
  }

  return (
    <Fields labelId={labelId} {...fieldsProps}>
      {children}
      <Group direction={direction}>
        {editors.map((editorConfig) => {
          return (
            <FieldEditor
              key={editorConfig.property}
              translate={translate}
              {...editorConfig}
            />
          );
        })}
      </Group>
    </Fields>
  );
};

GroupEditor.propTypes = {
  children: PropTypes.node,
  direction: PropTypes.string,
  editors: PropTypes.arrayOf(PropTypes.shape({})),
  translate: PropTypes.func,
};
