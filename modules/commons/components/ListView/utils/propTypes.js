import PropTypes from 'prop-types';

export const excludeActionsPropType = PropTypes.arrayOf(PropTypes.string);

export const listPropsPropType = PropTypes.shape({
  excludeActions: excludeActionsPropType,
});
