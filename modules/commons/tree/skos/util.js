import { entrystore, entrystoreUtil } from 'commons/store';
import { Entry, EntryStoreUtil } from '@entryscape/entrystore-js';
import {
  RDF_TYPE_CONCEPT,
  RDF_TYPE_CONCEPT_SCHEME,
  refreshEntry,
} from 'commons/util/entry';
import {
  constructURIFromPattern,
  getValueFromURIByPattern,
  spreadEntry,
} from 'commons/util/store';

const hasExplicitInverse = true;
const checkRelatedConcepts = false; // in case we want to check for related terms before deletion

// SKOS classes
// SKOS properties

/**
 * SKOS semantic relations are links between SKOS concepts, where the link is inherent
 * in the meaning of the linked concepts.
 */
const semanticRelations = {
  membershipToRootProperty: 'skos:inScheme',
  inScheme: 'skos:inScheme',
  toParentProperty: 'skos:broader',
  toChildProperty: 'skos:narrower',
  toRelatedProperty: 'skos:related',
  toRootProperty: 'skos:topConceptOf',
  fromRootProperty: 'skos:hasTopConcept',
};

const semanticRelationsProperties = [
  'skos:inScheme',
  'skos:broader',
  'skos:narrower',
  'skos:related',
  'skos:topConceptOf',
  'skos:hasTopConcept',
];

/**
 *  These properties are used to state mapping (alignment) links between SKOS concepts in
 *  different concept schemes, where the links are inherent in the meaning of the linked concepts.
 */
const mappingProperties = [
  'skos:closeMatch',
  'skos:exactMatch',
  'skos:broadMatch',
  'skos:narrowMatch',
  'skos:relatedMatch',
];

const otherProperties = {
  prefLabel: 'skos:prefLabel',
};

/*
 * This returns either a Concept or ConceptScheme entry resource URI
 */
const getParentResourceURI = (e) => {
  const eRURI = e.getResourceURI();
  const md = e.getMetadata();

  return (
    md.findFirstValue(eRURI, semanticRelations.toParentProperty) ||
    md.findFirstValue(eRURI, semanticRelations.toRootProperty)
  );
};

/**
 *
 * @param {object} data
 * @returns {*}
 */
const addNewConceptStmts = (data) => {
  const { metadata, conceptRURI, schemeRURI, label, lang, isRoot } = data;

  metadata.add(conceptRURI, 'rdf:type', RDF_TYPE_CONCEPT);
  metadata.add(conceptRURI, semanticRelations.inScheme, schemeRURI);

  if (label) {
    if (lang) {
      metadata.addL(conceptRURI, otherProperties.prefLabel, label, lang);
    } else {
      metadata.add(conceptRURI, otherProperties.prefLabel, label);
    }
  }

  if (isRoot) {
    metadata.add(conceptRURI, semanticRelations.toRootProperty, schemeRURI);
  }

  return metadata;
};

const getConceptSchemeEntryInContext = (context) =>
  entrystoreUtil.getEntryByType(RDF_TYPE_CONCEPT_SCHEME, context);

/**
 *
 * @param {string|Entry} entry If string then it is the entryURI otherwise the actual entry
 * @param {string} oldResourceURI
 * @param {string} newResourceURI
 * @returns {Promise}
 */
const updateConceptMetadata = (entry, oldResourceURI, newResourceURI) => {
  try {
    const toUpdateEntry =
      typeof entry === 'object'
        ? entry
        : entrystore.getEntry(entry, { direct: true }); // should be already in cache, it was just queried
    const md = toUpdateEntry.getMetadata();

    md.replaceURI(oldResourceURI, newResourceURI);
    toUpdateEntry.setMetadata(md);
    return toUpdateEntry.commitMetadata(); // async, no need to await
  } catch (err) {
    console.error('Failed updating concept metadata');
    // something went wrong. @todo
    throw Error(err);
  }
};

/**
 *
 * @param {Entry} conceptEntry
 * @param {Entry} schemeEntry
 * @returns {Promise}
 */
const addConceptToConceptScheme = async (conceptEntry, schemeEntry) => {
  await refreshEntry(schemeEntry);
  const { metadata, ruri } = spreadEntry(schemeEntry);
  metadata.add(
    ruri,
    semanticRelations.fromRootProperty,
    conceptEntry.getResourceURI()
  );
  return schemeEntry.commitMetadata();
};

/**
 * Pushes an element into an array (which might be newly created). The array is itself an element of
 * the 'map' identified by the 'key'
 *
 * @param {Map} map
 * @param {string} key
 * @param {Entry} newElement
 */
const addElementToMapOfArrays = (map, key, newElement) => {
  const elements = map.get(key) || [];
  elements.push(newElement);
  map.set(key, elements);
};

/**
 * A convenience data transformation function.
 * Map<propertyString, arrayOfEntries> => Map<entryURI, arrayOfProperties>
 *
 * @param {Map} semanticRelationsMap
 * @returns {Map<any, any>}
 */
const transformSemanticRelationsToEntriesMap = (semanticRelationsMap) => {
  const replaceObjectValuesMap = new Map();
  semanticRelationsMap.forEach((entries, semanticProperty) => {
    entries.forEach((entry) => {
      const entryURI = entry.getEntryInfo().getEntryURI();
      addElementToMapOfArrays(
        replaceObjectValuesMap,
        entryURI,
        semanticProperty
      );
    });
  });

  return replaceObjectValuesMap;
};

/**
 * Get a path of entries from a given entry to the concept scheme
 *
 * @param {Entry} entry
 * @param {Entry[]} entriesPath
 * @returns {Promise}
 */
const getConceptPath = (entry, entriesPath = []) => {
  const store = entry.getEntryStore();
  const storeUtil = new EntryStoreUtil(store);
  const broaderParentRURI = getParentResourceURI(entry);

  return broaderParentRURI
    ? storeUtil
        .getEntryByResourceURI(broaderParentRURI, entry.getContext())
        .then((broaderEntry) =>
          getConceptPath(broaderEntry, [...entriesPath, broaderEntry])
        )
    : Promise.resolve(entriesPath);
};

/**
 * Utility functions for working with SKOS
 */
const util = {
  /**
   * Get a Map of all *mapping* relationships incoming to an entry.
   * E.g Map(<skos:closeMatch>, [entry1, entry2, ...])
   *
   * @param {string} entryRURI  Entry's resource URI
   * @returns {Promise} Map key: <property>, value: <Array of entries>
   */
  getMappingRelations(entryRURI) {
    const mappedProperties = new Map();
    const loadRelationsPromise = new Promise((resolve) => {
      const queryPromises = [];

      /**
       * Mapping properties + skos:related. The latter should behave the same as a mapping
       * property on on concept removal
       *
       * @type {Array}
       */
      const mappingRelationProperties = mappingProperties;
      mappingRelationProperties.push(semanticRelations.toRelatedProperty);

      // populate the mappedProperties (Map) with mappings (keys) and entries (values)
      mappingRelationProperties.forEach((mappingProperty) => {
        queryPromises.push(
          entrystore
            .newSolrQuery()
            .uriProperty(mappingProperty, entryRURI)
            .forEach((mappedEntry) => {
              // update map with mapped entries
              if (mappedEntry.canWriteMetadata()) {
                addElementToMapOfArrays(
                  mappedProperties,
                  mappingProperty,
                  mappedEntry
                );
              }
            })
        );
      });

      // when map loaded with all entries per mapping property then resolve
      Promise.all(queryPromises).then(() => {
        resolve(mappedProperties);
      });
    });

    return loadRelationsPromise;
  },
  /**
   *
   * @param {string} resourceURI
   * @param {string[]} filteredRelations
   * @returns {Promise}
   */
  async getSemanticRelations(resourceURI, filteredRelations = []) {
    const semanticMapping = new Map();
    const promises = [];

    /**
     * @param {string} semanticRelationProperty
     * @returns {Array|boolean}
     */
    const shouldPassFilter = (semanticRelationProperty) =>
      Array.isArray(filteredRelations) && // it's array
      (!filteredRelations.length || // either empty or
        (filteredRelations.length > 0 &&
          filteredRelations.includes(semanticRelationProperty))); // not empty but includes property

    semanticRelationsProperties.forEach((semanticRelationProperty) => {
      if (shouldPassFilter(semanticRelationProperty)) {
        promises.push(
          entrystore
            .newSolrQuery()
            .uriProperty(semanticRelationProperty, resourceURI)
            .forEach((mappedEntry) => {
              // update map with mapped entries
              addElementToMapOfArrays(
                semanticMapping,
                semanticRelationProperty,
                mappedEntry
              );
            })
        );
      }
    });

    await Promise.all(promises);

    return semanticMapping;
  },
  deleteConcept(entry) {
    const entryRURI = entry.getResourceURI();
    const parentRURI = getParentResourceURI(entry);

    return entrystoreUtil
      .getEntryByResourceURI(parentRURI)
      .then((parentEntry) => {
        if (parentEntry) {
          if (hasExplicitInverse) {
            parentEntry.getMetadata().findAndRemove(null, null, entryRURI);
            parentEntry.commitMetadata();
          } else {
            parentEntry.setRefreshNeeded();
            parentEntry.refresh();
          }
        } else {
          console.log(
            'Something went wrong. No parent entry found for Concept.'
          );
        }

        // delete the entry and the outgoing mapped relations
        return entry.del();
      });
  },
  /**
   * Update concept scheme resource URI and all (semantic) links referring to the old resource URI
   *
   * @param {Entry} schemeEntry
   * @param {string} newResourceURI
   * @param {Entry[]} conceptEntries
   * @returns {Promise<Array>}
   */
  async updateConceptSchemeRURI(
    schemeEntry,
    newResourceURI,
    conceptEntries = []
  ) {
    // update ruri if needed
    const conceptSchemeEntryInfo = schemeEntry.getEntryInfo();
    const oldResourceURI = schemeEntry.getResourceURI();
    conceptSchemeEntryInfo.setResourceURI(newResourceURI);
    await conceptSchemeEntryInfo.commit();

    const promises = [];
    const properties = ['skos:inScheme', 'skos:topConceptOf'];
    if (conceptEntries.length > 0) {
      try {
        for (const conceptEntry of conceptEntries) {
          const pros = updateConceptMetadata(
            conceptEntry,
            oldResourceURI,
            newResourceURI
          );
          promises.push(pros);
        }
      } catch (err) {
        // something went wrong. @todo
        throw Error(err);
      }
    } else {
      // Map<'skos:inScheme', [Entry1, Entry2, ...]>
      const propertiesToEntriesMap = await util.getSemanticRelations(
        oldResourceURI,
        properties
      );
      // Map<EntryURI, ['skos:inScheme, ...]>
      const replaceObjectValuesEntries = transformSemanticRelationsToEntriesMap(
        propertiesToEntriesMap
      );

      try {
        for (const [entryURI] of Array.from(replaceObjectValuesEntries)) {
          const pros = updateConceptMetadata(
            entryURI,
            oldResourceURI,
            newResourceURI
          );
          promises.push(pros);
        }
      } catch (err) {
        // something went wrong. @todo
        console.error(err);
        return [];
      }
    }

    return promises;
  },
  /**
   *
   * @param {Entry} conceptSchemeEntry
   * @param {string} uriSpace
   * @returns {Promise}
   */
  async updateConceptSchemeUriSpace(conceptSchemeEntry, uriSpace) {
    const { ruri: resourceURI, metadata } = spreadEntry(conceptSchemeEntry);
    metadata.findAndRemove(resourceURI, 'void:uriSpace', null);
    metadata.add(resourceURI, 'void:uriSpace', uriSpace);
    return conceptSchemeEntry.commitMetadata();
  },
  /**
   * Update the concept resource URI and all (semantic) links referring to the old resource URI
   *
   * @param {Entry} entry
   * @param {string} newResourceURI
   * @param {Entry[]} conceptEntries
   * @returns {Promise<Array>}
   */
  async updateConceptResourceURI(entry, newResourceURI, conceptEntries = []) {
    const oldResourceURI = entry.getResourceURI();

    const entryInfo = entry.getEntryInfo();
    entryInfo.setResourceURI(newResourceURI);
    await entryInfo.commit();

    const promises = [];

    if (conceptEntries.length > 0) {
      try {
        for (const relatedEntry of conceptEntries) {
          const pros = updateConceptMetadata(
            relatedEntry,
            oldResourceURI,
            newResourceURI,
            semanticRelationsProperties
          );
          promises.push(pros);
        }
      } catch (err) {
        console.error('Failed to update concept resource uri');
        throw Error(err);

        // something went wrong. @todo
      }
    } else {
      const semanticMapping = await util.getSemanticRelations(oldResourceURI);
      const replaceObjectValuesEntries =
        transformSemanticRelationsToEntriesMap(semanticMapping);
      try {
        for (const [entryURI] of Array.from(replaceObjectValuesEntries)) {
          const pros = updateConceptMetadata(
            entryURI,
            oldResourceURI,
            newResourceURI
          );
          promises.push(pros);
        }
      } catch {
        // something went wrong. @todo
      }
    }
    return Promise.resolve(promises);
  },
  /**
   * @param {Entry} entry
   * @param {string} newResourceURI
   * @returns {Promise}
   */
  setResourceURI(entry, newResourceURI) {
    const entryInfo = entry.getEntryInfo();
    entryInfo.setResourceURI(newResourceURI);
    return entryInfo.commit();
  },
  /**
   *
   * @param {string} resourceURI
   * @returns {Promise}
   */
  async getResourceURIsForSemanticRelations(resourceURI) {
    const semanticRelationsMap = await util.getSemanticRelations(resourceURI);
    const replaceValuesEntriesMap =
      transformSemanticRelationsToEntriesMap(semanticRelationsMap);
    return Array.from(replaceValuesEntriesMap).map(
      ([relatedResourceURI]) => relatedResourceURI
    );
  },
  /**
   *
   * @param {Entry[]} conceptEntries
   * @param {object} secondaryEntityType
   * @param {string} contextName
   * @param {string} newPattern
   * @returns {object[]}
   */
  getURIChangesForConcepts(
    conceptEntries,
    secondaryEntityType,
    contextName,
    newPattern = ''
  ) {
    const uriChanges = conceptEntries.map((conceptEntry) => {
      const oldResourceURI = conceptEntry.getResourceURI();
      const entryName = getValueFromURIByPattern(
        oldResourceURI,
        secondaryEntityType.uriPattern,
        'entryName'
      );
      const newResourceURI = constructURIFromPattern(
        newPattern || secondaryEntityType.uriPattern,
        {
          contextName,
          entryName,
        }
      );
      return {
        conceptEntry,
        newResourceURI,
        oldResourceURI,
      };
    });
    return uriChanges;
  },
  /**
   *
   * @param {object[]} uriChanges
   * @returns {Promise}
   */
  async getURIChangesForSemanticRelations(uriChanges) {
    const promises = uriChanges.map(
      async ({ oldResourceURI, newResourceURI }) => {
        const resourceURIs =
          await util.getResourceURIsForSemanticRelations(oldResourceURI);
        return resourceURIs.map((resourceURI) => ({
          entry: entrystore.getEntry(resourceURI, {
            direct: true,
          }),
          oldResourceURI,
          newResourceURI,
        }));
      }
    );
    const uriSemanticChangesByConcept = await Promise.all(promises);
    return uriSemanticChangesByConcept.flat();
  },
  /**
   * @param {object[]} uriChanges
   * @returns {Promise}
   */
  async replaceURIs(uriChanges) {
    // First update all metadata and then commit, to avoid multiple revisions.
    for (const uriChange of uriChanges) {
      const { entry, oldResourceURI, newResourceURI } = uriChange;
      const metadata = entry.getMetadata();
      metadata.replaceURI(oldResourceURI, newResourceURI);
      entry.setMetadata(metadata);
    }

    const entriesToCommit = uriChanges.reduce((entries, { entry }) => {
      if (!entries.includes(entry)) {
        entries.push(entry);
      }
      return entries;
    }, []);

    // commit changes
    for (const entry of entriesToCommit) {
      try {
        await entry.commitMetadata();
      } catch (error) {
        console.error('Failed to commit uri change for entry');
        throw new Error(error);
      }
    }
  },
  hasChildrenOrRelationsConcepts(entry) {
    const md = entry.getMetadata();
    const entryRURI = entry.getResourceURI();
    const children = md.find(
      entryRURI,
      semanticRelations.toChildProperty,
      null
    );

    if (checkRelatedConcepts) {
      const related = md.find(
        entryRURI,
        semanticRelations.toRelatedProperty,
        null
      );
      return children.length && related.length;
    }

    return children.length;
  },
  // refactor
  async addConceptToScheme(newConcept, context, conceptScheme = null) {
    const conceptSchemeEntry =
      conceptScheme || (await getConceptSchemeEntryInContext(context));
    return addConceptToConceptScheme(newConcept, conceptSchemeEntry).catch(
      () => {
        throw Error('Concept could not be added to concept scheme');
      }
    );
  },
  getSemanticProperties() {
    return semanticRelations;
  },
  getMappingProperties() {
    return mappingProperties;
  },
  addNewConceptStmts,
};

export default util;
export {
  getConceptPath,
  getConceptSchemeEntryInContext,
  semanticRelationsProperties,
};
