import useAsyncCallback from 'commons/hooks/useAsyncCallback';
import { fetchCatalogs } from 'catalog/datasetTemplates/util';

const useCatalogs = (datasetTemplateEntrystore) => {
  const [catalogEntries] = useAsyncCallback(
    fetchCatalogs,
    datasetTemplateEntrystore
  );

  return catalogEntries || [];
};

export default useCatalogs;
