import GlobeIcon from '@mui/icons-material/Public';
import { Button, Grid, TextField } from '@mui/material';
import { Map } from 'commons/components/Map';
import {
  BoxControl,
  DrawControlGroup,
  MarkerControl,
} from 'commons/components/Map/controls';
import {
  MapProvider,
  useMapContext,
} from 'commons/components/Map/hooks/useMapContext';
import { useOpenlayers } from 'commons/components/Map/hooks/useOpenlayers';
import useGetMainDialog from 'commons/hooks/useGetMainDialog';
import { useTranslation } from 'commons/hooks/useTranslation';
import { localize } from 'commons/locale';
import escoRdformsNls from 'commons/nls/escoRdforms.nls';
import registry from 'commons/registry';
import { entrystore } from 'commons/store';
import config from 'config';
import PropTypes from 'prop-types';
import { useCallback, useEffect, useState } from 'react';
import ScaleLineControl from 'commons/components/Map/controls/ScaleLineControl';
import AttributionControl from 'commons/components/Map/controls/AttributionControl';
import ControlGroup from 'commons/components/Map/controls/ControlGroup';
import { getBoundsFromGeo, hasValidBounds } from '../utils/bounds';
import {
  supportsBoundingBoxInBinding,
  getBindingBounds,
  setBindingBounds,
  clearBoundsOutsideOfBinding,
} from '../utils/binding';
import './GeoEditor.scss';

/**
 * Check if value is a number (includes negative values)
 *
 * @param {string} value
 * @returns {boolean}
 */
export const isValidInputValue = (value) => {
  const regex = /^-?\d*(\.?\d*)$/;
  return regex.test(value);
};

const GeoDetect = ({ binding, geoDetect, updateBounds }) => {
  const { getAcknowledgeDialog } = useGetMainDialog();
  const { detectLabel, okLabel } = geoDetect;
  const detectFunction = registry.get('geoDetect');

  if (!detectFunction)
    throw Error(
      // eslint-disable-next-line max-len
      'Geodetect is configured but the detect function is missing. It has to be provided via registry through the extra scripts.'
    );

  const handleClick = () => {
    detectFunction(entrystore, binding).then(
      async (response) => {
        const { geo, message } = response;
        if (typeof geo === 'object' && geo !== null) {
          updateBounds(getBoundsFromGeo(geo));
        }
        if (typeof message === 'object' && message !== null) {
          await getAcknowledgeDialog({
            okLabel: localize(okLabel),
            content: localize(message),
          });
        }
      },
      async (error) => {
        if (typeof error === 'object' && error !== null) {
          await getAcknowledgeDialog({
            okLabel: localize(okLabel),
            content: localize(error),
          });
        }
      }
    );
  };

  return (
    <div>
      <Button variant="text" startIcon={<GlobeIcon />} onClick={handleClick}>
        {localize(detectLabel)}
      </Button>
    </div>
  );
};
GeoDetect.propTypes = {
  binding: PropTypes.shape({}),
  geoDetect: PropTypes.shape({
    detectLabel: PropTypes.shape({}),
    okLabel: PropTypes.shape({}),
  }),
  updateBounds: PropTypes.func,
};

const DirectionField = ({
  value,
  direction,
  placeholder = '',
  onChange,
  disabled,
}) => {
  const [fieldValue, setFieldValue] = useState('');

  /**
   * Update field value if value is changed in other way than editing the text
   * field.
   */
  useEffect(() => {
    setFieldValue((currentFieldValue) => {
      if (parseFloat(currentFieldValue) === value) return currentFieldValue;
      return value === undefined ? '' : `${value}`;
    });
  }, [value]);

  const handleValidateOnBlur = ({ target }) => {
    const { value: inputValue } = target;
    if (!inputValue) return;
    let number;

    if (inputValue.endsWith('-')) {
      number = 0;
      setFieldValue(`${number}`);
    }

    if (inputValue.endsWith('.')) {
      number = parseInt(inputValue, 10);
      setFieldValue(`${number}`);
    }

    if (number !== undefined && number !== value) {
      onChange({ direction, value: number });
    }
  };
  /**
   * Only update value when the parsed value has changed. The value should not
   * be updated, if for example a decimal separarator has been typed without any
   * following numbers.
   *
   * @param root0
   * @param root0.target
   */
  const handleChange = ({ target }) => {
    const { value: inputValue } = target;
    if (!isValidInputValue(inputValue)) return;
    const number =
      inputValue === '' || inputValue === '-'
        ? undefined
        : parseFloat(inputValue);

    if (Number.isNaN(number)) return;
    setFieldValue(inputValue);
    if (number !== value) {
      onChange({ direction, value: number });
    }
  };

  return (
    <TextField
      variant="filled"
      fullWidth
      id={`direction-field-${direction}`}
      label={placeholder}
      value={fieldValue}
      onChange={handleChange}
      disabled={disabled}
      onBlurCapture={handleValidateOnBlur}
    />
  );
};

DirectionField.propTypes = {
  value: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  direction: PropTypes.string,
  placeholder: PropTypes.string,
  onChange: PropTypes.func,
  disabled: PropTypes.bool,
};

const LatlngDirections = ({ directions = {}, updateBounds, editable }) => {
  const translate = useTranslation(escoRdformsNls);

  const handleChange = ({ value, direction }) => {
    updateBounds({
      type: 'point',
      directions: {
        ...directions,
        [direction]: value,
      },
    });
  };

  const { north, west } = directions;

  return (
    <div>
      <DirectionField
        value={north}
        direction="north"
        onChange={handleChange}
        placeholder={translate('latitudeLabelShort')}
        disabled={!editable}
      />
      <DirectionField
        value={west}
        direction="west"
        onChange={handleChange}
        placeholder={translate('longitudeLabelShort')}
        disabled={!editable}
      />
    </div>
  );
};
LatlngDirections.propTypes = {
  directions: PropTypes.shape({
    north: PropTypes.number,
    west: PropTypes.number,
  }),
  updateBounds: PropTypes.func,
  editable: PropTypes.bool,
};

const BoxDirections = ({ directions = {}, updateBounds, editable }) => {
  const translate = useTranslation(escoRdformsNls);

  const handleChange = ({ value, direction }) => {
    updateBounds({
      type: 'polygon',
      directions: {
        ...directions,
        [direction]: value,
      },
    });
  };

  const getDirectionValues = () => {
    return ['north', 'south', 'west', 'east'].map((direction) => ({
      direction,
      value: directions[direction],
    }));
  };

  return (
    <div>
      {getDirectionValues().map(({ direction, value }) => (
        <DirectionField
          key={direction}
          value={value}
          direction={direction}
          onChange={handleChange}
          placeholder={translate(`${direction}LabelShort`)}
          disable={!editable}
        />
      ))}
    </div>
  );
};

BoxDirections.propTypes = {
  directions: PropTypes.shape({}),
  updateBounds: PropTypes.func,
  editable: PropTypes.bool,
};

const GeoEditorMap = ({
  bounds,
  updateBounds,
  editable,
  includeBoxControl,
}) => {
  const [{ map }] = useMapContext();
  const mapUtils = useOpenlayers();
  const [previousBounds, setPreviousBounds] = useState(null);
  const isPoint = bounds?.type === 'point';

  const createPointFeature = ({ north, west }) => {
    const [coordinate] = mapUtils.convertFromLonLat([[west, north]]);
    const geometry = mapUtils.createPoint(coordinate);
    const feature = mapUtils.createFeature({ geometry });
    feature.setStyle(mapUtils.createMarker({ color: mapUtils.BLUE }));
    return feature;
  };

  const createPolygonFeature = ({ west, north, east, south }) => {
    const extent = mapUtils.extentFromLonLat([west, north, east, south]);
    const geometry = mapUtils.createPolygonFromExtent(extent);
    const feature = mapUtils.createFeature({ geometry });
    feature.setStyle(mapUtils.createPolygonStyle());
    return feature;
  };

  // clears current layer and adds a new layer based on bounds
  const refreshMap = () => {
    mapUtils.clearOverlays(map);
    if (!bounds) return;
    if (!hasValidBounds(bounds)) return;

    const { directions } = bounds;
    const feature = isPoint
      ? createPointFeature(directions)
      : createPolygonFeature(directions);

    const vectorLayer = mapUtils.createVectorLayer({
      features: [feature],
    });
    map.addLayer(vectorLayer);
    mapUtils.zoomToLayer(map, vectorLayer, map.getView().getMaxZoom() - 3);
  };

  // refresh map if bounds have changed
  if (bounds !== previousBounds && map) {
    setPreviousBounds(bounds);
    refreshMap();
  }

  const handleDrawEnd = ({ feature, type }) => {
    const [west, north, east, south] = mapUtils.extentToLonLat(
      feature.getGeometry().getExtent()
    );
    updateBounds({
      directions:
        type === 'point' ? { north, west } : { west, north, east, south },
      type,
    });
  };

  return (
    <Map height="300px">
      {editable ? (
        <DrawControlGroup>
          <MarkerControl onDrawEnd={handleDrawEnd} />
          {includeBoxControl ? <BoxControl onDrawEnd={handleDrawEnd} /> : null}
        </DrawControlGroup>
      ) : null}
      <ControlGroup
        position="bottom-right"
        renderControls={(target) => (
          <>
            <AttributionControl target={target} />
            <ScaleLineControl target={target} />
          </>
        )}
      />
    </Map>
  );
};

GeoEditorMap.propTypes = {
  bounds: PropTypes.shape({
    type: PropTypes.string,
    directions: PropTypes.shape({
      north: PropTypes.number,
      south: PropTypes.number,
      west: PropTypes.number,
      east: PropTypes.number,
    }),
  }),
  updateBounds: PropTypes.func,
  editable: PropTypes.bool,
  includeBoxControl: PropTypes.bool,
};

const GeoEditor = ({ binding, editable, context }) => {
  const geoDetect = config.get('itemstore.geoDetect');
  const [bounds, setBounds] = useState(() => getBindingBounds(binding));

  useEffect(() => {
    context.clear = () => {
      clearBoundsOutsideOfBinding(binding);
      setBounds();
    };
    return () => {
      if (!binding.isValid()) {
        clearBoundsOutsideOfBinding(binding);
      }
    };
  }, [context, binding]);

  const handleUpdateBounds = useCallback(
    (newBounds) => {
      setBounds(newBounds);
      if (newBounds) {
        if (!hasValidBounds(newBounds)) return;
        setBindingBounds(binding, newBounds);
      }
    },
    [binding]
  );

  return (
    <div className="escoGeoEditor">
      {geoDetect && editable && (
        <GeoDetect
          geoDetect={geoDetect}
          binding={binding}
          updateBounds={handleUpdateBounds}
        />
      )}
      <Grid container spacing={3}>
        <Grid className="escoGeoEditor__map" item xs={editable ? 8 : 12}>
          <MapProvider>
            <GeoEditorMap
              bounds={bounds}
              updateBounds={handleUpdateBounds}
              editable={editable}
              includeBoxControl={supportsBoundingBoxInBinding(binding)}
            />
          </MapProvider>
        </Grid>
        {editable ? (
          <Grid item xs={4}>
            {bounds?.type === 'point' ? (
              <LatlngDirections
                directions={bounds?.directions}
                updateBounds={handleUpdateBounds}
                editable={editable}
              />
            ) : (
              <BoxDirections
                directions={bounds?.directions}
                updateBounds={handleUpdateBounds}
                editable
              />
            )}
          </Grid>
        ) : null}
      </Grid>
    </div>
  );
};

GeoEditor.propTypes = {
  binding: PropTypes.shape({
    getValue: PropTypes.func,
    setValue: PropTypes.func,
    isValid: PropTypes.func,
  }),
  context: PropTypes.shape({
    clear: PropTypes.func,
  }),
  editable: PropTypes.bool,
};

export default GeoEditor;
