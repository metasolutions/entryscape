import escaCatalogNLS from 'catalog/nls/escaCatalog.nls';
import escoListNLS from 'commons/nls/escoList.nls';
import CreateDialog from 'catalog/catalogs/dialogs/CreateDialog';
import { isUserAllowedCatalogCreation } from 'catalog/utils/catalog';
import { EDIT_ENTRY_COLUMN } from 'commons/components/EntryListView';
import { withListRefresh } from 'commons/components/ListView';
import { LIST_ACTION_EDIT } from 'commons/components/EntryListView/actions';
import EditCatalog from '../dialogs/EditCatalog';

const ListCatalogEdit = withListRefresh(EditCatalog, 'onEntryEdit');

export const nlsBundles = [escaCatalogNLS, escoListNLS];

export const listActions = [
  {
    id: 'create',
    Dialog: CreateDialog,
    isVisible: ({ userEntry, userInfo }) =>
      isUserAllowedCatalogCreation(userEntry, userInfo),
    labelNlsKey: 'createButtonLabel',
    tooltipNlsKey: 'actionTooltip',
    highlightId: 'listActionCreate',
  },
];

export const EDIT_CATALOG_COLUMN = {
  ...EDIT_ENTRY_COLUMN,
  getProps: ({ entry, translate }) => ({
    action: {
      ...LIST_ACTION_EDIT,
      entry,
      Dialog: ListCatalogEdit,
    },
    title: translate('editEntry'),
  }),
};
