import FieldsLinkedDataBrowserDialog from 'commons/components/FieldsLinkedDataBrowserDialog';
import { LANGUAGE_LITERAL, LITERAL } from 'commons/components/forms/editors';
import escaVisualizationNLS from 'catalog/nls/escaVisualization.nls';

const NLS_BUNDLES = [escaVisualizationNLS];

const fields = [
  {
    nodetype: LANGUAGE_LITERAL,
    property: 'dcterms:title',
    labelNlsKey: 'vizDialogNameviz',
  },
  {
    nodetype: LITERAL,
    property: 'http://entryscape.com/terms/visualization/chartType',
    labelNlsKey: 'vizDialogTypeTitle',
  },
];

const VisualizationLDBDialog = (props) => {
  return (
    <FieldsLinkedDataBrowserDialog
      fields={fields}
      {...props}
      nlsBundles={NLS_BUNDLES}
    />
  );
};

export default VisualizationLDBDialog;
