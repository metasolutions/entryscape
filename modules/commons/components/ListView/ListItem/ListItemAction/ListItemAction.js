import { Grid } from '@mui/material';
import PropTypes from 'prop-types';

import NoPropagation from 'commons/components/NoPropagatation';

// Wrapper component for list items with actions such
// as buttons.
export const ListItemAction = ({
  xl = false,
  xs = 1,
  justifyContent = 'flex-start',
  noPropagation = true,
  children,
}) => {
  if (noPropagation) {
    return (
      <Grid item xs={xs} xl={xl}>
        <NoPropagation justifyContent={justifyContent} display="flex">
          {children}
        </NoPropagation>
      </Grid>
    );
  }

  return (
    <Grid item xs={xs} xl={xl}>
      {children}
    </Grid>
  );
};

ListItemAction.propTypes = {
  xl: PropTypes.number,
  xs: PropTypes.number,
  justifyContent: PropTypes.string,
  noPropagation: PropTypes.bool,
  children: PropTypes.node,
};
