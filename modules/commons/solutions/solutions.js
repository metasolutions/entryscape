import { fetchJson } from 'commons/util/async';
import merge from 'commons/merge';
import { get, set } from 'lodash-es';

/**
 * Prefixes names starting with underscore
 *
 * @param {string|string[]} nameOrNames
 * @param {string} prefix
 * @returns {string|string[]}
 */
const prefixNames = (nameOrNames, prefix) => {
  if (Array.isArray(nameOrNames)) {
    return nameOrNames.map((name) => {
      if (!name.startsWith('_')) return name;
      return `${prefix}${name}`;
    });
  }
  return nameOrNames.startsWith('_') ? `${prefix}${nameOrNames}` : nameOrNames;
};

/**
 * Prefixes names deep, by setting nested properties with paths. Nested paths
 * are expressed with '.' as delimiter.
 * For nested paths with arrays, the paths need to be recalculated to get the
 * correct path including the element index, for example 'relation[0].
 * entityType'.
 *
 * @param {object} type
 * @param {string} path
 * @param {string} solutionName
 * @returns {undefined}
 */
const prefixNamesDeep = (type, path, solutionName) => {
  // handles simple and nested object paths
  const value = get(type, path);
  if (value) {
    set(type, path, prefixNames(value, solutionName));
    return;
  }

  // handles nested paths with arrays.
  const [property, subProperty] = path.split('.');
  if (!(property in type)) return;
  const nestedValues = type[property];
  if (Array.isArray(nestedValues)) {
    nestedValues.forEach((nestedValue, index) => {
      if (typeof nestedValue === 'object' && nestedValue !== null) {
        const nestedPath = `${property}[${index}].${subProperty}`;
        set(type, nestedPath, prefixNames(get(type, nestedPath), solutionName));
      }
    });
  }
};

/**
 * Prefixes local entity and project type name references with the solution name
 *
 * @param {object[]} types entity or project types
 * @param {string[]} paths paths that should be checked and prefixed
 * @param {string} solutionName
 * @returns {object[]}
 */
const prefixLocalNameReferences = (types, paths, solutionName) => {
  return types.map((type) => {
    paths.forEach((path) => {
      prefixNamesDeep(type, path, solutionName);
    });
    return type;
  });
};

/**
 * Normalizes a solution by primarily prefixing local entity and project type
 * name references with the solution name
 *
 * @param {object} solutionJson
 * @returns {object}
 */
const normalizeSolution = (solutionJson) => {
  const { name, entitytypes, projecttypes, bundles, ...solution } =
    solutionJson;

  if (!name) {
    throw new Error(
      'A solution is missing the mandatory name property. Please provide a unique name.'
    );
  }

  if (entitytypes) {
    solution.entitytypes = prefixLocalNameReferences(
      entitytypes,
      ['name', 'useWith', 'refines', 'relations.entityType'],
      name
    );
  }

  if (projecttypes) {
    solution.projecttypes = prefixLocalNameReferences(
      projecttypes,
      ['name', 'default', 'optional'],
      name
    );
  }

  if (bundles) {
    solution.itemstore = {
      bundles,
    };
  }

  return solution;
};

/**
 * Fetches solution configurations and merges them into a config object. A
 * solution follows same naming convention for entity types, project types and
 * bundles as is used in local.js.
 * When a solution is loaded, some names are prefixed to avoid naming conflicts
 * for entity and project types. All names that starts with underscore _ are
 * considered to be defined locally in that solution file. It's also possible to
 * reference entity types in other solutions in which case the entity type is
 * expected to be prefixed with the solution name and underscore.
 *
 * @param {string[]} solutionURIs
 * @returns {Promise<object>}
 */
export const loadSolutions = async (solutionURIs) => {
  if (!solutionURIs) return;

  // load and normalize solutions
  const solutions = [];
  for (const solutionURI of solutionURIs) {
    const solutionJson = await fetchJson(solutionURI, 'solution');
    if (!solutionJson) return;
    const solution = normalizeSolution(solutionJson);
    solutions.push(solution);
  }

  // merge array of solutions to an object
  const mergedSolutions = merge(...solutions);
  return mergedSolutions;
};
