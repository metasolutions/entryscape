import PropTypes from 'prop-types';
import { Entry } from '@entryscape/entrystore-js';
import config from 'config';
import { useActions, withActionsProvider } from 'commons/components/ListView';
import { LIST_ACTION_INFO } from 'commons/components/EntryListView/actions';
import LinkedDataBrowserDialog from 'commons/components/LinkedDataBrowserDialog';
import useLDBFormsPresenter from 'commons/components/LinkedDataBrowserDialog/useLDBFormsPresenter';
import ConceptURI from './ConceptURI';

const PresentConcept = ({
  entry,
  onUpdateConceptRURI,
  uriPattern,
  uriSpace,
  readOnly,
}) => {
  const templateId = config.get('terms.conceptTemplateId');
  const metadata = entry.getAllMetadata();
  const { openActionDialog } = useActions();
  const handleLinkClick = ({ entry: linkEntry }) =>
    openActionDialog({
      ...LIST_ACTION_INFO,
      entry: linkEntry,
      Dialog: LinkedDataBrowserDialog,
    });

  const { PresenterComponent } = useLDBFormsPresenter(
    handleLinkClick,
    metadata,
    entry.getResourceURI(),
    templateId
  );

  const uriSpacePattern = uriSpace ? `${uriSpace}/\${entryName}` : '';
  const presenterElement = PresenterComponent ? <PresenterComponent /> : null;

  if (readOnly) {
    return presenterElement;
  }

  return (
    <>
      {uriSpacePattern || uriPattern ? (
        <ConceptURI
          entry={entry}
          uriPattern={uriSpacePattern || uriPattern}
          onUpdateConceptRURI={onUpdateConceptRURI}
        />
      ) : null}
      {presenterElement}
    </>
  );
};

PresentConcept.propTypes = {
  entry: PropTypes.instanceOf(Entry),
  onUpdateConceptRURI: PropTypes.func,
  uriPattern: PropTypes.string,
  uriSpace: PropTypes.string,
  readOnly: PropTypes.bool,
};

export default withActionsProvider(PresentConcept);
