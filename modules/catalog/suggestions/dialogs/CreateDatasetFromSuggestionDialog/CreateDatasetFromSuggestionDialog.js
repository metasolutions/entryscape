import PropTypes from 'prop-types';
import { useEffect, useState } from 'react';
import { Entry } from '@entryscape/entrystore-js';
import { useESContext } from 'commons/hooks/useESContext';
import useAsync from 'commons/hooks/useAsync';
import {
  useTranslation,
  nlsBundlesPropType,
} from 'commons/hooks/useTranslation';
import { RDF_TYPE_DATASET } from 'commons/util/entry';
import { createPrototypeDatasetEntry } from 'catalog/datasets/DatasetOverview/utils/datasets';
import { createLinkedDatasetEntry } from 'catalog/suggestions/util';
// eslint-disable-next-line max-len
import CreateEntryByTemplateDialog from 'commons/components/EntryListView/dialogs/create/CreateEntryByTemplateDialog';
import useGetDatasetTemplateEntry from 'catalog/datasetTemplates/useGetDatasetTemplateEntry';
import { LEVEL_MANDATORY } from 'commons/components/rdforms/LevelSelector';
import { getMetadataFromDatasetTemplate } from 'catalog/datasetTemplates/util';

const predicatesToReplace = ['dcterms:title', 'dcterms:description'];

const CreateDatasetFromSuggestionDialog = ({
  actionParams,
  nlsBundles,
  ...props
}) => {
  const { entry: suggestionEntry } = actionParams;
  const { context } = useESContext();
  const { data: prototypeEntry, runAsync } = useAsync(null);
  const [datasetTemplateEntry, datasetTemplateEntryStatus] =
    useGetDatasetTemplateEntry(suggestionEntry);
  const [protoEntryInitialized, setProtoEntryInitialized] = useState(false);

  const findAndReplace = (
    targetGraph,
    sourceGraph,
    targetSubject,
    sourceSubject,
    predicate
  ) => {
    const oldStatements = targetGraph.find(targetSubject, predicate);
    oldStatements.forEach((oldStatement) => {
      targetGraph.remove(oldStatement);
    });
    const newStatements = sourceGraph.find(sourceSubject, predicate);
    newStatements.forEach((newStatement) => {
      newStatement.setSubject(targetSubject);
      targetGraph.add(newStatement);
    });
  };

  useEffect(() => {
    runAsync(
      createPrototypeDatasetEntry(context, RDF_TYPE_DATASET).then(
        (datasetPrototypeEntry) => {
          // Automatically copies over title and descriptions from suggestion to Dataset,
          // easier to remove if not needed than to locate and copy paste.
          const metadata = suggestionEntry.getMetadata();
          predicatesToReplace.forEach((predicate) => {
            metadata
              .find(suggestionEntry.getResourceURI(), predicate)
              .forEach((statement) => {
                datasetPrototypeEntry.add(
                  statement.getPredicate(),
                  statement.getObject()
                );
              });
          });
          return datasetPrototypeEntry;
        }
      )
    );
  }, [context, suggestionEntry, runAsync]);

  useEffect(() => {
    if (!prototypeEntry || datasetTemplateEntryStatus !== 'resolved') return;

    if (!datasetTemplateEntry && prototypeEntry) {
      setProtoEntryInitialized(true);
      return;
    }
    const metadata = getMetadataFromDatasetTemplate(
      datasetTemplateEntry,
      prototypeEntry
    );

    const suggestionMetadata = prototypeEntry.getMetadata();

    prototypeEntry.setMetadata(metadata);

    predicatesToReplace.forEach((predicate) => {
      findAndReplace(
        prototypeEntry.getMetadata(),
        suggestionMetadata,
        prototypeEntry.getResourceURI(),
        prototypeEntry.getResourceURI(),
        predicate
      );
    });

    setProtoEntryInitialized(prototypeEntry.getMetadata().isChanged());
  }, [datasetTemplateEntry, prototypeEntry, datasetTemplateEntryStatus]);

  const translate = useTranslation(nlsBundles);

  const handleCreateLinkedDataset = async (entry, graph) =>
    createLinkedDatasetEntry(entry, graph, suggestionEntry);

  return (
    <>
      {protoEntryInitialized ? (
        <CreateEntryByTemplateDialog
          {...actionParams}
          {...props}
          entry={prototypeEntry}
          editorLevel={LEVEL_MANDATORY}
          createEntry={handleCreateLinkedDataset}
          createHeaderLabel={translate('createDatasetMenu')}
          fromDatasetTemplate
        />
      ) : null}
    </>
  );
};

CreateDatasetFromSuggestionDialog.propTypes = {
  actionParams: PropTypes.shape({ entry: PropTypes.instanceOf(Entry) }),
  nlsBundles: nlsBundlesPropType,
  onCreate: PropTypes.func,
};
export default CreateDatasetFromSuggestionDialog;
