import PropTypes from 'prop-types';
import { Entry, Context } from '@entryscape/entrystore-js';
import { EntityType } from 'entitytype-lookup';
// eslint-disable-next-line max-len
import CreateEntryByLinkOrFile from 'commons/components/EntryListView/dialogs/create/CreateEntryByLinkOrFile';
import { ONLY_LINK } from 'commons/components/EntryListView/dialogs/create/handlers';
import { useESContext } from 'commons/hooks/useESContext';
import { LEVEL_OPTIONAL } from 'commons/components/rdforms/LevelSelector';
import Lookup from 'commons/types/Lookup';
import { useState, useEffect } from 'react';
import {
  createEntry,
  createEntityTypeEntryFromLink,
  getGroupWithHomeContext,
} from 'commons/util/store';
import { useSnackbar } from 'commons/hooks/useSnackbar';
import { useTranslation } from 'commons/hooks/useTranslation';
import esmoClassesNLS from 'models/nls/esmoClasses.nls';
import { CreateNamespaceLinkField } from 'models/editors/NamespaceLinkField';

/**
 * TODO: Move to utility, copied from modules/workbench/entitytypes/list/dialogs/CreateDialog.js
 *
 * Explicitly sets the ACL of an entityType entry so that it's not public by default
 *
 * @param {Entry} entry
 * @param {Context} context
 * @param {EntityType} entityType
 * @returns {Promise}
 */
const setEntititypeEntryNonPublic = async (entry, context, entityType) => {
  const publishable = entityType.publishControl === true;
  if (!publishable) return;

  const groupEntry = await getGroupWithHomeContext(context);
  const entryInformation = entry.getEntryInfo();
  const accessControl = entryInformation.getACL(true);
  accessControl.admin.push(groupEntry.getId());
  entryInformation.setACL(accessControl);
  return entryInformation.commit();
};
const CreateEntryDialog = (props) => {
  const { context } = useESContext();
  const { rdfType } = props;
  const prototypeEntry = createEntry(context, rdfType);
  prototypeEntry.add('rdf:type', rdfType);
  const [entityType, setEntityType] = useState();
  const [addSnackbar] = useSnackbar();
  const translate = useTranslation(esmoClassesNLS);

  useEffect(() => {
    Lookup.inUse(prototypeEntry).then((type) => {
      setEntityType(type);
    });
  }, [prototypeEntry]);

  const handleCreateEntry = async (
    entry,
    graph,
    _currentContext,
    createEntryParams
  ) => {
    const newEntry = await createEntityTypeEntryFromLink({
      context,
      prototypeEntry: entry,
      graph,
      ...createEntryParams,
    });
    await setEntititypeEntryNonPublic(newEntry, context, entityType);
    return newEntry;
  };

  return entityType ? (
    <CreateEntryByLinkOrFile
      {...props}
      entry={prototypeEntry}
      entityType={entityType.get()}
      editorLevel={LEVEL_OPTIONAL}
      selectorType={ONLY_LINK}
      createEntry={handleCreateEntry}
      afterCreateEntry={() =>
        addSnackbar({ message: translate('createEntitySuccess') })
      }
      LinkField={CreateNamespaceLinkField}
    />
  ) : null;
};

CreateEntryDialog.propTypes = {
  rdfType: PropTypes.string,
};

export default CreateEntryDialog;
