import config from 'config';
import { negate, escape } from 'lodash-es';
import { Entry } from '@entryscape/entrystore-js';
import { template } from 'commons/util/template';
import { entrystore } from 'commons/store';
import { spreadEntry } from 'commons/util/store';
import { getLabel } from 'commons/util/rdfUtils';
import { i18n } from 'esi18n';
import esadUserNLS from 'admin/nls/esadUser.nls';
import {
  CONTEXT_ENTRY_ID_PRINCIPALS,
  CONTEXT_PRINCIPALS,
  USER_ENTRY_ID_ADMIN,
  USER_ENTRY_ID_ADMINS,
  USER_ENTRY_ID_GUEST,
} from './userIds';

export const ADMIN = 'admin';
export const ADMIN_RIGHTS = 'admin-rights';
export const PREMIUM = 'premium';

/**
 * Checks whether configuration for the premium group has been specified. Should
 * be used as a feature toggle.
 *
 * @returns {boolean}
 */
export const isPremiumConfigured = () =>
  Boolean(config.get('entrystore.premiumGroupId'));

/**
 * Checks whether a user is in the premium group.
 *
 * @param {Entry} userEntry
 * @returns {boolean}
 */
export const isUserPremium = (userEntry) => {
  const premiumGroupId = config.get('entrystore.premiumGroupId');
  if (!premiumGroupId) return false;

  const premiumEntryURI = entrystore.getEntryURI(
    CONTEXT_PRINCIPALS,
    premiumGroupId
  );

  return userEntry
    .getParentGroups()
    .some((groupEntryURI) => groupEntryURI === premiumEntryURI);
};

/**
 * Adds a user to the premium group. If the premium group doesn't exist yet, it
 * is created using the ID provided in the configuration.
 *
 * @param {Entry} userEntry
 * @returns {Promise}
 */
export const upgradeUserToPremium = async (userEntry) => {
  const premiumGroupId = config.get('entrystore.premiumGroupId');
  const premiumGroupURI = entrystore.getEntryURI('_principals', premiumGroupId);

  return entrystore
    .getEntry(premiumGroupURI)
    .then(null, () =>
      entrystore.newGroup(`premium ${premiumGroupId}`, premiumGroupId).commit()
    )
    .then((premiumGroupEntry) => {
      return premiumGroupEntry
        .getResource(true)
        .addEntry(userEntry)
        .then(() => {
          userEntry.setRefreshNeeded();
          return userEntry.refresh();
        });
    });
};

/**
 * Removes a user from the premium group. If the premium group doesn't exist yet,
 * it is created using the ID provided in the configuration.
 *
 * @param {Entry} userEntry
 * @returns {Promise}
 */
export const downgradeUserFromPremium = async (userEntry) => {
  const premiumGroupId = config.get('entrystore.premiumGroupId');
  const premiumGroupURI = entrystore.getEntryURI('_principals', premiumGroupId);

  return entrystore
    .getEntry(premiumGroupURI)
    .then(null, () =>
      entrystore.newGroup(premiumGroupId, premiumGroupId).commit()
    )
    .then((premiumGroupEntry) => {
      return premiumGroupEntry
        .getResource(true)
        .removeEntry(userEntry)
        .then(() => {
          userEntry.setRefreshNeeded();
          return userEntry.refresh();
        });
    });
};

/**
 * @param {Entry} userEntry
 * @returns {boolean}
 */
export const isAdmin = (userEntry) => userEntry.getId() === USER_ENTRY_ID_ADMIN;

/**
 * @param {Entry} userEntry
 * @returns {boolean}
 */
export const isGuest = (userEntry) => userEntry.getId() === USER_ENTRY_ID_GUEST;

/**
 *
 * @param {Entry} userEntry
 * @returns {boolean}
 */
export const isAdminOrGuest = (userEntry) =>
  isGuest(userEntry) || isAdmin(userEntry);

/**
 * @see isAdminOrGuest
 */
export const isRegularUser = negate(isAdminOrGuest);

/**
 * @param {Entry} userEntry
 * @returns {boolean}
 */
export const isInAdminGroup = (userEntry) => {
  const adminsGroupEntryURI = entrystore.getEntryURI(
    CONTEXT_ENTRY_ID_PRINCIPALS,
    USER_ENTRY_ID_ADMINS
  );

  const inAdminGroup = userEntry
    .getParentGroups()
    .includes(adminsGroupEntryURI);

  return inAdminGroup;
};

export const hasAdminRights = (userEntry) =>
  isAdmin(userEntry) || isInAdminGroup(userEntry);

export const isManager = (groupEntry, userEntry) =>
  groupEntry.getEntryInfo().getACL().admin.includes(userEntry.getResourceURI());

/**
 *
 * @param {*} userEntry
 * @returns {boolean}
 */
export const isUserDisabled = (userEntry) =>
  userEntry.getEntryInfo().isDisabled();

/**
 * @see isUserDisabled
 */
export const isUserEnabled = negate(isUserDisabled);

/**
 * Check if the username is already in use or some other _principal entry uses this name
 *
 * @param {*} username
 * @returns {Promise.<boolean>}
 */
export const isUsernameInUse = async (username) => {
  const entryNames = await entrystore
    .getREST()
    .get(
      `${entrystore.getBaseURI()}${CONTEXT_ENTRY_ID_PRINCIPALS}?entryname=${username}`
    );

  return entryNames.length > 0; // if there's data (array) then some entries already use that name
};

/**
 *
 * @param {Entry} userEntry
 * @returns {string} the username to render
 */
export const getUserRenderName = (userEntry) => {
  const username =
    userEntry.getEntryInfo().getName() ||
    userEntry.getResource(true)?.getName();

  const name = getLabel(userEntry);
  if (name == null && username == null) {
    const esadUser = i18n.getLocalization(esadUserNLS);
    return template(esadUser.unnamedUser, {
      id: userEntry.getId(),
    });
  }

  return [username, name]
    .filter((part) => part?.trim()) // get rid of undefined
    .map(escape) // XSS
    .join(' - '); // prettify
};

/**
 *
 * @param {*} userEntry
 * @returns {boolean}
 */
export const getIsUserDisabled = (userEntry) =>
  userEntry.getEntryInfo().isDisabled();

/**
 *
 * @param {*} userEntry
 * @returns {object}
 */
export const getAdditionalMetadata = (userEntry) => {
  const metadata = userEntry.getMetadata();
  const userResource = userEntry.getResource(true);

  const firstName =
    metadata.findFirstValue(null, 'http://xmlns.com/foaf/0.1/givenName') || '';
  const lastName =
    metadata.findFirstValue(null, 'http://xmlns.com/foaf/0.1/familyName') || '';

  return {
    firstName,
    lastName,
    get displayName() {
      if (firstName && lastName) {
        return `${firstName} ${lastName}`;
      }
      if (firstName) {
        return firstName;
      }

      return userResource.getName();
    },
    user: userResource.getName(),
    homeContext: userResource.getHomeContext(),
    entryId: userEntry.getId(),
    language: userResource.getLanguage(),
    isAdmin: isAdmin(userEntry),
    inAdminGroup: isInAdminGroup(userEntry),
    hasAdminRights: hasAdminRights(userEntry),
  };
};

export const login = async (username, password) => {
  const auth = entrystore.getAuth();
  const newUserInfo = await auth.login(username, password);
  const userEntry = await auth.getUserEntry();
  const additionalInfo = getAdditionalMetadata(userEntry);
  const userInfo = { ...newUserInfo, ...additionalInfo };
  return { userEntry, userInfo };
};

/**
 * Shouldn't be needed most of times since useUser context can be used.
 * Useful when current user entry is needed outside of component
 *
 * @returns {{userEntry: Entry, userInfo: object}}
 */
export const getCurrentUserEntry = async () => {
  const userEntry = await entrystore.getAuth().getUserEntry();
  const info = await entrystore.getUserInfo();
  const additionalInfo = getAdditionalMetadata(userEntry);
  const userInfo = { ...info, ...additionalInfo };
  return { userEntry, userInfo };
};

// TODO memoize?
export const getGuestUserEntry = async () => {
  const guestUserEntry = await entrystore.getEntry(
    entrystore.getEntryURI(CONTEXT_ENTRY_ID_PRINCIPALS, USER_ENTRY_ID_GUEST)
  );

  const additionalInfo = getAdditionalMetadata(guestUserEntry);
  return { userEntry: guestUserEntry, userInfo: additionalInfo };
};

/**
 * @returns {Promise}
 */
export const logout = async () =>
  entrystore.getAuth().logout().then(getGuestUserEntry);

/**
 * Replaces the user's first/last name
 *
 * @param {Entry} userEntry
 * @param {string} firstName
 * @param {string} lastName
 * @returns {Promise<Entry>}
 */
export const replaceName = (userEntry, firstName, lastName) => {
  const { metadata, ruri: resourceURI } = spreadEntry(userEntry);

  metadata.findAndRemove(resourceURI, 'foaf:firstName', null);
  metadata.findAndRemove(resourceURI, 'foaf:givenName', null);
  metadata.add(userEntry.getResourceURI(), 'foaf:givenName', {
    type: 'literal',
    value: firstName,
  });

  metadata.findAndRemove(resourceURI, 'foaf:lastName', null);
  metadata.findAndRemove(resourceURI, 'foaf:familyName', null);
  metadata.add(resourceURI, 'foaf:familyName', {
    type: 'literal',
    value: lastName,
  });

  metadata.findAndRemove(resourceURI, 'foaf:name');
  metadata.addL(resourceURI, 'foaf:name', `${firstName} ${lastName}`);

  return userEntry.commitMetadata();
};

/**
 * Checks user permissions
 *
 * @param {Entry} userEntry
 * @param {string[]} permissions
 * @returns {boolean}
 */
export const hasUserPermission = (userEntry, permissions) =>
  permissions.some((permission) => {
    switch (permission) {
      case ADMIN:
        return isAdmin(userEntry);
      case ADMIN_RIGHTS:
        return hasAdminRights(userEntry);
      case PREMIUM:
        return isUserPremium(userEntry);
      default:
        console.error(`Unknown permission: ${permission}`);
        return false;
    }
  });
