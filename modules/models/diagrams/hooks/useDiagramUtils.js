import { useEffect } from 'react';
import useAsync from 'commons/hooks/useAsync';

const loadDiagramUtils = () =>
  import(/* webpackChunkName: "diagramutils" */ '../utils/diagramUtils');

export const useDiagramUtils = () => {
  const { data: diagramUtils, status, error, runAsync } = useAsync();

  useEffect(() => {
    if (status !== 'idle') return;
    runAsync(loadDiagramUtils());
  }, [status, runAsync]);

  return { diagramUtils, status, error };
};
