import PropTypes from 'prop-types';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';

const AcknowledgeDialog = ({
  children,
  open,
  handleClose = () => {},
  buttonLabel = 'OK',
}) => {
  return (
    <Dialog
      open={open}
      onClose={handleClose}
      aria-labelledby="acknowledge-dialog-title"
      aria-describedby="acknowledge-dialog-description"
      maxWidth="sm"
      fullWidth
    >
      <DialogContent>
        <DialogContentText id="acknowledge-dialog-description">
          {children}
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose} variant="text" autoFocus>
          {buttonLabel}
        </Button>
      </DialogActions>
    </Dialog>
  );
};

AcknowledgeDialog.propTypes = {
  children: PropTypes.node,
  open: PropTypes.bool.isRequired,
  handleClose: PropTypes.func,
  buttonLabel: PropTypes.string,
};

export default AcknowledgeDialog;
