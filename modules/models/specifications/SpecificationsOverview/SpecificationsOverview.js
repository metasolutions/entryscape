import EntityOverview from 'commons/components/EntityOverview';
import esmoResourcesNLS from 'models/nls/esmoBuilds.nls';
import { sidebarActions } from 'commons/components/EntityOverview/actions';
import escoOverviewNLS from 'commons/nls/escoOverview.nls';
import escoListNLS from 'commons/nls/escoList.nls';
import escoEntityOverviewNLS from 'commons/nls/escoEntityOverview.nls';
import esmoSpecificationsNLS from 'models/nls/esmoSpecifications.nls';
import esmoCommonsNLS from 'models/nls/esmoCommons.nls';
import escoDialogsNLS from 'commons/nls/escoDialogs.nls';
import PreviewSpecDialog from 'models/specifications/dialogs/PreviewSpecDialog';
import {
  withOverviewModelProvider,
  overviewPropsPropType,
} from 'commons/components/overview';
import { createActions } from './actions';
import { RESOURCE_OVERVIEW_NAME } from '../../config/config';

const NLS_BUNDLES = [
  esmoSpecificationsNLS,
  esmoResourcesNLS,
  esmoCommonsNLS,
  escoOverviewNLS,
  escoListNLS,
  escoEntityOverviewNLS,
  escoDialogsNLS,
];

const getAggregationListProps = () => {
  return {
    to: RESOURCE_OVERVIEW_NAME,
    getCreateProps: () => ({ createActions }),
  };
};

const actions = sidebarActions.concat([
  {
    id: 'preview',
    Dialog: PreviewSpecDialog,
    labelNlsKey: 'preview',
  },
]);

const SpecificationsOverview = ({ overviewProps }) => {
  return (
    <EntityOverview
      actions={actions}
      nlsBundles={NLS_BUNDLES}
      getAggregationListProps={getAggregationListProps}
      overviewProps={overviewProps}
    />
  );
};

SpecificationsOverview.propTypes = {
  overviewProps: overviewPropsPropType,
};

export default withOverviewModelProvider(SpecificationsOverview);
