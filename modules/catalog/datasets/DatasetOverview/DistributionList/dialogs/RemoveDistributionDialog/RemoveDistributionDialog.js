import PropTypes from 'prop-types';
import { Entry } from '@entryscape/entrystore-js';
import { useEntry } from 'commons/hooks/useEntry';
import { useTranslation } from 'commons/hooks/useTranslation';
import escoDialogNLS from 'commons/nls/escoDialogs.nls';
import ContentWrapper from 'commons/components/common/ContentWrapper';
import ListActionDialog from 'commons/components/common/dialogs/ListActionDialog';
import { useVisualizations } from 'catalog/datasets/DatasetOverview/hooks/useVisualizations';
import useAsync, { PENDING } from 'commons/hooks/useAsync';
import LoadingButton from 'commons/components/LoadingButton';
import useGetRemoveDistributionParams from './useGetRemoveDistributionParams';

const RemoveDistributionDialog = ({
  entry: distributionEntry,
  apiDistributionResourceURIs,
  refreshDistributions,
  closeDialog,
}) => {
  const datasetEntry = useEntry();
  const { runAsync, status } = useAsync();
  const { actionType, message, remove, removeParams } =
    useGetRemoveDistributionParams(
      distributionEntry,
      datasetEntry,
      apiDistributionResourceURIs
    );
  const translate = useTranslation(escoDialogNLS);
  const closeButtonLabel =
    actionType === 'confirm' ? translate('reject') : translate('ok');
  const { refreshVisualizations } = useVisualizations(datasetEntry);

  const handleRemove = () =>
    runAsync(
      remove(...removeParams)
        .then(refreshDistributions)
        .then(refreshVisualizations)
        .then(closeDialog)
    );

  const dialogActions =
    actionType === 'confirm' ? (
      <LoadingButton
        onClick={handleRemove}
        loading={status === PENDING}
        color="primary"
        autoFocus
      >
        {translate('confirm')}
      </LoadingButton>
    ) : null;

  return (
    <ListActionDialog
      id="remove-distribution"
      actions={dialogActions}
      closeDialog={closeDialog}
      maxWidth="sm"
      closeDialogButtonLabel={closeButtonLabel}
    >
      <ContentWrapper>{message}</ContentWrapper>
    </ListActionDialog>
  );
};

RemoveDistributionDialog.propTypes = {
  entry: PropTypes.instanceOf(Entry),
  apiDistributionResourceURIs: PropTypes.arrayOf(PropTypes.string),
  refreshDistributions: PropTypes.func,
  closeDialog: PropTypes.func,
};

export default RemoveDistributionDialog;
