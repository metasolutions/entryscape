import PropTypes from 'prop-types';
import { useEffect } from 'react';
import { useParams } from 'react-router-dom';
import useAsync from 'commons/hooks/useAsync';
import { getSecondaryNavViews } from 'commons/util/site';
import { MainLayout } from '../MainLayout';

export const AsyncLayoutWrapper = ({
  route,
  viewName,
  children,
  getLayoutProps = getSecondaryNavViews,
}) => {
  const { contextId } = useParams();
  const {
    data: secondaryNavViews,
    status,
    runAsync,
    error,
  } = useAsync({ data: [] });

  useEffect(() => {
    if (status !== 'idle') return;

    runAsync(Promise.resolve(getLayoutProps(route, contextId)));
  }, [getLayoutProps, contextId, runAsync, status, route]);

  return (
    <MainLayout
      route={route}
      viewName={viewName}
      secondaryNavViews={secondaryNavViews}
      error={error}
    >
      {children}
    </MainLayout>
  );
};

AsyncLayoutWrapper.propTypes = {
  children: PropTypes.node,
  route: PropTypes.string,
  viewName: PropTypes.string,
  getLayoutProps: PropTypes.func,
};
