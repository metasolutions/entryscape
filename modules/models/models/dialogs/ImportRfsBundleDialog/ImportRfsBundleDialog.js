import React, { useCallback, useEffect } from 'react';
import { entrystore } from 'commons/store';
import PropTypes from 'prop-types';
import ListActionDialog from 'commons/components/common/dialogs/ListActionDialog';
import { useTranslation } from 'commons/hooks/useTranslation';
import ContentWrapper from 'commons/components/common/ContentWrapper';
import useLinkOrFile from 'commons/components/entry/Import/useLinkOrFile';
import EntryImport from 'commons/components/entry/Import';
import { useMainDialog } from 'commons/hooks/useMainDialog';
import { readFileAsText } from 'commons/util/file';
import ProgressList from 'commons/components/common/ProgressList';
import { STATUS_FAILED, useRunTasks } from 'commons/hooks/useTasksList';
import { AcknowledgeAction } from 'commons/components/common/dialogs/MainDialog';
import { Button } from '@mui/material';
import esmoImportFormsNLS from 'models/nls/esmoImport.nls';
import esmoCommonsNLS from 'models/nls/esmoCommons.nls';
import { addIgnore } from 'commons/errors/utils/async';
import { Entry } from '@entryscape/entrystore-js';
import {
  convertBundleToImportData,
  updateExtensions,
  importGraphs,
} from './utils';
import { TASKS_IMPORT_FORMS } from './tasks';

const NLS_BUNDLES = [esmoImportFormsNLS, esmoCommonsNLS];

const UpdateMessage = ({ updates, broken, brokenList, translate }) => {
  return (
    <>
      <div>
        {translate('updatedExtensionsMessage')}: {updates}
      </div>
      <div>
        {translate('leftBrokenMessage')}: {broken}
      </div>
      {brokenList.length ? <div>({brokenList.join(', ')})</div> : null}
    </>
  );
};

UpdateMessage.propTypes = {
  updates: PropTypes.number,
  broken: PropTypes.number,
  brokenList: PropTypes.arrayOf(PropTypes.string),
  translate: PropTypes.func,
};

const ImportRfsBundleDialog = ({ entry, closeDialog, onImport = () => {} }) => {
  const context = entry.isContext()
    ? entry.getResource(true)
    : entry.getContext();

  const {
    link,
    file,
    isLink,
    setIsLink,
    handleLinkChange,
    handleFileChange,
    buttonEnabled,
    handleTabChange,
    linkIsValid,
  } = useLinkOrFile();

  const {
    open: isMainDialogOpen,
    openMainDialog,
    setDialogContent,
    closeMainDialog,
    setDialogProps,
  } = useMainDialog();

  const { tasks, runTask, updateTaskMessage, allTasksDone, resetTasks } =
    useRunTasks([...TASKS_IMPORT_FORMS]);

  const hasAnyTaskFailed = tasks.some((task) => task.status === STATUS_FAILED);

  const closeDialogs = useCallback(() => {
    closeMainDialog();
    closeDialog();
  }, [closeMainDialog, closeDialog]);

  const translate = useTranslation(NLS_BUNDLES);

  const handleImportSuccess = useCallback(() => {
    onImport();
    closeDialog();
  }, [closeDialog, onImport]);

  useEffect(() => {
    setDialogContent({
      title: translate('importForms'),
      content: <ProgressList tasks={tasks} nlsBundles={[esmoImportFormsNLS]} />,
      actions: (
        <AcknowledgeAction
          onDone={handleImportSuccess}
          isDisabled={!allTasksDone && !hasAnyTaskFailed}
        />
      ),
    });

    if (hasAnyTaskFailed || allTasksDone)
      setDialogProps({
        ignoreBackdropClick: false,
        disableEscapeKeyDown: false,
      });
  }, [
    tasks,
    translate,
    allTasksDone,
    closeDialogs,
    setDialogContent,
    setDialogProps,
    hasAnyTaskFailed,
    handleImportSuccess,
  ]);

  useEffect(() => {
    if (!isMainDialogOpen && hasAnyTaskFailed) {
      resetTasks();
    }
  }, [hasAnyTaskFailed, isMainDialogOpen, resetTasks]);

  /**
   * Upload file
   *
   * @param {HTMLInputElement} inputElement
   * @returns {Promise.<string>}
   */
  const fileUpload = (inputElement) => {
    const fileItem = inputElement.files.item(0);
    return readFileAsText(fileItem);
  };

  /**
   * Upload link
   *
   * @param {string} url
   * @returns {Promise.<string>}
   */
  const linkUpload = (url) => {
    addIgnore('loadViaProxy', true, true);
    return entrystore.loadViaProxy(url, 'application/json').catch((err) => {
      let message;
      if (err.response?.status === 504) {
        message = translate('noResponseFromLink');
      } else {
        message = translate('loadFromLinkProblem');
      }
      throw Error(message);
    });
  };

  const getRdformBundle = async () => {
    const inputValue = isLink ? link : file;
    const getData = isLink ? linkUpload : fileUpload;
    const data = await getData(inputValue);
    if (!data) {
      throw new Error(translate('noDataError'));
    }
    return JSON.parse(data);
  };

  const convertBundleToImportItems = (rdformsBundle) => {
    const graphData = convertBundleToImportData(rdformsBundle);
    const { stats, importItems } = graphData;

    updateTaskMessage(
      'analyze',
      translate('nlsNumberOfFormsAnalyzed', {
        ...stats,
      })
    );
    return importItems;
  };

  const createEntries = async (importItems) => {
    let importedForms = 0;
    const getUpdateProgressDialog = (totalForms) => () => {
      importedForms += 1;

      updateTaskMessage(
        'import',
        translate('nlsNumberOfFormsImported', {
          importedForms,
          totalForms,
        })
      );
    };
    await importGraphs(importItems, getUpdateProgressDialog, context);
  };

  const update = async (importItems) => {
    const updateStatistics = await updateExtensions(importItems);
    updateTaskMessage(
      'update',
      <UpdateMessage translate={translate} {...updateStatistics} />
    );
  };

  const process = async () => {
    openMainDialog({
      title: translate('importForms'),
      content: <ProgressList tasks={tasks} nlsBundles={NLS_BUNDLES} />,
      actions: null,
      dialogProps: {
        ignoreBackdropClick: true,
        disableEscapeKeyDown: true,
      },
    });

    const rdformsBundle = await runTask('parse', getRdformBundle());
    const importItems = await runTask(
      'analyze',
      convertBundleToImportItems(rdformsBundle)
    );
    await runTask('import', createEntries(importItems));
    await runTask('update', update(importItems));
  };

  const actions = (
    <Button onClick={process} disabled={!buttonEnabled}>
      {translate('importButtonLabel')}
    </Button>
  );

  return (
    <ListActionDialog
      id="import-form"
      closeDialog={closeDialog}
      title={translate('importForms')}
      actions={actions}
      maxWidth="md"
    >
      <ContentWrapper>
        <EntryImport
          link={link}
          file={file}
          handleLinkChange={handleLinkChange}
          setIsLink={setIsLink}
          handleFileChange={handleFileChange}
          validURI={linkIsValid}
          handleTabChange={handleTabChange}
        />
      </ContentWrapper>
    </ListActionDialog>
  );
};

ImportRfsBundleDialog.propTypes = {
  entry: PropTypes.instanceOf(Entry),
  closeDialog: PropTypes.func,
  onImport: PropTypes.func,
};

export default ImportRfsBundleDialog;
