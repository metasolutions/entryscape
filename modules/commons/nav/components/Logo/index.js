import PropTypes from 'prop-types';
import { getLogoSource } from 'commons/util/config';

const Logo = ({ type = 'icon', className }) => {
  return <img src={getLogoSource(type)} className={className} alt="logo" />;
};

Logo.propTypes = {
  type: PropTypes.oneOf(['icon', 'full']),
  className: PropTypes.string,
};

export default Logo;
