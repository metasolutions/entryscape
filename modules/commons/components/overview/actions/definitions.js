import {
  OverviewSidebarButton,
  OverviewInfoButton,
} from 'commons/components/overview';
import { getShortDate } from 'commons/util/date';
import { getModifiedDate } from 'commons/util/metadata';
import {
  ACTION_EDIT as ORIGINAL_ACTION_EDIT,
  ACTION_REMOVE as ORIGINAL_ACTION_REMOVE,
  ACTION_INFO as ORIGINAL_ACTION_INFO,
  ACTION_REVISIONS as ORIGINAL_ACTION_REVISIONS,
} from 'commons/actions';
import InfoIcon from '@mui/icons-material/Info';
import PublishToggle from 'commons/components/entry/PublishToggle';
import LinkedDataBrowserDialog from 'commons/components/LinkedDataBrowserDialog';
import OverviewSidebarDivider from '../OverviewSidebarDivider';
import OverviewRemoveEntryDialog from '../dialogs/OverviewRemoveEntryDialog';
import OverviewEditEntryDialog from '../dialogs/OverviewEditEntryDialog';
import OverviewRevisionsEntryDialog from '../dialogs/OverviewRevisionsEntryDialog';

export const ACTION_EDIT = {
  ...ORIGINAL_ACTION_EDIT,
  Dialog: OverviewEditEntryDialog,
  Component: OverviewSidebarButton,
  highlightId: 'overviewActionEdit',
  getProps: () => ({}),
};

export const ACTION_REMOVE = {
  ...ORIGINAL_ACTION_REMOVE,
  Dialog: OverviewRemoveEntryDialog,
  Component: OverviewSidebarButton,
  highlightId: 'overviewActionRemove',
  getProps: () => ({
    color: 'secondary',
  }),
};

export const ACTION_INFO = {
  ...ORIGINAL_ACTION_INFO,
  Dialog: LinkedDataBrowserDialog,
  Component: OverviewInfoButton,
  highlightId: 'overviewActionInfo',
  getProps: () => ({}),
};

export const ACTION_INFO_WITH_ICON = {
  ...ACTION_INFO,
  icon: <InfoIcon />,
  tooltipNlsKey: 'infoEntry',
  highlightId: 'overviewActionInfo',
};

export const ACTION_REVISIONS = {
  ...ORIGINAL_ACTION_REVISIONS,
  Dialog: OverviewRevisionsEntryDialog,
};

export const ACTION_DIVIDER = {
  id: 'divider',
  Component: OverviewSidebarDivider,
};

export const ACTION_PUBLISH = {
  id: 'publish',
  Component: PublishToggle,
};

export const DESCRIPTION_UPDATED_ID = 'updated';

export const DESCRIPTION_UPDATED = {
  id: DESCRIPTION_UPDATED_ID,
  labelNlsKey: 'lastUpdateLabel',
  getValues: ({ entry }) =>
    entry ? [getShortDate(getModifiedDate(entry))] : [],
};

export const DESCRIPTION_CREATED = {
  id: 'create',
  labelNlsKey: 'createdLabel',
  getValues: ({ entry }) =>
    entry ? [getShortDate(entry.getEntryInfo().getCreationDate())] : [],
};

export const DESCRIPTION_PROJECT_TYPE = {
  id: 'projectType',
  labelNlsKey: 'projectTypeLabel',
};
