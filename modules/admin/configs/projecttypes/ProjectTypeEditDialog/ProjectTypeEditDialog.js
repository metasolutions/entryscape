import PropTypes from 'prop-types';
import TypeEditDialog from 'admin/configs/types/TypeEditDialog';
import { EditorProvider, Form } from 'commons/components/forms/editors';
import { Entry } from '@entryscape/entrystore-js';
import { useTranslation } from 'commons/hooks/useTranslation';
import { useListView } from 'commons/components/ListView';
import { editorFields } from '../utils/editorFields';

const ProjectTypeEditDialog = ({ closeDialog, entry, onEdit }) => {
  const { nlsBundles } = useListView();
  const t = useTranslation(nlsBundles);

  return (
    <EditorProvider entry={entry}>
      <TypeEditDialog
        title={t('editHeader')}
        closeDialog={closeDialog}
        nlsBundles={nlsBundles}
        fields={editorFields}
        onEdit={onEdit}
      >
        <Form fields={editorFields} size="small" nlsBundles={nlsBundles} />
      </TypeEditDialog>
    </EditorProvider>
  );
};

ProjectTypeEditDialog.propTypes = {
  closeDialog: PropTypes.func.isRequired,
  onEdit: PropTypes.func,
  entry: PropTypes.instanceOf(Entry),
};

export default ProjectTypeEditDialog;
