import React, { useEffect, useRef, useState } from 'react';
import escoChartNLS from 'commons/nls/escoChart.nls';
import { i18n } from 'esi18n';
import moment from 'moment/moment';
import { merge } from 'lodash-es';
import { COLOURS_TIME_CHART } from '../colors';
import defaultOpts, { getTimeUnitParsingFormat } from './options';
import '../style.css';

/**
 * Given a length guess what date format is appropriate to render.
 *
 * @param {number} dataLength
 * @return {string}
 * @todo remove, and prefer vnode.attrs.type
 */
const guessAxisFormatFromData = (dataLength) => {
  let xAxisDateFormat = 'day';
  if (dataLength > 10 && dataLength < 13) {
    xAxisDateFormat = 'month';
  } else if (dataLength > 13 && dataLength < 25) {
    xAxisDateFormat = 'hour';
  }

  return xAxisDateFormat;
};

export default ({
  timeUnit,
  type = 'bar',
  options = null,
  data,
  colors = [],
  dimensions = {},
}) => {
  const { width = '100%', height = '100%' } = dimensions;
  const chartContainer = useRef(null);
  const [chartInstance, setChartInstance] = useState(null);

  useEffect(() => {
    if (chartContainer && chartContainer.current) {
      import(/* webpackChunkName: "chart-js" */ 'chart.js')
        .then((Chart) => Chart.default)
        .then((Chart) => {
          setChartInstance(
            new Chart(chartContainer.current, {
              type,
              options: merge(defaultOpts, options),
            })
          );
        });
    }
  }, [chartContainer]);

  const dataExists =
    chartInstance &&
    data &&
    data.datasets &&
    data.datasets.length > 0 &&
    data.datasets[0].data.length > 0;
  if (dataExists) {
    const numberOfDataPoints = data.datasets[0].data.length;
    const xAxesTimeUnit =
      timeUnit || guessAxisFormatFromData(numberOfDataPoints);

    const timeUnitFormat = getTimeUnitParsingFormat(xAxesTimeUnit);
    chartInstance.options.tooltips.callbacks.title = (items) =>
      moment(items[0].label, 'MMMM Do YYYY, h A', moment.locale()).format(
        timeUnitFormat
      );
    // update chart data and xAxis if needed
    chartInstance.data = {
      labels: [],
      ...data,
    };

    // merge colours to apply
    const colorsToApply = [...colors, ...COLOURS_TIME_CHART];

    // update chart colors and axes and re-render
    const colorOptionsCount = colorsToApply.length;
    chartInstance.data.datasets.forEach((dataset, idx) => {
      const colorIdx = idx % colorOptionsCount; // repeat colors
      Object.assign(chartInstance.data.datasets[idx], {
        ...colorsToApply[colorIdx],
      }); // shallow is fine
    });
    chartInstance.options.scales.xAxes[0].time.unit = xAxesTimeUnit;
    chartInstance.update();
  }

  const containerStyle = {
    position: 'relative',
    height: `${height}`,
    width: `${width}`,
  };
  const escoChart = i18n.getLocalization(escoChartNLS);
  return (
    <div className="chart-container" style={containerStyle}>
      <div
        className="no-data"
        className={dataExists ? 'escoChart__hidden' : ''}
      >
        {escoChart.noDataMessage}
      </div>
      <canvas
        aria-label="Statistics chart"
        role="img"
        className={dataExists ? '' : 'escoChart__hidden'}
        ref={chartContainer}
      />
    </div>
  );
};
