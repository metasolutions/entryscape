export default {
  status: {
    /**
     * Exclude charts from the status overview using their hard-coded IDs
     *
     * @type {string[]}
     */
    excludeCharts: [],
  },
};
