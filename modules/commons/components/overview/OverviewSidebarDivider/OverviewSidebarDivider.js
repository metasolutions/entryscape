import { Divider } from '@mui/material';
import './OverviewSidebarDivider.scss';

const OverviewSidebarDivider = () => (
  <Divider className="escoOverviewSidebarDivider" />
);

export default OverviewSidebarDivider;
