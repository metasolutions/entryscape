import { entrystore } from 'commons/store';
import MultiList from 'commons/store/MultiList';

export const simpleFetchEntries = async (query, page) => {
  const searchList = query.list();
  const limit = searchList.getLimit();
  const entries = await searchList.getEntries(page);

  let actualSize = searchList.getSize();
  // Search returns negative results sometimes
  if (entries.length < limit || actualSize < 0) {
    actualSize = page * limit + entries.length;
  }

  return {
    entries,
    size: actualSize,
  };
};

export const multiListFetchEntries = async (queries, page) => {
  const lists = queries.map((multiListQuery) => multiListQuery.list());
  const searchList = new MultiList({
    limit: 10,
    lists,
  });

  return {
    entries: await searchList.getEntries(page),
    size: searchList.getSize(),
  };
};

/**
 *
 * entryType refers to rdf:type
 * @param {{entryType: 'string', contextType: 'string'}} param0
 * @param {{userEntry: Entry, userInfo: object}} param1
 */
export const getSearchQuery = ({
  entryType,
  graphType,
  resourceType,
  rdfType,
}) => {
  const query = entrystore.newSolrQuery();

  if (rdfType) {
    query.rdfType(rdfType);
  }

  if (entryType) {
    query.entryType(entryType);
  }

  if (graphType) {
    query.graphType(graphType);
  }

  if (resourceType) {
    query.resourceType(resourceType);
  }

  return [query, simpleFetchEntries];
};
