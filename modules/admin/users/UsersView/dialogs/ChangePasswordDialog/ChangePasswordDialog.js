import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { entrystore } from 'commons/store';
import { TextField, InputAdornment, IconButton } from '@mui/material';
import {
  Visibility as VisibilityIcon,
  VisibilityOff as VisibilityOffIcon,
} from '@mui/icons-material';
import ContentWrapper from 'commons/components/common/ContentWrapper';
import Tooltip from 'commons/components/common/Tooltip';
import { useTranslation } from 'commons/hooks/useTranslation';
import { useUserState } from 'commons/hooks/useUser';
import { hasAdminRights } from 'commons/util/user';
import ListActionDialog from 'commons/components/common/dialogs/ListActionDialog';
import esadUser from 'admin/nls/esadUser.nls';
import escoSignin from 'commons/nls/escoSignin.nls';
import usePassword from 'commons/auth/usePassword';
import { ErrorCatcher } from 'commons/errors/ErrorCatcher';
import { ErrorWithMessage } from 'commons/errors/utils/errors';
import useAsync, { PENDING } from 'commons/hooks/useAsync';
import LoadingButton from 'commons/components/LoadingButton';
import { useSnackbar } from 'commons/hooks/useSnackbar';
import './style.scss';

const ChangePasswordDialog = ({ entry: userEntry, closeDialog }) => {
  const translate = useTranslation([esadUser, escoSignin]);
  const {
    password,
    passwordError,
    isPasswordVisible,
    handleChangePassword,
    togglePasswordVisibility,
    passwordConfirmation,
    passwordConfirmationError,
    isPasswordConfirmationVisible,
    handleChangePasswordConfirmation,
    togglePasswordConfirmationVisibility,
    currentPassword,
    handleChangeCurrentPassword,
    isCurrentPasswordVisible,
    toggleCurrentVisibility,
  } = usePassword();
  const { userEntry: currentUserEntry } = useUserState();
  const { runAsync, error: changeError, status } = useAsync();
  const hasAdministratorRights = hasAdminRights(currentUserEntry);
  const isChangingOwnPassword = userEntry === currentUserEntry;
  const showCurrentPassword =
    !hasAdministratorRights ||
    (hasAdministratorRights && isChangingOwnPassword);
  const [currentPasswordError, setCurrentPasswordError] = useState(false);

  const hasValidPassAndConfirmation =
    password &&
    !passwordError &&
    passwordConfirmation &&
    !passwordConfirmationError;
  const canSave = showCurrentPassword
    ? hasValidPassAndConfirmation && currentPassword && !currentPasswordError
    : hasValidPassAndConfirmation;

  const [addSnackbar] = useSnackbar();

  const confirmPasswordChange = () => {
    return addSnackbar({ message: translate('setPasswordSuccess') });
  };

  const handleSave = () => {
    const formData = { password: password.trim() };
    if (showCurrentPassword) formData.currentPassword = currentPassword.trim();

    return userEntry
      .getResource()
      .then((resourceEntry) =>
        entrystore
          .getREST()
          .put(resourceEntry.getResourceURI(), JSON.stringify(formData))
      )
      .then(closeDialog)
      .then(confirmPasswordChange)
      .catch((error) => {
        if (error?.status === 403) {
          setCurrentPasswordError(true);
        } else throw new ErrorWithMessage(translate('setPasswordError'), error);
      });
  };

  const actions = (
    <LoadingButton
      loading={status === PENDING}
      disabled={!canSave}
      onClick={() => runAsync(handleSave())}
    >
      {translate('setPasswordButton')}
    </LoadingButton>
  );

  return (
    <>
      <ListActionDialog
        id="change-password"
        title={translate('setPasswordHeader')}
        actions={actions}
        closeDialog={closeDialog}
        maxWidth="sm"
      >
        <ContentWrapper>
          <form autoComplete="on">
            {showCurrentPassword ? (
              <TextField
                autoComplete="current-password"
                autoFocus
                id="current-password-input"
                aria-describedby="current-password-input"
                label={translate('currentPassword')}
                error={Boolean(currentPasswordError)}
                helperText={
                  currentPasswordError
                    ? translate('currentPasswordError')
                    : null
                }
                onChange={({ target }) => {
                  setCurrentPasswordError(false);
                  handleChangeCurrentPassword(target.value);
                }}
                type={isCurrentPasswordVisible ? 'text' : 'password'}
                required
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="end">
                      <Tooltip title={translate('passwordVisibility')}>
                        <IconButton
                          aria-label={translate('passwordVisibility')}
                          onClick={toggleCurrentVisibility}
                          onMouseDown={(e) => e.preventDefault()}
                        >
                          {isCurrentPasswordVisible ? (
                            <VisibilityIcon />
                          ) : (
                            <VisibilityOffIcon />
                          )}
                        </IconButton>
                      </Tooltip>
                    </InputAdornment>
                  ),
                }}
              />
            ) : null}

            <TextField
              autoComplete="new-password"
              autoFocus={!showCurrentPassword}
              error={Boolean(passwordError)}
              id="password-input"
              aria-describedby="password-input"
              label={translate('resetToNewPassword')}
              helperText={passwordError}
              onChange={({ target }) => handleChangePassword(target.value)}
              type={isPasswordVisible ? 'text' : 'password'}
              required
              InputProps={{
                endAdornment: (
                  <InputAdornment position="end">
                    <Tooltip title={translate('passwordVisibility')}>
                      <IconButton
                        aria-label={translate('passwordVisibility')}
                        onClick={togglePasswordVisibility}
                        onMouseDown={(e) => e.preventDefault()}
                      >
                        {isPasswordVisible ? (
                          <VisibilityIcon />
                        ) : (
                          <VisibilityOffIcon />
                        )}
                      </IconButton>
                    </Tooltip>
                  </InputAdornment>
                ),
              }}
            />
            <TextField
              autoComplete="new-password"
              error={Boolean(passwordConfirmationError)}
              id="password-confirm-input"
              aria-describedby="password-confirm-input"
              label={translate('confirmPassword')}
              helperText={passwordConfirmationError}
              onChange={({ target }) =>
                handleChangePasswordConfirmation(target.value)
              }
              type={isPasswordConfirmationVisible ? 'text' : 'password'}
              required
              InputProps={{
                endAdornment: (
                  <InputAdornment position="end">
                    <Tooltip title={translate('passwordVisibility')}>
                      <IconButton
                        aria-label={translate('passwordVisibility')}
                        onClick={togglePasswordConfirmationVisibility}
                        onMouseDown={(e) => e.preventDefault()}
                      >
                        {isPasswordConfirmationVisible ? (
                          <VisibilityIcon />
                        ) : (
                          <VisibilityOffIcon />
                        )}
                      </IconButton>
                    </Tooltip>
                  </InputAdornment>
                ),
              }}
            />
          </form>
        </ContentWrapper>
      </ListActionDialog>
      <ErrorCatcher error={changeError} />
    </>
  );
};

ChangePasswordDialog.propTypes = {
  closeDialog: PropTypes.func.isRequired,
  entry: PropTypes.shape({ getResource: PropTypes.func.isRequired }).isRequired,
};

export default ChangePasswordDialog;
