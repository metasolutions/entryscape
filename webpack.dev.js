const webpack = require('webpack');
const { merge } = require('webpack-merge');
const getCommonConfig = require('./webpack.common');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CircularDependencyPlugin = require('circular-dependency-plugin');

const serverType = 'https';

module.exports = ({
  app = 'suite', // needed for eslint to read the config
  host = 'localhost',
  'nls-warnings': nlsWarnings,
  port = 8080,
  circularPlugin = false,
} = {}) => {
  const showNLSWarnings = nlsWarnings || false;
  const commonConfig = getCommonConfig({ app, showNLSWarnings });

  const config = merge(commonConfig, {
    mode: 'development',
    entry: [
      path.join(__dirname, 'modules', 'commons', 'util', 'onLoadDev.js'),
      path.join(__dirname, 'app', app, 'index.js'),
    ],
    output: {
      chunkFilename: '[name].js',
      pathinfo: false,
      publicPath: '/',
    },
    context: path.join(__dirname, 'app', app),
    devtool: 'inline-source-map',
    devServer: {
      host,
      server: serverType,
      port,
      hot: true,
      historyApiFallback: true,
      client: {
        overlay: false,
      },
      static: {
        directory: path.join(__dirname, 'app', app),
      },
    },
    watchOptions: {
      ignored: /node_modules/,
    },
    plugins: [
      new webpack.DefinePlugin({
        DEVELOPMENT: true,
        DEVELOPMENT_HOST: JSON.stringify(`${serverType}://${host}:${port}/`),
      }),
      // TODO: enable again when circular deps with config is solved
      circularPlugin
        ? new CircularDependencyPlugin({
            // exclude detection of files based on a RegExp
            exclude: /a\.js|node_modules/,
            // add errors to webpack instead of warnings
            failOnError: false,
            // allow import cycles that include an asyncronous import,
            // e.g. via import(/* webpackMode: "weak" */ './file.js')
            allowAsyncCycles: false,
            // set the current working directory for displaying module paths
            cwd: process.cwd(),
          })
        : null,
      new HtmlWebpackPlugin({
        template: path.join(__dirname, 'app', app, 'index.dev.html'),
      }),
    ].filter((plugin) => plugin),
    optimization: {
      removeAvailableModules: false,
      removeEmptyChunks: false,
      splitChunks: false,
    },
  });

  return config;
};
