import { useState, useMemo } from 'react';
import PropTypes from 'prop-types';
import { Entry } from '@entryscape/entrystore-js';
import { entrystore } from 'commons/store';
import {
  List,
  ListItem,
  ListItemText,
  ListItemIcon,
  ListHeader,
  ListHeaderItemSort,
  ListView,
  ListPagination,
  withListModelProvider,
  useListQuery,
  useListModel,
  ListItemOpenMenuButton,
  ActionsMenu,
  ListItemActionsGroup,
} from 'commons/components/ListView';
import { getIconFromActionId } from 'commons/actions';
import { useTranslation } from 'commons/hooks/useTranslation';
import { TERMINOLOGY_OVERVIEW_NAME } from 'terms/config/config';
import esteCollectionNLS from 'terms/nls/esteCollection.nls';
import escoListNLS from 'commons/nls/escoList.nls';
import Tooltip from 'commons/components/common/Tooltip';
import { IconButton } from '@mui/material';
import InfoIcon from '@mui/icons-material/Info';
import { OverviewListHeader } from 'commons/components/overview/OverviewListHeader';
import { OverviewListPlaceholder } from 'commons/components/overview';
import { entriesToListQueryItems } from 'commons/components/ListView/utils/listQueryUtils';
import {
  RESOLVED,
  asyncStatusPropType,
  getAsyncStatus,
} from 'commons/hooks/useAsync';
import LinkedDataBrowserDialog from 'commons/components/LinkedDataBrowserDialog';
import { getPathFromViewName } from 'commons/util/site';
import { LIST_ACTION_INFO } from 'commons/components/EntryListView/actions';
import useGetSourceTerminologyEntries from '../hooks/useGetSourceTerminologyEntries';
import MembersDialog from '../../dialogs/MembersDialog';
import OtherStoreIcon from '../OtherStoreIcon';
import './index.scss';

const SEARCH_FIELDS = ['label'];

const nlsBundles = [escoListNLS, esteCollectionNLS];

const CollectionSourceTerminologiesList = ({
  collectionConcepts,
  collectionEntry,
  onConceptsChange,
  conceptsStatus,
}) => {
  const t = useTranslation(nlsBundles);
  const [infoDialogEntry, setInfoDialogEntry] = useState(null);
  const [{ search }] = useListModel();
  const [sourceTerminologyEntries, sourceTerminologiesStatus] =
    useGetSourceTerminologyEntries(collectionConcepts, conceptsStatus);
  const terminologyListQueryItems = useMemo(
    () =>
      conceptsStatus === RESOLVED
        ? entriesToListQueryItems(sourceTerminologyEntries)
        : null,
    [conceptsStatus, sourceTerminologyEntries]
  );
  const { result: sourceTerminologies, size } = useListQuery({
    items: terminologyListQueryItems,
    searchFields: SEARCH_FIELDS,
  });

  const status = getAsyncStatus([conceptsStatus, sourceTerminologiesStatus]);

  return (
    <>
      <OverviewListHeader
        heading={t('terminologiesListHeader')}
        helperText={t('sourceTerminologiesDescription')}
      />
      <ListView
        search={search}
        size={size}
        nlsBundles={nlsBundles}
        renderPlaceholder={() => (
          <OverviewListPlaceholder label={t('terminologiesListPlaceholder')} />
        )}
        status={status}
      >
        <ListHeader>
          <ListHeaderItemSort
            xs={8}
            sortBy="label"
            text={t('terminologiesListHeaderLabel')}
          />
          <ListHeaderItemSort
            sortBy="modified"
            text={t('terminologiesListHeaderModifiedDate')}
            xs={3}
          />
        </ListHeader>
        <List renderLoader={(Loader) => <Loader height="10vh" />}>
          {sourceTerminologies.map(({ entry, label, modified }) => {
            const fromOtherStore = entry.getEntryStore() !== entrystore;
            const overviewPath = !fromOtherStore
              ? getPathFromViewName(TERMINOLOGY_OVERVIEW_NAME, {
                  contextId: entry.getContext().getId(),
                  entryId: entry.getId(),
                })
              : '';

            return (
              <ListItem
                key={`${entry.getId()}-${entry.getContext().getId()}`}
                to={overviewPath}
              >
                <ListItemText
                  xs={7}
                  primary={label}
                  classes={{
                    primary: 'esteSourceTerminologiesListItemText__primary',
                  }}
                />
                <ListItemIcon
                  xs={1}
                  icon={fromOtherStore ? <OtherStoreIcon /> : null}
                />
                <ListItemText secondary={modified} xs={2} />
                <ListItemActionsGroup
                  xs={1}
                  actions={[
                    {
                      ...LIST_ACTION_INFO,
                      Dialog: LinkedDataBrowserDialog,
                      getProps: () => ({
                        entry,
                        icon: <InfoIcon />,
                        dialogTitle: t('terminologyInfoTooltip'),
                        onClick: () => setInfoDialogEntry(entry),
                      }),
                    },
                  ]}
                />
                <ListItemOpenMenuButton />
                <ActionsMenu
                  items={[
                    {
                      action: {
                        Dialog: MembersDialog,
                        collectionEntry,
                        callback: onConceptsChange,
                        conceptSchemeEntry: entry,
                      },
                      icon: getIconFromActionId('edit'),
                      label: t('manageMembers'),
                    },
                  ]}
                />
              </ListItem>
            );
          })}
        </List>
        <ListPagination />
      </ListView>
      {infoDialogEntry && (
        <LinkedDataBrowserDialog
          entry={infoDialogEntry}
          closeDialog={() => setInfoDialogEntry(null)}
        />
      )}
    </>
  );
};

CollectionSourceTerminologiesList.propTypes = {
  collectionConcepts: PropTypes.arrayOf(PropTypes.instanceOf(Entry)),
  collectionEntry: PropTypes.instanceOf(Entry),
  onConceptsChange: PropTypes.func,
  conceptsStatus: asyncStatusPropType,
};

const InfoButton = ({ label, onClick }) => {
  return (
    <Tooltip title={label}>
      <IconButton aria-label={label} onClick={onClick}>
        <InfoIcon />
      </IconButton>
    </Tooltip>
  );
};

InfoButton.propTypes = {
  label: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
};

export default withListModelProvider(CollectionSourceTerminologiesList, () => ({
  limit: 5,
}));
