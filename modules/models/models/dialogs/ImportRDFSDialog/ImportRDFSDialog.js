import React, { useCallback, useEffect } from 'react';
import { entrystore } from 'commons/store';
import PropTypes from 'prop-types';
import ListActionDialog from 'commons/components/common/dialogs/ListActionDialog';
import { useTranslation } from 'commons/hooks/useTranslation';
import ContentWrapper from 'commons/components/common/ContentWrapper';
import useLinkOrFile from 'commons/components/entry/Import/useLinkOrFile';
import EntryImport from 'commons/components/entry/Import';
import { useMainDialog } from 'commons/hooks/useMainDialog';
import { readFileAsText } from 'commons/util/file';
import ProgressList from 'commons/components/common/ProgressList';
import {
  useTasksList,
  STATUS_PROGRESS,
  STATUS_DONE,
  STATUS_FAILED,
} from 'commons/hooks/useTasksList';
import { AcknowledgeAction } from 'commons/components/common/dialogs/MainDialog';
import { Button } from '@mui/material';
import esmoImportRDFSNLS from 'models/nls/esmoImportRDFS.nls';
import { addIgnore } from 'commons/errors/utils/async';
import { Entry } from '@entryscape/entrystore-js';
import { converters, Rdfs } from '@entryscape/rdfjson';
import TASKS_IMPORT_RDFS from './tasks';
import {
  excludeClasses,
  excludeProperties,
  filterGenerator,
  importClasses,
  importProperties,
} from './utils';

const NLS_BUNDLES = [esmoImportRDFSNLS];

const ImportRDFSDialog = ({ entry, closeDialog, onImport }) => {
  const context = entry.isContext()
    ? entry.getResource(true)
    : entry.getContext();

  const {
    link,
    file,
    isLink,
    setIsLink,
    handleLinkChange,
    handleFileChange,
    buttonEnabled,
    handleTabChange,
    linkIsValid,
  } = useLinkOrFile();

  const {
    open: isMainDialogOpen,
    openMainDialog,
    setDialogContent,
    closeMainDialog,
    setDialogProps,
  } = useMainDialog();

  const { tasks, updateTask, allTasksDone, resetTasks } = useTasksList([
    ...TASKS_IMPORT_RDFS,
  ]);

  const hasAnyTaskFailed = tasks.some((task) => task.status === STATUS_FAILED);

  const closeDialogs = useCallback(() => {
    closeMainDialog();
    closeDialog();
  }, [closeMainDialog, closeDialog]);

  const handleImportSuccess = useCallback(() => {
    onImport();
    closeDialogs();
  }, [closeDialogs, onImport]);

  const translate = useTranslation(NLS_BUNDLES);

  useEffect(() => {
    setDialogContent({
      title: translate('importRDFS'),
      content: <ProgressList tasks={tasks} nlsBundles={NLS_BUNDLES} />,
      actions: (
        <AcknowledgeAction
          onDone={closeDialogs}
          isDisabled={!allTasksDone && !hasAnyTaskFailed}
        />
      ),
    });

    if (hasAnyTaskFailed || allTasksDone)
      setDialogProps({
        ignoreBackdropClick: false,
        disableEscapeKeyDown: false,
      });
  }, [
    tasks,
    translate,
    allTasksDone,
    closeDialogs,
    setDialogContent,
    setDialogProps,
    hasAnyTaskFailed,
    handleImportSuccess,
  ]);

  useEffect(() => {
    if (!isMainDialogOpen && hasAnyTaskFailed) {
      resetTasks();
    }
  }, [hasAnyTaskFailed, isMainDialogOpen, resetTasks]);

  /**
   * Upload file
   *
   * @param {HTMLInputElement} inputElement
   * @returns {Promise.<string>}
   */
  const fileUpload = (inputElement) => {
    const fileItem = inputElement.files.item(0);
    return readFileAsText(fileItem);
  };

  /**
   * Upload link
   *
   * @param {string} url
   * @returns {Promise.<string>}
   */
  const linkUpload = (url) => {
    addIgnore('loadViaProxy', true, true);
    return entrystore.loadViaProxy(url, 'application/rdf+xml').catch((err) => {
      const message =
        err.response?.status === 504
          ? translate('noResponseFromLink')
          : translate('loadFromLinkProblem');
      throw Error(message);
    });
  };

  /**
   * Process tasks
   *
   * @returns {Promise.<string>}
   */
  const process = async () => {
    const inputValue = isLink ? link : file;

    openMainDialog({
      title: translate('importRDFS'),
      content: <ProgressList tasks={tasks} nlsBundles={NLS_BUNDLES} />,
      actions: null,
      dialogProps: {
        ignoreBackdropClick: true,
        disableEscapeKeyDown: true,
      },
    });

    updateTask('parse', { status: STATUS_PROGRESS });

    let graph;
    try {
      const uploadFunction = isLink ? linkUpload : fileUpload;
      const uploadData = await uploadFunction(inputValue);

      const report = await converters.detect(uploadData);
      if (report.error) {
        updateTask('parse', { status: STATUS_FAILED, message: report.error });
        return undefined;
      }
      graph = report.graph;
      updateTask('parse', { status: STATUS_DONE });
    } catch (err) {
      updateTask('parse', { status: STATUS_FAILED, message: err.message });
      return undefined;
    }

    updateTask('analysis', { status: STATUS_PROGRESS });

    const addImplicityType = (discoverType, implicityType) =>
      graph
        .find(null, 'rdf:type', discoverType)
        .filter((stmt) => !stmt.isSubjectBlank())
        .map((stmt) => stmt.getSubject())
        .forEach((subj) => graph.add(subj, 'rdf:type', implicityType));

    // Add implicit rdf:Class for every owl:Class
    addImplicityType('owl:Class', 'rdfs:Class');
    // Add implicit rdf:Property for every owl property class
    [
      'owl:AnnotationProperty',
      'owl:ObjectProperty',
      'owl:DatatypeProperty',
      'owl:FunctionalProperty',
      'owl:InverseFunctionalProperty',
      'owl:TransitiveProperty',
      'owl:SymmetricProperty',
      'owl:OntologyProperty',
      'owl:ReflexiveProperty',
      'owl:IrreflexiveProperty',
      'owl:AsymmetricProperty',
      'owl:DeprecatedProperty',
    ].forEach((propType) => addImplicityType(propType, 'rdf:Property'));

    const rdfs = new Rdfs();
    rdfs.addGraph(graph);
    try {
      updateTask('analysis', {
        message: translate('rdfsAnalyzed', {
          classCount: rdfs.getClasses().length,
          propertyCount: rdfs.getProperties().length,
        }),
        status: STATUS_DONE,
      });
    } catch (err) {
      updateTask('analysis', { status: STATUS_FAILED, message: err.message });
      return undefined;
    }

    updateTask('importClasses', { status: STATUS_PROGRESS });

    // Import all the detected classes
    try {
      let importedClasses = 0;
      const classURIs = rdfs
        .getClasses()
        .map((cls) => cls.getURI())
        .filter(filterGenerator(excludeClasses, graph));
      const totalClasses = classURIs.length;
      const importProgress = () => {
        importedClasses += 1;
        updateTask('importClasses', {
          message: translate('classesImported', {
            importedClasses,
            totalClasses,
          }),
        });
      };
      await importClasses(graph, classURIs, context, importProgress);
      updateTask('importClasses', { status: STATUS_DONE });
    } catch (err) {
      updateTask('importClasses', {
        status: STATUS_FAILED,
        message: err.message,
      });
      return undefined;
    }

    // Import all the detected properties
    try {
      let importedProperties = 0;
      const propetyURIs = rdfs
        .getProperties()
        .map((prop) => prop.getURI())
        .filter(filterGenerator(excludeProperties, graph));
      const totalProperties = propetyURIs.length;
      const importProgress = () => {
        importedProperties += 1;
        updateTask('importProperties', {
          message: translate('propertiesImported', {
            importedProperties,
            totalProperties,
          }),
        });
      };
      await importProperties(graph, propetyURIs, context, importProgress);
      updateTask('importProperties', { status: STATUS_DONE });
    } catch (err) {
      updateTask('importProperties', {
        status: STATUS_FAILED,
        message: err.message,
      });
    }
  };

  const actions = (
    <Button onClick={process} disabled={!buttonEnabled}>
      {translate('importButtonLabel')}
    </Button>
  );

  return (
    <ListActionDialog
      id="import-form"
      closeDialog={closeDialog}
      title={translate('importRDFS')}
      actions={actions}
      maxWidth="md"
    >
      <ContentWrapper>
        <EntryImport
          link={link}
          file={file}
          handleLinkChange={handleLinkChange}
          setIsLink={setIsLink}
          handleFileChange={handleFileChange}
          validURI={linkIsValid}
          handleTabChange={handleTabChange}
        />
      </ContentWrapper>
    </ListActionDialog>
  );
};

ImportRDFSDialog.propTypes = {
  entry: PropTypes.instanceOf(Entry),
  closeDialog: PropTypes.func,
  onImport: PropTypes.func,
};

export default ImportRDFSDialog;
