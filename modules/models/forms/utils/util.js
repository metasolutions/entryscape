import { entrystore } from 'commons/store';
import { Graph, namespaces as ns } from '@entryscape/rdfjson';
import { Entry } from '@entryscape/entrystore-js';
import { CONTEXT_TYPE_MODELS } from 'commons/util/context';
import { getLabel } from 'commons/util/rdfUtils';
import {
  RDF_PROPERTY_ITEM,
  RDF_CLASS_EXTENSION,
  RDF_PROPERTY_ORDER,
  RDF_PROPERTY_EXTENDS,
} from 'models/utils/ns';
import { getOrIgnoreEntry } from 'commons/util/entry';
import {
  checkOverrideFormItems,
  findExtendedEntry,
  getExtendedEntry,
} from 'models/utils/extension';
import { copyLanguageLiteral, copyRdfType } from 'models/utils/metadata';

/**
 * getting the blanks for RDF_PROPERTY_ITEM in order
 *
 * @param {object} metadata
 * @param {string} resourceURI
 * @returns {string[]}
 */

const getBlankNodeIds = (metadata, resourceURI) =>
  metadata
    .find(resourceURI, RDF_PROPERTY_ITEM, null)
    .sort(
      (statement1, statement2) =>
        metadata.findFirstValue(statement1.getValue(), RDF_PROPERTY_ORDER) -
        metadata.findFirstValue(statement2.getValue(), RDF_PROPERTY_ORDER)
    )
    .map((statement) => statement.getValue());

export const addItem = (formEntry, graph) => {
  return formEntry.setMetadata(graph).commitMetadata();
};

/**
 * Get form item entries from form
 *
 * @param {Graph} graph
 * @param {string} resourceURI
 * @returns {Promise<Entry[]>}
 */
export const getFormItemEntries = async (graph, resourceURI) => {
  const blankNodeIds = getBlankNodeIds(graph, resourceURI);

  const formItemEntries = [];
  for (const blankNodeId of blankNodeIds) {
    const extendedURI = graph.findFirstValue(blankNodeId, RDF_PROPERTY_EXTENDS);
    const formItemEntry = await getOrIgnoreEntry(
      entrystore.getEntryURIFromURI(extendedURI)
    );
    if (formItemEntry) {
      formItemEntries.push(formItemEntry);
    }
  }
  return formItemEntries;
};

/**
 * Find blank by resourceURI of extended entry
 *
 * @param {Graph} graph
 * @param {string} resourceURI
 * @returns {string}
 */

export const getBlankFromExtend = (graph, extendedResourceURI) => {
  if (!extendedResourceURI) return;
  const [statement] = graph.find(
    null,
    RDF_PROPERTY_EXTENDS,
    extendedResourceURI
  );
  return statement.getSubject();
};

/**
 *
 * @param {Graph} graph
 * @param {Entry} extendedEntry
 * @param {object} labelProperties
 * @returns {string}
 */
export const getLabelFromExtended = (graph, extendedEntry, labelProperties) => {
  if (!extendedEntry) return '';
  const resourceURI = extendedEntry.getResourceURI();
  return (
    getLabel(graph, getBlankFromExtend(graph, resourceURI), labelProperties) ||
    getLabel(extendedEntry, null, labelProperties)
  );
};

/**
 * Function to get all project names for filter options
 *
 * @returns {object[]}
 */
export const getProjectEntries = async () => {
  const ProjectEntries = await entrystore
    .newSolrQuery()
    .rdfType(CONTEXT_TYPE_MODELS)
    .list()
    .getEntries();

  const filterItemProject = ProjectEntries.map((entry) => {
    return {
      value: entry.getId(),
      label: getLabel(entry),
    };
  });
  return filterItemProject;
};

/**
 * Update order in form graph based on order in form item entries array.
 *
 * @param {Entry[]} formItemEntries
 * @param {Graph} graph
 * @returns {Graph}
 */
export const updateOrderInMetadata = (formItemEntries, graph) => {
  formItemEntries.forEach((formItemEntry, index) => {
    const [formItemStatement] = graph.find(
      null,
      RDF_PROPERTY_EXTENDS,
      formItemEntry.getResourceURI()
    );
    const blankNodeId = formItemStatement.getSubject();
    const [orderStatement] = graph.find(blankNodeId, RDF_PROPERTY_ORDER);
    const order = `${index + 1}`;
    orderStatement.setValue(order);
  });
  return graph;
};

/**
 * Adding form item entry as extension in graph
 *
 * @param {Entry} formItemEntry
 * @param {number} order
 * @param {Graph} graph
 * @param {string} resourceURI
 * @returns {Graph}
 */
export const addFormItemInMetadata = (
  formItemEntry,
  order,
  graph,
  resourceURI
) => {
  const blankNodeId = graph.add(resourceURI, RDF_PROPERTY_ITEM).getValue();
  graph.add(blankNodeId, 'rdf:type', RDF_CLASS_EXTENSION);
  graph.addD(blankNodeId, RDF_PROPERTY_ORDER, `${order}`, 'xsd:integer');
  graph.add(blankNodeId, RDF_PROPERTY_EXTENDS, formItemEntry.getResourceURI());
  return graph;
};

/**
 * Delete blank nodes in metadata for form item entry
 *
 * @param {Entry} formItemEntry
 * @param {Graph} graph
 * @param {string} resourceURI
 * @returns {Graph}
 */
export const removeFormItemInMetadata = (formItemEntry, graph, resourceURI) => {
  const blankNodeId = graph
    .find(null, RDF_PROPERTY_EXTENDS, formItemEntry.getResourceURI())[0]
    .getSubject();

  graph.findAndRemove(blankNodeId);
  graph.findAndRemove(resourceURI, RDF_PROPERTY_ITEM, {
    value: blankNodeId,
    type: 'bnode',
  });
  return graph;
};

/**
 *
 * @param {Graph} targetGraph
 * @param {string} targetResourceURI
 * @param {Graph} sourceGraph
 * @param {string} sourceResourceURI
 * @returns {Graph}
 */
const copyFormItems = (
  targetGraph,
  targetResourceURI,
  sourceGraph,
  sourceResourceURI
) => {
  const blankNodeIds = getBlankNodeIds(sourceGraph, sourceResourceURI);

  for (const blankNodeId of blankNodeIds) {
    const newBlankId = targetGraph
      .add(targetResourceURI, RDF_PROPERTY_ITEM)
      .getValue();
    const statements = sourceGraph.find(blankNodeId);

    statements.forEach((statement) => {
      targetGraph.add(
        newBlankId,
        statement.getPredicate(),
        statement.getCleanObject()
      );
    });
  }

  return targetGraph;
};

/**
 *
 * @param {Graph} graph
 * @param {string} resourceURI
 * @returns {boolean}
 */
const checkHasFormItems = (graph, resourceURI) => {
  return Boolean(getBlankNodeIds(graph, resourceURI).length);
};

/**
 *
 * @param {Graph} graph
 * @param {string} resourceURI
 * @returns {Promise<Graph>}
 */
export const copyExtendedFormMetadata = async (graph, resourceURI) => {
  // find first extended entry that has form items
  let sourceEntry = await getExtendedEntry(
    graph,
    resourceURI,
    RDF_PROPERTY_ITEM,
    checkHasFormItems
  );
  // if no form entry with form items found, find closest extended form entry
  if (!sourceEntry) {
    sourceEntry = await findExtendedEntry(graph, resourceURI);
  }

  const sourceGraph = sourceEntry.getMetadata();
  const sourceResourceURI = sourceEntry.getResourceURI();

  copyRdfType(graph, resourceURI, sourceGraph, sourceResourceURI);

  const overrideFormItems = checkOverrideFormItems(graph, resourceURI);
  if (!overrideFormItems) return graph;

  copyFormItems(graph, resourceURI, sourceGraph, sourceResourceURI);

  return graph;
};

/**
 * Find overridden values for a form item. Some properties like order and rdf
 * type are not overridden and are ignored. Title is ignored as well, since a
 * custom one can be provided instead and is handled separately.
 *
 * @param {Graph} graph
 * @param {string} itemBlankNodeId
 * @returns {object}
 */
const findOverriddenValues = (graph, itemBlankNodeId) => {
  const excludeProperties = ['rdf:type', RDF_PROPERTY_ORDER, 'dcterms:title'];
  const items = graph.find(itemBlankNodeId).map((statement) => {
    return {
      property: statement.getPredicate(),
      object: statement.getCleanObject(),
    };
  });

  const ignoreProperties = excludeProperties.map((property) =>
    ns.expand(property)
  );
  return items.filter(({ property }) => {
    return !ignoreProperties.includes(property);
  });
};

/**
 * The title may either be stored as an overriden value in the form or in
 * original form item entry. The overridden title has precedence.
 *
 * @param {Graph} targetGraph
 * @param {string} targetResourceURI
 * @param {Graph} formGraph
 * @param {Entry} formItemEntry
 */
export const copyFormItemTitle = (
  targetGraph,
  targetResourceURI,
  formGraph,
  formItemEntry
) => {
  const formItemResourceURI = formItemEntry.getResourceURI();
  // if the title is overridden, it's stored in a blank on the form
  const formItemBlankId = getBlankFromExtend(formGraph, formItemResourceURI);
  const overriddenTitle = formGraph.findFirstValue(
    formItemBlankId,
    'dcterms:title'
  );

  const sourceGraph = overriddenTitle ? formGraph : formItemEntry.getMetadata();
  const sourceResourceURI = overriddenTitle
    ? formItemBlankId
    : formItemResourceURI;

  copyLanguageLiteral(
    targetGraph,
    targetResourceURI,
    sourceGraph,
    sourceResourceURI,
    'dcterms:title'
  );
};

/**
 * Create prototype form item entry with the basic properties copied from the
 * form and form item entry.
 *
 * @param {Context} context
 * @param {Graph} formGraph
 * @param {Entry} formItemEntry
 * @returns {Entry}
 */
export const createExternalizedEntry = (context, formGraph, formItemEntry) => {
  const prototypeEntry = context.newEntry();
  const protoMetadata = prototypeEntry.getMetadata();
  const protoResourceURI = prototypeEntry.getResourceURI();

  copyFormItemTitle(protoMetadata, protoResourceURI, formGraph, formItemEntry);
  copyRdfType(
    protoMetadata,
    protoResourceURI,
    formItemEntry.getMetadata(),
    formItemEntry.getResourceURI()
  );
  return prototypeEntry;
};

/**
 * Copy the overridden values from the form to the new form item entry and
 * remove the corresponding values from the form. The current form item
 * reference on the form is replaced by a reference to the new form item entry.
 *
 * @param {Graph} formGraph
 * @param {Entry} formItemEntry
 * @param {Graph} externalizedGraph
 * @param {Entry} prototypeEntry
 * @returns {Promise<Entry>}
 */
export const externalizeOverriddenValues = async (
  formGraph,
  formItemEntry,
  externalizedGraph,
  prototypeEntry
) => {
  const formItemResourceURI = formItemEntry.getResourceURI();
  const itemBlankNodeId = getBlankFromExtend(formGraph, formItemResourceURI);
  const overriddenValues = findOverriddenValues(formGraph, itemBlankNodeId);

  for (const { property, object } of overriddenValues) {
    externalizedGraph.add(prototypeEntry.getResourceURI(), property, object);
  }
  prototypeEntry.setMetadata(externalizedGraph);
  const newEntry = await prototypeEntry.commit();

  // remove the overridden values from the form
  for (const { property } of overriddenValues) {
    formGraph.findAndRemove(itemBlankNodeId, property, null, true); // remove silently
  }
  formGraph.add(
    itemBlankNodeId,
    RDF_PROPERTY_EXTENDS,
    newEntry.getResourceURI()
  );

  return newEntry;
};
