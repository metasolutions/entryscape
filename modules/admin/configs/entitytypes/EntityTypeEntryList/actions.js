import AddIcon from '@mui/icons-material/Add';
import { ListAction, withListRerender } from 'commons/components/ListView';
import { useTranslation } from 'commons/hooks/useTranslation';
import esadEntityTypeNLS from 'admin/nls/esadEntityType.nls';
import PropTypes from 'prop-types';
import { Entry } from '@entryscape/entrystore-js';
import {
  ENTITY_TYPE_ID_PREFIX,
  ENTITY_TYPE_CUSTOM,
} from 'admin/configs/utils/namespace';
import { ACTION_EDIT_ID, ACTION_REMOVE_ID } from 'commons/actions/actionIds';
import FieldsLinkedDataBrowserDialog from 'commons/components/FieldsLinkedDataBrowserDialog';
import { fieldPresenters } from '../utils/entityTypePresenters';
import EntityTypeCreateDialog from '../EntityTypeCreateDialog';
import EntityTypeEditDialog from '../EntityTypeEditDialog';
import EntityTypeRemoveDialog from '../EntityTypeRemoveDialog/EntityTypeRemoveDialog';
import { typeConfigToGraph } from '../../utils/exporters';

export const presenterAction = {
  Dialog: FieldsLinkedDataBrowserDialog,
  fields: fieldPresenters,
  typeConfigToGraph: ({ entry, config }) =>
    typeConfigToGraph({
      entry,
      typeConfig: config,
      idPrefix: ENTITY_TYPE_ID_PREFIX,
      fields: fieldPresenters,
    }),
  nlsKeyLabel: 'typeInfoDialogTitle',
};

export const rowActions = [
  {
    id: ACTION_EDIT_ID,
    Dialog: withListRerender(EntityTypeEditDialog, 'onEdit'),
    isVisible: ({ type }) => type === ENTITY_TYPE_CUSTOM,
    nlsKeyLabel: 'editEntry',
  },
  {
    id: ACTION_REMOVE_ID,
    Dialog: EntityTypeRemoveDialog,
    isVisible: ({ type }) => type === ENTITY_TYPE_CUSTOM,
    nlsKeyLabel: 'removeEntryLabel',
  },
];

export const CreateEntryButton = ({ entityTypeEntries, status }) => {
  const t = useTranslation(esadEntityTypeNLS);

  if (status === 'resolved') {
    return (
      <ListAction
        label={t('addLabel')}
        tooltip={t('addTooltip')}
        icon={<AddIcon />}
        action={{ Dialog: EntityTypeCreateDialog, entityTypeEntries }}
      />
    );
  }
  return null;
};

CreateEntryButton.propTypes = {
  entityTypeEntries: PropTypes.arrayOf(PropTypes.instanceOf(Entry)),
  status: PropTypes.string,
};
