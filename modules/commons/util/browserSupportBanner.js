/* eslint-disable */
import UAParser from 'ua-parser-js';

function browserSupportBanner() {
  var parser = new UAParser();
  var result = parser.getResult();
  var browser = result.browser.name;
  var version = result.browser.version.split('.')[0];

  // Supported browsers
  // Chrome v. >= 92
  if (browser === 'Chrome' && version >= 92) return;
  // Firefox v >= 91
  if (browser === 'Firefox' && version >= 91) return;
  // Safari v. >=14
  if (browser === 'Safari' && version >= 14) return;
  // Edge v. >= 93
  if (browser === 'Edge' && version >= 93) return;

  if (window.localStorage.getItem('acknowledgedBrowserSupport')) return;

  // Create stylesheet
  var style = document.createElement('style');
  style.appendChild(document.createTextNode('')); // In order to work in Webkit
  document.getElementsByTagName('head')[0].appendChild(style);

  var styleSheetRules = [
    {
      selector: '#browserSupportBanner',
      rules:
        "position: absolute; z-index: 9999; background-color: #fff; width: 100%; margin: 0px; padding: 10px 30px; text-align: center; color: #474d50; border: 1px solid lightgray; border-radius: 4px; font-family: 'Source Sans Pro', 'Helvetica Neue', Arial, sans-serif; font-weight: 400; line-height: 1.5;",
    },
    {
      selector: '#browserSupportBanner > *',
      rules: 'vertical-align: middle; text-align: left',
    },
    {
      selector: '.browserWarning__container',
      rules: 'display: inline-block; margin: 0 10px 10px 0;',
    },
    {
      selector: '.browserWarning__container p',
      rules: 'font-size: 16px;',
    },
    {
      selector: '.browserWarning__heading',
      rules: 'margin: 0px; font-size: 21px; font-weight: 600;',
    },
    {
      selector: '.buttonContainer',
      rules: 'display: inline-block; width: 100px;',
    },
    {
      selector: '.buttonContainer__button',
      rules:
        'width: 100px; height: 48px; font-size: 16px; background-color: #1673C7; color: #fff; border-width: 0px; border-radius: 4px;',
    },
    {
      selector: '.buttonContainer__button:hover',
      rules: 'cursor: pointer;',
    },
  ];

  for (var i = 0; i < styleSheetRules.length; i++) {
    style.sheet.insertRule(
      styleSheetRules[i].selector + '{' + styleSheetRules[i].rules + '}',
      style.sheet.cssRules.length
    );
  }

  // banner
  var browserSupportDiv = document.getElementById('browserSupportBanner');
  var warningContainer = document.createElement('div');
  var warningHeading = document.createElement('h1');
  var warningParagraph = document.createElement('p');
  var buttonContainer = document.createElement('div');
  var acknowledgeButton = document.createElement('button');

  var buttonText = document.createTextNode('OK');
  var headingText = document.createTextNode(
    'Your browser is not officially supported.'
  );
  var text = document.createTextNode(
    'EntryScape has been tested with the most recent versions of Chrome, Firefox, Edge and Safari.'
  );

  warningContainer.classList.add('browserWarning__container');
  warningHeading.classList.add('browserWarning__heading');
  buttonContainer.classList.add('buttonContainer');
  acknowledgeButton.classList.add('buttonContainer__button');

  warningHeading.appendChild(headingText);
  warningParagraph.appendChild(text);
  acknowledgeButton.appendChild(buttonText);

  warningContainer.appendChild(warningHeading);
  warningContainer.appendChild(warningParagraph);
  buttonContainer.appendChild(acknowledgeButton);

  acknowledgeButton.addEventListener('click', function saveUserAcknowledge() {
    window.localStorage.setItem('acknowledgedBrowserSupport', true);
    document.body.removeChild(browserSupportDiv);
  });

  browserSupportDiv.appendChild(warningContainer);
  browserSupportDiv.appendChild(buttonContainer);
}

export default browserSupportBanner;
