import { useState } from 'react';
import PropTypes from 'prop-types';
import { CropLandscape as BoxIcon } from '@mui/icons-material';
import { useTranslation } from 'commons/hooks/useTranslation';
import { useMapContext } from 'commons/components/Map/hooks/useMapContext';
import { useOpenlayers } from 'commons/components/Map/hooks/useOpenlayers';
import escoMapNls from 'commons/nls/escoMap.nls';
import { ControlButton } from '../ControlButton';
import { useDrawMode } from '../DrawControlGroup';

export const BOX_ACTION = 'box';

export const BoxControl = ({ onDrawEnd }) => {
  const translate = useTranslation(escoMapNls);
  const [drawMode, setDrawMode] = useDrawMode();
  const [{ map }] = useMapContext();
  const mapUtils = useOpenlayers();
  const [drawInteraction, setDrawInteraction] = useState(null);
  const isActive = drawMode === BOX_ACTION;

  const activateDraw = () => {
    const vectorLayer = mapUtils.createVectorLayer();
    const interaction = mapUtils.createDrawInteraction(
      vectorLayer.getSource(),
      'box'
    );
    setDrawInteraction(interaction);

    map.addInteraction(interaction);

    interaction.on('drawabort', () => {
      setDrawMode(null);
    });
    interaction.on('drawend', ({ feature }) => {
      setDrawMode(null);
      onDrawEnd({
        feature,
        type: 'polygon',
      });
    });
  };

  const handleClick = (event) => {
    event.stopPropagation();
    if (isActive) return;
    setDrawMode(BOX_ACTION);
    activateDraw();
  };

  if (drawInteraction && !isActive) {
    map.removeInteraction(drawInteraction);
    setDrawInteraction(null);
  }

  return (
    <ControlButton
      ariaLabel="box"
      active={isActive}
      onClick={handleClick}
      Icon={BoxIcon}
      title={translate('bboxEditTitle')}
    />
  );
};

BoxControl.propTypes = {
  onDrawEnd: PropTypes.func,
};
