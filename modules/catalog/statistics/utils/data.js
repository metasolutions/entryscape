import { entrystoreUtil } from 'commons/store';
import { namespaces as ns } from '@entryscape/rdfjson';
import statsAPI from 'commons/statistics/api';
import {
  getParentEntriesByResourceURI,
  RESOURCE_TYPE_API,
} from 'catalog/statistics/utils/distribution';
import { getRowstoreAPIUUID } from 'catalog/utils/react/rowstoreApi';
import timeRangeUtil from 'catalog/statistics/utils/timeRange';
import { getAbbreviatedMimeType } from 'commons/util/mimeTypes';
import { getLabel } from 'commons/util/rdfUtils';
import { isAPIDistribution } from 'catalog/datasets/DatasetOverview/utils/distributions';
import { spreadEntry } from 'commons/util/store';
import search from 'catalog/statistics/utils/search';

const EMPTY_CHART_DATASET = { datasets: [] };

/**
 * The API distribution entry has a accessURL which points to a Resource URI whose entry refers to the
 * PipelineResult (for the creation of the API).
 *
 * @todo remove as soon as the api/distribution model is fixed and there's a proper way to detect a distribution API
 *
 * @param entry
 * @returns {boolean}
 */
const isPipelineResultEntry = (entry) => {
  const { info, ruri } = spreadEntry(entry);
  const rdfType = info.getGraph().findFirstValue(ruri, 'rdf:type');
  return ns.expand('store:PipelineResult') === rdfType;
};

/**
 * Fetch the distribution/dataset (parent) entry metadata to render their titles
 * @return {Promise<*[]|*>}
 */
export const getStatisticItemsRelevantEntries = async (context, items) => {
  const statsItems = items;
  try {
    const resourceURIs = statsItems.map((item) => item.uri);
    const [fileEntries, distributionEntries, datasetEntries] =
      await getParentEntriesByResourceURI(resourceURIs, context);

    return statsItems.map((item) => {
      const copyItem = { ...item }; // shallow copy to avoid removing a
      const { uri, type, count } = copyItem;
      let fileType; // used to distinguish document files from distribution files

      // the following structure will also be used as a the CSV header in the export reducer
      // remove this if the Papaparse:transformHeader configuration works
      delete copyItem.count;
      delete copyItem.type;

      // Get the file format for the different file entries (distribution files or document files)
      let format;
      if (distributionEntries.has(uri)) {
        const distEntry = distributionEntries.get(uri);
        format = distEntry
          .getMetadata()
          .findFirstValue(distEntry.getResourceURI(), 'dcterms:format');
      } else {
        const entry = fileEntries.get(uri);
        format = entry?.getEntryInfo().getFormat() || '';
        fileType = 'documentFile';
      }
      format = format && format.trim();

      return {
        ...copyItem,
        type: fileType || type,
        format,
        abbrevFormat: format ? getAbbreviatedMimeType(format) : '-', // some formats have trailing space
        datasetName: datasetEntries.has(uri)
          ? getLabel(datasetEntries.get(uri))
          : null,
        distributionName: distributionEntries.has(uri)
          ? getLabel(distributionEntries.get(uri))
          : null,
        resourceName: fileEntries.has(uri)
          ? getLabel(fileEntries.get(uri))
          : null,
        downloadCount: count,
      };
    });
  } catch (err) {
    // no statistics found
    console.log(err);
    return [];
  }
};
/**
 *
 * @param resourceType
 * @param timeRange
 * @return {Promise<*|[]>}
 */
export const getStatisticsItems = async (context, resourceType, timeRange) => {
  let statsItems = [];
  try {
    statsItems = await statsAPI.getTopStatistics(
      context.getId(),
      resourceType,
      timeRangeUtil.toDateObject(timeRange)
    );
  } catch (err) {
    // no statistics found
    console.log(err);
    return Promise.reject(err);
  }
  return statsItems;
};

/**
 *
 * @param {Entry[]} entries
 * @param {Entry} context
 * @param {string} timeRange
 */
const getStatisticsEntries = (entries, context, timeRange) => {
  const statisticsEntriesPromises = entries.map((entry) => {
    const entryId =
      isAPIDistribution(entry) || isPipelineResultEntry(entry)
        ? getRowstoreAPIUUID(entry) // @todo @valentino check if this works with aliasses
        : entry.getId();

    return statsAPI.getEntryStatistics(
      context.getId(),
      entryId,
      timeRangeUtil.toDateObject(timeRange)
    );
  });

  return Promise.all(statisticsEntriesPromises).catch((error) => {
    console.error(error);
    throw Error('Error while trying to fetch entry statistics');
  });
};

export const getMultiDatasetChartData = async (
  entries,
  context,
  timeRange,
  names = []
) => {
  const labels = entries.map((entry, index) => {
    const label = getLabel(entry);
    if (label) return label;
    if (names.length) return names[index];
    return 'No label';
  });

  const statisticsEntries = await getStatisticsEntries(
    entries,
    context,
    timeRange
  );

  const datasets = statisticsEntries.map(
    ({ count, ...statisticsData } = {}, index) => {
      const data = timeRangeUtil.normalizeChartData(timeRange, statisticsData);
      return {
        data,
        label: labels[index],
      };
    }
  );

  return {
    datasets,
  };
};

/**
 * Checks if chart datasets contains at least one value
 *
 * @param {{}} chartData
 * @returns {boolean|undefined}
 */
export const hasChartData = (chartData) => {
  if (!chartData?.datasets?.length) return;
  return chartData.datasets.some(({ data }) => data.some(({ y }) => y));
};

/**
 *
 * @param items
 * @param timeRange
 * @returns {Promise<{datasets: []}>}
 */
export const getChartData = async (context, items, timeRange) => {
  const resourceURIs = items.map((item) => item.uri);
  const resourceFallbackDatasetNames = items.map((item) => {
    return item.type === RESOURCE_TYPE_API
      ? `${item.datasetName} (API)`
      : item.datasetName;
  });

  /**
   * @type {Entry[]}
   */
  const renderedEntries = await entrystoreUtil.loadEntriesByResourceURIs(
    resourceURIs,
    context,
    true
  );

  return getMultiDatasetChartData(
    renderedEntries,
    context,
    timeRange,
    resourceFallbackDatasetNames
  );
};

/**
 *
 * @param {Object} context
 * @param {string} timeRange
 * @param {string} resourceType
 * @param {Object} searchQuery
 * @param {boolean} checkContinue
 * @returns {Promise<{datasets: []}>}
 */
export const getStatisticsData = async (
  context,
  timeRange,
  resourceType,
  searchQuery,
  checkContinue
) => {
  const statisticsItems = await getStatisticsItems(
    context,
    resourceType,
    timeRange
  );

  if (!statisticsItems.length) return [];

  const resourceURISet = await search(searchQuery, context, checkContinue);
  const filteredItems = statisticsItems.filter((item) =>
    resourceURISet.has(item.uri)
  );
  if (!filteredItems.length) return [];

  const contextStatisticsItems = await getStatisticItemsRelevantEntries(
    context,
    filteredItems
  );

  return contextStatisticsItems;
};

export const getStatisticsGraphData = async (
  statisticsData,
  context,
  timeRange
) => {
  if (!statisticsData?.length) return EMPTY_CHART_DATASET;

  const chartData = await getChartData(context, statisticsData, timeRange);
  return chartData;
};

/**
 * Statistics request for files does not distinguish
 * between distribution files and document files, so we need
 * to filter the chartData for a specific type of file
 * @param {Array} chartData
 * @param {boolean} isDistributionFile
 * @param {boolean} isDocumentFile
 * @returns {array}
 */
export const filterByFileType = (
  chartData,
  isDistributionFile,
  isDocumentFile
) => {
  if (isDocumentFile && !isDistributionFile) {
    return chartData?.filter((item) => item.type !== 'file');
  }

  if (!isDocumentFile && isDistributionFile) {
    return chartData?.filter((item) => item.type !== 'documentFile');
  }

  return chartData;
};
