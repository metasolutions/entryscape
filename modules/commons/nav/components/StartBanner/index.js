import PropTypes from 'prop-types';
import { useCallback, useState, useEffect } from 'react';
import { useUserState } from 'commons/hooks/useUser';
import { Grid, Typography, Button } from '@mui/material';
import { USER_ENTRY_ID_GUEST } from 'commons/util/userIds';
import { getRouteFromView } from 'commons/util/site';
import { i18n } from 'esi18n';
import escoLayoutNLS from 'commons/nls/escoLayout.nls';
import { useTranslation } from 'commons/hooks/useTranslation';
import useExternalContentDialog from 'commons/hooks/useExternalContentDialog';
import 'commons/nav/components/StartBanner/index.scss';

const StartBanner = ({ banner }) => {
  const { header, text, details } = banner;
  const { buttonLabel, path, isLink, header: dialogHeader } = details;
  const { userInfo } = useUserState();
  const isGuest = userInfo.entryId === USER_ENTRY_ID_GUEST;
  const [open, setOpen] = useState(false);
  const [contentURI, setContentURI] = useState('');

  const closeDialog = useCallback(() => {
    setOpen(false);
  }, []);

  useExternalContentDialog({
    open,
    contentPath: path,
    title: dialogHeader[i18n.getLocale()],
    callback: closeDialog,
  });

  useEffect(() => {
    const localePath = `${path}_${i18n.getLocale()}.html`;
    fetch(localePath, {
      headers: {
        Accept: 'text/html',
      },
    })
      .then((response) => {
        if (response.ok) {
          setContentURI(`${window.location.origin}${localePath}`);
        } else {
          setContentURI(`${window.location.origin}${path}.html`);
          throw new Error(`${response.status} ${response.statusText}`);
        }
      })
      .catch((error) => console.log(error));
  }, [path]);

  const t = useTranslation(escoLayoutNLS);

  return (
    <Grid
      container
      direction="column"
      justifyContent="flex-start"
      alignItems="flex-start"
      className="escoStartBanner"
    >
      <Grid className="escoStartBanner__GridItem" item>
        <Typography variant="h1" className="escoStartBanner__header">
          {header[i18n.getLocale()]}
        </Typography>
      </Grid>
      <Grid className="escoStartBanner__GridItem" item>
        <Typography className="escoStartBanner__text">
          {text[i18n.getLocale()]}
        </Typography>
      </Grid>
      <Grid
        container
        direction="row"
        justifyContent="flex-start"
        className="escoStartBanner__GridItem escoStartBanner__GridItem--padding-top"
      >
        {isGuest && (
          <Grid
            classes={{ root: 'escoStartBanner__buttonContainer' }}
            className="escoStartBanner__GridItem--margin-right"
            item
          >
            <Button fullWidth href={getRouteFromView('signin')}>
              {t('signInFromBanner')}
            </Button>
          </Grid>
        )}
        {path && (
          <Grid classes={{ root: 'escoStartBanner__buttonContainer' }} item>
            {isLink ? (
              <Button
                fullWidth
                color="secondary"
                href={contentURI}
                target="_blank"
                rel="noreferrer"
              >
                {buttonLabel[i18n.getLocale()]}
              </Button>
            ) : (
              <Button fullWidth onClick={() => setOpen(true)} color="secondary">
                {buttonLabel[i18n.getLocale()]}
              </Button>
            )}
          </Grid>
        )}
      </Grid>
    </Grid>
  );
};

StartBanner.propTypes = {
  banner: PropTypes.shape({
    header: PropTypes.object,
    text: PropTypes.object,
    details: PropTypes.shape({
      header: PropTypes.object,
      buttonLabel: PropTypes.object,
      path: PropTypes.string,
      isLink: PropTypes.bool,
    }),
  }),
};

export default StartBanner;
