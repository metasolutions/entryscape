import { useEffect } from 'react';
import PropTypes from 'prop-types';
import { Graph } from '@entryscape/rdfjson';
import useAsync from 'commons/hooks/useAsync';
import { getExtendedEntry } from 'models/utils/extension';
import { graphPropType } from 'commons/util/entry';

/**
 *
 * @param {Graph} graph
 * @param {string} resourceURI
 * @param {string} property
 * @returns {boolean}
 */
export const hasValue = (graph, resourceURI, property) => {
  if (!property) return false;
  const predicates = property.split(' ');
  for (const predicate of predicates) {
    const value = graph.findFirstValue(resourceURI, predicate);
    if (value) return true;
  }
  return false;
};

export const useExtends = ({ graph, resourceURI, property }) => {
  const { data: extendedEntry, runAsync, status } = useAsync();

  useEffect(() => {
    if (!graph || !property) return;
    runAsync(getExtendedEntry(graph, resourceURI, property, hasValue));
  }, [runAsync, graph, resourceURI, property]);

  if (status !== 'resolved') return;

  return extendedEntry;
};

export const Extends = ({
  graph,
  resourceURI,
  property,
  Component,
  ...props
}) => {
  const extendedEntry = useExtends({ graph, resourceURI, property });

  return extendedEntry ? (
    <Component
      {...props}
      property={property}
      graph={extendedEntry.getMetadata()}
      resourceURI={extendedEntry.getResourceURI()}
    />
  ) : null;
};

Extends.propTypes = {
  graph: graphPropType,
  resourceURI: PropTypes.string,
  property: PropTypes.string,
  Component: PropTypes.func,
};
