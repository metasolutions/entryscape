import { RDF_TYPE_DATASET, RDF_TYPE_DATASET_SERIES } from 'commons/util/entry';
import {
  ANY,
  ANY_FILTER_ITEM,
  CHECKBOX,
} from 'commons/components/filters/utils/filterDefinitions';

export const DATASET_OR_SERIES_FILTER = {
  id: 'dataset-or-series-filter',
  headerNlsKey: 'datasetOrSeriesFilterHeader',
  applyFilterParams: (query, filter) => {
    const itemValue = filter.getSelected();
    if (itemValue === 'dataset') {
      return query.rdfType(RDF_TYPE_DATASET);
    }
    if (itemValue === 'datasetSeries') {
      return query.rdfType(RDF_TYPE_DATASET_SERIES);
    }
    return query;
  },
  items: [
    {
      labelNlsKey: 'anyFilterLabel',
      value: ANY,
    },
    {
      labelNlsKey: 'datasetFilterLabel',
      value: 'dataset',
    },
    {
      labelNlsKey: 'seriesFilterLabel',
      value: 'datasetSeries',
    },
  ],
};

export const IN_SERIES_CHECKBOX_FILTER = {
  id: 'in-series-filter',
  type: CHECKBOX,
  headerNlsKey: 'inSeriesFilterHeader',
  applyFilterParams: (query, filter) => {
    if (filter.getSelected() === ANY) return query;

    // clear the "dataset series or not in-series datasets" part
    query._or.forEach((orObject) =>
      orObject.rdfType === RDF_TYPE_DATASET_SERIES
        ? query._or.delete(orObject)
        : orObject
    );

    // only set an rdf type if none is available (when `Type`: `Any` is selected)
    if (!query.params.has('rdfType')) {
      query.rdfType([RDF_TYPE_DATASET, RDF_TYPE_DATASET_SERIES]);
    }
  },
  items: [
    ANY_FILTER_ITEM,
    {
      value: 'inSeries',
    },
  ],
};
