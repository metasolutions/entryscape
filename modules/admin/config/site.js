import UsersView from 'admin/users/UsersView';
import GroupsView from 'admin/groups/GroupsView';
import ProjectsView from 'admin/projects/ProjectsView';
import EntityTypeView from 'admin/configs/entitytypes/EntityTypeView';
import ProjectTypeView from 'admin/configs/projecttypes/ProjectTypeView';
import UserOverview from 'admin/users/UserOverview';
import GroupOverview from 'admin/groups/GroupOverview';
import ProjectOverview from 'admin/projects/ProjectOverview';
import ProjectTypeOverview from 'admin/configs/projecttypes/ProjectTypeOverview';
import { getSecondaryNavViewsForAdmin, checkPermission } from 'admin/utils';

export default {
  modules: [
    {
      name: 'admin',
      productName: { en: 'Admin', sv: 'Admin', de: 'Admin' },
      faClass: 'cogs',
      restrictTo: 'admin',
      startView: 'admin__users',
      sidebar: true,
      getLayoutProps: getSecondaryNavViewsForAdmin,
      checkPermission,
    },
  ],
  views: [
    {
      name: 'admin__users',
      class: UsersView,
      faClass: 'user',
      title: { en: 'Users', sv: 'Användare', de: 'Benutzer' },
      route: '/admin/users',
      parent: 'admin',
      module: 'admin',
    },
    {
      name: 'admin__users__user',
      class: UserOverview,
      title: {
        en: 'User',
        sv: 'Användare',
        de: 'Benutzer',
      },
      route: '/admin/:contextId/users/:entryId/overview',
      parent: 'admin_overview',
      matchRoute: '/admin/users',
      module: 'admin',
    },
    {
      name: 'admin__groups',
      class: GroupsView,
      faClass: 'users',
      title: { en: 'Groups', sv: 'Grupper', de: 'Gruppen' },
      route: '/admin/groups',
      parent: 'admin',
      module: 'admin',
      restrictTo: 'advancedAdmin',
    },
    {
      name: 'admin__groups__group',
      class: GroupOverview,
      title: {
        en: 'Group',
        sv: 'Grupp',
        de: 'Gruppe',
      },
      route: '/admin/:contextId/groups/:entryId/overview',
      parent: 'admin_overview',
      matchRoute: '/admin/groups',
      module: 'admin',
    },
    {
      name: 'admin__projects',
      class: ProjectsView,
      faClass: 'building',
      title: { en: 'Projects', sv: 'Projekt', de: 'Projekte' },
      route: '/admin/projects',
      parent: 'admin',
      module: 'admin',
      restrictTo: 'advancedAdmin',
    },
    {
      name: 'admin__projects__project',
      class: ProjectOverview,
      title: {
        en: 'Project',
        sv: 'Projekt',
        de: 'Projekt',
      },
      route: '/admin/:contextId/projects/:entryId/overview',
      parent: 'admin_overview',
      matchRoute: '/admin/projects',
      module: 'admin',
    },
    {
      name: 'admin__entitytypes',
      class: EntityTypeView,
      faClass: 'building',
      title: { en: 'Entity types', sv: 'Entitetstyper', de: 'Objekttypen' },
      route: '/admin/entitytypes',
      parent: 'admin',
      module: 'admin',
      restrictTo: 'advancedAdmin',
    },
    {
      name: 'admin__projecttypes',
      class: ProjectTypeView,
      faClass: 'building',
      title: { en: 'Project types', sv: 'Projekttyper', de: 'Projekttypen' },
      route: '/admin/projecttypes',
      parent: 'admin',
      module: 'admin',
      restrictTo: 'advancedAdmin',
      navbar: false,
    },
    {
      name: 'admin__projecttypes__projecttype',
      class: ProjectTypeOverview,
      title: {
        en: 'Project type',
        sv: 'Projekttyp',
        de: 'Projekttyp',
      },
      route: '/admin/:contextId/projecttypes/:entryId/overview',
      matchRoute: '/admin/projecttypes',
      parent: 'admin_overview',
      module: 'admin',
    },
  ],
};
