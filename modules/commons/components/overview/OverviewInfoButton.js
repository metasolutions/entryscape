import InfoIcon from '@mui/icons-material/Info';
import { IconButton } from '@mui/material';

const OverviewInfoButton = ({ ...rest }) => (
  <IconButton {...rest}>
    <InfoIcon />
  </IconButton>
);

export default OverviewInfoButton;
