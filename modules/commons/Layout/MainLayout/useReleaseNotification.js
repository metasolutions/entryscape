import { useEffect, useState } from 'react';
import config from 'config';
import useAsync, { IDLE } from 'commons/hooks/useAsync';

const useReleaseNotification = (userEntry) => {
  const appVersion = config.get('entryscape.static.version');
  const { data: user, runAsync, status } = useAsync();
  const [hasUnreadReleaseInfo, setHasUnreadReleaseInfo] = useState(false);

  useEffect(() => {
    if (status !== IDLE) return;
    if (user) return;

    runAsync(
      userEntry.getResource().then((userResource) => {
        const userReleaseInfoVersion =
          userResource.getCustomProperties().releaseinfoversion;
        setHasUnreadReleaseInfo(appVersion !== userReleaseInfoVersion);
        return userResource;
      })
    );
  }, [appVersion, runAsync, status, user, userEntry]);

  const updateReleaseInfoVersion = () => {
    user.setCustomProperties({
      ...user.getCustomProperties(),
      releaseinfoversion: appVersion,
    });
    setHasUnreadReleaseInfo(false);
  };

  return {
    updateReleaseInfoVersion,
    hasUnreadReleaseInfo,
  };
};

export default useReleaseNotification;
