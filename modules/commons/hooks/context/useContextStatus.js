import { useState, useEffect } from 'react';
import { setContextPublic, loadContextEntry } from 'commons/util/context';

/**
 * Manage the status (public/not public) of a context
 * @param {Entry} entry
 * @returns {[boolean, function]}
 */
const useContextStatus = (entry) => {
  const [isPublic, setPublic] = useState(false);
  const [canAdminister, setCanAdminister] = useState(false);

  useEffect(() => {
    loadContextEntry(entry).then((contextEntry) => {
      setPublic(contextEntry.isPublic());
      setCanAdminister(contextEntry.canAdministerEntry());
    });
  }, [entry]);

  const togglePublic = () => {
    const isChecked = !isPublic;
    loadContextEntry(entry)
      .then((contextEntry) => setContextPublic(contextEntry, isChecked))
      .then(() => setPublic(isChecked));
  };

  return [isPublic, togglePublic, canAdminister];
};

export default useContextStatus;
