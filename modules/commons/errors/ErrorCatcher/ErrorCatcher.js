import PropTypes from 'prop-types';
import ErrorBoundary from '../ErrorBoundary';
import useErrorHandler from '../hooks/useErrorHandler';
import SnackbarFallbackComponent from '../SnackbarFallbackComponent';

const ErrorThrower = ({ error }) => {
  useErrorHandler(error);

  return null;
};

ErrorThrower.propTypes = {
  error: PropTypes.instanceOf(Error),
};

// ErrorCatcher is used to forward an error to a child component that will
// handle the error. This can for example be useful to prevent a dialog from
// breaking and instead show an error message in a snackbar.
export const ErrorCatcher = ({
  error,
  FallbackComponent = SnackbarFallbackComponent,
}) => {
  if (!error) return null;

  return (
    <ErrorBoundary FallbackComponent={FallbackComponent}>
      <ErrorThrower error={error} />
    </ErrorBoundary>
  );
};

ErrorCatcher.propTypes = {
  error: PropTypes.instanceOf(Error),
  FallbackComponent: PropTypes.func,
};
