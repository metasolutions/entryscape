const options = {
  maintainAspectRatio: false,
  tooltips: {
    callbacks: {
      title(item) {
        return moment(item[0].label).format('MMM Do, YYYY');
      },
    },
  },
  scales: {
    xAxes: [
      {
        type: 'time',
        time: {
          unit: 'month',
        },
        offset: true,
        ticks: {
          autoSkip: true,
        },
      },
    ],
    yAxes: [
      {
        ticks: {
          min: 0,
          precision: 0,
        },
      },
    ],
  },
};

export default options;

/**
 * Get a sensible time unit format to show in chart's tooltips
 * @param timeUnit
 * @return {string}
 */
export const getTimeUnitParsingFormat = (timeUnit) => {
  switch (timeUnit) {
    case 'hour':
      return 'MMMM Do YYYY, h A';
    case 'month':
      return 'MMMM YYYY';
    case 'day':
    default:
      return 'MMM Do, YYYY';
  }
};
