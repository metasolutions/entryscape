import { useEffect } from 'react';
import useAsync from 'commons/hooks/useAsync';
import useErrorHandler from 'commons/errors/hooks/useErrorHandler';

const loadMuiLab = () => import(/* webpackChunkName: "mui-lab" */ '@mui/lab');

const useTimelineComponents = () => {
  const { data: muiLab, status, error, runAsync } = useAsync({ data: {} });
  useErrorHandler(error);

  useEffect(() => {
    if (status !== 'idle') return;
    runAsync(loadMuiLab());
  }, [status, runAsync]);

  return { ...muiLab, status, error };
};

export default useTimelineComponents;
