import {
  WMS_FORMAT,
  WMS_SRVC_FORMAT,
  isWMSDistribution,
} from './distributions';

describe('isWMSDistribution', () => {
  test('Correctly identifies a WMS distribution by WMS_FORMAT', () => {
    const distributionEntry = {
      projection: jest.fn(() => ({ format: WMS_FORMAT })),
    };
    expect(isWMSDistribution(distributionEntry)).toBe(true);
  });

  test('Correctly identifies a WMS distribution by WMS_SRVC_FORMAT', () => {
    const distributionEntry = {
      projection: jest.fn(() => ({ format: WMS_SRVC_FORMAT })),
    };
    expect(isWMSDistribution(distributionEntry)).toBe(true);
  });

  test('Correctly identifies a WMS distribution by accessURL', () => {
    const distributionEntry = {
      projection: jest.fn(() => ({
        format: 'wfs',
        accessURL: 'https://wms.example.com?service=wms',
      })),
    };
    expect(isWMSDistribution(distributionEntry)).toBe(true);
  });

  test('Correctly identifies a WMS distribution by uppercase service in accessURL', () => {
    const distributionEntry = {
      projection: jest.fn(() => ({
        format: 'wfs',
        accessURL: 'https://wms.example.com?SERVICE=WMS',
      })),
    };
    expect(isWMSDistribution(distributionEntry)).toBe(true);
  });

  test('Correctly identifies a non WMS distribution', () => {
    const distributionEntry = {
      projection: jest.fn(() => ({ format: 'application/vnd.ogc.wfs_xml' })),
    };
    expect(isWMSDistribution(distributionEntry)).toBe(false);
  });

  test('Correctly identifies a non WMS distribution with non WMS service in accessURL', () => {
    const distributionEntry = {
      projection: jest.fn(() => ({
        accessURL: 'https://wms.example.com?service=wfs',
      })),
    };
    expect(isWMSDistribution(distributionEntry)).toBe(false);
  });

  test('Correctly identifies a non WMS distribution with undefined accessURL', () => {
    const distributionEntry = {
      projection: jest.fn(() => ({ format: undefined })),
    };
    expect(isWMSDistribution(distributionEntry)).toBe(false);
  });
});
