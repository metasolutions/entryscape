import { ACTION_EDIT } from 'commons/components/overview/actions';
import { isExtended } from 'models/utils/extension';
import { withEditorProvider } from 'commons/components/forms/editors';
import EditFieldDialog from 'models/fields/dialogs/EditFieldDialog';
import { EditNote } from '@mui/icons-material';
import { withOverviewRefresh } from 'commons/components/overview';
import ChangeTypeDialog from 'models/components/ChangeTypeDialog/ChangeTypeDialog';
import EditOptionsDialog from '../dialogs/OptionsDialog';
import EditAttributesDialog from '../dialogs/EditAttributesDialog';
import { isSelectField } from '../utils/fields';
import editSections from '../utils/fieldEditors';
import { RDF_TYPE_TO_RFS_FIELD, RFS_FIELDS } from '../utils/rfsFields';

export const sidebarActions = [
  {
    ...ACTION_EDIT,
    Dialog: withOverviewRefresh(
      withEditorProvider(EditFieldDialog, editSections),
      'onEntryEdit'
    ),
  },
  {
    id: 'edit-attributes',
    Dialog: withOverviewRefresh(
      withEditorProvider(EditAttributesDialog),
      'onSave'
    ),
    labelNlsKey: 'editAttributesLabel',
    getProps: () => ({
      startIcon: <EditNote />,
    }),
  },
  {
    id: 'edit-options',
    Dialog: withOverviewRefresh(
      withEditorProvider(EditOptionsDialog),
      'onSave'
    ),
    isVisible: ({ entry }) => isSelectField(entry),
    labelNlsKey: 'editOptions',
    getProps: ({ entry }) => ({
      ...(isSelectField(entry) &&
      isExtended(entry.getMetadata(), entry.getResourceURI())
        ? {
            disabled: true,
            tooltipNlsKey: 'optionsDisabledTooltip',
          }
        : {}),
    }),
  },
  {
    id: 'change-type',
    Dialog: withOverviewRefresh(ChangeTypeDialog, 'onTypeChange'),
    labelNlsKey: 'changeTypeLabel',
    getProps: () => ({
      action: {
        rfsItems: RFS_FIELDS,
        rdfTypeToRfsItem: RDF_TYPE_TO_RFS_FIELD,
      },
    }),
  },
];
