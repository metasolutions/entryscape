import PropTypes from 'prop-types';
import React, { useState, useEffect } from 'react';
import ListActionDialog from 'commons/components/common/dialogs/ListActionDialog';
import { useTranslation } from 'commons/hooks/useTranslation';
import { useListModel, CREATE } from 'commons/components/ListView';
import esadUserNLS from 'admin/nls/esadUser.nls';
import { namespaces as ns } from '@entryscape/rdfjson';
import { entrystore } from 'commons/store';
import ContentWrapper from 'commons/components/common/ContentWrapper';
import { TextField, FormControlLabel, Checkbox } from '@mui/material';
import LoadingButton from 'commons/components/LoadingButton';
import { removeWhitespace } from 'commons/util/util';
import config from 'config';
import { useSnackbar } from 'commons/hooks/useSnackbar';
import { ErrorCatcher } from 'commons/errors/ErrorCatcher';
import { ErrorWithMessage } from 'commons/errors/utils/errors';
import useAsync, { PENDING, RESOLVED } from 'commons/hooks/useAsync';
import { useConfirmCloseAction } from 'commons/components/EntryListView/dialogs/create/handlers';

const validateUsername = async (username) => {
  if (!username.isPristine && !username.value) return 'usernameRequired';
  if (username.value.split(' ').length > 1) return 'usernameHasSpaces';

  return entrystore
    .getREST()
    .get(
      `${entrystore.getBaseURI()}_principals?entryname=${username.value.toLowerCase()}`
    )
    .then((data) => {
      if (data.length > 0) return 'usernameTaken';
      return null;
    });
};

const validateFirstname = (firstname) => {
  return !firstname.isPristine && !firstname.value ? 'firstnameRequired' : null;
};

const validateLastname = (lastname) => {
  return !lastname.isPristine && !lastname.value ? 'lastnameRequired' : null;
};

const CreateUserDialog = ({ closeDialog }) => {
  const [, dispatch] = useListModel();
  const { runAsync, error: createError, status } = useAsync();
  const translate = useTranslation(esadUserNLS);
  const [username, setUsername] = useState({
    value: '',
    error: null,
    isPristine: true,
  });
  const [firstname, setFirstname] = useState({
    value: '',
    error: null,
    isPristine: true,
  });
  const [lastname, setLastname] = useState({
    value: '',
    error: null,
    isPristine: true,
  });
  const confirmClose = useConfirmCloseAction(closeDialog);
  const close = () =>
    confirmClose(
      !username.isPristine || !firstname.isPristine || !lastname.isPristine
    );
  const [context, setContext] = useState(false);
  const [usernameTimer, setUsernameTimer] = useState(null);
  const [changed, setChanged] = useState(false);
  const includePersonalProject = config.get('admin.includePersonalProject');
  const [addSnackbar] = useSnackbar();

  const update = (state, set) => (evt) => {
    set({
      ...state,
      isPristine: false,
      value: removeWhitespace(evt.target.value),
    });
  };

  const usernameChanged = () => setChanged(!changed);
  const handleUsernameChange = (evt) => {
    setUsername({
      ...username,
      isPristine: false,
      value: evt.target.value,
    });

    clearTimeout(usernameTimer);
    setUsernameTimer(setTimeout(usernameChanged, 500));
  };

  useEffect(() => {
    if (!firstname.isPristine) {
      const firstnameError = validateFirstname(firstname);
      setFirstname((firstnameState) => ({
        ...firstnameState,
        error: firstnameError ? translate(firstnameError) : null,
      }));
    }

    if (!lastname.isPristine) {
      const lastnameError = validateLastname(lastname);
      setLastname((s) => ({
        ...s,
        error: lastnameError ? translate(lastnameError) : null,
      }));
    }
  }, [firstname.value, lastname.value]);

  useEffect(() => {
    if (!username.isPristine) {
      validateUsername(username).then((e) =>
        setUsername((usernameState) => ({
          ...usernameState,
          error: e ? translate(e) : null,
        }))
      );
    }
  }, [changed]);

  const hasNoErrors =
    username.error === null &&
    firstname.error === null &&
    lastname.error === null;
  const isNotPristine =
    !username.isPristine && !firstname.isPristine && !lastname.isPristine;
  const canSubmit =
    hasNoErrors && isNotPristine && firstname.value && lastname.value;

  const dispatchEntryCreated = () => {
    dispatch({ type: CREATE });
  };

  const handleSubmit = () => {
    if (!canSubmit) return;

    let userEntry;
    let contextEntry;

    const pue = entrystore.newUser(username.value);
    const md = pue.getMetadata();
    const resURI = pue.getResourceURI();
    const usersURI = entrystore.getResourceURI('_principals', '_users');
    md.add(pue.getResourceURI(), ns.expand('foaf:givenName'), {
      type: 'literal',
      value: firstname.value,
    });
    md.add(pue.getResourceURI(), ns.expand('foaf:familyName'), {
      type: 'literal',
      value: lastname.value,
    });
    md.add(pue.getResourceURI(), ns.expand('foaf:name'), {
      type: 'literal',
      value: `${firstname.value} ${lastname.value}`,
    });
    pue.getEntryInfo().setACL({
      mread: [usersURI],
      mwrite: [resURI],
      rwrite: [resURI],
    });

    return pue
      .commit()
      .then((ue) => {
        userEntry = ue;
        if (context) {
          const cpe = entrystore.newContext();
          cpe.getEntryInfo().setACL({ admin: [userEntry.getResourceURI()] });
          return cpe.commit();
        }
        return userEntry;
      })
      .then((ce) => {
        contextEntry = ce;
        if (context) {
          return userEntry
            .getResource(true)
            .setHomeContext(contextEntry.getId())
            .then(() => {
              contextEntry.setRefreshNeeded();
              return contextEntry.refresh();
            });
        }
        return ce;
      })
      .then(() => {
        closeDialog();
        dispatchEntryCreated();
        addSnackbar({ message: translate('createUserSuccess') });
      })
      .catch((error) => {
        if (userEntry) userEntry.del();
        if (contextEntry) contextEntry.del();
        throw new ErrorWithMessage(translate('createUserFail'), error);
      });
  };

  const actions = (
    <LoadingButton
      success={status === RESOLVED}
      loading={status === PENDING}
      onClick={() => runAsync(handleSubmit())}
      disabled={!canSubmit}
    >
      {translate('createUserButton')}
    </LoadingButton>
  );
  return (
    <>
      <ListActionDialog
        id="create-user"
        title={translate('createUserHeader')}
        actions={actions}
        closeDialog={close}
        maxWidth="md"
      >
        <ContentWrapper>
          <form
            noValidate
            onSubmit={() => canSubmit && runAsync(handleSubmit())}
          >
            <TextField
              placeholder={translate('createUsernamePlaceholder')}
              error={username.error && !username.isPristine}
              helperText={
                username.error
                  ? username.error
                  : translate('createUsernameHelptext')
              }
              autoFocus
              id="username"
              label={translate('createUsername')}
              type="email"
              value={username.value}
              onChange={handleUsernameChange}
              required
            />
            <TextField
              placeholder={translate('createFirstnamePlaceholder')}
              error={firstname.error && !firstname.isPristine}
              helperText={firstname.error ? firstname.error : ''}
              id="firstname"
              label={translate('createFirstname')}
              value={firstname.value}
              onChange={update(firstname, setFirstname)}
              required
            />
            <TextField
              placeholder={translate('createLastnamePlaceholder')}
              error={lastname.error && !lastname.isPristine}
              helperText={lastname.error ? lastname.error : ''}
              id="lastname"
              label={translate('createLastname')}
              value={lastname.value}
              onChange={update(lastname, setLastname)}
              required
            />
            {includePersonalProject ? (
              <FormControlLabel
                control={<Checkbox color="primary" />}
                label={translate('createContextWithUser')}
                labelPlacement="start"
                onChange={() => setContext(!context)}
                checked={context}
              />
            ) : null}
          </form>
        </ContentWrapper>
      </ListActionDialog>
      <ErrorCatcher error={createError} />
    </>
  );
};

CreateUserDialog.propTypes = {
  closeDialog: PropTypes.func.isRequired,
};

export default CreateUserDialog;
