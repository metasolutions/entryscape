import PropTypes from 'prop-types';
import {
  Button,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  Divider,
  Grid,
} from '@mui/material';
import esreHarvestNLS from 'harvest/nls/esreHarvest.nls';
import { useTranslation } from 'commons/hooks/useTranslation';

const EditNotificationsDialog = ({ children, open, onClose, dialogAction }) => {
  const t = useTranslation(esreHarvestNLS);
  return (
    <Dialog
      fullWidth
      maxWidth="md"
      aria-labelledby="configure-notifications"
      open={open}
      onClose={onClose}
    >
      <DialogTitle id="configure-notifications">
        {t('notificationsDialogTitle')}
      </DialogTitle>
      <Divider />
      <DialogContent>
        <Grid container justifyContent="center" alignItems="center">
          <Grid item xs={8}>
            {children}
          </Grid>
        </Grid>
      </DialogContent>
      <Divider />
      <DialogActions>
        <Button autoFocus variant="text" onClick={onClose}>
          {t('cancel')}
        </Button>
        {dialogAction}
      </DialogActions>
    </Dialog>
  );
};

EditNotificationsDialog.propTypes = {
  children: PropTypes.node.isRequired,
  open: PropTypes.bool,
  onClose: PropTypes.func,
  dialogAction: PropTypes.node,
};

export default EditNotificationsDialog;
