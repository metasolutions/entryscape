import esadGroupNLS from 'admin/nls/esadGroup.nls';
import esadUserNLS from 'admin/nls/esadUser.nls';
import escoListNLS from 'commons/nls/escoList.nls';
import LinkedDataBrowserDialog from 'commons/components/LinkedDataBrowserDialog';
import { canReadMetadata, canWriteResource } from 'commons/util/entry';
import AddUserToGroup from './AddUserToGroupDialog';
import RemoveUserDialog from './RemoveUserDialog';

export const nlsBundles = [escoListNLS, esadGroupNLS, esadUserNLS];

export const listActions = [
  {
    id: 'add-user',
    Dialog: AddUserToGroup,
    labelNlsKey: 'addUserToGroupNLS',
  },
];
export const rowActions = [
  {
    id: 'remove',
    Dialog: RemoveUserDialog,
    isVisible: ({ entry }) => canWriteResource(entry),
    labelNlsKey: 'removeEntryLabel',
  },
];

export const presenterAction = {
  id: 'info',
  Dialog: LinkedDataBrowserDialog,
  isVisible: ({ entry }) => canReadMetadata(entry),
  labelNlsKey: 'infoEntry',
  formTemplateId: 'esc:Group',
};
