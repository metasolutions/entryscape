import PropTypes from 'prop-types';
import { Grid, Typography } from '@mui/material';
import './index.scss';

const DescriptionItem = ({ label, values }) => {
  return (
    <Grid container direction="column">
      <Grid item>
        <Typography
          variant="h6"
          component="h2"
          className="escaDescriptionItem__header"
        >
          {label}
        </Typography>
      </Grid>
      {values.map((value) => (
        <Grid key={value} item>
          <Typography>{value}</Typography>
        </Grid>
      ))}
    </Grid>
  );
};

DescriptionItem.propTypes = {
  label: PropTypes.string,
  values: PropTypes.arrayOf(PropTypes.string),
};

export default DescriptionItem;
