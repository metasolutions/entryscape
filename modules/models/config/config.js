export const MODELS_LISTVIEW = 'models__list';
export const NAMESPACES_LISTVIEW = 'models__namespaces';
export const CLASSES_LISTVIEW = 'models__classes';
export const PROPERTIES_LISTVIEW = 'models__properties';
export const FORMS_LISTVIEW = 'models__forms';
export const FIELDS_LISTVIEW = 'models__fields';
export const DIAGRAMS_LISTVIEW = 'models__diagrams';
export const RESOURCES_LISTVIEW = 'models__resources';
export const SPECIFICATIONS_LISTVIEW = 'models__specifications';

export const NAMESPACE_OVERVIEW_NAME = 'models__namespaces__namespace';
export const CLASS_OVERVIEW_NAME = 'models__classes__class';
export const PROPERTY_OVERVIEW_NAME = 'models__properties__property';
export const FORM_OVERVIEW_NAME = 'models__forms__form';
export const FIELD_OVERVIEW_NAME = 'models__fields__field';
export const RESOURCE_OVERVIEW_NAME = 'models__resources__resource';
export const SPECIFICATION_OVERVIEW_NAME = 'models__specifications__overview';

export default {
  models: {
    /**
     * Determines whether or which filters are visible in models. Using a boolean shows/hides
     * all filters, while an array containing the names of the filters to exclude can
     * also be provided.
     *
     * @type {boolean|string[]}
     */
    excludeFilters: false,

    /**
     * @type {object[]}
     */
    alternativeTexts: [
      { label: { en: 'Definition', sv: 'Definition' }, value: 'definition' },
      {
        label: { en: 'Usage note', sv: 'Användingsanmärkning' },
        value: 'usageNote',
      },
    ],
  },

  entitytypes: [
    {
      name: 'models',
      label: {
        en: 'Model',
        sv: 'Modell',
        de: 'Modell',
      },
      rdfType: ['http://entryscape.com/terms/FormsContext'],
      template: 'esc:Context',
      mainModule: 'models',
      module: 'models',
      overview: 'models__overview',
    },
    {
      name: 'namespaces',
      label: { en: 'Namespace', sv: 'Namnrymd', de: 'Namensraum' },
      rdfType: 'http://www.w3.org/ns/shacl#PrefixDeclaration',
      mainModule: 'models',
      module: 'models',
      listview: NAMESPACES_LISTVIEW,
      overview: NAMESPACE_OVERVIEW_NAME,
    },
    {
      name: 'fields',
      label: { en: 'Field', sv: 'Fält', de: 'Feld' },
      rdfType: 'https://rdforms.org/terms/Field',
      mainModule: 'models',
      module: 'models',
      listview: FIELDS_LISTVIEW,
      overview: FIELD_OVERVIEW_NAME,
    },
    {
      name: 'forms',
      label: { en: 'Form', sv: 'Formulär', de: 'Formular' },
      rdfType: 'https://rdforms.org/terms/Form',
      mainModule: 'models',
      module: 'models',
      listview: FORMS_LISTVIEW,
      overview: FORM_OVERVIEW_NAME,
    },
    {
      name: 'specifications',
      label: { en: 'Specification', sv: 'Specifikation', de: 'Spezifikation' },
      rdfType: ['prof:Profile', 'dcterms:Standard'],
      includeLink: true,
      template: 'prof:Profile',
      templateLevel: 'recommended',
      uriPattern:
        '${domain}/resource/specification/${contextName}/${entryName}',
      mainModule: 'models',
      module: 'models',
      listview: SPECIFICATIONS_LISTVIEW,
      overview: SPECIFICATION_OVERVIEW_NAME,
      relations: [
        {
          entityType: 'resources',
          type: 'aggregation',
          property: 'prof:hasResource',
        },
      ],
    },
    {
      name: 'classes',
      label: { en: 'Class', sv: 'Klass', de: 'Klasse' },
      rdfType: 'rdfs:Class',
      mainModule: 'models',
      module: 'models',
      template: 'rdfs:Class',
      includeInternal: false,
      includeFile: false,
      includeLink: true,
      inlineCreation: false,
      publishControl: true,
      templateLevel: 'recommended',
      allContexts: true,
      listview: CLASSES_LISTVIEW,
      overview: CLASS_OVERVIEW_NAME,
    },
    {
      name: 'properties',
      label: { en: 'Property', sv: 'Egenskap', de: 'Eigenschaft' },
      rdfType: 'rdf:Property',
      mainModule: 'models',
      module: 'models',
      template: 'rdf:Property',
      includeInternal: false,
      includeFile: false,
      includeLink: true,
      inlineCreation: false,
      publishControl: true,
      templateLevel: 'recommended',
      allContexts: true,
      listview: PROPERTIES_LISTVIEW,
      overview: PROPERTY_OVERVIEW_NAME,
    },
    {
      name: 'diagrams',
      label: { en: 'Diagram', sv: 'Diagram', de: 'Diagramm' },
      rdfType: 'esterms:Diagram',
      mainModule: 'models',
      module: 'models',
      template: 'esc:Diagram',
      includeInternal: true,
      includeFile: false,
      includeLink: false,
      inlineCreation: false,
      publishControl: false,
      templateLevel: 'recommended',
      allContexts: true,
      listview: DIAGRAMS_LISTVIEW,
      overview: DIAGRAMS_LISTVIEW,
    },
    {
      name: 'rdformsBundleResources',
      label: {
        en: 'RDForms Bundle',
        sv: 'RDForms Bundle',
        de: 'RDForms Bundle',
      },
      rdfType: ['rfs:Bundle'],
      template: 'esc:RdformsBundleResource',
      includeFile: false,
      includeLink: false,
      inlineCreation: true,
      createDialog: true,
      mainModule: 'models',
      module: 'models',
      templateLevel: 'recommended',
      overview: RESOURCE_OVERVIEW_NAME,
    },
    {
      name: 'diagramResources',
      label: {
        en: 'Diagram resource',
        sv: 'Diagramresurs',
        de: 'Diagrammressource',
      },
      rdfType: ['http://purl.org/dc/dcmitype/Image'],
      template: 'esc:RdformsBundleResource',
      includeFile: false,
      includeLink: false,
      inlineCreation: true,
      createDialog: true,
      mainModule: 'models',
      module: 'models',
      templateLevel: 'recommended',
      contentviewers: ['imageview'],
      overview: RESOURCE_OVERVIEW_NAME,
    },
    {
      name: 'resources',
      label: {
        en: 'Resource',
        sv: 'Resurs',
        de: 'Ressource',
      },
      rdfType: 'prof:ResourceDescriptor',
      template: 'prof:ResourceDescriptor',
      includeFile: true,
      includeLink: true,
      includeInternal: false,
      inlineCreation: true,
      createDialog: true,
      mainModule: 'models',
      module: 'models',
      templateLevel: 'recommended',
      listview: RESOURCES_LISTVIEW,
      overview: RESOURCE_OVERVIEW_NAME,
    },
    {
      name: 'modelsPublisher',
      label: {
        en: 'Publisher',
        de: 'Herausgeber',
        sv: 'Utgivare',
      },
      rdfType: [
        'http://xmlns.com/foaf/0.1/Agent',
        'http://xmlns.com/foaf/0.1/Person',
        'http://xmlns.com/foaf/0.1/Organization',
      ],
      mainModule: 'models',
      template: 'dcat:foaf:Agent',
      includeLink: true,
      includeInternal: false,
      inlineCreation: true,
      module: 'models',
      templateLevel: 'recommended',
      restrictTo: 'admin',
      uriPattern: '${domain}/resource/organisation/${entryName}',
    },
  ],
};
