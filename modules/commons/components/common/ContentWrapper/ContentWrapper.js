import PropTypes from 'prop-types';
import { Grid } from '@mui/material';

const ContentWrapper = ({ children, ...contentProps }) => {
  const {
    justifyContent = 'center',
    alignItems = 'center',
    xs = 12,
    md = 10,
  } = contentProps;

  return (
    <Grid container justifyContent={justifyContent} alignItems={alignItems}>
      <Grid item xs={xs} md={md}>
        {children}
      </Grid>
    </Grid>
  );
};

ContentWrapper.propTypes = {
  children: PropTypes.node,
  contentProps: PropTypes.shape({
    justifyContent: PropTypes.string,
    alignItems: PropTypes.string,
    xs: PropTypes.number,
    md: PropTypes.number,
  }),
};

export default ContentWrapper;
