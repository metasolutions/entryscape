import React, { useCallback } from 'react';
import PropTypes from 'prop-types';
import {
  withListModelProvider,
  ListItemActionIconButton,
  ListItemButtonAction,
} from 'commons/components/ListView';
import {
  EntryListView,
  useSolrQuery,
  MODIFIED_COLUMN,
  TITLE_COLUMN,
  INFO_COLUMN,
} from 'commons/components/EntryListView';
// eslint-disable-next-line max-len
import { ACTION_INFO_ID } from 'commons/actions/actionIds';
import { useTranslation } from 'commons/hooks/useTranslation';
import escoEntryChooserNLS from 'commons/nls/escoEntryChooser.nls';
import escoListNLS from 'commons/nls/escoList.nls';
import CreateDialog from 'commons/components/EntityOverview/dialogs/CreateEntityDialog';
import { getRdformsContextAndName } from 'commons/hooks/useRdformsContext';
import { getESContextAndName } from 'commons/hooks/useESContext';
import Lookup from 'commons/types/Lookup';
import { canReadMetadata } from 'commons/util/entry';
import {
  applyQueryParams as defaultApplyQueryParams,
  applyEntityTypeParams,
} from 'commons/util/solr';
import eswoBenchNLS from 'workbench/nls/eswoBench.nls';
// eslint-disable-next-line max-len
import LinkedDataBrowserDialog from 'commons/components/LinkedDataBrowserDialog';
import useSearchFilters from 'commons/components/filters/hooks/useSearchFilters';
import { CONTEXT_FILTER } from 'commons/components/filters/utils/filterDefinitions';
import { getFullLengthLabel } from 'commons/util/rdfUtils';
import { getContextFilterItems } from '../util';
import { getQueryFromItem } from '../queryFromItem';
import getChoiceFromEntry from '../getChoiceFromEntry';
import EntrySelectButton from '../EntrySelectButton';
import './EntrySelectList.scss';

const ROWS_PER_PAGE = 5;

const getTitleProps = ({ entry }) => {
  const label = getFullLengthLabel(entry);
  const contextEntry = entry.getContext().getEntry(true);
  const contextLabel = contextEntry ? getFullLengthLabel(contextEntry) : null;
  return {
    primary: label,
    ...(contextLabel && label !== contextLabel
      ? { secondary: contextLabel }
      : {}),
  };
};

/**
 * Gets the type from constraints.
 * Constraints are used to match entities to templates when the type is not unique enough.
 *
 * @param {object} constraints
 * @returns {string}
 */
const rdfTypeFromConstraints = (constraints) => {
  const rdfTypes =
    constraints['http://www.w3.org/1999/02/22-rdf-syntax-ns#type'];
  if (!rdfTypes) return '';
  if (Array.isArray(rdfTypes)) return rdfTypes[0];
  return rdfTypes;
};

const EntrySelectList = ({ binding, onClose, onSelect }) => {
  const item = binding.getItem();
  const constraints = item.getConstraints();
  const entityType = Lookup.getEntityTypeByConstraints(constraints)?.get();
  const translate = useTranslation(escoEntryChooserNLS);

  const { context: rdformsContext, contextName: rdformsContextName } =
    getRdformsContextAndName();
  const { context: esContext, contextName: esContextName } =
    getESContextAndName();
  const context = rdformsContext || esContext;
  const contextName = rdformsContextName || esContextName;

  const showContextFilter = entityType && entityType.allContexts === true;
  const filters = showContextFilter
    ? [
        {
          ...CONTEXT_FILTER,
          loadItems: () => getContextFilterItems(context.getId(), translate),
        },
      ]
    : undefined;
  const { applyFilters, isLoading, hasFilters, ...filterProps } =
    useSearchFilters(filters);

  // Using get-methods since we cannot rely on
  // the React providers (useESContext and useRdformsContext)
  // EntryChooser is registered initially by renderingContext very high up in the component tree.
  const [, fetchContextEntries, searchProps] = getQueryFromItem(
    item,
    '',
    context
  );

  const createQuery = useCallback(() => {
    const [query] = getQueryFromItem(item, '', context);
    return query;
  }, [context, item]);

  const applyParams = useCallback(
    (query, conf) => {
      if (searchProps) {
        applyEntityTypeParams(query, conf, { searchProps });
      } else {
        defaultApplyQueryParams(query, conf);
      }
    },
    [searchProps]
  );

  const queryResults = useSolrQuery({
    createQuery,
    applyQueryParams: applyParams,
    runQuery: fetchContextEntries,
    limit: ROWS_PER_PAGE,
    applyFilters,
    wait: isLoading,
  });

  const listActions = [
    {
      id: 'create',
      Dialog: CreateDialog,
      labelNlsKey: 'createButton',
      tooltipNlsKey: 'actionTooltip',
    },
  ];

  const onSelectEntry = (entry) => {
    onClose();
    onSelect(getChoiceFromEntry(entry));
  };

  const rdfType = rdfTypeFromConstraints(constraints);

  return (
    <EntryListView
      nlsBundles={[escoEntryChooserNLS, escoListNLS]}
      includeFilters={hasFilters}
      filtersProps={hasFilters ? filterProps : null}
      listActions={entityType?.inlineCreation ? listActions : []}
      listActionsProps={{
        actionParams: {
          entityType,
          afterCreateEntry: onSelectEntry,
          context,
          contextName,
          rdfType,
          nlsBundles: [eswoBenchNLS],
        },
      }}
      viewPlaceholderProps={{
        label: translate('errorMessage', item.getLabel()),
        className: 'escoEntrySelectList__placeholder',
      }}
      columns={[
        {
          ...TITLE_COLUMN,
          getProps: getTitleProps,
        },
        MODIFIED_COLUMN,
        {
          ...INFO_COLUMN,
          xs: 2,
          Component: ListItemActionIconButton,
          getProps: ({ entry }) => ({
            action: {
              id: ACTION_INFO_ID, // not using action def to avoid circ dependency
              labelNlsKey: 'infoEntry',
              isVisible: () => canReadMetadata(entry),
              entry,
              Dialog: LinkedDataBrowserDialog,
              entityTypeDef: entityType || {
                template: null,
                contentviewers: null,
              },
            },
          }),
        },
        {
          id: 'select',
          xs: 1,
          Component: ListItemButtonAction,
          getProps: ({ entry }) => ({
            ButtonComponent: EntrySelectButton,
            entry,
            binding,
            onSelect: onSelectEntry,
          }),
        },
      ]}
      {...queryResults}
    />
  );
};

EntrySelectList.propTypes = {
  binding: PropTypes.shape({ getItem: PropTypes.func }),
  onClose: PropTypes.func,
  onSelect: PropTypes.func,
};

export default withListModelProvider(EntrySelectList, () => ({
  limit: ROWS_PER_PAGE,
}));
