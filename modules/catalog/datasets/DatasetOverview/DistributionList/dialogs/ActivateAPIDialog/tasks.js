import { STATUS_IDLE } from 'commons/hooks/useTasksList';

export const INIT_TASK_ID = 'init';
export const FILEPROCESS_TASK_ID = 'fileprocess';

export const TASKS_ACTIVATE_API = [
  {
    id: INIT_TASK_ID,
    nlsKeyLabel: 'apiInitialized',
    status: STATUS_IDLE,
    message: '',
  },
  {
    id: FILEPROCESS_TASK_ID,
    nlsKeyLabel: 'apiGenerationTask',
    status: STATUS_IDLE,
    message: '',
  },
];
