import PropTypes from 'prop-types';
import { graphPropType } from 'commons/util/entry';
import {
  useBlankEdit,
  SelectEntryEditor,
  URI,
  useEditorContext,
  CHANGE,
  REMOVE_FIELD_GROUPS,
} from 'commons/components/forms/editors';
import { findGroupsByResourceURI } from 'commons/components/forms/editors/utils/editorContext';
import SelectEditMode from 'models/components/SelectEditMode';
import esmoCommonsNls from 'models/nls/esmoCommons.nls';
import {
  RDF_PROPERTY_RESOURCE_ENTRY,
  RDF_TYPE_QUALIFIED_RESOURCE,
} from 'models/utils/ns';
import { STATUS_FILTER } from 'models/utils/filterDefinitions';

export const ResourceEntryEditor = ({
  graph,
  resourceURI,
  property,
  editModeChoices,
  onEditModeChange,
  editModeValue,
  resourceRdfType,
  selectTooltipNlsKey,
  placeholderProps,
  getTitleProps,
  resourceProperty = RDF_PROPERTY_RESOURCE_ENTRY,
  dialogProps = {},
  isResourceURI = false,
  dispatch,
  validate,
  ...fieldsProps
}) => {
  const {
    blanks: [blank],
    fieldGroup,
    removeBlank,
  } = useBlankEdit({
    name: fieldsProps.name,
    graph,
    resourceURI,
    rdfType: RDF_TYPE_QUALIFIED_RESOURCE,
    property,
    validate,
    dispatch,
    mandatory: fieldsProps.mandatory,
  });
  const { dispatch: editorContextDispatch, fieldSet } = useEditorContext();
  const handleSelectEditMode = (value) => {
    const groups = findGroupsByResourceURI(fieldSet, blank.getBlankNodeId());
    dispatch({
      type: REMOVE_FIELD_GROUPS,
      fieldGroups: [fieldGroup, ...groups],
    });
    onEditModeChange(value);
  };

  const handleChange = (entry, groupField) => {
    const hasValue = groupField.hasValue();
    blank.setAsserted(hasValue);
    if (dispatch) dispatch({ type: CHANGE, value: { entry } });
  };

  return (
    <SelectEntryEditor
      graph={graph}
      resourceURI={blank.getBlankNodeId()}
      renderInlineContent={() => (
        <SelectEditMode
          value={editModeValue}
          choices={editModeChoices}
          nlsBundles={esmoCommonsNls}
          onChange={(value) => handleSelectEditMode(value)}
        />
      )}
      selectEntryTooltipNlsKey={selectTooltipNlsKey}
      dialogProps={{
        rdfType: resourceRdfType,
        filters: [STATUS_FILTER],
        placeholderProps,
        getTitleProps,
        ...dialogProps,
      }}
      property={resourceProperty}
      isResourceURI={isResourceURI}
      onChange={handleChange}
      onRemove={() => removeBlank(blank)}
      nodetype={URI}
      dispatch={editorContextDispatch}
      {...fieldsProps}
    />
  );
};

ResourceEntryEditor.propTypes = {
  name: PropTypes.string,
  property: PropTypes.string,
  graph: graphPropType,
  resourceURI: PropTypes.string,
  editModeChoices: PropTypes.arrayOf(PropTypes.shape({})),
  onEditModeChange: PropTypes.func,
  editModeValue: PropTypes.string,
  resourceRdfType: PropTypes.string,
  selectTooltipNlsKey: PropTypes.string,
  placeholderProps: PropTypes.shape({}),
  getTitleProps: PropTypes.func,
  dispatch: PropTypes.func,
  onChange: PropTypes.func,
  resourceProperty: PropTypes.string,
  dialogProps: PropTypes.shape({}),
  isResourceURI: PropTypes.bool,
  validate: PropTypes.func,
};
