import { Context, Entry } from '@entryscape/entrystore-js';
import { namespaces as ns } from '@entryscape/rdfjson';
import { loadEntityTypeOptions } from 'commons/types/useGetEntityTypes';
import { localize } from 'commons/locale';
import { entrystore } from 'commons/store';
import { getLabel } from 'commons/util/rdfUtils';
import { RDF_PROPERTY_DRAFT_STATUS } from 'commons/util/entry';

export const ANY = 'any';
export const ANY_FILTER_ITEM = {
  labelNlsKey: 'anyFilterLabel',
  value: ANY,
};

export const AUTOCOMPLETE = 'autocomplete';
export const CHECKBOX = 'checkbox';

export const EXTERNAL_STATUS_FILTER = {
  headerNlsKey: 'psiFilterHeader',
  applyFilterParams: (query, filter) => {
    const itemValue = filter.getSelected();
    if (itemValue === 'psiExternal')
      return query.literalProperty('esterms:psi', 'true', 'not');
    if (itemValue === 'psiInternal')
      return query.literalProperty('esterms:psi', 'true');
    return query;
  },
  items: [
    {
      labelNlsKey: 'anyFilterLabel',
      value: ANY,
    },
    {
      labelNlsKey: 'psiExternalLabel',
      value: 'psiExternal',
    },
    {
      labelNlsKey: 'psiInternalLabel',
      value: 'psiInternal',
    },
  ],
  id: 'psi-filter',
};

export const PUBLIC_STATUS_FILTER = {
  headerNlsKey: 'publicFilterHeader',
  applyFilterParams: (query, filter) => {
    const itemValue = filter.getSelected();
    if (itemValue === 'pubPublic') return query.publicRead(true);
    if (itemValue === 'pubUnpublished') return query.publicRead(false);
    return query;
  },
  items: [
    {
      labelNlsKey: 'anyFilterLabel',
      value: ANY,
    },
    {
      labelNlsKey: 'pubPublicLabel',
      value: 'pubPublic',
    },
    {
      labelNlsKey: 'pubUnpublishedLabel',
      value: 'pubUnpublished',
    },
  ],
  id: 'public-filter',
};

export const DRAFT_STATUS_FILTER = {
  headerNlsKey: 'draftFilterHeader',
  applyFilterParams: (query, filter) => {
    const itemValue = filter.getSelected();
    if (itemValue === 'draft')
      return query.status(ns.expand(RDF_PROPERTY_DRAFT_STATUS));
    if (itemValue === 'completed')
      return query.status(ns.expand(RDF_PROPERTY_DRAFT_STATUS), true);
    return query;
  },
  items: [
    {
      labelNlsKey: 'anyFilterLabel',
      value: ANY,
    },
    {
      labelNlsKey: 'draftLabel',
      value: 'draft',
    },
    {
      labelNlsKey: 'completedLabel',
      value: 'completed',
    },
  ],
  id: 'draft-filter',
};

/**
 * @param {Entry} entry
 * @returns {Promise<object[]>}
 */
export const loadProfileItems = async (entry) => {
  const entityTypes = await loadEntityTypeOptions(entry);
  if (entityTypes.length < 2) return [];
  const profileItems = entityTypes.map((entityType) => ({
    label: localize(entityType.label()),
    value: entityType.getId(),
  }));
  return [ANY_FILTER_ITEM, ...profileItems];
};

export const PROFILE_FILTER = {
  headerNlsKey: 'profileFilterHeader',
  applyFilterParams: (query, filter) => {
    const itemValue = filter.getSelected();
    const firstNonAnyItem = filter
      .getItems()
      .find((item) => item.value !== ANY);

    if (itemValue !== firstNonAnyItem?.value) return query.profile(itemValue);
    return query.or({
      profile: itemValue,
      and: { '*': '*', not: { profile: '*' } },
    });
  },
  items: [
    {
      labelNlsKey: 'anyFilterLabel',
      value: ANY,
    },
  ],
  id: 'profile-filter',
};

const PUBLIC_SECTOR = 'publicSector';
const PRIVATE_SECTOR = 'privateSector';
export const SECTOR_FILTER = {
  headerNlsKey: 'sectorFilterHeader',
  applyFilterParams: (query, filter) => {
    const value = filter.getSelected();
    if (value === PUBLIC_SECTOR)
      return query.literalProperty('dcterms:subject', 'psi');
    if (value === PRIVATE_SECTOR)
      return query.literalProperty('dcterms:subject', 'psi', 'not');
    return query;
  },
  items: [
    ANY_FILTER_ITEM,
    {
      labelNlsKey: 'publicFilterLabel',
      value: PUBLIC_SECTOR,
    },
    {
      labelNlsKey: 'privateFilterLabel',
      value: PRIVATE_SECTOR,
    },
  ],
  id: 'sector-filter',
};

/**
 * Find all contexts except current and convert to filter items.
 *
 * @param {Context} context
 * @param {string} rdfType
 * @returns {Promise<object>}
 */
export const loadProjectItems = async (context, rdfType) => {
  const projectEntries = await entrystore
    .newSolrQuery()
    .rdfType(rdfType)
    .list()
    .getEntries();
  const projectItems = projectEntries
    .filter((projectEntry) => projectEntry.getResource(true) !== context)
    .map((projectEntry) => {
      return {
        value: projectEntry.getId(),
        label: getLabel(projectEntry),
      };
    });
  return [ANY_FILTER_ITEM, ...projectItems];
};

export const PROJECT_FILTER = {
  type: 'autocomplete',
  headerNlsKey: 'projectLabel',
  applyFilterParams: (query, filter) => {
    const value = filter.getSelected();
    return query.context(value);
  },
  id: 'project-filter',
};

export const HVD_FILTER = {
  type: CHECKBOX,
  headerNlsKey: 'hvdFilterHeader',
  applyFilterParams: (query, filter) => {
    if (filter.getSelected() === ANY) return query;
    return query.uriProperty(
      'http://data.europa.eu/r5r/applicableLegislation',
      'http://data.europa.eu/eli/reg_impl/2023/138/oj'
    );
  },
  items: [
    ANY_FILTER_ITEM,
    {
      value: 'hvd',
    },
  ],
  id: 'hvd-filter',
};

export const CONTEXT_FILTER = {
  id: 'context-filter',
  type: AUTOCOMPLETE,
  headerNlsKey: 'contextFilterLabel',
  applyFilterParams: (query, filter) => {
    const value = filter.getSelected();
    if (value === ANY) return query;

    // the default query contains a NOT modifier on `context` to exclude users - in
    // principals context - that has to be cleared when a different context is selected
    if (query.modifiers.get('context')) {
      query.modifiers.delete('context');
    }

    return query.context(value);
  },
};

export const CONTEXT_CHECKBOX_FILTER = {
  type: CHECKBOX,
  headerNlsKey: 'contextFilterHeader',
  applyFilterParams: (query, filter) => {
    const selected = filter.getSelected();
    if (selected === ANY) return query;
    return query.context(selected);
  },
  id: 'context-checkbox-filter',
};
