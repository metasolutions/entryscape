import PropTypes from 'prop-types';
import { TextField, MenuItem, InputAdornment } from '@mui/material';
import { Translate as TranslateIcon } from '@mui/icons-material';
import { useTranslation } from 'commons/hooks/useTranslation';
import escoLayoutNLS from 'commons/nls/escoLayout.nls';
import { sortLanguages } from 'commons/locale';
import { useLanguage } from 'commons/hooks/useLanguage';
import config from 'config';

const LanguageSelect = ({ textFieldProps }) => {
  const languages = sortLanguages(config.get('locale.supported'));
  const [language, setLanguage] = useLanguage();
  const t = useTranslation(escoLayoutNLS);

  return (
    <TextField
      select
      label={t('language')}
      value={language}
      onChange={({ target: { value: newLanguage } }) => {
        setLanguage(newLanguage);
      }}
      variant="outlined"
      InputProps={{
        startAdornment: (
          <InputAdornment position="start">
            <TranslateIcon color="secondary" />
          </InputAdornment>
        ),
        inputProps: {
          id: 'language-select-input',
        },
      }}
      InputLabelProps={{ htmlFor: 'language-select-input' }}
      {...textFieldProps}
    >
      {languages.map(({ lang, label }) => (
        <MenuItem key={lang} value={lang}>
          {label}
        </MenuItem>
      ))}
    </TextField>
  );
};

LanguageSelect.propTypes = {
  textFieldProps: PropTypes.objectOf(PropTypes.string),
};

export default LanguageSelect;
