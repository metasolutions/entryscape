import PropTypes from 'prop-types';
import { IconButton, InputAdornment, InputBase } from '@mui/material';
import { Clear as ClearIcon, Search as SearchIcon } from '@mui/icons-material';
import Tooltip from 'commons/components/common/Tooltip';
import './SearchInput.scss';

const SearchInput = ({
  query,
  onChange,
  clear,
  placeholder,
  clearTooltip,
  inputClass = 'escoSearchInput',
}) => {
  return (
    <InputBase
      value={query}
      placeholder={placeholder}
      classes={{ root: inputClass }}
      onChange={onChange}
      inputProps={{ 'aria-label': placeholder }}
      startAdornment={
        <InputAdornment
          position="start"
          classes={{ root: 'escoSearchInput__adornment' }}
        >
          <SearchIcon />
        </InputAdornment>
      }
      endAdornment={
        query ? (
          <InputAdornment position="end">
            <Tooltip title={clearTooltip}>
              <IconButton
                aria-label={clearTooltip}
                onClick={clear}
                onMouseDown={(e) => e.preventDefault()}
                color="secondary"
              >
                <ClearIcon />
              </IconButton>
            </Tooltip>
          </InputAdornment>
        ) : null
      }
    />
  );
};

SearchInput.propTypes = {
  query: PropTypes.string,
  onChange: PropTypes.func,
  clear: PropTypes.func,
  placeholder: PropTypes.string,
  clearTooltip: PropTypes.string,
  inputClass: PropTypes.string,
};

export default SearchInput;
