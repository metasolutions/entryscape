import PropTypes from 'prop-types';
import { useEffect, useState } from 'react';
import ListActionDialog from 'commons/components/common/dialogs/ListActionDialog';
import {
  useTranslation,
  nlsBundlesPropType,
} from 'commons/hooks/useTranslation';
import ContentWrapper from 'commons/components/common/ContentWrapper';
import { Button } from '@mui/material';
import { useEditorContext } from 'commons/components/forms/editors';
import useAsync from 'commons/hooks/useAsync';
import { useSnackbar, SUCCESS_EDIT } from 'commons/hooks/useSnackbar';
import { ErrorCatcher } from 'commons/errors/ErrorCatcher';
import { ErrorWithMessage } from 'commons/errors/utils/errors';

const TypeEditDialog = ({
  closeDialog,
  title,
  nlsBundles,
  children,
  onEdit = () => {},
}) => {
  const translate = useTranslation(nlsBundles);
  const [canSubmit, setCanSubmit] = useState(false);
  const { runAsync, error: editError } = useAsync();
  const { entry, graph, resourceURI, validate } = useEditorContext();
  const [addSnackbar] = useSnackbar();

  useEffect(() => {
    if (!graph) return;
    graph.onChange = () => setCanSubmit(true);
  }, [graph, resourceURI]);

  const saveChanges = async () => {
    const errors = validate();
    if (errors.length) {
      return;
    }

    entry.setMetadata(graph);

    runAsync(
      entry
        .commitMetadata()
        .then(() => {
          closeDialog();
          onEdit();
          addSnackbar({ type: SUCCESS_EDIT });
        })
        .catch((error) => {
          throw new ErrorWithMessage(
            translate('editCustomEntityTypeFail'),
            error
          );
        })
    );
  };

  const actions = (
    <Button onClick={() => saveChanges()} disabled={!canSubmit}>
      {translate('saveButton')}
    </Button>
  );

  return (
    <>
      <ListActionDialog
        id="edit-type-dialog"
        title={title}
        actions={actions}
        closeDialog={closeDialog}
      >
        <ContentWrapper>{children}</ContentWrapper>
      </ListActionDialog>
      <ErrorCatcher error={editError} />
    </>
  );
};

TypeEditDialog.propTypes = {
  closeDialog: PropTypes.func.isRequired,
  title: PropTypes.string,
  nlsBundles: nlsBundlesPropType,
  children: PropTypes.node,
  onEdit: PropTypes.func,
};

export default TypeEditDialog;
