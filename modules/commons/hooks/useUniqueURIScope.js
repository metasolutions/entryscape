import PropTypes from 'prop-types';
import { useState, useContext, createContext } from 'react';

/**
 * Manages the context of unique URI scope
 */
const UniqueURIScopeContext = createContext();

/**
 * A context provider for URI uniqueness scope
 * @param {node} children
 */
export const UniqueURIScopeProvider = ({ children }) => {
  const [isURIUnique, setIsURIUnique] = useState(true);

  return (
    <UniqueURIScopeContext.Provider value={{ isURIUnique, setIsURIUnique }}>
      {children}
    </UniqueURIScopeContext.Provider>
  );
};

UniqueURIScopeProvider.propTypes = {
  children: PropTypes.node,
};

export const useUniqueURIScopeContext = () => {
  const context = useContext(UniqueURIScopeContext);
  if (!context) {
    throw Error(
      'useUniqueURIScopeContext must be used within a ContextProvider'
    );
  }
  return context;
};

/**
 * Wraps a componet with UniqueURIScopeProvider
 * @return {node}
 */

export const withURIScopeContextProvider = (Component) => {
  return (props) => {
    return (
      <UniqueURIScopeProvider>
        <Component {...props} />
      </UniqueURIScopeProvider>
    );
  };
};
