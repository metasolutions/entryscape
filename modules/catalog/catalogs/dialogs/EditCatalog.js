import { useCallback } from 'react';
import { updateContextTitle } from 'commons/util/context';
import EditEntryDialog from 'commons/components/entry/EditEntryDialog';

const EditCatalog = (props) => {
  const onEdit = useCallback(async (entry) => {
    await updateContextTitle(entry);
  }, []);

  return <EditEntryDialog onEntryEdit={onEdit} {...props} />;
};

export default EditCatalog;
