import { useESContext } from 'commons/hooks/useESContext';
import { useUserState } from 'commons/hooks/useUser';
import { useState, useEffect } from 'react';
import { Typography, Divider, Button } from '@mui/material';
import esreHarvestNLS from 'harvest/nls/esreHarvest.nls';
import { useTranslation } from 'commons/hooks/useTranslation';
import { getPipelineEntryAndResource } from 'harvest/util/data';
import {
  getOptions,
  OPTION_OFF,
  OPTION_VALIDATION,
  OPTION_HARVEST,
} from 'harvest/Notifications/util/options';
import {
  getNotificationSettingsDefaultEmail,
  getNotificationsSettings,
  setNotificationsSettings,
} from 'harvest/Notifications/util/store';
import {
  getOptionByValue,
  getOptionByType,
} from 'harvest/Notifications/handlers';
import NotificationsDialog from 'harvest/Notifications/';

const NotificationsView = () => {
  const { context } = useESContext();
  const contextId = context.getId();
  const { userInfo } = useUserState();

  const selectOptions = getOptions();
  const harvestNotificationLabel = selectOptions[OPTION_HARVEST][0].label;
  const harvestSelectOpts = selectOptions[OPTION_HARVEST][0].selectOpts;
  const validationNotificationLabel = selectOptions[OPTION_VALIDATION][0].label;
  const validationSelectOpts = selectOptions[OPTION_VALIDATION][0].selectOpts;

  const [open, setOpen] = useState(false);
  const [entry, setEntry] = useState(null);
  const [settings, setSettings] = useState({
    email: '',
    harvesting: OPTION_OFF,
    validation: OPTION_OFF,
  });

  const [showCurrentHarvestingSetting, setShowCurrentHarvestingSetting] =
    useState(false);
  const [showCurrentValidationSetting, setShowCurrentValidationSetting] =
    useState(false);
  const [harvestingCurrentSettings, setHarvestingCurrentSettings] = useState(
    {}
  );
  const [validationCurrentSettings, setValidationCurrentSettings] = useState(
    {}
  );

  /**
   * Create an array of notifciation options containing the status off and a value
   *
   * Type can be:
   *  - strict
   *  - lax
   *  - off
   *
   * Value ranges from 0 to the total number of notifications
   *
   * Message is the description of the notification
   *
   * @param {array} selectOpts - Notification options containing message and type
   * @return {array} Notification options for a specific type of notification
   */
  const getNotificationOptions = (selectOpts) => {
    const opt = [
      {
        type: OPTION_OFF,
        message: 'off',
        value: 0,
      },
    ];
    selectOpts.forEach((option, idx) => {
      opt.push({
        type: option.value,
        message: option.label,
        value: idx + 1,
      });
    });
    return opt;
  };

  const harvestNotificationOptions = getNotificationOptions(harvestSelectOpts);
  const validationNotificationOptions =
    getNotificationOptions(validationSelectOpts);

  const notificationStatuses = (harvesting, validation) => {
    const harvestingSettings = getOptionByType(
      harvestNotificationOptions,
      harvesting
    );
    const validationSettings = getOptionByType(
      validationNotificationOptions,
      validation
    );
    const showHarvesting = harvesting !== OPTION_OFF;
    setShowCurrentHarvestingSetting(showHarvesting);
    setHarvestingCurrentSettings(harvestingSettings);

    const showValidation = validation !== OPTION_OFF;
    setShowCurrentValidationSetting(showValidation);
    setValidationCurrentSettings(validationSettings);
  };

  useEffect(() => {
    const fetchData = async () => {
      const { pipelineEntry } = await getPipelineEntryAndResource(contextId);
      setEntry(pipelineEntry);
    };

    fetchData();
  }, []);

  useEffect(() => {
    if (entry) {
      const getSettings = async () => {
        const currentUserEmail = getNotificationSettingsDefaultEmail(
          entry,
          userInfo.user
        );
        const { isEnabled, email, harvesting, validation } =
          await getNotificationsSettings(entry);

        setSettings({
          isEnabled: isEnabled || 'true',
          email: email || currentUserEmail,
          harvesting: harvesting || 'off',
          validation: validation || 'off',
        });
      };

      getSettings();
    }
  }, [entry]);

  useEffect(() => {
    notificationStatuses(settings.harvesting, settings.validation);
  }, [settings.harvesting, settings.validation]);

  const handleSaveForm = async (updatedSettings) => {
    const { isEnabled, email, harvestingValue, validationValue } =
      updatedSettings;

    const harvestOption = getOptionByValue(
      harvestNotificationOptions,
      harvestingValue
    );

    const validationOption = getOptionByValue(
      validationNotificationOptions,
      validationValue
    );

    const newSettings = {
      isEnabled,
      email,
      harvesting: harvestOption.type,
      validation: validationOption.type,
    };
    try {
      await setNotificationsSettings(entry, newSettings);
      setSettings(newSettings);
      setOpen(false);
    } catch (err) {
      throw Error(
        'Something went wrong with saving pipeline arguments. Please try again'
      );
    }
  };

  const closeDialog = () => {
    setOpen(false);
  };

  const t = useTranslation(esreHarvestNLS);
  const { email } = settings;
  const dialogProps = {
    harvestNotificationLabel,
    validationNotificationLabel,
    harvestSelectOpts,
    validationSelectOpts,
    harvestNotificationOptions,
    validationNotificationOptions,
    showCurrentHarvestingSetting,
    showCurrentValidationSetting,
    harvestingCurrentSettings: harvestingCurrentSettings.value,
    validationCurrentSettings: validationCurrentSettings.value,
  };

  return (
    <div className="esreNotifications__view">
      <div className="esreNotifications__editButton">
        <Button onClick={() => setOpen(true)}>
          {t('notificationsPromptAction')}
        </Button>
      </div>
      <div className="esreNotifications__form">
        <div>
          <Typography classes={{ root: 'esreNotifications__view__label' }}>
            {t('email')}
          </Typography>
          <Typography>{email}</Typography>
        </div>
        <Typography
          classes={{ root: 'esreNotifications__typography' }}
          variant="body1"
        >
          {t('emailAlert')}
        </Typography>
        <Typography classes={{ root: 'esreNotifications__h2' }} variant="h2">
          {t('harvestingStatusHeader')}
        </Typography>
        <Divider />
        <Typography classes={{ root: 'esreNotifications__notificationStatus' }}>
          {showCurrentHarvestingSetting
            ? t('notificationsOn')
            : t('notificationsOff')}
        </Typography>
        {showCurrentHarvestingSetting && (
          <Typography
            classes={{ root: 'esreNotifications__view__text' }}
            id="harvesting-status-notification"
            variant="body1"
          >
            {`${harvestNotificationLabel} ${harvestingCurrentSettings.message}`}
          </Typography>
        )}
        <Typography classes={{ root: 'esreNotifications__h2' }} variant="h2">
          {t('dataValidationHeader')}
        </Typography>
        <Divider />
        <Typography classes={{ root: 'esreNotifications__notificationStatus' }}>
          {showCurrentValidationSetting
            ? t('notificationsOn')
            : t('notificationsOff')}
        </Typography>
        {showCurrentValidationSetting && (
          <Typography
            classes={{ root: 'esreNotifications__view__text' }}
            id="data-validation-notification"
            variant="body1"
          >
            {`${validationNotificationLabel} ${validationCurrentSettings.message}`}
          </Typography>
        )}
      </div>
      {open && (
        <NotificationsDialog
          configSettings={settings}
          dialogProps={dialogProps}
          getOptions={getOptions}
          onSave={handleSaveForm}
          open={open}
          onClose={closeDialog}
        />
      )}
    </div>
  );
};

export default NotificationsView;
