import PropTypes from 'prop-types';
import { Entry } from '@entryscape/entrystore-js';
import {
  useTranslation,
  nlsBundlesPropType,
} from 'commons/hooks/useTranslation';
import ProgressList from 'commons/components/common/ProgressList';
import {
  useTasksList,
  STATUS_PROGRESS,
  STATUS_DONE,
} from 'commons/hooks/useTasksList';
import { useState, useEffect, useCallback } from 'react';
import { constructURIFromPattern } from 'commons/util/store';
import Lookup from 'commons/types/Lookup';
import { Button, TextField, InputAdornment } from '@mui/material';
import { useEntityURI } from 'commons/hooks/useEntityTitleAndURI';
import { useMainDialog } from 'commons/hooks/useMainDialog';
import { AcknowledgeAction } from 'commons/components/common/dialogs/MainDialog';
import ListActionDialog from 'commons/components/common/dialogs/ListActionDialog';
import { TASKS_UPDATE_PROJECT_URI } from './tasks';
import {
  updateURISpace,
  getItemsToUpdate,
  updateResourceURIs,
  updateContextName,
} from './utils';

const UpdateProjectURIDialog = ({
  entry,
  closeDialog,
  nlsBundles,
  onChange,
}) => {
  const primaryEntityType = Lookup.getMainEntityType('workbench');
  const { uriPattern } = primaryEntityType;

  const { openMainDialog, setDialogContent, closeMainDialog } = useMainDialog();

  const { name, setName, isNameFree, handleNameChange } = useEntityURI({
    uriPattern,
  });
  const [contextName, setContextName] = useState('');
  const [pristine, setPristine] = useState(true);
  const { tasks, updateTask, allTasksDone } = useTasksList([
    ...TASKS_UPDATE_PROJECT_URI,
  ]);

  // load the context entry to get the context name (stored in the entry info graph)
  useEffect(() => {
    entry.refresh(true, true).then(() => {
      setContextName(entry.getEntryInfo().getName());
      setName(entry.getEntryInfo().getName());
    });
  }, [entry, setName]);

  const t = useTranslation(nlsBundles);

  const closeDialogs = useCallback(() => {
    closeMainDialog();
    closeDialog();
  }, [closeDialog, closeMainDialog]);

  // TODO move into a hook
  const updateMainDialogTasks = useCallback(
    (updatedTasks) => {
      setDialogContent({
        title: t('taskUpdateProjectURL'),
        content: <ProgressList tasks={updatedTasks} nlsBundles={nlsBundles} />,
        actions: (
          <AcknowledgeAction onDone={closeDialogs} isDisabled={!allTasksDone} />
        ),
      });
    },
    [allTasksDone, closeDialogs, nlsBundles, setDialogContent, t]
  );

  useEffect(() => {
    updateMainDialogTasks(tasks);
  }, [tasks, updateMainDialogTasks]);

  const onChangeName = async () => {
    const contextEntry = entry;
    const context = await updateContextName(contextEntry, name);
    await updateURISpace(contextEntry, name, uriPattern);

    openMainDialog({
      title: t('taskUpdateProjectURL'),
      content: <ProgressList tasks={tasks} nlsBundles={nlsBundles} />,
      actions: null,
    });

    const items = await getItemsToUpdate(context, name);

    const totalEntriesToChange = items?.length;
    if (totalEntriesToChange) {
      let fulfilledCount = 0;
      const promises = updateResourceURIs(items);

      // For each of the update promises above, await and update progress dialog
      for (const entryUpdated of promises) {
        await entryUpdated;
        fulfilledCount += 1;
        updateTask('project', {
          message: t('taskUpdateProjectLinks', {
            updatedEntriesCount: fulfilledCount,
            totalEntries: totalEntriesToChange,
          }),
          status: STATUS_PROGRESS,
        });
      }
    } else {
      updateTask('project', {
        message: t('taskUpdateProjectNoEntriesToUpdate'),
      });
    }

    updateTask('project', { status: STATUS_DONE });
    onChange();
  };

  const actions = (
    <Button
      autoFocus
      disabled={!name || !isNameFree || pristine}
      onClick={onChangeName}
    >
      {t('editProjectNameButton')}
    </Button>
  );
  return (
    <ListActionDialog
      id="update-project-uri"
      closeDialog={closeDialog}
      actions={actions}
      title={t('editProjectNameTitle')}
    >
      <TextField
        disabled
        label={t('replaceEntityURLCurrentLinkLabel')}
        id="project-edit-name-current-link"
        InputProps={{
          startAdornment: (
            <InputAdornment position="start">
              {constructURIFromPattern(uriPattern, {
                contextName,
              })}
            </InputAdornment>
          ),
        }}
      />
      <TextField
        label={t('replaceEntityURLNewLinkLabel')}
        id="project-edit-name-new-link"
        onChange={(evt) => {
          setPristine(false);
          handleNameChange(evt);
        }}
        error={!pristine && (!name || !isNameFree)}
        helperText={
          !pristine &&
          (!name
            ? t('URLRequired')
            : !isNameFree && t('unavailableURLFeedback'))
        }
        InputProps={{
          startAdornment: (
            <InputAdornment position="start">
              {constructURIFromPattern(uriPattern)}
            </InputAdornment>
          ),
        }}
      />
    </ListActionDialog>
  );
};

UpdateProjectURIDialog.propTypes = {
  entry: PropTypes.instanceOf(Entry),
  closeDialog: PropTypes.func,
  nlsBundles: nlsBundlesPropType,
  onChange: PropTypes.func,
};
export default UpdateProjectURIDialog;
