import PropTypes from 'prop-types';
import { Entry } from '@entryscape/entrystore-js';
import PublicStatusIcon from 'commons/components/common/Icons/PublicIcon';
import { ListItemAction } from 'commons/components/ListView';
import { isDraftEntry } from 'commons/util/entry';
import { ListDraftStatusIcon } from '../ListDraftStatusIcon';

export const ListPublicStatusIcon = ({
  entry,
  xs = 1,
  internalTooltip,
  publicTooltip,
  privateTooltip,
}) => {
  if (isDraftEntry(entry)) return <ListDraftStatusIcon entry={entry} xs={xs} />;
  return (
    <ListItemAction xs={xs}>
      <PublicStatusIcon
        entry={entry}
        internalTooltip={internalTooltip}
        publicTooltip={publicTooltip}
        privateTooltip={privateTooltip}
      />
    </ListItemAction>
  );
};

ListPublicStatusIcon.propTypes = {
  entry: PropTypes.instanceOf(Entry),
  internalTooltip: PropTypes.string,
  publicTooltip: PropTypes.string,
  privateTooltip: PropTypes.string,
  xs: PropTypes.number,
};

export default ListPublicStatusIcon;
