import { getLabel, getDescription } from 'commons/util/rdfUtils';
import { Context, Entry } from '@entryscape/entrystore-js';
import FormCellStore from './FormCellStore';
import ClassCellStore from './ClassCellStore';
import {
  importLayout,
  getDiagramTypeFromMetadata,
  getImplicitClassesFromMetadata,
  getLabelTypeFromMetadata,
  exportLayout,
} from './layoutInRDF';
import { createNamespaceStore } from './namespaceStore';

/**
 *
 * @param {Context} context
 * @param {string} diagramEntryId
 * @returns {Entry}
 */
export const createDiagramPrototypeEntry = (context, diagramEntryId) => {
  return context
    .newGraph({}, diagramEntryId)
    .add('rdf:type', 'esterms:Diagram');
};

export const getDiagramLabel = (entry, translate, isDefault = false) => {
  let heading = entry ? getLabel(entry) : undefined;
  if (!heading) {
    heading = isDefault
      ? translate('defaultDiagram')
      : translate('unnamedDiagram');
  }
  return heading;
};

export const getDiagramDescription = (entry, translate, isDefault = false) => {
  let heading = entry ? getDescription(entry) : undefined;
  if (!heading) {
    heading = isDefault
      ? translate('defaultDiagramDescription')
      : translate('noDiagramDescription');
  }
  return heading;
};

export const initLayout = async (diagramUtils, graph, diagramEntry) => {
  graph.getLinks().forEach((link) => link.prop('vertices', []));
  const resource = await diagramEntry.getResource();
  const resourceGraph = resource.getGraph();
  if (resourceGraph.size() === 0) {
    return diagramUtils.applyAutoLayout(graph);
  }
  importLayout(graph, resourceGraph);
};

export class DiagramAdapter {
  constructor({
    diagramUtils,
    onClick,
    canvas,
    diagramEntry,
    context,
    translate,
  }) {
    this._canvas = canvas;
    this._onClick = onClick;
    this._context = context;
    this._translate = translate;
    this._diagramUtils = diagramUtils;
    this._diagramEntry = diagramEntry;
    this._graph = diagramUtils.createGraph();
    this._paper = this.createPaper(canvas);
  }

  async createCellStores() {
    const diagramType = this.getDiagramType();
    const storeProps = {
      context: this._context,
      diagramUtils: this._diagramUtils,
      graph: this._graph,
      includeImplicitClasses: this.getIncludeImplicitClasses(),
      labelType: this.getLabelType(),
      namespaceStore: this._namespaceStore,
      translate: this._translate,
    };
    if (diagramType === 'form' || diagramType === 'mixed') {
      this._formCellStore = new FormCellStore(storeProps);
      await this._formCellStore.init();
    } else {
      this._formCellStore = null;
    }
    if (diagramType === 'class' || diagramType === 'mixed') {
      this._classCellStore = new ClassCellStore(storeProps);
      await this._classCellStore.init();
    } else {
      this._classCellStore = null;
    }
    await initLayout(this._diagramUtils, this._graph, this._diagramEntry);
  }

  async init() {
    this._namespaceStore = await createNamespaceStore(this._context);
    await this.createCellStores();

    this._paper.unfreeze();
    return this;
  }

  getGraph() {
    return this._graph;
  }

  createPaper(canvas, editMode = false) {
    const paper = this._diagramUtils.createPaper(
      canvas,
      this._graph,
      this._onClick,
      editMode
    );
    this._diagramUtils.initMeasureText(paper);
    return paper;
  }

  getPaper() {
    return this._paper;
  }

  getIncludeImplicitClasses() {
    return getImplicitClassesFromMetadata(this._diagramEntry);
  }

  getLabelType() {
    return getLabelTypeFromMetadata(this._diagramEntry);
  }

  getDiagramType() {
    return getDiagramTypeFromMetadata(this._diagramEntry);
  }

  clearCells() {
    this._graph.clear();
    this._paper.updateViews();
  }

  clearLayout() {
    this._graph.getLinks().forEach((link) => link.prop('vertices', []));
    this._diagramUtils.applyAutoLayout(this._graph);
  }

  exportLayout() {
    return exportLayout(this._graph);
  }

  hasElements() {
    return Boolean(this.getGraph().getElements().length);
  }

  labelByType(labelType) {
    if (this._formCellStore) {
      this._formCellStore.labelByType(labelType);
    }
    if (this._classCellStore) {
      this._classCellStore.labelByType(labelType);
    }
  }
}
