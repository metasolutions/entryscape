/* eslint-disable */

import 'regenerator-runtime/runtime';
import 'commons/webpack/publicPath';

import registry from 'commons/registry';
import initDefaults from './defaults';

initDefaults(); // init defaults
registry.setApp('registry');

export default registry;
