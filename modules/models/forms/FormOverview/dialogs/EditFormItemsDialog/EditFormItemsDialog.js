import { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { Entry } from '@entryscape/entrystore-js';
import { Button } from '@mui/material';
import { useTranslation } from 'commons/hooks/useTranslation';
import { useEditorContext } from 'commons/components/forms/editors';
import esmoFormsNls from 'models/nls/esmoForms.nls';
import esmoCommonsNls from 'models/nls/esmoCommons.nls';
import escoListNLS from 'commons/nls/escoList.nls';
import ListActionDialog from 'commons/components/common/dialogs/ListActionDialog';
import { getLabel } from 'commons/util/rdfUtils';
import { FieldEditor } from 'models/editors/FieldEditor';
import { isForm } from 'models/forms/utils/forms';
import { extendedFieldEditors } from 'models/fields/utils/fieldEditors';
import { extendedFormEditors } from 'models/forms/utils/formEditors';

const NLS_BUNDLES = [esmoFormsNls, esmoCommonsNls, escoListNLS];

const EditFormItemsDialog = ({ closeDialog, extendedEntry, onAccept }) => {
  const { graph, resourceURI: blank } = useEditorContext();
  const [buttonDisabled, setButtonDisabled] = useState(true);
  const translate = useTranslation(NLS_BUNDLES);
  const title = graph
    ? getLabel(graph, blank) || getLabel(extendedEntry) || ''
    : '';
  const fields = isForm(extendedEntry)
    ? extendedFormEditors
    : extendedFieldEditors;

  useEffect(() => {
    if (graph) {
      graph.onChange = () => {
        setButtonDisabled(false);
      };
    }
  }, [graph]);

  const handleAccept = () => {
    onAccept(graph);
    closeDialog();
  };

  const actions = (
    <Button autoFocus onClick={handleAccept} disabled={buttonDisabled}>
      {translate('doneLabel')}
    </Button>
  );

  return (
    <ListActionDialog
      closeDialog={closeDialog}
      id="edit-form-item"
      actions={actions}
      title={translate('editHeader', title)}
    >
      {fields.map(({ labelNlsKey, ...editorProps }) => (
        <FieldEditor
          key={editorProps.property || editorProps.name}
          isExtended
          label={labelNlsKey ? translate(labelNlsKey) : ''}
          translate={translate}
          {...editorProps}
        />
      ))}
    </ListActionDialog>
  );
};

EditFormItemsDialog.propTypes = {
  closeDialog: PropTypes.func,
  extendedEntry: PropTypes.instanceOf(Entry),
  onAccept: PropTypes.func,
};

export default EditFormItemsDialog;
