import { useEffect } from 'react';
import PropTypes from 'prop-types';
import { FeatureInfo, Map } from 'commons/components/Map';
import { useOpenlayers } from 'commons/components/Map/hooks/useOpenlayers';
import { useMapContext } from 'commons/components/Map/hooks/useMapContext';
import ScaleLineControl from 'commons/components/Map/controls/ScaleLineControl';
import AttributionControl from 'commons/components/Map/controls/AttributionControl';
import ControlGroup from 'commons/components/Map/controls/ControlGroup';
import { getMapData } from '../VisualizationChart/utils/csvData';

const VisualizationMap = ({
  data,
  xField,
  yField,
  featureTitleField,
  scaleLineControl,
  height = '400px',
  mapOptions,
  onViewExtentChange,
}) => {
  const { addXyData, zoomToLayer, clearOverlays } = useOpenlayers();
  const [{ map }] = useMapContext();

  useEffect(() => {
    if (data && map) {
      const mapData = getMapData({
        data,
        xField,
        yField,
      });
      clearOverlays(map);
      const layer = addXyData(map, mapData);

      if (mapOptions.initialView) map.getView().fit(mapOptions.initialView);
      else zoomToLayer(map, layer);

      if (!onViewExtentChange) return;

      map.on('moveend', onViewExtentChange);

      return () => {
        map.un('moveend', onViewExtentChange);
      };
    }
  }, [
    map,
    data,
    xField,
    yField,
    onViewExtentChange,
    mapOptions.initialView,
    addXyData,
    clearOverlays,
    zoomToLayer,
  ]);
  return (
    <Map height={height} mapOptions={mapOptions}>
      <FeatureInfo titleField={featureTitleField} />
      <ControlGroup
        position="bottom-right"
        renderControls={(target) => (
          <>
            <AttributionControl target={target} />
            {scaleLineControl === 'true' ? (
              <ScaleLineControl target={target} />
            ) : null}
          </>
        )}
      />
    </Map>
  );
};

VisualizationMap.propTypes = {
  data: PropTypes.shape({}),
  onViewExtentChange: PropTypes.func,
  xField: PropTypes.string,
  yField: PropTypes.string,
  featureTitleField: PropTypes.string,
  scaleLineControl: PropTypes.string,
  height: PropTypes.string,
  mapOptions: PropTypes.shape({
    initialView: PropTypes.arrayOf(PropTypes.number),
  }),
};

export default VisualizationMap;
