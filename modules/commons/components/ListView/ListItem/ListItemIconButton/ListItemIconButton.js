import PropTypes from 'prop-types';
import Tooltip from 'commons/components/common/Tooltip';
import { IconButton } from '@mui/material';
import { ListItemAction } from '../ListItemAction';

// List item component with icon button.
export const ListItemIconButton = ({
  ariaControls,
  title,
  onClick,
  xs,
  justifyContent,
  disabled,
  classes,
  icon,
  children,
  edge = 'end',
  highlightId,
  ...rest
}) => {
  return (
    <ListItemAction justifyContent={justifyContent} xs={xs}>
      <Tooltip title={title}>
        <span>
          <IconButton
            edge={edge}
            aria-label={title}
            aria-controls={ariaControls}
            aria-haspopup="true"
            classes={classes}
            onClick={onClick}
            disabled={disabled}
            data-highlight-id={highlightId}
            {...rest}
          >
            {icon || children}
          </IconButton>
        </span>
      </Tooltip>
    </ListItemAction>
  );
};

ListItemIconButton.propTypes = {
  ariaControls: PropTypes.string,
  title: PropTypes.string,
  onClick: PropTypes.func,
  xs: PropTypes.number,
  justifyContent: PropTypes.string,
  disabled: PropTypes.bool,
  classes: PropTypes.shape({
    root: PropTypes.string,
    button: PropTypes.string,
  }),
  icon: PropTypes.node,
  children: PropTypes.node,
  edge: PropTypes.oneOf(['start', 'end', false]),
  highlightId: PropTypes.string,
};
