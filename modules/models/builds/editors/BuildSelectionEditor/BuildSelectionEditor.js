import { useState, Fragment } from 'react';
import PropTypes from 'prop-types';
import { List, ListView } from 'commons/components/ListView';
import { Button, Box, FormHelperText } from '@mui/material';
import { useTranslation } from 'commons/hooks/useTranslation';
import esmoBuildsNLS from 'models/nls/esmoBuilds.nls';
import { Fields, SelectionListItem } from 'commons/components/forms/editors';
import FormAndFieldChooser from '../FormAndFieldChooser';

export const BuildSelectionEditor = ({
  label,
  mandatory,
  onChange,
  selection,
  buildItems,
  status,
  error,
}) => {
  const translate = useTranslation(esmoBuildsNLS);
  const [showFormsAndFieldsChooser, setShowFormsAndFieldsChooser] =
    useState(false);

  const handleSelectionChange = (newSelection, newBuildItems) => {
    onChange(newSelection, newBuildItems);
    setShowFormsAndFieldsChooser(false);
  };

  return (
    <>
      <Fields
        mandatory={mandatory}
        labelId="select-forms-fields-editor"
        label={label}
        max={1}
      >
        <ListView
          status={status}
          size={selection?.length ?? -1}
          renderPlaceholder={() => (
            <>
              <Box sx={{ fontStyle: 'italic' }}>
                {translate('selectionListPlaceholder')}
              </Box>
              <Button
                variant="text"
                onClick={() => setShowFormsAndFieldsChooser(true)}
                sx={{ paddingLeft: 0, paddingRight: 0 }}
              >
                {translate('addSelectionLabel')}
              </Button>
            </>
          )}
        >
          <List renderLoader={(Loader) => <Loader height="10vh" />}>
            <SelectionListItem
              translate={translate}
              label={translate('formAndFieldLabel')}
              onClick={() => setShowFormsAndFieldsChooser(true)}
              selection={selection}
              disabled={status !== 'resolved'}
            />
          </List>
        </ListView>
        {error ? <FormHelperText error>{error}</FormHelperText> : null}
      </Fields>
      {showFormsAndFieldsChooser ? (
        <FormAndFieldChooser
          selection={selection}
          buildItems={buildItems}
          onChange={handleSelectionChange}
          closeDialog={() => setShowFormsAndFieldsChooser(false)}
        />
      ) : null}
    </>
  );
};

BuildSelectionEditor.propTypes = {
  label: PropTypes.string,
  mandatory: PropTypes.bool,
  status: PropTypes.string,
  error: PropTypes.string,
  onChange: PropTypes.func,
  selection: PropTypes.arrayOf(PropTypes.shape({})),
  buildItems: PropTypes.arrayOf(PropTypes.shape({})),
};
