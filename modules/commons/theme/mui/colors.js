export const primary = {
  main: '#1673C7',

  name: 'Primary',
  usage: 'Default link color',
};

export const accent = {
  main: '#e05f21',

  name: 'Accent',
  usage: 'Accent',
};

export const white = {
  main: '#ffffff',

  name: 'White',
};

export const canvas = {
  main: '#f6f7f7',

  name: 'Canvas',
  usage: 'Canvas bg',
};

export const lightGray = {
  light: '#e6eaeb',
  main: '#dfe4e6',

  name: 'Light gray',
  usage: 'Borders and lines',
};

export const mediumGray = {
  lightest: '#F2F3F4', // main w/ 10% opacity
  light: '#ECEEEE', // main w/ 15%
  main: '#808b90',

  name: 'Medium gray',
  usage: 'Text',
};

export const darkGray = {
  light: '#61676A', // 10% lighter
  main: '#474d50',

  name: 'Dark grey',
  usage: 'Default text',
};

export const grayBlue = {
  main: '#7A8184',

  name: 'Blue Gray',
};

export const info = {
  main: '#191c1d',

  name: 'Info',
  usage: 'Tooltips and snackbars bg',
};

export const success = {
  light: '#c0d999',
  main: '#629f00',

  name: 'Success',
  usage: 'Confirmations',
};

export const error = {
  light: '#de524c',
  main: '#cd2823',

  name: 'Error',
  usage: 'Errors',
};

export const warning = {
  main: '#f7ce26',

  name: 'Warning',
  usage: 'Highlight color',
};

export default [
  primary,
  accent,
  white,
  canvas,
  lightGray,
  mediumGray,
  darkGray,
  grayBlue,
  info,
  success,
  error,
  warning,
];
