import React, { useCallback, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { useTranslation } from 'commons/hooks/useTranslation';
import useAsync from 'commons/hooks/useAsync';
import esmoDiagramsNLS from 'models/nls/esmoDiagrams.nls';
import esmoCommonsNLS from 'models/nls/esmoCommons.nls';
import escoDialogsNLS from 'commons/nls/escoDialogs.nls';
import Button from '@mui/material/Button';
import ClearIcon from '@mui/icons-material/Clear';
import SaveIcon from '@mui/icons-material/Save';
import RestoreIcon from '@mui/icons-material/Restore';
import { AutoFixHigh } from '@mui/icons-material';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import InputLabel from '@mui/material/InputLabel';
import Select from '@mui/material/Select';
import FormControlLabel from '@mui/material/FormControlLabel';
import Switch from '@mui/material/Switch';
import RemoveEntryDialog from 'commons/components/entry/RemoveEntryDialog';
import { useESContext } from 'commons/hooks/useESContext';
import {
  setDiagramTypeInMetadata,
  setImplicitClassesFromMetadata,
  setLabelTypeInMetadata,
} from 'models/diagrams/utils/layoutInRDF';
import { useSnackbar, SUCCESS_EDIT } from 'commons/hooks/useSnackbar';
import {
  DEFAULT_DIAGRAM_ID,
  LABEL_TYPE_LABEL,
  LABEL_TYPE_TITLE,
  LABEL_TYPE_URI,
} from 'models/diagrams/utils/definitions';
import {
  SET_DIAGRAM_ENTRY,
  SET_DIAGRAM_ENTRY_ID,
  SET_LABEL_TYPE,
  useDiagramContext,
} from '../hooks/useDiagramContext';
import { createDiagramPrototypeEntry } from '../utils/diagram';

const NLS_BUNDLES = [esmoDiagramsNLS, esmoCommonsNLS, escoDialogsNLS];

const DiagramEditPanel = ({ paper, onRemove }) => {
  const translate = useTranslation(NLS_BUNDLES);
  const { context } = useESContext();
  const { runAsync: runOnSave } = useAsync();
  const {
    diagramEntry,
    isPrototype,
    diagramEntryId,
    labelType,
    diagramAdapter,
    dispatch,
  } = useDiagramContext();
  const [removeDialogOpen, setRemoveDialogOpen] = useState(false);
  const [diagramType, setDiagramType] = useState('mixed');
  const [implicitClasses, setImplicitClasses] = useState(true);
  const [addSnackbar] = useSnackbar();

  useEffect(() => {
    if (diagramEntry) {
      setDiagramType(diagramAdapter.getDiagramType());
      setImplicitClasses(diagramAdapter.getIncludeImplicitClasses());
    }
  }, [diagramEntry, diagramAdapter]);

  const onSave = async () => {
    const rdfGraph = diagramAdapter.exportLayout();
    const resource = await diagramEntry.getResource();
    resource.setGraph(rdfGraph);

    if (isPrototype) {
      const newDiagramEntry = await diagramEntry.commit();
      dispatch({
        type: SET_DIAGRAM_ENTRY,
        diagramEntry: newDiagramEntry,
        isPrototype: false,
      });
    } else {
      if (diagramEntry.getMetadata().isChanged()) {
        await diagramEntry.commitMetadata();
      }
      await resource.commit();
      diagramEntry.setRefreshNeeded();
      await diagramEntry.refresh();
    }
  };

  const clearLayout = () => {
    diagramAdapter.clearLayout();
  };

  const redraw = useCallback(async () => {
    // freeze paper to not render until all udates are done
    paper.freeze();
    diagramAdapter.clearCells();
    paper.updateViews();
    await diagramAdapter.createCellStores();
    paper.unfreeze();
  }, [paper, diagramAdapter]);

  const changeDiagramType = ({ target }) => {
    setDiagramTypeInMetadata(diagramEntry, target.value);
    setDiagramType(target.value);
    redraw();
  };

  const changeLabelType = async ({ target }) => {
    dispatch({ type: SET_LABEL_TYPE, labelType: target.value });
    setLabelTypeInMetadata(diagramEntry, target.value);
    await diagramAdapter.labelByType(target.value);
  };

  const changeImplicitClasses = ({ target }) => {
    setImplicitClassesFromMetadata(diagramEntry, target.checked);
    setImplicitClasses(target.checked);
    redraw();
  };

  const diagramRemoved = () => {
    if (diagramEntryId === DEFAULT_DIAGRAM_ID) {
      dispatch({
        type: SET_DIAGRAM_ENTRY,
        isPrototype: true,
        diagramEntry: createDiagramPrototypeEntry(context, DEFAULT_DIAGRAM_ID),
      });
    } else {
      dispatch({
        type: SET_DIAGRAM_ENTRY_ID,
        diagramEntryId: DEFAULT_DIAGRAM_ID,
      });
    }
    onRemove();
  };

  return (
    <div className="esmoDiagram__EditButtons">
      <FormControl>
        <InputLabel id="diagramStyleSelectLabel">
          {translate('diagramStyle')}
        </InputLabel>
        <Select
          labelId="diagramStyleSelectLabel"
          value={diagramType}
          label={translate('diagramStyle')}
          onChange={changeDiagramType}
        >
          <MenuItem value="mixed">{translate('diagramStyleMixed')}</MenuItem>
          <MenuItem value="form">{translate('diagramStyleForms')}</MenuItem>
          <MenuItem value="class">{translate('diagramStyleClasses')}</MenuItem>
        </Select>
      </FormControl>
      <FormControl>
        <InputLabel id="diagramLabelTypeLabel">
          {translate('labelType')}
        </InputLabel>
        <Select
          labelId="diagramLabelTypeLabel"
          value={labelType}
          label={translate('labelType')}
          onChange={changeLabelType}
        >
          <MenuItem value={LABEL_TYPE_TITLE}>
            {translate('labelTypeTitle')}
          </MenuItem>
          <MenuItem value={LABEL_TYPE_LABEL}>
            {translate('labelTypeLabel')}
          </MenuItem>
          <MenuItem value={LABEL_TYPE_URI}>
            {translate('labelTypeUri')}
          </MenuItem>
        </Select>
      </FormControl>
      <FormControl>
        <FormControlLabel
          control={
            <Switch
              checked={implicitClasses}
              onChange={changeImplicitClasses}
            />
          }
          label={translate('implicitClasses')}
        />
      </FormControl>
      <Button
        className="esmoDiagram__EditButton"
        variant="contained"
        onClick={redraw}
        startIcon={<RestoreIcon />}
      >
        {translate('diagramUndoChanges')}
      </Button>
      <Button
        className="esmoDiagram__EditButton"
        variant="contained"
        onClick={clearLayout}
        startIcon={<AutoFixHigh />}
      >
        {translate('diagramLayout')}
      </Button>
      {isPrototype ? null : (
        <>
          <Button
            className="esmoDiagram__EditButton"
            variant="contained"
            onClick={() => {
              setRemoveDialogOpen(true);
            }}
            startIcon={<ClearIcon />}
          >
            {translate('remove')}
          </Button>
          {removeDialogOpen ? (
            <RemoveEntryDialog
              entry={diagramEntry}
              closeDialog={() => setRemoveDialogOpen(false)}
              removeConfirmMessage={
                diagramEntryId === DEFAULT_DIAGRAM_ID
                  ? translate('defaultDiagramRemoveMessage')
                  : translate('diagramRemoveMessage')
              }
              onRemoveCallback={diagramRemoved}
            />
          ) : null}
        </>
      )}
      <Button
        className="esmoDiagram__EditButton"
        variant="contained"
        onClick={() =>
          runOnSave(onSave().then(() => addSnackbar({ type: SUCCESS_EDIT })))
        }
        startIcon={<SaveIcon />}
      >
        {translate('save')}
      </Button>
    </div>
  );
};

DiagramEditPanel.propTypes = {
  graph: PropTypes.shape({
    getLinks: PropTypes.func,
    getElements: PropTypes.func,
  }),
  paper: PropTypes.shape({
    freeze: PropTypes.func,
    unfreeze: PropTypes.func,
    updateViews: PropTypes.func,
  }),
  onRemove: PropTypes.func,
};

export default DiagramEditPanel;
