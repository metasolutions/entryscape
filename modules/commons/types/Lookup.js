import { entrystore } from 'commons/store';
import { Context } from '@entryscape/entrystore-js';
import { itemStore } from 'commons/rdforms/itemstore';
import config from 'config';
import { getModuleNameOfCurrentView } from 'commons/util/site';
import { LookupStore } from 'entitytype-lookup';
import {
  findModuleNameFromContext,
  findModuleNameFromContextEntry,
} from 'commons/util/module';

const Lookup = new LookupStore();

/**
 *
 * @param {Context} context
 * @param {boolean} direct
 * @returns {string|Promise<string>}
 */
const getCurrentModule = (context, direct = true) => {
  // TODO: always use findModuleNameFromContext
  const moduleName = getModuleNameOfCurrentView(context);

  // If current module is search, the module must be read from context.
  // Otherwise entity type is gonna be matched with search module.
  if (moduleName === 'search' && context) {
    const contextEntry = context.getEntry(true);
    return findModuleNameFromContextEntry(contextEntry);
  }
  if (direct) return moduleName;
  return findModuleNameFromContext(context);
};

export const init = () => {
  const entityTypeConfigs = config.get('entitytypes');
  const projectTypeConfigs = config.get('projecttypes');

  Lookup.init({
    entityTypeConfigs,
    projectTypeConfigs,
    entryStore: entrystore,
    itemStore,
    getCurrentModule,
  });
};

export default Lookup;
