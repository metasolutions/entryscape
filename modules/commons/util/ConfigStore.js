import merge from 'commons/merge';
import { cloneDeep, get } from 'lodash-es';

class ConfigurationStore {
  constructor(configurations) {
    this._store = configurations ? merge(...configurations) : {};
  }

  get(path, defaultValue) {
    return path ? get(this._store, path, defaultValue) : { ...this._store };
  }

  add(configuration) {
    this._store = merge(this._store, configuration);
  }

  set(configurations) {
    const clonedConfigurations = configurations.map(cloneDeep);
    this._store = merge(...clonedConfigurations);
  }
}

export default ConfigurationStore;
