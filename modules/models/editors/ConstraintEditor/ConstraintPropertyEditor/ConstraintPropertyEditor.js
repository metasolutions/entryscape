import { useState } from 'react';
import PropTypes from 'prop-types';
import { entryPropType, graphPropType } from 'commons/util/entry';
import { useESContext } from 'commons/hooks/useESContext';
import useGetNamespaceEntries from 'models/namespaces/hooks/useGetNamespaceEntries';
import {
  NAMESPACE_ENTRY,
  RESOURCE_ENTRY,
  getURIEditorType,
} from 'models/utils/metadata';
import {
  RDF_TYPE_PROPERTY,
  RDF_PROPERTY_CONSTRAINT_PROPERTY,
} from 'models/utils/ns';
import {
  nlsBundlesPropType,
  useTranslation,
} from 'commons/hooks/useTranslation';
import { getLabel } from 'commons/util/rdfUtils';
import { NamespaceEntryEditor } from 'models/editors/NamespaceEntryEditor';
import { ResourceEntryEditor } from 'models/editors/ResourceEntryEditor';
import { ResourceEditor } from 'models/editors/ResourceEditor';
import { useCustomDispatch } from 'commons/components/forms/editors';

const ConstraintPropertyEditor = ({
  graph,
  resourceURI,
  namespaceEntries,
  property,
  dispatch,
  onChange,
  editChoices,
  ...props
}) => {
  const translate = useTranslation(props.nlsBundles);
  const [editMode, setEditMode] = useState(
    getURIEditorType(
      graph,
      resourceURI,
      RDF_PROPERTY_CONSTRAINT_PROPERTY,
      RESOURCE_ENTRY
    )
  );

  const getEditorComponent = () => {
    if (editMode === RESOURCE_ENTRY) return ResourceEntryEditor;
    if (editMode === NAMESPACE_ENTRY) return NamespaceEntryEditor;
    return ResourceEditor;
  };

  const EditorComponent = getEditorComponent();

  const editModeChoices = editChoices.map(({ value, labelNlsKey }) => ({
    label: translate(labelNlsKey),
    value,
  }));

  // custom dispatch to run onChange and onRemove callbacks
  const customDispatch = useCustomDispatch({
    dispatch,
    onChange,
  });

  return (
    <EditorComponent
      graph={graph}
      resourceURI={resourceURI}
      onEditModeChange={setEditMode}
      editModeChoices={editModeChoices}
      property={property}
      dispatch={customDispatch}
      namespaceEntries={editMode === NAMESPACE_ENTRY ? namespaceEntries : null}
      {...(editMode === RESOURCE_ENTRY
        ? {
            editModeValue: RESOURCE_ENTRY,
            resourceRdfType: RDF_TYPE_PROPERTY,
            selectTooltipNlsKey: 'selectPropertyLabel',
            placeholderProps: { label: translate('noPropertiesWarning') },
            getTitleProps: ({ entry }) => ({
              primary: getLabel(entry),
              secondary: entry.getResourceURI(),
            }),
          }
        : {})}
      {...props}
    />
  );
};

ConstraintPropertyEditor.propTypes = {
  property: PropTypes.string,
  graph: graphPropType,
  resourceURI: PropTypes.string,
  namespaceEntries: PropTypes.arrayOf(entryPropType),
  nlsBundles: nlsBundlesPropType,
  editChoices: PropTypes.arrayOf(PropTypes.shape({})),
  dispatch: PropTypes.func,
  onChange: PropTypes.func,
};

const ConstraintPropertyEditorWithGetNamespaceEntries = (props) => {
  const { context } = useESContext();
  const { namespaceEntries, status } = useGetNamespaceEntries(context);
  return status === 'resolved' ? (
    <ConstraintPropertyEditor namespaceEntries={namespaceEntries} {...props} />
  ) : null;
};

export { ConstraintPropertyEditorWithGetNamespaceEntries as ConstraintPropertyEditor };
