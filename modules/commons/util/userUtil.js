/**
 * Retrieves user's custom privacy and terms version settings
 * @param {string} currentPrivacyVersion - The current privacy policy version
 * @param {string} currentTermsVersion - The current terms and conditions version
 * @return {boolean[]} Is privacy accepted? Are terms accepted?
 */
const privacyPolicyAndTermsAccepted = (
  userEntry,
  currentPrivacyVersion,
  currentTermsVersion
) => {
  const user = userEntry.getResource(true);
  const {
    privacyversion: acceptedPrivacyVersion,
    termsversion: acceptedTermsVersion,
  } = user.getCustomProperties();

  return [
    // Not doing strict equality comparison since the version can be either a string or a number
    // eslint-disable-next-line eqeqeq
    acceptedPrivacyVersion == currentPrivacyVersion,
    // eslint-disable-next-line eqeqeq
    acceptedTermsVersion == currentTermsVersion,
  ];
};

const hasAcceptedSomePrivacy = (userEntry) => {
  const user = userEntry.getResource(true);
  const customProperties = user.getCustomProperties();
  return (
    'privacyversion' in customProperties && customProperties.privacyversion
  );
};

const hasAcceptedSomeTerms = (userEntry) => {
  const user = userEntry.getResource(true);
  const customProperties = user.getCustomProperties();
  return 'termsversion' in customProperties && customProperties.termsversion;
};

/**
 * Saves the privacy policy version that the user has agreed to in the custom properties
 * @param {Object} userEntry
 * @param {string} version - The version the user last agreed to
 */
const setPrivacyPolicyVersion = (userEntry, version) => {
  const user = userEntry.getResource(true);
  user.setCustomProperties({
    ...user.getCustomProperties(),
    privacyversion: version.toString(),
    policysettime: new Date().getTime().toString(),
  });
};

/**
 * Saves the TC version that the user has agreed to in the custom properties
 * @param {Object} userEntry
 * @param {string} version - The version the user last agreed to
 */
const setTermsVersion = (userEntry, version) => {
  const user = userEntry.getResource(true);
  user.setCustomProperties({
    ...user.getCustomProperties(),
    termsversion: version.toString(),
    termssettime: new Date().getTime().toString(),
  });
};

const setTermsAndPrivacy = (userEntry, termsVersion, privacyVersion) => {
  const user = userEntry.getResource(true);
  const time = new Date().getTime().toString();
  user.setCustomProperties({
    ...user.getCustomProperties(),
    termsversion: termsVersion.toString(),
    termssettime: time,
    privacyversion: privacyVersion.toString(),
    policysettime: time,
  });
};

export default {
  privacyPolicyAndTermsAccepted,
  setPrivacyPolicyVersion,
  setTermsVersion,
  hasAcceptedSomePrivacy,
  hasAcceptedSomeTerms,
  setTermsAndPrivacy,
};
