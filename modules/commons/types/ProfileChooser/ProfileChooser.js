import { useState } from 'react';
import PropTypes from 'prop-types';
import { Button } from '@mui/material';
import { Settings as SettingsIcon } from '@mui/icons-material';
import Tooltip from 'commons/components/common/Tooltip';
import ProfileChooserDialog from 'commons/types/ProfileChooserDialog';
import escoRdformsNLS from 'commons/nls/escoRdforms.nls';
import { useTranslation } from 'commons/hooks/useTranslation';
import './ProfileChooser.scss';

const ProfileChooser = ({
  entityTypes,
  selectedEntityType,
  onChangeEntityType,
}) => {
  const t = useTranslation(escoRdformsNLS);
  const [dialogOpen, setDialogOpen] = useState(false);

  return (
    <>
      <Tooltip title={t('entityChooserTooltip')}>
        <Button
          className="escoProfileChooserButton"
          variant="outlined"
          startIcon={<SettingsIcon />}
          color="secondary"
          onClick={() => setDialogOpen(true)}
        >
          {t('entityChooserLabel')}
        </Button>
      </Tooltip>
      <ProfileChooserDialog
        open={dialogOpen}
        onClose={() => setDialogOpen(false)}
        entityTypes={entityTypes}
        selectedId={selectedEntityType?.getId()}
        onChangeEntityType={onChangeEntityType}
      />
    </>
  );
};

ProfileChooser.propTypes = {
  entityTypes: PropTypes.arrayOf(PropTypes.shape({})),
  selectedEntityType: PropTypes.shape({ getId: PropTypes.func }),
  onChangeEntityType: PropTypes.func,
};

export default ProfileChooser;
