import { useCallback } from 'react';
import { entrystore } from 'commons/store';
import Lookup from 'commons/types/Lookup';
import TableView from 'commons/components/TableView';
import dataServicesNLS from 'catalog/nls/escaServices.nls';
import { useESContext } from 'commons/hooks/useESContext';
import escoListNLS from 'commons/nls/escoList.nls';
import { getPathFromViewName } from 'commons/util/site';
import { RDF_TYPE_DATASERVICE } from 'commons/util/entry';
import {
  EntryListView,
  useSolrQuery,
  TITLE_COLUMN,
  MODIFIED_COLUMN,
  ACTIONS_GROUP_COLUMN,
  TOGGLE_ENTRY_COLUMN,
} from 'commons/components/EntryListView';
import {
  LIST_ACTION_INFO,
  LIST_ACTION_EDIT,
} from 'commons/components/EntryListView/actions';
import {
  useListModel,
  withListModelProviderAndLocation,
  TOGGLE_TABLEVIEW,
  listPropsPropType,
} from 'commons/components/ListView';
import { HVD_FILTER } from 'commons/components/filters/utils/filterDefinitions';
import useSearchFilters from 'commons/components/filters/hooks/useSearchFilters';
import { listActions } from './actions';

const FILTERS = [HVD_FILTER];

const DataServicesView = ({ listProps }) => {
  const nlsBundles = [dataServicesNLS, escoListNLS];
  const [{ showTableView }, dispatch] = useListModel();
  const { context } = useESContext();

  const { applyFilters, isLoading, hasFilters, ...filterProps } =
    useSearchFilters(FILTERS);
  const createQuery = useCallback(
    () =>
      entrystore.newSolrQuery().rdfType(RDF_TYPE_DATASERVICE).context(context),
    [context]
  );
  const queryResults = useSolrQuery({ createQuery, applyFilters });

  return showTableView ? (
    <TableView
      setTableView={() => dispatch({ type: TOGGLE_TABLEVIEW })}
      rdfType={RDF_TYPE_DATASERVICE}
      templateId={Lookup.getByName('dataService').complementaryTemplateId()}
      filterScheme={FILTERS}
      nlsBundles={[dataServicesNLS]}
    />
  ) : (
    <EntryListView
      {...queryResults}
      {...listProps}
      nlsBundles={nlsBundles}
      includeFilters={hasFilters}
      filtersProps={hasFilters ? filterProps : null}
      getListItemProps={({ entry }) => ({
        to: getPathFromViewName('catalog__dataservices__dataservice', {
          contextId: context.getId(),
          entryId: entry.getId(),
        }),
      })}
      columns={[
        {
          ...TOGGLE_ENTRY_COLUMN,
          getProps: ({ entry, translate }) => ({
            entry,
            publicTitle: translate('publicServiceTitle'),
            privateTitle: translate('privateServiceTitle'),
            publishDisabledTitle: translate('privateDisabledServiceTitle'),
            draftDisabledTitle: translate('draftDisabledDataserviceTitle'),
            toggleAriaLabel: translate('publishToggleAriaLabel'),
          }),
        },
        {
          ...TITLE_COLUMN,
          xs: 7,
        },
        MODIFIED_COLUMN,
        {
          ...ACTIONS_GROUP_COLUMN,
          actions: [LIST_ACTION_INFO, LIST_ACTION_EDIT],
        },
      ]}
      includeTableView
      listActions={listActions}
      listActionsProps={{ nlsBundles }}
    />
  );
};

DataServicesView.propTypes = {
  listProps: listPropsPropType,
};

export default withListModelProviderAndLocation(DataServicesView);
