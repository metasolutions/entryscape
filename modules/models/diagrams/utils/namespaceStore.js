/* eslint-disable max-len */
import { namespaces } from '@entryscape/rdfjson';
import { Context } from '@entryscape/entrystore-js';
import {
  getAllEntries,
  getNamespaceAbbreviation,
  getNamespaceUri,
  isBuiltin,
} from 'models/namespaces/utils/namespaceMetadata';

class NamespaceStore {
  constructor() {
    this._nss = {};
    this._nscounter = 0;
  }

  /**
   * Registers a namespace, both the abbreviation and its expansion.
   *
   * @param {string} ns the abbreviation, e.g. "foaf"
   * @param {string} full the expansion for the abbreviation, e.g. "http://xmlns.com/foaf/0.1/name"
   */
  add(ns, full) {
    if (typeof ns === 'string') {
      this._nss[ns] = full;
    } else if (typeof ns === 'object') {
      Object.keys(ns).forEach((nskey) => {
        this._nss[nskey] = ns[nskey];
      });
    }
  }

  _nsify(ns, expanded, localname) {
    if (!this._nss[ns]) {
      this._nss[ns] = expanded;
    }
    return {
      abbrev: ns,
      ns: expanded,
      localname,
      full: expanded + localname,
      pretty: `${ns}:${localname}`,
    };
  }

  /**
   * Returns an object that contain the following attributes:
   *
   * abbrev - the short namespace
   * ns - what the short namespace abbreviates
   * localname - the localname of the URI, given the current namespace
   * full - the original URI
   * pretty - the shortened version of the URI using the abbreviation, e.g. foaf:name
   *
   * @param {string} uri
   * @returns {{abbrev, ns, localname, full, pretty}}
   */
  nsify(uri) {
    const ens = Object.keys(this._nss).find(
      (ns) => uri.indexOf(this._nss[ns]) === 0
    );
    if (ens) {
      return this._nsify(
        ens,
        this._nss[ens],
        uri.substring(this._nss[ens].length)
      );
    }

    let slash = uri.lastIndexOf('/');
    const hash = uri.lastIndexOf('#');
    if (hash > slash) {
      slash = hash;
    }
    this._nscounter += 1;
    return this._nsify(
      `ns${this._nscounter}`,
      uri.substring(0, slash + 1),
      uri.substring(slash + 1)
    );
  }

  /**
   * Expands an abbreviated URI from the list of registered namespaces.
   *
   * @param {string} nsuri a namespaced uri like "foaf:name"
   * @returns {string} a full URI like "http://xmlns.com/foaf/0.1/name"
   */
  expand(nsuri) {
    const arr = nsuri.split(':');
    if (arr.length === 2 && this._nss.hasOwnProperty(arr[0])) {
      return this._nss[arr[0]] + arr[1];
    }
    return nsuri;
  }

  /**
   * Abbreviates all uris, if no matching namespace is found a suitable one is generated and
   * registered automatically.
   *
   * @param {string} uri for example: http://xmlns.com/foaf/0.1/name
   * @returns {string} in the form "foaf:name" or "ns1:name" if foaf would not be registered already.
   */
  shorten(uri) {
    return this.nsify(uri).pretty;
  }

  /**
   * Only abbreviates a URI if it can be matched to one of the already registered namespaces.
   *
   * @param {string} uri for example: http://xmlns.com/foaf/0.1/name
   * @returns {string} in the form "foaf:name"
   */

  shortenKnown(uri) {
    const ens = Object.keys(this._nss).find(
      (ns) => uri.indexOf(this._nss[ns]) === 0
    );
    if (ens) {
      return this._nsify(
        ens,
        this._nss[ens],
        uri.substring(this._nss[ens].length)
      ).pretty;
    }
    return uri;
  }
}

/**
 *
 * @param {object} namespaceStore
 * @param {Context} context
 */
const addNamespaceEntries = async (namespaceStore, context) => {
  const namespaceEntries = await getAllEntries(context);
  namespaceEntries.forEach((namespaceEntry) => {
    if (isBuiltin(namespaceEntry)) return;
    namespaceStore.add(
      getNamespaceAbbreviation(namespaceEntry),
      getNamespaceUri(namespaceEntry)
    );
  });
};

/**
 * Create namespace store and init with global namespaces and namespace
 * entries from current context.
 *
 * @param {Context} context
 * @returns {Promise<object>}
 */
export const createNamespaceStore = async (context) => {
  const namespaceStore = new NamespaceStore();
  namespaceStore.add(namespaces.registry()); // global namespaces
  await addNamespaceEntries(namespaceStore, context);
  return namespaceStore;
};
