import { Grid, Typography } from '@mui/material';
import PropTypes from 'prop-types';
import './OverviewHeader.scss';

const OverviewHeader = ({ header, subheader }) => {
  return (
    <>
      <Grid item className="esmoOverviewHeader">
        <Typography className="esmoOverviewHeader__heading" variant="h1">
          {header}
        </Typography>
      </Grid>
      {subheader ? (
        <Grid item className="esmoOverviewHeader">
          <Typography className="esmoOverviewHeader__subHeading" variant="h3">
            {subheader}
          </Typography>
        </Grid>
      ) : null}
    </>
  );
};

OverviewHeader.propTypes = {
  header: PropTypes.string,
  subheader: PropTypes.string,
};

export default OverviewHeader;
