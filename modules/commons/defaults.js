import { namespaces } from '@entryscape/rdfjson';
import merge from 'commons/merge';
import contentviewers from 'commons/contentview';
import App from 'commons/App';
import MaintenanceView from 'commons/components/Maintenance';
import { locale } from 'commons/locale';
import itemstore from 'commons/rdforms/itemstore';
import initEntrystore from 'commons/store/index';
import { init as initLook } from 'commons/types/Lookup';
import rdfUtils from 'commons/util/rdfUtils';
import { loadExtras } from 'commons/util/script';
import config, { getApplicationConfigs, getInstanceConfigs } from 'config';
import { APP_NODE_ID } from 'commons/util/config';
import { createRoot } from 'react-dom/client';
import registry from 'commons/registry';
import { loadSolutions } from 'commons/solutions';
import { loadFonts } from './util/loadFonts';

// init functions
const init = {
  namespaces: () => {
    namespaces.add(config.get('rdf.namespaces'));
    registry.set('namespaces', namespaces);
  },
  configs: async (siteConfig) => {
    // extract the view components from site config
    // the class component reference must be removed to avoid circular
    // dependency in config
    const viewComponents = new Map();
    const { views } = siteConfig;
    views.forEach((view) => {
      const { name, class: View } = view;
      if (!View) return;
      viewComponents.set(name, View);
      delete view.class;
    });
    registry.setViews(viewComponents);

    const applicationConfigs = getApplicationConfigs();
    const mergedApplicationConfigs = merge(...applicationConfigs, {
      site: siteConfig,
    });
    registry.setApplicationConfig(mergedApplicationConfigs);

    const instanceConfigs = getInstanceConfigs();
    const mergedInstanceConfigs = merge(...instanceConfigs);
    registry.setInstanceConfig(mergedInstanceConfigs);

    const solutions = await loadSolutions(
      registry.getInstanceConfig().solutions
    );
    registry.setSolutions(solutions);

    // the configs should be loaded in this order
    config.set([mergedApplicationConfigs, solutions, mergedInstanceConfigs]);
  },
  entrystore: initEntrystore,
  entitytypes: initLook,
  contentviewers,
  itemstore,
  rdfutils: rdfUtils, // needed?
  locale,
  extraScripts() {
    loadExtras(config.get('entryscape.extras'));
  },
  async app() {
    await loadFonts();

    const appContainer = document.getElementById(APP_NODE_ID);
    const root = createRoot(appContainer);
    const underMaintenance = config.get(
      'entryscape.maintenance.ongoing.enabled'
    );

    if (underMaintenance) {
      root.render(<MaintenanceView />);
      return;
    }

    init.extraScripts();
    root.render(<App />);
  },
};

export default async (siteConfig) => {
  await init.configs(siteConfig);
  init.namespaces();
  init.entrystore();
  init.entitytypes();
  init.contentviewers();
  init.rdfutils();
  await init.itemstore();
  init.locale();

  // initApp
  if (document.readyState === 'loading') {
    document.addEventListener('DOMContentLoaded', init.app);
  } else {
    init.app();
  }
};
