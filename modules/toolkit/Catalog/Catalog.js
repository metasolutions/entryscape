import { useState, useEffect, useRef, useCallback } from 'react';
import {
  AppBar,
  Button,
  Tabs,
  Tab,
  TextField,
  Typography,
} from '@mui/material';
import { converters } from '@entryscape/rdfjson';
import { useTranslation } from 'commons/hooks/useTranslation';
import { useClipboardGraph } from 'commons/contexts/ClipboardGraphContext';
import esreSourceNLS from 'toolkit/nls/esreSource.nls';
import LoadingButton from 'commons/components/LoadingButton';
import { Link } from 'react-router-dom';
import { saveAs } from 'file-saver/FileSaver';
import { useUserState } from 'commons/hooks/useUser';
import { USER_ENTRY_ID_GUEST } from 'commons/util/userIds';
import TabPanel from 'toolkit/Catalog/TabPanel';
import UploadDialog from 'toolkit/Catalog/UploadDialog';
import useAsync from 'commons/hooks/useAsync';
import config from 'config';
import { ErrorCatcher } from 'commons/errors/ErrorCatcher';
import configUtil from 'commons/util/config';
import withLocationChangeListener from '../LocationChangeListener';
import './Catalog.scss';

/**
 * @returns {string}
 */
const getExampleURL = () => {
  const examplePathConfig = config.get('registry.toolkitExamplePath');
  const baseURL =
    examplePathConfig || DEVELOPMENT
      ? configUtil.getBaseUrl()
      : configUtil.getStaticBuildPath();
  const path = examplePathConfig || 'assets/toolkitExample.rdf';

  return new URL(path, baseURL).href;
};

const loadRdfFile = () => {
  const exampleURL = getExampleURL();
  return fetch(exampleURL)
    .then((response) => {
      if (response.ok) {
        return response.text();
      }
      throw Error("A problem occured while loading the toolkit's example");
    })
    .catch((error) => {
      return Promise.reject(error);
    });
};

const Source = () => {
  const [selectedTab, setSelectedTab] = useState(0); // 0: xml, 1: json
  const [graphValue, setGraphValue] = useState(null);
  const graphValueRef = useRef();
  const [rdfXMLValue, setRdfXMLValue] = useState('');
  const [rdfJSONValue, setRdfJSONValue] = useState('');
  const [readOnly, setReadOnly] = useState(false);
  const [error, setError] = useState(null);
  const { userInfo } = useUserState();
  const isGuest = userInfo.entryId === USER_ENTRY_ID_GUEST;
  const [clipboardGraph, setClipboardGraph] = useClipboardGraph();
  const [openLoadDialog, setOpenLoadDialog] = useState(false);
  const maxInputLength = 100000;
  const xmlLabel = 'RDF/XML';
  const jsonLabel = 'RDF/JSON';
  const t = useTranslation(esreSourceNLS);
  const readOnlyInfo = readOnly ? t('readOnlyInfo') : null;
  const isMounted = useRef(false);
  const { runAsync, error: loadExampleError } = useAsync({ data: null });

  const setRDFXML = useCallback(
    async (graph) => {
      let xmlValue = await converters.rdfjson2rdfxml(graph);
      const isInputLarge = xmlValue.length > maxInputLength;

      if (isInputLarge) {
        xmlValue = `${xmlValue.substring(0, maxInputLength)}\n    ----- \n ${t(
          'truncatingRDF'
        )} \n   ------`;
      }

      setReadOnly(isInputLarge);
      setRdfXMLValue(xmlValue);
    },
    [t]
  );

  const detectGraph = async (rdf) => {
    const report = await converters.detect(rdf);
    setError(report.error);

    if (report.graph) {
      setGraphValue(report.graph);
    }

    if (report.format) {
      setSelectedTab(report.format === 'rdf/xml' ? 0 : 1);
    }
  };

  /**
   * Loads initial graph and xml values. If clipboard graph is available, that value will be
   * used to initialize. If not available, defaults will be used.
   */
  useEffect(() => {
    if (isMounted.current) return; // make sure rdf xml is not reinitialized
    isMounted.current = true;
    if (clipboardGraph) {
      setRDFXML(clipboardGraph);
      setGraphValue(clipboardGraph);
    } else {
      const rdfDefault =
        '<?xml version="1.0"?>\n' +
        '<rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">\n' +
        '</rdf:RDF>';
      setRdfXMLValue(rdfDefault);
      detectGraph(rdfDefault);
    }
  }, [clipboardGraph, setClipboardGraph, setRDFXML]);

  useEffect(() => {
    if (graphValue) {
      graphValueRef.current = graphValue;
      setClipboardGraph(graphValue);
    }
  }, [graphValue, setClipboardGraph]);

  const setRDFJSON = (graph) => {
    let jsonValue = JSON.stringify(graph.exportRDFJSON(), 0, 2);
    const isInputLarge = jsonValue.length > maxInputLength;

    if (isInputLarge) {
      jsonValue = `${jsonValue.substring(0, maxInputLength)}\n    ----- \n ${t(
        'truncatingRDF'
      )} \n   ------`;
    }

    setReadOnly(isInputLarge);
    setRdfJSONValue(jsonValue);
  };

  const loadExample = async () => {
    const exampleRDF = await loadRdfFile();
    const report = await converters.detect(exampleRDF);

    setError(report.error);
    if (report.graph) {
      setGraphValue(report.graph);
      setRDFXML(report.graph);
      setRDFJSON(report.graph);
    }
  };

  const download = async () => {
    const rdfxml = await converters.rdfjson2rdfxml(graphValue);
    const blob = new Blob([rdfxml], {
      type: 'application/rdf+xml;charset=utf-8',
    });

    saveAs(blob, 'catalog.rdf', true);
  };

  const handleTabChange = (event, newValue) => {
    setSelectedTab(newValue);
    if (graphValue) {
      setRDFXML(graphValue);
      setRDFJSON(graphValue);
    }
  };

  const handleXMLChange = (event) => {
    const newXMLInput = event.target.value;
    setRdfXMLValue(newXMLInput);
    detectGraph(newXMLInput);
  };

  const handleJSONChange = (event) => {
    const newJSONInput = event.target.value;
    setRdfJSONValue(newJSONInput);

    try {
      const parsedJSONInput = JSON.parse(newJSONInput);
      detectGraph(parsedJSONInput);
    } catch (_error) {
      setError(t('invalidJSONError'));
    }
  };

  const commonTextAreaStaticProps = {
    multiline: true,
    rows: 24,
    variant: 'outlined',
    InputProps: {
      classes: { multiline: 'esreSource__textArea' },
    },
    margin: 'none',
  };

  const tabA11yProps = (index) => {
    return {
      id: `tab-${index}`,
      'aria-controls': `tabpanel-${index}`,
    };
  };

  return (
    <>
      <Typography variant="h2" classes={{ root: 'esreSource__header' }}>
        {t('header')}
      </Typography>
      <AppBar
        position="static"
        classes={{ root: 'esreSource__AppBar' }}
        elevation={0}
      >
        <Tabs
          value={selectedTab}
          onChange={handleTabChange}
          aria-label={t('tabsAriaLabel')}
          classes={{
            root: 'esreSource__Tabs',
            indicator: 'esreSource__Tabs--selected',
          }}
        >
          <Tab
            label={xmlLabel}
            {...tabA11yProps(0)}
            classes={{
              root: 'esreSource__Tab esreSource__Tab--first',
              selected: 'esreSource__Tab--selected',
            }}
            disabled={error && selectedTab !== 0}
          />
          <Tab
            label={jsonLabel}
            {...tabA11yProps(1)}
            classes={{
              root: 'esreSource__Tab',
              selected: 'esreSource__Tab--selected',
            }}
            disabled={error && selectedTab !== 1}
          />
        </Tabs>
        <Button
          className="esreSource__TabBarButton"
          variant="text"
          onClick={() => runAsync(loadExample())}
        >
          {t('example')}
        </Button>
        <Button
          className="esreSource__TabBarButton"
          disabled={isGuest}
          onClick={() => setOpenLoadDialog(true)}
        >
          {t('upload')}
        </Button>
      </AppBar>
      <TabPanel value={selectedTab} index={0} className="esreSource__TabPanel">
        <TextField
          value={rdfXMLValue}
          onChange={handleXMLChange}
          error={!!error}
          helperText={error || readOnlyInfo}
          inputProps={{
            'aria-label': xmlLabel,
            readOnly,
          }}
          {...commonTextAreaStaticProps}
        />
      </TabPanel>
      <TabPanel value={selectedTab} index={1} className="esreSource__TabPanel">
        <TextField
          value={rdfJSONValue}
          onChange={handleJSONChange}
          error={!!error}
          helperText={error || readOnlyInfo}
          inputProps={{
            'aria-label': jsonLabel,
            readOnly,
          }}
          {...commonTextAreaStaticProps}
        />
      </TabPanel>

      <div className="esreSource__actionBar">
        <Button disabled={!!error} onClick={download}>
          {t('download')}
        </Button>
        <LoadingButton
          color="secondary"
          fullWidth={false}
          success={error ? false : null}
          className={`esreSource__actionBarSecondaryButton ${
            error ? 'esreSource__LoadingButton--failure' : ''
          }`}
          component={Link}
          to="/toolkit/validate"
        >
          {t('validationButtonLabel')}
        </LoadingButton>
      </div>
      <UploadDialog
        open={openLoadDialog}
        setOpen={setOpenLoadDialog}
        setGraph={setGraphValue}
        setRDFXML={setRDFXML}
        setEditorTab={setSelectedTab}
      />
      <ErrorCatcher error={loadExampleError} />
    </>
  );
};

export default withLocationChangeListener(Source);
