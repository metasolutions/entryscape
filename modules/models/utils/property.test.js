import { createContext } from '@test-utils/entry';
import { BASE_URI } from '@test-utils/constants';
import {
  RDF_PROPERTY_NAMESPACE_ENTRY,
  RDF_PROPERTY_PROPERTY,
  RDF_PROPERTY_RESOURCE_ENTRY,
} from './ns';
import {
  RESOURCE,
  RESOURCE_ENTRY,
  NAMESPACE_ENTRY,
  getURIEditorType,
} from './metadata';

const createResourceGraph = (entry) => {
  const graph = entry.getMetadata();
  const resourceURI = entry.getResourceURI();
  graph.add(resourceURI, RDF_PROPERTY_PROPERTY, 'http://example.com');
  return { graph, resourceURI };
};

const createResourceEntryGraph = (entry) => {
  const graph = entry.getMetadata();
  const resourceURI = entry.getResourceURI();
  const blankNodeId = graph.add(resourceURI, RDF_PROPERTY_PROPERTY).getValue();
  graph.add(blankNodeId, RDF_PROPERTY_RESOURCE_ENTRY, `${BASE_URI}1/entry/1`);
  return { graph, resourceURI };
};

const createNamespaceEntryGraph = (entry) => {
  const graph = entry.getMetadata();
  const resourceURI = entry.getResourceURI();
  const blankNodeId = graph.add(resourceURI, RDF_PROPERTY_PROPERTY).getValue();
  graph.add(blankNodeId, RDF_PROPERTY_NAMESPACE_ENTRY, `${BASE_URI}1/entry/1`);
  return { graph, resourceURI };
};

describe('property metadata util functions', () => {
  describe('getPropertyType', () => {
    test('detects resource when in graph', () => {
      const context = createContext();
      const entry = context.newEntry(context);
      const { graph, resourceURI } = createResourceGraph(entry);
      expect(
        getURIEditorType(
          graph,
          resourceURI,
          RDF_PROPERTY_PROPERTY,
          RESOURCE_ENTRY
        )
      ).toBe(RESOURCE);
    });

    test('detects namespaceEntry when in graph', () => {
      const context = createContext();
      const entry = context.newEntry(context);
      const { graph, resourceURI } = createNamespaceEntryGraph(entry);
      expect(
        getURIEditorType(
          graph,
          resourceURI,
          RDF_PROPERTY_PROPERTY,
          RESOURCE_ENTRY
        )
      ).toBe(NAMESPACE_ENTRY);
    });

    test('detects resourceEntry when in graph', () => {
      const context = createContext();
      const entry = context.newEntry(context);
      const { graph, resourceURI } = createResourceEntryGraph(entry);
      expect(
        getURIEditorType(
          graph,
          resourceURI,
          RDF_PROPERTY_PROPERTY,
          RESOURCE_ENTRY
        )
      ).toBe(RESOURCE_ENTRY);
    });

    test('gets resourceEntry as default', () => {
      const context = createContext();
      const entry = context.newEntry(context);
      expect(
        getURIEditorType(
          entry.getMetadata(),
          entry.getResourceURI(),
          RDF_PROPERTY_PROPERTY,
          RESOURCE_ENTRY
        )
      ).toBe(RESOURCE_ENTRY);
    });
  });
});
