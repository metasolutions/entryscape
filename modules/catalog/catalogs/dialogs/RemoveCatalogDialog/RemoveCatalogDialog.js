import { useEffect } from 'react';
import { useUserState } from 'commons/hooks/useUser';
import useGetMainDialog from 'commons/hooks/useGetMainDialog';
import useAsyncCallback from 'commons/hooks/useAsyncCallback';
import { useTranslation } from 'commons/hooks/useTranslation';
import escaCatalogNLS from 'catalog/nls/escaCatalog.nls';
import { getApiDistributions } from 'catalog/catalogs/utils/catalog';
import { deleteContext } from 'commons/util/context';
import { entryPropType } from 'commons/util/entry';
import PropTypes from 'prop-types';
import { useSnackbar, ERROR } from 'commons/hooks/useSnackbar';

const RemoveCatalogDialog = ({
  entry: catalogEntry,
  closeDialog,
  onRemove,
}) => {
  const { getConfirmationDialog, getAcknowledgeDialog } = useGetMainDialog();
  const translate = useTranslation(escaCatalogNLS);
  const { userEntry } = useUserState();
  const context = catalogEntry.getContext();
  const [apiDistributions] = useAsyncCallback(getApiDistributions, context);
  const [addSnackbar] = useSnackbar();

  useEffect(() => {
    if (!apiDistributions) {
      return;
    }

    if (apiDistributions.length) {
      getAcknowledgeDialog({
        content: translate('confirmRemoveCatalogWithApiDistributions'),
      }).then(() => closeDialog());
      return;
    }

    getConfirmationDialog({
      content: translate('confirmRemoveCatalog'),
    })
      .then((proceed) => {
        if (!proceed) {
          return;
        }
        deleteContext(catalogEntry, userEntry).then(() => {
          addSnackbar({ message: translate('removeSnackbarMessage') });
          onRemove();
        });
      })
      .catch((err) => {
        console.error(err);
        addSnackbar({
          message: translate('removeFailSnackbarMessage'),
          type: ERROR,
        });
      })
      .finally(() => {
        closeDialog();
      });
  }, [
    apiDistributions,
    catalogEntry,
    closeDialog,
    context,
    onRemove,
    getAcknowledgeDialog,
    getConfirmationDialog,
    translate,
    userEntry,
    addSnackbar,
  ]);

  return null;
};

RemoveCatalogDialog.propTypes = {
  entry: entryPropType,
  closeDialog: PropTypes.func,
  onRemove: PropTypes.func,
};

export default RemoveCatalogDialog;
