import PropTypes from 'prop-types';
import { graphPropType } from 'commons/util/entry';
import { Fields } from 'commons/components/forms/editors/Fields/Fields';
import { Field } from 'commons/components/forms/editors/Field/Field';
import { LITERAL, useFieldsEdit } from 'commons/components/forms/editors';
import { LanguageSelect } from 'commons/components/forms/editors/LanguageSelect/LanguageSelect';

export const LanguageEditor = ({
  property,
  size,
  graph,
  resourceURI,
  dispatch,
  ...fieldsProps
}) => {
  const { fields, labelId, addField, removeField, updateValue } = useFieldsEdit(
    {
      name: fieldsProps.name,
      property,
      nodetype: LITERAL,
      graph,
      resourceURI,
      dispatch,
    }
  );

  return (
    <Fields max={1} labelId={labelId} addField={addField} {...fieldsProps}>
      {fields.map((field) => {
        return (
          <Field
            key={field.getId()}
            size={fields.length}
            onRemove={() => removeField(field)}
            onClear={() => updateValue(field, '')}
          >
            <LanguageSelect
              labelId={labelId}
              language={field.getValue()}
              updateLanguage={({ target }) => updateValue(field, target.value)}
              size={size}
            />
          </Field>
        );
      })}
    </Fields>
  );
};

LanguageEditor.propTypes = {
  property: PropTypes.string,
  xs: PropTypes.number,
  size: PropTypes.string,
  graph: graphPropType,
  resourceURI: PropTypes.string,
  dispatch: PropTypes.func,
};
