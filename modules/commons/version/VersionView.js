import Branches from './components/branches';
import Versions from './components/versions';

export default () => {
  return (
    <div>
      <Versions />
      <Branches />
    </div>
  );
};
