import {
  LITERAL,
  DATATYPE_LITERAL,
  LANGUAGE_LITERAL,
  URI,
  BLANK,
  ONLY_LITERAL,
} from 'commons/components/forms/utils/nodetypes';
import { validateField } from './validate';
import { BaseField } from './BaseField';

/**
 *
 * @param {string} nodetype
 * @returns {string}
 */
const getTypeFromNodetype = (nodetype) => {
  switch (nodetype) {
    case DATATYPE_LITERAL:
    case LANGUAGE_LITERAL:
    case LITERAL:
    case ONLY_LITERAL: {
      return 'literal';
    }
    case URI: {
      return 'uri';
    }
    case BLANK: {
      return 'bnode';
    }
    default:
      throw new Error(`Unknow nodetype ${nodetype}`);
  }
};

/**
 *
 * @param {object} statementProperties
 * @returns {object}
 */
const createStatement = ({
  graph,
  resourceURI,
  property,
  datatype,
  language,
  nodetype,
  initialValue,
}) => {
  const type = getTypeFromNodetype(nodetype);
  const statementObject =
    nodetype !== BLANK
      ? {
          value: initialValue || '',
          type,
          ...(datatype ? { datatype } : {}),
          ...(nodetype === LANGUAGE_LITERAL ? { lang: language } : {}),
        }
      : null;
  const statement = graph.create(
    resourceURI,
    property,
    statementObject,
    (initialValue !== undefined && initialValue !== null) || false
  );
  return statement;
};

export class Field extends BaseField {
  constructor({
    statement,
    validate = validateField,
    graph,
    resourceURI,
    property,
    language,
    nodetype,
    datatype,
    mandatory,
    initialValue,
  }) {
    super({ graph, resourceURI, validate, property, nodetype, mandatory });
    this._statement =
      statement ||
      createStatement({
        datatype,
        graph,
        resourceURI,
        property,
        language,
        nodetype,
        initialValue,
      });
  }

  getValue() {
    return this._statement.getValue();
  }

  setValue(value) {
    if (value) {
      this._statement.setValue(value);
    }
    if (value && !this._statement.isAsserted()) {
      this._statement.setAsserted(true);
    }
    if (!value) {
      this._statement.setValue('');
      this._statement.setAsserted(false);
    }
  }

  clear() {
    this.setValue('');
  }

  remove() {
    if (this._statement) {
      this._graph.remove(this._statement);
    }
  }

  getLanguage() {
    return this._statement.getLanguage();
  }

  setLanguage(language) {
    this._statement.setLanguage(language);
  }
}
