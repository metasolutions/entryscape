import React from 'react';
import PropTypes from 'prop-types';
import './style.scss';
import IndexCard from 'commons/components/common/card/IndexCard';
import {
  DescriptionItem,
  DescriptionBlock,
} from 'commons/overview/DescriptionItem/index';
import DescriptionList from '../DescriptionList';

const Overview = ({ modsList, statsList, description = '' }) => {
  return (
    <div className="escoOverview">
      <DescriptionList>
        {description.length ? (
          <DescriptionBlock description={description} />
        ) : null}
        <div className="escoDescriptionList__modification">
          {modsList.map((modification, i) => (
            <DescriptionItem key={i} {...modification} />
          ))}
        </div>
      </DescriptionList>
      <div className="escoOverview__cards">
        {statsList
          .filter(({ isVisible }) => isVisible)
          .map((statistic) => (
            <IndexCard key={statistic.key} {...statistic} />
          ))}
      </div>
    </div>
  );
};

Overview.propTypes = {
  modsList: PropTypes.arrayOf(PropTypes.object).isRequired,
  statsList: PropTypes.arrayOf(PropTypes.object).isRequired,
  description: PropTypes.string,
};

export default Overview;
