import PropTypes from 'prop-types';
import { useParams } from 'react-router-dom';
import useNavigate from 'commons/components/router/useNavigate';
import { entryPropType } from 'commons/util/entry';
import { getPathFromViewName } from 'commons/util/site';
import RemoveEntryDialog from 'commons/components/entry/RemoveEntryDialog';
import useCheckReferences from 'commons/hooks/references/useCheckReferences';
import { nlsBundlesPropType } from 'commons/hooks/useTranslation';
import { RESOLVED } from 'commons/hooks/useAsync';

const RemoveEntityDialog = ({
  entry,
  closeDialog,
  parentViewName,
  nlsBundles,
  ...restProps
}) => {
  const { navigate } = useNavigate();
  const viewParams = useParams();
  const collectionsPath = getPathFromViewName(parentViewName, viewParams);

  const { references, checkReferencesStatus } = useCheckReferences({
    entry,
    nlsBundles,
    callback: closeDialog,
  });

  if (checkReferencesStatus !== RESOLVED || references.length !== 0)
    return null;

  return (
    <RemoveEntryDialog
      entry={entry}
      closeDialog={closeDialog}
      onRemoveCallback={() => navigate(collectionsPath)}
      nlsBundles={nlsBundles}
      {...restProps}
    />
  );
};

RemoveEntityDialog.propTypes = {
  entry: entryPropType,
  closeDialog: PropTypes.func,
  parentViewName: PropTypes.string,
  nlsBundles: nlsBundlesPropType,
};

export default RemoveEntityDialog;
