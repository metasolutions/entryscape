import { entrystore } from 'commons/store';
import { Context, Entry } from '@entryscape/entrystore-js';
import config from 'config';
import { RDF_TYPE_CONCEPT, RDF_TYPE_CONCEPT_SCHEME } from 'commons/util/entry';
import skosUtil from 'commons/tree/skos/util';
import TreeModel from 'commons/tree/TreeModel';
import { getLabel } from 'commons/util/rdfUtils';
import {
  constructURIFromPattern,
  createEntryFromEntityType,
  shouldUseUriPattern,
  createEntry,
} from 'commons/util/store';
import { setGroupIdForContext } from 'commons/util/context';
import { i18n } from 'esi18n';
import Lookup from 'commons/types/Lookup';
import { RDF_PROPERTY_PROJECTTYPE } from 'entitytype-lookup';

const cid2count = new Map();

const setConceptCount = (context, count) => {
  cid2count.set(context.getId(), count);
};

const incrementConceptCount = (context) => {
  const contextId = context.getId();
  cid2count.set(
    contextId,
    cid2count.has(contextId) ? cid2count.get(contextId) + 1 : 1
  );
};

const decrementConceptCount = (context) => {
  const contextId = context.getId();
  cid2count.set(
    contextId,
    cid2count.has(contextId) ? cid2count.get(contextId) - 1 : 0
  );
};

/**
 * Get the number of concepts belonging to a context
 *
 * @param {Entry} conceptSchemeEntry
 * @returns {Promise<number>}
 */
const getConceptCount = async (conceptSchemeEntry) => {
  const store = conceptSchemeEntry.getEntryStore();
  const contextId = conceptSchemeEntry.getContext();
  const searchList = store
    .newSolrQuery()
    .rdfType(RDF_TYPE_CONCEPT)
    .context(contextId)
    .list();
  await searchList.getEntries(0);

  return searchList.getSize();
};

/**
 * Checks whether the number of concepts has reached a configurable limit
 *
 * @param {Entry} conceptSchemeEntry
 * @param {bool} isExemptFromRestrictions
 * @returns {Promise<bool>}
 */
const withinConceptLimit = async (
  conceptSchemeEntry,
  isExemptFromRestrictions
) => {
  if (isExemptFromRestrictions) return true;

  const conceptLimit = config.get('terms.conceptLimit');
  if (!conceptLimit) return true;

  const conceptCount = await getConceptCount(conceptSchemeEntry);
  if (conceptCount < conceptLimit) return true;

  return false;
};

/**
 *
 * @param {string} projectType
 * @returns {EntityType|undefined}
 */
const getConceptSchemeEntityType = (projectType) => {
  return Lookup.getEntityTypeByConstraints(
    {
      'rdf:type': ['skos:ConceptScheme'],
    },
    projectType
  );
};

/**
 *
 * @param {string} projectType
 * @returns {EntityType|undefined}
 */
const getConceptEntityType = (projectType) => {
  return Lookup.getEntityTypeByConstraints(
    {
      'skos:inScheme': null,
      'rdf:type': 'skos:Concept',
    },
    projectType
  );
};

/**
 * Get a tree model from a concept scheme entry (in a context)
 *
 * @param {Context} context
 * @returns {Promise<TreeModel>}
 */
const getTreeModel = (context) => {
  // although this is a list view, the underlying model is still a tree
  // and we use the model for operations like delete
  return entrystore
    .newSolrQuery()
    .rdfType(RDF_TYPE_CONCEPT_SCHEME)
    .context(context)
    .limit(1)
    .getEntries(0)
    .then((entries) => {
      // this is hack, get the first (and hopefully only) concept sceheme in this context
      const conceptSchemeEntry = entries[0];
      return new TreeModel({
        ...skosUtil.getSemanticRelations(conceptSchemeEntry.getResourceURI()),
        rootEntry: conceptSchemeEntry,
      });
    });
};

/**
 * For each SKOS mapping to a conceptRURI delete those mappings and refresh entry
 *
 * @param {string} conceptRURI
 * @returns {Promise}
 */
const removeConceptMappings = async (conceptRURI) => {
  const mappingRelations = await skosUtil.getMappingRelations(conceptRURI);

  const removeMappingPromises = [];
  mappingRelations.forEach((entries, property) => {
    removeMappingPromises.push(
      entries.map((entry) => {
        entry
          .getMetadata()
          .findAndRemove(entry.getResourceURI(), property, conceptRURI);

        return entry.commitMetadata().then(() => {
          entry.setRefreshNeeded();
          return entry.refresh();
        });
      })
    );
  });

  return Promise.all(removeMappingPromises);
};

/**
 * Delete concept entry and any SKOS mappings with other concepts
 * If no treeModel is provided, one will be created via context (solr query)
 *
 * @param {Entry} entry
 * @param {Context} context
 * @param {TreeModel} treeModel
 * @returns {Promise}
 */
const removeConcept = async (entry, context, treeModel = null) => {
  const entryRURI = entry.getResourceURI(); // keep uri of entry to be deleted
  const model = treeModel || (await getTreeModel(context));

  await model.deleteEntry(entry);

  decrementConceptCount(context);

  return removeConceptMappings(entryRURI);
};

/**
 *
 * Creates the necessary entries for a new terminology:
 *  - STEP 1: create group and context
 *  - STEP 2: create concept scheme entry
 *  - STEP 3: administrative step, set context type and ACL fix
 *  - STEP 4: add project type if provided
 *
 *
 * @returns {store/Entry} conceptSchemeEntry
 */
const createTerminology = async ({
  title,
  description,
  name: alias,
  publisher,
  entityType,
  hasAdminRights,
  projectType,
}) => {
  // STEP 1: create group and context
  const name = shouldUseUriPattern(entityType) ? alias : null;
  let groupEntry;
  let context;
  try {
    groupEntry = await entrystore.createGroupAndContext(name);
    const resource = await groupEntry.getResource();
    const homeContext = resource.getHomeContext();
    context = entrystore.getContextById(homeContext);
  } catch (err) {
    console.error('Could not create group and context entry');
    throw Error(err);
  }

  // STEP 2: create concept scheme entry
  let conceptSchemeEntry;
  try {
    let prototypeEntry;
    if (name) {
      prototypeEntry = createEntryFromEntityType(context, entityType, {
        contextName: name,
      });
    } else {
      prototypeEntry = createEntry(context, 'skos:ConceptScheme');
    }

    const metadata = prototypeEntry.getMetadata();
    const prototypeEntryRURI = prototypeEntry.getResourceURI();
    const locale = i18n.getLocale();
    metadata.add(prototypeEntryRURI, 'rdf:type', 'skos:ConceptScheme');
    metadata.addL(prototypeEntryRURI, 'dcterms:title', title, locale);
    if (description) {
      metadata.addL(
        prototypeEntryRURI,
        'dcterms:description',
        description,
        locale
      );
    }
    if (!config.get('terms.createWithoutPublisher') && publisher) {
      metadata.add(prototypeEntryRURI, 'dcterms:publisher', publisher);
    }

    if (name) {
      const uriSpace = constructURIFromPattern(entityType.uriPattern, {
        contextName: name,
      });
      metadata.add(prototypeEntryRURI, 'void:uriSpace', uriSpace);
    }
    conceptSchemeEntry = await prototypeEntry.commit();
  } catch (err) {
    console.error('Could not create concept scheme entry');
    throw Error(err);
  }

  try {
    // STEP 3: administrative step, set context type and ACL fix
    const contextEntry = await context.getEntry();
    const contextEntryInfo = contextEntry.getEntryInfo();
    contextEntryInfo
      .getGraph()
      .add(
        contextEntry.getResourceURI(),
        'rdf:type',
        'esterms:TerminologyContext'
      ); // TODO remove when entrystore is changed so groups have read access
    // to homecontext metadata by default.
    // Start fix with missing metadata rights on context for group
    const acl = contextEntryInfo.getACL(true);
    acl.mread.push(groupEntry.getId());
    contextEntryInfo.setACL(acl);
    // End fix

    // step 4
    if (projectType) {
      contextEntryInfo
        .getGraph()
        .add(
          contextEntry.getResourceURI(),
          RDF_PROPERTY_PROJECTTYPE,
          projectType
        );
    }

    await contextEntryInfo.commit();
  } catch (err) {
    console.error('Failed to set context type or ACL');
    throw Error(err);
  }

  // STEP 4: Update the UI and refresh entry
  if (!hasAdminRights) {
    setGroupIdForContext(context.getId(), groupEntry.getId());
  }

  return conceptSchemeEntry;
};

/**
 *
 * @param {string} contextId
 * @returns {Promise.<Array.<{value: string, label: string}>>}
 */
const getPublishersByContext = (contextId = '') =>
  entrystore
    .newSolrQuery()
    .context(contextId)
    .rdfType(['foaf:Agent', 'foaf:Person', 'foaf:Organization'])
    .list()
    .getAllEntries()
    .then((entries) =>
      entries.map((entry) => ({
        value: entry.getResourceURI(),
        label: getLabel(entry),
      }))
    );

/**
 * Checks whether the context entry corresponds to an enhanced terminology.
 *
 * @param {Entry} contextEntry
 * @returns
 */
const isEnhancedTerminology = (contextEntry) =>
  contextEntry
    .getEntryInfo()
    .getGraph()
    .find(
      contextEntry.getResourceURI(),
      'rdf:type',
      'esterms:EnhancedTerminologyContext'
    ).length === 1;

export {
  removeConcept,
  setConceptCount,
  incrementConceptCount,
  decrementConceptCount,
  withinConceptLimit,
  createTerminology,
  getConceptSchemeEntityType,
  getConceptEntityType,
  getPublishersByContext,
  isEnhancedTerminology,
  getConceptCount,
};
