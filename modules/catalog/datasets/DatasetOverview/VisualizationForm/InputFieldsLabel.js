import PropTypes from 'prop-types';

const InputFieldsLabel = ({ label, id, mandatory = false }) => (
  <span className="escaVisualizationForm__inputLabel" id={id}>
    {label}
    {mandatory ? (
      <span className="escaVisualizationForm__fieldMarker" aria-hidden>
        *
      </span>
    ) : null}
  </span>
);

InputFieldsLabel.propTypes = {
  label: PropTypes.string,
  id: PropTypes.string,
  mandatory: PropTypes.bool,
};

export default InputFieldsLabel;
