import Slide from '@mui/material/Slide';

const SlideTransition = (props) => {
  return <Slide {...props} direction="up" />;
};

export default SlideTransition;
