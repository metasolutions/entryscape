import TerminologiesList from 'terms/terminologies/TerminologiesView/TerminologiesView';
import ConceptsView from 'terms/concepts/ConceptsView';
// import ConceptsList from 'terms/list';
import Overview from 'terms/terminologies/TerminologyOverview/TerminologyOverview';
import CollectionsView from 'terms/collections/CollectionsView';
import CollectionOverview from 'terms/collections/CollectionOverview';
import {
  TERMINOLOGY_OVERVIEW_NAME,
  COLLECTION_OVERVIEW_NAME,
  TERMINOLOGY_CONCEPTS_VIEW_NAME,
} from './config';

export default {
  modules: [
    {
      name: 'terms',
      productName: { en: 'Terms', sv: 'Terms', de: 'Terms' },
      faClass: 'sitemap',
      startView: 'terminology__scheme__list',
      sidebar: true,
    },
  ],
  views: [
    {
      name: 'terminology__scheme__list',
      class: TerminologiesList,
      title: {
        en: 'Termino\u00ADlogies',
        sv: 'Termino\u00ADlogier',
        de: 'Termino\u00ADlogien',
      },
      constructorParams: { rowClickView: 'terminology__overview' },
      faClass: 'archive',
      route: '/terms',
      module: 'terms',
    },
    // {
    //   name: 'terminology',
    //   // class: Cards,
    //   class: ReactView,
    //   labelCrumb: true,
    //   constructorParams: { entryType: 'skos:ConceptScheme' },
    //   route: '/terms/:contextId',
    //   module: 'terms',
    //   parent: 'terminology__scheme__list',
    // },
    {
      name: TERMINOLOGY_OVERVIEW_NAME,
      class: Overview,
      faClass: 'eye',
      title: { en: 'Overview', sv: 'Översikt', de: 'Überblick' },
      route: '/terms/:contextId/overview',
      parent: 'terminology__scheme__list',
      module: 'terms',
    },
    {
      name: TERMINOLOGY_CONCEPTS_VIEW_NAME,
      // class: Concepts,
      class: ConceptsView,
      faClass: 'sitemap',
      wide: false,
      title: { en: 'Concepts', sv: 'Begrepp', de: 'Begriffe' },
      route: '/terms/:contextId/concepts',
      parent: 'terminology__scheme__list',
      module: 'terms',
    },
    // {
    //   name: 'terminology__list',
    //   class: ConceptsList,
    //   faClass: 'list',
    //   title: { en: 'List', sv: 'Lista', de: 'Liste' },
    //   route: '/terms/:contextId/list',
    //   parent: 'terminology__scheme__list',
    //   module: 'terms',
    // },
    {
      name: 'terminology__collections',
      class: CollectionsView,
      faClass: 'cubes',
      title: {
        en: 'Collections',
        sv: 'Samlingar',
        de: 'Samm\u00ADlun\u00ADgen',
      },
      route: '/terms/:contextId/collections',
      parent: 'terminology__scheme__list',
      module: 'terms',
    },
    {
      name: COLLECTION_OVERVIEW_NAME,
      class: CollectionOverview,
      faClass: 'cubes',
      title: {
        en: 'Collection',
        sv: 'Samling',
        de: 'Sammlung',
      },
      route: '/terms/:contextId/collections/:entryId/overview',
      parent: 'terminology__collections',
      module: 'terms',
    },
  ],
};
