import { simpleFetchEntries } from 'commons/util/solr/entry';
import { entrystore } from 'commons/store';
import { applyEntityTypeParams } from 'commons/util/solr';
import { getQueryConfiguration } from './util';

const getQueryFromItem = (item, term, currentContext, userSelectedContext) => {
  const {
    searchLimit,
    sortFields,
    searchProps,
    explicitContext,
    contextRestriction,
    invertContextRestriction,
    primaryStore,
  } = getQueryConfiguration(item);
  const store = primaryStore || entrystore;

  const query = applyEntityTypeParams(
    store.newSolrQuery(),
    { search: term },
    {
      constraints: item.getConstraints(),
      searchProps,
    }
  );

  const limit = searchLimit || 10;
  query.limit(limit);

  if (sortFields?.length) {
    query.sort(sortFields);
  }

  if (userSelectedContext) {
    query.context(userSelectedContext);
    return [query, simpleFetchEntries, searchProps];
  }
  if (explicitContext) {
    query.context(entrystore.getContextById(explicitContext));
    return [query, simpleFetchEntries, searchProps];
  }
  if (contextRestriction && currentContext) {
    query.context(currentContext, invertContextRestriction);
  }
  return [query, simpleFetchEntries, searchProps];
};

export { getQueryFromItem };
