import RemoveEntryDialog from 'commons/components/entry/RemoveEntryDialog';
import PropTypes from 'prop-types';
import { entryPropType } from 'commons/util/entry';
import { useParams } from 'react-router-dom';
import useNavigate from 'commons/components/router/useNavigate';
import { getPathFromViewName } from 'commons/util/site';
import { useEffect } from 'react';
import escaDatasetNLS from 'catalog/nls/escaDataset.nls';
import { useTranslation } from 'commons/hooks/useTranslation';
import useAsync from 'commons/hooks/useAsync';
import ErrorBoundary from 'commons/errors/ErrorBoundary';
import DialogFallbackComponent from 'commons/errors/DialogFallbackComponent';
import { ErrorWithMessage } from 'commons/errors/utils/errors';
import useErrorHandler from 'commons/errors/hooks/useErrorHandler';
import useRemoveDataset from '../../hooks/useRemoveDataset';

const RemoveDatasetDialog = ({
  isPublished,
  datasetEntry,
  closeDialog,
  stopNavigation = false,
  onRemove,
  ...rest
}) => {
  const [checkCanRemove, removeDataset] = useRemoveDataset(datasetEntry);
  const { navigate } = useNavigate();
  const viewParams = useParams();
  const datasetsPath = getPathFromViewName('catalog__datasets', viewParams);
  const translate = useTranslation(escaDatasetNLS);
  const {
    data: canRemove,
    runAsync,
    error: checkError,
  } = useAsync({ data: false });
  useErrorHandler(checkError);

  const handleRemove = async () => {
    try {
      await removeDataset();
      await onRemove?.();
      if (!stopNavigation) navigate(datasetsPath);
    } catch (error) {
      throw new ErrorWithMessage(
        translate('failedToRemoveDatasetDistributions'),
        error
      );
    }
  };

  useEffect(() => {
    const handleCheck = async () => {
      try {
        const isRemovable = await checkCanRemove();
        if (!isRemovable) closeDialog();
        return isRemovable;
      } catch (error) {
        throw new ErrorWithMessage(translate('failedToCheckDataset'), error);
      }
    };
    runAsync(handleCheck());
  }, [checkCanRemove, closeDialog, translate, runAsync]);

  return canRemove ? (
    <ErrorBoundary
      FallbackComponent={DialogFallbackComponent}
      onReset={closeDialog}
    >
      <RemoveEntryDialog
        {...rest}
        onRemove={handleRemove}
        closeDialog={closeDialog}
      />
    </ErrorBoundary>
  ) : null;
};

RemoveDatasetDialog.propTypes = {
  closeDialog: PropTypes.func,
  isPublished: PropTypes.bool,
  stopNavigation: PropTypes.bool,
  onRemove: PropTypes.func,
  datasetEntry: entryPropType,
};

export default RemoveDatasetDialog;
