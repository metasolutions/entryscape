import Feature from 'ol/Feature';

export const createFeature = (featureOptions) => {
  return new Feature(featureOptions);
};
