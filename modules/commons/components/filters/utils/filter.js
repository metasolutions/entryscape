import { getModuleNameOfCurrentView } from 'commons/util/site';
import config from 'config';
import { ANY } from './filterDefinitions';

export default class Filter {
  constructor({
    id,
    type = 'select',
    headerNlsKey,
    items = [],
    initialSelection = ANY,
    defaultValue = ANY,
    autoHide = true,
    applyFilterParams = () => {},
    loadItems,
    onLoadItems,
    onSelect = () => {},
  }) {
    this._id = id;
    this._type = type;
    this._headerNlsKey = headerNlsKey;
    this._items = items;
    this._initial = initialSelection;
    this._defaultValue = defaultValue;
    this._selected = initialSelection;
    this._applyFilterParams = applyFilterParams;
    this._loadItems = loadItems;
    this._onSelect = onSelect;
    this._onLoadItems = onLoadItems;
    this._autoHide = autoHide;
  }

  setItems(items) {
    this._items = items;
  }

  addItems(newItems) {
    this.setItems([...this.getItems(), ...newItems]);
  }

  async loadItems() {
    if (!this._loadItems) return;
    const items = await this._loadItems();
    if (items.length) {
      this.setItems(items);
    }
    if (this._onLoadItems) {
      this._onLoadItems(items);
    }
  }

  getInitialSelection() {
    return this._initial;
  }

  getDefaultValue() {
    return this._defaultValue;
  }

  getDefaultItem() {
    return this.getItems().find(
      (item) => item.value === this.getDefaultValue()
    );
  }

  getItems() {
    return this._items;
  }

  findItem(itemValue) {
    const item = this.getItems().find(({ value }) => value === itemValue);
    return item;
  }

  getHeaderNlsKey() {
    return this._headerNlsKey;
  }

  getId() {
    return this._id;
  }

  get autoHide() {
    return this._autoHide;
  }

  getType() {
    return this._type;
  }

  setSelected(value) {
    if (this._items.find((item) => item.value === value)) {
      this._selected = value;
      this._onSelect(value);
    }
  }

  clearSelected() {
    this._selected = this._defaultValue;
    this._onSelect(this._defaultValue);
  }

  getSelected() {
    return this._selected;
  }

  hasSelected() {
    return this._selected !== this._defaultValue;
  }

  applyFilterParams(query) {
    if (this.hasSelected() === false && this.getDefaultValue() === ANY)
      return query;
    return this._applyFilterParams(query, this);
  }
}

/**
 * Decides which NLS to use depending on the input
 *
 * @param {boolean} hasSearchQuery
 * @param {boolean} hasFilters
 * @returns {string}
 */
export const getNoResultsNLS = (hasSearchQuery, hasFilters) => {
  if (hasSearchQuery)
    return hasFilters ? 'noSearchResultsFiltersAndQuery' : 'noSearchResults';
  if (hasFilters) return 'noSearchResultsFilters';
  return 'noResults';
};

/**
 * Uses the per module `excludeFilters` config to determine the included filters
 *
 * @param {object[]} filters
 * @returns {object[]}
 */
export const getIncludedFilters = (filters) => {
  const moduleName = getModuleNameOfCurrentView();
  const excludeConfig = config.get(`${moduleName}.excludeFilters`);
  const excludeFilters = Array.isArray(excludeConfig) ? false : excludeConfig;
  const excludedFilters = Array.isArray(excludeConfig) ? excludeConfig : [];

  if (excludeFilters) return [];
  if (excludedFilters.length > 0) {
    return filters.filter(({ id }) => !excludedFilters.includes(id));
  }
  return filters;
};

/**
 * Create filters that should be visible. Any initial selections are added.
 *
 * @param {object[]} filterDefinitions
 * @param {object} initialSelections
 * @returns {Filter[]}
 */
export const createFilters = (filterDefinitions = [], initialSelections) => {
  const includedFilters = getIncludedFilters(filterDefinitions);
  return includedFilters.map((filterDefinition) => {
    const filterWithSelection = {
      ...filterDefinition,
      initialSelection:
        initialSelections[filterDefinition.id] ||
        filterDefinition.initialSelection,
    };
    return new Filter(filterWithSelection);
  });
};

/**
 * Removes ANY option from filter definition
 *
 * @param {object} filterDefinition
 * @returns {object}
 */
export const removeAnyOption = (filterDefinition) => ({
  ...filterDefinition,
  items: filterDefinition.items.filter((item) => item.value !== ANY),
});
