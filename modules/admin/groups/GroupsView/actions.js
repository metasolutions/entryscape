import {
  LIST_ACTION_EDIT,
  LIST_ACTION_REVISIONS,
  LIST_ACTION_REMOVE,
  LIST_ACTION_SHARING_SETTINGS,
} from 'commons/components/EntryListView/actions';
import esadGroupNLS from 'admin/nls/esadGroup.nls';
import escoPlaceholderNLS from 'commons/nls/escoPlaceholder.nls';
import escoListNLS from 'commons/nls/escoList.nls';
import CreateDialog from '../dialogs/CreateDialog';
import RemoveDialog from '../dialogs/RemoveDialog';
import MembersDialog from '../dialogs/MembersDialog';

const LIST_ACTION_GROUPS_ID = 'groups';
const nlsBundles = [esadGroupNLS, escoPlaceholderNLS, escoListNLS];

const listActions = [
  {
    id: 'create',
    Dialog: CreateDialog,
    labelNlsKey: 'createButton',
    tooltipNlsKey: 'actionTooltip',
  },
];

const rowActions = [
  { ...LIST_ACTION_EDIT, formTemplateId: 'esc:Group' },
  LIST_ACTION_REVISIONS,
  {
    id: LIST_ACTION_SHARING_SETTINGS,
    Dialog: '',
    isVisible: () => false,
    labelNlsKey: 'groupACL',
  },
  {
    id: LIST_ACTION_GROUPS_ID,
    Dialog: MembersDialog,
    labelNlsKey: 'memberList',
  },
  { ...LIST_ACTION_REMOVE, Dialog: RemoveDialog },
];

export { listActions, rowActions, nlsBundles, LIST_ACTION_GROUPS_ID };
