import RfsItem from 'models/utils/RfsItem';
import { RDF_TYPE_FIELD } from 'models/utils/ns';
import { RFS_FIELDS_COMMON_PROPERTIES } from './rfsFields';

class RfsField extends RfsItem {
  constructor(props) {
    super({
      ...props,
      baseRdfType: RDF_TYPE_FIELD,
      commonProperties: RFS_FIELDS_COMMON_PROPERTIES,
    });
  }
}

export default RfsField;
