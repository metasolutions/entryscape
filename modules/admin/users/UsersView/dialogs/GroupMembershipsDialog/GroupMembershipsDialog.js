import { useCallback, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Entry } from '@entryscape/entrystore-js';
import ContentWrapper from 'commons/components/common/ContentWrapper';
import { entrystore } from 'commons/store';
import { getRenderName } from 'commons/util/rdfUtils';
import { USERS_ENTRY_ID } from 'commons/util/userIds';
import { useTranslation } from 'commons/hooks/useTranslation';
import ListActionDialog from 'commons/components/common/dialogs/ListActionDialog';
import {
  EntryListView,
  TITLE_COLUMN,
  TITLE_SORT,
  MODIFIED_COLUMN,
  ACTION_MENU_COLUMN,
  INFO_COLUMN,
} from 'commons/components/EntryListView';
import { LIST_ACTION_INFO } from 'commons/components/EntryListView/actions';
import useAsync from 'commons/hooks/useAsync';
import {
  useListModel,
  useListQuery,
  ListModelProvider,
  REFRESH,
} from 'commons/components/ListView';
import LinkedDataBrowserDialog from 'commons/components/LinkedDataBrowserDialog';

import {
  rowActions,
  listActions,
  nlsBundles,
  presenterAction,
} from './actions';

const GroupsList = ({ userEntry }) => {
  const { data: groupItems, runAsync, status } = useAsync();
  const [, dispatch] = useListModel();
  const userGroupIds = groupItems?.map(({ entry }) => entry.getId()) || [];
  const { size, result: groupItemsInPage } = useListQuery({
    items: groupItems,
  });
  const translate = useTranslation(nlsBundles);
  const getTitle = useCallback(
    (entry) =>
      getRenderName(entry) || translate('unnamedWorkspace', entry.getId()),
    [translate]
  );

  const getGroupItems = useCallback(async () => {
    const usersGroupURI = entrystore.getEntryURI('_principals', USERS_ENTRY_ID);
    const groupURIs = userEntry
      .getParentGroups()
      .filter((groupURI) => groupURI !== usersGroupURI);
    return Promise.all(
      groupURIs.map((groupURI) => entrystore.getEntry(groupURI))
    ).then((groupEntries) =>
      groupEntries.map((entry) => ({
        entry,
        title: getTitle(entry),
      }))
    );
  }, [userEntry, getTitle]);

  useEffect(() => {
    runAsync(getGroupItems());
  }, [runAsync, getGroupItems]);

  const updateList = () => {
    runAsync(
      getGroupItems().then((entries) => {
        dispatch({ type: REFRESH });
        return entries;
      })
    );
  };

  return (
    <EntryListView
      entries={groupItemsInPage.map(({ entry }) => entry)}
      status={status}
      size={size}
      nlsBundles={nlsBundles}
      includeDefaultListActions={false}
      listActions={listActions}
      listActionsProps={{ userEntry, updateList, userGroupIds }}
      getListItemProps={({ entry }) => ({
        action: {
          ...presenterAction,
          entry,
        },
      })}
      columns={[
        {
          ...TITLE_COLUMN,
          xs: 8,
          sortBy: TITLE_SORT,
          getProps: ({ entry }) => ({
            primary: getTitle(entry),
          }),
        },
        MODIFIED_COLUMN,
        {
          ...INFO_COLUMN,
          justifyContent: 'end',
          getProps: ({ entry, translate: t }) => ({
            action: {
              ...LIST_ACTION_INFO,
              entry,
              nlsBundles,
              formTemplateId: 'esc:Group',
              Dialog: LinkedDataBrowserDialog,
            },
            title: t('infoEntry'),
          }),
        },
        {
          ...ACTION_MENU_COLUMN,
          userEntry,
          updateList,
          actions: rowActions,
        },
      ]}
    />
  );
};

GroupsList.propTypes = {
  userEntry: PropTypes.shape().isRequired,
};

const GroupMembershipsDialog = ({ entry, closeDialog }) => {
  const t = useTranslation(nlsBundles);

  return (
    <ListActionDialog
      id="group-membership"
      closeDialog={closeDialog}
      title={t('groupHeader', { user: getRenderName(entry) })}
      closeDialogButtonLabel={t('close')}
    >
      <ContentWrapper>
        <ListModelProvider>
          <GroupsList userEntry={entry} />
        </ListModelProvider>
      </ContentWrapper>
    </ListActionDialog>
  );
};

GroupMembershipsDialog.propTypes = {
  entry: PropTypes.instanceOf(Entry),
  closeDialog: PropTypes.func.isRequired,
};

export default GroupMembershipsDialog;
