import { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { isEmpty } from 'lodash-es';
import { Card, CardContent, Link as MuiLink, Typography } from '@mui/material';
import OpenInNewIcon from '@mui/icons-material/OpenInNew';
import ThumbUpIcon from '@mui/icons-material/ThumbUp';
import DoughnutChart from 'commons/components/chart/Doughnut';
import { COLOURS_PIE_STATUS } from 'commons/components/chart/colors';
import { useTranslation } from 'commons/hooks/useTranslation';
import esreLinkCheckReportNLS from 'harvest/nls/esreLinkCheckReport.nls';
import { loadLinkCheckReport } from 'harvest/util/data';
import FailedLinks from 'harvest/LinkCheckReport/tables/FailedLinks';
import Placeholder from 'commons/components/common/Placeholder';
import './LinkCheckReport.scss';

const LinkCheckReport = ({ contextId }) => {
  const [report, setReport] = useState(null);
  const [failedLinks, setFailedLinks] = useState([]);

  const t = useTranslation(esreLinkCheckReportNLS);

  useEffect(() => {
    const fetchData = async () => {
      const linkCheckReport = await loadLinkCheckReport(contextId);
      setReport(linkCheckReport);
    };

    fetchData();
  }, [contextId]);

  useEffect(() => {
    if (!isEmpty(report)) {
      const brokenLinks = report.resource.filter(
        (check) => check.status === 'broken'
      );
      setFailedLinks(brokenLinks);
    }
  }, [report]);

  return !isEmpty(report) ? (
    <>
      <div className="esreLinkCheck__HeaderContainer">
        <Typography variant="h2" component="h3">
          {`${t('linkCheckReportHeader')} - ${report.createdAt.substring(
            0,
            report.createdAt.indexOf('T')
          )}`}
        </Typography>
        <MuiLink
          classes={{ root: 'esreOpenNewLink__Link' }}
          href={report.resourceURI}
          target="_blank"
        >
          {t('viewRawData')}
          <OpenInNewIcon classes={{ root: 'esreViewRawData__Icon' }} />
        </MuiLink>
      </div>
      <DoughnutChart
        data={{
          datasets: [
            {
              data: [
                report.failedLinkChecks,
                report.excludedLinkChecks,
                report.linkChecks -
                  report.failedLinkChecks -
                  report.excludedLinkChecks,
              ],
            },
          ],
          labels: [t('failed'), t('excluded'), t('successful')],
        }}
        type="pie"
        colors={COLOURS_PIE_STATUS}
        dimensions={{ height: '180px' }}
        // Important: Dimension attributes don't work without the following.
        // Only passed width is used and height is calculated according to aspect ratio,
        // ignoring the vnode attr.
        options={{ maintainAspectRatio: false }}
      />
      <div className="esreLinkCheckDashboard">
        <Card className="esreLinkCheckDashboard__Card">
          <CardContent>
            <Typography variant="h1" component="span">
              {report.linkChecks}
            </Typography>
            <Typography variant="body1">{t('total')}</Typography>
          </CardContent>
        </Card>
        <Card className="esreLinkCheckDashboard__Card">
          <CardContent>
            <Typography
              classes={{ root: 'esreLinkCheckDashboard__CardContent--failed' }}
              variant="h1"
              component="span"
            >
              {report.failedLinkChecks}
            </Typography>
            <Typography variant="body1">{t('failed')}</Typography>
          </CardContent>
        </Card>
        <Card className="esreLinkCheckDashboard__Card">
          <CardContent>
            <Typography
              classes={{
                root: 'esreLinkCheckDashboard__CardContent--excluded',
              }}
              variant="h1"
              component="span"
            >
              {report.excludedLinkChecks}
            </Typography>
            <Typography variant="body1">{t('excluded')}</Typography>
          </CardContent>
        </Card>
      </div>
      {!isEmpty(failedLinks) ? (
        <FailedLinks failedLinks={failedLinks} />
      ) : (
        <div>
          <Placeholder
            icon={<ThumbUpIcon fontSize="large" color="primary" />}
            label={t('noBrokenPlaceholder')}
            animated
          />
        </div>
      )}
    </>
  ) : (
    <div>
      <Placeholder label={t('placeholder')} animated />
    </div>
  );
};

LinkCheckReport.propTypes = {
  contextId: PropTypes.string.isRequired,
};

export default LinkCheckReport;
