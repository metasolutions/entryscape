import { createEntry } from 'commons/util/store';
import PropTypes from 'prop-types';
// eslint-disable-next-line max-len
import CreateEntryByTemplateDialog from 'commons/components/EntryListView/dialogs/create/CreateEntryByTemplateDialog';
import { useESContext } from 'commons/hooks/useESContext';
import {
  useTranslation,
  nlsBundlesPropType,
} from 'commons/hooks/useTranslation';
import {
  RDF_TYPE_SUGGESTION,
  RDF_STATUS_INVESTIGATING,
} from 'commons/util/entry';
import { LEVEL_RECOMMENDED } from 'commons/components/rdforms/LevelSelector';
import { useSnackbar } from 'commons/hooks/useSnackbar';

const CreateSuggestionDialog = ({ nlsBundles, closeDialog, onCreate }) => {
  const translate = useTranslation(nlsBundles);
  const { context } = useESContext();
  const [addSnackbar] = useSnackbar();
  const prototypeEntry = createEntry(context, RDF_TYPE_SUGGESTION);
  prototypeEntry
    .getMetadata()
    .add(prototypeEntry.getResourceURI(), 'rdf:type', RDF_TYPE_SUGGESTION);

  prototypeEntry
    .getEntryInfo()
    .getGraph()
    .add(prototypeEntry.getURI(), 'store:status', RDF_STATUS_INVESTIGATING);

  return (
    <CreateEntryByTemplateDialog
      nlsBundles={nlsBundles}
      closeDialog={closeDialog}
      entry={prototypeEntry}
      editorLevel={LEVEL_RECOMMENDED}
      entityType="suggestionListTitle"
      createHeaderLabel={translate('createHeader')}
      onEntryEdit={() =>
        addSnackbar({ message: translate('createSuccessMessage', 1) })
      }
      onCreate={onCreate}
    />
  );
};

CreateSuggestionDialog.propTypes = {
  nlsBundles: nlsBundlesPropType,
  closeDialog: PropTypes.func,
  onCreate: PropTypes.func,
};

export default CreateSuggestionDialog;
