import { useTranslation } from 'commons/hooks/useTranslation';
import { graphPropType } from 'commons/util/entry';
import PropTypes from 'prop-types';
import escoFormsNls from 'commons/nls/escoForms.nls';
import { useFieldsEdit } from '../hooks/useFieldsEdit';
import { Fields } from '../Fields';
import { Field } from '../Field';
import { Select } from '../Select';
import { normalizeChoices } from '../utils/choices';

export const SelectEditor = ({
  property,
  choices: optionalChoices,
  getChoices,
  nodetype,
  xs,
  disabled,
  size,
  children,
  graph,
  resourceURI,
  dispatch,
  onChange,
  resetOnFormType,
  confirmChange,
  error,
  helperText,
  validate,
  ...fieldsProps
}) => {
  const translate = useTranslation(escoFormsNls);
  const choices = optionalChoices || getChoices?.(graph, property);
  const { fields, labelId, addField, removeField, updateValue } = useFieldsEdit(
    {
      name: fieldsProps.name,
      property,
      nodetype,
      graph,
      resourceURI,
      dispatch,
      onChange,
      mandatory: fieldsProps.mandatory,
      resetOnFormType,
      validate,
    }
  );

  const handleChange = async (field, value) => {
    if (confirmChange) {
      const proceed = await confirmChange(field, value);
      if (!proceed) return;
    }
    updateValue(field, value);
  };

  return (
    <Fields labelId={labelId} addField={addField} {...fieldsProps}>
      {fields.map((field) => {
        return (
          <Field
            key={field.getId()}
            disabled={disabled}
            size={fields.length}
            onRemove={() => removeField(field)}
            onClear={() => updateValue(field, '')}
          >
            <Select
              disabled={disabled}
              xs={xs}
              labelId={labelId}
              value={field.getValue()}
              choices={normalizeChoices(choices, nodetype, translate)}
              onChange={({ target }) => handleChange(field, target.value)}
              size={size}
              error={error || field.hasError()}
              helperText={
                helperText ||
                (field.hasError() ? translate(field.getError()) : '')
              }
            />
          </Field>
        );
      })}
    </Fields>
  );
};

SelectEditor.propTypes = {
  choices: PropTypes.arrayOf(PropTypes.shape({})),
  getChoices: PropTypes.func,
  nodetype: PropTypes.string.isRequired,
  property: PropTypes.string.isRequired,
  xs: PropTypes.number,
  disabled: PropTypes.bool,
  size: PropTypes.string,
  graph: graphPropType,
  resourceURI: PropTypes.string,
  dispatch: PropTypes.func,
  onChange: PropTypes.func,
  resetOnFormType: PropTypes.bool,
  confirmChange: PropTypes.func,
  error: PropTypes.bool,
  helperText: PropTypes.string,
  children: PropTypes.node,
  validate: PropTypes.func,
};
