import FieldsLinkedDataBrowserDialog from 'commons/components/FieldsLinkedDataBrowserDialog';
import { ACTION_INFO_ID } from 'commons/actions/actionIds';
import esmoNamespacesNLS from 'models/nls/esmoNamespaces.nls';
import esmoCommonsNLS from 'models/nls/esmoCommons.nls';
import escoListNLS from 'commons/nls/escoList.nls';
import escoDialogsNLS from 'commons/nls/escoDialogs.nls';
import { fieldDefinitions } from '../../utils/fieldDefinitions';

export const nlsBundles = [
  esmoNamespacesNLS,
  esmoCommonsNLS,
  escoListNLS,
  escoDialogsNLS,
];

export const presenterAction = {
  id: ACTION_INFO_ID,
  Dialog: FieldsLinkedDataBrowserDialog,
  fields: fieldDefinitions,
};
