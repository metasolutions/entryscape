import { entrystore } from 'commons/store';
import { PrototypeEntry, Entry, Context } from '@entryscape/entrystore-js';
import { getExporter } from './exporters';
import { CONFIG_CONTEXT } from './namespace';

/**
 * @returns {Context}
 */
export const getConfigContext = () => {
  return entrystore.getContextById(CONFIG_CONTEXT);
};

/**
 * Get config context entry
 *
 * @returns {Promise<Entry>}
 */
const getConfigContextEntry = () => {
  return getConfigContext().getEntry();
};

/**
 * Get the config context entry or create it, if it doesn't exist.
 *
 * @returns {Entry}
 */
export const getOrCreateConfigContextEntry = () => {
  return getConfigContextEntry().then(null, () => {
    const prototypeEntry = entrystore.newContext(
      CONFIG_CONTEXT,
      CONFIG_CONTEXT
    );
    const entryInfo = prototypeEntry.getEntryInfo();
    const acl = entryInfo.getACL(true);
    acl.rread.push('_users');
    acl.mread.push('_users');
    entryInfo.setACL(acl);
    return prototypeEntry.commit();
  });
};

/**
 * Converts a type object to a type prototype entry. Exports each property in
 * type object to metadata graph by finding matching exporter from field
 * definitions.
 *
 * @param {*} prototypeEntryOptions
 * @returns {Entry}
 */
export const typeToPrototypeEntry = ({
  context,
  idPrefix,
  type,
  fieldDefinitions,
  rdfType,
}) => {
  const { name, ...typeConfig } = type;

  // create prototype entry
  const prototypeEntry = new PrototypeEntry(
    context,
    idPrefix ? `${idPrefix}-${name}` : name
  );
  const graph = prototypeEntry.getMetadata();
  const resourceURI = prototypeEntry.getResourceURI();

  // add rdf type to prototype
  const rdfTypes = Array.isArray(rdfType) ? rdfType : [rdfType];
  for (const rdfTypeValue of rdfTypes) {
    graph.add(resourceURI, 'rdf:type', rdfTypeValue);
  }

  // add each type property to metadata
  const typeKeyValues = Object.entries(typeConfig);
  for (const [key, value] of typeKeyValues) {
    const fieldDefinition = fieldDefinitions[key];
    const exportPropertyToGraph = getExporter(fieldDefinition);

    if (exportPropertyToGraph) {
      exportPropertyToGraph({ graph, resourceURI, value, ...fieldDefinition });
    }
  }
  return prototypeEntry;
};

/**
 * Converts array of type objects to type entries.
 *
 * @param {*} typeMigrationOptions
 * @returns {Promise<Entry[]>}
 */
export const migrateTypesToStore = async ({
  types,
  idPrefix,
  fieldDefinitions,
  rdfType,
}) => {
  const contextEntry = await getOrCreateConfigContextEntry();
  const context = contextEntry.getResource(true);

  const prototypeEntries = types.map((type) => {
    return typeToPrototypeEntry({
      context,
      idPrefix,
      type,
      rdfType,
      fieldDefinitions,
    });
  });

  const typeEntries = [];
  for (const prototypeEntry of prototypeEntries) {
    const typeEntry = await prototypeEntry.commit().then(null, () => {
      throw new Error('Something went wrong while migrating type to store');
    });
    typeEntries.push(typeEntry);
  }
  return typeEntries;
};

/**
 * @param {{rdfType: string}} queryOptions
 * @returns {Promise<object[]>}
 */
export const getConfigEntries = async ({ rdfType }) => {
  let context;
  try {
    context = await getConfigContextEntry();
  } catch (_error) {
    return;
  }

  return entrystore
    .newSolrQuery()
    .context(context)
    .rdfType(rdfType)
    .getEntries()
    .then((entries) => {
      return {
        entries,
        size: entries.length,
      };
    });
};
