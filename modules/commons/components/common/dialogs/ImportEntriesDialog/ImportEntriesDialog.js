import PropTypes from 'prop-types';
import { useCallback, useEffect, useMemo, useState } from 'react';
import {
  EntryListView,
  useSolrQuery,
  TITLE_COLUMN,
  MODIFIED_COLUMN,
  INFO_COLUMN,
} from 'commons/components/EntryListView';
import ListActionDialog from 'commons/components/common/dialogs/ListActionDialog';
import { useESContext } from 'commons/hooks/useESContext';
import {
  ListItemCheckbox,
  withListModelProvider,
} from 'commons/components/ListView';
import { getLabel } from 'commons/util/rdfUtils';
import { LIST_ACTION_INFO } from 'commons/components/EntryListView/actions';
import useAsync, { PENDING, RESOLVED } from 'commons/hooks/useAsync';
import {
  useTranslation,
  nlsBundlesPropType,
} from 'commons/hooks/useTranslation';
import { entrystore } from 'commons/store';
import { Context } from '@entryscape/entrystore-js';
import LoadingButton from 'commons/components/LoadingButton';
import { useSnackbar } from 'commons/hooks/useSnackbar';
import useErrorHandler from 'commons/errors/hooks/useErrorHandler';
import LinkedDataBrowserDialog from 'commons/components/LinkedDataBrowserDialog';
import {
  loadProjectItems,
  PROJECT_FILTER,
} from 'commons/components/filters/utils/filterDefinitions';
import useSearchFilters from 'commons/components/filters/hooks/useSearchFilters';

/**
 *
 * @param {Context} context
 * @param {string} rdfType
 * @returns {object}
 */
const getImportedEntries = (context, rdfType) =>
  entrystore
    .newSolrQuery()
    .rdfType(rdfType)
    .context(context)
    .list()
    .getAllEntries();

const ImportEntriesDialog = ({
  contextRdfType,
  rdfType,
  nlsBundles,
  closeDialog,
  afterCreateEntry = () => {},
}) => {
  const translate = useTranslation(nlsBundles);
  const { context } = useESContext();
  const [selectedEntries, setSelectedEntries] = useState([]);
  const {
    runAsync: runGetImportedEntries,
    data: importedResourceURIs,
    status: selectionStatus,
  } = useAsync({ data: [] });
  const {
    runAsync: runSaveChanges,
    status: saveStatus,
    error: saveError,
  } = useAsync();
  const [addSnackbar] = useSnackbar();

  useErrorHandler(saveError);

  const filters = useMemo(() => {
    return [
      {
        ...PROJECT_FILTER,
        loadItems: () => loadProjectItems(context, contextRdfType),
      },
    ];
  }, [context]);

  const { applyFilters, isLoading, hasFilters, ...filtersProps } =
    useSearchFilters(filters);
  const { hasSelectedFilter } = filtersProps;

  const createQuery = useCallback(() => {
    const query = entrystore
      .newSolrQuery()
      .rdfType(rdfType)
      .entryType('LinkReference', true);
    if (hasSelectedFilter()) return query;
    return query.context(context, true);
  }, [context, rdfType, hasSelectedFilter]);

  useEffect(() => {
    runGetImportedEntries(
      Promise.resolve(getImportedEntries(context, rdfType)).then(
        (importedEntries) => {
          return importedEntries.map((importedEntry) =>
            importedEntry.getResourceURI()
          );
        }
      )
    );
  }, [runGetImportedEntries, context, rdfType]);

  const { status: queryStatus, ...queryResults } = useSolrQuery({
    createQuery,
    applyFilters,
    wait: isLoading,
  });

  const isSelected = (entry) => selectedEntries.includes(entry);
  const isImported = (entry) =>
    importedResourceURIs.includes(entry.getResourceURI());

  const getCheckboxProps = ({ entry }) => {
    const disabled = isImported(entry);
    return {
      value: entry.getURI(),
      checked: isSelected(entry) || disabled,
      disabled,
      ariaLabel: getLabel(entry),
      tooltip: disabled ? translate('importedTooltip') : '',
    };
  };

  const handleSelectChange = async ({ target }) => {
    const { value, checked } = target;
    const entry = await entrystore.getEntry(value);
    const newSelection = checked
      ? [...selectedEntries, entry]
      : selectedEntries.filter(
          (selectedEntry) => selectedEntry.getURI() !== value
        );
    setSelectedEntries(newSelection);
  };

  const handleSaveChanges = () => {
    const saveChangesAndClose = async () => {
      for (const selectedEntry of selectedEntries) {
        const resourceURI = selectedEntry.getResourceURI();
        const metadataURI = selectedEntry.getEntryInfo().getMetadataURI();
        const prototypeEntry = context.newLinkRef(resourceURI, metadataURI);
        await prototypeEntry.commit();
      }
      await Promise.resolve(afterCreateEntry());
      closeDialog();
      addSnackbar({ message: translate('importSuccess') });
    };
    runSaveChanges(saveChangesAndClose());
  };

  const actions = (
    <LoadingButton
      loading={saveStatus === PENDING}
      onClick={handleSaveChanges}
      disabled={selectedEntries.length < 1}
    >
      {translate('reuseHeader')}
    </LoadingButton>
  );

  return (
    <ListActionDialog
      closeDialog={closeDialog}
      actions={actions}
      id="import-entries-dialog"
      title={translate('reuseHeader')}
      fixedHeight
    >
      <EntryListView
        nlsBundles={nlsBundles}
        status={selectionStatus !== RESOLVED ? selectionStatus : queryStatus}
        {...queryResults}
        includeFilters={hasFilters}
        filtersProps={filtersProps}
        columns={[
          {
            ...TITLE_COLUMN,
            xs: 8,
            getProps: ({ entry }) => ({
              primary: getLabel(entry),
            }),
          },
          MODIFIED_COLUMN,
          {
            ...INFO_COLUMN,
            getProps: ({ entry }) => ({
              action: {
                ...LIST_ACTION_INFO,
                Dialog: LinkedDataBrowserDialog,
                entry,
                nlsBundles,
              },
              title: translate('infoDialogTitle'),
            }),
          },
          {
            id: 'select-column',
            Component: ListItemCheckbox,
            getProps: getCheckboxProps,
            onChange: (event) => handleSelectChange(event),
          },
        ]}
      />
    </ListActionDialog>
  );
};

ImportEntriesDialog.propTypes = {
  contextRdfType: PropTypes.string,
  rdfType: PropTypes.string.isRequired,
  nlsBundles: nlsBundlesPropType,
  afterCreateEntry: PropTypes.func,
  closeDialog: PropTypes.func,
};

export default withListModelProvider(ImportEntriesDialog);
