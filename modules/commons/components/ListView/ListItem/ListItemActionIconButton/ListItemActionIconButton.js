import PropTypes from 'prop-types';
import { ListItemIconButton } from '../ListItemIconButton';
import { useActions } from '../../ActionsProvider';

// Component for list item icon buttons with actions.
// Enhances ListItemIconButton by providing an action for builtin handling of action dialog.
export const ListItemActionIconButton = ({
  action,
  title,
  disabled,
  classes,
  xs = 3,
  icon,
  children,
  ...rest
}) => {
  const { openActionDialog } = useActions();
  const handleClick = () => openActionDialog(action);

  return (
    <ListItemIconButton
      title={title}
      disabled={disabled}
      classes={classes}
      onClick={action && handleClick}
      xs={xs}
      highlightId={action?.highlightId}
      {...rest}
    >
      {icon || children}
    </ListItemIconButton>
  );
};

ListItemActionIconButton.propTypes = {
  action: PropTypes.shape({
    id: PropTypes.string,
    highlightId: PropTypes.string,
  }),
  title: PropTypes.string,
  icon: PropTypes.node,
  disabled: PropTypes.bool,
  classes: PropTypes.shape({}),
  xs: PropTypes.oneOfType([PropTypes.number, PropTypes.bool]),
  children: PropTypes.node,
};
