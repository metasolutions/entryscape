/**
 * Template replaces values in a string by using same delimeter ${} as for a
 * string literal. Second argument specifies what to replace, as key values.
 * @param {string} replaceString string to replace
 * @param {Object} properties data properties to replace
 * @returns {string} replaced string
 */
export const template = (replaceString, properties) => {
  if (!properties) return replaceString;

  let replacedString = replaceString;
  Object.entries(properties).forEach(([key, value = '']) => {
    replacedString = replacedString.replace(
      new RegExp(`\\$\\{${key}\\}`, 'g'),
      value
    );
  });
  return replacedString;
};
