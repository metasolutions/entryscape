import { Entry, Graph } from '@entryscape/entrystore-js';
import {
  extractAndSetSvgStyles,
  setAbsoluteDimensions,
  svgToCanvas,
} from 'commons/util/svg';
import { createBlob } from 'commons/util/file';

/**
 *
 * @param {Blob|string} data
 * @param {string} type
 * @param {Entry} prototypeEntry
 * @param {Graph} graph
 * @returns {Promise<Entry>}
 */
const commitEntryWithResource = async (data, type, prototypeEntry, graph) => {
  prototypeEntry.setMetadata(graph);
  const entry = await prototypeEntry.commit();
  return entry.getResource(true).put(data, type);
};

/**
 * Takes an svg element and converts it to a string representation. The entry is
 * commited with the svg string as resource.
 *
 * @param {Node} svgElement
 * @param {Entry} prototypeEntry
 * @param {Graph} graph
 * @returns {Promise<Entry>}
 */
export const createSvgResource = (svgElement, prototypeEntry, graph) => {
  setAbsoluteDimensions(svgElement);
  extractAndSetSvgStyles(svgElement);

  const svgString = new XMLSerializer().serializeToString(svgElement);
  return commitEntryWithResource(
    svgString,
    'image/svg+xml',
    prototypeEntry,
    graph
  );
};

/**
 * Takes an svg element and converts it to a png blob. The entry is commited
 * with the png blob as resource.
 *
 * @param {Node} svgElement
 * @param {Entry} prototypeEntry
 * @param {Graph} graph
 * @returns {Promise<Entry>}
 */
export const createPngResource = async (svgElement, prototypeEntry, graph) => {
  // make adjustments to svg
  setAbsoluteDimensions(svgElement);
  extractAndSetSvgStyles(svgElement);

  // convert svg to png blob
  const canvas = await svgToCanvas(svgElement);
  const [, pngData] = canvas.toDataURL('image/png').split(',');
  const pngBlob = createBlob(pngData);

  return commitEntryWithResource(pngBlob, 'image/png', prototypeEntry, graph);
};
