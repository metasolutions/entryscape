import * as fields from 'models/utils/fieldDefinitions';

export const fieldDefinitions = [
  fields.FIELD_ABBREVIATION,
  fields.FIELD_NAMESPACE,
  {
    ...fields.FIELD_TITLE,
    name: 'fullTitle',
    labelNlsKey: 'fullTitleLabel',
  },
  {
    ...fields.FIELD_DCT_DESCRIPTION,
    name: 'description',
  },
];
