import { namespaces as ns } from '@entryscape/rdfjson';
import { getBoundsFromWkt, getWktFromBounds } from './bounds';

ns.add('geo', 'http://www.w3.org/2003/01/geo/wgs84_pos#');

const detectRepresentation = (item) => {
  const predicate = item.getProperty();
  if (predicate?.startsWith('http://schema.org')) {
    return 'schema';
  }
  if (predicate?.startsWith('http://www.w3.org/2003/01/geo/wgs84_pos#')) {
    return 'geo';
  }
  return 'wkt';
};

/**
 * Checks if the expression in the binding supports setting a boundingbox (or only a point).
 *
 * @param binding
 * @return {boolean} true if a boundingbox should be possible to express.
 */
export const supportsBoundingBoxInBinding = (binding) =>
  !binding.getItem().hasStyle('geopoint');

// -- Getters ---
const getBoundsFromLatLongBinding = (binding, latitudePred, longitudePred) => {
  const value = binding.getValue();
  if (!value) {
    return undefined;
  }
  const predicate = binding.getItem().getProperty();
  const graph = binding.getGraph();
  const resource = binding.getStatement().getSubject();
  let north;
  let west;
  if (predicate === latitudePred) {
    north = parseFloat(value);
    west = parseFloat(graph.findFirstValue(resource, longitudePred));
  } else {
    north = parseFloat(graph.findFirstValue(resource, latitudePred));
    west = parseFloat(value);
  }
  return { type: 'point', directions: { north, west } };
};

const getBoundsFromSchemaBinding = (binding) =>
  getBoundsFromLatLongBinding(
    binding,
    ns.expand('schema:latitude'),
    ns.expand('schema:longitude')
  );

const getBoundsFromGeoBinding = (binding) =>
  getBoundsFromLatLongBinding(
    binding,
    ns.expand('geo:lat'),
    ns.expand('geo:long')
  );
const getBoundsFromWktBinding = (binding) => {
  const wkt = binding.getValue();
  if (!wkt) {
    return undefined;
  }
  return getBoundsFromWkt(wkt);
};

/**
 * Extracts a bounds object from a binding, supports expressions using WKT literal
 * as well as the lat and long properties in geo and schema namespaces.
 *
 * @param binding
 * @return {{directions: {east: number, south: number, north: number, west: number}, type: string}}
 */
export const getBindingBounds = (binding) => {
  switch (detectRepresentation(binding.getItem())) {
    case 'schema':
      return getBoundsFromSchemaBinding(binding);
    case 'geo':
      return getBoundsFromGeoBinding(binding);
    default:
      return getBoundsFromWktBinding(binding);
  }
};

// -- Setters ---
const setBoundsInLatLongBinding = (
  binding,
  bounds,
  latitudePred,
  longitudePred
) => {
  const predicate = binding.getItem().getProperty();
  const graph = binding.getGraph();
  const resource = binding.getStatement().getSubject();
  if (predicate === latitudePred) {
    graph.findAndRemove(resource, longitudePred);
    if (bounds) {
      binding.setValue(`${bounds.directions.north}`);
      graph.addL(resource, longitudePred, `${bounds.directions.west}`);
    }
  } else {
    graph.findAndRemove(resource, latitudePred);
    if (bounds) {
      graph.addL(resource, latitudePred, `${bounds.directions.north}`);
      binding.setValue(`${bounds.directions.west}`);
    }
  }
};

const setBoundsInSchemaBinding = (binding, bounds) =>
  setBoundsInLatLongBinding(
    binding,
    bounds,
    ns.expand('schema:latitude'),
    ns.expand('schema:longitude')
  );

const setBoundsInGeoBinding = (binding, bounds) =>
  setBoundsInLatLongBinding(
    binding,
    bounds,
    ns.expand('geo:lat'),
    ns.expand('geo:long')
  );
const setBoundsInWktBinding = (binding, bounds) => {
  const wkt = getWktFromBounds(bounds);
  binding.setValue(wkt);
};

/**
 * Sets the bounds in RDF as a WKT literal or separate properties in the geo or schema namespaces.
 *
 * @param binding
 * @param {{directions: {east: number, south: number, north: number, west: number}, type: string}} bounds
 */
export const setBindingBounds = (binding, bounds) => {
  switch (detectRepresentation(binding.getItem())) {
    case 'schema':
      setBoundsInSchemaBinding(binding, bounds);
      break;
    case 'geo':
      setBoundsInGeoBinding(binding, bounds);
      break;
    default:
      setBoundsInWktBinding(binding, bounds);
  }
};

// --- Clear ---
/**
 * When the binding is cleared (via the clear button in RDForms) additional properties need to be removed
 * in the latitude and longitude case using schema or geo namespace. The reason for this is that RDForms
 * items can only be triggered on a single predicate. For example, if the latitude is bound into the binding the
 * longitude triple needs to be cleared in a way that RDForms does not understand.
 *
 * For the WKT case nothing needs to be done as the WKT expression is stored in a single triple and entirely
 * handled with the clearing of the binding.
 *
 * @param binding
 */
export const clearBoundsOutsideOfBinding = (binding) => {
  // eslint-disable-next-line default-case
  switch (detectRepresentation(binding.getItem())) {
    case 'schema':
      setBoundsInSchemaBinding(binding);
      break;
    case 'geo':
      setBoundsInGeoBinding(binding);
      break;
  }
};
