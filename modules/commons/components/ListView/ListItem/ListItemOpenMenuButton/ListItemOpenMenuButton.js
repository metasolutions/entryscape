import PropTypes from 'prop-types';
import { OpenActionsMenuButton } from '../../ActionsMenu';
import { ListItemAction } from '../ListItemAction';

// Standard list item action button to use together with ActionsMenu.
export const ListItemOpenMenuButton = ({
  justifyContent = 'flex-end',
  xs,
  ...buttonProps
}) => {
  return (
    <ListItemAction xs={xs} justifyContent={justifyContent}>
      <OpenActionsMenuButton {...buttonProps} />
    </ListItemAction>
  );
};

ListItemOpenMenuButton.propTypes = {
  title: PropTypes.string,
  justifyContent: PropTypes.string,
  xs: PropTypes.number,
};
