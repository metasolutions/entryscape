import React, { useCallback, useEffect, useMemo } from 'react';
import PropTypes from 'prop-types';
import config from 'config';
import Lookup from 'commons/types/Lookup';
import { Badge } from '@mui/material';
import { Comment as CommentIcon } from '@mui/icons-material';
import { getTitle } from 'commons/util/metadata';
import { RDF_TYPE_DATASET, RDF_TYPE_DATASET_SERIES } from 'commons/util/entry';
import { ACTION_CREATE_ID } from 'commons/actions/actionIds';
import { useESContext } from 'commons/hooks/useESContext';
import { useTranslation } from 'commons/hooks/useTranslation';
import 'commons/components/rdforms/GeonamesChooser';
import CommentsDialog from 'commons/CommentsDialog';
import TableView from 'commons/components/TableView';
import { getPathFromViewName } from 'commons/util/site';
import useCommentsCount from 'commons/CommentsDialog/hooks/useCommentsCount';
import {
  PROFILE_FILTER,
  PUBLIC_STATUS_FILTER,
  EXTERNAL_STATUS_FILTER,
  DRAFT_STATUS_FILTER,
  HVD_FILTER,
  loadProfileItems,
} from 'commons/components/filters/utils/filterDefinitions';
import useSearchFilters from 'commons/components/filters/hooks/useSearchFilters';
import {
  EntryListView,
  useSolrQuery,
  TITLE_COLUMN,
  MODIFIED_COLUMN,
  PUBLIC_STATUS_COLUMN,
  MultiCreateButton,
  ACTIONS_GROUP_COLUMN,
} from 'commons/components/EntryListView';
import {
  LIST_ACTION_EDIT,
  LIST_ACTION_INFO,
} from 'commons/components/EntryListView/actions';
import {
  useListModel,
  withListModelProviderAndLocation,
  TOGGLE_TABLEVIEW,
  REFRESH,
  listPropsPropType,
  getIncludeEdit,
  withListRefresh,
} from 'commons/components/ListView';
import { applyQueryParams } from 'commons/util/solr';
import { DATASET_OVERVIEW_NAME } from 'catalog/config/config';
import useAsync from 'commons/hooks/useAsync';
import { removeAnyOption } from 'commons/components/filters/utils/filter';
import { getDraftsEnabled } from 'commons/types/utils/entityType';
import SelectDatasetTemplateDialog from '../dialogs/SelectDatasetTemplateDialog';
import CreateDatasetDialog from '../dialogs/CreateDatasetDialog';
import CreateDatasetSeriesDialog from '../dialogs/CreateDatasetSeriesDialog';
import { listActions as initialListActions, nlsBundles } from './actions';
import {
  DATASET_OR_SERIES_FILTER,
  IN_SERIES_CHECKBOX_FILTER,
} from './filterDefinitions';
import { getListQuery } from '../queries';
import { SERIES_TAG } from '../tags';

const CommentIconWithBadge = ({ commentsCount }) => (
  <Badge badgeContent={commentsCount} color="primary">
    <CommentIcon fontSize="small" color="secondary" />
  </Badge>
);

CommentIconWithBadge.propTypes = {
  commentsCount: PropTypes.number,
};

const DatasetsView = ({ listProps }) => {
  const translate = useTranslation(nlsBundles);
  const { context } = useESContext();
  const [{ showTableView }, dispatch] = useListModel();
  const { runAsync, data: profileItems } = useAsync({ data: [] });
  const includeEdit = getIncludeEdit(listProps);
  const datasetTemplatesEnabled = config.get('catalog.datasetTemplateEndpoint');
  const datasetSeriesEnabled = config.get('catalog.includeDatasetSeries');

  const datasetEntityType = Lookup.getByName('dataset');
  const draftsEnabled = getDraftsEnabled(datasetEntityType);

  const prototypeEntry = useMemo(
    () => context.newEntry().add('rdf:type', RDF_TYPE_DATASET),
    [context]
  );

  useEffect(() => {
    runAsync(loadProfileItems(prototypeEntry));
  }, [prototypeEntry, runAsync]);

  const includePSISwitch = config.get('catalog.allowInternalDatasetPublish');
  const commonFilters = useMemo(
    () =>
      includePSISwitch
        ? [EXTERNAL_STATUS_FILTER, PUBLIC_STATUS_FILTER]
        : [PUBLIC_STATUS_FILTER],
    [includePSISwitch]
  );

  const initialListFilters = useMemo(() => {
    const profileFilter = {
      ...PROFILE_FILTER,
      loadItems: () => loadProfileItems(prototypeEntry),
    };
    return [
      ...commonFilters,
      profileFilter,
      ...(draftsEnabled ? [DRAFT_STATUS_FILTER] : []),
      ...(datasetSeriesEnabled
        ? [DATASET_OR_SERIES_FILTER, IN_SERIES_CHECKBOX_FILTER]
        : []),
      HVD_FILTER,
    ];
  }, [commonFilters, prototypeEntry, datasetSeriesEnabled, draftsEnabled]);

  const tableViewFilters = useMemo(() => {
    const datasetEntityTypeId = profileItems.find((item) =>
      item.value.endsWith('/dataset')
    )?.value;
    const geoDatasetEntityTypeId = profileItems.find((item) =>
      item.value.endsWith('/geodataset')
    )?.value;

    const isGeoCatalog = Lookup.getProjectTypeInUse(context, true)
      ?.getPrimary()
      ?.some((id) => id.endsWith('/geodataset'));

    const profileFilter = {
      ...PROFILE_FILTER,
      items: profileItems,
      initialSelection: isGeoCatalog
        ? geoDatasetEntityTypeId
        : datasetEntityTypeId,
      defaultValue: datasetEntityTypeId,
    };

    return [
      ...commonFilters,
      removeAnyOption(profileFilter),
      ...(draftsEnabled ? [DRAFT_STATUS_FILTER] : []),
      ...(datasetSeriesEnabled
        ? [
            removeAnyOption({
              ...DATASET_OR_SERIES_FILTER,
              defaultValue: 'dataset',
              initialSelection: 'dataset',
            }),
          ]
        : []),
      HVD_FILTER,
    ];
  }, [
    commonFilters,
    datasetSeriesEnabled,
    profileItems,
    context,
    draftsEnabled,
  ]);

  const { applyFilters, isLoading, hasFilters, ...filterProps } =
    useSearchFilters(initialListFilters);
  const customApplyParams = useCallback(
    (baseQuery, params) => {
      const query = applyQueryParams(baseQuery, params);
      applyFilters(query);
    },
    [applyFilters]
  );

  const createQuery = useCallback(() => getListQuery(context), [context]);
  const { status, ...queryResults } = useSolrQuery({
    createQuery,
    applyQueryParams: customApplyParams,
    wait: isLoading,
  });

  const [getCommentsCount, refreshCommentsCount] = useCommentsCount(
    queryResults.entries
  );

  const createListAction = {
    id: ACTION_CREATE_ID,
    highlightId: 'listActionCreate',
    items: [
      {
        id: 'create',
        action: {
          Dialog: withListRefresh(CreateDatasetDialog, 'onCreate'),
        },
        labelNlsKey: 'createDatasetButton',
      },
      ...(datasetTemplatesEnabled
        ? [
            {
              id: 'create-from-dataset-template',
              action: {
                Dialog: SelectDatasetTemplateDialog,
              },
              labelNlsKey: 'createFromTemplateButton',
            },
          ]
        : []),
      ...(datasetSeriesEnabled
        ? [
            {
              id: 'create-dataset-series',
              action: {
                Dialog: withListRefresh(CreateDatasetSeriesDialog, 'onCreate'),
              },
              labelNlsKey: 'createSeriesButton',
            },
          ]
        : []),
    ],
  };
  const listActions =
    datasetTemplatesEnabled || datasetSeriesEnabled
      ? [
          {
            Component: MultiCreateButton,
            ...createListAction,
          },
        ]
      : initialListActions;
  const actionParams = {
    numberOfDatasets: queryResults.entries.length,
    refreshDatasets: () => dispatch({ type: REFRESH }),
    tooltip: translate('createButtonTooltipWithSubSeries'),
  };

  return showTableView ? (
    <TableView
      setTableView={() => dispatch({ type: TOGGLE_TABLEVIEW })}
      rdfType={[
        RDF_TYPE_DATASET,
        ...(datasetSeriesEnabled ? [RDF_TYPE_DATASET_SERIES] : []),
      ]}
      templateId={Lookup.getByName('dataset').complementaryTemplateId()}
      nlsBundles={nlsBundles}
      filterScheme={tableViewFilters}
    />
  ) : (
    <EntryListView
      {...queryResults}
      {...listProps}
      status={isLoading ? 'pending' : status}
      nlsBundles={nlsBundles}
      getListItemProps={({ entry }) => ({
        to: getPathFromViewName(DATASET_OVERVIEW_NAME, {
          contextId: context.getId(),
          entryId: entry.getId(),
        }),
      })}
      includeTableView={includeEdit}
      includeFilters={hasFilters}
      filtersProps={hasFilters ? filterProps : null}
      listActions={listActions}
      listActionsProps={{
        nlsBundles,
        actionParams,
      }}
      renderViewPlaceholder={
        datasetSeriesEnabled || datasetTemplatesEnabled
          ? (Placeholder) => (
              <Placeholder
                {...(datasetSeriesEnabled
                  ? { label: translate('emptyListWarningSeriesEnabled') }
                  : {})}
              >
                <MultiCreateButton
                  action={{ ...createListAction, actionParams }}
                />
              </Placeholder>
            )
          : null
      }
      columns={[
        {
          ...PUBLIC_STATUS_COLUMN,
          getProps: ({ entry, translate: t }) => ({
            entry,
            publicTooltip: t('publicDatasetTitle'),
            privateTooltip: t('privateDatasetTitle'),
            internalTooltip: t('internallyPublishedTitle'),
          }),
        },
        {
          ...TITLE_COLUMN,
          xs: 7,
          tags: SERIES_TAG,
        },
        { ...MODIFIED_COLUMN },
        {
          ...ACTIONS_GROUP_COLUMN,
          xs: 2,
          actions: [
            LIST_ACTION_INFO,
            { ...LIST_ACTION_EDIT, include: includeEdit },
            {
              id: 'comments',
              labelNlsKey: 'comments',
              Dialog: CommentsDialog,
              getProps: ({ entry }) => ({
                entry,
                icon: (
                  <CommentIconWithBadge
                    commentsCount={getCommentsCount(entry.getResourceURI())}
                  />
                ),
                dialogTitle: translate('commentHeader', getTitle(entry)),
                refreshCommentsCount,
                nlsBundles,
              }),
            },
          ],
        },
      ]}
    />
  );
};

DatasetsView.propTypes = {
  listProps: listPropsPropType,
};

export default withListModelProviderAndLocation(DatasetsView);
