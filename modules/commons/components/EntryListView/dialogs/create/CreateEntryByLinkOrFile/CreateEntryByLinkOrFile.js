import PropTypes from 'prop-types';
import React, { useCallback, useEffect, useState } from 'react';
import { Entry } from '@entryscape/entrystore-js';
import { Typography } from '@mui/material';
import escoRdformsNLS from 'commons/nls/escoRdforms.nls';
import escoListNLS from 'commons/nls/escoList.nls';
import escoEntryTypeNLS from 'commons/nls/escoEntryType.nls';
import escoErrorsNLS from 'commons/nls/escoErrors.nls';
import {
  useTranslation,
  nlsBundlesPropType,
  nlsPropType,
} from 'commons/hooks/useTranslation';
import useLevelProfile from 'commons/components/rdforms/hooks/useLevelProfile';
import LevelSelector, {
  LEVEL_MANDATORY,
} from 'commons/components/rdforms/LevelSelector';
import ListActionDialog from 'commons/components/common/dialogs/ListActionDialog';
import RDFormsFieldWrapper from 'commons/components/rdforms/FieldWrapper';
import {
  RdformsDialogFormWrapper,
  RdformsStickyDialogContent,
  RdformsDialogContent,
} from 'commons/components/rdforms/RdformsDialogFormWrapper';
import { useListModel, CREATE } from 'commons/components/ListView';
import { localize } from 'commons/locale';
import { getESContext } from 'commons/hooks/useESContext';
import useAsync, { PENDING } from 'commons/hooks/useAsync';
import DialogTwoColumnLayout, {
  PrimaryColumn,
  SecondaryColumn,
} from 'commons/components/common/dialogs/DialogTwoColumnLayout';
import RdformsOutline from 'commons/components/rdforms/RdformsOutline';
// eslint-disable-next-line max-len
import useRdformsOutlineSearch from 'commons/components/rdforms/RdformsOutline/useRdformsOutlineSearch';
import LoadingButton from 'commons/components/LoadingButton';
import { useRDFormsValidation } from 'commons/components/rdforms/hooks/useRDFormsValidation';
import { ErrorCatcher } from 'commons/errors/ErrorCatcher';
import { ErrorWithMessage } from 'commons/errors/utils/errors';
import { validate } from '@entryscape/rdforms';
import useAssignDraft from 'commons/hooks/useAssignDraft';
import Lookup from 'commons/types/Lookup';
import {
  useConfirmCloseAction,
  useCreateEntryState,
  ONLY_LINK,
  ONLY_FILE,
  INTERNAL_LINK,
  PATTERNED_LINK,
  LINK_AND_FILE,
  UPGRADE_LINK,
} from '../handlers';
import PatternedLinkField from '../PatternedLinkField';
import LinkFieldDefault from '../LinkField';
import UpgradeLinkField from '../UpgradeLinkField';
import FileField from '../FileField';
import LinkOrFileTabs from '../LinkOrFileTabs';

const NLS_BUNDLES = [
  escoListNLS,
  escoRdformsNLS,
  escoEntryTypeNLS,
  escoErrorsNLS,
];

const CreateEntryByLinkOrFile = ({
  entry,
  closeDialog,
  nlsBundles = NLS_BUNDLES,
  // dynamic props - to be set per edit dialog
  formTemplateId,
  createEntry,
  afterCreateEntry,
  editorLevel = LEVEL_MANDATORY,
  selectorType,
  entityType,
  upgradeLink,
  label,
  createErrorNls = 'createFail',
  LinkField = LinkFieldDefault,
}) => {
  const [, dispatch] = useListModel();
  const {
    level,
    setLevel,
    editor,
    EditorComponent,
    graph,
    templateId,
    setButtonDisabled,
  } = useCreateEntryState({
    entry,
    formTemplateId,
    editorLevel,
  });
  const disabledLevels = useLevelProfile(templateId);
  const context = getESContext();
  const [createEntryParams, setCreateEntryParams] = useState({});
  const [extraFieldIsValid, setExtraFieldIsValid] = useState(false);

  const translate = useTranslation(nlsBundles);
  const confirmClose = useConfirmCloseAction(closeDialog);
  const { runAsync: runSaveChanges, status, error: createError } = useAsync();
  const outlineSearchProps = useRdformsOutlineSearch();
  const { clearSearch: clearOutlineSearch } = outlineSearchProps;
  const { formErrorMessage, setFormErrorMessage, formErrors } =
    useRDFormsValidation({
      editor,
      onError: clearOutlineSearch,
    });
  const actualLabel = label || entityType?.label;
  const { checkCanDraft, applyDraftChanges } = useAssignDraft(true);

  useEffect(() => {
    if (graph) {
      graph.onChange = () => {
        setFormErrorMessage(null);
        setButtonDisabled(false);
      };
    }
  }, [graph, setFormErrorMessage, setButtonDisabled]);

  const handleSaveForm = useCallback(async () => {
    const { errors } = validate.bindingReport(editor.binding);
    const hasExtraField = selectorType !== INTERNAL_LINK;
    const extraFieldIsEmpty = !(
      createEntryParams.link || createEntryParams.file
    );
    const extraFieldHasError =
      hasExtraField &&
      (extraFieldIsEmpty || (!extraFieldIsEmpty && !extraFieldIsValid));
    if (extraFieldHasError) return;

    const extraErrorCode = !extraFieldHasError ? '' : 'invalid';
    const extraError = { code: extraFieldIsEmpty ? 'min' : extraErrorCode };
    const errorsToCheck = !extraFieldHasError
      ? errors
      : [...errors, extraError];

    const entityTypeInstance = entityType && Lookup.getByName(entityType.name);
    const canDraft = await checkCanDraft(errorsToCheck, entityTypeInstance);
    if (!canDraft) return;

    const updateParentList = () => dispatch({ type: CREATE });

    const afterMetadataUpdate = (event) =>
      afterCreateEntry ? afterCreateEntry(event) : Promise.resolve();

    const saveChanges = async () => {
      return createEntry(entry, graph, context, createEntryParams).catch(
        (error) => {
          throw new ErrorWithMessage(translate(createErrorNls), error);
        }
      );
    };

    runSaveChanges(
      applyDraftChanges(entry)
        .then(saveChanges)
        .then(afterMetadataUpdate)
        .then(updateParentList)
        .then(closeDialog)
    );
  }, [
    entry,
    context,
    graph,
    afterCreateEntry,
    createEntry,
    createEntryParams,
    extraFieldIsValid,
    closeDialog,
    dispatch,
    runSaveChanges,
    translate,
    createErrorNls,
    checkCanDraft,
    applyDraftChanges,
    editor?.binding,
    selectorType,
    entityType,
  ]);

  const handleCloseAction = useCallback(() => {
    const changeCondition = graph.isChanged() || createEntryParams.link;
    confirmClose(changeCondition);
  }, [graph, confirmClose, createEntryParams]);

  const createButtonDisabled =
    selectorType === INTERNAL_LINK ? false : !extraFieldIsValid;

  const dialogActions = (
    <LoadingButton
      autoFocus
      onClick={handleSaveForm}
      loading={status === PENDING}
      disabled={createButtonDisabled}
    >
      {translate('createButton')}
    </LoadingButton>
  );

  return (
    <>
      <ListActionDialog
        id="create-entry"
        title={translate('createHeader', localize(actualLabel))}
        actions={dialogActions}
        closeDialog={handleCloseAction}
        alert={
          formErrorMessage
            ? {
                message: formErrorMessage,
                setMessage: setFormErrorMessage,
              }
            : undefined
        }
      >
        <DialogTwoColumnLayout>
          <SecondaryColumn>
            {editor ? (
              <RdformsOutline
                editor={editor}
                root="create-entry"
                errors={formErrors}
                searchProps={outlineSearchProps}
              />
            ) : null}
          </SecondaryColumn>
          <PrimaryColumn>
            <RdformsDialogFormWrapper>
              <RdformsStickyDialogContent>
                <LevelSelector
                  level={level}
                  onUpdateLevel={setLevel}
                  disabledLevels={disabledLevels}
                />
              </RdformsStickyDialogContent>
              <RdformsDialogContent>
                {selectorType === LINK_AND_FILE ? (
                  <LinkOrFileTabs
                    setCreateEntryParams={setCreateEntryParams}
                    setExtraFieldIsValid={setExtraFieldIsValid}
                    uniqueURI={Boolean(entityType?.uniqueURIScope)}
                  />
                ) : null}
                {selectorType === ONLY_FILE ? (
                  <FileField
                    setCreateEntryParams={setCreateEntryParams}
                    setExtraFieldIsValid={setExtraFieldIsValid}
                  />
                ) : null}
                {selectorType === ONLY_LINK ? (
                  <LinkField
                    setCreateEntryParams={setCreateEntryParams}
                    setExtraFieldIsValid={setExtraFieldIsValid}
                    upgradeLink={upgradeLink}
                  />
                ) : null}
                {selectorType === INTERNAL_LINK ? (
                  <RDFormsFieldWrapper
                    label={translate('webAddressLabel')}
                    editor
                  >
                    <Typography
                      variant="body1"
                      gutterBottom // margin should be on RDFormsFieldWrapper
                    >
                      {translate('internalLinkLabel')}
                    </Typography>
                  </RDFormsFieldWrapper>
                ) : null}
                {selectorType === UPGRADE_LINK ? (
                  <UpgradeLinkField
                    link={upgradeLink}
                    setCreateEntryParams={setCreateEntryParams}
                    setExtraFieldIsValid={setExtraFieldIsValid}
                  />
                ) : null}
                {selectorType === PATTERNED_LINK ? (
                  <PatternedLinkField
                    entityType={entityType}
                    setCreateEntryParams={setCreateEntryParams}
                    setExtraFieldIsValid={setExtraFieldIsValid}
                  />
                ) : null}
                {editor ? <EditorComponent /> : null}
              </RdformsDialogContent>
            </RdformsDialogFormWrapper>
          </PrimaryColumn>
        </DialogTwoColumnLayout>
      </ListActionDialog>
      <ErrorCatcher error={createError} />
    </>
  );
};

CreateEntryByLinkOrFile.propTypes = {
  entry: PropTypes.instanceOf(Entry),
  closeDialog: PropTypes.func,
  nlsBundles: nlsBundlesPropType,
  formTemplateId: PropTypes.string,
  createEntry: PropTypes.func,
  afterCreateEntry: PropTypes.func,
  editorLevel: PropTypes.string,
  selectorType: PropTypes.string,
  entityType: PropTypes.shape({
    label: PropTypes.shape({
      en: PropTypes.string,
      sv: PropTypes.string,
      de: PropTypes.string,
    }),
    uniqueURIScope: PropTypes.string,
    draftsEnabled: PropTypes.bool,
    name: PropTypes.string,
  }),
  upgradeLink: PropTypes.string,
  label: nlsPropType,
  createErrorNls: PropTypes.string,
  LinkField: PropTypes.func,
};

export default CreateEntryByLinkOrFile;
