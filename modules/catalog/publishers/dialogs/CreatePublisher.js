import { useState } from 'react';
import { createEntry } from 'commons/util/store';
import { Context, Entry } from '@entryscape/entrystore-js';
import PropTypes from 'prop-types';
// eslint-disable-next-line max-len
import CreateEntryByTemplateDialog from 'commons/components/EntryListView/dialogs/create/CreateEntryByTemplateDialog';
import { useESContext } from 'commons/hooks/useESContext';
import config from 'config';
import { LEVEL_OPTIONAL } from 'commons/components/rdforms/LevelSelector';
import { useTranslation } from 'commons/hooks/useTranslation';
import escaPublishersNLS from 'catalog/nls/escaPublishers.nls';
import { useSnackbar } from 'commons/hooks/useSnackbar';

/**
 *
 * @param {Context} context
 * @returns {Entry}
 */
const createPrototypeEntry = (context) => {
  const rdfType = config.get('catalog.defaultAgentType');
  const prototypeEntry = createEntry(context, rdfType);
  prototypeEntry
    .getMetadata()
    .add(prototypeEntry.getResourceURI(), 'rdf:type', rdfType);
  return prototypeEntry;
};

const CreatePublisher = ({ onCreate, ...props }) => {
  const translate = useTranslation(escaPublishersNLS);
  const [addSnackbar] = useSnackbar();

  const { context } = useESContext();
  const [prototypeEntry] = useState(() => createPrototypeEntry(context));

  return (
    <CreateEntryByTemplateDialog
      {...props}
      entry={prototypeEntry}
      editorLevel={LEVEL_OPTIONAL}
      createHeaderLabel={translate('createFOAFButton')}
      onEntryEdit={() => {
        onCreate();
        addSnackbar({ message: translate('createPublisherSuccess') });
      }}
    />
  );
};

CreatePublisher.propTypes = { onCreate: PropTypes.func };

export default CreatePublisher;
