import PropTypes from 'prop-types';
import { ListItemText, Button } from '@mui/material';
import escoListNLS from 'commons/nls/escoList.nls';
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown';
import ArrowDropUpIcon from '@mui/icons-material/ArrowDropUp';
import { useTranslation } from 'commons/hooks/useTranslation';
import Tooltip from 'commons/components/common/Tooltip';
import { SORT, useListModel } from '../../hooks/useListModel';
import './ListItemSort.scss';

/**
 * @param {string} sortOrder
 * @returns {string}
 */
const reverseSortOrder = (sortOrder) => (sortOrder === 'asc' ? 'desc' : 'asc');

const ArrowIcon = ({ sortOrder }) =>
  sortOrder === 'asc' ? <ArrowDropUpIcon /> : <ArrowDropDownIcon />;

ArrowIcon.propTypes = {
  sortOrder: PropTypes.string,
};

export const ListItemSort = ({ sortBy, text, tooltipAlias = '' }) => {
  const t = useTranslation(escoListNLS);
  const [{ sort }, dispatch] = useListModel();
  const { order, field } = sort;
  const isSorted = field === sortBy;
  const sortOrder = isSorted ? order : 'desc';

  const fieldLabel =
    tooltipAlias ||
    (sortBy === 'modified'
      ? t('sortingFieldModifiedDate')
      : text.toLowerCase());

  const getSortedTooltip = () =>
    sortOrder === 'asc'
      ? t('sortFieldInDesc', fieldLabel)
      : t('sortFieldInAsc', fieldLabel);

  const tooltip = isSorted
    ? getSortedTooltip()
    : t('actionTooltipSort', fieldLabel);

  const handleSort = () => {
    const value = {
      field: sortBy,
      order: isSorted ? reverseSortOrder(sortOrder) : 'desc', // use default desc if not sorted
    };
    dispatch({ type: SORT, value });
  };

  return (
    <ListItemText>
      <Tooltip title={tooltip}>
        <Button
          variant="text"
          className="escoListViewHeaderSortButton"
          disableElevation
          onClick={() => handleSort()}
          endIcon={isSorted ? <ArrowIcon sortOrder={sortOrder} /> : null}
        >
          {text}
        </Button>
      </Tooltip>
    </ListItemText>
  );
};

ListItemSort.propTypes = {
  sortBy: PropTypes.string,
  text: PropTypes.string,
  tooltipAlias: PropTypes.string,
};
