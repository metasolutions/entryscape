import PropTypes from 'prop-types';
import { AddFieldButton } from '../AddFieldButton';
import { FieldsLabel } from '../FieldsLabel';
import './Fields.scss';

export const FieldsWrapper = ({ children }) => {
  return <div className="esmoFields">{children}</div>;
};

FieldsWrapper.propTypes = {
  children: PropTypes.node,
};

export const Fields = ({
  addField,
  label,
  labelId,
  mandatory,
  max,
  children,
}) => (
  <FieldsWrapper>
    <FieldsLabel mandatory={mandatory} label={label} id={labelId} />
    {children}
    {max !== 1 ? (
      <AddFieldButton onClick={addField} label={label} title="Add" />
    ) : null}
  </FieldsWrapper>
);

Fields.propTypes = {
  addField: PropTypes.func,
  label: PropTypes.string,
  labelId: PropTypes.string,
  mandatory: PropTypes.bool,
  max: PropTypes.number,
  children: PropTypes.node,
};

export default Fields;
