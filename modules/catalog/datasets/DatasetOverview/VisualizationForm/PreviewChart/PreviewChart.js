import PropTypes from 'prop-types';
import { MapProvider } from 'commons/components/Map/hooks/useMapContext';
import VisualizationChart from '../../VisualizationChart';
import VisualizationTable from '../../VisualizationTable';
import VisualizationMap from '../../VisualizationMap';

const PreviewChart = ({
  csvData,
  chartType,
  xField,
  yField,
  operation,
  featureTitle,
  scaleLineControl,
  mapOptions,
  onViewExtentChange,
}) => {
  const fields = csvData.meta?.fields || [];
  return (
    <>
      {chartType === 'table' && (
        <VisualizationTable data={csvData?.data || []} tableColumns={fields} />
      )}
      {(chartType === 'bar' || chartType === 'line' || chartType === 'pie') && (
        <VisualizationChart
          data={csvData}
          type={chartType}
          xField={xField}
          yField={yField}
          operation={operation}
        />
      )}
      {chartType === 'map' && (
        <MapProvider>
          <VisualizationMap
            data={csvData}
            xField={xField}
            yField={yField}
            featureTitleField={featureTitle}
            scaleLineControl={scaleLineControl}
            mapOptions={mapOptions}
            onViewExtentChange={onViewExtentChange}
            height="400px"
          />
        </MapProvider>
      )}
    </>
  );
};

PreviewChart.propTypes = {
  chartType: PropTypes.string,
  csvData: PropTypes.shape({
    meta: PropTypes.shape({ fields: PropTypes.arrayOf(PropTypes.string) }),
    data: PropTypes.arrayOf(PropTypes.shape({})),
  }),
  mapOptions: PropTypes.shape({}),
  onViewExtentChange: PropTypes.func,
  fields: PropTypes.arrayOf(PropTypes.string),
  xField: PropTypes.string,
  yField: PropTypes.string,
  operation: PropTypes.string,
  featureTitle: PropTypes.string,
  scaleLineControl: PropTypes.string,
};
export default PreviewChart;
