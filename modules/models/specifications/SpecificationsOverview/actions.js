import CreateEntityDialog from 'commons/components/EntityOverview/dialogs/CreateEntityDialog';
import BuildCreateDialog from '../../builds/dialogs/BuildCreateDialog';
import CreateDiagramDialog from '../../builds/dialogs/CreateDiagramDialog';
import CreateRDFSDialog from '../../builds/dialogs/CreateRDFSDialog';

export const createActions = [
  {
    id: 'rdforms-bundles',
    labelNlsKey: 'rdformsBundleLabel',
    action: {
      Dialog: BuildCreateDialog,
    },
  },
  {
    id: 'diagrams',
    labelNlsKey: 'diagramResourceLabel',
    action: {
      Dialog: CreateDiagramDialog,
    },
  },
  {
    id: 'rdfs',
    labelNlsKey: 'rdfsResourceLabel',
    action: {
      Dialog: CreateRDFSDialog,
    },
  },
  {
    id: 'resource',
    labelNlsKey: 'resourceLabel',
    action: {
      Dialog: CreateEntityDialog,
    },
  },
];
