import { loadCanvg } from './canvg';

/**
 * Extract computed styles for an element.
 *
 * @param {Element} element
 * @returns {object}
 */
const getComputedStyles = (element) => {
  const styles = window.getComputedStyle(element);
  const computedStyles = {};
  for (const style of styles) {
    computedStyles[style] = styles.getPropertyValue(style);
  }
  return computedStyles;
};

/**
 * Extract computed styles on an empty element of same type as a source element.
 *
 * @param {Element} sourceElement
 * @param {Element} svgElement
 * @returns {object}
 */
const getDefaultStyles = (sourceElement, svgElement) => {
  const { tagName } = sourceElement;
  const element = document.createElement(tagName);
  svgElement.appendChild(element);

  const defaultStyles = getComputedStyles(element);
  svgElement.removeChild(element);
  return defaultStyles;
};

/**
 *
 * @param {object} computedStyles
 * @param {object} defaultStyles
 * @returns {object}
 */
const getStylesDiff = (computedStyles, defaultStyles) => {
  const stylesDiff = Object.entries(computedStyles).reduce(
    (styles, [property, value]) => {
      // only include styles that differ
      if (property in defaultStyles && defaultStyles[property] === value)
        return styles;
      styles[property] = value;
      return styles;
    },
    {}
  );
  return stylesDiff;
};

/**
 * Clean element attributes on the computed styles. Also attributes are
 * extracted when computing styles for an element and needs to be removed.
 *
 * @param {Element} element
 * @param {object} styles
 * @returns {object}
 */
const excludeAttributes = (element, styles) => {
  return Object.entries(styles).reduce((acc, [property, value]) => {
    const attribute = element.getAttribute(property);
    if (!attribute) {
      acc[property] = value;
    }
    return acc;
  }, {});
};

/**
 * Apply styles on element and remove class
 *
 * @param {Element} element
 * @param {object} styles
 * @returns {undefined}
 */
const replaceClassWithStyles = (element, styles) => {
  Object.entries(styles).forEach(([property, value]) => {
    element.style[property] = value;
  });
  element.removeAttribute('class');
};

/**
 * Extract styles from classes and inherited styles for each element in a svg.
 * Extracted styles are applied and classes are removed.
 * This is useful when exporting a svg, where css classes are not available.
 *
 * @param {Element} svgElement
 * @returns {Element}
 */
export const extractAndSetSvgStyles = (svgElement) => {
  // create clean svg for getting default style
  const svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
  svg.style.display = 'none';
  document.body.appendChild(svg);

  const allSvgElements = Array.from(svgElement.querySelectorAll('*'));
  allSvgElements.push(svgElement);

  // extract styles
  const extractedStyles = allSvgElements.map((element) => {
    const computedStyles = getComputedStyles(element);
    const defaultStyles = getDefaultStyles(element, svg);
    const stylesDiff = getStylesDiff(computedStyles, defaultStyles);
    const styles = excludeAttributes(element, stylesDiff);
    return { element, styles };
  });
  // apply styles
  extractedStyles.forEach(({ element, styles }) => {
    replaceClassWithStyles(element, styles);
  });

  document.body.removeChild(svg);
  return svgElement;
};

/**
 * Adds a colored background rectangle
 *
 * @param {Element} svgElement
 * @param {string} color
 * @returns {Element}
 */
export const setBackgroundColor = (svgElement, color = '#fff') => {
  const backgroundRect = document.createElementNS(
    'http://www.w3.org/2000/svg',
    'rect'
  );
  backgroundRect.setAttribute('width', '100%');
  backgroundRect.setAttribute('height', '100%');
  backgroundRect.setAttribute('fill', color);
  svgElement.insertBefore(backgroundRect, svgElement.firstChild);

  return svgElement;
};

/**
 * Calculate dimensions for the original svg element and set them as absolute values.
 *
 * @param {Element} svgElement
 * @returns {Element}
 */
export const setAbsoluteDimensions = (svgElement) => {
  const boundingBox = svgElement.getBoundingClientRect();
  const { height, width } = boundingBox;

  svgElement.setAttribute('height', height);
  svgElement.setAttribute('width', width);
  return svgElement;
};

/**
 *
 * @param {string} svgString
 * @param {Element} canvas
 * @param {object} renderOptions
 * @returns {Element}
 */
export const renderSvgStringOnCanvas = async (
  svgString,
  canvas,
  renderOptions = {
    ignoreMouse: true,
    ignoreAnimation: true,
  }
) => {
  const Canvg = await loadCanvg();
  const context = canvas.getContext('2d');
  const canvg = Canvg.fromString(context, svgString, renderOptions);
  await canvg.render();

  return context.canvas;
};

/**
 *
 * @param {Element} svgElement
 * @param {object} renderOptions
 * @returns {Promise<Element>}
 */
export const svgToCanvas = async (svgElement, renderOptions) => {
  const canvas = document.createElement('canvas');
  const svgString = new XMLSerializer().serializeToString(svgElement);

  return renderSvgStringOnCanvas(svgString, canvas, renderOptions);
};
