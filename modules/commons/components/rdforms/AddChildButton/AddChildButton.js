import PropTypes from 'prop-types';
import Button from '@mui/material/Button';
import AddIcon from '@mui/icons-material/Add';

const AddChildButton = ({ disabled, onClick, title, label }) => {
  return (
    <Button
      className="rdformsAddChild"
      onClick={onClick}
      aria-label={title}
      title={title}
      disabled={disabled}
      variant="text"
      color="primary"
      startIcon={<AddIcon />}
    >
      {label}
    </Button>
  );
};

AddChildButton.propTypes = {
  disabled: PropTypes.bool,
  onClick: PropTypes.func,
  title: PropTypes.string,
  label: PropTypes.string,
};

export default AddChildButton;
