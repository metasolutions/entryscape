import PropTypes from 'prop-types';
import { useEffect, useState } from 'react';
import { Button } from '@mui/material';
import {
  useTranslation,
  nlsBundlesPropType,
} from 'commons/hooks/useTranslation';
import useAsync from 'commons/hooks/useAsync';
import ListActionDialog from 'commons/components/common/dialogs/ListActionDialog';
import ContentWrapper from 'commons/components/common/ContentWrapper';
import {
  useEditorContext,
  validateFieldSet,
  EditorProvider,
  Form,
} from 'commons/components/forms/editors';
import { entryPropType, refreshEntry } from 'commons/util/entry';
import Loader from 'commons/components/Loader';
import * as ns from 'models/utils/ns';
import { fieldDefinitions } from 'models/namespaces/utils/fieldDefinitions';
import { useSnackbar, SUCCESS_EDIT } from 'commons/hooks/useSnackbar';
import { ErrorCatcher } from 'commons/errors/ErrorCatcher';
import { ErrorWithMessage } from 'commons/errors/utils/errors';

const EditDialog = ({ closeDialog, nlsBundles, onEntryEdit, children }) => {
  const translate = useTranslation(nlsBundles);
  const { runAsync, error: editError } = useAsync();
  const [hasChange, setHasChange] = useState(false);
  const { entry, graph, resourceURI, fieldSet, canSubmit } = useEditorContext();
  const [addSnackbar] = useSnackbar();

  const title = graph?.findFirstValue(resourceURI, ns.RDF_TYPE_PREFIX) || '';

  useEffect(() => {
    if (!graph) return;

    graph.onChange = () => setHasChange(true);
  }, [graph]);

  const handleSaveForm = () => {
    const errors = validateFieldSet(fieldSet);
    if (errors.length || !hasChange) {
      return;
    }

    entry.setMetadata(graph);

    // run after metadata has been updated
    const afterMetadataUpdate = async () => {
      await refreshEntry(entry);
      return onEntryEdit ? onEntryEdit(entry) : Promise.resolve();
    };

    runAsync(
      entry
        .commitMetadata()
        .then(() => {
          onEntryEdit();
          afterMetadataUpdate();
          closeDialog();
          addSnackbar({ type: SUCCESS_EDIT });
        })
        .catch((error) => {
          throw new ErrorWithMessage(translate('saveEditsFail'), error);
        })
    );
  };

  const actions = (
    <Button
      autoFocus
      onClick={handleSaveForm}
      disabled={!canSubmit || !hasChange}
    >
      {translate('save')}
    </Button>
  );

  return (
    <>
      <ListActionDialog
        id="edit-namespace"
        title={translate('editHeader', title)}
        actions={actions}
        closeDialog={closeDialog}
      >
        <ContentWrapper>
          {graph ? children : <Loader height="40vh" />}
        </ContentWrapper>
      </ListActionDialog>
      <ErrorCatcher error={editError} />
    </>
  );
};

EditDialog.propTypes = {
  closeDialog: PropTypes.func.isRequired,
  onEntryEdit: PropTypes.func,
  nlsBundles: nlsBundlesPropType,
  children: PropTypes.node,
};

const NamespaceEditDialog = ({
  closeDialog,
  entry,
  nlsBundles,
  onEntryEdit,
}) => {
  const translate = useTranslation(nlsBundles);

  return (
    <EditorProvider entry={entry}>
      <EditDialog
        title={translate('editHeader')}
        closeDialog={closeDialog}
        nlsBundles={nlsBundles}
        onEntryEdit={onEntryEdit}
      >
        <Form fields={fieldDefinitions} size="small" nlsBundles={nlsBundles} />
      </EditDialog>
    </EditorProvider>
  );
};

NamespaceEditDialog.propTypes = {
  closeDialog: PropTypes.func.isRequired,
  onEntryEdit: PropTypes.func,
  nlsBundles: nlsBundlesPropType,
  entry: entryPropType,
};

export default NamespaceEditDialog;
