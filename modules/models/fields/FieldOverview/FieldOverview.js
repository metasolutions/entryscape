import { useEntry } from 'commons/hooks/useEntry';
import { getPathFromViewName } from 'commons/util/site';
import { useParams } from 'react-router-dom';
import useNavigate from 'commons/components/router/useNavigate';
import {
  OverviewSection,
  withOverviewModelProvider,
  overviewPropsPropType,
} from 'commons/components/overview';
import {
  ACTION_INFO_WITH_ICON,
  ACTION_DIVIDER,
  ACTION_PUBLISH,
} from 'commons/components/overview/actions';
import esmoFieldsNls from 'models/nls/esmoFields.nls';
import esmoCommonsNls from 'models/nls/esmoCommons.nls';
import escoListNls from 'commons/nls/escoList.nls';
import escoDialogsNLS from 'commons/nls/escoDialogs.nls';
import { useTranslation } from 'commons/hooks/useTranslation';
import { FieldPresenter } from 'models/presenters';
import ReferenceList from 'models/lists/ReferenceList';
import PreviewFormDialog from 'models/components/PreviewFormDialog';
import RemoveEntryDialog from 'commons/components/entry/RemoveEntryDialog';
import {
  RDF_TYPE_FIELD,
  RDF_TYPE_PROPERTY_FORM,
  RDF_TYPE_OBJECT_FORM,
  RDF_TYPE_SECTION_FORM,
  RDF_TYPE_PROFILE_FORM,
} from 'models/utils/ns';
import { withActionsProvider } from 'commons/components/ListView';
import { FIELDS_FORMS_INFO } from 'models/utils/fieldDefinitions';
import FieldsLinkedDataBrowserDialog from 'commons/components/FieldsLinkedDataBrowserDialog';
import Overview from 'commons/components/overview/Overview';
import { getFieldLabel } from '../utils/fields';
import {
  primaryFields,
  secondaryFields as fields,
} from '../utils/fieldPresenters';
import { sidebarActions } from './actions';
import './FieldOverview.scss';

const NLS_BUNDLES = [
  esmoFieldsNls,
  escoListNls,
  esmoCommonsNls,
  escoDialogsNLS,
];
const USED_BY_RDF_TYPES = [
  RDF_TYPE_SECTION_FORM,
  RDF_TYPE_PROPERTY_FORM,
  RDF_TYPE_OBJECT_FORM,
  RDF_TYPE_PROFILE_FORM,
];
const EXTENDED_BY_RDF_TYPE = RDF_TYPE_FIELD;

const FieldOverview = ({ overviewProps }) => {
  const fieldEntry = useEntry();
  const translate = useTranslation(NLS_BUNDLES);
  const { navigate } = useNavigate();
  const viewParams = useParams();

  const onRemoveField = () => {
    const fieldsPath = getPathFromViewName('models__fields', viewParams);
    navigate(fieldsPath);
  };

  return (
    <Overview
      {...overviewProps}
      backLabel={translate('backTitle')}
      headerAction={{
        ...ACTION_INFO_WITH_ICON,
        nlsBundles: NLS_BUNDLES,
        entry: fieldEntry,
        Dialog: FieldsLinkedDataBrowserDialog,
        fields: FIELDS_FORMS_INFO,
      }}
      entry={fieldEntry}
      nlsBundles={NLS_BUNDLES}
      renderPrimaryContent={() => (
        <div className="esmoHeaderPresenters">
          {primaryFields.map((field) => (
            <FieldPresenter
              key={field.property}
              graph={fieldEntry.getMetadata()}
              resourceURI={fieldEntry.getResourceURI()}
              {...field}
            />
          ))}
        </div>
      )}
      headerSubLabel={getFieldLabel(fieldEntry, translate)}
      sidebarActions={[
        ...sidebarActions,
        {
          id: 'preview',
          Dialog: PreviewFormDialog,
          labelNlsKey: 'preview',
        },
        {
          id: 'remove',
          Dialog: RemoveEntryDialog,
          labelNlsKey: 'remove',
          getProps: ({ translate: localTranslate }) => ({
            action: {
              removeConfirmMessage: localTranslate('removalConfirmation'),
              onRemoveCallback: onRemoveField,
            },
          }),
        },
        ACTION_DIVIDER,
        ACTION_PUBLISH,
      ]}
    >
      <OverviewSection>
        {fields.map((field) => (
          <FieldPresenter
            key={field.property}
            graph={fieldEntry.getMetadata()}
            resourceURI={fieldEntry.getResourceURI()}
            {...field}
          />
        ))}
      </OverviewSection>
      <OverviewSection>
        <ReferenceList
          entry={fieldEntry}
          nlsBundles={NLS_BUNDLES}
          rdfType={USED_BY_RDF_TYPES}
          headingNlsKey="usedByListHeader"
          placeholderProps={{
            labelNlsKey: 'usedByPlaceholder',
          }}
          pathviewName="models__forms__form"
          translate={translate}
          actions={sidebarActions}
          onRemoveCallback={onRemoveField}
        />
      </OverviewSection>
      <OverviewSection>
        <ReferenceList
          entry={fieldEntry}
          nlsBundles={NLS_BUNDLES}
          rdfType={EXTENDED_BY_RDF_TYPE}
          headingNlsKey="extendedByHeader"
          placeholderProps={{
            labelNlsKey: 'extendedByFieldPlaceholder',
          }}
          pathviewName="models__fields__field"
        />
      </OverviewSection>
    </Overview>
  );
};

FieldOverview.propTypes = {
  overviewProps: overviewPropsPropType,
};

export default withOverviewModelProvider(withActionsProvider(FieldOverview));
