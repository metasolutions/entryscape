import { getLabel } from 'commons/util/rdfUtils';
import { getProperty } from 'models/utils/property';
import {
  extractFieldsAndForms,
  findForm,
  getFormEntries,
  getFormLabel,
  getItemLabels,
  getSingleClassConstraint,
} from './formDataUtils';
import { getLinkLabelSetter } from './diagramUtils';
import { LABEL_TYPE_URI } from './definitions';

class FormCellStore {
  constructor({
    context,
    diagramUtils,
    graph,
    includeImplicitClasses,
    labelType,
    namespaceStore,
    translate,
  }) {
    this._context = context;
    this._translate = translate;
    this._diagramUtils = diagramUtils;
    this._graph = graph;
    this._includeImplicitClasses = includeImplicitClasses;
    this._labelType = labelType;
    this._namespaceStore = namespaceStore;
    this._resourceURI2Rectangle = {};
    this._rectangleCells = [];
    this._linkCells = [];
  }

  _addRectangleCell(rectangle) {
    this._graph.addCell(rectangle);
    this._rectangleCells.push(rectangle);
  }

  _addLinkCell(link) {
    this._graph.addCell(link);
    this._linkCells.push(link);
  }

  async _addRectangleField(rectangle, newField) {
    const fieldDefs = rectangle.get('fieldDefs');
    rectangle.set('fieldDefs', [...fieldDefs, newField]);
    const itemLabels = await getItemLabels(
      rectangle.get('fieldDefs'),
      this._labelType,
      this._namespaceStore
    );
    rectangle.set('fields', itemLabels);
    rectangle.updateRectangle();
    rectangle.resizeWidth(this._diagramUtils.measureText);
  }

  async _addClassTarget(sourceRectangle, itemEntry, linkItem) {
    const classConstraint = await getSingleClassConstraint(itemEntry);
    if (!classConstraint) return;
    let targetRectangle = this._graph
      .getElements()
      .find((element) => element.prop('class') === classConstraint);
    if (targetRectangle) return targetRectangle;
    // add implicit class target rectangle
    if (this._includeImplicitClasses) {
      targetRectangle = this._diagramUtils.createRectangleFromURI(
        classConstraint,
        [],
        this._namespaceStore.shortenKnown(classConstraint),
        []
      );
      this._addRectangleCell(targetRectangle);
      return targetRectangle;
    }
    // no class target can be used, add field to source rectangle as fallback
    this._addRectangleField(sourceRectangle, linkItem);
  }

  async _addLinkAndTargetRectangle(linkItem) {
    const { formEntry, itemEntry } = linkItem;
    const targetFormEntry = await findForm(itemEntry, this._formEntries);
    const sourceRectangle =
      this._resourceURI2Rectangle[formEntry.getResourceURI()];
    let targetRectangle;
    if (targetFormEntry) {
      targetRectangle =
        this._resourceURI2Rectangle[targetFormEntry.getResourceURI()];
    } else {
      targetRectangle = await this._addClassTarget(
        sourceRectangle,
        itemEntry,
        linkItem
      );
    }
    if (targetRectangle && sourceRectangle) {
      const property = await getProperty(itemEntry);
      const label =
        this._labelType === LABEL_TYPE_URI ? property : getLabel(itemEntry);
      const link = this._diagramUtils.createLinkFromField(
        formEntry,
        itemEntry,
        property,
        this._namespaceStore.shortenKnown(label), // TODO, does not take into account overridden values
        sourceRectangle.id,
        targetRectangle.id
      );
      this._addLinkCell(link);
    }
  }

  async _addRectangle(formEntry, fields) {
    const itemLabels = await getItemLabels(
      fields,
      this._labelType,
      this._namespaceStore
    );
    const formLabel = await getFormLabel(
      formEntry,
      this._labelType,
      this._namespaceStore
    );
    const rectangle = this._diagramUtils.createRectangleFromForm(
      formEntry,
      fields,
      formLabel,
      itemLabels
    );

    const cls = await getSingleClassConstraint(formEntry);
    if (cls) {
      rectangle.prop('class', cls);
    }

    this._resourceURI2Rectangle[formEntry.getResourceURI()] = rectangle;
    this._addRectangleCell(rectangle);
  }

  async labelByType(labelType) {
    this._labelType = labelType;
    for (const rectangle of this._rectangleCells) {
      const fields = rectangle.get('fieldDefs');
      const itemLabels = await getItemLabels(
        fields,
        labelType,
        this._namespaceStore
      );
      const label = await getFormLabel(
        rectangle.get('formEntry'),
        this._labelType,
        this._namespaceStore
      );
      rectangle.set('label', label);
      rectangle.set('fields', itemLabels);
      rectangle.updateRectangle();
      rectangle.resizeWidth(this._diagramUtils.measureText);
    }

    const setLabel = getLinkLabelSetter(labelType);
    this._linkCells.forEach((linkCell) => {
      setLabel(linkCell, this._namespaceStore);
    });
  }

  async init() {
    this._formEntries = await getFormEntries(this._context);
    if (!this._formEntries.length) return;

    const linkItems = [];

    for (const formEntry of this._formEntries) {
      const [fields, forms] = await extractFieldsAndForms(
        formEntry,
        this._translate
      );
      forms.forEach((formAndItemPair) => {
        linkItems.push(formAndItemPair);
      });
      await this._addRectangle(formEntry, fields);
    }

    for (const linkItem of linkItems) {
      await this._addLinkAndTargetRectangle(linkItem);
    }
  }
}

export default FormCellStore;
