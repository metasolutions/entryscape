import PropTypes from 'prop-types';
import { Entry } from '@entryscape/entrystore-js';
import { Presenter } from 'commons/components/forms/presenters';
import { useEntry } from 'commons/hooks/useEntry';
import ContentWrapper from 'commons/components/common/ContentWrapper';
import ListActionDialog from 'commons/components/common/dialogs/ListActionDialog';
import { useTranslation } from 'commons/hooks/useTranslation';
import escaDatasetNLS from 'catalog/nls/escaDataset.nls';
import useLinkOrFile from 'commons/components/entry/Import/useLinkOrFile';
import FileUpload from 'commons/components/common/FileUpload';
import useAsync, { PENDING } from 'commons/hooks/useAsync';
import LoadingButton from 'commons/components/LoadingButton';
import { replaceFile } from 'catalog/datasets/DatasetOverview/utils/distributions';
import {
  getVisualizationsForFile,
  removeVisualizationsEntriesFromDataset,
} from 'catalog/datasets/DatasetOverview/utils/visualizations';
import useGetMainDialog from 'commons/hooks/useGetMainDialog';
import { useEffect } from 'react';

const ReplaceFileDialog = ({
  distributionEntry,
  closeDialog,
  entry: fileEntry,
  refreshFileEntries,
  setEditedFiles,
}) => {
  const datasetEntry = useEntry();
  const { file, handleFileChange } = useLinkOrFile();
  const { data: visualizationEntries, runAsync } = useAsync(null);
  const { runAsync: runReplaceFile, status: replaceFileStatus } = useAsync();
  const translate = useTranslation(escaDatasetNLS);
  const { getConfirmationDialog } = useGetMainDialog();

  useEffect(() => {
    runAsync(getVisualizationsForFile(fileEntry, datasetEntry));
  }, [runAsync, fileEntry, datasetEntry]);

  const handleAssociatedVisualizations = async () => {
    if (!visualizationEntries.length) return;

    return getConfirmationDialog({
      content: translate('replaceFileHasVisualization'),
      rejectLabel: translate('removeVizButtonLabel'),
      affirmLabel: translate('keepVizButtonLabel'),
      // Need backdrop click to behave like affirmation
      backdropClickPromiseResolution: true,
    })
      .then((keepVisualizations) => {
        if (keepVisualizations) return;

        // Remove visualizations associated with the file being replaced.
        removeVisualizationsEntriesFromDataset(
          visualizationEntries,
          datasetEntry
        );
      })
      .then(() => closeDialog());
  };

  const handleReplaceFile = () =>
    runReplaceFile(
      replaceFile(fileEntry, distributionEntry, file).then(() => {
        setEditedFiles(true);
        refreshFileEntries();
        handleAssociatedVisualizations();
        closeDialog();
      })
    );

  const dialogActions = (
    <LoadingButton
      autoFocus
      onClick={handleReplaceFile}
      loading={replaceFileStatus === PENDING}
      disabled={!file}
    >
      {translate('replaceFile')}
    </LoadingButton>
  );

  return (
    <ListActionDialog
      id="replace-file"
      title={translate('replaceFileTitle')}
      actions={dialogActions}
      closeDialog={closeDialog}
      maxWidth="sm"
    >
      <Presenter
        label={translate('currentFileLabel')}
        getValue={() => fileEntry.getEntryInfo().getLabel()}
      />
      <ContentWrapper md={12}>
        <FileUpload onSelectFile={handleFileChange} />
      </ContentWrapper>
    </ListActionDialog>
  );
};

ReplaceFileDialog.propTypes = {
  distributionEntry: PropTypes.instanceOf(Entry),
  closeDialog: PropTypes.func.isRequired,
  entry: PropTypes.instanceOf(Entry),
  refreshFileEntries: PropTypes.func,
  setEditedFiles: PropTypes.func,
};

export default ReplaceFileDialog;
