import { useEffect, useState } from 'react';
import ListActionDialog from 'commons/components/common/dialogs/ListActionDialog';
import PropTypes from 'prop-types';
import { Button } from '@mui/material';
import escoRdformsNLS from 'commons/nls/escoRdforms.nls';
import {
  Form,
  FormOutline,
  useEditorContext,
} from 'commons/components/forms/editors';
import useAsync from 'commons/hooks/useAsync';
import { useTranslation } from 'commons/hooks/useTranslation';
import esmoCommonsNls from 'models/nls/esmoCommons.nls';
import esmoFieldsNls from 'models/nls/esmoFields.nls';
import escoListNls from 'commons/nls/escoList.nls';
import escoDialogsNLS from 'commons/nls/escoDialogs.nls';
import { FieldEditor } from 'models/editors';
import { isExtended as checkIsExtended } from 'models/utils/extension';
import { useSnackbar, SUCCESS_EDIT } from 'commons/hooks/useSnackbar';
import { ErrorWithMessage } from 'commons/errors/utils/errors';
import { ErrorCatcher } from 'commons/errors/ErrorCatcher';
import { useConfirmCloseAction } from 'commons/components/EntryListView/dialogs/create/handlers';
import DialogTwoColumnLayout, {
  PrimaryColumn,
  SecondaryColumn,
} from 'commons/components/common/dialogs/DialogTwoColumnLayout';
import editSections, { extendedFieldEditors } from '../../utils/fieldEditors';

const NLS_BUNDLES = [
  escoListNls,
  esmoFieldsNls,
  esmoCommonsNls,
  escoDialogsNLS,
  escoRdformsNLS,
];

const EditFieldDialog = ({ closeDialog, onEntryEdit }) => {
  const translate = useTranslation(NLS_BUNDLES);
  const { runAsync, error: saveError } = useAsync();
  const [hasChange, setHasChange] = useState(false);
  const {
    graph,
    entry: fieldEntry,
    fieldSet,
    validate,
    canSubmit,
  } = useEditorContext();
  const title =
    graph?.findFirstValue(fieldEntry.getResourceURI(), 'dcterms:title') || '';
  const [addSnackbar] = useSnackbar();
  const confirmClose = useConfirmCloseAction(closeDialog);
  const isExtended = checkIsExtended(
    fieldEntry.getMetadata(),
    fieldEntry.getResourceURI()
  );

  useEffect(() => {
    if (graph) {
      graph.onChange = () => {
        setHasChange(true);
      };
    }
  }, [graph]);

  const handleSaveForm = () => {
    const errors = validate(fieldSet);
    if (errors.length) {
      return;
    }

    runAsync(
      fieldEntry
        .setMetadata(graph)
        .commitMetadata()
        .then(() => closeDialog())
        .then(() => onEntryEdit?.())
        .then(() => addSnackbar({ type: SUCCESS_EDIT }))
        .catch((error) => {
          throw new ErrorWithMessage(translate('saveEditsFail'), error);
        })
    );
  };

  const actions = (
    <Button
      autoFocus
      onClick={handleSaveForm}
      disabled={!(hasChange && canSubmit)}
    >
      {translate('save')}
    </Button>
  );

  const fields = isExtended ? extendedFieldEditors : editSections;

  return (
    <>
      <ListActionDialog
        closeDialog={() => confirmClose(hasChange)}
        id="edit-entry"
        actions={actions}
        title={translate('editHeader', title)}
        fixedHeight
      >
        {!isExtended ? (
          <DialogTwoColumnLayout>
            <PrimaryColumn>
              <Form
                nlsBundles={NLS_BUNDLES}
                size="small"
                fields={fields}
                FieldGroupEditor={FieldEditor}
                editorProps={{
                  isExtended,
                }}
              />
            </PrimaryColumn>
            <SecondaryColumn>
              <FormOutline root="edit-entry" />
            </SecondaryColumn>
          </DialogTwoColumnLayout>
        ) : (
          <Form
            nlsBundles={NLS_BUNDLES}
            size="small"
            fields={fields}
            FieldGroupEditor={FieldEditor}
            editorProps={{
              isExtended,
            }}
          />
        )}
      </ListActionDialog>
      <ErrorCatcher error={saveError} />
    </>
  );
};

EditFieldDialog.propTypes = {
  closeDialog: PropTypes.func,
  onEntryEdit: PropTypes.func,
};

export default EditFieldDialog;
