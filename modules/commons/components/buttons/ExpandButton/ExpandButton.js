import PropTypes from 'prop-types';
import { useState } from 'react';
import { Button, Collapse } from '@mui/material';
import {
  ExpandLess as ExpandLessIcon,
  ExpandMore as ExpandMoreIcon,
} from '@mui/icons-material';
import {
  useTranslation,
  nlsBundlesPropType,
} from 'commons/hooks/useTranslation';
import Tooltip from 'commons/components/common/Tooltip';
import './ExpandButton.scss';

const ExpandButton = ({
  PresenterComponent,
  initiallyExpanded,
  nlsBundles = [],
  alternativeClass,
  collapseClasses = null,
  children,
}) => {
  const [expanded, setExpanded] = useState(initiallyExpanded);
  const translate = useTranslation(nlsBundles);

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };

  return (
    <>
      <Tooltip title={translate('buttonTooltip')}>
        <Button
          classes={{
            root: alternativeClass || 'escoExpandButton__button',
          }}
          variant="text"
          endIcon={expanded ? <ExpandLessIcon /> : <ExpandMoreIcon />}
          onClick={handleExpandClick}
          aria-expanded={expanded}
          aria-label={
            expanded ? translate('hideMetadata') : translate('showMetadata')
          }
        >
          {expanded ? translate('hideMetadata') : translate('showMetadata')}
        </Button>
      </Tooltip>
      <Collapse
        classes={
          collapseClasses || {
            container: 'escoExpandButton__collapse',
          }
        }
        in={expanded}
        timeout="auto"
        unmountOnExit
      >
        {PresenterComponent ? <PresenterComponent /> : children}
      </Collapse>
    </>
  );
};

ExpandButton.propTypes = {
  PresenterComponent: PropTypes.func,
  initiallyExpanded: PropTypes.bool,
  nlsBundles: nlsBundlesPropType,
  alternativeClass: PropTypes.string,
  collapseClasses: PropTypes.shape({}),
  children: PropTypes.node,
};

export default ExpandButton;
