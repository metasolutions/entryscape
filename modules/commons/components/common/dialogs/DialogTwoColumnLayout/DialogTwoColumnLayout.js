import PropTypes from 'prop-types';
import { Grid } from '@mui/material';
import './index.scss';

const DialogTwoColumnLayout = ({ children }) => {
  return (
    <Grid container className="escoDialogTwoColumnLayout">
      {children}
    </Grid>
  );
};

DialogTwoColumnLayout.propTypes = {
  children: PropTypes.node,
};

export const PrimaryColumn = ({ children, xs = 9 }) => {
  return (
    <Grid item xs={xs} className="escoDialogTwoColumnLayout__primaryColumn">
      {children}
    </Grid>
  );
};

PrimaryColumn.propTypes = {
  children: PropTypes.node,
  xs: PropTypes.number,
};

export const SecondaryColumn = ({ children, xs = 3 }) => {
  return (
    <Grid item xs={xs} className="escoDialogTwoColumnLayout__secondaryColumn">
      {children}
    </Grid>
  );
};

SecondaryColumn.propTypes = {
  children: PropTypes.node,
  xs: PropTypes.number,
};

export default DialogTwoColumnLayout;
