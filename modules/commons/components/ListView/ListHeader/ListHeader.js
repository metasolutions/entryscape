import PropTypes from 'prop-types';
import { Grid, ListItem, ListItemText } from '@mui/material';
import { ListItemSort } from './ListItemSort';
import { useListView } from '../hooks/useListView';
import './ListHeader.scss';

// Base list header item. Can be used for customized header items.
export const ListHeaderItem = ({ xs = 3, xl = false, children, title }) => {
  return (
    <Grid
      container
      item
      xs={xs}
      xl={xl}
      classes={{ root: 'escoListViewHeader__gridItem' }}
      title={title}
    >
      {children}
    </Grid>
  );
};

ListHeaderItem.propTypes = {
  xl: PropTypes.number,
  xs: PropTypes.number,
  children: PropTypes.node,
  title: PropTypes.string,
};

// Standard list header item. If sorting is needed, see ListHeaderItemSort.
export const ListHeaderItemText = ({ xs, xl, text, title, justifyContent }) => {
  return (
    <ListHeaderItem xs={xs} xl={xl} text={text} title={title}>
      <ListItemText
        primary={text}
        primaryTypographyProps={{ justifyContent }}
        classes={{
          primary: 'escoListViewHeaderItem__primary',
        }}
      />
    </ListHeaderItem>
  );
};

ListHeaderItemText.propTypes = {
  xl: PropTypes.number,
  xs: PropTypes.number,
  text: PropTypes.string,
  title: PropTypes.string,
  justifyContent: PropTypes.string,
};

// List header item with sorting
export const ListHeaderItemSort = ({
  xs,
  xl,
  sortBy,
  sortTooltipAlias,
  text,
  title,
}) => {
  return (
    <ListHeaderItem xs={xs} xl={xl} text={text} title={title}>
      <ListItemSort
        text={text}
        sortBy={sortBy}
        tooltipAlias={sortTooltipAlias}
      />
    </ListHeaderItem>
  );
};

ListHeaderItemSort.propTypes = {
  xl: PropTypes.number,
  xs: PropTypes.number,
  sortBy: PropTypes.string.isRequired,
  sortTooltipAlias: PropTypes.string,
  text: PropTypes.string.isRequired,
  title: PropTypes.string,
};

// Container for list header items.
export const ListHeader = ({ children }) => {
  const { size } = useListView();

  if (size === 0) return null;

  return (
    <ListItem className="escoListViewHeader" key="header">
      <Grid container spacing={2} className="escoListViewHeader__gridContainer">
        {children}
      </Grid>
    </ListItem>
  );
};

ListHeader.propTypes = {
  children: PropTypes.node,
};
