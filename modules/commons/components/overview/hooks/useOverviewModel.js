import { createContext, useContext, useReducer } from 'react';
import PropTypes from 'prop-types';
import { REFRESH } from 'commons/actions/actionTypes';
import withActionCallback from 'commons/actions/hocs/withActionCallback';

const OverviewModelContext = createContext();

const OVERVIEW_MODEL_DEFAULT_VALUE = {
  refreshCount: 0,
};

export { REFRESH };

const overviewModelReducer = (state, action) => {
  switch (action.type) {
    case REFRESH:
      return {
        ...state,
        refreshCount: state.refreshCount + 1,
      };
    default:
      throw new Error(`${action.type} is not a valid list action type`);
  }
};

const OverviewModelProvider = ({
  children,
  initialValue = OVERVIEW_MODEL_DEFAULT_VALUE,
}) => {
  const [overviewModel, dispatch] = useReducer(
    overviewModelReducer,
    initialValue
  );

  return (
    <OverviewModelContext.Provider value={[overviewModel, dispatch]}>
      {children}
    </OverviewModelContext.Provider>
  );
};

OverviewModelProvider.propTypes = {
  children: PropTypes.node,
  initialValue: PropTypes.shape({}),
};

export const useOverviewModel = () => {
  const context = useContext(OverviewModelContext);
  if (context === undefined)
    throw new Error(
      'OverviewModel context must be used within context provider'
    );
  return context;
};

const OverviewUsingProvider = ({ Component, overviewProps }) => {
  useOverviewModel();
  return <Component {...overviewProps} />;
};

OverviewUsingProvider.propTypes = {
  Component: PropTypes.func,
  overviewProps: PropTypes.shape({}),
};

export const withOverviewModelProvider =
  (Component, initialValue = {}) =>
  (overviewProps) =>
    (
      <OverviewModelProvider
        initialValue={{ ...OVERVIEW_MODEL_DEFAULT_VALUE, ...initialValue }}
      >
        <OverviewUsingProvider
          Component={Component}
          overviewProps={overviewProps}
        />
      </OverviewModelProvider>
    );

export const withOverviewRefresh = (Dialog, callbackName) =>
  withActionCallback(useOverviewModel, Dialog, callbackName, REFRESH);
