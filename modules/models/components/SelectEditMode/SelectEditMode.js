import PropTypes from 'prop-types';
import { FormControl, InputLabel, MenuItem, Select } from '@mui/material';
import { useUniqueId } from 'commons/hooks/useUniqueId';
import {
  useTranslation,
  nlsBundlesPropType,
} from 'commons/hooks/useTranslation';

const SelectEditMode = ({
  value: selectedValue,
  choices,
  onChange,
  nlsBundles,
}) => {
  const labelId = useUniqueId();
  const selectId = useUniqueId();
  const translate = useTranslation(nlsBundles);

  return (
    <FormControl variant="outlined" size="small" sx={{ minWidth: 120 }}>
      <InputLabel htmlFor={labelId}>{translate('editModeLabel')}</InputLabel>
      <Select
        inputProps={{ id: labelId }}
        id={selectId}
        value={selectedValue}
        label={translate('editModeLabel')}
        onChange={({ target }) => onChange(target.value)}
      >
        {choices.map(({ label, labelNlsKey, value }) => (
          <MenuItem key={value} value={value}>
            {label ?? translate(labelNlsKey)}
          </MenuItem>
        ))}
      </Select>
    </FormControl>
  );
};

SelectEditMode.propTypes = {
  value: PropTypes.string,
  choices: PropTypes.arrayOf(
    PropTypes.shape({ value: PropTypes.string, label: PropTypes.string })
  ),
  onChange: PropTypes.func,
  nlsBundles: nlsBundlesPropType,
};

export default SelectEditMode;
