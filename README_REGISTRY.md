# EntryScape Platform

### Pre-requisites

In order to run and build Entryscape Registry, you will need to have git and yarn installed.
[https://git-scm.com/](https://git-scm.com/)
[https://yarnpkg.com](https://yarnpkg.com)

You will also need to have an Entrystore instance running somewhere (remote with CORS or locally).
You can find out more about installing entrystore at [http://entrystore.org](http://entrystore.org).

Lastly, you will need to create a directory named `theme` that contains your local configuration. Place this folder in `/app/registry/` and make sure that it contains a proper `local.js` file, specifying various configuration options. Use the example configuration `entryscape/app/registry/config/local-example-minimal.js` as a starting point.

As a minimum you need to make sure the `repository` key points to a working EntryStore installation.

### Setup

Once you have cloned this repo and setup a theme and local configuration, install all the necessary dependencies by running:

```
yarn
```

### Building

Building a distributable copy of Registry can be done by running:

```
yarn build:registry
```

The distributable files will be located in `/src/app/registry/dist`

### Running a development server

A development server can be run using webpack-dev-server via the script:

```
yarn dev:registry
```

You can access the running dev server in the browser at `http://localhost:8080`

### Generating Licenses

If you would like to create a file listing all the licenses of dependencies run:

```
yarn print-licenses
```
