import { useCallback, useMemo } from 'react';
import { getDistributionFileRURIs } from 'catalog/datasets/DatasetOverview/utils/distributions';
import { useTranslation } from 'commons/hooks/useTranslation';
import escaApiProgressNLS from 'catalog/nls/escaApiProgress.nls';
import { getPipelineResource } from 'catalog/datasets/DatasetOverview/utils/pipelines';
import { checkStatusOnRepeat } from 'catalog/datasets/DatasetOverview/utils/api';
import { entrystore, entrystoreUtil } from 'commons/store';
import { clone } from 'lodash-es';
import {
  STATUS_DONE,
  STATUS_FAILED,
  STATUS_PROGRESS,
} from 'commons/hooks/useTasksList';
import { INIT_TASK_ID, FILEPROCESS_TASK_ID } from './tasks';
import { createApiDistribution } from './util';

const useActivateAPI = (distributionEntry, datasetEntry, updateTask) => {
  const t = useTranslation(escaApiProgressNLS);
  const fileURIs = useMemo(
    () => getDistributionFileRURIs(distributionEntry),
    [distributionEntry]
  );
  const totalNoFiles = fileURIs.length;

  /**
   * For each of the file URIs execute the pipeline with the append transform sequentially.
   *
   * @param {string[]} fileResourceURIs
   * @param {store/Pipeline} pipelineResource
   * @returns {Promise}
   */
  const processFiles = useCallback(
    async (fileResourceURIs, pipelineResource) => {
      let processedFiles = 1; // One already done in activateAPI
      for (const fileResourceURI of fileResourceURIs) {
        await entrystoreUtil
          .getEntryByResourceURI(fileResourceURI)
          .then((fileEntry) => pipelineResource.execute(fileEntry, {}))
          .then((result) => checkStatusOnRepeat(result[0], t))
          .catch((error) => {
            updateTask(FILEPROCESS_TASK_ID, {
              status: STATUS_FAILED,
              message: t('apiProgressError'),
            });
            throw error;
          });
        processedFiles += 1;
        updateTask(FILEPROCESS_TASK_ID, {
          status: STATUS_PROGRESS,
          message: t('apiFileProcessed', {
            number: processedFiles,
            totalFiles: totalNoFiles,
          }),
        });
      }

      updateTask(FILEPROCESS_TASK_ID, {
        status: STATUS_DONE,
      });
    },
    [t, totalNoFiles, updateTask]
  );

  /**
   *  Activates/generates an API distribution.
   *
   * @returns {Promise}
   */
  const activateAPI = useCallback(async () => {
    let tempFileURIs = clone(fileURIs);

    try {
      updateTask(INIT_TASK_ID, {
        status: STATUS_PROGRESS,
      });

      const pipelineResource = await getPipelineResource(
        distributionEntry.getContext()
      );

      const transformId = pipelineResource.getTransformForType(
        pipelineResource.transformTypes.ROWSTORE
      );
      pipelineResource.setTransformArguments(transformId, {});
      pipelineResource.setTransformArguments(transformId, { action: 'create' });
      await pipelineResource.commit();

      updateTask(INIT_TASK_ID, {
        status: STATUS_DONE,
      });

      updateTask(FILEPROCESS_TASK_ID, {
        status: STATUS_PROGRESS,
      });

      const fileEntry = await entrystoreUtil.getEntryByResourceURI(
        tempFileURIs[0]
      );
      const pipelineResultURIs = await pipelineResource.execute(fileEntry, {});
      await checkStatusOnRepeat(pipelineResultURIs[0], t);
      updateTask(FILEPROCESS_TASK_ID, {
        status: STATUS_PROGRESS,
        message: t('apiFileProcessed', {
          number: 1,
          totalFiles: totalNoFiles,
        }),
      });

      // First file uri has been used to initialize the API, so doesn't need
      // to go through the following file processing steps
      tempFileURIs = tempFileURIs.slice(1);

      if (tempFileURIs.length === 0) {
        await createApiDistribution(
          pipelineResultURIs[0],
          datasetEntry,
          distributionEntry
        );

        updateTask(FILEPROCESS_TASK_ID, {
          status: STATUS_DONE,
        });
      } else {
        const pipelineResultEntry = await entrystore.getEntry(
          pipelineResultURIs[0]
        );
        pipelineResource.setTransformArguments(transformId, {});
        pipelineResource.setTransformArguments(transformId, {
          action: 'append',
          datasetURL: pipelineResultEntry.getResourceURI(),
        });
        await pipelineResource.commit();
        await processFiles(tempFileURIs, pipelineResource);
        await createApiDistribution(
          pipelineResultURIs[0],
          datasetEntry,
          distributionEntry
        );
      }
    } catch (error) {
      updateTask(FILEPROCESS_TASK_ID, {
        status: STATUS_FAILED,
        message: t('apiProgressError'),
      });
      console.error(error);
    }
  }, [
    fileURIs,
    updateTask,
    distributionEntry,
    t,
    totalNoFiles,
    datasetEntry,
    processFiles,
  ]);

  return [activateAPI];
};

export default useActivateAPI;
