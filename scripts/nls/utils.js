const cyan = '\x1b[36m%s\x1b[0m';
const capitalize = (s) => s.charAt(0).toUpperCase() + s.slice(1);
const logGroup = (txt, isCyan = true) => console.group(isCyan ? cyan : '', txt);

const delay = (ms) => new Promise((res) => setTimeout(res, ms));
const getModuleNames = (paths) =>
  paths
    .map((path) => {
      const parts = path.split('/');
      return capitalize(parts[parts.length - 2]);
    })
    .join(', ');

const logProgress = (progress) => {
  process.stdout.clearLine();
  process.stdout.cursorTo(4);
  process.stdout.write(progress);
};

const delayWithProgress = async (time, stages = 10) => {
  const waitPerRound = Math.ceil(time / stages);

  let count = '.';
  logProgress(count);
  await new Array(stages).fill(0).reduce(
    (accumulator) =>
      accumulator.then(() => {
        count += '.';
        logProgress(count);
        return delay(waitPerRound);
      }),
    delay(waitPerRound)
  );

  process.stdout.clearLine();
  process.stdout.cursorTo(0);
};

module.exports = {
  delay,
  delayWithProgress,
  logProgress,
  capitalize,
  logGroup,
  getModuleNames,
};
