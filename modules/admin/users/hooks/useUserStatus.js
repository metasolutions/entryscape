import { useState } from 'react';
import { getIsUserDisabled } from 'commons/util/user';

/**
 * Manages a user's status (enabled/disabled)
 * @param {Entry} userEntry
 * @returns {[boolean, function]}
 */
const useUserStatus = (userEntry) => {
  const [isUserDisabled, setUserStatus] = useState(
    getIsUserDisabled(userEntry)
  );

  const toggleUserStatus = async () => {
    const newUserStatus = !isUserDisabled;

    const userResource = await userEntry.getResource();
    await userResource.setDisabled(newUserStatus);
    setUserStatus(newUserStatus);
  };

  return [isUserDisabled, toggleUserStatus];
};

export default useUserStatus;
