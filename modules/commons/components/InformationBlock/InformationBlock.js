import PropTypes from 'prop-types';
import { Info as InfoIcon } from '@mui/icons-material';
import './InformationBlock.scss';

const InformationBlock = ({ header, info, className }) => (
  <div className={!className ? 'escoInfoBlock' : `escoInfoBlock ${className}`}>
    <InfoIcon color="primary" className="escoInfoBlock__icon" />
    <div className="escoInfoBlock__body">
      <div className="escoInfoBlock__header">{header}</div>
      <div>{info}</div>
    </div>
  </div>
);

InformationBlock.propTypes = {
  header: PropTypes.string,
  info: PropTypes.string,
  className: PropTypes.string,
};

export default InformationBlock;
