import { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { Button } from '@mui/material';
import {
  Form,
  FormOutline,
  useEditorContext,
} from 'commons/components/forms/editors';
import useAsync from 'commons/hooks/useAsync';
import { useTranslation } from 'commons/hooks/useTranslation';
import esmoFormsNls from 'models/nls/esmoForms.nls';
import esmoCommonsNls from 'models/nls/esmoCommons.nls';
import escoListNls from 'commons/nls/escoList.nls';
import escoDialogsNLS from 'commons/nls/escoDialogs.nls';
import ListActionDialog from 'commons/components/common/dialogs/ListActionDialog';
import { FieldEditor } from 'models/editors';
import { isExtended as checkIsExtended } from 'models/utils/extension';
import { ErrorWithMessage } from 'commons/errors/utils/errors';
import { ErrorCatcher } from 'commons/errors/ErrorCatcher';
import { useSnackbar, SUCCESS_EDIT } from 'commons/hooks/useSnackbar';
import { useConfirmCloseAction } from 'commons/components/EntryListView/dialogs/create/handlers';
import DialogTwoColumnLayout, {
  PrimaryColumn,
  SecondaryColumn,
} from 'commons/components/common/dialogs/DialogTwoColumnLayout';
import editSections, { extendedFormEditors } from '../../utils/formEditors';

const NLS_BUNDLES = [esmoFormsNls, escoListNls, esmoCommonsNls, escoDialogsNLS];

const EditFormDialog = ({ closeDialog, onEntryEdit }) => {
  const { runAsync, error: saveError } = useAsync();
  const translate = useTranslation(NLS_BUNDLES);
  const {
    graph,
    entry: formEntry,
    fieldSet,
    validate,
    canSubmit,
  } = useEditorContext();
  const [hasChange, setHasChange] = useState(false);
  const title =
    graph?.findFirstValue(formEntry.getResourceURI(), 'dcterms:title') || '';
  const [addSnackbar] = useSnackbar();
  const confirmClose = useConfirmCloseAction(closeDialog);
  const isExtended = checkIsExtended(
    formEntry.getMetadata(),
    formEntry.getResourceURI()
  );

  useEffect(() => {
    if (graph) {
      graph.onChange = () => {
        setHasChange(true);
      };
    }
  }, [graph]);

  const handleSaveForm = () => {
    const errors = validate(fieldSet);
    if (errors.length) {
      return;
    }

    runAsync(
      formEntry
        .setMetadata(graph)
        .commitMetadata()
        .then(() => closeDialog())
        .then(() => onEntryEdit?.())
        .then(() => addSnackbar({ type: SUCCESS_EDIT }))
        .catch((error) => {
          throw new ErrorWithMessage(translate('saveEditsFail'), error);
        })
    );
  };

  const actions = (
    <Button
      autoFocus
      onClick={handleSaveForm}
      disabled={!(hasChange && canSubmit)}
    >
      {translate('save')}
    </Button>
  );

  return (
    <>
      <ListActionDialog
        closeDialog={() => confirmClose(hasChange)}
        id="edit-entry"
        actions={actions}
        title={translate('editHeader', title)}
        fixedHeight
      >
        {!isExtended ? (
          <DialogTwoColumnLayout>
            <PrimaryColumn>
              <Form
                fields={isExtended ? extendedFormEditors : editSections}
                size="small"
                nlsBundles={NLS_BUNDLES}
                FieldGroupEditor={FieldEditor}
                editorProps={{
                  isExtended,
                }}
              />
            </PrimaryColumn>
            <SecondaryColumn>
              <FormOutline root="edit-entry" />
            </SecondaryColumn>
          </DialogTwoColumnLayout>
        ) : (
          <Form
            fields={isExtended ? extendedFormEditors : editSections}
            size="small"
            nlsBundles={NLS_BUNDLES}
            FieldGroupEditor={FieldEditor}
            editorProps={{
              isExtended,
            }}
          />
        )}
      </ListActionDialog>
      <ErrorCatcher error={saveError} />
    </>
  );
};

EditFormDialog.propTypes = {
  closeDialog: PropTypes.func,
  onEntryEdit: PropTypes.func,
};

export default EditFormDialog;
