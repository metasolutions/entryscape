import { withListRefresh } from 'commons/components/ListView';
import esteCollectionNLS from 'terms/nls/esteCollection.nls';
import esteConceptNLS from 'terms/nls/esteConcept.nls';
import escoListNLS from 'commons/nls/escoList.nls';
import CreateCollectionDialog from './dialogs/CreateCollectionDialog';

export const nlsBundles = [esteCollectionNLS, esteConceptNLS, escoListNLS];

export const listActions = [
  {
    id: 'create',
    Dialog: withListRefresh(CreateCollectionDialog, 'onCreate'),
    labelNlsKey: 'createButtonLabel',
    tooltipNlsKey: 'actionTooltip',
  },
];
