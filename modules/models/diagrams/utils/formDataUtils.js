import { Entry, Context } from '@entryscape/entrystore-js';
import { getExtendedEntry } from 'models/utils/extension';
import { hasValue } from 'models/components/Extends';
import { extractConstraints } from 'models/utils/constraint';
import { matchConstraints } from 'commons/types/utils/constraints';
import {
  RDF_TYPE_PROFILE_FORM,
  RDF_TYPE_PROPERTY_FORM,
  RDF_TYPE_OBJECT_FORM,
  RDF_PROPERTY_CONSTRAINT,
  RDF_PROPERTY_ITEM,
  RDF_TYPE_LOOKUP_FIELD,
  RDF_TYPE_INLINE_FIELD,
  RDF_TYPE_SECTION_FORM,
  RDF_PROPERTY_LABEL,
} from 'models/utils/ns';
import { entrystore } from 'commons/store';
import {
  getFormItemEntries,
  getLabelFromExtended,
} from 'models/forms/utils/util';
import { getNLSAndRDFType } from 'models/utils/metadata';
import { isInlineField } from 'models/fields/utils/fields';
import { getObjectFormEntry } from 'models/builds/utils/dependencies';
import { getProperty } from 'models/utils/property';
import { getLabel } from 'commons/util/rdfUtils';
import { namespaces } from '@entryscape/rdfjson';
import { LABEL_TYPE_LABEL, LABEL_TYPE_URI } from './definitions';

/**
 * When extracting fields and forms, section forms should be excluded in the end
 * result. The parentEntry param is used to indicate that the form being
 * extracted is a section form and should use the parent form entry instead as a
 * container for the children.
 *
 * @param {Entry} formEntry
 * @param {object[]} fields
 * @param {object[]} forms
 * @param {Function} translate
 * @param {Entry} parentEntry
 */
export const extractFieldsAndFormsRecursively = async (
  formEntry,
  fields,
  forms,
  translate,
  parentEntry
) => {
  const metadata = formEntry.getMetadata();
  const resourceURI = formEntry.getResourceURI();
  const extendedItems = await getFormItemEntries(metadata, resourceURI);
  const itemsWithType = extendedItems.map((formItemEntry) => ({
    itemEntry: formItemEntry,
    nlsAndType: getNLSAndRDFType({ translate, entry: formItemEntry }),
  }));

  for (const { itemEntry, nlsAndType } of itemsWithType) {
    switch (nlsAndType.rdfType) {
      case RDF_TYPE_LOOKUP_FIELD:
      case RDF_TYPE_INLINE_FIELD:
      case RDF_TYPE_PROPERTY_FORM:
        forms.push({ formEntry: parentEntry || formEntry, itemEntry });
        break;
      case RDF_TYPE_SECTION_FORM:
        await extractFieldsAndFormsRecursively(
          itemEntry,
          fields,
          forms,
          translate,
          formEntry
        );
        break;
      default:
        fields.push({ formEntry, itemEntry });
    }
  }
};

/**
 * Finds all items in a form and separates those that are fields and those that are forms.
 * Also flattens all section forms.
 *
 * @param {Entry} formEntry
 * @param {Function} translate
 */
export const extractFieldsAndForms = async (formEntry, translate) => {
  const fields = [];
  const forms = [];
  await extractFieldsAndFormsRecursively(formEntry, fields, forms, translate);
  return [fields, forms];
};

/**
 * Find first rdf type in constraint
 *
 * @param {Entry} formEntry
 * @returns {Promise<string|undefined>}
 */
export const getSingleClassConstraint = async (formEntry) => {
  const constraintDefinedEntry = await getExtendedEntry(
    formEntry.getMetadata(),
    formEntry.getResourceURI(),
    RDF_PROPERTY_CONSTRAINT,
    hasValue
  );
  const constraintsEntry = constraintDefinedEntry || formEntry;
  const constraints = (await extractConstraints(constraintsEntry)) || {};
  const classURIs = constraints[namespaces.expand('rdf:type')];
  if (classURIs && Object.keys(constraints).length === 1) {
    return Array.isArray(classURIs) ? classURIs[0] : classURIs;
  }
};

/**
 * Get label from form entry depending on label type
 *
 * @param {Entry} formEntry
 * @param {string} labelType
 * @param {object} namespaceStore
 * @returns {Promise<string>}
 */
export const getFormLabel = async (formEntry, labelType, namespaceStore) => {
  // label type uri
  if (labelType === LABEL_TYPE_URI) {
    const rdfType = await getSingleClassConstraint(formEntry);
    if (!rdfType) return getLabel(formEntry);
    return namespaceStore.shortenKnown(rdfType);
  }
  let labelProperties;
  // label type label
  if (labelType === LABEL_TYPE_LABEL) {
    labelProperties = [RDF_PROPERTY_LABEL, 'dcterms:title'];
  }
  // label type title
  return getLabel(formEntry, null, labelProperties);
};

/**
 * Get label from class entry depending on label type
 *
 * @param {Entry} classEntry
 * @param {string} labelType
 * @param {object} namespaceStore
 * @returns {Promise<string>}
 */
export const getClassLabel = (classEntry, labelType, namespaceStore) => {
  if (labelType !== LABEL_TYPE_URI) return getLabel(classEntry);
  return namespaceStore.shortenKnown(classEntry.getResourceURI());
};

/**
 * Get labels from form item entries depending on label type
 *
 * @param {Entry[]} formAndItemPairs
 * @param {string} labelType
 * @param {object} namespaceStore
 * @returns {Promise<string[]>}
 */
export const getItemLabels = async (
  formAndItemPairs,
  labelType,
  namespaceStore
) => {
  if (labelType === LABEL_TYPE_URI) {
    const itemLabels = [];
    for (const { itemEntry } of formAndItemPairs) {
      const property = await getProperty(itemEntry);
      itemLabels.push(namespaceStore.shortenKnown(property));
    }
    return itemLabels;
  }

  let labelProperties;
  if (labelType === LABEL_TYPE_LABEL) {
    labelProperties = [RDF_PROPERTY_LABEL, 'dcterms:title'];
  }
  return formAndItemPairs.map(({ formEntry, itemEntry }) =>
    getLabelFromExtended(formEntry.getMetadata(), itemEntry, labelProperties)
  );
};

/**
 *
 * @param {Entry} itemEntry
 * @param {Entry[]} formEntries
 * @returns {Promise<Entry>}
 */
export const findForm = async (itemEntry, formEntries) => {
  if (isInlineField(itemEntry)) {
    return getObjectFormEntry(itemEntry);
  }

  const constraintDefinedEntry = await getExtendedEntry(
    itemEntry.getMetadata(),
    itemEntry.getResourceURI(),
    RDF_PROPERTY_CONSTRAINT,
    hasValue
  );
  const constraintsEntry = constraintDefinedEntry || itemEntry;
  const constraints = extractConstraints(constraintsEntry) || {};

  let bestProfile;
  let bestProfileWeight = 0;
  formEntries.forEach((profileFormEntry) => {
    const profileConstraints = extractConstraints(profileFormEntry) || {};
    const weight = matchConstraints(constraints, profileConstraints);
    if (bestProfileWeight < weight) {
      bestProfileWeight = weight;
      bestProfile = profileFormEntry;
    }
  });
  return bestProfile;
};

/**
 *
 * @param {Context} context
 * @returns {Entry[]}
 */
export const getFormEntries = async (context) => {
  // Find all profileForms and propertyForms.
  const formEntries = await entrystore
    .newSolrQuery()
    .context(context)
    .rdfType([
      RDF_TYPE_PROFILE_FORM,
      RDF_TYPE_PROPERTY_FORM,
      RDF_TYPE_OBJECT_FORM,
    ])
    .list()
    .getAllEntries();

  // Ignore propertyForms without items (typically those that are extending another form)
  return formEntries.filter((formEntry) => {
    const resourceURI = formEntry.getResourceURI();
    const metadata = formEntry.getMetadata();
    return metadata.find(resourceURI, 'rdf:type', RDF_TYPE_PROPERTY_FORM)
      .length === 1
      ? metadata.find(resourceURI, RDF_PROPERTY_ITEM).length > 0
      : true;
  });
};
