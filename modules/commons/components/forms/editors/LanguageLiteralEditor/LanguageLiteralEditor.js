import PropTypes from 'prop-types';
import { useTranslation } from 'commons/hooks/useTranslation';
import escoFormsNls from 'commons/nls/escoForms.nls';
import { graphPropType } from 'commons/util/entry';
import { LANGUAGE_LITERAL } from 'commons/components/forms/utils/nodetypes';
import { useFieldsEdit } from '../hooks/useFieldsEdit';
import { Fields } from '../Fields';
import { Field } from '../Field';
import { TextField } from '../TextField';
import { LanguageSelect } from '../LanguageSelect';

export const LanguageLiteralEditor = ({
  property,
  xs = 8,
  size,
  multiline,
  graph,
  resourceURI,
  dispatch,
  onChange,
  error,
  helperText,
  children,
  validate,
  ...fieldsProps
}) => {
  const t = useTranslation(escoFormsNls);
  const {
    fields,
    labelId,
    addField,
    removeField,
    updateValue,
    updateLanguage,
  } = useFieldsEdit({
    name: fieldsProps.name,
    property,
    mandatory: fieldsProps.mandatory,
    dispatch,
    onChange,
    nodetype: LANGUAGE_LITERAL,
    graph,
    resourceURI,
    validate,
  });

  const clearValueAndLanguage = (field) => {
    updateValue(field, '');
    updateLanguage(field, '');
  };

  return (
    <Fields labelId={labelId} addField={addField} {...fieldsProps}>
      {children}
      {fields.map((field) => {
        return (
          <Field
            key={field.getId()}
            size={fields.length}
            spacing={2}
            onRemove={() => removeField(field)}
            onClear={() => clearValueAndLanguage(field, '')}
          >
            <TextField
              xs={xs}
              inputProps={{ 'aria-labelledby': labelId }}
              onChange={({ target }) => updateValue(field, target.value)}
              value={field.getValue() || ''}
              error={error || field.hasError()}
              helperText={
                helperText || (field.hasError() ? t(field.getError()) : '')
              }
              size={size}
              multiline={multiline}
            />
            <LanguageSelect
              labelId={labelId}
              language={field.getLanguage() || ''}
              updateLanguage={({ target }) =>
                updateLanguage(field, target.value)
              }
              helperText={error || field.hasError() ? ' ' : ''}
              size={size}
            />
          </Field>
        );
      })}
    </Fields>
  );
};

LanguageLiteralEditor.propTypes = {
  children: PropTypes.node,
  property: PropTypes.string,
  xs: PropTypes.number,
  size: PropTypes.string,
  multiline: PropTypes.bool,
  graph: graphPropType,
  resourceURI: PropTypes.string,
  dispatch: PropTypes.func,
  onChange: PropTypes.func,
  error: PropTypes.string,
  helperText: PropTypes.string,
  validate: PropTypes.func,
};
