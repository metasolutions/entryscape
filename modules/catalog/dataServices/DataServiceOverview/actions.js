import {
  ACTION_EDIT,
  ACTION_REVISIONS,
  ACTION_REMOVE,
} from 'commons/components/overview/actions';
import RemoveDataServiceDialog from '../dialogs/RemoveDataServiceDialog';

const sidebarActions = [
  ACTION_EDIT,
  ACTION_REVISIONS,
  { ...ACTION_REMOVE, Dialog: RemoveDataServiceDialog },
];

export { sidebarActions };
