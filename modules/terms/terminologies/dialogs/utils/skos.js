import { Graph } from '@entryscape/rdfjson';
import { entrystore } from 'commons/store';
import { isUri } from 'commons/util/util';
import config from 'config';

/**
 *
 * @param {object} params
 * @returns {object}
 */
export const fixURIs = (params) => {
  const { graph, conceptSchemeURI } = params;
  const replaceConfig = config.get('terms.replace');
  if (replaceConfig == null) return params;

  let counter = 0;
  let onlyCounter = false;
  const ids = {};
  let conceptSchemeURIReplaced = false;
  params.newURIs = [];
  params.anythingReplaced = false;
  let conceptSchemeStatements;

  // Find all terminology statements with the configured predicate and datatype
  if (replaceConfig.matchPredicate) {
    conceptSchemeStatements = graph.find(null, replaceConfig.matchPredicate);
    if (replaceConfig.matchDatatype) {
      conceptSchemeStatements = conceptSchemeStatements.filter(
        (statement) => statement.getDatatype() === replaceConfig.matchDatatype
      );
    }
  }

  if (
    conceptSchemeStatements.length === 0 &&
    (replaceConfig.URIRegexp || replaceConfig.allURIs)
  ) {
    conceptSchemeStatements = graph.find(null, 'rdf:type', 'skos:Concept');
    onlyCounter = true;
    if (replaceConfig.URIRegexp) {
      // Filter the statements to only the ones matching the configuration regex
      conceptSchemeStatements = conceptSchemeStatements.filter((statement) =>
        replaceConfig.URIRegexp.exec(statement.getSubject())
      );
    }
  }

  conceptSchemeStatements.forEach(conceptSchemeStatements, (statement) => {
    const subject = statement.getSubject();
    let id;
    if (onlyCounter) {
      counter += 1;
      id = `${id}concept_${counter}`;
    } else {
      id = statement.getValue();
      if (ids[id]) {
        counter += 1;
        id = `${id}_${counter}`;
      }
    }
    ids[id] = true;
    const newURI = entrystore.getResourceURI(params.homeContextId, id);
    graph.replaceURI(subject, newURI);
    params.newURIs[newURI] = { old: subject, newId: id };
    params.anythingReplaced = true;
    if (conceptSchemeURI === subject) {
      conceptSchemeURIReplaced = true;
      params.conceptSchemeURI = newURI;
    }
  });

  if (!conceptSchemeURIReplaced) {
    if (
      replaceConfig.URIRegexp &&
      (replaceConfig.URIRegexp.exec(conceptSchemeURI) !== null ||
        replaceConfig.allURIs)
    ) {
      const newCSURI = entrystore.getResourceURI(
        params.homeContextId,
        'terminology'
      );
      graph.replaceURI(conceptSchemeURI, newCSURI);
      params.newURIs[newCSURI] = {
        old: params.conceptSchemeURI,
        newId: 'terminology',
      };
      params.conceptSchemeURI = newCSURI;
    }
  }

  return params;
};

/**
 *
 * @param {object} params
 * @returns {object}
 */
export const fixSymmetry = (params) => {
  const { graph } = params;
  const conceptSchemeURI = graph
    .find(null, 'rdf:type', 'skos:ConceptScheme')[0]
    .getSubject();

  const broader = {};
  const topConcepts = graph.find(conceptSchemeURI, 'skos:hasTopConcept') || [];
  topConcepts.forEach((statement) => {
    broader[statement.getValue()] = conceptSchemeURI;
    graph.add(
      statement.getValue(),
      'skos:topConceptOf',
      statement.getSubject()
    );
  });
  const conceptSchemeStatements =
    graph.find(null, 'rdf:type', 'skos:Concept') || [];
  conceptSchemeStatements.forEach((schemeStatement) => {
    const conceptURI = schemeStatement.getSubject();
    graph.add(conceptURI, 'skos:inScheme', conceptSchemeURI);
    const broaderConcepts = graph.find(conceptURI, 'skos:broader') || [];
    broaderConcepts.forEach((statement) => {
      broader[conceptURI] = statement.getValue();
      graph.add(statement.getValue(), 'skos:narrower', conceptURI);
    });
    const topConceptOf = graph.find(conceptURI, 'skos:topConceptOf') || [];
    topConceptOf.forEach((statement) => {
      broader[conceptURI] = statement.getValue();
      graph.add(
        statement.getValue(),
        'skos:hasTopConcept',
        statement.getSubject()
      );
    });
  });

  conceptSchemeStatements.forEach((statement) => {
    const conceptURI = statement.getSubject();
    if (!broader[conceptURI]) {
      graph.add(conceptURI, 'skos:topConceptOf', conceptSchemeURI);
      graph.add(conceptSchemeURI, 'skos:hasTopConcept', conceptURI);
    }
  });

  return params;
};

/**
 * Check if graph has exactly one skos:ConceptScheme and returns its statement.
 * Otherwise throw an error.
 *
 * @param {Graph} graph
 * @param {Function} translate
 * @returns {string}
 */
export const getConceptSchemeURI = (graph, translate) => {
  const stmts = graph.find(null, 'rdf:type', 'skos:ConceptScheme');
  if (stmts.length !== 1) {
    throw Error(translate('noSKOS')); // TODO this is a nlsKeyLabel, not great
  }
  return stmts[0].getSubject();
};

const getShortestElement = (array) =>
  array.reduce((a, b) => (a.length <= b.length ? a : b));

const getLongestElement = (array) =>
  array.reduce((a, b) => (a.length >= b.length ? a : b));

/**
 * Finds the common prefix between an array's elements
 *
 * @param {string[]} array
 * @returns {string}
 */
const getCommonPrefix = (array) => {
  if (!array.length) return '';

  const shortest = getShortestElement(array);
  const longest = getLongestElement(array);
  let commonPrefix = '';
  const minLength = shortest.length;

  for (let i = 0; i < minLength; i++) {
    if (shortest[i] === longest[i]) {
      commonPrefix += shortest[i];
    } else {
      break;
    }
  }

  return commonPrefix.replace(/\/$/, '');
};

/**
 * Gets the URI space from the graph or by determining it from the graph's concepts
 * from the graph's concepts, add that prefix as the uri space
 *
 * @param {Graph} graph
 * @returns {Graph}
 */
export const getUriSpace = (graph) => {
  const graphUriSpace = graph.findFirstValue(null, 'void:uriSpace');
  if (graphUriSpace) return graphUriSpace;

  const conceptStatements = graph.find(null, 'rdf:type', 'skos:Concept');
  const commonPrefix = getCommonPrefix(
    conceptStatements.map((statement) => statement.getSubject())
  );

  return isUri(commonPrefix) ? commonPrefix : '';
};
