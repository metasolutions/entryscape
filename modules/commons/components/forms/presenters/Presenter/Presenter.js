import { useEffect } from 'react';
import PropTypes from 'prop-types';
import { graphPropType } from 'commons/util/entry';
import { Entry } from '@entryscape/entrystore-js';
import useAsync from 'commons/hooks/useAsync';
import { Fields } from '../Fields';
import { Field } from '../Field';
import { Text } from '../Text';

export const Presenter = ({
  entry,
  graph,
  resourceURI,
  property,
  inline,
  children,
  direct = true, // whether getValue is async or not
  getValue,
  translate,
  ...fieldsProps
}) => {
  const { runAsync, data } = useAsync();

  useEffect(() => {
    if (direct) return;
    runAsync(getValue({ entry, graph, resourceURI, property, translate }));
  }, [
    runAsync,
    direct,
    graph,
    entry,
    resourceURI,
    property,
    getValue,
    translate,
  ]);

  const valueOrValues = direct
    ? getValue({ entry, graph, resourceURI, property, translate })
    : data;
  const values = Array.isArray(valueOrValues) ? valueOrValues : [valueOrValues];
  const hasValue = values.every((value) => Boolean(value)) && values.length;

  return hasValue ? (
    // eslint-disable-next-line react/jsx-props-no-spreading
    <Fields inline={inline} {...fieldsProps}>
      {values.map((value, index) => {
        return (
          // eslint-disable-next-line react/no-array-index-key
          <Field key={index} inline={inline}>
            <Text text={value} />
          </Field>
        );
      })}
      {children}
    </Fields>
  ) : null;
};

Presenter.propTypes = {
  entry: PropTypes.instanceOf(Entry),
  graph: graphPropType,
  resourceURI: PropTypes.string,
  property: PropTypes.string,
  direct: PropTypes.bool,
  getValue: PropTypes.func,
  translate: PropTypes.func,
  inline: PropTypes.bool,
  children: PropTypes.node,
};
