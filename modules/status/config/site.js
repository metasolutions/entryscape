import Statistics from 'status/Statistics';
import Overview from 'status/Overview';
import Organizations from 'status/Organizations';

export default {
  modules: [
    {
      name: 'status',
      productName: {
        en: 'Status report',
        sv: 'Statusrapport',
        de: 'Statusbericht',
      },
      startView: 'status__visualization', // compulsory
      title: { en: 'Status report', sv: 'Statusrapport', de: 'Statusbericht' },
      sidebar: true,
      asCrumb: true,
      faClass: 'eye',
      text: {
        sv: 'En automatiserad granskning av vilka offentliga organisationer som redovisar sin öppna data',
        en: 'An automated audit of which public organizations that document their open data',
        de: 'Ein automatisch erstellter Bericht über welche öffentlichen Organisationen ihre offenen Daten dokumentieren',
      },
    },
  ],
  views: [
    {
      name: 'status__visualization',
      class: Overview,
      title: { en: 'Overview', sv: 'Översikt', de: 'Übersicht' },
      route: '/status/overview',
      module: 'status',
      parent: 'status__visualization',
      public: true,
    },
    {
      name: 'status_reports',
      class: Organizations,
      title: {
        en: 'Reports',
        sv: 'Rapporter',
        de: 'Berichte',
      },
      route: '/status/reports',
      module: 'status',
      parent: 'status__visualization',
      public: true,
    },
    {
      name: 'status__statistics',
      class: Statistics,
      title: {
        en: 'Statistics',
        sv: 'Statistik',
        de: 'Statistiken',
      },
      route: '/status/statistics',
      module: 'status',
      parent: 'status__visualization',
      restrictTo: 'admin',
      public: true,
    },
  ],
};
