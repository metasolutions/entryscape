const fs = require('fs');
const path = require('path');
const papa = require('papaparse');

const MODULE_COMMONS = 'Commons';
const MODULES = [
  'Catalog',
  MODULE_COMMONS,
  'Models',
  'Registry',
  'Search',
  'Terms',
  'Workbench',
];

// Jira issue types
const TYPE_NEW_FEATURE = 'New Feature';
const TYPE_TASK = 'Task';
const TYPE_SUBTASK = 'Sub-task';
const TYPE_EPIC = 'Epic';
const TYPE_STORY = 'Story';

// Jira issue properties
const ISSUE_TYPE = 'Issue Type';
const ISSUE_KEY = 'Issue key';
const ISSUE_LABELS = 'Labels';

// Jira issue labels
const LABEL_TECHDEBT = 'Techdebt';

// Changelog-specific
const CH_MAINTENANCE = 'Maintenance';

const TYPES_TO_EXCLUDE = [TYPE_SUBTASK, TYPE_TASK];

const FILE_PATH = path.resolve(__dirname, './jira-export.csv');

/**
 * Gets the labels of a Jira issue.
 * The Jira export stores each label in a separate column labelled `Label`.
 * Parsing ends up calling them `Labels`, `Labels_1`, `Labels_2` etc.
 *
 * @param {object} issue
 * @returns {string[]} labels - the actual issue labels
 */
const getLabels = (issue) => {
  const labelColumns = Object.keys(issue).filter((column) =>
    column.startsWith(ISSUE_LABELS)
  );
  const labels = labelColumns
    .map((labelColumn) => issue[labelColumn])
    .filter(Boolean);
  return labels;
};

/**
 * Determines the type an issue will be under in the changelog.
 * If the issue is meant to be excluded from the changelog an undefined type is returned.
 *
 * @param {object} issue
 * @returns {string}
 */
const getChangelogType = (issue) => {
  const labels = getLabels(issue);
  const type = issue[ISSUE_TYPE];

  if (type === TYPE_EPIC || type === TYPE_STORY) return TYPE_NEW_FEATURE;
  if (type === TYPE_TASK && labels.includes(LABEL_TECHDEBT))
    return CH_MAINTENANCE;
  if (TYPES_TO_EXCLUDE.includes(type)) return;
  return type;
};

/**
 *
 * @param {object[]} issues
 * @returns {object} issuesMap - the issues grouped by module and type
 */
const groupIssues = (issues) => {
  const groupedIssuesMap = {};
  MODULES.forEach((esModule) => {
    groupedIssuesMap[esModule] = {};
  });
  issues.forEach((issue) => {
    const changelogType = getChangelogType(issue);
    if (!changelogType) return;

    const esModule = issue.Components || MODULE_COMMONS;
    if (!groupedIssuesMap[esModule].hasOwnProperty(changelogType)) {
      groupedIssuesMap[esModule][changelogType] = [];
    }
    groupedIssuesMap[esModule][changelogType].push(issue);
  });

  return groupedIssuesMap;
};

/**
 *
 * @param {object} issuesMap
 */
const generateChangelog = (issuesMap) => {
  MODULES.forEach((esModule) => {
    const includeModule = Object.keys(issuesMap[esModule]).some(
      (type) => issuesMap[esModule][type].length
    );
    if (!includeModule) return;

    console.log(`### ${esModule}\n`);
    Object.keys(issuesMap[esModule]).forEach((type) => {
      const issues = issuesMap[esModule][type];
      if (!issues.length) return;

      console.log(`#### ${type}\n`);
      issues.forEach((issue) => {
        const title = issue.Summary;
        const key = issue[ISSUE_KEY];
        console.log(
          `- ${title} ([${key}](https://metasolutions.atlassian.net/browse/${key}))`
        );
      });
      console.log('\n');
    });
  });
};

(async () => {
  console.log(`Reading ${FILE_PATH}`);
  const csvFile = fs.readFileSync(FILE_PATH, 'utf8');

  console.log('Parsing CSV data');
  const parsedData = papa.parse(csvFile, {
    header: true,
    skipEmptyLines: true,
  });
  const { data: issues, errors = [] } = parsedData;
  if (errors.length) {
    console.log('Errors:');
    errors.forEach((error) => console.error(error));
  }

  console.log('Grouping issues');
  const issuesMap = groupIssues(issues);

  console.log('Generated changelog:\n');
  generateChangelog(issuesMap);
})();
