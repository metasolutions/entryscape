import config from 'config';
import { render } from '@testing-library/react';
// import '@testing-library/jest-dom';
import { i18n } from 'esi18n';
import { Entry, User } from '@entryscape/entrystore-js';
import { USER_ENTRY_ID_GUEST } from 'commons/util/userIds';
import escoLayoutNLS from 'commons/nls/escoLayout.nls';
import PrivacyDialog from './PrivacyDialog';

const DIALOG_ID = 'PrivacyDialog-id';
const PRIVACY_PROMPT_ID = 'PrivacyDialog-privacy-prompt-id';
const TERMS_PROMPT_ID = 'PrivacyDialog-terms-prompt-id';
const PRIVACY_AND_TERMS_PROMPT_ID = 'PrivacyDialog-privacy-terms-prompt-id';
const DIALOG_BUTTON_ID = 'PrivacyDialog-button-id';

const NLS_EN = escoLayoutNLS.root;

const DEFAULT_LANG = 'en';

const DEFAULT_PRIVACY_CONFIG = {
  url: {
    en: 'https://entryscape.com/en/privacypolicy/v2/#dialog',
    sv: 'https://entryscape.com/sv/integritetspolicy/v2/#dialog',
  },
  version: 2,
  requireApproval: true,
};

const DEFAULT_TERMS_CONFIG = {
  url: {
    en: 'https://entryscape.com/en/terms/v2/#dialog',
    sv: 'https://entryscape.com/sv/terms/v2/#dialog',
  },
  version: 2,
  requireApproval: true,
};

config.get = jest.fn();

const customMockHelper = (
  userId = 1,
  customProperties = {
    policysettime: '1654072962109',
    privacyversion: '1',
    termsversion: '1',
    termssettime: '1658157598491',
  }
) => {
  jest.spyOn(i18n, 'getLocalization').mockReturnValue(NLS_EN);

  const userEntry = new Entry('testContext', {});
  const user = new User('', '', '', '');

  const spyGetId = jest
    .spyOn(userEntry, 'getId')
    .mockImplementation(() => userId);

  const spyGetResource = jest
    .spyOn(userEntry, 'getResource')
    .mockImplementation(() => user);

  const spyGetCustomProperties = jest
    .spyOn(user, 'getCustomProperties')
    .mockImplementation(() => {
      return customProperties;
    });

  return userEntry;
};

beforeEach(() => {
  config.get.mockImplementation((param) => {
    switch (param) {
      case 'locale.fallback':
        return DEFAULT_LANG;
      case 'privacyPolicy':
        return DEFAULT_PRIVACY_CONFIG;
      case 'termsAndConditions':
        return DEFAULT_TERMS_CONFIG;
      default:
        return '';
    }
  });
});

afterEach(() => {
  // restore the spy created with spyOn
  jest.restoreAllMocks();
});

test('User is guest', async () => {
  const userEntry = customMockHelper(USER_ENTRY_ID_GUEST, {});

  const { queryByTestId } = render(<PrivacyDialog userEntry={userEntry} />);
  expect(queryByTestId(DIALOG_ID)).toBeNull();
});

test('User has accepted none before', async () => {
  const userEntry = customMockHelper(1, {});

  const { queryByTestId, findByTestId } = render(
    <PrivacyDialog userEntry={userEntry} />
  );

  expect(queryByTestId(DIALOG_ID)).not.toBeNull();

  const termsText = await findByTestId(TERMS_PROMPT_ID);
  expect(termsText.textContent).toBe(
    `${NLS_EN.termsConditionsInitialInner} ${NLS_EN.linkText}.`
  );

  const privacyText = await findByTestId(PRIVACY_PROMPT_ID);
  expect(privacyText.textContent).toBe(
    `${NLS_EN.privacyPolicyInitialInner} ${NLS_EN.linkText}.`
  );

  const privacyAndTermsPrompt = await findByTestId(PRIVACY_AND_TERMS_PROMPT_ID);
  expect(privacyAndTermsPrompt.textContent).toBe(NLS_EN.privacyAndTermsPrompt);

  const actionButton = await findByTestId(DIALOG_BUTTON_ID);
  expect(actionButton.textContent).toBe(NLS_EN.privacyPolicyAndTermsAgree);
});

test('User has accepted both latest versions', async () => {
  const userEntry = customMockHelper(1, {
    policysettime: '1654072962109',
    privacyversion: '2',
    termsversion: '2',
    termssettime: '1658157598491',
  });

  const { queryByTestId } = render(<PrivacyDialog userEntry={userEntry} />);
  expect(queryByTestId(DIALOG_ID)).toBeNull();
});

test('User has accepted both, but old versions', async () => {
  const userEntry = customMockHelper();

  const { queryByTestId, findByTestId } = render(
    <PrivacyDialog userEntry={userEntry} />
  );

  expect(queryByTestId(DIALOG_ID)).not.toBeNull();

  const termsText = await findByTestId(TERMS_PROMPT_ID);
  expect(termsText.textContent).toBe(
    `${NLS_EN.termsConditionsUpdateInner} ${NLS_EN.linkText}.`
  );

  const privacyText = await findByTestId(PRIVACY_PROMPT_ID);
  expect(privacyText.textContent).toBe(
    `${NLS_EN.privacyPolicyUpdateInner} ${NLS_EN.linkText}.`
  );

  const privacyAndTermsPrompt = await findByTestId(PRIVACY_AND_TERMS_PROMPT_ID);
  expect(privacyAndTermsPrompt.textContent).toBe(NLS_EN.privacyAndTermsPrompt);

  const actionButton = await findByTestId(DIALOG_BUTTON_ID);
  expect(actionButton.textContent).toBe(NLS_EN.privacyPolicyAndTermsAgree);
});

test('User has not accepted latest privacy policy', async () => {
  const userEntry = customMockHelper(1, {
    policysettime: '1654072962109',
    privacyversion: '1',
    termsversion: '2',
    termssettime: '1658157598491',
  });

  const { queryByTestId, findByTestId } = render(
    <PrivacyDialog userEntry={userEntry} />
  );

  expect(queryByTestId(DIALOG_ID)).not.toBeNull();

  expect(queryByTestId(TERMS_PROMPT_ID)).toBeNull();

  const privacyText = await findByTestId(PRIVACY_PROMPT_ID);
  expect(privacyText.textContent).toBe(
    `${NLS_EN.privacyPolicyUpdateInner} ${NLS_EN.linkText}.`
  );

  const privacyAndTermsPrompt = await findByTestId(PRIVACY_AND_TERMS_PROMPT_ID);
  expect(privacyAndTermsPrompt.textContent).toBe(NLS_EN.privacyAndTermsPrompt);

  const actionButton = await findByTestId(DIALOG_BUTTON_ID);
  expect(actionButton.textContent).toBe(NLS_EN.privacyPolicyAgree);
});

test('User has not accepted latest terms and conditions', async () => {
  const userEntry = customMockHelper(1, {
    policysettime: '1654072962109',
    privacyversion: '2',
    termsversion: '1',
    termssettime: '1658157598491',
  });

  const { queryByTestId, findByTestId } = render(
    <PrivacyDialog userEntry={userEntry} />
  );

  expect(queryByTestId(DIALOG_ID)).not.toBeNull();

  expect(queryByTestId(PRIVACY_PROMPT_ID)).toBeNull();

  const termsText = await findByTestId(TERMS_PROMPT_ID);
  expect(termsText.textContent).toBe(
    `${NLS_EN.termsConditionsUpdateInner} ${NLS_EN.linkText}.`
  );

  const privacyAndTermsPrompt = await findByTestId(PRIVACY_AND_TERMS_PROMPT_ID);
  expect(privacyAndTermsPrompt.textContent).toBe(NLS_EN.privacyAndTermsPrompt);

  const actionButton = await findByTestId(DIALOG_BUTTON_ID);
  expect(actionButton.textContent).toBe(NLS_EN.termsConditionsAgree);
});

test('Config requires no approval', async () => {
  const privacyConfig = { ...DEFAULT_PRIVACY_CONFIG, requireApproval: false };
  const termsConfig = { ...DEFAULT_TERMS_CONFIG, requireApproval: false };

  config.get.mockImplementation((param) => {
    switch (param) {
      case 'locale.fallback':
        return DEFAULT_LANG;
      case 'privacyPolicy':
        return privacyConfig;
      case 'termsAndConditions':
        return termsConfig;
      default:
        return '';
    }
  });

  const userEntry = customMockHelper();

  const { queryByTestId } = render(<PrivacyDialog userEntry={userEntry} />);

  expect(queryByTestId(DIALOG_ID)).toBeNull();
});

// Should also test:
// * config requesting only the approval of privacy
// * config requesting only the approval of terms
// * Displaying the dialog in another language
