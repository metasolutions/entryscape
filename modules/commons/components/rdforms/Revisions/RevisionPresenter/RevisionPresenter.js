import PropTypes from 'prop-types';
import { Entry } from '@entryscape/entrystore-js';
import { Button } from '@mui/material';
import Tooltip from 'commons/components/common/Tooltip';
import Alert from '@mui/material/Alert';
import React, { useCallback, useEffect } from 'react';
import useRDFormsPresenter from 'commons/components/rdforms/hooks/useRDFormsPresenter';
import { spreadEntry } from 'commons/util/store';
import {
  useTranslation,
  nlsBundlesPropType,
} from 'commons/hooks/useTranslation';
import { useMainDialog } from 'commons/hooks/useMainDialog';
import { ConfirmAction } from 'commons/components/common/dialogs/MainDialog';
import useAsync from 'commons/hooks/useAsync';
import {
  getPreviousRevisionGraph,
  getRevisionGraph,
  isCurrentRevision,
  isSimilar,
  checkHasExcludeDiff,
  revertToGraph,
  getRevertExceptionsNLSKey,
} from '../util';
import './RevisionPresenter.scss';

const RevisionPresenter = ({
  entry,
  revision,
  formTemplateId,
  nlsBundles,
  excludeProperties,
  filterPredicates,
  onRevertCallback = () => {},
}) => {
  const {
    openMainDialog: openConfirmationDialog,
    closeMainDialog: closeConfirmationDialog,
  } = useMainDialog();

  const { runAsync: runGetRevisionGraph, data: revisionGraph } = useAsync();
  const { runAsync: runGetPreviousRevisionGraph, data: previousRevisionGraph } =
    useAsync();

  useEffect(() => {
    runGetRevisionGraph(getRevisionGraph(revision, entry));
    runGetPreviousRevisionGraph(getPreviousRevisionGraph(revision, entry));
  }, [revision, entry, runGetRevisionGraph, runGetPreviousRevisionGraph]);

  const { metadata: entryGraph, ruri: resource } = spreadEntry(entry);
  const { presenter, PresenterComponent } = useRDFormsPresenter(
    revisionGraph,
    resource,
    formTemplateId,
    false,
    filterPredicates
  );

  const translate = useTranslation(nlsBundles);

  const revertExceptionsNLSKey = getRevertExceptionsNLSKey(
    entry,
    revisionGraph,
    excludeProperties
  );

  const exceptionMessage = `${translate('revertMessage')} ${
    revertExceptionsNLSKey ? translate(revertExceptionsNLSKey) : ''
  }`;

  const revertAction = useCallback(() => {
    revertToGraph(entry, revisionGraph)
      .then(closeConfirmationDialog)
      .then(onRevertCallback)
      .catch((err) => {
        console.error(
          `Failed to revert back to ${revision.rev} for entry ${entry.getId()}`
        );
        throw Error(err);
      });
  }, [
    entry,
    revisionGraph,
    closeConfirmationDialog,
    onRevertCallback,
    revision.rev,
  ]);

  const handleRevert = useCallback(() => {
    openConfirmationDialog({
      content: exceptionMessage,
      actions: (
        <ConfirmAction
          onDone={revertAction}
          affirmLabel={translate('revertConfirm')}
          rejectLabel={translate('revertReject')}
        />
      ),
    });
  }, [exceptionMessage, openConfirmationDialog, revertAction, translate]);

  const isCurrent = isCurrentRevision(revision, entry);
  const isSimilarGraph = isSimilar(
    entryGraph,
    revisionGraph,
    excludeProperties
  );
  const hasExcludeDiff = checkHasExcludeDiff(
    revisionGraph,
    previousRevisionGraph,
    excludeProperties
  );

  const isRevertButtonDisabled = isSimilarGraph || hasExcludeDiff;

  const getRevertButtonTitle = () => {
    if (!isRevertButtonDisabled) return translate('revertTitle');
    return hasExcludeDiff
      ? translate('noRevertSameGraphExcludeTitle') // Some change is there in the excluded properties
      : translate('noRevertSameGraphTitle'); // No change to entryGraph
  };

  const revertButtonTitle = getRevertButtonTitle();

  const hasExcludePropertiesDiffMessage =
    hasExcludeDiff && translate('reasonForRevisionMessage');

  return (
    <>
      <div>
        {presenter ? <PresenterComponent /> : null}
        {hasExcludePropertiesDiffMessage ? (
          <Alert severity="info">{hasExcludePropertiesDiffMessage}</Alert>
        ) : null}
      </div>
      {isCurrent ? null : (
        <div className="escoRevisionsPresenter__actions">
          <Tooltip title={revertButtonTitle}>
            <span>
              <Button
                color="primary"
                disabled={isRevertButtonDisabled}
                onClick={handleRevert}
              >
                {translate('revertLabel')}
              </Button>
            </span>
          </Tooltip>
        </div>
      )}
    </>
  );
};

RevisionPresenter.propTypes = {
  entry: PropTypes.instanceOf(Entry),
  revision: PropTypes.shape({ rev: PropTypes.string, uri: PropTypes.string }),
  filterPredicates: PropTypes.shape({}),
  formTemplateId: PropTypes.string,
  nlsBundles: nlsBundlesPropType,
  excludeProperties: PropTypes.arrayOf(PropTypes.string),
  onRevertCallback: PropTypes.func,
};

export default RevisionPresenter;
