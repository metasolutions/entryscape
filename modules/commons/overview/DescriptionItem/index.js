import React from 'react';
import PropTypes from 'prop-types';
import { useTranslation } from 'commons/hooks/useTranslation';
import escoModules from 'commons/nls/escoModules.nls';
import './style.scss';

const DescriptionBlock = ({ description }) => {
  const translate = useTranslation(escoModules);
  return (
    <>
      <div className="escoDescriptionBlock__title">
        {translate('description-title')}
      </div>
      <div className="escoDescriptionBlock__description">{description}</div>
    </>
  );
};
DescriptionBlock.propTypes = {
  description: PropTypes.string,
};

const DescriptionItem = ({ label, value }) => {
  return (
    <div className="escoDescriptionItem">
      <div className="escoDescriptionItem__label">{label}</div>
      <div>{value}</div>
    </div>
  );
};

DescriptionItem.propTypes = {
  label: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
};

export { DescriptionItem, DescriptionBlock };
