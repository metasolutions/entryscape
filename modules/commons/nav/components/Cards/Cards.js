import { Link } from 'react-router-dom';
import React from 'react';
import { Grid, Typography } from '@mui/material';
import GridList from 'commons/components/common/GridList';
import GridListTile from 'commons/components/common/GridListTile';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import OpenInNewIcon from '@mui/icons-material/OpenInNew';
import modulesNLS from 'commons/nls/escoModules.nls';
import { useTranslation } from 'commons/hooks/useTranslation';
import PropTypes from 'prop-types';
import { getModuleIconFromName } from 'commons/util/module';
import 'commons/nav/components/Cards/index.scss';

const LinkWrapper = ({ to = '', children }) =>
  to.startsWith('http') ? (
    <a href={to} className="escoLinkWrapper" target="_blank" rel="noreferrer">
      {children}
    </a>
  ) : (
    <Link className="escoLinkWrapper" to={to}>
      {children}
    </Link>
  );

LinkWrapper.propTypes = {
  to: PropTypes.string,
  children: PropTypes.node,
};

const SquareBlock = ({
  tooltip,
  label = '',
  text,
  path = '',
  name,
  icon: iconName,
  link,
  appTheme,
}) => {
  const translate = useTranslation(modulesNLS);
  const icon = getModuleIconFromName(name, iconName, {
    fontSize: 'default',
    style: {
      marginLeft: '8px',
    },
  });
  return (
    <LinkWrapper to={path}>
      <div
        className={appTheme === 'registry' ? '' : 'escoCard'}
        aria-label={tooltip}
        title={tooltip}
      >
        <header className="escoCard__header">
          <Typography variant="h2">{label}</Typography>
          {icon}
          {link && <OpenInNewIcon className="escoCard__linkIcon" />}
        </header>
        {text && (
          <div className="escoCard__content">
            <Typography className="escoCard__text">{text}</Typography>
          </div>
        )}
        <div className="escoCard_footer">
          <Typography className="escoCard__text escoCard__text--color-blue">
            {translate('openLink')}
          </Typography>
          <ChevronRightIcon color="primary" />
        </div>
      </div>
    </LinkWrapper>
  );
};

SquareBlock.propTypes = {
  text: PropTypes.string,
  label: PropTypes.string,
  path: PropTypes.string,
  tooltip: PropTypes.string,
  name: PropTypes.string,
  link: PropTypes.bool,
  icon: PropTypes.string,
  appTheme: PropTypes.string,
};

const Cards = ({ cards, gap = 10, cols = 3, appTheme }) => {
  const cardClasses = {
    registry: {
      root: 'esreCardList__li',
    },
    suite: {
      root: 'escoCardList__li',
    },
  };

  return (
    <Grid
      container
      justifyContent="flex-start"
      direction="row"
      className={appTheme === 'registry' ? 'esreCards' : 'escoCards'}
    >
      <GridList
        classes={{ root: 'escoCardList' }}
        rowHeight={170}
        gap={gap}
        cols={cols}
      >
        {cards.map(({ label, text, path, tooltip, name, icon, link }) => (
          <GridListTile classes={cardClasses[appTheme]} key={Math.random()}>
            <SquareBlock
              label={label}
              text={text}
              path={path}
              tooltip={tooltip}
              name={name}
              link={link}
              icon={icon}
              appTheme={appTheme}
            />
          </GridListTile>
        ))}
      </GridList>
    </Grid>
  );
};

Cards.propTypes = {
  cards: PropTypes.arrayOf(PropTypes.object),
  gap: PropTypes.number,
  cols: PropTypes.number,
  appTheme: PropTypes.string,
};

Cards.defaultProps = {
  cards: [],
};

export default Cards;
