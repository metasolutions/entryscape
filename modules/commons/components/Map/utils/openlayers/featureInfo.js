import Overlay from 'ol/Overlay';

export const createOverlay = (overlayOptions = {}) => {
  return new Overlay({
    stopEvent: true,
    offset: [-16, 0],
    positioning: 'center-right',
    autoPan: true,
    ...overlayOptions,
  });
};
