import PropTypes from 'prop-types';
import { Grid, Button, Box, Collapse } from '@mui/material';
import FilterOffIcon from '@mui/icons-material/FilterAltOff';
import {
  useTranslation,
  nlsBundlesPropType,
} from 'commons/hooks/useTranslation';
import SearchFilter from 'commons/components/filters/SearchFilter';
import escoFiltersNLS from 'commons/nls/escoFilters.nls';
import { useListModel } from 'commons/components/ListView';
import './SearchFilters.scss';
import Filter from '../utils/filter';

const SearchFilters = ({
  filterProps: { filters, clearFilters, hasSelectedFilter },
  nlsBundles = [],
  disabled = false,
  onFilterSelect,
}) => {
  const t = useTranslation([escoFiltersNLS, ...nlsBundles]);
  const [{ showFilters }] = useListModel();

  const handleClear = () => {
    clearFilters();
    onFilterSelect();
  };

  const handleSelect = (filter) => (value) => {
    filter.setSelected(value);
    onFilterSelect(filter, value);
  };

  return (
    <Collapse in={showFilters} className="escoSearchFilters__collapse">
      <div className="escoSearchFilters__container">
        <Grid container>
          {filters.map((filter) => (
            <Grid item xs="auto" key={filter.getId()}>
              <SearchFilter
                filter={filter}
                translate={t}
                onSelect={handleSelect(filter)}
                disabled={disabled}
              />
            </Grid>
          ))}
        </Grid>
        <Box display="flex" justifyContent="flex-end" alignItems="flex-start">
          <Button
            startIcon={<FilterOffIcon />}
            color="secondary"
            onClick={handleClear}
            className="escoSearchFilters__clearButton"
            disabled={disabled || !hasSelectedFilter()}
          >
            {t('clearAllButton')}
          </Button>
        </Box>
      </div>
    </Collapse>
  );
};

SearchFilters.propTypes = {
  filterProps: PropTypes.shape({
    filters: PropTypes.arrayOf(PropTypes.instanceOf(Filter)).isRequired,
    clearFilters: PropTypes.func.isRequired,
    hasSelectedFilter: PropTypes.func.isRequired,
  }).isRequired,
  nlsBundles: nlsBundlesPropType,
  disabled: PropTypes.bool,
  onFilterSelect: PropTypes.func,
};

export default SearchFilters;
