import { EntityTypeSelectEditor } from 'admin/configs/projecttypes/EntityTypeSelectEditor';
import * as fieldDefinitions from './fieldDefinitions';

export const editorFields = [
  { ...fieldDefinitions.label },
  { ...fieldDefinitions.description },
  { ...fieldDefinitions.module },
  { ...fieldDefinitions.defaults, Editor: EntityTypeSelectEditor },
  { ...fieldDefinitions.optional, Editor: EntityTypeSelectEditor },
];
