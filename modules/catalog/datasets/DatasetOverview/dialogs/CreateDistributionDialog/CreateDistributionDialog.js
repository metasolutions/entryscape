import { useEffect, useState } from 'react';
import config from 'config';
import { namespaces as ns } from '@entryscape/rdfjson';
import PropTypes from 'prop-types';
import LinkOrFileTabs from 'commons/components/EntryListView/dialogs/create/LinkOrFileTabs';
import { useConfirmCloseAction } from 'commons/components/EntryListView/dialogs/create/handlers';
import ListActionDialog from 'commons/components/common/dialogs/ListActionDialog';
import { useESContext } from 'commons/hooks/useESContext';
import { useTranslation } from 'commons/hooks/useTranslation';
import useRDFormsEditor from 'commons/components/rdforms/hooks/useRDFormsEditor';
import useLevelProfile from 'commons/components/rdforms/hooks/useLevelProfile';
import LevelSelector, {
  LEVEL_RECOMMENDED,
} from 'commons/components/rdforms/LevelSelector';
import Lookup from 'commons/types/Lookup';
import useAsync, { PENDING } from 'commons/hooks/useAsync';
import { ErrorCatcher } from 'commons/errors/ErrorCatcher';
import { ErrorWithMessage } from 'commons/errors/utils/errors';
import {
  RdformsDialogFormWrapper,
  RdformsStickyDialogContent,
  RdformsDialogContent,
} from 'commons/components/rdforms/RdformsDialogFormWrapper';
import DialogTwoColumnLayout, {
  PrimaryColumn,
  SecondaryColumn,
} from 'commons/components/common/dialogs/DialogTwoColumnLayout';
import RdformsOutline from 'commons/components/rdforms/RdformsOutline';
// eslint-disable-next-line max-len
import useRdformsOutlineSearch from 'commons/components/rdforms/RdformsOutline/useRdformsOutlineSearch';
import LoadingButton from 'commons/components/LoadingButton';
import { useRDFormsValidation } from 'commons/components/rdforms/hooks/useRDFormsValidation';
import { entryPropType } from 'commons/util/entry';
import {
  createEntryByLinkOrFile,
  createPrototypeEntry,
  DCAT_ACCESSURL,
} from '../../utils/distributions';
import { nlsBundles } from '../../DistributionList/actions';
import './CreateDistributionDialog.scss';

const CreateDistributionDialog = ({ actionParams, closeDialog }) => {
  const { context } = useESContext();
  const [fileOrLink, setFileOrLink] = useState({ isLink: true });
  const excludeFileUpload = config.get('catalog.excludeFileuploadDistribution');
  const [fileOrLinkFieldIsValid, setFileOrLinkFieldIsValid] =
    useState(excludeFileUpload);
  const [filterPredicates, setFilterPredicates] = useState({
    'http://www.w3.org/ns/dcat#accessURL': !excludeFileUpload,
    'http://www.w3.org/ns/dcat#downloadURL': fileOrLink.isLink,
  });
  const [prototypeEntry] = useState(createPrototypeEntry(context));
  const confirmClose = useConfirmCloseAction(closeDialog);
  const { error: createError, runAsync, status } = useAsync();
  const {
    datasetEntry,
    refreshDistributions,
    editorLevel = LEVEL_RECOMMENDED,
  } = actionParams;
  const [level, setLevel] = useState(editorLevel);
  const [templateId, setTemplateId] = useState(null);
  const outlineSearchProps = useRdformsOutlineSearch();
  const { editor, EditorComponent, graph } = useRDFormsEditor({
    entry: prototypeEntry,
    templateId,
    includeLevel: level,
    filterPredicates,
  });
  const { checkFormErrors, formErrorMessage, setFormErrorMessage, formErrors } =
    useRDFormsValidation({
      editor,
      onError: outlineSearchProps.clearSearch,
    });

  const translate = useTranslation(nlsBundles);
  const disabledLevels = useLevelProfile(templateId);

  useEffect(() => {
    if (!templateId) {
      Lookup.inUse(prototypeEntry, datasetEntry).then((entityType) => {
        setTemplateId(entityType.templateId());
      });
    }
  }, [prototypeEntry, datasetEntry, templateId]);

  useEffect(() => {
    setFilterPredicates((state) => ({
      ...state,
      'http://www.w3.org/ns/dcat#downloadURL': !fileOrLink.isLink,
    }));
  }, [fileOrLink.isLink]);

  const createDistributionEntry = async () => {
    try {
      await createEntryByLinkOrFile(
        prototypeEntry,
        datasetEntry,
        graph,
        fileOrLink,
        context
      );
      refreshDistributions();
      closeDialog();
    } catch (error) {
      throw new ErrorWithMessage(translate('createFail'), error);
    }
  };

  const handleCreateEntry = async () => {
    const hasError = checkFormErrors(
      excludeFileUpload
        ? null
        : (error) => error?.item.getProperty() !== ns.expand(DCAT_ACCESSURL)
    );
    if (hasError) return;

    return runAsync(createDistributionEntry());
  };

  const handleCloseAction = () => {
    const fileOrLinkIsChanged = () =>
      Boolean(fileOrLink.isLink ? fileOrLink.link : fileOrLink.file);
    const changeCondition = graph.isChanged() || fileOrLinkIsChanged();
    confirmClose(changeCondition);
  };

  const dialogActions = (
    <LoadingButton
      autoFocus
      onClick={handleCreateEntry}
      loading={status === PENDING}
      disabled={!fileOrLinkFieldIsValid}
    >
      {translate('createButton')}
    </LoadingButton>
  );

  return (
    <>
      <ListActionDialog
        id="create-entry"
        title={translate('createDistributionHeader')}
        actions={dialogActions}
        closeDialog={handleCloseAction}
        alert={
          formErrorMessage
            ? {
                message: formErrorMessage,
                setMessage: setFormErrorMessage,
              }
            : undefined
        }
      >
        <DialogTwoColumnLayout>
          <SecondaryColumn>
            <RdformsOutline
              editor={editor}
              root="create-entry"
              errors={formErrors}
              searchProps={outlineSearchProps}
            />
          </SecondaryColumn>
          <PrimaryColumn>
            <RdformsDialogFormWrapper>
              <RdformsStickyDialogContent>
                <LevelSelector
                  level={level}
                  onUpdateLevel={setLevel}
                  disabledLevels={disabledLevels}
                />
              </RdformsStickyDialogContent>
              <RdformsDialogContent>
                {excludeFileUpload ? null : (
                  <div className="escaLinkOrFileTabsContainer">
                    <LinkOrFileTabs
                      setCreateEntryParams={setFileOrLink}
                      setExtraFieldIsValid={setFileOrLinkFieldIsValid}
                    />
                  </div>
                )}
                {editor && <EditorComponent />}
              </RdformsDialogContent>
            </RdformsDialogFormWrapper>
          </PrimaryColumn>
        </DialogTwoColumnLayout>
      </ListActionDialog>
      <ErrorCatcher error={createError} />
    </>
  );
};

CreateDistributionDialog.propTypes = {
  actionParams: PropTypes.shape({
    refreshDistributions: PropTypes.func,
    datasetEntry: entryPropType,
    editorLevel: PropTypes.string,
  }),
  closeDialog: PropTypes.func.isRequired,
};

export default CreateDistributionDialog;
