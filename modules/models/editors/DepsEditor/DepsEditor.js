import { useCallback, useEffect, useMemo, useRef, useState } from 'react';
import PropTypes from 'prop-types';
import {
  CHANGE,
  GroupEditor,
  LITERAL,
  LiteralEditor,
  REMOVE_FIELD_GROUP,
  SelectEditor,
  SelectEntryEditor,
  URI,
  useEditorContext,
} from 'commons/components/forms/editors';
import { findGroupByProperty } from 'commons/components/forms/editors/utils/editorContext';
import useAsync, { RESOLVED } from 'commons/hooks/useAsync';
import { Entry } from '@entryscape/entrystore-js';
import { entrystore } from 'commons/store';
import { findSelectFieldChoices } from 'models/fields/utils/options';
import {
  RDF_PROPERTY_DEPENDS_ON,
  RDF_PROPERTY_DEPENDS_ON_VALUE,
  RDF_TYPE_FIELD,
  RDF_TYPE_SELECT_FIELD,
} from 'models/utils/ns';
import { TYPE_FILTER } from 'models/utils/filterDefinitions';
import { FIELD_TYPE_CHOICES } from 'models/utils/choiceDefinitions';
import { ANY_FILTER_ITEM } from 'commons/components/filters/utils/filterDefinitions';
import { nlsBundlesPropType } from 'commons/hooks/useTranslation';
import { entryPropType } from 'commons/util/entry';

const SELECT_EDITOR = 'select';
const LITERAL_EDITOR = 'literal';

const FIELD_DEPENDS_ON = {
  name: 'field-depends-on',
  property: RDF_PROPERTY_DEPENDS_ON,
  labelNlsKey: 'depsOnFieldLabel',
};
const FIELD_DEPENDS_ON_VALUE = {
  name: 'field-depends-on',
  property: RDF_PROPERTY_DEPENDS_ON_VALUE,
  labelNlsKey: 'depsOnValueFieldLabel',
  nodetype: LITERAL,
  Editor: LiteralEditor,
  max: 1,
};

const SELECT_FIELD_FILTERS = [
  {
    ...TYPE_FILTER,
    items: [ANY_FILTER_ITEM, ...FIELD_TYPE_CHOICES],
  },
];

/**
 *
 * @param {Entry} entry
 * @returns {boolean}
 */
export const isSelectField = (entry) => {
  if (!entry) return false;
  return Boolean(
    entry
      .getMetadata()
      .find(entry.getResourceURI(), 'rdf:type', RDF_TYPE_SELECT_FIELD).length
  );
};

const SelectDepsEntryEditor = (props) => {
  const { translate } = props;

  return (
    <SelectEntryEditor
      selectEntryTooltipNlsKey="selectFieldTooltip"
      dialogProps={{
        rdfType: [RDF_TYPE_FIELD],
        filters: SELECT_FIELD_FILTERS,
        placeholderProps: {
          label: translate('selectFieldPlaceholder'),
        },
        titleNlsKey: 'selectFieldHeader',
      }}
      isResourceURI={false}
      nodetype={URI}
      {...props}
    />
  );
};

SelectDepsEntryEditor.propTypes = {
  translate: PropTypes.func,
};

const SelectFieldValueEditor = ({ entry, ...props }) => {
  const { data: choices, runAsync, status } = useAsync();
  const graph = entry?.getMetadata();
  const resourceURI = entry?.getResourceURI();

  useEffect(() => {
    if (!graph) return;
    runAsync(findSelectFieldChoices(graph, resourceURI));
  }, [graph, resourceURI, runAsync]);

  if (status !== RESOLVED) return null;

  return (
    <SelectEditor
      graph={graph}
      resourceURI={resourceURI}
      choices={choices}
      {...props}
    />
  );
};

SelectFieldValueEditor.propTypes = {
  entry: entryPropType,
};

// The value editor depends on the selected field entry. If the selected entry
// is a select field, then the available options should be selectable. For other
// field types a literal editor for free text should be used.
const useValueEditor = () => {
  const { fieldSet, dispatch, graph, resourceURI } = useEditorContext();
  const editorType = useRef();
  const { runAsync } = useAsync();
  const [entry, setEntry] = useState();

  // get the initial deps entry
  useEffect(() => {
    const entryURI = graph.findFirstValue(resourceURI, RDF_PROPERTY_DEPENDS_ON);
    if (entryURI) {
      runAsync(
        entrystore.getEntry(entryURI).then((initialEntry) => {
          editorType.current = isSelectField(initialEntry)
            ? SELECT_EDITOR
            : LITERAL_EDITOR;
          setEntry(initialEntry);
        })
      );
    }
  }, [runAsync, graph, resourceURI]);

  // either a literal or select editor may be used
  const valueEditor = useMemo(() => {
    if (!entry) return;
    return {
      ...FIELD_DEPENDS_ON_VALUE,
      ...(editorType.current === SELECT_EDITOR
        ? {
            Editor: SelectFieldValueEditor,
            entry,
          }
        : {}),
    };
  }, [entry]);

  // When switching value editor, the context state must be cleaned up, to avoid
  // duplicate field groups for the same property. Also the value will always be
  // cleared when switching to a select field, since select fields have there
  // own set of valid values.
  const switchValueEditor = useCallback(
    (entryURI) => {
      const newEntry = entryURI
        ? entrystore.getEntry(entryURI, { direct: true })
        : null;
      const fieldGroup = findGroupByProperty(
        fieldSet,
        RDF_PROPERTY_DEPENDS_ON_VALUE
      );
      const newEditorType = isSelectField(newEntry)
        ? SELECT_EDITOR
        : LITERAL_EDITOR;

      // cleanup if there's a current value field group
      if (fieldGroup) {
        if (
          newEditorType === SELECT_EDITOR ||
          editorType.current === SELECT_EDITOR
        ) {
          fieldGroup.clear();
        }
        if (newEditorType !== editorType.current) {
          dispatch({ type: REMOVE_FIELD_GROUP, field: fieldGroup });
        }
      }
      editorType.current = newEditorType;
      setEntry(newEntry);
    },
    [fieldSet, dispatch]
  );

  return [valueEditor, switchValueEditor];
};

export const DepsEditor = (props) => {
  const { translate, nlsBundles } = props;
  const [valueEditor, switchValueEditor] = useValueEditor();

  const handleEntryChange = ({ type, value: entryURI }) => {
    if (type !== CHANGE) return;
    switchValueEditor(entryURI);
  };

  const entryEditor = {
    ...FIELD_DEPENDS_ON,
    Editor: SelectDepsEntryEditor,
    onChange: handleEntryChange,
    nlsBundles,
  };

  const editors = valueEditor ? [entryEditor, valueEditor] : [entryEditor];

  return (
    <GroupEditor translate={translate} editors={editors} {...props} max={1} />
  );
};

DepsEditor.propTypes = {
  translate: PropTypes.func,
  nlsBundles: nlsBundlesPropType,
};
