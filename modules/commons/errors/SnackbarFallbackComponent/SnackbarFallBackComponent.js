import { useEffect } from 'react';
import PropTypes from 'prop-types';
import { useSnackbar, ERROR } from 'commons/hooks/useSnackbar';
import { useTranslation } from 'commons/hooks/useTranslation';
import errorsNls from 'commons/nls/escoErrors.nls';
import { ErrorWithMessage } from '../utils/errors';

const SnackbarFallbackComponent = ({ error }) => {
  const [addSnackbar] = useSnackbar();
  const translate = useTranslation(errorsNls);

  useEffect(() => {
    if (!error) return;
    const message =
      error instanceof ErrorWithMessage
        ? error.message
        : translate('errorHeader');
    addSnackbar({ message, type: ERROR });
  }, [addSnackbar, error, translate]);

  return null;
};

SnackbarFallbackComponent.propTypes = {
  error: PropTypes.instanceOf(Error),
};

export default SnackbarFallbackComponent;
