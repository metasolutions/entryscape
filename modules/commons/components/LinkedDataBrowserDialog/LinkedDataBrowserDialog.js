import PropTypes from 'prop-types';
import React, { useState, useMemo } from 'react';
import { Graph, namespaces as ns } from '@entryscape/rdfjson';
import {
  Breadcrumbs,
  Box,
  Button,
  Dialog,
  DialogContent,
  DialogActions,
  Divider,
  IconButton,
  Paper,
  Stack,
  ToggleButton,
  MenuItem,
  ListItemText,
  Select,
} from '@mui/material';
import {
  ArrowBack as ArrowBackIcon,
  ArrowForward as ArrowForwardIcon,
  NavigateNext as NavigateNextIcon,
  Language as AllLanguagesIcon,
} from '@mui/icons-material';
import { useUserState } from 'commons/hooks/useUser';
import { useTranslation } from 'commons/hooks/useTranslation';
import { Entry } from '@entryscape/entrystore-js';
import escoDialogsNLS from 'commons/nls/escoDialogs.nls';
import escoRdformsNLS from 'commons/nls/escoRdforms.nls';
import { getTitle } from 'commons/util/metadata';
import { Tab, Tabs } from 'commons/components/common/Tabs';
import ResourceInfo from 'commons/components/rdforms/ResourceInfo';
import { RESOLVED } from 'commons/hooks/useAsync';
import useLDBTemplate from 'commons/hooks/useLDBTemplate';
import Loader from 'commons/components/Loader';
import Tooltip from 'commons/components/common/Tooltip';
import { getAllMetadata } from 'commons/util/rdfUtils';
import ToggleButtonGroup from 'commons/components/buttons/ToggleButtonGroup';
import useLanguageControls from 'commons/hooks/useLanguageControls';
import { getBestLanguage } from 'commons/locale';
import ReferencesList from './ReferencesList';
import useLDBFormsPresenter from './useLDBFormsPresenter';
import './LinkedDataBrowserDialog.scss';
import {
  useLinkedBrowserModel,
  withLinkedBrowserModelProvider,
  BACK,
  FORWARD,
  MOVE,
  LINK,
} from './useLinkedBrowserModel';
import ImagePresenter from '../ImagePresenter';
import BrowserPlaceholder from './BrowserPlaceholder';

const LOCAL = 'local';
const EXTERNAL = 'external';
const COMBINED = 'all';

/**
 * Checks if metadata is empty. Since rdf:type must be excluded the
 * metadata.isEmpty() method can't be used.
 *
 * @param {Graph} metadata
 * @returns {boolean}
 */
const checkIsMetadataEmpty = (metadata) => {
  if (!metadata) return false;
  const statements = metadata
    .find()
    .filter((statement) => statement.getPredicate() !== ns.expand('rdf:type'));
  return !statements.length;
};

const MetadataVisibilityControl = ({ selected, options, handleChange }) => {
  const translate = useTranslation(escoRdformsNLS);
  return (
    <div className="escoMdVisibilityControl">
      <Paper
        elevation={0}
        className="escoMdVisibilityControl__Paper"
        sx={{ border: (theme) => `1px solid ${theme.palette.divider}` }}
      >
        <ToggleButtonGroup
          size="small"
          value={selected}
          exclusive
          onChange={handleChange}
          aria-label={translate('mdVisibilityLabel')}
        >
          {options.map(
            ({ value, labelNlsKey, ariaLabelNlsKey, tooltipNlsKey }) => (
              <Tooltip
                title={translate(tooltipNlsKey)}
                placement="bottom"
                key={`${value}-tooltip`}
              >
                <ToggleButton
                  value={value}
                  aria-label={translate(ariaLabelNlsKey)}
                  color="secondary"
                >
                  {translate(labelNlsKey)}
                </ToggleButton>
              </Tooltip>
            )
          )}
        </ToggleButtonGroup>
      </Paper>
    </div>
  );
};

MetadataVisibilityControl.propTypes = {
  selected: PropTypes.oneOf([LOCAL, EXTERNAL, COMBINED]),
  options: PropTypes.arrayOf(
    PropTypes.shape({
      value: PropTypes.string,
      labelNlsKey: PropTypes.string,
      ariaLabelNlsKey: PropTypes.string,
      tooltipNlsKey: PropTypes.string,
    })
  ),
  handleChange: PropTypes.func,
};

const getVisibilityOptions = (templateId, externalTemplateId) => [
  {
    value: LOCAL,
    labelNlsKey: 'localLabel',
    ariaLabelNlsKey: 'localLabel',
    tooltipNlsKey: 'localTooltip',
  },
  {
    value: EXTERNAL,
    labelNlsKey: 'externalLabel',
    ariaLabelNlsKey: 'externalLabel',
    tooltipNlsKey: 'externalTooltip',
  },
  ...(templateId === externalTemplateId
    ? [
        {
          value: COMBINED,
          labelNlsKey: 'combinedLabel',
          ariaLabelNlsKey: 'combinedLabel',
          tooltipNlsKey: 'combinedTooltip',
        },
      ]
    : []),
];

const LinkedDataBrowserDialog = ({
  closeDialog,
  formTemplateId,
  formExternalTemplateId,
  renderPresenter = null,
  entry,
  parentEntry,
}) => {
  const translate = useTranslation([escoDialogsNLS]);
  const [state, dispatch] = useLinkedBrowserModel(entry);
  const [tabIndex, setTabIndex] = useState(0);
  const {
    language,
    changeLanguage,
    toggleAllLanguages,
    allLanguagesEnabled,
    languageOptions,
    primaryLanguageCodes,
  } = useLanguageControls();
  const { userInfo } = useUserState();
  const fallbackLanguage = getBestLanguage(userInfo.language);

  const onTabChange = (_event, newTabIndex) => setTabIndex(newTabIndex);

  const {
    template,
    metadataVisibility,
    setMetadataVisiblity,
    entityType,
    status,
    error,
    isLoading,
  } = useLDBTemplate(
    entry,
    parentEntry,
    formTemplateId,
    renderPresenter === null
  );

  const externalTemplateId =
    entityType?.externalTemplateId() || formExternalTemplateId;
  const hasContentViewer = entityType?.get('contentviewers');
  const showVisibilityControl =
    externalTemplateId && state.entry?.isLinkReference();

  const back = () => dispatch({ type: BACK });
  const forward = () => dispatch({ type: FORWARD });
  const move = (value) => dispatch({ type: MOVE, value });
  const handleReferenceClick = ({ entry: value }) => {
    dispatch({ type: LINK, value });
    setTabIndex(0);
  };

  const metadata = useMemo(() => {
    if (!state.entry) return null;
    if (metadataVisibility === LOCAL) return state.entry.getMetadata();
    if (metadataVisibility === EXTERNAL)
      return state.entry.getCachedExternalMetadata();
    return getAllMetadata(state.entry);
  }, [state.entry, metadataVisibility]);

  const isMetadataEmpty = checkIsMetadataEmpty(metadata);

  const { PresenterComponent } = useLDBFormsPresenter(
    handleReferenceClick,
    metadata,
    state.entry?.getResourceURI(),
    metadataVisibility === EXTERNAL ? externalTemplateId : template,
    false,
    {},
    null,
    true,
    !allLanguagesEnabled,
    'en',
    language
  );

  const presenters = [
    {
      Component: renderPresenter
        ? ({ initiallyExpanded = false }) =>
            renderPresenter({
              dispatch,
              templateId: template,
              initiallyExpanded,
              entry: state.entry,
              stackIndex: state.index,
            })
        : null,
      id: 'provided-presenter',
    },
    {
      Component: hasContentViewer
        ? () => <ImagePresenter entry={state.entry} entityType={entityType} />
        : null,
      id: 'image-presenter',
    },
    {
      Component: PresenterComponent ? () => <PresenterComponent /> : null,
      id: 'main-presenter',
    },
  ].filter((presenter) => presenter.Component && !isMetadataEmpty);

  const hasNoPresenters = presenters.length === 0;
  const hasSinglePresenter = presenters.length === 1;

  const showLanguageControls =
    presenters.some((presenter) => presenter.id === 'main-presenter') &&
    tabIndex === 0;

  const handleLanguageChange = (event) => {
    changeLanguage(event.target.value);
  };

  return (
    <Dialog
      fullWidth
      maxWidth="md"
      aria-labelledby="linked-data-browser-dialog"
      open
      onClose={closeDialog}
    >
      <Stack direction="row" className="escoLinkedDataBrowser__navigation">
        <IconButton
          aria-label={translate('backAriaLabel')}
          onClick={back}
          disabled={state.index === 0}
        >
          <ArrowBackIcon />
        </IconButton>
        <IconButton
          aria-label={translate('forwardAriaLabel')}
          onClick={forward}
          disabled={state.index === state.stack.length - 1}
        >
          <ArrowForwardIcon />
        </IconButton>
        <Breadcrumbs
          className="escoLinkedDataBrowser__breadcrumbs"
          separator={<NavigateNextIcon fontSize="small" />}
          aria-label={translate('breadcrumbsAriaLabel')}
          maxItems={3}
        >
          {state.stack.map(({ entry: currentEntry, entityTypeName }, index) => {
            const isCurrent = index === state.index;
            const title =
              currentEntry && getTitle(currentEntry)
                ? getTitle(currentEntry)
                : translate(
                    entityTypeName ? 'unnamedEntity' : 'unnamedLabel',
                    entityTypeName
                  );
            return (
              <Button
                disabled={isCurrent}
                variant="text"
                onClick={() => !isCurrent && move(index)}
                key={`breadcrumb-${title}`}
                className={
                  isCurrent
                    ? 'escoLinkedDataBrowser__currentBreadcrumb'
                    : 'escoLinkedDataBrowser__breadcrumbButton'
                }
              >
                {title}
              </Button>
            );
          })}
        </Breadcrumbs>
      </Stack>
      <Divider />
      <Box className="escoLinkedDataBrowser__controls">
        <Tabs
          value={tabIndex}
          onChange={onTabChange}
          aria-label={translate('tabsAriaLabel')}
          className="escoLinkedDataBrowser__tabs"
          variant="standard"
        >
          <Tab label={translate('infoTabLabel')} key="info" />

          <Tab
            label={translate('refsTabLabel')}
            key="refs"
            disabled={state.entry === undefined}
          />
          <Tab
            label={translate('aboutTabLabel')}
            key="about"
            disabled={state.entry === undefined}
          />
        </Tabs>

        {showLanguageControls ? (
          <>
            <ToggleButton
              selected={allLanguagesEnabled}
              onClick={toggleAllLanguages}
              value="languages"
              className="escoLinkedDataBrowser__allLanguagesToggle"
            >
              <Tooltip
                title={translate(
                  allLanguagesEnabled
                    ? 'showSelectedLanguageTitle'
                    : 'showAllLanguagesTitle'
                )}
              >
                <AllLanguagesIcon />
              </Tooltip>
            </ToggleButton>

            <Select
              value={language}
              onChange={handleLanguageChange}
              IconComponent={() => null}
              variant="standard"
              disableUnderline
              disabled={allLanguagesEnabled}
              className="escoLinkedDataBrowser__languageSelectContainer"
              renderValue={(selected) => (
                <span className="escoLinkedDataBrowser__languageTag">
                  {selected}
                </span>
              )}
              inputProps={{ 'aria-labelledby': 'language-switcher' }}
            >
              {primaryLanguageCodes.map((code) => {
                const { label: labelObject } = languageOptions.find(
                  (option) => option.value === code
                );
                const label =
                  labelObject[language] || labelObject[fallbackLanguage];
                return (
                  <MenuItem key={code} value={code}>
                    <ListItemText
                      className="escoLinkedDataBrowser__languageSwitcherButton"
                      primary={label}
                    />
                  </MenuItem>
                );
              })}

              <MenuItem value="" disabled>
                ──────────
              </MenuItem>

              {languageOptions
                .filter(
                  (option) =>
                    !primaryLanguageCodes.includes(option.value) &&
                    option.value !== ''
                )
                .map(({ value, label: labelObject }) => {
                  const label =
                    labelObject[language] || labelObject[fallbackLanguage];
                  return (
                    <MenuItem key={value} value={value}>
                      <ListItemText
                        className="escoLinkedDataBrowser__languageSwitcherButton"
                        primary={label}
                      />
                    </MenuItem>
                  );
                })}
            </Select>
          </>
        ) : null}
      </Box>
      <Divider />
      <DialogContent
        className="escoLinkedDataBrowser__dialogContent"
        sx={{ height: hasContentViewer ? '600px' : '400px' }}
      >
        {isLoading ? (
          <Loader className="escoLinkedDataBrowser__loader" />
        ) : (
          <>
            <TabPanel
              currentTabIndex={tabIndex}
              index={0}
              className="escoLinkedDataBrowser__infoPanel"
            >
              {showVisibilityControl ? (
                <MetadataVisibilityControl
                  selected={metadataVisibility}
                  options={getVisibilityOptions(template, externalTemplateId)}
                  handleChange={({ target: { value } }) =>
                    setMetadataVisiblity(value)
                  }
                />
              ) : null}

              {presenters.map(({ Component, id }) => (
                <Component key={id} initiallyExpanded={hasSinglePresenter} />
              ))}

              {hasNoPresenters ? (
                <>
                  {error ? <span>{translate('errorPlaceholder')}</span> : null}
                  {status === RESOLVED && !template ? (
                    <span>{translate('templatePlaceholder')}</span>
                  ) : null}
                  {template && status === RESOLVED ? (
                    <BrowserPlaceholder label={translate('emptyPresenters')} />
                  ) : null}
                </>
              ) : null}
            </TabPanel>
            <TabPanel
              currentTabIndex={tabIndex}
              index={1}
              className="escoLinkedDataBrowser__refsPanel"
            >
              <ReferencesList
                hideTitle
                entry={state.entry}
                onRowClick={handleReferenceClick}
              />
            </TabPanel>
            <TabPanel
              currentTabIndex={tabIndex}
              index={2}
              className="escoLinkedDataBrowser__aboutPanel"
            >
              <ResourceInfo entry={state.entry} />
            </TabPanel>
          </>
        )}
      </DialogContent>
      <Divider />
      <DialogActions>
        <Button autoFocus variant="text" onClick={closeDialog}>
          {translate('close')}
        </Button>
      </DialogActions>
    </Dialog>
  );
};

LinkedDataBrowserDialog.propTypes = {
  closeDialog: PropTypes.func,
  entry: PropTypes.instanceOf(Entry),
  parentEntry: PropTypes.instanceOf(Entry),
  formTemplateId: PropTypes.string,
  formExternalTemplateId: PropTypes.string,
  renderPresenter: PropTypes.func,
};

const TabPanel = ({ children, index, currentTabIndex, className }) => {
  return (
    <div
      role="tabpanel"
      hidden={currentTabIndex !== index}
      id={`tab-${index}`}
      aria-labelledby={`tab-${index}`}
      className={className}
    >
      {currentTabIndex === index ? children : null}
    </div>
  );
};

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.number,
  currentTabIndex: PropTypes.number,
  className: PropTypes.string,
};

export default withLinkedBrowserModelProvider(LinkedDataBrowserDialog);
