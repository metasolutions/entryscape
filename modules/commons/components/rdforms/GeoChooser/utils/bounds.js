/**
 *
 * @param {Object} geodata
 * @returns {string}
 */
export const getWktFromGeodata = ({ type, coordinates }) => {
  /* Well-Known Text (WKT) encoding */
  if (type === 'point') {
    const { lat, lng } = coordinates[0];
    return `POINT(${lng} ${lat})`;
  }

  const wktCoordinates = coordinates
    .map(({ lat, lng }) => `${lng} ${lat}`)
    .join(',');
  return `POLYGON((${wktCoordinates}))`;
};

/**
 *
 * @param {string} wkt
 * @returns {Object|undefined}
 */
export const getGeodataFromWkt = (wkt) => {
  const polygonMatches = wkt.match(/POLYGON\(\((.*)\)\)/);
  const pointMatches = wkt.match(/POINT\((.*)\)/);
  const isPoint = pointMatches?.length;
  const isPolygon = polygonMatches?.length;

  if (isPolygon) {
    const coordinates = polygonMatches[1].split(',');

    if (coordinates.length !== 5) return;

    const [west, north] = coordinates[0]
      .split(' ')
      .map((coordinate) => parseFloat(coordinate));
    const [east, south] = coordinates[2]
      .split(' ')
      .map((coordinate) => parseFloat(coordinate));

    return {
      type: 'polygon',
      coordinates: [
        { lat: north, lng: west },
        { lat: north, lng: east },
        { lat: south, lng: east },
        { lat: south, lng: west },
        { lat: north, lng: west },
      ],
    };
  }

  if (isPoint) {
    const [lng, lat] = pointMatches[1]
      .split(' ')
      .map((coordinate) => parseFloat(coordinate));

    return {
      type: 'point',
      coordinates: [{ lat, lng }],
    };
  }
};

/**
 *
 * @param {Object[]} coordinates
 * @returns {Object[]}
 */
export const getBoxCoordinates = ([
  { lat: north, lng: west },
  { lat: south, lng: east },
]) => {
  return [
    { lat: north, lng: west },
    { lat: north, lng: east },
    { lat: south, lng: east },
    { lat: south, lng: west },
    { lat: north, lng: west },
  ];
};

/**
 * Get latlng coordinates from directions and
 * exclude latlng with undefined values.
 * @param {Object} directions
 * @returns {Object[]}
 */
export const getCoordinatesFromDirections = ({ north, west, south, east }) =>
  [
    { lat: north, lng: west },
    { lat: south, lng: east },
  ].filter(({ lat, lng }) => lat !== undefined && lng !== undefined);

/**
 * Extract latlng coordinates from bounds
 * @param {Object} directions
 * @param {string} type
 * @returns {Object[]}
 */
export const getCoordinatesFromBounds = ({ directions, type }) => {
  const coordinates = getCoordinatesFromDirections(directions);
  if (type === 'polygon' && coordinates.length === 2) {
    return getBoxCoordinates(coordinates);
  }
  return coordinates;
};

/**
 *
 * @param {Object} geodata
 * @returns {Object}
 */
export const getBoundsFromGeodata = ({ coordinates, type }) => {
  if (type === 'point') {
    const { lat: north, lng: west } = coordinates[0];
    return { type, directions: { north, west } };
  }
  const { lat: north, lng: west } = coordinates[0];
  const { lat: south, lng: east } = coordinates[2];
  return {
    type,
    directions: { north, west, south, east },
  };
};

/**
 *
 * @param {string} wkt
 * @returns {Object}
 */
export const getBoundsFromWkt = (wkt) => {
  const geodata = getGeodataFromWkt(wkt);
  return getBoundsFromGeodata(geodata);
};

/**
 *
 * @param {Object} bounds
 * @returns {string}
 */
export const getWktFromBounds = (bounds) => {
  const { type } = bounds;
  return getWktFromGeodata({
    type,
    coordinates: getCoordinatesFromBounds(bounds),
  });
};

/**
 *
 * @param {Object} latlngBounds
 * @param {string} type
 * @return {Object}
 */
export const getBoundsFromLatlngBounds = (latlngBounds, type) => {
  const { _northEast, _southWest } = latlngBounds;
  const { lat: north, lng: east } = _northEast;
  const { lat: south, lng: west } = _southWest;

  const directions =
    type === 'point' ? { north, west } : { north, west, south, east };

  return {
    type,
    directions,
  };
};

/**
 * Checks if bounds has complete latlng values for type
 * @param {Object} bounds
 * @returns {boolean}
 */
export const hasValidBounds = (bounds) => {
  const { type } = bounds;
  const coordinates = getCoordinatesFromBounds(bounds);
  if (type === 'point' && coordinates.length === 1) return true;
  if (type === 'polygon' && coordinates.length > 3) return true;
  return false;
};

/**
 * Extract bounds properties from a geo object,
 * a response object from geoDetect.
 * @param {Object} geo
 * @returns {Object}
 */
export const getBoundsFromGeo = (geo) => {
  const type = geo.marker ? 'point' : 'polygon';
  const directions =
    type === 'polygon' ? geo : { north: geo.lat, west: geo.lng };
  return { type, directions };
};
