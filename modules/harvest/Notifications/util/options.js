import { i18n } from 'esi18n';
import esreHarvestNLS from 'harvest/nls/esreHarvest.nls';

// model names
const OPTION_HARVEST = 'harvesting';
const OPTION_VALIDATION = 'validation';
const OPTION_OFF = 'off';
const OPTION_STRICT = 'strict';
const OPTION_LAX = 'lax';

// model data
const getOptions = () => {
  const esreHarvest = i18n.getLocalization(esreHarvestNLS);
  const options = {};
  options[OPTION_HARVEST] = [
    {
      label: esreHarvest.harvestingNotifyRadioLabel,
      selectOpts: [
        {
          label: esreHarvest.firstFailureSelectOption,
          value: OPTION_STRICT,
        },
        {
          label: esreHarvest.consequentFailuresSelectOption,
          value: OPTION_LAX,
        },
      ],
    },
    {
      label: esreHarvest.harvestingDontNotifyRadioLabel,
      value: OPTION_OFF,
    },
  ];

  /**
   * If value is OPTION_LAX, then notification will only be sent
   * when mandatory fields are missing.
   *
   * If value is OPTION_STRICT, then notifications will be sent
   * when both mandatory and recommended fields are missing
   *
   */
  options[OPTION_VALIDATION] = [
    {
      label: esreHarvest.validationNotifyRadioLabel,
      selectOpts: [
        {
          label: esreHarvest.mandatoryFieldsSelectOption,
          value: OPTION_LAX,
        },
        {
          label: esreHarvest.recommendedFieldsSelectOption,
          value: OPTION_STRICT,
        },
      ],
    },
    {
      label: esreHarvest.validationDontNotifyRadioLabel,
      value: OPTION_OFF,
    },
  ];

  return options;
};

const getValueOption = (options, value) => {
  const findValue = (el) => el.value === value;
  return options.find((option) =>
    option && option.selectOpts ? option.selectOpts.find(findValue) : [option].find(findValue)
  );
};

const getOptionLabel = (type, value) => {
  // helpers functions
  const options = getOptions()[type];
  const foundOption = getValueOption(options, value);

  if (foundOption) {
    if (!('selectOpts' in foundOption)) {
      return foundOption.label;
    }

    return `${foundOption.label} ${foundOption.selectOpts.find((el) => el.value === value).label}`;
  }
  return '';
};

export { OPTION_HARVEST, OPTION_VALIDATION, OPTION_OFF, OPTION_STRICT, OPTION_LAX, getOptions, getOptionLabel };

