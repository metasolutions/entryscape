import PropTypes from 'prop-types';
import { Grid } from '@mui/material';
import './Placeholder.scss';

const Placeholder = ({ label, icon = null, variant, className, children }) => {
  return (
    <Grid
      container
      direction="column"
      alignContent="center"
      className={`escoPlaceholder${
        variant ? ` escoPlaceholder--${variant}` : ''
      } ${className}`}
    >
      {icon && <Grid item>{icon}</Grid>}
      <Grid item>{label}</Grid>
      {children ? (
        <Grid item className="escoPlaceholder__children">
          {children}
        </Grid>
      ) : null}
    </Grid>
  );
};

Placeholder.propTypes = {
  label: PropTypes.node,
  icon: PropTypes.element,
  className: PropTypes.string,
  variant: PropTypes.oneOf(['compact', 'view', 'dialog']),
  children: PropTypes.node,
};

export default Placeholder;
