import { createContext } from '@test-utils/entry';
import { BASE_URI } from '@test-utils/constants';
import {
  RDF_PROPERTY_CONSTRAINT_PROPERTY,
  RDF_PROPERTY_ORDER,
  RDF_PROPERTY_CONSTRAINT,
  RDF_PROPERTY_RESOURCE_ENTRY,
  RDF_TYPE_CONSTRAINT,
  RDF_TYPE_QUALIFIED_RESOURCE,
} from './ns';
import {
  getConstraintType,
  CLASS_ENTRY,
  ADVANCED_CONSTRAINT,
} from './constraint';

const createClassEntryGraph = (entry) => {
  const graph = entry.getMetadata();
  const resourceURI = entry.getResourceURI();
  const blankNodeId = graph
    .add(resourceURI, RDF_PROPERTY_CONSTRAINT)
    .getValue();
  graph.add(blankNodeId, RDF_PROPERTY_RESOURCE_ENTRY, `${BASE_URI}1/entry/1`);
  graph.add(blankNodeId, 'rdf:type', RDF_TYPE_QUALIFIED_RESOURCE);
  return { graph, resourceURI };
};

const createAdvancedConstraintGraph = (entry) => {
  const graph = entry.getMetadata();
  const resourceURI = entry.getResourceURI();
  const blankNodeId = graph
    .add(resourceURI, RDF_PROPERTY_CONSTRAINT)
    .getValue();
  graph.add(blankNodeId, 'rdf:type', RDF_TYPE_CONSTRAINT);
  graph.add(blankNodeId, RDF_PROPERTY_CONSTRAINT_PROPERTY, 'rdf:type');
  graph.add(blankNodeId, 'rdf:value', 'http://somepropertyvalue.com');
  graph.addD(blankNodeId, RDF_PROPERTY_ORDER, '1', 'xsd:integer');
  return { graph, resourceURI };
};

describe('constraint metadata util functions', () => {
  describe('getConstraintType', () => {
    test('detects class entry when in graph', () => {
      const context = createContext();
      const entry = context.newEntry(context);
      const { graph, resourceURI } = createClassEntryGraph(entry);
      expect(getConstraintType(graph, resourceURI)).toBe(CLASS_ENTRY);
    });

    test('detects advanced constraint when in graph', () => {
      const context = createContext();
      const entry = context.newEntry(context);
      const { graph, resourceURI } = createAdvancedConstraintGraph(entry);
      expect(getConstraintType(graph, resourceURI)).toBe(ADVANCED_CONSTRAINT);
    });

    test('no constraint should return undefined', () => {
      const context = createContext();
      const entry = context.newEntry(context);
      expect(
        getConstraintType(entry.getMetadata(), entry.getResourceURI())
      ).toBe(undefined);
    });
  });
});
