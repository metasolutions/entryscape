import PropTypes from 'prop-types';
import useNavigate from 'commons/components/router/useNavigate';
import {
  Grid,
  ListItem as MuiListItem,
  ListItemButton as MuiListItemButton,
} from '@mui/material';
import Tooltip, { TOOLTIP_DELAY } from 'commons/components/common/Tooltip';
import useLocationState from 'commons/components/router/useLocationState';
import { withActionsProvider, useActions } from '../ActionsProvider';
import './ListItem.scss';

const ListItemGrid = ({ children }) => (
  <Grid
    container
    spacing={2}
    alignItems="center"
    className="escoListViewListItem__gridContainer"
  >
    {children}
  </Grid>
);

ListItemGrid.propTypes = {
  children: PropTypes.node,
};

// Base list item. Requires a list item action provider.
// By providing an action prop, the list item is handled
// as button with click event.
// ListItemBase assumes a 12 column grid, which is the mui default for a grid
// container. All grid items should together have a 12 column width, to get
// proper width for each item. The widths of the header items should match these
// widths.
const ListItemBase = ({ action, to, children, tooltip = '' }) => {
  const { navigate } = useNavigate();
  const { openActionDialog } = useActions();
  const handleAction = () => openActionDialog(action);
  const { locationState } = useLocationState();
  const handleNavigation = () =>
    navigate(to, { state: { listState: { ...locationState } } });
  const isButton = Boolean(action || to);

  return isButton ? (
    <MuiListItemButton
      classes={{ root: 'escoListViewListItem' }}
      onClick={to ? handleNavigation : handleAction}
      disableRipple
    >
      <ListItemGrid>{children}</ListItemGrid>
    </MuiListItemButton>
  ) : (
    <Tooltip
      title={tooltip}
      placement="bottom"
      enterDelay={TOOLTIP_DELAY}
      enterNextDelay={TOOLTIP_DELAY}
    >
      <MuiListItem
        classes={{
          root: 'escoListViewListItem',
        }}
      >
        <ListItemGrid>{children}</ListItemGrid>
      </MuiListItem>
    </Tooltip>
  );
};

ListItemBase.propTypes = {
  action: PropTypes.shape({}),
  to: PropTypes.oneOfType([PropTypes.string, PropTypes.func]),
  children: PropTypes.node,
  tooltip: PropTypes.string,
};

// ListItem has ListItemsActionsProvider for enabling interaction
// with list action menu.
export const ListItem = withActionsProvider(ListItemBase);
