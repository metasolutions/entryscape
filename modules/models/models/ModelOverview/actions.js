import { getIconFromActionId } from 'commons/actions';
import { ACTION_EDIT } from 'commons/components/overview/actions';
import { LIST_ACTION_SHARING_SETTINGS } from 'commons/components/EntryListView/actions';
import { hasAdminRights } from 'commons/util/user';
import ProjectTypeChangeDialog from 'commons/types/dialogs/ProjectTypeChangeDialog';
import Lookup from 'commons/types/Lookup';
import EditEntryDialog from 'commons/components/entry/EditEntryDialog';
import { withOverviewRefresh } from 'commons/components/overview/hooks/useOverviewModel';
import RemoveContextDialog from 'commons/components/context/RemoveContextDialog';
import ImportFormDialog from '../dialogs/ImportRfsBundleDialog';
import ImportRDFSDialog from '../dialogs/ImportRDFSDialog';

const sidebarActions = [
  {
    ...ACTION_EDIT,
    getProps: ({ entry, translate }) => ({
      action: {
        ...ACTION_EDIT,
        entry,
        formTemplateId: 'esc:Context',
        Dialog: withOverviewRefresh(EditEntryDialog, 'onEntryEdit'),
      },
      title: translate('editEntry'),
    }),
  },
  {
    id: 'change-projecttype',
    Dialog: withOverviewRefresh(ProjectTypeChangeDialog, 'onChange'),
    isVisible: ({ userEntry, userInfo }) =>
      hasAdminRights(userEntry) &&
      Lookup.getProjectTypeOptions(userInfo).length,
    labelNlsKey: 'changeProjectTypeLabel',
  },
  {
    id: 'import',
    Dialog: withOverviewRefresh(ImportFormDialog, 'onImport'),
    labelNlsKey: 'importRfsButtonLabel',
  },
  {
    id: 'import-rdfs',
    startIcon: getIconFromActionId('import'),
    Dialog: withOverviewRefresh(ImportRDFSDialog, 'onImport'),
    labelNlsKey: 'importRDFSLabel',
  },
  LIST_ACTION_SHARING_SETTINGS,
  {
    id: 'remove',
    Dialog: RemoveContextDialog,
    labelNlsKey: 'removeButtonLabel',
  },
];

export { sidebarActions };
