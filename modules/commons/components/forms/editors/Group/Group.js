import PropTypes from 'prop-types';
import { Grid } from '@mui/material';
import './Group.scss';

export const Group = ({ direction = 'column', children, ...props }) => {
  return (
    <Grid container direction={direction} className="escoFormGroup" {...props}>
      {children}
    </Grid>
  );
};

Group.propTypes = {
  direction: PropTypes.string,
  children: PropTypes.node,
};
