import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Button } from '@mui/material';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import { withActionsProvider, useActions } from 'commons/components/ListView';
import Tooltip from 'commons/components/common/Tooltip';
import { truncateLabel } from 'commons/util/util';
import './OverviewTileButton.scss';

export const TileButton = ({
  labelPrefix,
  label,
  disabled = false,
  to,
  isPrimary = false,
  ...rest
}) => {
  const buttonProps = to ? { component: Link, to, ...rest } : rest;

  const buttonClasses = [
    'escoOverviewTileButton',
    isPrimary && 'escoOverviewTileButton--primary',
  ]
    .filter(Boolean)
    .join(' ');

  const [truncatedLabel, labelAsTooltip] = truncateLabel(label, 12);

  return (
    <Tooltip title={labelAsTooltip}>
      <Button
        classes={{
          root: buttonClasses,
          disabled: 'escoOverviewTileButton--disabled',
        }}
        disabled={disabled}
        {...buttonProps}
      >
        <span className="escoOverviewTileButton__labelPrefix">
          {labelPrefix}
        </span>
        <span className="escoOverviewTileButton__label">{truncatedLabel}</span>
        {to && (
          <ChevronRightIcon
            classes={{ root: 'escoOverviewTileButton__icon' }}
          />
        )}
      </Button>
    </Tooltip>
  );
};

TileButton.propTypes = {
  label: PropTypes.string,
  labelPrefix: PropTypes.number,
  disabled: PropTypes.bool,
  to: PropTypes.string,
  isPrimary: PropTypes.bool,
};

const TileActionButton = ({ action, ...rest }) => {
  const { openActionDialog } = useActions();
  const handleActionClick = () => openActionDialog(action);
  return <TileButton onClick={handleActionClick} {...rest} />;
};

TileActionButton.propTypes = {
  action: PropTypes.shape({}),
};

const TileActionButtonWithProvider = withActionsProvider(TileActionButton);

const OverviewTileButton = ({ action, ...rest }) => {
  if (action) return <TileActionButtonWithProvider action={action} {...rest} />;
  return <TileButton {...rest} />;
};

OverviewTileButton.propTypes = {
  action: PropTypes.shape({}),
};

export default OverviewTileButton;
