import PropTypes from 'prop-types';
import {
  FormControl,
  FormControlLabel,
  TextField,
  Typography,
} from '@mui/material';
import config from 'config';
import Switch from 'commons/components/common/Switch';
import Tooltip from 'commons/components/common/Tooltip';
import { hasAdminRights as hasAdministratorRights } from 'commons/util/user';
import { isUri, validateEmail, removeWhitespace } from 'commons/util/util';
import { useTranslation } from 'commons/hooks/useTranslation';
import esreHarvestNLS from 'harvest/nls/esreHarvest.nls';
import {
  CHANGE_TITLE,
  CHANGE_ORG_ID,
  CHANGE_PSI,
  CHANGE_PUBLIC,
  CHANGE_SOURCE,
  CHANGE_USERNAME,
  CHANGE_DESCRIPTION,
} from '../PipelineFormModel';
import './PipelineForm.scss';

const getError = (value, isValidFunction, requiredError, invalidError) => {
  if (!value) return requiredError;
  if (!isValidFunction(value)) return invalidError;
  return '';
};

const PipelineForm = ({ userEntry, data, dispatch, isDcat }) => {
  const translate = useTranslation([esreHarvestNLS]);
  const hasAdminRights = hasAdministratorRights(userEntry);

  const psiPath = config.get('registry.psidataPath') || 'psi';

  const handleTitleChange = (event) => {
    const updatedTitle = removeWhitespace(event.target.value);
    dispatch({
      type: CHANGE_TITLE,
      title: updatedTitle,
      errors: { title: updatedTitle ? '' : translate('cpNotValidTitle') },
    });
  };

  const handleUsernameChange = (event) => {
    const updatedUsername = event.target.value;
    dispatch({
      type: CHANGE_USERNAME,
      username: updatedUsername,
      errors: {
        username: getError(
          updatedUsername,
          validateEmail,
          translate('cpNotValidUsername'),
          translate('invalidEmail')
        ),
      },
    });
  };

  const handleIsPublicChange = () => {
    const updatedIsPublic = !data.isPublic;
    dispatch({
      type: CHANGE_PUBLIC,
      isPublic: updatedIsPublic,
      // Reinit psi fields when public is toggled off, otherwise
      // they'd interfere with the form data and validation.
      ...(!updatedIsPublic
        ? { psi: '', orgId: '', errors: { psi: '', orgId: '' } }
        : {}),
    });
  };

  const handlePSIChange = (event) => {
    const updatedPSI = removeWhitespace(event.target.value);
    dispatch({
      type: CHANGE_PSI,
      psi: updatedPSI,
      errors: {
        psi: getError(
          updatedPSI,
          isUri,
          translate('fieldIsRequired'),
          translate('invalidURL')
        ),
      },
    });
  };

  const handleOrgIdChange = (event) => {
    const updatedOrgId = removeWhitespace(event.target.value);
    dispatch({
      type: CHANGE_ORG_ID,
      orgId: updatedOrgId,
      errors: {
        orgId: updatedOrgId ? '' : translate('fieldIsRequired'),
      },
    });
  };

  const handleSourceUrlChange = (event) => {
    const updatedSource = event.target.value.trim();
    dispatch({
      type: CHANGE_SOURCE,
      sourceUrl: updatedSource,
      errors: {
        sourceUrl: getError(
          updatedSource,
          isUri,
          translate('fieldIsRequired'),
          translate('invalidURL')
        ),
      },
    });
  };

  const handleDescriptionChange = (event) =>
    dispatch({
      type: CHANGE_DESCRIPTION,
      description: removeWhitespace(event.target.value),
    });

  const {
    errors: {
      title: titleError,
      username: usernameError,
      orgId: orgIdError,
      sourceUrl: sourceUrlError,
    },
  } = data;

  return (
    <>
      <Typography
        variant="h2"
        gutterBottom
        className="esrePipelineForm__header"
      >
        {translate('organizationDetails')}
      </Typography>

      <TextField
        id="title-input"
        aria-describedby="title-input"
        label={translate('cPTitle')}
        required
        onChange={handleTitleChange}
        error={Boolean(titleError)}
        helperText={titleError || translate('cPTitlePlaceholder')}
        value={data.title}
      />

      <TextField
        id="description-input"
        aria-describedby="description-input"
        label={translate('cPDescription')}
        helperText={translate('cPDescPlaceholder')}
        onChange={handleDescriptionChange}
        value={data.description}
      />

      {hasAdminRights && (
        <TextField
          id="username-input"
          label={translate('cPUsernameLabel')}
          required
          onChange={handleUsernameChange}
          error={Boolean(
            usernameError || (data.username && !validateEmail(data.username))
          )}
          helperText={usernameError || translate('cPUsernamePlaceholder')}
          value={data.username}
        />
      )}
      {hasAdminRights && (
        <FormControl>
          <Tooltip title={translate('changeOrganizationTitle')}>
            <FormControlLabel
              control={
                <Switch
                  checked={data.isPublic}
                  onChange={handleIsPublicChange}
                  name="isPSIOrg"
                  value="public"
                />
              }
              label={translate('publicLabel')}
            />
          </Tooltip>
        </FormControl>
      )}

      {data.isPublic && (
        <>
          <TextField
            id="psi-input"
            label={translate('cpWebpage')}
            required
            onChange={handlePSIChange}
            error={Boolean(data.errors.psi || (data.psi && !isUri(data.psi)))}
            helperText={data.errors.psi || translate('cpWebpagePlaceholder')}
            value={data.psi}
          />

          <TextField
            id="psi-path-input"
            label={translate('cpPSIWebpage')}
            helperText={translate('cpPSIWebpagePlaceholder')}
            disabled
            value={`${data.psi}/${psiPath}`}
          />

          <TextField
            id="org-id-input"
            label={translate('cpOrgId')}
            required
            onChange={handleOrgIdChange}
            error={Boolean(orgIdError)}
            helperText={orgIdError || translate('cpOrgIdPlaceholder')}
            value={data.orgId}
          />
        </>
      )}

      <Typography
        variant="h2"
        gutterBottom
        className="esrePipelineForm__header"
      >
        {translate('harvesterDetails')}
      </Typography>

      {isDcat ? (
        <TextField
          id="source-url-input"
          label={translate('epUrl')}
          helperText={
            sourceUrlError || translate('pipelineDescriptionTypeDCAT')
          }
          required
          onChange={handleSourceUrlChange}
          error={Boolean(
            sourceUrlError || (data.sourceUrl && !isUri(data.sourceUrl))
          )}
          value={data.sourceUrl}
        />
      ) : null}
    </>
  );
};

PipelineForm.propTypes = {
  userEntry: PropTypes.shape().isRequired,
  data: PropTypes.shape({
    title: PropTypes.string,
    description: PropTypes.string,
    username: PropTypes.string,
    psi: PropTypes.string,
    orgId: PropTypes.string,
    sourceUrl: PropTypes.string,
    isPublic: PropTypes.bool,
    errors: PropTypes.shape({
      title: PropTypes.string,
      username: PropTypes.string,
      sourceUrl: PropTypes.string,
      psi: PropTypes.string,
      orgId: PropTypes.string,
    }),
  }).isRequired,
  dispatch: PropTypes.func,
  isDcat: PropTypes.bool,
};

export default PipelineForm;
