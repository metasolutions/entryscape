import PropTypes from 'prop-types';
import { Button } from '@mui/material';
import Tooltip from 'commons/components/common/Tooltip';
import ConditionalWrapper from 'commons/components/common/ConditionalWrapper';

// Generic action button
export const ListActionButton = ({
  onClick,
  icon,
  disabled,
  tooltip,
  label,
  highlightId,
}) => {
  return (
    <ConditionalWrapper
      condition={Boolean(tooltip)}
      wrapper={(children) => (
        <Tooltip title={tooltip}>
          <span>{children}</span>
        </Tooltip>
      )}
    >
      <Button
        startIcon={icon}
        onClick={onClick}
        disabled={disabled}
        data-highlight-id={highlightId}
      >
        {label}
      </Button>
    </ConditionalWrapper>
  );
};

ListActionButton.propTypes = {
  onClick: PropTypes.func,
  icon: PropTypes.node,
  disabled: PropTypes.bool,
  tooltip: PropTypes.string,
  label: PropTypes.string,
  highlightId: PropTypes.string,
};
