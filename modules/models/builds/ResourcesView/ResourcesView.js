import { useCallback } from 'react';
import { useESContext } from 'commons/hooks/useESContext';
import { useTranslation } from 'commons/hooks/useTranslation';
import { RDF_TYPE_BUNDLES } from 'models/utils/ns';
import { getShortModifiedDate } from 'commons/util/metadata';
import { getLabel } from 'commons/util/rdfUtils';
import { entrystore } from 'commons/store';
import {
  ListActions,
  List,
  ListItem,
  ListItemText,
  ListHeader,
  ListHeaderItemText,
  ListHeaderItemSort,
  ListSearch,
  ListView,
  ListPagination,
  ListItemActionIconButton,
  withListModelProviderAndLocation,
  useListModel,
  REFRESH,
  useFilterListActions,
  listPropsPropType,
} from 'commons/components/ListView';
import {
  MultiCreateButton,
  RefreshButton,
  ToggleFiltersButton,
  useSolrQuery,
} from 'commons/components/EntryListView';
import InfoIcon from '@mui/icons-material/Info';
import escoListNLS from 'commons/nls/escoList.nls';
import esmoBuildsNLS from 'models/nls/esmoBuilds.nls';
import escoDialogsNLS from 'commons/nls/escoDialogs.nls';
import { LIST_ACTION_INFO } from 'commons/components/EntryListView/actions';
import { ACTION_CREATE_ID } from 'commons/actions/actionIds';
import { getPathFromViewName } from 'commons/util/site';
import useSearchFilters from 'commons/components/filters/hooks/useSearchFilters';
import SearchFilters from 'commons/components/filters/SearchFilters';
import LinkedDataBrowserDialog from 'commons/components/LinkedDataBrowserDialog';
import { filters, createActions } from './actions';

const NLS_BUNDLES = [esmoBuildsNLS, escoListNLS, escoDialogsNLS];

const ResourcesView = ({ listProps }) => {
  const { context } = useESContext();
  const createQuery = useCallback(
    () =>
      entrystore
        .newSolrQuery()
        .rdfType([RDF_TYPE_BUNDLES, 'prof:ResourceDescriptor'])
        .context(context),
    [context]
  );
  const [, dispatch] = useListModel();
  const refreshList = useCallback(
    () => dispatch({ type: REFRESH }),
    [dispatch]
  );

  const { applyFilters, hasFilters, ...filtersProps } =
    useSearchFilters(filters);
  const hasSelectedFilter = filtersProps?.hasSelectedFilter();

  const { entries, size, search, status } = useSolrQuery({
    createQuery,
    applyFilters,
  });
  const translate = useTranslation(NLS_BUNDLES);

  const createListAction = {
    items: createActions,
  };

  const defaultActions = [
    {
      id: ACTION_CREATE_ID,
      element: (
        <MultiCreateButton action={createListAction} key={ACTION_CREATE_ID} />
      ),
    },
  ];
  const listActions = useFilterListActions({
    listActions: defaultActions,
    listActionsProps: {},
    excludeActions: listProps.excludeActions,
    nlsBundles: NLS_BUNDLES,
  });

  return (
    <ListView
      status={status}
      search={search}
      size={size}
      nlsBundles={NLS_BUNDLES}
      renderPlaceholder={(Placeholder) => (
        <Placeholder>
          <MultiCreateButton action={createListAction} />
        </Placeholder>
      )}
    >
      <ListActions disabled={size === -1}>
        <ListSearch />
        {hasFilters ? <ToggleFiltersButton action={hasSelectedFilter} /> : null}
        <RefreshButton />
        {listActions.map(({ element }) => element)}
      </ListActions>
      {hasFilters ? (
        <SearchFilters
          filterProps={filtersProps}
          nlsBundles={NLS_BUNDLES}
          onFilterSelect={refreshList}
        />
      ) : null}
      <List>
        <ListHeader>
          <ListHeaderItemText xs={7} text={translate('listHeaderLabel')} />
          <ListHeaderItemSort
            sortBy="modified"
            text={translate('listHeaderModifiedDate')}
          />
        </ListHeader>
        {entries.map((entry) => {
          const infoAction = {
            ...LIST_ACTION_INFO,
            Dialog: LinkedDataBrowserDialog,
            entry,
            title: getLabel(entry),
            nlsBundles: NLS_BUNDLES,
          };

          return (
            <ListItem
              key={`${entry.getId()}-${entry.getContext().getId()}`}
              to={getPathFromViewName('models__resources__resource', {
                contextId: entry.getContext().getId(),
                entryId: entry.getId(),
              })}
            >
              <ListItemText xs={7} primary={getLabel(entry)} />
              <ListItemText secondary={getShortModifiedDate(entry)} />
              <ListItemActionIconButton
                title={translate(infoAction.labelNlsKey)}
                action={infoAction}
                xs={1}
              >
                <InfoIcon />
              </ListItemActionIconButton>
            </ListItem>
          );
        })}
      </List>
      <ListPagination />
    </ListView>
  );
};

ResourcesView.propTypes = {
  listProps: listPropsPropType,
};

export default withListModelProviderAndLocation(ResourcesView);
