import PropTypes from 'prop-types';
import { Context } from '@entryscape/entrystore-js';
import { useEffect, useMemo, useState } from 'react';
import ListActionDialog from 'commons/components/common/dialogs/ListActionDialog';
import {
  useTranslation,
  nlsBundlesPropType,
} from 'commons/hooks/useTranslation';
import escoListNLS from 'commons/nls/escoList.nls';
import esmoBuildsNLS from 'models/nls/esmoBuilds.nls';
import esmoCommonsNLS from 'models/nls/esmoCommons.nls';
import escoDialogsNLS from 'commons/nls/escoDialogs.nls';
import ContentWrapper from 'commons/components/common/ContentWrapper';
import { Button } from '@mui/material';
import {
  EditorProvider,
  FieldEditor,
  useEditorContext,
  LiteralEditor,
} from 'commons/components/forms/editors';
import { useESContext } from 'commons/hooks/useESContext';
import { CREATE, useListModel } from 'commons/components/ListView';
import { useSnackbar } from 'commons/hooks/useSnackbar';
import useAsync from 'commons/hooks/useAsync';
import Loader from 'commons/components/Loader';
import { findGroupByProperty } from 'commons/components/forms/editors/utils/editorContext';
import { ErrorCatcher } from 'commons/errors/ErrorCatcher';
import { ErrorWithMessage } from 'commons/errors/utils/errors';
import { useConfirmCloseAction } from 'commons/components/EntryListView/dialogs/create/handlers';
import { fields, FIELD_ONTOLOGY } from './actions';
import { copyMetadataToResource, findOntologyURI } from './utils/rdfs';

const NLS_BUNDLES = [
  esmoBuildsNLS,
  esmoCommonsNLS,
  escoListNLS,
  escoDialogsNLS,
];

const OntologyEditor = ({ context, initialValue, ...props }) => {
  const { graph, resourceURI } = useMemo(() => {
    if (!initialValue) return {};
    const prototypeEntry = context
      .newGraph()
      .add('rdfs:ontology', initialValue); // this value is only used as state

    // The ontology uri is not saved in the entry and only used as a dependency
    // in the export step. Thus the state of the ontology uri is stored in a
    // separate graph.
    return {
      graph: prototypeEntry.getMetadata(),
      resourceURI: prototypeEntry.getResourceURI(),
    };
  }, [context, initialValue]);

  if (!graph) return null;

  return <LiteralEditor {...props} graph={graph} resourceURI={resourceURI} />;
};

OntologyEditor.propTypes = {
  context: PropTypes.instanceOf(Context),
  initialValue: PropTypes.string,
};

const CreateRDFSDialog = ({ closeDialog }) => {
  const { context } = useESContext();
  const translate = useTranslation(NLS_BUNDLES);
  const [createError, setCreateError] = useState();
  const [, dispatch] = useListModel();
  const {
    entry: prototypeEntry,
    graph,
    canSubmit,
    fieldSet,
    validate,
  } = useEditorContext();
  const [addSnackbar] = useSnackbar();
  const {
    runAsync,
    data: initialOntologyURI,
    isLoading,
  } = useAsync({ data: '' });
  const confirmClose = useConfirmCloseAction(closeDialog);

  useEffect(() => {
    if (!context) return;
    runAsync(findOntologyURI(context));
  }, [context, runAsync]);

  // Creates resource entry. Before commiting, metadata is copied to resource
  // graph, including metadata from available classes and properties in context.
  const createGraphEntry = async () => {
    prototypeEntry.setMetadata(graph);
    const resourceGraph = prototypeEntry.getResource().getGraph();
    const ontologyURI = findGroupByProperty(fieldSet, FIELD_ONTOLOGY.property)
      .getFields()[0]
      .getValue();

    await Promise.all([
      copyMetadataToResource(context, resourceGraph, 'rdfs:Class', ontologyURI),
      copyMetadataToResource(
        context,
        resourceGraph,
        'rdf:Property',
        ontologyURI
      ),
    ]);
    fields.forEach((field) => {
      const { property } = field;
      graph
        .find(null, property)
        .forEach((statement) =>
          resourceGraph.add(ontologyURI, property, statement.getObject())
        );
    });
    return prototypeEntry.commit();
  };

  const handleCreateResource = async () => {
    const errors = validate();
    if (errors.length) return;
    try {
      await createGraphEntry();
      dispatch({ type: CREATE });
      addSnackbar({ message: translate('createRDFSSuccess') });
    } catch (error) {
      setCreateError(
        new ErrorWithMessage(translate('createResourceFail'), error)
      );
    }
    closeDialog();
  };

  const actions = (
    <Button onClick={handleCreateResource} disabled={!canSubmit || isLoading}>
      {translate('createButtonLabel')}
    </Button>
  );

  return (
    <>
      <ListActionDialog
        id="create-diagram-dialog"
        title={translate('createRDFSHeader')}
        actions={actions}
        closeDialog={() => confirmClose(graph.isChanged())}
        fixedHeight
      >
        <ContentWrapper>
          {!isLoading ? (
            <form noValidate autoComplete="off">
              {fields.map(({ labelNlsKey, ...fieldProps }) => (
                <FieldEditor
                  key={fieldProps.property}
                  label={translate(labelNlsKey)}
                  size="small"
                  {...fieldProps}
                />
              ))}
              <FieldEditor
                {...FIELD_ONTOLOGY}
                label={translate('ontolotyURILabel')}
                size="small"
                context={context}
                initialValue={initialOntologyURI}
                Editor={OntologyEditor}
              />
            </form>
          ) : (
            <Loader />
          )}
        </ContentWrapper>
      </ListActionDialog>
      <ErrorCatcher error={createError} />
    </>
  );
};

CreateRDFSDialog.propTypes = {
  closeDialog: PropTypes.func.isRequired,
  nlsBundles: nlsBundlesPropType,
};

const CreateRDFSDialogWithEditorContext = (props) => {
  const { context } = useESContext();

  const prototypeEntry = useMemo(() => {
    return context
      .newGraph()
      .add('rdf:type', 'owl:Ontology')
      .add('rdf:type', 'prof:ResourceDescriptor')
      .add('prof:hasRole', 'http://www.w3.org/ns/dx/prof/role/specification');
  }, [context]);

  return (
    <EditorProvider entry={prototypeEntry}>
      <CreateRDFSDialog {...props} />
    </EditorProvider>
  );
};

export default CreateRDFSDialogWithEditorContext;
