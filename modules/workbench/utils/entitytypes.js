import { Entry, Context } from '@entryscape/entrystore-js';
import { localize } from 'commons/locale';
import { spreadEntry } from 'commons/util/store';
import { entrystore } from 'commons/store';
import Lookup from 'commons/types/Lookup';
import {
  EntityType,
  RDF_PROPERTY_PROJECTTYPE,
  findCompositionEntityTypes,
} from 'entitytype-lookup';
import { filterOnRestrictions } from 'commons/types/utils/restrictions';
import { applyEntityTypeParams } from 'commons/util/solr';
import { getPathFromViewName, getSecondaryNavViews } from 'commons/util/site';
import registry from 'commons/registry';

const RDF_ENTITYTYPE_PROPERTY = 'esterms:entityType';
const WORKBENCH = 'workbench';

export const sortByLabel = (entityTypes) => {
  return [...entityTypes].sort((a, b) => {
    const labelA = localize(a.get('label')).toLowerCase();
    const labelB = localize(b.get('label')).toLowerCase();
    if (labelA < labelB) {
      return -1;
    }
    if (labelA > labelB) {
      return 1;
    }
    return 0; // if names are equal
  });
};

/**
 * Moves "main" entity types to the front
 *
 * @param {EntityType[]} entityTypes
 * @returns {EntityType[]}
 */
const sortByMain = (entityTypes) => {
  const entityConfigsByType = entityTypes.reduce(
    (accumulator, entityType) => {
      if (entityType.get('main')) {
        accumulator.main.push(entityType);
      } else {
        accumulator.other.push(entityType);
      }

      return accumulator;
    },
    { main: [], other: [] }
  );

  return [...entityConfigsByType.main, ...entityConfigsByType.other];
};

/**
 *
 * @param {Entry} entry
 * @param {string[]} entityTypeURIs
 * @param {string} projectTypeId
 * @returns {Promise}
 */
export const updateSelectedEntityTypes = (
  entry,
  entityTypeURIs = [],
  projectTypeId
) => {
  const { ruri, info } = spreadEntry(entry);
  const graph = info.getGraph();
  graph.findAndRemove(ruri, RDF_ENTITYTYPE_PROPERTY);
  graph.findAndRemove(ruri, RDF_PROPERTY_PROJECTTYPE);

  if (projectTypeId) {
    graph.add(ruri, RDF_PROPERTY_PROJECTTYPE, projectTypeId);
    return info.commit();
  }

  entityTypeURIs.forEach((entityTypeURIOrName) => {
    if (entityTypeURIOrName.startsWith('http')) {
      graph.add(ruri, RDF_ENTITYTYPE_PROPERTY, entityTypeURIOrName);
    } else {
      graph.addL(ruri, RDF_ENTITYTYPE_PROPERTY, entityTypeURIOrName);
    }
  });

  return info.commit();
};

/**
 * Get an entity type URI from the name
 *
 * @param {string} entityTypeName
 * @returns {string}
 */
export const getEntityURIFromName = (entityTypeName) =>
  !entityTypeName.startsWith(entrystore.getBaseURI())
    ? entrystore.getResourceURI('entitytypes', entityTypeName)
    : entityTypeName;

/**
 *
 * @param {Entry} entry
 * @returns {string[]|null}
 */
export const getSelectedEntityTypeURIs = (entry) => {
  const { ruri, info } = spreadEntry(entry);
  const graph = info.getGraph();
  const entityTypeStmts = graph.find(ruri, RDF_ENTITYTYPE_PROPERTY);
  return entityTypeStmts.length > 0
    ? entityTypeStmts.map((stmt) => stmt.getValue())
    : null;
};

/**
 * Sorts entity types by label and optionally by main
 *
 * @param {EntityType[]} entityTypes
 * @param {boolean} mainFirst
 * @returns {EntityType[]}
 */
export const sortEntityTypes = (entityTypes, mainFirst = true) => {
  const sortedByLabel = sortByLabel(entityTypes);

  if (mainFirst) return sortByMain(sortedByLabel);
  return sortedByLabel;
};

export class EntitytypeConfigError extends Error {
  constructor() {
    super('Config error, no types given for workbench.');
    // this.name = 'EntitytypeConfigError';
  }
}

/**
 * Gets counts for entity types
 *
 * @param {context} context
 * @param {EntityType} entityType
 * @returns {Promise<number[]>}
 */
export const getEntitytypeCountQuery = (context, entityType) => {
  const searchList = applyEntityTypeParams(
    entrystore.newSolrQuery(),
    {},
    entityType.get()
  )
    .context(context)
    .limit('1')
    .list();

  return searchList.getEntries().then(() => {
    return searchList.getSize();
  });
};

/**
 * Maps entity types to view-definition-like objects
 *
 * @param {EntityType[]} entityTypes
 * @param {string} contextId
 * @returns {object}
 */
const mapEntityTypesToViews = (entityTypes, contextId) => {
  const viewName = 'workbench__entities__type';
  return entityTypes.map((entityType) => ({
    name: viewName,
    title: entityType.get('label'),
    route: getPathFromViewName(viewName, {
      contextId,
      entityName: entityType.get('name'),
    }),
    count: entityType.get('count'),
    main: entityType.get('main'),
  }));
};

/**
 * Gets the entity types that should be visible in secondary navigation,
 * filtering them using relations.
 *
 * @param {EntityType[]} entityTypes
 * @returns {EntityType[]}
 */
const getSecondaryNavEntityTypes = (entityTypes) => {
  const compositionEntityTypes = findCompositionEntityTypes(entityTypes);
  const secondaryNavEntityTypes = entityTypes.filter(
    (entityType) =>
      !compositionEntityTypes.some(
        (compositionType) =>
          compositionType.get('name') === entityType.get('name')
      )
  );

  return sortByMain(secondaryNavEntityTypes);
};

/**
 * Exclude built in entity types. This is a current workaround to make sure that
 * shared entity types are not included as main entity types in secondary nav
 * and overview in Workbench. When introducing default project types, this
 * workaround will no longer be needed.
 *
 * @param {EntityType[]} entityTypes
 * @returns {EntityType[]}
 */
const excludeBuiltinEntityTypes = (entityTypes) => {
  const { entitytypes: builtinEntityTypes } = registry.getApplicationConfig();
  return entityTypes.filter((entityType) => {
    return !builtinEntityTypes.find(
      ({ name }) => entityType.get('name') === name
    );
  });
};

/**
 * Gets available entitypes for a given project type. If no project type is
 * used, available entity types will be used instead. Difference to
 * getEntityTypeConfig is that all available entity types must be listed, not
 * only saved entity types in context.
 *
 * @param {string} projectTypeId
 * @param {object} userInfo
 * @returns {Promise<object[]>}
 */
export const getAvailableEntityTypes = (projectTypeId, userInfo) => {
  const entityTypesFromProjectType = Lookup.getProjectType(projectTypeId)
    ?.getPrimary()
    .map((enityTypeId) => Lookup.get(enityTypeId));
  const availableEntityTypes =
    entityTypesFromProjectType ||
    excludeBuiltinEntityTypes(Lookup.getEntityTypes());

  return sortByLabel(
    filterOnRestrictions(availableEntityTypes, userInfo, WORKBENCH, false)
  );
};

/**
 * @param {Context} context
 * @param {object} userInfo
 * @returns {Promise<EntityType[]>}
 */
export const getPrimaryEntityTypes = async (context, userInfo) => {
  const entityTypes = await Lookup.getEntityTypesInContext(context, userInfo);
  const primaryEntityTypes = excludeBuiltinEntityTypes(entityTypes);
  return sortEntityTypes(primaryEntityTypes);
};

/**
 * Gets entitytypes and creates view-definition-like objects for each
 *
 * @param {string} route
 * @param {string} contextId
 * @returns {Promise<object[]>}
 */
export const getEntityTypeViews = async (route, contextId) => {
  if (!contextId) return getSecondaryNavViews(route);
  const nonEntityTypeViews = getSecondaryNavViews(route);

  const context = entrystore.getContextById(contextId);
  const userInfo = await entrystore.getUserInfo();
  const primaryEntityTypes = await getPrimaryEntityTypes(context, userInfo);
  const secondaryNavEntityTypes =
    getSecondaryNavEntityTypes(primaryEntityTypes);

  return [
    ...nonEntityTypeViews,
    ...mapEntityTypesToViews(secondaryNavEntityTypes, contextId),
  ];
};
