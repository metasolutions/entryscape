import { useParams } from 'react-router-dom';
import EntityListView from 'commons/components/views/EntityListView';
import { listPropsPropType } from 'commons/components/ListView';

const EntitytypesList = ({ listProps }) => {
  const viewParams = useParams();
  const selectedEntityType = viewParams?.entityName;

  return selectedEntityType ? (
    <EntityListView entityTypeName={selectedEntityType} listProps={listProps} />
  ) : null;
};

EntitytypesList.propTypes = {
  listProps: listPropsPropType,
};

export default EntitytypesList;
