import React from 'react';
import { Draggable } from 'react-beautiful-dnd';
import PropTypes from 'prop-types';
import { ListItemButton, ListItemText, ListItemIcon } from '@mui/material';
import Tooltip from 'commons/components/common/Tooltip';

const DraggableListItem = ({
  id,
  index,
  onClick,
  selected,
  primaryText,
  icon,
  iconTooltipLabel,
  iconClassName,
}) => {
  return (
    <Draggable draggableId={id} index={index}>
      {(provided) => (
        <ListItemButton
          selected={selected}
          onClick={() => onClick()}
          ref={provided.innerRef}
          {...provided.draggableProps}
          {...provided.dragHandleProps}
        >
          <ListItemText primary={primaryText} />
          {icon ? (
            <Tooltip title={iconTooltipLabel} sx={{ minWidth: 'initial' }}>
              <ListItemIcon className={iconClassName}>{icon}</ListItemIcon>
            </Tooltip>
          ) : null}
        </ListItemButton>
      )}
    </Draggable>
  );
};

DraggableListItem.propTypes = {
  id: PropTypes.string,
  index: PropTypes.number,
  onClick: PropTypes.func,
  selected: PropTypes.bool,
  primaryText: PropTypes.string,
  icon: PropTypes.node,
  iconTooltipLabel: PropTypes.string,
  iconClassName: PropTypes.string,
};

export { DraggableListItem };
/* esmoDraggableListItemIcon */
