import PropTypes from 'prop-types';
import { Box, Tab as MuiTab, Tabs as MuiTabs } from '@mui/material';
import './Tabs.scss';

const TAB_ID_PREFIX = 'tab-';
const TAB_PANEL_ID_PREFIX = 'tab-panel-';

export const TabPanel = ({ children, id, value, ...rest }) => {
  return (
    <div
      role="tabpanel"
      hidden={value !== id}
      id={`${TAB_PANEL_ID_PREFIX}${id}`}
      className="escoTabpanel"
      aria-labelledby={`${TAB_ID_PREFIX}${id}`}
      {...rest}
    >
      {value === id && (
        <Box p={3}>
          <div>{children}</div>
        </Box>
      )}
    </div>
  );
};

TabPanel.propTypes = {
  children: PropTypes.node,
  id: PropTypes.string,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
};

export const Tab = ({ label, icon, id, ...rest }) => (
  <MuiTab
    id={`${TAB_ID_PREFIX}${id}`}
    aria-controls={`${TAB_PANEL_ID_PREFIX}${id}`}
    label={label}
    icon={icon}
    iconPosition="start"
    classes={{
      wrapper: 'escoTab__wrapper',
      root: 'escoTab',
    }}
    {...rest}
  />
);

Tab.propTypes = {
  label: PropTypes.string,
  icon: PropTypes.node,
  id: PropTypes.string,
};

export const Tabs = ({
  children,
  value,
  onChange,
  className = '',
  ...rest
}) => (
  <MuiTabs
    value={value}
    onChange={onChange}
    classes={{
      root: className ? `escoTabs ${className}` : 'escoTabs',
      indicator: 'escoTabs__indicator',
    }}
    indicatorColor="primary"
    textColor="primary"
    variant="scrollable"
    scrollButtons="auto"
    {...rest}
  >
    {children}
  </MuiTabs>
);

Tabs.propTypes = {
  children: PropTypes.node,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  onChange: PropTypes.func,
  className: PropTypes.string,
};

export default Tabs;
