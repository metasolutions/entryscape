import { useState } from 'react';
import PropTypes from 'prop-types';
import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import DialogContent from '@mui/material/DialogContent';
import DialogActions from '@mui/material/DialogActions';
import Button from '@mui/material/Button';
import escoEntryChooser from 'commons/nls/escoEntryChooser.nls';
import escoDialogNLS from 'commons/nls/escoDialogs.nls';
import { useTranslation } from 'commons/hooks/useTranslation';
import EntrySelectList from './EntrySelectList';

const EntryChooserDialog = ({ binding, onClose, onSelect }) => {
  const itemLabel = binding.getItem().getLabel();
  const [open, setOpen] = useState(true);
  const onDialogClose = () => {
    setOpen(false);
    setTimeout(onClose, 500);
  };
  const translate = useTranslation([escoDialogNLS, escoEntryChooser]);

  return (
    <Dialog
      fullWidth
      maxWidth="lg"
      open={open}
      onClose={onClose}
      classes={{
        paper: 'escoEntryChooserDialog',
      }}
    >
      <DialogTitle onClose={onClose}>
        {translate('searchForHeader', itemLabel)}
      </DialogTitle>
      <DialogContent>
        <EntrySelectList
          binding={binding}
          onClose={onDialogClose}
          onSelect={onSelect}
        />
      </DialogContent>
      <DialogActions>
        <Button color="primary" onClick={onClose} autoFocus>
          {translate('close')}
        </Button>
      </DialogActions>
    </Dialog>
  );
};

EntryChooserDialog.propTypes = {
  binding: PropTypes.shape({ getItem: PropTypes.func }),
  onClose: PropTypes.func,
  onSelect: PropTypes.func,
};

const showDialog = (binding, onSelect, field) => {
  let child;
  const onClose = () => field.removeChild(child);
  child = (
    <EntryChooserDialog
      key="entrychooser"
      binding={binding}
      onClose={onClose}
      onSelect={onSelect}
    />
  );
  field.appendChild(child);
};

export default showDialog;
