import { BASE_PATH } from './paths';

/**
 * NOTE! this is needed to set the webpack public path on the fly.
 * @see https://webpack.js.org/guides/public-path/
 */

// webpack dedicated free variable
__webpack_public_path__ = BASE_PATH; // eslint-disable-line
