import { getAppVersion } from 'commons/util/config';
import getAvailableVersions from 'commons/version/util/fetchVersions';
import React, { useEffect, useState } from 'react';
import {
  Button,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
} from '@mui/material';

export default () => {
  const [version, setVersion] = useState('');
  const [versions, setVersions] = useState([]);
  const [loading, setLoading] = useState(true);

  /**
   * Make sure to persist the selection of the version in the session
   *
   * @see https://developer.mozilla.org/en-US/docs/Web/API/Window/sessionStorage
   * @param {object} event
   */
  const changeVersionHandler = (event) => {
    window.sessionStorage.setItem('version', event.target.value);
    window.location.reload();
  };

  /**
   * Update the state with available versions
   */
  const setAvailableVersions = async () => {
    const allVersions = await getAvailableVersions();
    const reactVersions = allVersions.filter((v) => !['1', '2'].includes(v[0]));
    setVersions(reactVersions);
    setLoading(false);
  };

  const resetVersion = () => {
    sessionStorage.removeItem('version');
    setVersion(getAppVersion());
    window.location.reload();
  };

  useEffect(() => {
    setVersion(getAppVersion());
    setAvailableVersions();
  }, []);

  return (
    <>
      <h2>Switch version</h2>
      <br />
      <p>
        Your current version is <b>{version}</b>
      </p>
      <div className="row" style={{ marginBottom: '48px' }}>
        {loading ? (
          <div>Fetching available versions...</div>
        ) : (
          <div>
            <FormControl variant="filled" sx={{ width: '240px' }}>
              <InputLabel id="version-select-label">Select version</InputLabel>
              <Select
                labelId="version-select-label"
                id="version-select"
                value={version}
                onChange={changeVersionHandler}
              >
                {versions.map((versionName) => (
                  <MenuItem value={versionName} key={versionName}>
                    {versionName}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>
            <br />
            <Button onClick={resetVersion} size="small">
              Reset version
            </Button>
          </div>
        )}
      </div>
    </>
  );
};
