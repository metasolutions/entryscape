import * as ns from 'models/utils/ns';
import { Graph } from '@entryscape/rdfjson';

export const MANDATORY = 'mandatory';
export const RECOMMENDED = 'recommended';
export const OPTIONAL = 'optional';
const XSD_INTEGER = 'xsd:integer';

/**
 *
 * @param {Graph} graph
 * @param {string} resourceURI
 */
export const setOptional = (graph, resourceURI) => {
  graph.addD(resourceURI, ns.RDF_PROPERTY_MIN, '0', XSD_INTEGER);
};

/**
 *
 * @param {Graph} graph
 * @param {string} resourceURI
 */
export const setMandatory = (graph, resourceURI) => {
  graph.addD(resourceURI, ns.RDF_PROPERTY_MIN, '1', XSD_INTEGER);
};

/**
 *
 * @param {Graph} graph
 * @param {string} resourceURI
 */
export const setRecommended = (graph, resourceURI) => {
  graph.addD(resourceURI, ns.RDF_PROPERTY_PREF, '1', XSD_INTEGER);
};

/**
 *
 * @param {Graph} graph
 * @param {string} resourceURI
 * @param {string|number} max
 */
export const setMax = (graph, resourceURI, max) => {
  graph.addD(resourceURI, ns.RDF_PROPERTY_MAX, `${max}`, XSD_INTEGER);
};

/**
 *
 * @param {Graph} graph
 * @param {string} resourceURI
 */
export const clearLevel = (graph, resourceURI) => {
  graph.findAndRemove(resourceURI, ns.RDF_PROPERTY_MIN);
  graph.findAndRemove(resourceURI, ns.RDF_PROPERTY_PREF);
};

/**
 *
 * @param {string} resourceURI
 * @param {Graph} graph
 * @returns {string}
 */
export const getIncludeLevel = (resourceURI, graph) => {
  if (graph.findFirstValue(resourceURI, ns.RDF_PROPERTY_MIN) > 0)
    return MANDATORY;
  if (graph.findFirstValue(resourceURI, ns.RDF_PROPERTY_PREF) > 0)
    return RECOMMENDED;
  if (graph.findFirstValue(resourceURI, ns.RDF_PROPERTY_MIN) === '0')
    return OPTIONAL;
  return '';
};

/**
 *
 * @param {string} resourceURI
 * @param {Graph} graph
 * @returns {string}
 */
export const getMax = (resourceURI, graph) => {
  const maxValue = graph?.findFirstValue(resourceURI, ns.RDF_PROPERTY_MAX);
  return maxValue;
};

/**
 *
 * @param {Graph} graph
 * @param {string} resourceURI
 * @returns {object}
 */
export const getCardinality = (graph, resourceURI) => {
  const includeLevel = getIncludeLevel(resourceURI, graph) || OPTIONAL;
  const max = getMax(resourceURI, graph);
  return { max, includeLevel };
};

/**
 *
 * @param {Graph} graph
 * @param {string} resourceURI
 * @param {string} level
 * @param {string|number|undefined} max
 */
export const setCardinality = (graph, resourceURI, level, max) => {
  clearLevel(graph, resourceURI);
  switch (level) {
    case MANDATORY:
      setMandatory(graph, resourceURI);
      break;
    case RECOMMENDED:
      setRecommended(graph, resourceURI);
      break;
    case OPTIONAL:
      setOptional(graph, resourceURI);
      break;
    case '':
      graph.findAndRemove(resourceURI, ns.RDF_PROPERTY_MIN);
      graph.findAndRemove(resourceURI, ns.RDF_PROPERTY_PREF);
      graph.findAndRemove(resourceURI, ns.RDF_PROPERTY_MAX);
      break;
    default:
      console.log(`Unknown include level ${level}`);
  }
  if (max) {
    setMax(graph, resourceURI);
  }
};

/**
 * Set cardinality in metadata based on rdforms template.
 *
 * @param {Graph} graph
 * @param {string} resourceURI
 * @param {object} template
 */
export const setCardinalityFromRfs = (graph, resourceURI, template) => {
  const { cardinality = {} } = template;
  const { min, pref, max } = cardinality;
  if (min) {
    setMandatory(graph, resourceURI);
  } else if (pref) {
    setRecommended(graph, resourceURI);
  } else {
    setOptional(graph, resourceURI);
  }
  if (max) {
    setMax(graph, resourceURI, max);
  }
};
