import PropTypes from 'prop-types';
import { useState } from 'react';
import { createEntry } from 'commons/util/store';
// eslint-disable-next-line max-len
import CreateEntryByTemplateDialog from 'commons/components/EntryListView/dialogs/create/CreateEntryByTemplateDialog';
import { namespaces as ns } from '@entryscape/rdfjson';
import { useESContext } from 'commons/hooks/useESContext';
import { RDF_TYPE_SHOWCASE } from 'commons/util/entry';
import { LEVEL_MANDATORY } from 'commons/components/rdforms/LevelSelector';
import { useTranslation } from 'commons/hooks/useTranslation';
import escaResultsNLS from 'catalog/nls/escaResults.nls';
import { useSnackbar } from 'commons/hooks/useSnackbar';

const createPrototypeEntry = (context) => {
  const prototypeEntry = createEntry(context, RDF_TYPE_SHOWCASE);
  prototypeEntry
    .getMetadata()
    .add(
      prototypeEntry.getResourceURI(),
      ns.expand('rdf:type'),
      ns.expand(RDF_TYPE_SHOWCASE)
    );
  return prototypeEntry;
};

const CreateShowcaseDialog = ({ onCreate, ...props }) => {
  const translate = useTranslation(escaResultsNLS);
  const [addSnackbar] = useSnackbar();
  const { context } = useESContext();
  const [prototypeEntry] = useState(() => createPrototypeEntry(context));

  return (
    <CreateEntryByTemplateDialog
      // eslint-disable-next-line react/jsx-props-no-spreading
      {...props}
      entry={prototypeEntry}
      editorLevel={LEVEL_MANDATORY}
      createHeaderLabel={translate('createHeader')}
      onEntryEdit={() => {
        onCreate();
        addSnackbar({ message: translate('createShowcaseSuccess') });
      }}
    />
  );
};

CreateShowcaseDialog.propTypes = { onCreate: PropTypes.func };

export default CreateShowcaseDialog;
