import adminDefaults from 'admin/defaults'; // init catalog
import catalogDefaults from 'catalog/defaults'; // init catalog
import commonsDefaults from 'commons/defaults'; // TODO HACK this needs to be before '../config/site' in order for namespaces to be set in registry
import siteConfig from './config/site';

export default async () => {
  await commonsDefaults(siteConfig);
  catalogDefaults();
  adminDefaults();
};
