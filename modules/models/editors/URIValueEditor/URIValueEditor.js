import { useState } from 'react';
import PropTypes from 'prop-types';
import { entryPropType, graphPropType } from 'commons/util/entry';
import { useESContext } from 'commons/hooks/useESContext';
import { RDF_PROPERTY_VALUE } from 'models/utils/ns';
import useGetNamespaceEntries from 'models/namespaces/hooks/useGetNamespaceEntries';
import { NAMESPACE_ENTRY, getURIEditorType } from 'models/utils/metadata';
import {
  nlsBundlesPropType,
  useTranslation,
} from 'commons/hooks/useTranslation';
import { NamespaceEntryEditor } from '../NamespaceEntryEditor';
import { ResourceEditor } from '../ResourceEditor';

const URIValueEditor = ({
  graph,
  resourceURI,
  namespaceEntries,
  property,
  editChoices,
  ...props
}) => {
  const translate = useTranslation(props.nlsBundles);
  const [editMode, setEditMode] = useState(
    getURIEditorType(graph, resourceURI, RDF_PROPERTY_VALUE, NAMESPACE_ENTRY)
  );

  const getEditorComponent = () => {
    if (editMode === NAMESPACE_ENTRY) return NamespaceEntryEditor;
    return ResourceEditor;
  };

  const EditorComponent = getEditorComponent();

  const editModeChoices = editChoices.map(({ value, labelNlsKey }) => ({
    label: translate(labelNlsKey),
    value,
  }));

  return (
    <EditorComponent
      graph={graph}
      resourceURI={resourceURI}
      onEditModeChange={setEditMode}
      editModeChoices={editModeChoices}
      property={property}
      namespaceEntries={editMode === NAMESPACE_ENTRY ? namespaceEntries : null}
      {...props}
    />
  );
};

URIValueEditor.propTypes = {
  property: PropTypes.string,
  graph: graphPropType,
  resourceURI: PropTypes.string,
  namespaceEntries: PropTypes.arrayOf(entryPropType),
  nlsBundles: nlsBundlesPropType,
  editChoices: PropTypes.arrayOf(PropTypes.shape({})),
};

const URIValueEditorWithGetNamespaceEntries = (props) => {
  const { context } = useESContext();
  const { namespaceEntries, status } = useGetNamespaceEntries(context);
  return status === 'resolved' ? (
    <URIValueEditor namespaceEntries={namespaceEntries} {...props} />
  ) : null;
};

export { URIValueEditorWithGetNamespaceEntries as URIValueEditor };
