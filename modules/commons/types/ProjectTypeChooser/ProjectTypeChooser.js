import PropTypes from 'prop-types';
import { Entry } from '@entryscape/entrystore-js';
import { useState, useEffect } from 'react';
import { FormControl, MenuItem, InputLabel, Select } from '@mui/material';
import escoRdformsNLS from 'commons/nls/escoRdforms.nls';
import { useTranslation } from 'commons/hooks/useTranslation';
import { localize } from 'commons/locale';
import Lookup from '../Lookup';

const ProjectTypeChooser = ({
  entry,
  selectedId,
  onChangeProjectType,
  required = false,
  userInfo,
}) => {
  const [projectTypes, setProjectTypes] = useState([]);
  const t = useTranslation(escoRdformsNLS);

  useEffect(() => {
    setProjectTypes(Lookup.getProjectTypeOptions(userInfo));
  }, [entry, userInfo]);

  const handleSelectProjectType = ({ target }) => {
    onChangeProjectType(target.value);
  };

  return projectTypes?.length ? (
    <FormControl variant="filled" fullWidth required={required}>
      <InputLabel id="project-type-label" shrink>
        {t('projectTypeChooserLabel')}
      </InputLabel>
      <Select
        id="project-type-select"
        value={selectedId}
        onChange={handleSelectProjectType}
        displayEmpty
        inputProps={{ 'aria-labelledby': 'project-type-label' }}
      >
        <MenuItem value="">{t('selectNoneLabel')}</MenuItem>
        {projectTypes.map((projectType) => (
          <MenuItem key={projectType.getId()} value={projectType.getId()}>
            {localize(projectType.getLabel())}
          </MenuItem>
        ))}
      </Select>
    </FormControl>
  ) : null;
};

ProjectTypeChooser.propTypes = {
  entry: PropTypes.instanceOf(Entry),
  selectedId: PropTypes.string,
  onChangeProjectType: PropTypes.func,
  required: PropTypes.bool,
  userInfo: PropTypes.shape({}),
};

export default ProjectTypeChooser;
