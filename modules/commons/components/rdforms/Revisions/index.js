import PropTypes from 'prop-types';
import { Entry } from '@entryscape/entrystore-js';
import { LinearProgress } from '@mui/material';
import { useEffect } from 'react';
import ExpandableListItem from 'commons/components/common/ExpandableListItem';
import escoVersionsNLS from 'commons/nls/escoVersions.nls';
import useAsync from 'commons/hooks/useAsync';
import {
  useTranslation,
  nlsBundlesPropType,
} from 'commons/hooks/useTranslation';
import RevisionPresenter from './RevisionPresenter';
import { loadRevisions, getRevisionRenderName } from './util';

const Revisions = ({
  entry,
  formTemplateId,
  nlsBundles,
  excludeProperties,
  filterPredicates,
  onRevertCallback,
}) => {
  // by, rev, time, uri, user
  const t = useTranslation(escoVersionsNLS);
  const { data: revisions, status, runAsync } = useAsync({ data: [] });
  const isLoading = status === 'pending';

  useEffect(() => {
    runAsync(loadRevisions(entry));
  }, [entry, runAsync]);

  return (
    <>
      {isLoading && <LinearProgress />}
      {revisions.map((revision) => (
        <ExpandableListItem
          primary={getRevisionRenderName(revision, entry, t)}
          key={revision.uri + Math.random()}
        >
          <RevisionPresenter
            entry={entry}
            filterPredicates={filterPredicates}
            revision={revision}
            formTemplateId={formTemplateId}
            nlsBundles={nlsBundles}
            excludeProperties={excludeProperties}
            onRevertCallback={onRevertCallback}
          />
        </ExpandableListItem>
      ))}
    </>
  );
};

Revisions.propTypes = {
  entry: PropTypes.instanceOf(Entry),
  filterPredicates: PropTypes.shape({}),
  formTemplateId: PropTypes.string,
  nlsBundles: nlsBundlesPropType,
  excludeProperties: PropTypes.arrayOf(PropTypes.string),
  onRevertCallback: PropTypes.func,
};

export default Revisions;
