import { getGroupWithHomeContext } from 'commons/util/store';
import useAsync, { PENDING } from 'commons/hooks/useAsync';
import useGetMainDialog from 'commons/hooks/useGetMainDialog';
import { useUserState } from 'commons/hooks/useUser';
import { useTranslation } from 'commons/hooks/useTranslation';
import escaDatasetNLS from 'catalog/nls/escaDataset.nls';
import { useSnackbar, ERROR } from 'commons/hooks/useSnackbar';
import {
  getIsAllowedPublisher,
  getHasDistributions,
  publishDatasetInternally,
  unpublishDatasetInternally,
} from '../utils/publish';

const useDatsetInternalPublish = ({ entry, datasetStatus, onChange }) => {
  const translate = useTranslation(escaDatasetNLS);
  const { getConfirmationDialog, getAcknowledgeDialog } = useGetMainDialog();
  const { userEntry } = useUserState();
  const { isSeries, isInternallyPublished } = datasetStatus;
  const { status, runAsync } = useAsync();
  const [addSnackbar] = useSnackbar();

  const unpublishInternally = async () => {
    return unpublishDatasetInternally(entry);
  };

  const publishInternally = async () => {
    const skipDistributionCheck = getHasDistributions(entry) || isSeries;

    if (!skipDistributionCheck) {
      const proceed = await getConfirmationDialog({
        content: translate('confirmPublishWithoutDistributions'),
        rejectLabel: translate('cancelPublishWithoutDistributions'),
        affirmLabel: translate('proceedPublishWithoutDistributions'),
      });
      if (!proceed) return isInternallyPublished;
    }
    return publishDatasetInternally(entry);
  };

  const togglePublish = async () => {
    if (!getIsAllowedPublisher(userEntry)) {
      await getAcknowledgeDialog({
        content: translate(
          isInternallyPublished ? 'unpublishProhibited' : 'publishProhibited'
        ),
      });
      return isInternallyPublished;
    }

    const groupEntry = await getGroupWithHomeContext(entry.getContext());

    if (!groupEntry.canAdministerEntry()) {
      await getAcknowledgeDialog({
        content: translate('datasetSharingNoAccess'),
      });
      return isInternallyPublished;
    }

    if (isInternallyPublished) {
      return unpublishInternally();
    }
    return publishInternally();
  };

  const toggleIsInternallyPublished = () =>
    runAsync(
      togglePublish()
        .then((newValue) => {
          if (newValue === isInternallyPublished) return; // cancelling
          onChange({ isInternallyPublished: newValue });
          const successNls = isSeries
            ? 'publishDatasetSeriesSuccess'
            : 'publishDatasetSuccess';
          addSnackbar({
            message: translate(`${newValue ? '' : 'un'}${successNls}`),
          });
        })
        .catch((error) => {
          console.error(error);
          const failNls = isSeries
            ? 'publishDatasetSeriesFail'
            : 'publishDatasetFail';
          addSnackbar({
            message: translate(
              `${isInternallyPublished ? 'un' : ''}${failNls}`
            ),
            type: ERROR,
          });
        })
    );

  return { toggleIsInternallyPublished, isLoading: status === PENDING };
};

export default useDatsetInternalPublish;
