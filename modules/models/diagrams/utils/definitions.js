export const DEFAULT_DIAGRAM_ID = '_defaultDiagram';
export const RDF_PROPERTY_LABEL_TYPE = 'esterms:labelType';

export const LABEL_TYPE_TITLE = 'title';
export const LABEL_TYPE_LABEL = 'label';
export const LABEL_TYPE_URI = 'uri';
