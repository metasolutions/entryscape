import { useState } from 'react';
import PropTypes from 'prop-types';
import { Extends, useExtends, hasValue } from 'models/components/Extends';
import { ExtendsButton } from 'models/components/ExtendsButton';
import { Button } from '@mui/material';
import { useEditorContext } from 'commons/components/forms/editors';
import { Fields } from 'commons/components/forms/presenters';
import { useTranslation } from 'commons/hooks/useTranslation';
import esmoCommonsNls from 'models/nls/esmoCommons.nls';
import './FieldEditor.scss';

const OverrideButton = ({ onClick }) => {
  const t = useTranslation(esmoCommonsNls);

  return (
    <Button className="esmoOverrideButton" variant="text" onClick={onClick}>
      {t('overrideButtonLabel')}
    </Button>
  );
};

OverrideButton.propTypes = {
  onClick: PropTypes.func,
};

// If value is overridden for the field, then the standard editor is shown for
// that field, with expand button to show what has been overridden. If there are
// no overridden value, then the values from the extended entry is shown, with
// option to override.
// If overriding, all values are overridden.
const ExtendsFieldEditor = ({
  property,
  extendable,
  Editor,
  Presenter,
  label,
  ...editorProps
}) => {
  const { graph, resourceURI, dispatch } = useEditorContext();
  const { translate } = editorProps;
  const extendedEntry = useExtends({ graph, resourceURI, property });
  const [overridden, setOverriddden] = useState(
    hasValue(graph, resourceURI, property)
  );

  if (!graph) return null;
  if (!extendable) return null;

  /**
   * If property is found in extended entry, values are copied to graph
   */
  const override = () => {
    if (extendedEntry) {
      const predicates = property.split(' ');
      for (const predicate of predicates) {
        const statements = extendedEntry
          .getMetadata()
          .find(extendedEntry.getResourceURI(), predicate);
        for (const statement of statements) {
          graph.add(resourceURI, predicate, statement.getCleanObject());
        }
      }
    }
    setOverriddden(true);
  };

  return overridden ? (
    <Editor
      key={editorProps.property}
      graph={graph}
      resourceURI={resourceURI}
      label={label}
      property={property}
      dispatch={dispatch}
      {...editorProps}
    >
      <Extends
        graph={graph}
        resourceURI={resourceURI}
        property={property}
        Component={ExtendsButton}
        Presenter={Presenter}
        label={label}
        {...editorProps}
      />
    </Editor>
  ) : (
    <>
      {extendedEntry ? (
        <Presenter
          graph={extendedEntry.getMetadata()}
          resourceURI={extendedEntry.getResourceURI()}
          label={label}
          property={property}
          {...editorProps}
        >
          <OverrideButton onClick={override} />
        </Presenter>
      ) : (
        <Fields label={label}>
          <div>{translate('overridePlaceholder')}</div>
          <OverrideButton onClick={override} />
        </Fields>
      )}
    </>
  );
};

ExtendsFieldEditor.propTypes = {
  property: PropTypes.string,
  Editor: PropTypes.func.isRequired,
  Presenter: PropTypes.func,
  extendable: PropTypes.bool,
  label: PropTypes.string,
};

// Wrapper to determine which kind of editor to use, the default editor or an
// editor adjusted for extensions.
export const FieldEditor = ({
  Editor,
  isVisible = () => true,
  isExtended = false,
  ...editorProps
}) => {
  const { graph, resourceURI, dispatch } = useEditorContext();

  if (!graph) return null; // graph may be null if waiting for entry to refresh
  if (!isVisible(graph, resourceURI)) return null;

  return isExtended ? (
    <ExtendsFieldEditor
      key={editorProps.property}
      Editor={Editor}
      {...editorProps}
    />
  ) : (
    <Editor
      key={editorProps.property}
      graph={graph}
      resourceURI={resourceURI}
      dispatch={dispatch}
      {...editorProps}
    />
  );
};

FieldEditor.propTypes = {
  label: PropTypes.string,
  Editor: PropTypes.func.isRequired,
  isVisible: PropTypes.func,
  isExtended: PropTypes.bool,
};
