import PropTypes from 'prop-types';
import { Grid, TextField as MuiTextField } from '@mui/material';

export const TextField = ({
  xs = 8,
  size,
  autoComplete = 'off',
  ...textFieldProps
}) => {
  return (
    <Grid item xs={xs}>
      <MuiTextField
        size={size}
        autoComplete={autoComplete}
        {...textFieldProps}
      />
    </Grid>
  );
};

TextField.propTypes = {
  xs: PropTypes.number,
  size: PropTypes.string,
  autoComplete: PropTypes.string,
};

export default TextField;
