import PropTypes from 'prop-types';
import { graphPropType } from 'commons/util/entry';
import { nlsBundlesPropType } from 'commons/hooks/useTranslation';
import { useBlankEdit } from '../hooks/useBlankEdit';
import { Fields } from '../Fields';
import { Field } from '../Field';
import { FieldEditor } from '../FieldEditor';
import { Group } from '../Group';

export const BlankEditor = ({
  property,
  size,
  graph,
  resourceURI,
  dispatch,
  editors,
  children,
  nlsBundles,
  translate,
  ...fieldsProps
}) => {
  const { blanks, labelId, addBlank } = useBlankEdit({
    name: fieldsProps.name,
    property,
    mandatory: fieldsProps.mandatory,
    dispatch,
    graph,
    resourceURI,
  });

  /**
   * Verify that editors included in group has any value. If not the group
   * should not be included in the graph.
   *
   * @param {object} _action
   * @param {object} blank
   */
  const handleEditorChange = (_action, blank) => {
    const blankNodeId = blank.getBlankNodeId();
    const hasValue = graph.findFirstValue(blankNodeId);
    if (hasValue) {
      blank.setAsserted(true);
      return;
    }
    blank.setAsserted(false);
  };

  if (!editors) {
    throw new Error('A group editor must have editors');
  }

  return (
    <Fields labelId={labelId} addField={addBlank} {...fieldsProps}>
      {children}
      {blanks.map((blank) => {
        return (
          <Field key={blank.getId()}>
            <Group>
              {editors.map((editorConfig) => {
                return (
                  <FieldEditor
                    key={editorConfig.name || editorConfig.property}
                    size={size}
                    onChange={(action) => handleEditorChange(action, blank)}
                    {...editorConfig}
                    blankNodeId={blank.getBlankNodeId()}
                    nlsBundles={nlsBundles}
                    translate={translate}
                  />
                );
              })}
            </Group>
          </Field>
        );
      })}
    </Fields>
  );
};

BlankEditor.propTypes = {
  children: PropTypes.node,
  property: PropTypes.string,
  size: PropTypes.string,
  graph: graphPropType,
  resourceURI: PropTypes.string,
  dispatch: PropTypes.func,
  editors: PropTypes.arrayOf(PropTypes.shape({})),
  nlsBundles: nlsBundlesPropType,
  translate: PropTypes.func,
};
