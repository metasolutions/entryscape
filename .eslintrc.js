const path = require('path');

module.exports = {
  extends: [
    'airbnb',
    'plugin:prettier/recommended',
    'plugin:jsdoc/recommended',
  ],
  plugins: ['react', 'react-hooks', 'jest', 'jsdoc'],
  overrides: [
    {
      files: ['**/*.test.js', '**/*.test.jsx'],
      rules: {
        'react/prop-types': 'off',
      },
    },
  ],
  settings: {
    'import/resolver': {
      webpack: { config: path.join(__dirname, 'webpack.dev.js') },
    },
  },
  env: {
    browser: true,
    'jest/globals': true,
  },
  parser: '@babel/eslint-parser',
  parserOptions: {
    requireConfigFile: false,
    sourceType: 'module',
    allowImportExportEverywhere: true,
    ecmaFeatures: {
      jsx: true,
    },
    babelOptions: {
      presets: ['@babel/preset-react'],
    },
  },
  rules: {
    'import/no-amd': 'off',
    // TODO remove this. use requirejs rules instead
    'no-undef': 'warn',
    //  Maybe this is better : no-underscore-dangle: [2, { 'allowAfterThis': true }]
    'no-underscore-dangle': 'off',
    'prefer-rest-params': 'off',
    'no-plusplus': ['error', { allowForLoopAfterthoughts: true }],
    'import/no-dynamic-require': 'off',
    'import/no-cycle': 'off',
    'global-require': 'off',
    'no-console': 'off',
    'no-prototype-builtins': 'off',
    'no-param-reassign': ['error', { props: false }],
    'import/extensions': ['error', { '.js': 'never' }],
    'no-await-in-loop': 'off',
    'no-restricted-syntax': [
      'error',
      'ForInStatement',
      'LabeledStatement',
      'WithStatement',
    ],
    'max-len': [
      'warn',
      { code: 100, ignoreTrailingComments: true, ignoreTemplateLiterals: true },
    ],
    'no-use-before-define': 'warn',
    'import/prefer-default-export': 'off',
    'consistent-return': 'off',
    'no-template-curly-in-string': 'off',
    'jsx-a11y/click-events-have-key-events': 'warn',
    'jsx-a11y/no-static-element-interactions': 'warn',
    'default-param-last': 'off',
    'no-restricted-exports': 'off',
    'react/jsx-uses-react': 'error',
    'react/react-in-jsx-scope': 'off',
    'react/jsx-uses-vars': 1,
    'react/jsx-filename-extension': 'off',
    'react/jsx-props-no-spreading': 'off',
    'react/require-default-props': 'off',
    'react-hooks/rules-of-hooks': 'error',
    'react/function-component-definition': 'off',
    'react/no-unstable-nested-components': 'warn',
    'react-hooks/exhaustive-deps': 'warn',
    'react/jsx-no-constructed-context-values': 'off',
    'prettier/prettier': [
      'error',
      {
        singleQuote: true,
        trailingComma: 'es5',
      },
    ],
    // jsdoc
    'jsdoc/require-param-description': 'off',
    'jsdoc/require-returns-description': 'off',
    'jsdoc/tag-lines': ['warn', 'any', { startLines: 1 }],
  },
};
