import PropTypes from 'prop-types';
import { entryPropType, graphPropType } from 'commons/util/entry';
import { useESContext } from 'commons/hooks/useESContext';
import useGetNamespaceEntries from 'models/namespaces/hooks/useGetNamespaceEntries';
import {
  NAMESPACE_ENTRY,
  RESOURCE,
  RESOURCE_ENTRY,
} from 'models/utils/metadata';
import { URI, useCustomDispatch } from 'commons/components/forms/editors';
import * as ns from 'models/utils/ns';
import { getLabel } from 'commons/util/rdfUtils';
import {
  useTranslation,
  nlsBundlesPropType,
} from 'commons/hooks/useTranslation';
import { NamespaceEntryEditor } from '../../NamespaceEntryEditor';
import { ResourceEditor } from '../../ResourceEditor';
import { ResourceEntryEditor } from '../../ResourceEntryEditor';

const ConstraintValueEditor = ({
  graph,
  resourceURI,
  namespaceEntries,
  property,
  field,
  editMode,
  onChange,
  onRemove,
  dispatch,
  nlsBundles,
  ...props
}) => {
  const translate = useTranslation(nlsBundles);

  const getEditorComponent = () => {
    if (editMode === NAMESPACE_ENTRY) return NamespaceEntryEditor;
    if (editMode === RESOURCE_ENTRY) return ResourceEntryEditor;
    return ResourceEditor;
  };

  const EditorComponent = getEditorComponent();

  // custom dispatch to run onChange and onRemove callbacks
  const customDispatch = useCustomDispatch({
    dispatch,
    field,
    onChange,
    onRemove,
  });

  return (
    <EditorComponent
      graph={graph}
      resourceURI={resourceURI}
      property={property}
      namespaceEntries={editMode === NAMESPACE_ENTRY ? namespaceEntries : null}
      dispatch={customDispatch}
      {...(editMode === RESOURCE
        ? {
            nodetype: URI,
          }
        : null)}
      {...(editMode === RESOURCE_ENTRY
        ? {
            editModeValue: RESOURCE_ENTRY,
            resourceRdfType: ns.RDF_TYPE_CLASS,
            selectTooltipNlsKey: 'selectClassLabel',
            placeholderProps: { label: translate('noClassesWarning') },
            getTitleProps: ({ entry }) => ({
              primary: getLabel(entry),
              secondary: entry.getResourceURI(),
            }),
            nlsBundles,
          }
        : {})}
      {...props}
    />
  );
};

ConstraintValueEditor.propTypes = {
  property: PropTypes.string,
  graph: graphPropType,
  resourceURI: PropTypes.string,
  namespaceEntries: PropTypes.arrayOf(entryPropType),
  field: PropTypes.shape({}),
  editMode: PropTypes.string,
  onChange: PropTypes.func,
  onRemove: PropTypes.func,
  dispatch: PropTypes.func,
  nlsBundles: nlsBundlesPropType,
};

const ConstraintValueEditorWithGetNamespaceEntries = (props) => {
  const { context } = useESContext();
  const { namespaceEntries, status } = useGetNamespaceEntries(context);
  return status === 'resolved' ? (
    <ConstraintValueEditor namespaceEntries={namespaceEntries} {...props} />
  ) : null;
};

export { ConstraintValueEditorWithGetNamespaceEntries as ConstraintValueEditor };
