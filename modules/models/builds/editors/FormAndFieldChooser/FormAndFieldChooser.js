import React, { useState, useCallback, useMemo } from 'react';
import PropTypes from 'prop-types';
import {
  FormControl,
  MenuItem,
  Select,
  InputLabel,
  Button,
  IconButton,
  Checkbox,
} from '@mui/material';
import { Info } from '@mui/icons-material';
import { entrystore } from 'commons/store';
import { useESContext } from 'commons/hooks/useESContext';
import { useTranslation } from 'commons/hooks/useTranslation';
import ListActionDialog from 'commons/components/common/dialogs/ListActionDialog';
import Tooltip from 'commons/components/common/Tooltip';
import esmoBuilsdNLS from 'models/nls/esmoBuilds.nls';
import esmoFormsNls from 'models/nls/esmoForms.nls';
import esmoCommonsNLS from 'models/nls/esmoCommons.nls';
import escoListNLS from 'commons/nls/escoList.nls';
import escoDialogsNLS from 'commons/nls/escoDialogs.nls';
import { getTitle, getShortModifiedDate } from 'commons/util/metadata';
import {
  ListActions,
  List,
  ListItem,
  ListItemAction,
  ListItemText,
  ListHeader,
  ListHeaderItemText,
  ListHeaderItemSort,
  ListSearch,
  ListView,
  ListPagination,
  ListPlaceholder,
  useSolrSearch,
  withListModelProvider,
  REFRESH,
  useListModel,
} from 'commons/components/ListView';
import FieldsLinkedDataBrowserDialog from 'commons/components/FieldsLinkedDataBrowserDialog';
import { RefreshButton } from 'commons/components/EntryListView';
import {
  RDF_TYPE_FIELD,
  RDF_TYPE_FORM,
  RDF_TYPE_SECTION_FORM,
  RDF_TYPE_PROPERTY_FORM,
  RDF_TYPE_PROFILE_FORM,
  RDF_PROPERTY_STATUS_UNSTABLE,
  RDF_PROPERTY_STATUS_STABLE,
  RDF_PROPERTY_STATUS_DEPRECATED,
  RDF_TYPE_TEXT_FIELD,
  RDF_TYPE_DATATYPE_FIELD,
  RDF_TYPE_SELECT_FIELD,
  RDF_TYPE_LOOKUP_FIELD,
  RDF_TYPE_INLINE_FIELD,
  RDF_TYPE_OBJECT_FORM,
} from 'models/utils/ns';
import { FIELDS_FORMS_INFO } from 'models/utils/fieldDefinitions';
import ConditionalWrapper from 'commons/components/common/ConditionalWrapper';
import { getBuildItems } from '../../utils/dependencies';

const NLS_BUNDLES = [
  esmoBuilsdNLS,
  esmoFormsNls,
  esmoCommonsNLS,
  escoListNLS,
  escoDialogsNLS,
];

const EntryFilter = ({
  inputLabel,
  selectedValue,
  handleChange,
  translate,
  noTranslation = false,
  menuItems,
}) => {
  return (
    <FormControl variant="filled">
      <div>
        <InputLabel id={inputLabel} shrink>
          {translate(inputLabel)}
        </InputLabel>
        <Select
          className="esmoAddItemsToList__select"
          value={selectedValue}
          onChange={handleChange}
          inputProps={{ 'aria-labelledby': inputLabel }}
        >
          <MenuItem value="all">{translate('selectAll')}</MenuItem>

          {menuItems.map((item) => (
            <MenuItem value={item.value} key={item.value}>
              {noTranslation ? item.label : translate(item.label)}
            </MenuItem>
          ))}
        </Select>
      </div>
    </FormControl>
  );
};

EntryFilter.propTypes = {
  inputLabel: PropTypes.string,
  selectedValue: PropTypes.string,
  handleChange: PropTypes.func,
  translate: PropTypes.func,
  noTranslation: PropTypes.bool,
  menuItems: PropTypes.arrayOf(PropTypes.shape({})),
};

const InfoButton = ({ handleClick }) => {
  const t = useTranslation([esmoBuilsdNLS]);

  return (
    <Tooltip title={t('selectionChooserInfoTooltip')}>
      <IconButton
        aria-label={t('selectionChooserInfoTooltip')}
        onClick={handleClick}
      >
        <Info />
      </IconButton>
    </Tooltip>
  );
};

InfoButton.propTypes = {
  handleClick: PropTypes.func,
};

const FormAndFieldChooser = ({
  closeDialog,
  selection,
  buildItems,
  onChange,
}) => {
  const { context } = useESContext();
  const [filterStatus, setFilterStatus] = useState('all');
  const [type, setType] = useState('all');
  const [infoDialogEntry, setInfoDialogEntry] = useState(null);
  const translate = useTranslation(NLS_BUNDLES);
  const [, dispatch] = useListModel();
  const [currentSelection, setCurrentSelection] = useState(selection);
  const [currentBuildItems, setCurrentBuildItems] = useState(buildItems);

  const createQuery = useCallback(() => {
    const rdfTypeParam =
      type === 'all' ? [RDF_TYPE_FIELD, RDF_TYPE_FORM] : [type];
    const query = entrystore.newSolrQuery().rdfType(rdfTypeParam);

    query.context(context.getId());

    if (filterStatus !== 'all') {
      query.uriProperty('rfs:status', filterStatus);
    }
    return query;
  }, [type, filterStatus, context]);
  const { entries, size, search, status } = useSolrSearch({ createQuery });

  const entryListItems = useMemo(() => {
    if (!entries || !currentSelection) return [];
    return entries.map((entry) => {
      const isDependency = Boolean(
        currentBuildItems.find(
          ({ entry: itemEntry, inlineEntry }) =>
            itemEntry === entry || inlineEntry === entry
        )
      );
      const selected = currentSelection.includes(entry);

      // disable if the entry is selected indirectly as an extension or part of
      // a form
      const disabled = !selected && isDependency;
      return { entry, selected: selected || disabled, disabled };
    });
  }, [entries, currentSelection, currentBuildItems]);

  const filterItemType = [
    { value: RDF_TYPE_TEXT_FIELD, label: 'textFieldLabel' },
    { value: RDF_TYPE_DATATYPE_FIELD, label: 'dataTypeLabel' },
    { value: RDF_TYPE_SELECT_FIELD, label: 'selectFieldLabel' },
    { value: RDF_TYPE_LOOKUP_FIELD, label: 'lookupFieldLabel' },
    { value: RDF_TYPE_INLINE_FIELD, label: 'inlineFieldLabel' },
    { value: RDF_TYPE_SECTION_FORM, label: 'sectionFormLabel' },
    { value: RDF_TYPE_PROPERTY_FORM, label: 'propertyFormLabel' },
    { value: RDF_TYPE_PROFILE_FORM, label: 'profileFormLabel' },
    { value: RDF_TYPE_OBJECT_FORM, label: 'objectFormLabel' },
  ];

  const filterItemStatus = [
    { value: RDF_PROPERTY_STATUS_UNSTABLE, label: 'unstableLabel' },
    { value: RDF_PROPERTY_STATUS_STABLE, label: 'stableLabel' },
    { value: RDF_PROPERTY_STATUS_DEPRECATED, label: 'deprecatedLabel' },
  ];

  // Filter functions
  const handleChangeStatus = (event) => {
    dispatch({ type: REFRESH });
    setFilterStatus(event.target.value);
  };

  const handleChangeType = (event) => {
    dispatch({ type: REFRESH });
    setType(event.target.value);
  };

  const handleSelect = async (entry, selected) => {
    const newSelection = selected
      ? [...currentSelection, entry]
      : currentSelection.filter((selectedEntry) => selectedEntry !== entry);
    setCurrentSelection(newSelection);
    const items = await getBuildItems(newSelection);
    setCurrentBuildItems(items);
  };

  const infoButtonClickHandler = (_event, entry) => {
    setInfoDialogEntry(entry);
  };

  const closeInfoDialog = () => {
    setInfoDialogEntry(null);
  };

  const actions = (
    <Button
      disabled={currentSelection.length === 0}
      onClick={() => onChange(currentSelection, currentBuildItems)}
    >
      {translate('selectLabel')}
    </Button>
  );

  return (
    <ListActionDialog
      id="select-user-dialog"
      closeDialog={closeDialog}
      closeDialogButtonLabel={translate('cancel')}
      title={translate('editSelectionTitle')}
      fixedHeight
      actions={actions}
    >
      <ListView
        status={status}
        search={search}
        size={size}
        nlsBundles={NLS_BUNDLES}
        showPlaceholder={Boolean(search)}
      >
        <ListActions disabled={size === -1}>
          <div className="esmoListActions">
            <div className="esmoAddItemsToForms__search">
              <ListSearch />
              <RefreshButton />
            </div>
            <div className="esmoAddItemsToForms__filter">
              <EntryFilter
                inputLabel="statusLabel"
                selectedValue={filterStatus}
                handleChange={handleChangeStatus}
                translate={translate}
                menuItems={filterItemStatus}
              />
              <EntryFilter
                inputLabel="typeLabel"
                selectedValue={type}
                handleChange={handleChangeType}
                translate={translate}
                menuItems={filterItemType}
              />
            </div>
          </div>
        </ListActions>
        <ListHeader>
          <ListHeaderItemText xs={6} text={translate('listHeaderLabel')} />
          <ListHeaderItemSort
            sortBy="modified"
            text={translate('listHeaderModifiedDate')}
          />
        </ListHeader>
        <List
          renderPlaceholder={() => (
            <ListPlaceholder label={translate('noResultFound')} />
          )}
        >
          {entryListItems.map(({ entry, selected, disabled }) => (
            <ListItem key={`${entry.getId()}-${entry.getContext().getId()}`}>
              <ListItemText xs={6} primary={getTitle(entry)} />
              <ListItemText xs={3} secondary={getShortModifiedDate(entry)} />
              <ListItemAction xs={2}>
                <InfoButton
                  handleClick={(event) => infoButtonClickHandler(event, entry)}
                />
              </ListItemAction>
              <ListItemAction xs={1}>
                <ConditionalWrapper
                  condition={disabled}
                  // eslint-disable-next-line react/no-unstable-nested-components
                  wrapper={(tooltipChildren) => (
                    <Tooltip title={translate('disabledSelectionTooltip')}>
                      {tooltipChildren}
                    </Tooltip>
                  )}
                >
                  <Checkbox
                    checked={selected}
                    disabled={disabled}
                    color="primary"
                    onChange={() => handleSelect(entry, !selected)}
                    inputProps={{ 'aria-labelledby': 'select-entry-checkbox' }}
                    id="select-entry-checkbox"
                  />
                </ConditionalWrapper>
              </ListItemAction>
            </ListItem>
          ))}
        </List>
        <ListPagination />
      </ListView>

      {infoDialogEntry ? (
        <FieldsLinkedDataBrowserDialog
          entry={infoDialogEntry}
          fields={FIELDS_FORMS_INFO}
          title={translate('infoDialogTitle')}
          closeDialog={closeInfoDialog}
          nlsBundles={NLS_BUNDLES}
        />
      ) : null}
    </ListActionDialog>
  );
};

FormAndFieldChooser.propTypes = {
  closeDialog: PropTypes.func.isRequired,
  selection: PropTypes.arrayOf(PropTypes.shape({})),
  buildItems: PropTypes.arrayOf(PropTypes.shape({})),
  onChange: PropTypes.func,
};

export default withListModelProvider(FormAndFieldChooser);
