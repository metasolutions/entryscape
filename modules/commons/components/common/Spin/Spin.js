import PropTypes from 'prop-types';
import { useEffect, useState } from 'react';
import { PENDING, REJECTED, RESOLVED } from 'commons/hooks/useAsync';
import './Spin.scss';

export const Spin = ({ children, size = 24 }) => {
  return (
    <div
      className="escoSpin"
      style={{ width: `${size}px`, height: `${size}px` }}
    >
      {children}
    </div>
  );
};

Spin.propTypes = {
  children: PropTypes.node,
  size: PropTypes.number,
};

const STOPPED = 'stopped';
const STARTED = 'started';
const SPINNING = 'spinning';

export const useSpinState = (listStatus) => {
  const [status, setStatus] = useState(STOPPED);
  const start = () => setStatus(STARTED);

  useEffect(() => {
    if (status === STARTED && listStatus === PENDING) {
      setStatus(SPINNING);
      return;
    }

    if (status === SPINNING && [RESOLVED, REJECTED].includes(listStatus)) {
      setStatus('stopped');
    }
  }, [status, listStatus]);

  return [status === SPINNING, start];
};

export default Spin;
