const STATIC_INFRA_VERSIONS_URL = 'https://static.infra.entryscape.com/versions.json';

export default () => fetch(STATIC_INFRA_VERSIONS_URL).then((res) => res.json());
