import { namespaces as ns } from '@entryscape/rdfjson';

export default () => {
  ns.add('esterms', 'http://entryscape.com/terms/');
  ns.add('dcat', 'http://www.w3.org/ns/dcat#');
};
