import { Entry, Context } from '@entryscape/entrystore-js';
import { entrystore } from 'commons/store';
import { getLabel } from 'commons/util/rdfUtils';
import { getShortDate } from 'commons/util/date';
import sleep from 'commons/util/sleep';
import { chunk } from 'lodash-es';

/**
 * Adding Comment/reply
 *
 * @param {Entry} entry
 * @param {string} comment
 * @param {string} subject
 * @returns {object}
 */

const addComment = ({ entry, comment, subject = '' }) => {
  const context = entry.getContext();
  const CommentEntry = context.newEntry();
  const resourceURI = CommentEntry.getResourceURI();
  const metadata = CommentEntry.getMetadata();
  metadata.add(resourceURI, 'rdf:type', 'oa:Annotation');
  metadata.addL(resourceURI, 'dcterms:title', subject);
  metadata.add(resourceURI, 'oa:motivatedBy', 'oa:commenting');
  metadata.add(resourceURI, 'oa:hasTarget', entry.getResourceURI());
  const stmt = metadata.add(resourceURI, 'oa:hasBody');
  metadata.add(stmt.getValue(), 'rdf:type', 'oa:TextualBody');
  metadata.addL(stmt.getValue(), 'dc:format', 'text/plain');
  metadata.addL(stmt.getValue(), 'rdf:value', comment);

  try {
    CommentEntry.commit().then(() => {
      entry.setRefreshNeeded();
      entry.refresh();
    });
  } catch (err) {
    console.log(err);
  }
  return entry;
};

/**
 * Adding Edit Comment/reply
 *
 * @param {Entry} entry
 * @param {string} commentTxt
 * @param {Function} fetchComments
 */
const editComment = (entry, commentTxt, fetchComments) => {
  const metadata = entry.getMetadata();
  const stmt = metadata.find(null, 'oa:hasBody')[0];
  metadata.findAndRemove(null, 'rdf:value');
  metadata.addL(stmt.getValue(), 'rdf:value', commentTxt);
  try {
    entry.commitMetadata().then(async () => {
      await fetchComments();
    });
  } catch (err) {
    console.log(err);
  }
};

/**
 * Get Comment/reply
 *
 * @param {Entry} entry
 * @returns {Entry[]}
 */

const getReplyList = async (entry, context) => {
  await sleep(2000);

  const commentList = await entrystore
    .newSolrQuery()
    .context(context)
    .uriProperty('oa:hasTarget', entry.getResourceURI())
    .rdfType('oa:Annotation')
    .sort('modified+desc')
    .list()
    .getEntries();
  return commentList;
};

/**
 * Comment Subject
 *
 * @param {Entry} entry
 * @returns {string}
 */

const getCommentSubject = (entry) => {
  return entry.getMetadata().findFirstValue(null, 'dcterms:title');
};

/**
 * Comment Body
 *
 * @param {Entry} entry
 * @returns {string}
 */

const getCommentText = (entry) => {
  return entry.getMetadata().findFirstValue(null, 'rdf:value');
};

/**
 * Auther name and author uri
 *
 * @param {Entry} entry
 * @returns {Promise}
 */

const getCommentorAndURI = async (entry) => {
  const userResourceUri = entry.getEntryInfo().getCreator();
  const userEntryURI = entrystore.getEntryURIFromURI(userResourceUri);
  const userEntry = await entrystore.getEntry(userEntryURI);
  return [getLabel(userEntry), userEntryURI];
};

/**
 * Comment date
 *
 * @param {Entry} entry
 * @returns {Date}
 */

const getCommentCreationDate = (entry) => {
  const date = entry.getEntryInfo().getCreationDate();
  return getShortDate(date);
};

/**
 * No of reply of a comment/entry
 *
 * @param {Entry} entry
 * @returns {number}
 */

const getNrOfReplies = (entry) => {
  return entry.getReferrers('oa:hasTarget').length;
};

/**
 * Reply in a formetted object
 *
 * @param {entry} reply
 * @returns {object}
 */

const getReplyEntry = async (reply) => {
  const [author, authorURI] = await getCommentorAndURI(reply);
  return {
    subject: getCommentSubject(reply),
    body: getCommentText(reply),
    author,
    authorURI,
    date: getCommentCreationDate(reply),
    noOfReply: getNrOfReplies(reply),
    entry: reply,
  };
};

const getReplies = async (comment) => {
  const replies = await getReplyList(comment);
  const result = replies.map((reply) => getReplyEntry(reply));
  return Promise.all(result);
};

/**
 * Comment in a formetted object
 *
 * @param {entry} comment
 * @returns {object}
 */

const getCommentEntry = async (comment) => {
  const noOfReply = getNrOfReplies(comment);
  const [author, authorURI] = await getCommentorAndURI(comment);
  let reply = [];
  if (noOfReply > 0) {
    reply = await getReplies(comment);
  }
  return {
    subject: getCommentSubject(comment),
    body: getCommentText(comment),
    author,
    authorURI,
    date: getCommentCreationDate(comment),
    reply,
    noOfReply,
    entry: comment,
  };
};

/**
 * Array of all Comments
 *
 * @param {Array} comments
 * @returns {Promise}
 */

const getComments = (comments) =>
  Promise.all(comments.map((comment) => getCommentEntry(comment)));

const findAndDeleteReplies = async (entry) => {
  if (!entry.getReferrers('oa:hasTarget').length) return;
  return entrystore
    .newSolrQuery()
    .uriProperty('oa:hasTarget', entry.getResourceURI())
    .rdfType('oa:Annotation')
    .list()
    .forEach((replyEntry) =>
      findAndDeleteReplies(replyEntry).then(() => replyEntry.del())
    );
};

const deleteCommentAndReplies = (entry, fetchComments) => {
  return findAndDeleteReplies(entry)
    .then(entry.del.bind(entry))
    .then(async () => {
      await fetchComments();
    });
};

/**
 * @param {Entry} entry
 * @returns {number}
 */
const getNrOfComments = (entry) => {
  const list = entrystore
    .newSolrQuery()
    .uriProperty('oa:hasTarget', entry.getResourceURI())
    .rdfType('oa:Annotation')
    .limit(1)
    .list();
  return list.getEntries().then(() => list.getSize());
};

/**
 * @param {Entry[]} entries
 * @param {Context} context
 * @returns {Promise<object>}
 */
const getCommentsByEntries = async (entries, context) => {
  const commentsCountByEntry = {};
  await entrystore
    .newSolrQuery()
    .uriProperty(
      'oa:hasTarget',
      entries.map((entry) => entry.getResourceURI())
    )
    .rdfType('oa:Annotation')
    .context(context)
    .list()
    .forEach((commentEntry) => {
      const [statement] = commentEntry
        .getMetadata()
        .find(commentEntry.getResourceURI(), 'oa:hasTarget');
      const entryResourceURI = statement.getValue();

      if (!(entryResourceURI in commentsCountByEntry)) {
        commentsCountByEntry[entryResourceURI] = 0;
      }
      commentsCountByEntry[entryResourceURI] += 1;
    });
  return commentsCountByEntry;
};

/**
 * @param {Entry[]} entries
 * @returns {Promise<object>}
 */
export const fetchComments = (entries) => {
  const entriesChunks = chunk(entries, 5);
  const [firstEntry] = entries;

  return Promise.all(
    entriesChunks.map((entriesChunk) =>
      getCommentsByEntries(entriesChunk, firstEntry.getContext())
    )
  ).then((commentsCountResult) =>
    commentsCountResult.reduce(
      (combinedCommentsCountByEntry, commentsCountByEntry) => ({
        ...combinedCommentsCountByEntry,
        ...commentsCountByEntry,
      }),
      {}
    )
  );
};

export {
  addComment,
  getReplyList,
  getComments,
  getReplies,
  deleteCommentAndReplies,
  getCommentText,
  editComment,
  getNrOfComments,
};
