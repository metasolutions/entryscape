import { namespaces, utils } from '@entryscape/rdfjson';

const datasetType = namespaces.expand('dcat:Dataset');
const rdfType = namespaces.expand('rdf:type');

/**
 *
 * @param {rdfjson} catalog
 * @param {rdfjson[]} catalogList
 * @return {rdfjson} catalog
 */
const merge = (mainCatalog, catalogList) => {
  const targetCatalog = mainCatalog.clone();
  const [targetStatement] = targetCatalog.find(
    null,
    'rdf:type',
    'dcat:Catalog'
  );
  const catalogURI = targetStatement.getSubject();

  const cleanCatalogBeforeMerge = (catalog) => {
    const [statement] = catalog.find(null, 'rdf:type', 'dcat:Catalog');
    if (statement) {
      const mergeCatalogURI = statement.getSubject();
      const ignore = {
        [namespaces.expand('dcat:dataset')]: true,
      };
      utils.remove(catalog, mergeCatalogURI, ignore);
    }
    return catalog;
  };

  const mergeWithTarget = (statement) => {
    if (
      statement.getValue() === datasetType &&
      statement.getPredicate() === rdfType
    ) {
      targetCatalog.add(catalogURI, 'dcat:dataset', statement.getSubject());
    }
    targetCatalog.add(statement);
  };

  catalogList.forEach((catalog) => {
    cleanCatalogBeforeMerge(catalog)
      .find()
      .forEach((statement) => {
        mergeWithTarget(statement);
      });
  });

  return targetCatalog;
};

export default merge;
