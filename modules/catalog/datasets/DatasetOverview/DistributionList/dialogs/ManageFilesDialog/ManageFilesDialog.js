import PropTypes from 'prop-types';
import { useState, useCallback, useEffect } from 'react';
import { Entry } from '@entryscape/entrystore-js';
import { entryPropType } from 'commons/util/entry';
import ListActionDialog from 'commons/components/common/dialogs/ListActionDialog';
import { useTranslation } from 'commons/hooks/useTranslation';
import ContentWrapper from 'commons/components/common/ContentWrapper';
import { entrystoreUtil } from 'commons/store';
import { useESContext } from 'commons/hooks/useESContext';
import { useEntry } from 'commons/hooks/useEntry';
import {
  EntryListView,
  TITLE_COLUMN,
  MODIFIED_COLUMN,
  ACTION_MENU_COLUMN,
  SOLR_DELAY,
  ACTIONS_GROUP_COLUMN,
} from 'commons/components/EntryListView';
import {
  LIST_ACTION_EDIT,
  LIST_ACTION_INFO,
} from 'commons/components/EntryListView/actions';
import {
  useListModel,
  ListModelProvider,
  REFRESH,
  useListQuery,
} from 'commons/components/ListView';
import useAsync from 'commons/hooks/useAsync';
import { getLabel } from 'commons/util/rdfUtils';
import { getDistributionFileRURIs } from 'catalog/datasets/DatasetOverview/utils/distributions';
import useGetMainDialog from 'commons/hooks/useGetMainDialog';
import sleep from 'commons/util/sleep';
import EditFileDialog from '../EditFileDialog';
import { useVisualizations } from '../../../hooks/useVisualizations';
import { listActions, rowActions, nlsBundles } from './actions';
import RefreshAPIDialogWrapper from '../RefreshAPIDialog/RefreshAPIDialogWrapper';

const ManageFilesDialog = ({
  apiDistributionEntry,
  distributionEntry,
  onClose,
  onRefreshApi,
}) => {
  const context = useESContext();
  const { getConfirmationDialog } = useGetMainDialog();
  const translate = useTranslation(nlsBundles);
  const [editedFiles, setEditedFiles] = useState(false);
  const { runAsync, data: allListItems, status } = useAsync();
  const [{ search, refreshCount }, dispatch] = useListModel();
  const { result: listItems, size } = useListQuery({
    items: allListItems,
    searchFields: ['label'],
  });

  useEffect(() => {
    const getAllListItems = async () => {
      if (refreshCount > 0) {
        // solr reindexing
        await sleep(SOLR_DELAY);
      }
      const entries = await entrystoreUtil.loadEntriesByResourceURIs(
        getDistributionFileRURIs(distributionEntry),
        context,
        true
      );
      return entries.map((entry) => ({ entry, label: getLabel(entry) }));
    };

    runAsync(getAllListItems());
  }, [runAsync, distributionEntry, context, refreshCount]);

  const refreshFileEntries = async () => dispatch({ type: REFRESH });

  const handleClose = useCallback(() => {
    if (!apiDistributionEntry || !editedFiles) {
      onClose();
      return;
    }

    getConfirmationDialog({
      content: translate('refreshAPI'),
    }).then((proceed) => {
      if (!proceed) {
        onClose();
        return;
      }

      onRefreshApi(true);
    });
  }, [
    apiDistributionEntry,
    editedFiles,
    getConfirmationDialog,
    onClose,
    onRefreshApi,
    translate,
  ]);

  return (
    <ListActionDialog
      id="manage-files"
      title={translate('manageFilesHeader')}
      closeDialog={handleClose}
      closeDialogButtonLabel={translate('manageFilesFooterButton')}
      fixedHeight
    >
      <ContentWrapper>
        <EntryListView
          items={listItems}
          size={size}
          search={search}
          status={status}
          nlsBundles={nlsBundles}
          columns={[
            { ...TITLE_COLUMN, xs: 7 },
            MODIFIED_COLUMN,
            {
              ...ACTIONS_GROUP_COLUMN,
              xs: 2,
              actions: [
                LIST_ACTION_INFO,
                { ...LIST_ACTION_EDIT, Dialog: EditFileDialog },
              ],
            },

            {
              ...ACTION_MENU_COLUMN,
              actions: rowActions,
              filesCount: size,
              distributionEntry,
              refreshFileEntries,
              setEditedFiles,
            },
          ]}
          getListItemProps={({ entry }) => ({
            action: { ...rowActions[0], entry, refreshFileEntries },
          })}
          listActions={listActions}
          listActionsProps={{
            refreshFileEntries,
            setEditedFiles,
            distributionEntry,
          }}
        />
      </ContentWrapper>
    </ListActionDialog>
  );
};

ManageFilesDialog.propTypes = {
  distributionEntry: PropTypes.instanceOf(Entry),
  apiDistributionEntry: PropTypes.instanceOf(Entry),
  onClose: PropTypes.func,
  onRefreshApi: PropTypes.func,
};

const ManageFilesDialogWrapper = ({
  entry: distributionEntry,
  closeDialog,
  refreshDistributions,
  apiDistributionEntry,
}) => {
  const datasetEntry = useEntry();
  const [confirmedApiRefresh, setConfirmedApiRefresh] = useState(false);
  const { refreshVisualizations } = useVisualizations(datasetEntry);

  const refreshAndClose = useCallback(() => {
    refreshVisualizations();
    closeDialog();
  }, [closeDialog, refreshVisualizations]);

  return !confirmedApiRefresh ? (
    <ListModelProvider>
      <ManageFilesDialog
        distributionEntry={distributionEntry}
        apiDistributionEntry={apiDistributionEntry}
        onRefreshApi={setConfirmedApiRefresh}
        onClose={refreshAndClose}
      />
    </ListModelProvider>
  ) : (
    <RefreshAPIDialogWrapper
      entry={apiDistributionEntry}
      closeDialog={refreshAndClose}
      refreshDistributions={refreshDistributions}
    />
  );
};

ManageFilesDialogWrapper.propTypes = {
  entry: PropTypes.instanceOf(Entry),
  closeDialog: PropTypes.func,
  refreshDistributions: PropTypes.func,
  apiDistributionEntry: entryPropType,
};

export default ManageFilesDialogWrapper;
