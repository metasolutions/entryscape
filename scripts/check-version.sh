#!/bin/bash

ENTRYSCAPE_VERSION=$(cat package.json \
  | grep \"version\" \
  | sed 's/[",]//g')

if [[ $ENTRYSCAPE_VERSION != *-dev ]]; then
  echo "Development version must end with the suffix \"-dev\" e.g. 3.2.0-dev";
  exit 1;
fi
