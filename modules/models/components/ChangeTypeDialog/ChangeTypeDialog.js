import ListActionDialog from 'commons/components/common/dialogs/ListActionDialog';
import PropTypes from 'prop-types';
import { entryPropType } from 'commons/util/entry';
import { Button, Grid } from '@mui/material';
import useAsync, { RESOLVED } from 'commons/hooks/useAsync';
import { useTranslation } from 'commons/hooks/useTranslation';
import esmoCommonsNls from 'models/nls/esmoCommons.nls';
import escoDialogsNLS from 'commons/nls/escoDialogs.nls';
import { useSnackbar, SUCCESS_EDIT } from 'commons/hooks/useSnackbar';
import { ErrorWithMessage } from 'commons/errors/utils/errors';
import { ErrorCatcher } from 'commons/errors/ErrorCatcher';
import { useMemo, useState } from 'react';
import {
  findAndCreateRfsItem,
  findRfsItemByRdfType,
} from 'models/utils/rfsItems';
import { applyEntryProps, getEntryProps } from 'models/utils/metadata';
import { CardSelect } from 'commons/components/forms/inputs/CardSelect';
import useReferenceList from 'models/lists/ReferenceList/useReferenceList';
import { withListModelProvider } from 'commons/components/ListView';
import Loader from 'commons/components/Loader';
import Alert from 'commons/components/Alert';
import './ChangeTypeDialog.scss';
import {
  RDF_PROPERTY_EXTENDS,
  RDF_TYPE_FIELD,
  RDF_TYPE_FORM,
} from 'models/utils/ns';

const NLS_BUNDLES = [esmoCommonsNls, escoDialogsNLS];
const RDF_TYPE = [RDF_TYPE_FIELD, RDF_TYPE_FORM];

/**
 * Form item entries uses the extends property, but doesn't have a breaking
 * dependency to the form entry. Thus they can be excluded.
 * Form item extensions don't have the resourceURI as subject.
 *
 * @param {Entry[]} entries
 * @returns {Entry[]}
 */
const exludeFormItemEntries = (entries = []) => {
  return entries.filter((entry) => {
    const { graph, resourceURI } = getEntryProps({ entry });
    return Boolean(graph.findFirstValue(resourceURI, RDF_PROPERTY_EXTENDS));
  });
};

const ChangeTypeDialog = ({
  closeDialog,
  entry,
  onTypeChange,
  rfsItems,
  rdfTypeToRfsItem,
}) => {
  const translate = useTranslation(NLS_BUNDLES);
  const initialRfsItem = useMemo(() => {
    return applyEntryProps(findAndCreateRfsItem, entry, rfsItems);
  }, [entry, rfsItems]);
  const allRfsItems = useMemo(() => {
    const rfsItemsToChangeTo = rfsItems
      .map((RfsItem) => new RfsItem(getEntryProps({ entry })))
      .filter(
        (rfsItem) => rfsItem.getRdfType() !== initialRfsItem.getRdfType()
      );
    return [initialRfsItem, ...rfsItemsToChangeTo];
  }, [rfsItems, initialRfsItem, entry]);
  const [selectedType, setSelectedType] = useState(initialRfsItem.getRdfType());
  const { runAsync, error: saveError } = useAsync();
  const {
    entries,
    status: referenceStatus,
    error: referenceError,
  } = useReferenceList({
    entry,
    rdfType: RDF_TYPE,
    limit: 100,
  });

  // check if the entry is extended or extends
  const hasDependencies = useMemo(() => {
    if (referenceStatus !== RESOLVED) return;
    if (exludeFormItemEntries(entries).length) return true;

    const { graph, resourceURI } = getEntryProps({ entry });
    return Boolean(graph.findFirstValue(resourceURI, RDF_PROPERTY_EXTENDS));
  }, [referenceStatus, entries, entry]);

  const [addSnackbar] = useSnackbar();

  const handleChangeType = () => {
    const changeType = async () => {
      const RfsItem = findRfsItemByRdfType(selectedType, rdfTypeToRfsItem);
      const rfsItem = new RfsItem(getEntryProps({ entry }));
      rfsItem.validateProperties(initialRfsItem.getProperties());
      return entry
        .commitMetadata()
        .then(() => {
          closeDialog();
          onTypeChange();
          addSnackbar({ type: SUCCESS_EDIT });
        })
        .catch((error) => {
          throw new ErrorWithMessage(translate('saveEditsFail'), error);
        });
    };

    runAsync(changeType());
  };

  const actions = (
    <Button
      autoFocus
      onClick={handleChangeType}
      disabled={initialRfsItem.getRdfType() === selectedType}
    >
      {translate('proceed')}
    </Button>
  );

  return (
    <>
      <ListActionDialog
        closeDialog={closeDialog}
        id="change-type"
        actions={actions}
        title={translate('changeTypeLabel')}
        maxWidth="md"
      >
        {referenceStatus === RESOLVED ? (
          <>
            <Grid container>
              <CardSelect
                label={translate('typeLabel')}
                choices={allRfsItems.map((rfsItem) => ({
                  label: translate(rfsItem.getNlsKeyLabel()),
                  value: rfsItem.getRdfType(),
                }))}
                onChange={setSelectedType}
                selectedValue={selectedType}
              />
            </Grid>
            {hasDependencies ? (
              <Alert
                severity="warning"
                title={translate('breakingChangeTitle')}
              >
                {translate('typeChangeWarning')}
              </Alert>
            ) : (
              <Alert
                severity="info"
                title={translate('irreversibleChangeTitle')}
              >
                {translate('typeChangeInfo')}
              </Alert>
            )}
          </>
        ) : (
          <Loader />
        )}
      </ListActionDialog>
      <ErrorCatcher error={saveError || referenceError} />
    </>
  );
};

ChangeTypeDialog.propTypes = {
  closeDialog: PropTypes.func,
  entry: entryPropType,
  onTypeChange: PropTypes.func,
  rfsItems: PropTypes.arrayOf(PropTypes.func),
  rdfTypeToRfsItem: PropTypes.shape({}),
};

export default withListModelProvider(ChangeTypeDialog);
