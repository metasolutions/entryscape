import { Divider as MuiDivider } from '@mui/material';
import './Divider.scss';

const Divider = () => {
  return <MuiDivider className="escoDivider" />;
};

export default Divider;
