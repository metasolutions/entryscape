import { useRef, useEffect } from 'react';
import { useMapContext } from './useMapContext';

export const useMapCursor = () => {
  const hasFeatureAtPointer = useRef(false);
  const [{ map }] = useMapContext();

  // Effect to handle cursor change
  useEffect(() => {
    /**
     * Refreshes the cursor based on the state of last pointer position
     */
    function refreshCursor() {
      map.getTarget().style.cursor = hasFeatureAtPointer.current
        ? 'pointer'
        : '';
    }

    /**
     * Listener to detect feature on pointer move. If feature is detected the
     * cursor is set to pointer.
     *
     * @param {object} event
     * @returns {undefined}
     */
    function onPointerMove(event) {
      const pixel = event.map.getEventPixel(event.originalEvent);
      hasFeatureAtPointer.current = map.hasFeatureAtPixel(pixel);
      if (event.dragging) {
        map.getTarget().style.cursor = 'grab';
        return;
      }
      refreshCursor();
    }

    if (!map) return;
    map.on('pointermove', onPointerMove);
    map.on('moveend', refreshCursor);

    return () => {
      map.un('pointermove', onPointerMove);
      map.un('moveend', refreshCursor);
    };
  }, [map]);
};
