import { Context, Entry } from '@entryscape/entrystore-js';
import {
  AccountTree as AccountTreeIcon,
  BusinessCenter as BusinessCenterIcon,
  AppRegistration as AppRegistrationIcon,
  BrokenImage as BrokenImageIcon,
  Build as BuildIcon,
  MenuBook as MenuBookIcon,
  Search as SearchIcon,
  Security as SecurityIcon,
  Equalizer as EqualizerIcon,
  Business as BusinessIcon,
  Polyline as PolylineIcon,
  Public as PublicIcon,
  Power as PowerIcon,
  Description as DescriptionIcon,
} from '@mui/icons-material';
import { getActiveModules } from 'commons/util/config';
import { getModuleNameOfCurrentView } from 'commons/util/site';
import { contextTypes } from 'commons/util/context';

export const getModuleIconFromName = (name, icon, props) => {
  if (icon) {
    switch (icon) {
      case 'public':
        return <PublicIcon {...props} />;
      case 'power':
        return <PowerIcon {...props} />;
      case 'document':
        return <DescriptionIcon {...props} />;
      case 'build':
        return <BuildIcon {...props} />;
      default:
        return <BrokenImageIcon {...props} />;
    }
  }
  switch (name) {
    case 'catalog':
      return <BusinessCenterIcon {...props} />;
    case 'terms':
      return <AccountTreeIcon {...props} />;
    case 'search':
      return <SearchIcon {...props} />;
    case 'workbench':
      return <AppRegistrationIcon {...props} />;
    case 'models':
      return <PolylineIcon {...props} />;
    case 'admin':
      return <SecurityIcon {...props} />;
    case 'documentation':
      return <MenuBookIcon {...props} />;
    case 'toolkit':
      return <BuildIcon {...props} />;
    case 'status':
      return <EqualizerIcon {...props} />;
    case 'register':
      return <BusinessIcon {...props} />;
    default:
      return <BrokenImageIcon {...props} />;
  }
};

/**
 * Get the current module's async function, if any
 *
 * @param {object} userInfo
 * @returns {Function|undefined}
 */
export const getModuleLayoutPropsFunction = (userInfo) => {
  const moduleName = getModuleNameOfCurrentView();
  const module = getActiveModules(userInfo).find(
    ({ name }) => name === moduleName
  );

  return module?.getLayoutProps;
};

/**
 * Gets the module definition from the name
 *
 * @param {string} name
 * @param {object} userInfo
 * @returns {object}
 */
export const getModuleFromName = (name, userInfo = {}) => {
  const activeModules = getActiveModules(userInfo);
  return activeModules.find((module) => module.name === name);
};

/**
 *
 * @param {Entry} contextEntry
 * @returns {string|undefined}
 */
export const findModuleNameFromContextEntry = (contextEntry) => {
  const resourceURI = contextEntry.getResourceURI();
  const graph = contextEntry.getEntryInfo().getGraph();
  const contextType = contextTypes.find(({ rdfType }) => {
    const statements = graph.find(resourceURI, 'rdf:type', rdfType);
    return statements.length > 0;
  });
  return contextType?.module;
};

/**
 *
 * @param {Context} context
 * @returns {Promise<string|undefined>}
 */
export const findModuleNameFromContext = async (context) => {
  const contextEntry = await context.getEntry();
  return findModuleNameFromContextEntry(contextEntry);
};

export default { getModuleIconFromName };
