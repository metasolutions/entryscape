import { RDF_TYPE_CONCEPT } from 'commons/util/entry';
import { useESContext } from 'commons/hooks/useESContext';
import Lookup from 'commons/types/Lookup';

const useRemoteStore = () => {
  const { context } = useESContext();
  const constraints = {
    'skos:inScheme': null,
    'rdf:type': RDF_TYPE_CONCEPT,
  };
  const projectType = Lookup.getProjectTypeInUse(context, true);
  const entityType = Lookup.getEntityTypeByConstraints(
    constraints,
    projectType
  );
  const remoteStoreConfig = entityType?.getStoreConfig();

  if (!remoteStoreConfig) return {};

  return {
    remoteStore: remoteStoreConfig.store,
    remoteStoreUtil: remoteStoreConfig.util,
    remoteStoreLabel: remoteStoreConfig.label,
  };
};

export default useRemoteStore;
