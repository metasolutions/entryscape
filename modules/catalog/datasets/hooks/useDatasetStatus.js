import { useCallback, useState } from 'react';
import { isDraftEntry, isInternallyPublished } from 'commons/util/entry';
import { isDatasetSeries } from '../utils';

const getInitialDatasetStatus = (entry) => {
  return {
    isDraft: isDraftEntry(entry),
    isSeries: isDatasetSeries(entry),
    isPublished: entry.isPublic(),
    isInternallyPublished: isInternallyPublished(entry),
  };
};

const useDatasetStatus = (entry) => {
  const [datasetStatus, setDatasetStatus] = useState(
    getInitialDatasetStatus(entry)
  );
  const updateStatus = useCallback((change) => {
    setDatasetStatus((currentStatus) => ({
      ...currentStatus,
      ...change,
    }));
  }, []);

  return [datasetStatus, updateStatus];
};

export default useDatasetStatus;
