import PropTypes from 'prop-types';
import { useCallback } from 'react';
import RevisionsEntryDialog from 'commons/components/entry/RevisionsEntryDialog';
import { useOverviewModel, REFRESH } from '../../hooks/useOverviewModel';

const OverviewRevisionsEntryDialog = ({ closeDialog, ...props }) => {
  const [, dispatch] = useOverviewModel();

  const refreshAndClose = useCallback(() => {
    dispatch({ type: REFRESH });
    closeDialog();
  }, [closeDialog, dispatch]);

  return (
    <RevisionsEntryDialog
      onRevertCallback={refreshAndClose}
      closeDialog={closeDialog}
      {...props}
    />
  );
};

OverviewRevisionsEntryDialog.propTypes = {
  closeDialog: PropTypes.func,
};

export default OverviewRevisionsEntryDialog;
