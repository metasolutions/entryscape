import PropTypes from 'prop-types';
import {
  getOutlineItemClasses,
  isActive,
  highlightElement,
  scrollToElement,
  moveFocusToLabel,
} from 'commons/components/rdforms/RdformsOutline/util';
import { useEffect, useRef, useMemo } from 'react';
import {
  IconButton,
  InputAdornment,
  InputBase,
  Link,
  List,
  ListItem,
} from '@mui/material';
import { Clear as ClearIcon, Search as SearchIcon } from '@mui/icons-material';
import Tooltip from 'commons/components/common/Tooltip';
import escoRdformsNLS from 'commons/nls/escoRdforms.nls';
import { useTranslation } from 'commons/hooks/useTranslation';
import { useIntersect } from 'commons/components/rdforms/RdformsOutline/useIntersect';
// eslint-disable-next-line max-len
import useRdformsOutlineSearch from 'commons/components/rdforms/RdformsOutline/useRdformsOutlineSearch';
import esmoCommonsNLS from 'models/nls/esmoCommons.nls';
import sleep from 'commons/util/sleep';
import { useEditorContext } from '../EditorContext';

export const FormOutline = ({
  root,
  formClassName = 'escoFormEditSection',
  errors = [],
}) => {
  const translate = useTranslation([escoRdformsNLS, esmoCommonsNLS]);
  const { graph, resourceURI, rootEditors } = useEditorContext();
  const { searchQuery, onSearchQueryChange, clearSearch } =
    useRdformsOutlineSearch();

  const [observedVisibility, setObservedElements] = useIntersect(
    root,
    formClassName
  );

  const formItems = useMemo(() => {
    if (!graph) return [];
    const includedFormItems = [];
    rootEditors.forEach((rootEditor) => {
      const { editors } = rootEditor;
      const includedEditors = editors?.filter(({ isVisible, labelNlsKey }) => {
        const fieldGroup = isVisible ? isVisible(graph, resourceURI) : true;
        if (!fieldGroup) return false;
        if (searchQuery) {
          const label = translate(labelNlsKey);
          return label.toLowerCase().includes(searchQuery.trim());
        }
        return true;
      });
      if (!includedEditors.length) return;
      const items = [rootEditor, ...includedEditors].map(
        ({ name, labelNlsKey }, index) => ({
          id: name,
          labelId: `fields-label-${name}`,
          label: translate(labelNlsKey),
          indented: index !== 0,
        })
      );
      includedFormItems.push(...items);
    });
    return includedFormItems;
  }, [graph, resourceURI, rootEditors, searchQuery, translate]);

  useEffect(() => {
    if (!formItems.length) return;
    const maxRetries = 10;
    let retryCount = 0;

    // Make sure element is available in the DOM. Could use mutation observer
    // instead, but this solution is easier to control with simpler logic.
    const getOrWaitForElement = async (id) => {
      if (retryCount > maxRetries) {
        console.log(`Tried to find element ${id} but gave up.`);
        return;
      }
      const element = document.getElementById(id);
      if (!element) {
        await sleep(100);
        retryCount += 1;
        return getOrWaitForElement(id);
      }
      return element;
    };

    const getElementsToObserve = async () => {
      const elements = [];
      for (const formItem of formItems) {
        const element = await getOrWaitForElement(formItem.labelId);
        if (element) {
          elements.push(element);
        }
      }
      setObservedElements(elements);
    };
    getElementsToObserve();

    return () => {
      retryCount = maxRetries;
    };
  }, [formItems, setObservedElements]);

  const handleLinkClick = (event, id) => {
    event.preventDefault();
    scrollToElement(id, root, formClassName);
    highlightElement(id);
  };

  const handleKeyDown = (event, id) => {
    if (event.key !== 'Enter') return;

    scrollToElement(id, root, formClassName);
    highlightElement(id);
    // Timeout is necessary, moving the focus interrupts the scrolling behaviour
    setTimeout(() => moveFocusToLabel(id), 600);
  };

  const listItemsRef = useRef();

  return (
    <div className="escoRdformsOutline">
      {
        // using outlineItems here cause formItems change depending on query
        formItems.length > 0 || searchQuery ? (
          <InputBase
            value={searchQuery}
            placeholder={translate('fieldOutlineSearchPlaceholder')}
            classes={{ root: 'escoRdformsOutline__searchInput' }}
            onChange={onSearchQueryChange}
            inputProps={{ 'aria-label': 'Search' }}
            startAdornment={
              <InputAdornment
                position="start"
                classes={{
                  root: 'escoRdformsOutline__searchInputAdornment',
                }}
              >
                <SearchIcon />
              </InputAdornment>
            }
            endAdornment={
              searchQuery ? (
                <InputAdornment position="end">
                  <Tooltip title={translate('fieldOutlineClearTooltip')}>
                    <IconButton
                      aria-label={translate('fieldOutlineClearTooltip')}
                      onClick={clearSearch}
                      onMouseDown={(e) => e.preventDefault()}
                      color="secondary"
                    >
                      <ClearIcon />
                    </IconButton>
                  </Tooltip>
                </InputAdornment>
              ) : null
            }
          />
        ) : null
      }
      {
        // eslint-disable-next-line no-nested-ternary
        formItems.length ? (
          <>
            <nav className="escoRdformsOutline__navigation">
              <List ref={listItemsRef} dense>
                {formItems.map(
                  ({ id, labelId, label, editLabel, indented }) => {
                    const isInvalid = errors.includes(id);
                    return (
                      <ListItem
                        id={`${labelId}_outline`}
                        key={`rdformsOutline-listItem-${id}-${label}`}
                        classes={{
                          root: getOutlineItemClasses(
                            isActive(labelId, observedVisibility),
                            indented,
                            isInvalid
                          ),
                        }}
                      >
                        <Link
                          key={`rdformsOutline-link-${id}`}
                          href={`#${labelId}`}
                          noWrap
                          onClick={(event) => handleLinkClick(event, labelId)}
                          onKeyDown={(event) => handleKeyDown(event, labelId)}
                        >
                          {editLabel || label}
                        </Link>
                      </ListItem>
                    );
                  }
                )}
              </List>
            </nav>
          </>
        ) : searchQuery ? (
          <div className="escoRdformsOutline__searchPlaceholder" role="status">
            {translate('fieldOutlineNoSearchResults')}
          </div>
        ) : null
      }
    </div>
  );
};

FormOutline.propTypes = {
  root: PropTypes.string,
  formClassName: PropTypes.string,
  fieldSet: PropTypes.shape({}),
  errors: PropTypes.arrayOf(PropTypes.string),
};
