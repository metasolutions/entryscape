import { RDF_TYPE_DATASET, RDF_TYPE_DATASET_SERIES } from 'commons/util/entry';
import { entrystore } from 'commons/store';
import config from 'config';

const getDatasetQuery = (context) =>
  entrystore.newSolrQuery().rdfType(RDF_TYPE_DATASET).context(context);

const getDatasetAndSeriesQuery = (context) =>
  entrystore
    .newSolrQuery()
    .context(context)
    .or({
      rdfType: RDF_TYPE_DATASET_SERIES,
      and: {
        rdfType: RDF_TYPE_DATASET,
        not: { 'metadata.predicate.uri.0f683595': '*' }, // dcat:inSeries
      },
    });

export const getListQuery = (context) =>
  config.get('catalog.includeDatasetSeries')
    ? getDatasetAndSeriesQuery(context)
    : getDatasetQuery(context);

export const getCountQuery = async (context) => {
  const query = getListQuery(context).list('1');
  await query.getEntries();
  return query.getSize();
};
