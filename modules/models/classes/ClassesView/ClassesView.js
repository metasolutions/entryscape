import { useCallback } from 'react';
import { useESContext } from 'commons/hooks/useESContext';
import { entrystore } from 'commons/store';
import { Link as LinkIcon } from '@mui/icons-material';
import {
  ListItemIcon,
  withListModelProviderAndLocation,
  listPropsPropType,
} from 'commons/components/ListView';
import {
  EntryListView,
  useSolrQuery,
  TITLE_COLUMN,
  MODIFIED_COLUMN,
  TOGGLE_ENTRY_COLUMN,
  ACTIONS_GROUP_COLUMN,
} from 'commons/components/EntryListView';
import {
  LIST_ACTION_INFO,
  LIST_ACTION_EDIT,
} from 'commons/components/EntryListView/actions';
import { getPathFromViewName } from 'commons/util/site';
import { getLabel } from 'commons/util/rdfUtils';
import ModelsLDBDialog from 'models/dialogs/ModelsLDBDialog/ModelsLDBDialog';
import { listActions, nlsBundles } from './actions';

const ClassesView = ({ listProps }) => {
  const { context } = useESContext();

  const createQuery = useCallback(
    () => entrystore.newSolrQuery().rdfType('rdfs:Class').context(context),
    [context]
  );
  const queryResults = useSolrQuery({ createQuery });

  return (
    <EntryListView
      {...queryResults}
      {...listProps}
      nlsBundles={nlsBundles}
      listActions={listActions}
      listActionsProps={{ nlsBundles }}
      getListItemProps={({ entry }) => ({
        to: getPathFromViewName('models__classes__class', {
          contextId: entry.getContext().getId(),
          entryId: entry.getId(),
        }),
      })}
      columns={[
        TOGGLE_ENTRY_COLUMN,
        {
          ...TITLE_COLUMN,
          xs: 6,
          getProps: ({ entry }) => ({
            primary: getLabel(entry),
            secondary: entry.getResourceURI(),
          }),
        },
        {
          id: 'status-icons',
          xs: 1,
          Component: ListItemIcon,
          getProps: ({ entry, translate }) =>
            entry.isLinkReference()
              ? {
                  icon: <LinkIcon color="primary" />,
                  tooltip: translate('importedTooltip'),
                }
              : {},
        },
        MODIFIED_COLUMN,
        {
          ...ACTIONS_GROUP_COLUMN,
          actions: [
            {
              ...LIST_ACTION_INFO,
              Dialog: ModelsLDBDialog,
            },
            LIST_ACTION_EDIT,
          ],
        },
      ]}
    />
  );
};

ClassesView.propTypes = {
  listProps: listPropsPropType,
};

export default withListModelProviderAndLocation(ClassesView);
