import { useESContext } from 'commons/hooks/useESContext';
import Status from './index';

const StatusView = () => {
  const { context } = useESContext();

  return <Status contextId={context.getId()} />;
};

export default StatusView;
