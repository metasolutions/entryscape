/* eslint-disable react/no-array-index-key */
import PropTypes from 'prop-types';
import config from 'config';
import {
  Field,
  Fields,
  Text,
  Language,
} from 'commons/components/forms/presenters';
import { graphPropType } from 'commons/util/entry';
import { getRfsAlternativeTexts } from 'models/utils/alternativeText';
import { useMemo } from 'react';
import { localize } from 'commons/locale';

/**
 * Find matching label in config for a given role.
 *
 * @param {string} role
 * @returns {string|undefined}
 */
const findAlternativeTextLabel = (role) => {
  const alternativeTextConfigs = config.get('models.alternativeTexts');
  const alternativeTextConfig = alternativeTextConfigs?.find(
    ({ value }) => value === role
  );
  if (!alternativeTextConfig) return;
  return localize(alternativeTextConfig.label);
};

const LanguageLiterals = ({ lang2val }) => {
  return Object.entries(lang2val).map(([language, value], index) => {
    return (
      <Field key={index}>
        <Text text={value} />
        <Language language={language} />
      </Field>
    );
  });
};

const AlternativeText = ({ role, texts }) => {
  const label = findAlternativeTextLabel(role) || role;

  return (
    <Fields label={label}>
      {texts.map((lang2val, index) => (
        <LanguageLiterals key={index} lang2val={lang2val} />
      ))}
    </Fields>
  );
};

AlternativeText.propTypes = {
  role: PropTypes.string,
  texts: PropTypes.arrayOf(PropTypes.shape({})),
};

export const AlternativeTexts = ({ graph, resourceURI }) => {
  const alternativeTexts = useMemo(() => {
    return Object.entries(getRfsAlternativeTexts(graph, resourceURI));
  }, [graph, resourceURI]);

  if (!alternativeTexts.length) return null;

  return alternativeTexts.map(([role, texts]) => {
    return <AlternativeText key={role} role={role} texts={texts} />;
  });
};

AlternativeTexts.propTypes = {
  graph: graphPropType,
  resourceURI: PropTypes.string,
};
