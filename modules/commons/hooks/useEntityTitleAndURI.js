import { debounce } from 'lodash-es';
import { useState } from 'react';
import { i18n } from 'esi18n';
import {
  isNameFreeToUse,
  constructURIFromPattern,
  isResourceURIUniqueInContext,
} from 'commons/util/store';
import { replaceSpecialChar } from 'commons/util/util';

const DELAY = 300;

/**
 *
 * Check if ruri is available in context
 *
 * @param {*} name
 * @param {*} context
 * @param {*} contextName
 * @param {*} uriPattern
 */
const isNameUniqueInContext = (name, context, contextName, uriPattern) => {
  const resourceURI = constructURIFromPattern(uriPattern, {
    contextName,
    entryName: name,
  });
  return isResourceURIUniqueInContext(resourceURI, context);
};

const useEntityURI = ({
  context = null,
  contextName = '',
  uriPattern = '',
}) => {
  // dom state
  const [name, setName] = useState(''); // the last part of the uri

  // ui state
  const [isNameFree, setIsNameFree] = useState(true); // can I use this name?
  const [isLoading, setIsLoading] = useState(true); // defaul to true to avoid initial invalid feedback
  const [hasNameChangedManually, setHasNameChangedManually] = useState(false); // Keep state if name has been changed manually to be avoid changing it automatically if title changes

  const checkNameStatus = (checkName) => {
    setIsLoading(true);

    // different check for primary entity types in context vs secondary entity types.
    // - Primary entity type (e.g ConceptScheme) => checks only if alias/name is available in the ES installation
    // - Secondary entity type (e.g Concept) => checks if the resource uri is unique in given context
    const uniqunessCheckPromise = context
      ? isNameUniqueInContext(checkName, context, contextName, uriPattern)
      : isNameFreeToUse(checkName);

    uniqunessCheckPromise.then(setIsNameFree).then(() => setIsLoading(false));
  };

  const checkNameStatusDebounced = debounce(checkNameStatus, DELAY);

  const updateName = (value) => {
    const language = i18n.getLocale();
    const newValue = replaceSpecialChar(value, language);

    //  name doesn't update the name anymore
    const normalizedName = newValue.replace(/[^\w.-]+/g, ''); // remove any non-[alphanumeric | underscore | dash | dot] characters
    // check which field was updated and update the name accordingly
    setName(normalizedName);

    // make sure that inputed name does not exists and update UI accordingly
    if (normalizedName) {
      checkNameStatusDebounced(normalizedName);
    }
  };

  /**
   * Handle the name input field and check if name is unique on
   * @param {Event} event
   */
  const handleNameChange = (event) => {
    updateName(event.target.value);
    setHasNameChangedManually(true);
  };

  return {
    name,
    setName, // no check for uniqness
    updateName,
    handleNameChange,
    isNameFree,
    isLoading,
    hasNameChangedManually,
    setHasNameChangedManually,
  };
};

const useEntityTitleAndURI = ({
  context = null,
  contextName = '',
  uriPattern = '',
}) => {
  // dom state
  const [title, setTitle] = useState('');

  const {
    name,
    updateName,
    handleNameChange,
    // ui state
    isNameFree,
    isLoading,
    hasNameChangedManually,
    setHasNameChangedManually,
  } = useEntityURI({ context, contextName, uriPattern });

  const handleTitleChange = (evt) => {
    const { value } = evt.target;
    const newTitle = value.trim().length === 0 ? value.trim() : value;

    setTitle(newTitle);
    if (uriPattern && !hasNameChangedManually) {
      updateName(newTitle);
    }
  };

  const reinitTitleAndURIState = () => {
    setTitle('');
    updateName('');
  };

  return {
    title,
    name,
    isNameFree,
    isLoading,
    handleTitleChange,
    handleNameChange,
    reinitTitleAndURIState,
    setHasNameChangedManually,
  };
};

export { useEntityTitleAndURI, useEntityURI };
