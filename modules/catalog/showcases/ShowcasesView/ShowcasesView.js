import Lookup from 'commons/types/Lookup';
import { useESContext } from 'commons/hooks/useESContext';
import { RDF_TYPE_SHOWCASE } from 'commons/util/entry';
import { entrystore } from 'commons/store';
import { getPathFromViewName } from 'commons/util/site';
import TableView from 'commons/components/TableView';
import {
  EntryListView,
  useSolrQuery,
  TITLE_COLUMN,
  MODIFIED_COLUMN,
  ACTIONS_GROUP_COLUMN,
  TOGGLE_ENTRY_COLUMN,
} from 'commons/components/EntryListView';
import {
  LIST_ACTION_INFO,
  LIST_ACTION_EDIT,
} from 'commons/components/EntryListView/actions';
import {
  useListModel,
  withListModelProviderAndLocation,
  TOGGLE_TABLEVIEW,
  listPropsPropType,
} from 'commons/components/ListView';
import { useCallback } from 'react';
import { listActions, nlsBundles } from './actions';

const ShowcasesView = ({ listProps }) => {
  const [{ showTableView }, dispatch] = useListModel();
  const { context } = useESContext();

  const createQuery = useCallback(
    () => entrystore.newSolrQuery().rdfType(RDF_TYPE_SHOWCASE).context(context),
    [context]
  );
  const queryResults = useSolrQuery({ createQuery });

  return showTableView ? (
    <TableView
      setTableView={() => dispatch({ type: TOGGLE_TABLEVIEW })}
      rdfType={RDF_TYPE_SHOWCASE}
      templateId={Lookup.getByName('datasetResult').complementaryTemplateId()}
    />
  ) : (
    <EntryListView
      {...queryResults}
      {...listProps}
      nlsBundles={nlsBundles}
      getListItemProps={({ entry }) => ({
        to: getPathFromViewName('catalog__showcases__showcase', {
          contextId: context.getId(),
          entryId: entry.getId(),
        }),
      })}
      includeTableView
      listActions={listActions}
      listActionsProps={{ nlsBundles }}
      columns={[
        {
          ...TOGGLE_ENTRY_COLUMN,
          getProps: ({ entry, translate }) => ({
            entry,
            publicTitle: translate('publicResultTitle'),
            privateTitle: translate('privateResultTitle'),
            publishDisabledTitle: translate('publishDisabledTitle'),
            toggleAriaLabel: translate('publishToggleAriaLabel'),
          }),
        },
        { ...TITLE_COLUMN, xs: 7 },
        MODIFIED_COLUMN,
        {
          ...ACTIONS_GROUP_COLUMN,
          actions: [LIST_ACTION_INFO, LIST_ACTION_EDIT],
        },
      ]}
    />
  );
};

ShowcasesView.propTypes = {
  listProps: listPropsPropType,
};

export default withListModelProviderAndLocation(ShowcasesView);
