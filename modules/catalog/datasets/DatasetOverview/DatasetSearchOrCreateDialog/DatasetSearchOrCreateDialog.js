import PropTypes from 'prop-types';
import { useCallback, useState } from 'react';
import config from 'config';
import { Button } from '@mui/material';
import {
  withListModelProvider,
  useListModel,
  ListItemButtonAction,
  REFRESH,
  withListRefresh,
} from 'commons/components/ListView';
import {
  EntryListView,
  useSolrQuery,
  MODIFIED_COLUMN,
  TITLE_COLUMN,
  INFO_COLUMN,
  MultiCreateButton,
  DRAFT_STATUS_COLUMN,
} from 'commons/components/EntryListView';
import { ACTION_CREATE_ID } from 'commons/actions/actionIds';
import { LIST_ACTION_INFO } from 'commons/components/EntryListView/actions';
import {
  nlsBundlesPropType,
  useTranslation,
} from 'commons/hooks/useTranslation';
import escoDialogsNLS from 'commons/nls/escoDialogs.nls';
import escoPlaceholderNLS from 'commons/nls/escoPlaceholder.nls';
import ContentWrapper from 'commons/components/common/ContentWrapper';
import { entrystore } from 'commons/store';
import { addRelationToParent } from 'commons/types/utils/relations';
import {
  RDF_TYPE_DATASET,
  RDF_TYPE_DATASET_SERIES,
  refreshEntry,
} from 'commons/util/entry';
import { useESContext } from 'commons/hooks/useESContext';
import { useEntry } from 'commons/hooks/useEntry';
// eslint-disable-next-line max-len
import DatasetLinkedDataBrowserDialog from 'catalog/datasetTemplates/DatasetLinkedDataBrowserDialog';
import ListActionDialog from 'commons/components/common/dialogs/ListActionDialog';
import CreateDatasetDialog from '../../dialogs/CreateDatasetDialog';
import SelectDatasetTemplateDialog from '../../dialogs/SelectDatasetTemplateDialog';
import CreateDatasetSeriesDialog from '../../dialogs/CreateDatasetSeriesDialog';
import {
  createProtoDatasetFromSeriesCallback,
  createProtoSeriesFromDatasetCallback,
} from '../dialogs/ManageSeriesDialog/util';
import { isDatasetSeries } from '../../utils';
import { SERIES_TAG } from '../../tags';

const defaultNlsBundles = [escoDialogsNLS, escoPlaceholderNLS];

const baseCreateAction = {
  id: 'create',
  Dialog: withListRefresh(CreateDatasetDialog, 'onCreate'),
  labelNlsKey: 'createButtonLabel',
  tooltipNlsKey: 'actionTooltip',
};

const DatasetSearchOrCreateDialog = ({
  actionParams: { refreshDatasets, numberOfDatasets },
  closeDialog,
  nlsBundles: optionalNlsBundles = [],
}) => {
  const nlsBundles = [...optionalNlsBundles, ...defaultNlsBundles];
  const translate = useTranslation(nlsBundles);
  const { context } = useESContext();
  const seriesEntry = useEntry();
  const [, dispatch] = useListModel();
  const [hasConnected, setHasConnected] = useState(false);
  const includeSubSeries = config.get('catalog.includeSubSeries');

  const createQuery = useCallback(
    () =>
      entrystore
        .newSolrQuery()
        .rdfType([
          RDF_TYPE_DATASET,
          ...(includeSubSeries ? [RDF_TYPE_DATASET_SERIES] : []),
        ])
        .uri(seriesEntry.getURI(), true)
        .context(context),
    [context, seriesEntry, includeSubSeries]
  );
  const queryResults = useSolrQuery({ createQuery });

  const getConnectButtonProps = ({ entry: datasetEntry, translate: t }) => {
    const relationStatements = datasetEntry
      .getMetadata()
      .find(null, 'dcat:inSeries', seriesEntry.getResourceURI());
    const connected = Boolean(relationStatements.length);

    const onClick = async () => {
      await addRelationToParent(
        datasetEntry,
        seriesEntry,
        'dcat:inSeries'
      ).then(() => {
        dispatch({ type: REFRESH });
        setHasConnected(true);
      });
    };

    return {
      disabled: connected,
      children: connected ? t('connectedButtonLabel') : t('connectButtonLabel'),
      variant: 'text',
      onClick,
      justifyContent: 'center',
    };
  };

  const handleClose = () => {
    closeDialog();
    if (hasConnected) refreshDatasets();
  };

  const onCreate = async (datasetEntry) => {
    // is called in create dataset dialog and create entry by template (without a param)
    if (!datasetEntry) return;

    await refreshEntry(datasetEntry);

    datasetEntry
      .getMetadata()
      .add(
        datasetEntry.getResourceURI(),
        'dcat:inSeries',
        seriesEntry.getResourceURI()
      );
    await datasetEntry.commitMetadata();
  };

  const afterCreateEntry = () => {
    refreshDatasets();
    handleClose();
  };

  const datasetTemplatesEnabled = config.get('catalog.datasetTemplateEndpoint');
  const datasetSeriesEnabled = config.get('catalog.includeDatasetSeries');
  const createAction = {
    id: ACTION_CREATE_ID,
    onCreate,
    afterCreateEntry,
    items: [
      {
        id: 'create',
        action: {
          Dialog: withListRefresh(CreateDatasetDialog, 'onCreate'),
        },
        labelNlsKey: 'createDatasetButton',
      },
      ...(datasetTemplatesEnabled
        ? [
            {
              id: 'create-from-dataset-template',
              action: {
                Dialog: withListRefresh(
                  SelectDatasetTemplateDialog,
                  'onCreate'
                ),
              },
              labelNlsKey: 'createFromTemplateButton',
              tooltipNlsKey: 'createDatasetFromTemplateButtonTooltip',
            },
          ]
        : []),
      ...(datasetSeriesEnabled && includeSubSeries
        ? [
            {
              id: 'create-dataset-series',
              action: {
                Dialog: withListRefresh(CreateDatasetSeriesDialog, 'onCreate'),
              },
              labelNlsKey: 'createSeriesButton',
            },
          ]
        : []),
    ],
  };

  const actions =
    datasetTemplatesEnabled || (datasetSeriesEnabled && includeSubSeries)
      ? [
          {
            Component: MultiCreateButton,
            ...createAction,
          },
        ]
      : [baseCreateAction];

  const prototypeCallback = useCallback(
    (prototypeEntry) =>
      isDatasetSeries(prototypeEntry)
        ? createProtoSeriesFromDatasetCallback(seriesEntry, prototypeEntry)
        : createProtoDatasetFromSeriesCallback(seriesEntry, prototypeEntry),
    [seriesEntry]
  );

  const actionParams = {
    tooltip: translate(
      includeSubSeries
        ? 'createButtonTooltipWithSubSeries'
        : 'createButtonTooltip'
    ),
    numberOfDatasets,
    refreshDatasets: () => dispatch({ type: REFRESH }),
    prototypeCallback,
  };

  return (
    <ListActionDialog
      id="search-or-create-dialog"
      title={translate('searchOrCreateDialogTitle')}
      closeDialog={handleClose}
      closeDialogButtonLabel={translate('close')}
      maxWidth="md"
      fixedHeight
    >
      <ContentWrapper md={12}>
        <EntryListView
          {...queryResults}
          nlsBundles={nlsBundles}
          listActions={actions}
          listActionsProps={{
            onCreate,
            afterCreateEntry,
            nlsBundles,
            actionParams,
          }}
          renderViewPlaceholder={
            includeSubSeries || datasetTemplatesEnabled
              ? (Placeholder) => (
                  <Placeholder
                    {...(datasetSeriesEnabled && includeSubSeries
                      ? { label: translate('emptyListWarningSeriesEnabled') }
                      : {})}
                  >
                    <MultiCreateButton
                      action={{ ...createAction, actionParams }}
                    />
                  </Placeholder>
                )
              : null
          }
          columns={[
            DRAFT_STATUS_COLUMN,
            {
              ...TITLE_COLUMN,
              xs: 6,
              tags: SERIES_TAG,
            },
            MODIFIED_COLUMN,
            {
              ...INFO_COLUMN,
              getProps: ({ entry, translate: t }) => ({
                action: {
                  ...LIST_ACTION_INFO,
                  entry,
                  nlsBundles,
                  Dialog: DatasetLinkedDataBrowserDialog,
                },
                title: t('infoEntry'),
              }),
            },
            {
              id: 'connect',
              xs: 2,
              Component: ListItemButtonAction,
              ButtonComponent: Button,
              getProps: getConnectButtonProps,
            },
          ]}
        />
      </ContentWrapper>
    </ListActionDialog>
  );
};

DatasetSearchOrCreateDialog.propTypes = {
  closeDialog: PropTypes.func,
  actionParams: PropTypes.shape({
    refreshDatasets: PropTypes.func,
    numberOfDatasets: PropTypes.number,
  }),
  nlsBundles: nlsBundlesPropType,
};

export default withListModelProvider(DatasetSearchOrCreateDialog);
