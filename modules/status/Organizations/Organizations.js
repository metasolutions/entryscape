import { useCallback } from 'react';
import { types } from '@entryscape/entrystore-js';
import CheckCircleIcon from '@mui/icons-material/CheckCircle';
import { entrystore } from 'commons/store';
import escoListNLS from 'commons/nls/escoList.nls';
import esreStatusNLS from 'status/nls/esreStatus.nls';
import escoPlaceholderNLS from 'commons/nls/escoPlaceholder.nls';
import { useTranslation } from 'commons/hooks/useTranslation';
import { useUserState } from 'commons/hooks/useUser';
import { success } from 'commons/theme/mui/colors';
import { isGuest } from 'commons/util/user';
import {
  EntryListView,
  useSolrQuery,
  TITLE_COLUMN,
  MODIFIED_COLUMN,
  INFO_COLUMN,
} from 'commons/components/EntryListView';
import { LIST_ACTION_INFO } from 'commons/components/EntryListView/actions';
import {
  withListModelProvider,
  ListItemIcon,
} from 'commons/components/ListView';
import PipelineResultDialog from 'harvest/dialogs/PipelineResultDialog';
import { SECTOR_FILTER } from 'commons/components/filters/utils/filterDefinitions';
import useSearchFilters from 'commons/components/filters/hooks/useSearchFilters';
import { multiListFetchEntries } from 'commons/util/solr/entry';
import config from 'config';
import OrganizationLinkedDataBrowserDialog from './OrganizationLinkedDataBrowserDialog';

const nlsBundles = [esreStatusNLS, escoPlaceholderNLS, escoListNLS];

const Organizations = () => {
  const translate = useTranslation(nlsBundles);
  const { userEntry } = useUserState();
  const { applyFilters, isLoading, hasFilters, ...filterProps } =
    useSearchFilters([SECTOR_FILTER]);

  const createQuery = useCallback(() => {
    const queries = [
      entrystore.newSolrQuery().literalProperty('storepr:merge', 'true'),
      entrystore.newSolrQuery().literalProperty('storepr:merge', 'true', 'not'),
    ];
    return queries.map((query) => {
      query.graphType(types.GT_PIPELINERESULT).tagLiteral(['latest']);
      if (isGuest(userEntry)) query.publicRead(true);
      return query;
    });
  }, [userEntry]);

  const multiListApplyFilters = useCallback(
    (queries) => queries.map((query) => applyFilters(query)),
    [applyFilters]
  );

  const queryResults = useSolrQuery({
    createQuery,
    runQuery: multiListFetchEntries,
    applyFilters: multiListApplyFilters,
  });

  return (
    <EntryListView
      {...queryResults}
      nlsBundles={nlsBundles}
      viewPlaceholderProps={{
        label: translate(
          'emptyMessageWithName',
          translate('placeholderEntityName')
        ),
      }}
      getListItemProps={({ entry }) => ({
        action: {
          entry,
          Dialog: PipelineResultDialog,
          contextId: entry.getContext().getId(),
        },
      })}
      includeFilters={hasFilters}
      filtersProps={filterProps}
      rowsPerPageOptions={[]}
      columns={[
        {
          ...TITLE_COLUMN,
          headerNlsKey: 'organizationLabel',
          title: translate('otherOrgTitle'),
          xs: 7,
        },
        {
          id: 'datasets',
          xs: 2,
          headerNlsKey: 'dcatAPLabel',
          title: translate('dcatAPTitle'),
          justifyContent: 'center',
          Component: ListItemIcon,
          getProps: ({ entry }) => {
            const datasetsFound = entry
              .getMetadata()
              .findFirstValue(null, 'storepr:merge');

            return {
              icon: datasetsFound ? (
                <CheckCircleIcon style={{ color: success.main }} />
              ) : null,
            };
          },
        },
        {
          ...MODIFIED_COLUMN,
          headerNlsKey: 'checkedLabel',
          title: translate('checkedTitle'),
          sortTooltipAlias: translate('checkedSortTooltipLabel'),
        },
        {
          ...INFO_COLUMN,
          getProps: ({ entry, translate: translateProp }) => ({
            action: {
              ...LIST_ACTION_INFO,
              entry,
              nlsBundles,
              Dialog: OrganizationLinkedDataBrowserDialog,
            },
            title: translateProp('infoEntry'),
          }),
        },
      ]}
    />
  );
};

export default withListModelProvider(Organizations, () => ({
  showFilters: config.get('theme.expandReportsFilter'),
}));
