import * as fieldDefinitions from './fieldDefinitions';

const presenterNames = [
  'label',
  'description',
  'module',
  'defaults',
  'optional',
];

export const fieldPresenters = presenterNames.map((name) => ({
  name,
  ...fieldDefinitions[name],
}));
