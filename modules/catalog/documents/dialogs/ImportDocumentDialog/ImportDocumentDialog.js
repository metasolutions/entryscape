import PropTypes from 'prop-types';
import { useState, useCallback, useMemo } from 'react';
import { OpenInNew as ExternalLinkIcon } from '@mui/icons-material';
import { Button } from '@mui/material';
import { Entry, EntryStore } from '@entryscape/entrystore-js';
import sleep from 'commons/util/sleep';
import ListActionDialog from 'commons/components/common/dialogs/ListActionDialog';
import ContentWrapper from 'commons/components/common/ContentWrapper';
import escoListNLS from 'commons/nls/escoList.nls';
import escaImportSpecificationNLS from 'catalog/nls/escaImportSpecification.nls';
import { useTranslation } from 'commons/hooks/useTranslation';
import { useESContext } from 'commons/hooks/useESContext';
import {
  getImportedSpecEntries,
  importSpecification,
} from 'catalog/documents/util';
import config from 'config';
import useAsyncCallback from 'commons/hooks/useAsyncCallback';
import {
  EntryListView,
  useSolrQuery,
  TITLE_COLUMN,
  MODIFIED_COLUMN,
  INFO_COLUMN,
} from 'commons/components/EntryListView';
import {
  useListModel,
  withListModelProvider,
  CREATE,
  ListItemIconButton,
  ListItemAction,
} from 'commons/components/ListView';
import { useSnackbar, ERROR } from 'commons/hooks/useSnackbar';
import escaDocumentsNLS from 'catalog/nls/escaDocuments.nls';
import LinkedDataBrowserDialog from 'commons/components/LinkedDataBrowserDialog';

const IMPORT_DELAY_MSEC = 100;

const nlsBundles = [escoListNLS, escaImportSpecificationNLS, escaDocumentsNLS];

const ImportButton = ({
  entry,
  disabled: importedOrPending,
  pendingImports,
  setPendingImports,
}) => {
  const translate = useTranslation([escaImportSpecificationNLS]);

  return (
    <ListItemAction>
      <Button
        disabled={importedOrPending}
        variant="text"
        onClick={(event) => {
          event.stopPropagation();
          setPendingImports([...pendingImports, entry]);
        }}
      >
        {importedOrPending
          ? translate('importedButtonLabel')
          : translate('importSpecDialogButton')}
      </Button>
    </ListItemAction>
  );
};

ImportButton.propTypes = {
  entry: PropTypes.instanceOf(Entry).isRequired,
  disabled: PropTypes.bool,
  pendingImports: PropTypes.arrayOf(PropTypes.instanceOf(Entry)),
  setPendingImports: PropTypes.func,
};

const ImportDocumentDialog = ({
  closeDialog,
  actionParams: { refreshDocuments },
}) => {
  const translate = useTranslation(nlsBundles);
  const [addSnackbar] = useSnackbar();
  const { context } = useESContext();
  const [infoDialogEntry, setInfoDialogEntry] = useState(null);
  const [pendingImports, setPendingImports] = useState([]);
  const [importedSpecEntries] = useAsyncCallback(
    getImportedSpecEntries,
    context,
    []
  );
  const [, dispatch] = useListModel();

  const remoteStoreURI = config.get('catalog.remoteSpecEndpoint');
  if (!remoteStoreURI) {
    console.error('Missing remote specifications endpoint configuration');
  }

  const remoteSpecsEntryStore = useMemo(
    () => new EntryStore(remoteStoreURI),
    [remoteStoreURI]
  );
  remoteSpecsEntryStore.getREST().disableJSONP();
  remoteSpecsEntryStore.getREST().disableCredentials();

  const createQuery = useCallback(
    () =>
      remoteSpecsEntryStore
        .newSolrQuery()
        .rdfType(['dcterms:Standard', 'prof:Profile'])
        .publicRead(true),
    [remoteSpecsEntryStore]
  );
  const queryResults = useSolrQuery({ createQuery });

  const closeInfoDialog = () => {
    setInfoDialogEntry(null);
  };

  const dispatchEntryCreated = async () => {
    await sleep(pendingImports.length * IMPORT_DELAY_MSEC); // default SOLR_DELAY not enough when importing multiple
    dispatch({ type: CREATE });
  };

  const importAndClose = async () => {
    closeDialog();

    await Promise.all(
      pendingImports.map((remoteEntry) =>
        importSpecification(remoteEntry, context)
      )
    )
      .then(() => {
        if (!pendingImports.length) return;

        dispatchEntryCreated();
        refreshDocuments();
        addSnackbar({
          message: translate('importDocumentsSuccess', pendingImports.length),
        });
      })
      .catch((err) => {
        console.error(err);
        addSnackbar({
          message: translate('importDocumentsFail', pendingImports.length),
          type: ERROR,
        });
      });
  };

  return (
    <>
      <ListActionDialog
        id="import-specification-dialog"
        title={translate('importSpecDialogHeader')}
        closeDialog={importAndClose}
        closeDialogButtonLabel={translate('importDialogCloseButton')}
        maxWidth="md"
        fixedHeight
      >
        <ContentWrapper md={12}>
          <EntryListView
            {...queryResults}
            nlsBundles={nlsBundles}
            columns={[
              { ...TITLE_COLUMN, xs: 6, headerNlsKey: 'specListHeaderLabel' },
              { ...MODIFIED_COLUMN, headerNlsKey: 'specListHeaderModified' },
              {
                ...INFO_COLUMN,
                title: translate('infoEntry'),
                getProps: ({ entry }) => ({
                  onClick: () => setInfoDialogEntry(entry),
                }),
              },
              {
                id: 'source-link',
                xs: 1,
                Component: ListItemIconButton,
                getProps: ({ entry }) => ({
                  title: translate('specLinkTooltip'),
                  children: <ExternalLinkIcon />,
                  href: entry.getResourceURI(),
                  target: '_blank',
                  'aria-label': translate('specLinkTooltip'),
                }),
              },
              {
                id: 'import-action',
                xs: 2,
                Component: ImportButton,
                getProps: ({ entry }) => ({
                  entry,
                  disabled: [...importedSpecEntries, ...pendingImports].some(
                    (importedEntry) =>
                      entry.getResourceURI() === importedEntry.getResourceURI()
                  ),
                  pendingImports,
                  setPendingImports,
                }),
              },
            ]}
          />
        </ContentWrapper>
      </ListActionDialog>
      {infoDialogEntry && (
        <LinkedDataBrowserDialog
          entry={infoDialogEntry}
          closeDialog={closeInfoDialog}
        />
      )}
    </>
  );
};

ImportDocumentDialog.propTypes = {
  closeDialog: PropTypes.func.isRequired,
  actionParams: PropTypes.shape({ refreshDocuments: PropTypes.func }),
};

export default withListModelProvider(ImportDocumentDialog);
