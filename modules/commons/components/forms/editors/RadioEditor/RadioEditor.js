import PropTypes from 'prop-types';
import { graphPropType } from 'commons/util/entry';
import { useTranslation } from 'commons/hooks/useTranslation';
import escoFormsNls from 'commons/nls/escoForms.nls';
import { useFieldsEdit } from '../hooks/useFieldsEdit';
import { Fields } from '../Fields';
import { Field } from '../Field';
import { RadioGroup } from '../RadioGroup';
import { normalizeChoices } from '../utils/choices';

// Note that if choices should be expanded, nodetype must be specified as URI.
export const RadioEditor = ({
  property,
  choices,
  nodetype,
  label,
  xs,
  disabled,
  row,
  size,
  children,
  graph,
  resourceURI,
  dispatch,
  isFormType,
  initialValue,
  translate,
  ...fieldsProps
}) => {
  const translateError = useTranslation(escoFormsNls);
  const { fields, labelId, addField, removeField, updateValue } = useFieldsEdit(
    {
      name: fieldsProps.name,
      property,
      nodetype,
      graph,
      resourceURI,
      dispatch,
      mandatory: fieldsProps.mandatory,
      initialValue,
      isFormType,
    }
  );

  return (
    <Fields
      label={label}
      labelId={labelId}
      addField={addField}
      {...fieldsProps}
    >
      {children}
      {fields.map((field) => {
        return (
          <Field
            key={field.getId()}
            disabled={disabled}
            size={fields.length}
            onRemove={() => removeField(field)}
            onClear={() => updateValue(field, '')}
          >
            <RadioGroup
              xs={xs}
              labelId={labelId}
              choices={normalizeChoices(choices, nodetype, translate)}
              onChange={({ target }) => updateValue(field, target.value)}
              value={field.getValue()}
              row={row}
              size={size}
              errorText={
                field.hasError() ? translateError(field.getError()) : ''
              }
            />
          </Field>
        );
      })}
    </Fields>
  );
};

RadioEditor.propTypes = {
  label: PropTypes.string,
  choices: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  nodetype: PropTypes.string.isRequired,
  property: PropTypes.string.isRequired,
  xs: PropTypes.number,
  disabled: PropTypes.bool,
  row: PropTypes.bool,
  size: PropTypes.string,
  graph: graphPropType,
  resourceURI: PropTypes.string,
  dispatch: PropTypes.func,
  isFormType: PropTypes.bool,
  initialValue: PropTypes.string,
  children: PropTypes.node,
  translate: PropTypes.func,
};
