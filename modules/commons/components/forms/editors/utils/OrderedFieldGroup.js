import { OrderedField } from './OrderedField';
import { FieldGroup } from './FieldGroup';

/**
 *
 * @param {object} fieldProperties
 * @returns {object[]}
 */
const createInitialFields = (fieldProperties) => {
  const { property, resourceURI, graph, insertEmptyField } = fieldProperties;

  if (!property) {
    throw new Error('Property is mandatory');
  }
  const statements = graph.find(resourceURI, property);
  if (!statements.length) {
    return insertEmptyField ? [new OrderedField(fieldProperties)] : [];
  }

  return statements.map(
    (statement) =>
      new OrderedField({
        blankNodeId: statement.getValue(),
        ...fieldProperties,
      })
  );
};

// Class for grouping edit fields using same type of rdf property. This makes it
// easier to for example validate the containing fields together.
export class OrderedFieldGroup extends FieldGroup {
  constructor(fieldProperties) {
    super(fieldProperties);
    this.fields = createInitialFields(fieldProperties);
    this._sortByOrder();
  }

  _updateOrderValues() {
    this.fields.forEach((field, index) => {
      field.setOrder(index + 1);
    });
  }

  _sortByOrder() {
    this.fields.sort((field1, field2) => {
      return field1.getOrder() - field2.getOrder();
    });
  }

  addField() {
    const newField = new OrderedField({
      ...this.fieldProperties,
      order: this.fields.length + 1,
    });
    this.fields = [...this.fields, newField];
    return newField;
  }

  moveField(firstField, sourceIndex, targetIndex) {
    const newFields = [...this.fields];
    const secondField = newFields[targetIndex];
    newFields[targetIndex] = firstField;
    newFields[sourceIndex] = secondField;
    this._setFields(newFields);
    this._updateOrderValues();
  }

  removeField(field) {
    field.remove();
    this.fields = this.fields.filter(
      (thisField) => thisField.getId() !== field.getId()
    );
    this._updateOrderValues();
    this.validate();
    return field;
  }
}
