import { useState } from 'react';
import PropTypes from 'prop-types';
import Button from '@mui/material/Button';
import { Entry } from '@entryscape/entrystore-js';
import { entrystore } from 'commons/store';
import ContentWrapper from 'commons/components/common/ContentWrapper';
import ListActionDialog from 'commons/components/common/dialogs/ListActionDialog';
import {
  useTranslation,
  nlsBundlesPropType,
} from 'commons/hooks/useTranslation';

const ReindexDialog = ({ entry, closeDialog, nlsBundles }) => {
  const [statusError, setStatusError] = useState(false);
  const translate = useTranslation(nlsBundles);

  const handleClose = () => {
    closeDialog();
    setStatusError(false);
  };

  const triggerReindex = async () => {
    const url = `${entrystore.getBaseURI()}management/solr`;

    try {
      const response = await fetch(url, {
        credentials: 'include',
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          command: 'reindex',
          context: entry.getURI(),
        }),
      });

      if (response.status !== 202) {
        setStatusError(true);
      } else {
        handleClose();
      }
    } catch (error) {
      setStatusError(true);
      console.error(error);
    }
  };

  const actions = (
    <Button autoFocus onClick={() => triggerReindex()}>
      {translate('projectReindex')}
    </Button>
  );

  return (
    <>
      <ListActionDialog
        id="reindex-entry"
        closeDialog={() => handleClose()}
        actions={actions}
        maxWidth="xs"
        alert={
          statusError
            ? { message: translate('reindexError') }
            : { message: null }
        }
      >
        <ContentWrapper xs={10}>
          <div>{translate('reindexConfirmationQuestionNLS')}</div>
        </ContentWrapper>
      </ListActionDialog>
    </>
  );
};

ReindexDialog.propTypes = {
  entry: PropTypes.instanceOf(Entry),
  closeDialog: PropTypes.func.isRequired,
  nlsBundles: nlsBundlesPropType,
};

export default ReindexDialog;
