import {
  AUTH_LOGIN_SUCCESS,
  AUTH_LOGOUT_SUCCESS,
  AUTH_CURRENT_USER_SUCCESS,
} from './actions';

/**
 * Not really needed
 * @param {*} state
 * @param {*} action
 * @param {*} exec
 */
const reducer = (state, { type, data }) => {
  switch (type) {
    case AUTH_LOGIN_SUCCESS:
    case AUTH_LOGOUT_SUCCESS: // gets the _guest entry
    case AUTH_CURRENT_USER_SUCCESS:
      return {
        ...state,
        userEntry: data.userEntry,
        userInfo: data.userInfo,
      };

    default:
      return state;
  }
};

export default reducer;
