import { COLOURS_BAR_CHART } from '../../colors';

/**
 *
 * @param {Object[]} datasets
 * @param {Object[]} colors
 * @returns {Object[]}
 */
export const mergeDatasetColors = (datasets, colors) => {
  const datasetCount = datasets?.length;
  if (!datasetCount) return datasets;

  const mergedColors = colors
    ? [...colors, ...COLOURS_BAR_CHART]
    : COLOURS_BAR_CHART;
  return datasets.map((dataset, index) => {
    const datasetIndex = index % datasetCount;
    return { ...dataset, ...mergedColors[datasetIndex] };
  });
};
