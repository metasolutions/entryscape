import PropTypes from 'prop-types';
import PublicOffIcon from '@mui/icons-material/PublicOff';
import PublicIcon from '@mui/icons-material/Public';
import InternalIcon from '@mui/icons-material/VpnLock';
import Tooltip from 'commons/components/common/Tooltip';
import { entryPropType, isInternallyPublished } from 'commons/util/entry';
import './index.scss';

const PublicStatusIcon = ({
  entry,
  internalTooltip,
  publicTooltip,
  privateTooltip,
}) => {
  const getProps = () => {
    if (entry.isPublic())
      return {
        icon: <PublicIcon className="escoPublicIcon__statusPublic" />,
        tooltip: publicTooltip,
      };
    if (isInternallyPublished(entry))
      return {
        icon: <InternalIcon className="escoPublicIcon__statusPublic" />,
        tooltip: internalTooltip,
      };
    return {
      icon: <PublicOffIcon color="secondary" />,
      tooltip: privateTooltip,
    };
  };

  const { tooltip, icon } = getProps();

  return (
    <Tooltip title={tooltip}>
      <div className="escoPublicIcon">{icon}</div>
    </Tooltip>
  );
};

PublicStatusIcon.propTypes = {
  entry: entryPropType,
  internalTooltip: PropTypes.string,
  publicTooltip: PropTypes.string,
  privateTooltip: PropTypes.string,
};

export default PublicStatusIcon;
