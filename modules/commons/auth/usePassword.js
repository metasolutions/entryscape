import config from 'config';
import { useReducer } from 'react';
import { isInputTextValid } from 'commons/util/util';
import { i18n } from 'esi18n';
import escoSigninNLS from 'commons/nls/escoSignin.nls';
import { useTranslation } from 'commons/hooks/useTranslation';

// Reducer actions
const CLEAR_PASSWORD = 'CLEAR_PASSWORD';
const CHANGE_PASSWORD = 'CHANGE_PASSWORD';
const CHANGE_CONFIRMATION = 'CHANGE_CONFIRMATION';
const CHANGE_CURRENT_PASSWORD = 'CHANGE_CURRENT_PASSWORD';
const TOGGLE_PASSWORD_VISIBILITY = 'TOGGLE_PASSWORD_VISIBILITY';
const TOGGLE_CONFIRMATION_VISIBILITY = 'TOGGLE_CONFIRMATION_VISIBILITY';
const TOGGLE_CURRENT_PASSWORD_VISIBILITY = 'TOGGLE_CURRENT_PASSWORD_VISIBILITY';

// NLS keys plus path to configurable error message
const CONFIG_ERROR_KEY = 'entrystore.password.message';
const MISMATCH_KEY = 'passwordMismatch';
const REQUIRED_KEY = 'passwordRequired';

const getValidationError = (passwd) => {
  const {
    custom: customCheck,
    uppercase: uppercaseRequired,
    lowercase: lowercaseRequired,
    symbol: symbolRequired,
    number: numberRequired,
    minLength,
  } = config.get('entrystore.password');

  if (!isInputTextValid(passwd)) return REQUIRED_KEY;

  const noUppercase = passwd === passwd.toLowerCase();
  const noLowercase = passwd === passwd.toUpperCase();
  const noSymbol = passwd.match(/[^a-zA-Z\d]+/) === null;
  const noNumber = passwd.match(/[0-9]/) === null;
  const failedCustomCheck = customCheck?.some(
    (regexp) => passwd.match(regexp) === null
  );

  if (
    passwd.length < minLength ||
    (uppercaseRequired && noUppercase) ||
    (lowercaseRequired && noLowercase) ||
    (symbolRequired && noSymbol) ||
    (numberRequired && noNumber) ||
    (customCheck && failedCustomCheck)
  ) {
    return CONFIG_ERROR_KEY;
  }

  return '';
};

const reducer = (state, action) => {
  switch (action.type) {
    case CLEAR_PASSWORD:
      return {
        ...state,
        password: '',
      };
    case TOGGLE_PASSWORD_VISIBILITY:
      return { ...state, passwordVisible: !state.passwordVisible };
    case TOGGLE_CONFIRMATION_VISIBILITY:
      return { ...state, confirmationVisible: !state.confirmationVisible };
    case TOGGLE_CURRENT_PASSWORD_VISIBILITY:
      return {
        ...state,
        currentPasswordVisible: !state.currentPasswordVisible,
      };
    case CHANGE_PASSWORD:
      return {
        ...state,
        password: action.value,
        passwordErrorKey: getValidationError(action.value),
        confirmationErrorKey:
          state.confirmation !== action.value ? MISMATCH_KEY : '',
      };
    case CHANGE_CONFIRMATION:
      return {
        ...state,
        confirmation: action.value,
        confirmationErrorKey:
          state.password !== action.value ? MISMATCH_KEY : '',
      };
    case CHANGE_CURRENT_PASSWORD:
      return {
        ...state,
        currentPassword: action.value,
        currentErrorKey: action.value ? '' : 'currentPasswordRequired',
      };
    default:
      throw new Error(`${action.type} is not a valid password action type`);
  }
};
const initialState = {
  password: '',
  passwordErrorKey: '',
  passwordVisible: false,
  confirmation: '',
  confirmationErrorKey: '',
  confirmationVisible: false,
  currentPassword: '',
  currentErrorKey: '',
  currentPasswordVisible: false,
};

const usePassword = () => {
  const locale = i18n.getLocale();
  const translate = useTranslation(escoSigninNLS);

  const [
    {
      password,
      passwordErrorKey,
      passwordVisible,
      confirmation,
      confirmationErrorKey,
      confirmationVisible,
      currentPassword,
      currentErrorKey,
      currentPasswordVisible,
    },
    dispatch,
  ] = useReducer(reducer, initialState);

  const clearPassword = () => dispatch({ type: CLEAR_PASSWORD });

  const handleChangePassword = (newPassword) =>
    dispatch({ type: CHANGE_PASSWORD, value: newPassword });

  const togglePasswordVisibility = () =>
    dispatch({ type: TOGGLE_PASSWORD_VISIBILITY });

  const handleChangePasswordConfirmation = (newConfirmation) =>
    dispatch({ type: CHANGE_CONFIRMATION, value: newConfirmation });

  const togglePasswordConfirmationVisibility = () =>
    dispatch({ type: TOGGLE_CONFIRMATION_VISIBILITY });

  const handleChangeCurrentPassword = (newValue) =>
    dispatch({ type: CHANGE_CURRENT_PASSWORD, value: newValue });

  const toggleCurrentVisibility = () =>
    dispatch({ type: TOGGLE_CURRENT_PASSWORD_VISIBILITY });

  const customTranslate = (key) => {
    if (!key) return '';
    if (key !== CONFIG_ERROR_KEY) return translate(key);
    const passwordConfigError = config.get(CONFIG_ERROR_KEY);
    return passwordConfigError[locale] || passwordConfigError.en;
  };

  return {
    password,
    passwordError: customTranslate(passwordErrorKey),
    isPasswordVisible: passwordVisible,
    clearPassword,
    handleChangePassword,
    togglePasswordVisibility,
    passwordConfirmation: confirmation,
    passwordConfirmationError: customTranslate(confirmationErrorKey),
    isPasswordConfirmationVisible: confirmationVisible,
    handleChangePasswordConfirmation,
    togglePasswordConfirmationVisibility,
    currentPassword,
    currentPasswordError: customTranslate(currentErrorKey),
    handleChangeCurrentPassword,
    isCurrentPasswordVisible: currentPasswordVisible,
    toggleCurrentVisibility,
  };
};

export default usePassword;
