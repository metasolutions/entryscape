/* eslint-disable */

// NOTE! order of imports is important
import 'regenerator-runtime/runtime';
import 'commons/webpack/publicPath';
import 'jquery';

import registry from 'commons/registry';
import initDefaults from './defaults';

initDefaults(); // init defaults
registry.setApp('suite');

export default registry;
