import { namespaces as ns } from '@entryscape/rdfjson';

export const CONFIG_NAMESPACE = 'escfg';
export const CONFIG_CONTEXT = 'config';
ns.add(CONFIG_NAMESPACE, `http://entryscape.com/terms/${CONFIG_CONTEXT}/`);

export const RDF_CLASS_ENTITY_TYPE = `${CONFIG_NAMESPACE}:EntityType`;
export const RDF_CLASS_CUSTOM_ENTITY_TYPE = `${CONFIG_NAMESPACE}:EntityType`;

export const ENTITY_TYPE_ID_PREFIX = '_et';
export const ENTITY_TYPE_BUILTIN = 'builtin';
export const ENTITY_TYPE_CUSTOM = 'custom';

export const PROJECT_TYPE_ID_PREFIX = '_pt';
export const RDF_CLASS_PROJECT_TYPE = `${CONFIG_NAMESPACE}:ProjectType`;
