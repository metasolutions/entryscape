import PropTypes from 'prop-types';
import { useEffect } from 'react';
import { entrystore } from 'commons/store';
import {
  Button,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
} from '@mui/material';
import DetailsTable from 'harvest/LinkCheckReport/tables/Details';
import { useTranslation } from 'commons/hooks/useTranslation';
import useAsync from 'commons/hooks/useAsync';
import esreLinkCheckReportNLS from 'harvest/nls/esreLinkCheckReport.nls';
import ReferenceEntriesList from 'commons/components/EntryListView/lists/ReferenceEntriesList';
import './index.scss';

const LinkCheckDetails = ({
  open,
  onClose,
  selectedEntryData,
  selectedLinkCheckResult,
}) => {
  const t = useTranslation(esreLinkCheckReportNLS);
  const { data: linkCheckEntry, runAsync } = useAsync(null);
  const entryURI = selectedEntryData[2].value;

  useEffect(() => {
    if (!entryURI) return;
    runAsync(entrystore.getEntry(entryURI));
  }, [entryURI, runAsync, selectedEntryData]);

  return (
    <Dialog
      aria-labelledby={t('linkCheckDialogHeader')}
      open={open}
      fullWidth
      maxWidth="lg"
      onClose={onClose}
    >
      <DialogTitle classes={{ root: 'esreLinkCheck__Header' }}>
        {t('linkCheckDialogHeader')}
      </DialogTitle>
      <DialogContent>
        <DetailsTable
          data={selectedLinkCheckResult}
          tableHeaderNLSKey="linkCheckResult"
        />
        <DetailsTable data={selectedEntryData} tableHeaderNLSKey="entryData" />
        {linkCheckEntry ? (
          <ReferenceEntriesList
            entry={linkCheckEntry}
            title={t('entryReferencesTitle')}
            titleClasses="linkCheckDetails__referencesTitle"
          />
        ) : null}
      </DialogContent>
      <DialogActions>
        <Button onClick={onClose} variant="text">
          {t('linkCheckDialogFooterButton')}
        </Button>
      </DialogActions>
    </Dialog>
  );
};

LinkCheckDetails.propTypes = {
  open: PropTypes.bool,
  onClose: PropTypes.func,
  selectedEntryData: PropTypes.arrayOf(
    PropTypes.shape({ label: PropTypes.string, value: PropTypes.string })
  ).isRequired,
  selectedLinkCheckResult: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
};

export default LinkCheckDetails;
