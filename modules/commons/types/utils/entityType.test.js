import { checkEntityTypeInModules } from './entityType';

const catalogDataset = { name: 'dataset', module: 'catalog' };
const catalogDistribution = { name: 'distribution', module: ['catalog'] };
const modelsPublisher = { name: 'modelsPublisher', module: 'models' };
const artist = { name: 'artist' };

describe('checkEntityTypeInModules', () => {
  test('entity type module is valid for module name lists', () => {
    expect(checkEntityTypeInModules(catalogDataset, ['models'])).toBe(false); // et module is 'catalog'
    expect(checkEntityTypeInModules(catalogDistribution, ['catalog'])).toBe(
      true
    ); // et module is array including 'catalog'
    expect(checkEntityTypeInModules(modelsPublisher, ['models'])).toBe(true); // et module is 'models'
    expect(checkEntityTypeInModules(artist, ['models'])).toBe(true); // et module is undefined
  });
});
