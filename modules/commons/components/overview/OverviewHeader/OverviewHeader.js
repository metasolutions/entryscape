import PropTypes from 'prop-types';
import { Typography, Grid, IconButton } from '@mui/material';
import Tooltip from 'commons/components/common/Tooltip';
import { truncateLabel } from 'commons/util/util';
import { withActionsProvider, useActions } from 'commons/components/ListView';
import './OverviewHeader.scss';

const OverviewHeaderAction = ({ action, translate }) => {
  const { openActionDialog } = useActions();
  const handleActionClick = () => openActionDialog(action);
  const { tooltipNlsKey, icon, highlightId } = action;

  return (
    <Tooltip title={tooltipNlsKey ? translate(tooltipNlsKey) : ''}>
      <span>
        <IconButton
          aria-label="info button"
          onClick={handleActionClick}
          data-highlight-id={highlightId}
        >
          {icon}
        </IconButton>
      </span>
    </Tooltip>
  );
};

OverviewHeaderAction.propTypes = {
  action: PropTypes.shape({
    icon: PropTypes.node,
    tooltipNlsKey: PropTypes.string,
    highlightId: PropTypes.string,
  }),
  translate: PropTypes.func,
};

const OverviewHeaderActionWithProvider =
  withActionsProvider(OverviewHeaderAction);

const OverviewHeader = ({
  label,
  sublabel,
  maxPrimaryLength = 40,
  icon,
  action,
  translate,
  ...typographyProps
}) => {
  const [labelText, tooltipText] = truncateLabel(label, maxPrimaryLength);
  return (
    <>
      <Grid item className="escoOverviewHeader">
        <Tooltip title={tooltipText}>
          <Grid container direction="row" className="escoOverviewHeader__label">
            <Grid item>
              <Typography
                className="escoOverviewHeader__heading"
                variant="h1"
                {...typographyProps}
              >
                {labelText}
              </Typography>
            </Grid>
          </Grid>
        </Tooltip>
        {icon}
        {action ? (
          <OverviewHeaderActionWithProvider
            action={action}
            translate={translate}
          />
        ) : null}
      </Grid>

      {sublabel ? (
        <Grid item className="escoOverviewHeader">
          <Typography className="escoOverviewHeader__subHeading" variant="h3">
            {sublabel}
          </Typography>
        </Grid>
      ) : null}
    </>
  );
};

OverviewHeader.propTypes = {
  label: PropTypes.string,
  sublabel: PropTypes.string,
  icon: PropTypes.node,
  translate: PropTypes.func,
  maxPrimaryLength: PropTypes.number,
  action: PropTypes.shape({ icon: PropTypes.node }),
};

export default OverviewHeader;
