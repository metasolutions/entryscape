import { useMemo } from 'react';
import { useUserState } from 'commons/hooks/useUser';
import { getContextSearchQuery } from 'commons/util/solr/context';
import { CONTEXT_TYPE_MODELS } from 'commons/util/context';
import {
  EntryListView,
  useSolrQuery,
  TOGGLE_CONTEXT_COLUMN,
  TITLE_COLUMN,
  MODIFIED_COLUMN,
  ACTIONS_GROUP_COLUMN,
} from 'commons/components/EntryListView';
import {
  LIST_ACTION_INFO,
  LIST_ACTION_EDIT,
} from 'commons/components/EntryListView/actions';

import {
  withListModelProviderAndLocation,
  listPropsPropType,
} from 'commons/components/ListView';
import { getPathFromViewName } from 'commons/util/site';
import ListEditEntryDialog from 'commons/components/EntryListView/dialogs/ListEditEntryDialog';
import { nlsBundles, listActions } from './actions';

const ModelsView = ({ listProps }) => {
  const { userEntry, userInfo } = useUserState();

  const queryParams = useMemo(() => {
    const queryTypeParams = { contextType: CONTEXT_TYPE_MODELS };
    return getContextSearchQuery(queryTypeParams, {
      userEntry,
      userInfo,
    });
  }, [userEntry, userInfo]);

  const queryResults = useSolrQuery(queryParams);

  return (
    <EntryListView
      {...queryResults}
      {...listProps}
      nlsBundles={nlsBundles}
      listActions={listActions}
      listActionsProps={{ nlsBundles }}
      getListItemProps={({ entry }) => ({
        to: getPathFromViewName('models__overview', {
          contextId: entry.getId(),
        }),
      })}
      columns={[
        TOGGLE_CONTEXT_COLUMN,
        { ...TITLE_COLUMN, xs: 7 },
        MODIFIED_COLUMN,
        {
          ...ACTIONS_GROUP_COLUMN,
          xs: 1,
          actions: [
            LIST_ACTION_INFO,
            {
              ...LIST_ACTION_EDIT,
              nlsBundles,
              formTemplateId: 'esc:Context',
              Dialog: ListEditEntryDialog,
            },
          ],
        },
      ]}
    />
  );
};

ModelsView.propTypes = {
  listProps: listPropsPropType,
};

export default withListModelProviderAndLocation(ModelsView);
