import { entrystore } from 'commons/store';
import { useCallback, useEffect, useReducer, useState } from 'react';
import { useTranslation } from 'commons/hooks/useTranslation';
import IntervalRunner from 'commons/util/IntervalRunner';
import escoErrors from 'commons/nls/escoErrors.nls';
import {
  NO_PROBLEM,
  INPROGRESS,
  LOST_CONNECTION,
  extractProblem,
  checkIgnoreTrue,
  checkIgnore,
} from '../utils/async';

import {
  check as connectionCheck,
  stop as connectionCheckStop,
} from '../utils/checkConnection';

// Case Generic problem: Something went wrong... send error report / continue anyway / ok,
//  proceed anyway.
// Case signed out: Sign in again - > close dialog and open sign in dialog
// Case lost connection: No contact with server, retry

const PROGRESS_DELAY = 2000;

const checkProgress = (promiseItems) => {
  const currentTime = new Date().getTime();
  const item = promiseItems.find(
    (promiseItem) => currentTime - promiseItem.time > PROGRESS_DELAY
  );
  return Boolean(item);
};

const getPromiseItem = (promise, promiseItems) =>
  promiseItems.find((item) => item.promise === promise);

const check = (promise, callType, dispatch) => {
  if (checkIgnoreTrue(callType)) {
    return;
  }

  const promiseItem = {
    promise,
    callType,
    time: new Date().getTime(),
  };

  dispatch({ type: 'add', promiseItem });

  promise.then(
    (value) =>
      dispatch({
        type: 'resolved',
        promise,
        value,
      }),
    (value) =>
      dispatch({
        type: 'rejected',
        promise,
        value,
      })
  );
};

const getErrorMessage = (promiseItem) => {
  if (promiseItem?.err?.response?.status === 412) {
    // In case of conflict problems the status text is enough.
    return null;
  }
  const errorMessage =
    typeof promiseItem.err?.message === 'string'
      ? promiseItem.err.message
      : promiseItem.err;
  return errorMessage;
};

const initialErrorState = {
  promiseItems: [],
  status: NO_PROBLEM,
};

const errorReducer = (state, action) => {
  switch (action.type) {
    case 'add': {
      const { promiseItem } = action;

      return {
        ...state,
        promiseItems: [...state.promiseItems, promiseItem],
      };
    }
    case 'resolved': {
      const promiseItem = getPromiseItem(action.promise, state.promiseItems);
      if (!promiseItem) {
        return state;
      }
      const promiseItems = state.promiseItems.filter(
        (item) => item.promise !== action.promise
      );

      // Only needed to remove from ignoreNext when callType and status is a match
      checkIgnore(promiseItem);
      return {
        ...state,
        promiseItems,
      };
    }
    case 'rejected': {
      const promiseItem = getPromiseItem(action.promise, state.promiseItems);
      if (!promiseItem) {
        return state;
      }
      promiseItem.err = action.value;
      const newPromiseItems = state.promiseItems.filter(
        (item) => item.promise !== action.promise
      );

      if (checkIgnore(promiseItem)) {
        return {
          ...state,
          promiseItems: newPromiseItems,
        };
      }

      const status = extractProblem(promiseItem.err);
      if (status <= state.status) return state;
      return {
        ...state,
        promiseItems: newPromiseItems,
        status,
        errorMessage: getErrorMessage(promiseItem),
        errorCallType: promiseItem.callType,
      };
    }
    // one or more promises has exceeded progress delay limit
    case 'pending': {
      if (state.status > INPROGRESS) return state;
      if (state.status === INPROGRESS && action.isPending) return state;

      return {
        ...state,
        status: action.isPending ? INPROGRESS : NO_PROBLEM,
      };
    }
    case 'clear': {
      return initialErrorState;
    }
    default:
      throw new Error('Unknown action for errorReducer');
  }
};

const ERROR_MESSAGE_KEYS = ['conflictProblem'];

const useError = (errorMessage, errorCallType, status) => {
  const t = useTranslation(escoErrors);
  if (!errorMessage) return null;
  const translatedErrorMessage = ERROR_MESSAGE_KEYS.includes(errorMessage)
    ? t(errorMessage)
    : errorMessage;
  const error = new Error(translatedErrorMessage);
  error.name = errorCallType;
  error.status = status;
  return error;
};

const useEntrystoreError = () => {
  const [errorState, dispatch] = useReducer(errorReducer, initialErrorState);
  const [countdown, setCountdown] = useState(0);
  const [intervalRunner] = useState(
    new IntervalRunner([], PROGRESS_DELAY, (items) => {
      dispatch({ type: 'pending', isPending: checkProgress(items) });
    })
  );
  const { status, errorMessage, errorCallType, promiseItems } = errorState;
  const error = useError(errorMessage, errorCallType, status);

  useEffect(() => {
    const entrystoreListener = (promise, callType) => {
      check(promise, callType, dispatch);
    };
    entrystore.addAsyncListener(entrystoreListener);

    return () => {
      entrystore.removeAsyncListener(entrystoreListener);
    };
  }, [dispatch]);

  useEffect(() => {
    if (status === LOST_CONNECTION) {
      connectionCheck(setCountdown, () => {});
    }
    return connectionCheckStop;
  }, [status, dispatch]);

  /**
   * Checks if there's a current promise exceeding progress delay limit.
   * A check is executed with progress delay as interval.
   */
  useEffect(() => {
    intervalRunner.setItems(promiseItems);

    return () => {
      intervalRunner.stop();
    };
  }, [intervalRunner, promiseItems]);

  const clear = useCallback(() => {
    dispatch({ type: 'clear' });
    connectionCheckStop();
  }, []);

  return {
    countdown,
    error,
    status,
    clear,
  };
};

export default useEntrystoreError;
