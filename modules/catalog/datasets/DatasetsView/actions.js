import escaDatasetNLS from 'catalog/nls/escaDataset.nls';
import escaDatasetTemplateNLS from 'catalog/nls/escaDatasetTemplate.nls';
import escoListNLS from 'commons/nls/escoList.nls';
import { withListRefresh } from 'commons/components/ListView';
import CreateDatasetDialog from '../dialogs/CreateDatasetDialog';

export const nlsBundles = [escaDatasetNLS, escoListNLS, escaDatasetTemplateNLS];

export const listActions = [
  {
    id: 'create',
    Dialog: withListRefresh(CreateDatasetDialog, 'onCreate'),
    labelNlsKey: 'createButtonLabel',
    tooltipNlsKey: 'actionTooltip',
  },
];
