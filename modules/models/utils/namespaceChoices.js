import { namespaces as ns } from '@entryscape/rdfjson';
import { Entry } from '@entryscape/entrystore-js';
import { builtinNamespacesAbbreviations } from 'models/namespaces/utils/builtinNamespaces';
import {
  getNamespaceAbbreviation,
  getDefaultEntry,
  getNamespaceUri,
} from 'models/namespaces/utils/namespaceMetadata';
import { isUri } from 'commons/util/util';

/**
 * Looks up registered namespaces and converts to choices
 *
 * @param {Entry[]} entries
 * @returns {object[]}
 */

export const getNamespaceChoices = (entries) => {
  const builtinNamespaceChoices = [
    ...builtinNamespacesAbbreviations
      .sort()
      .map((value) => ({ value, label: value })),
  ];

  if (!entries) {
    return [{ value: '' }, ...builtinNamespaceChoices];
  }

  const filteredBuiltinNamespaceChoices = builtinNamespaceChoices.filter(
    ({ value }) =>
      !entries.map((entry) => getNamespaceAbbreviation(entry)).includes(value)
  );

  const namespaceEntriesChoices = entries.map((entry) => ({
    value: getNamespaceAbbreviation(entry),
    label: getNamespaceAbbreviation(entry),
  }));

  const defaultEntry = getDefaultEntry(entries);
  if (!defaultEntry) {
    return [
      { value: '' },
      ...namespaceEntriesChoices,
      null,
      ...filteredBuiltinNamespaceChoices,
    ];
  }

  const filteredNamespaceChoices = namespaceEntriesChoices.filter(
    (entry) => entry.value !== getNamespaceAbbreviation(defaultEntry)
  );

  return [
    { value: '' },
    {
      value: getNamespaceAbbreviation(defaultEntry),
      label: `${getNamespaceAbbreviation(defaultEntry)} (default)`,
    },
    ...filteredNamespaceChoices,
    null,
    ...filteredBuiltinNamespaceChoices,
  ];
};

/**
 * @param {string} uri
 * @param {Entry[]} entries
 * @returns {Entry|undefined}
 */
const getCustomNamespaceFromValue = (uri, entries) => {
  return entries?.find((entry) => uri.startsWith(getNamespaceUri(entry)));
};

/**
 *
 * @param {object} field
 * @param {Entry[]} entries
 * @returns {string[]}
 */
export const getNamespace = (field, entries) => {
  const defaultEntry = getDefaultEntry(entries);
  const value = field.getValue();

  if (!value || !isUri(value))
    return [getNamespaceAbbreviation(defaultEntry) || '', ''];

  const customNamespaceEntry = getCustomNamespaceFromValue(value, entries);
  if (!customNamespaceEntry) {
    const shortened = ns.shortenKnown(value);
    return shortened.split(':');
  }

  const namespaceAbbreviation = getNamespaceAbbreviation(customNamespaceEntry);
  const localename = value.substring(
    getNamespaceUri(customNamespaceEntry).length
  );
  return [namespaceAbbreviation, localename];
};

/**
 *
 * @param {string} namespaceKey
 * @param {string} namespaceValue
 * @param {Entry} namespaceEntryFromKey
 * @returns {string}
 */
export const getFullURI = (
  namespaceKey,
  namespaceValue,
  namespaceEntryFromKey
) => {
  if (!namespaceKey || !namespaceValue) return '';

  if (!namespaceEntryFromKey) {
    return ns.expand(`${namespaceKey}:${namespaceValue}`);
  }
  return `${getNamespaceUri(namespaceEntryFromKey)}${namespaceValue}`;
};

/**
 * @param {object} field
 * @param {Entry[]} namespaceEntries
 * @returns {Entry|undefined}
 */
export const getNamespaceEntry = (field, namespaceEntries) => {
  const defaultEntry = getDefaultEntry(namespaceEntries);
  const namespaceEntryURI = field.getValue();

  if (!namespaceEntryURI) return defaultEntry;
  return namespaceEntries.find((namespaceEntry) => {
    return namespaceEntry.getURI() === namespaceEntryURI;
  });
};

/**
 * @param {Entry[]} namespaceEntries
 * @param {boolean} entryAsValue
 * @returns {object[]}
 */
export const getNamespaceEntryChoices = (
  namespaceEntries,
  entryAsValue = true
) => {
  const defaultEntry = getDefaultEntry(namespaceEntries);
  const entries = defaultEntry
    ? [
        defaultEntry,
        ...namespaceEntries.filter(
          (namespaceEntry) => namespaceEntry !== defaultEntry
        ),
      ]
    : namespaceEntries;
  return entries.map((entry) => {
    const namespaceAbbreviation = getNamespaceAbbreviation(entry);
    return {
      value: entryAsValue ? entry : getNamespaceUri(entry),
      label:
        entry === defaultEntry
          ? `${namespaceAbbreviation} (default)`
          : namespaceAbbreviation,
      id: entry.getURI(),
    };
  });
};
