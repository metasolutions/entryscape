import { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { Grid } from '@mui/material';
import { entrystore } from 'commons/store';
import useAsync from 'commons/hooks/useAsync';
import { getLabel } from 'commons/util/rdfUtils';
import {
  useTranslation,
  nlsBundlesPropType,
} from 'commons/hooks/useTranslation';
import { getOrIgnoreEntry, graphPropType } from 'commons/util/entry';
import escoFormsNls from 'commons/nls/escoForms.nls';
import { useFieldsEdit } from '../hooks/useFieldsEdit';
import { Fields } from '../Fields';
import { Field } from '../Field';
import { TextField } from '../TextField';
import { SearchButton } from '../FieldControl';
import { SelectEntryDialog } from '../../dialogs/SelectEntryDialog';

export const SelectEntryEditor = ({
  property,
  size,
  graph,
  resourceURI,
  dispatch,
  nodetype,
  Dialog = SelectEntryDialog,
  dialogProps = {},
  selectEntryTooltipNlsKey = 'selectEntryTooltip',
  nlsBundles,
  resetOnFormType,
  renderInlineContent,
  isResourceURI = true,
  error,
  helperText,
  onChange = () => {},
  onRemove = () => {},
  validate,
  fieldsCount = 1,
  renderControls,
  ...fieldsProps
}) => {
  const translate = useTranslation([...nlsBundles, escoFormsNls]);
  const [showEntrySelectDialog, setShowEntrySelectDialog] = useState(false);
  const [selectedEntry, setSelectedEntry] = useState();
  const { runAsync, status } = useAsync({ data: null });
  const {
    fields: [field],
    labelId,
    updateValue,
    fieldGroup,
    removeField,
  } = useFieldsEdit({
    name: fieldsProps.name,
    property,
    mandatory: fieldsProps.mandatory,
    dispatch,
    nodetype,
    graph,
    resourceURI,
    resetOnFormType,
    validate,
  });

  useEffect(() => {
    const getInitialEntry = () => {
      const uri = field.getValue();
      if (!uri) return Promise.resolve(null);
      const entryURI = isResourceURI ? entrystore.getEntryURIFromURI(uri) : uri;
      return getOrIgnoreEntry(entryURI);
    };

    runAsync(getInitialEntry().then((entry) => setSelectedEntry(entry)));
  }, [runAsync, field, isResourceURI]);

  const onSelect = (entry) => {
    updateValue(field, isResourceURI ? entry.getResourceURI() : entry.getURI());
    setSelectedEntry(entry);
    setShowEntrySelectDialog(false);
    onChange(entry, fieldGroup);
  };

  const onClear = () => {
    updateValue(field, '');
    setSelectedEntry(null);
    onChange(null, fieldGroup);
  };

  const handleRemove = () => {
    onRemove();
    removeField(field);
  };

  if (status !== 'resolved') return null;

  return (
    <Fields labelId={labelId} addField={() => {}} max={1} {...fieldsProps}>
      {renderInlineContent ? renderInlineContent(fieldGroup) : null}
      <Field size={fieldsCount} onClear={onClear} onRemove={handleRemove}>
        <TextField
          disabled
          required
          xs={renderControls ? 7 : 8}
          inputProps={{ 'aria-labelledby': labelId }}
          size={size}
          onChange={() => {}}
          error={error || field.hasError()}
          helperText={
            helperText || (field.hasError() ? translate(field.getError()) : '')
          }
          value={selectedEntry ? getLabel(selectedEntry) : ''}
        />
        <Grid item xs={renderControls ? 1 : 4}>
          <SearchButton
            tooltip={translate(selectEntryTooltipNlsKey)}
            onClick={() => setShowEntrySelectDialog(true)}
          />
        </Grid>
        {renderControls ? renderControls() : null}
      </Field>
      {showEntrySelectDialog ? (
        <Dialog
          closeDialog={() => {
            setShowEntrySelectDialog(false);
          }}
          selectedEntry={selectedEntry}
          onSelect={onSelect}
          nlsBundles={nlsBundles}
          {...dialogProps}
        />
      ) : null}
    </Fields>
  );
};

SelectEntryEditor.propTypes = {
  property: PropTypes.string,
  size: PropTypes.string,
  graph: graphPropType,
  resourceURI: PropTypes.string,
  dispatch: PropTypes.func,
  nodetype: PropTypes.string,
  mandatory: PropTypes.bool,
  Dialog: PropTypes.node,
  dialogProps: PropTypes.shape({}),
  selectEntryTooltipNlsKey: PropTypes.string,
  resetOnFormType: PropTypes.bool,
  renderInlineContent: PropTypes.func,
  isResourceURI: PropTypes.bool,
  onChange: PropTypes.func,
  onRemove: PropTypes.func,
  nlsBundles: nlsBundlesPropType,
  helperText: PropTypes.string,
  error: PropTypes.bool,
  validate: PropTypes.func,
  renderControls: PropTypes.func,
  fieldsCount: PropTypes.number,
};
