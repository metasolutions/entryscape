import React, { useState } from 'react';
import { utils } from '@entryscape/rdforms';
import PropTypes from 'prop-types';
import FormControlLabel from '@mui/material/FormControlLabel';
import PriorityHighIcon from '@mui/icons-material/PriorityHigh';
import { Button, Checkbox, Typography } from '@mui/material';
import { Entry } from '@entryscape/entrystore-js';
import Tooltip from 'commons/components/common/Tooltip';
import ListActionDialog from 'commons/components/common/dialogs/ListActionDialog';
import ContentWrapper from 'commons/components/common/ContentWrapper';
import { useTranslation } from 'commons/hooks/useTranslation';
import LinearProgress from 'commons/components/progress/LinearProgress';
import escoProgress from 'commons/nls/escoProgress.nls';
import { useConfirmCloseAction } from 'commons/components/EntryListView/dialogs/create/handlers';
import {
  getChecklistProgress,
  getChecklistSteps,
  showChecklistProgress,
  getCheckedTasks,
  addOrRemoveEntryTasks,
} from '../../util';
import './SuggestionChecklistDialog.scss';

const SuggestionCheckListDialog = ({ entry, closeDialog, onSave }) => {
  const [checkList, setCheckList] = useState(getChecklistProgress(entry));
  const [hasChange, setHasChange] = useState(false);
  const checklistSteps = getChecklistSteps();
  const [metadataTasks, setMetadataTasks] = useState(
    getCheckedTasks(entry, checklistSteps)
  );
  const translate = useTranslation(escoProgress);
  const confirmClose = useConfirmCloseAction(closeDialog);

  const handleChangeCheckBox = (event) => {
    const { name } = event.target;
    const updatedTasks = {};
    updatedTasks[name] = event.target.checked;
    Object.keys(metadataTasks).forEach((metadataTask) => {
      if (name !== metadataTask) {
        updatedTasks[metadataTask] = metadataTasks[metadataTask];
      }
    });

    setMetadataTasks(updatedTasks);
    setCheckList(showChecklistProgress(checklistSteps, updatedTasks));
    if (!hasChange) setHasChange(true);
  };

  const saveChecklist = () => {
    addOrRemoveEntryTasks(entry, metadataTasks);
    try {
      entry.getEntryInfo().commit();
      onSave();
    } catch (err) {
      console.log(err);
    }
    closeDialog();
  };

  const actions = (
    <Button autoFocus onClick={saveChecklist} disabled={!hasChange}>
      {translate('progressFooterButton')}
    </Button>
  );

  return (
    <ListActionDialog
      id="suggestion-checklist"
      title={translate('progressHeader')}
      actions={actions}
      closeDialog={() => confirmClose(hasChange)}
      maxWidth="md"
    >
      <Typography>{translate('checkListDescription')}</Typography>
      <ContentWrapper>
        <LinearProgress
          progress={checkList}
          className="escaSuggestionChecklistDialog__linearProgress"
        />
        {checklistSteps.map((checklistStep) => (
          <div key={checklistStep.name}>
            <div className="escaSuggestionChecklistDialog__taskHeading">
              <div className="escaSuggestionChecklistDialog__checklistItem">
                <FormControlLabel
                  control={
                    <Checkbox
                      checked={Boolean(metadataTasks[checklistStep.name])}
                      name={checklistStep.name}
                      color="primary"
                      onChange={handleChangeCheckBox}
                    />
                  }
                  label={utils.getLocalizedValue(checklistStep.label).value}
                />
              </div>
              <div>
                {checklistStep.mandatory && (
                  <Tooltip title={translate('mandatoryChecklistTitle')}>
                    <PriorityHighIcon
                      classes={{
                        root: 'escaSuggestionChecklistDialog__priorityIcon',
                      }}
                    />
                  </Tooltip>
                )}
              </div>
            </div>
            <div className="escaSuggestionChecklistDialog__taskDescription">
              {utils.getLocalizedValue(checklistStep.description).value}
            </div>
          </div>
        ))}
      </ContentWrapper>
    </ListActionDialog>
  );
};

SuggestionCheckListDialog.propTypes = {
  closeDialog: PropTypes.func,
  onSave: PropTypes.func,
  entry: PropTypes.instanceOf(Entry),
};

export default SuggestionCheckListDialog;
