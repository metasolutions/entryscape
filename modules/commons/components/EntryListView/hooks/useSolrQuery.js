import { useCallback } from 'react';
import { simpleFetchEntries } from 'commons/util/solr/entry';
import { applyQueryParams as defaultApplyQueryParams } from 'commons/util/solr';
import { useListModel } from 'commons/components/ListView';
import { useFetchEntries } from './useFetchEntries';

/**
 * useSolrQuery is meant to be used together with a list view. Changes in ui
 * state, read from the list model provider, triggers creating and running new
 * solr queries. Note that the ui state in the list model is independent of the
 * result from the solr query in this hook. The hook first creates the query
 * function and then runs it.
 *
 * @param {{createQuery: Function, applyQueryParams: Function, runQuery:
 * Function, limit: number, minimumSearchLength: number, wait: boolean}} queryOption
 * @returns {object}
 */
export const useSolrQuery = ({
  createQuery,
  applyQueryParams = defaultApplyQueryParams,
  applyFilters,
  runQuery = simpleFetchEntries,
  minimumSearchLength = 3,
  wait,
  callback,
}) => {
  // get the ui list state for the query
  const [{ page, search, sort, refreshCount, limit }] = useListModel();
  /**
   * The search string that will be used in the solr query. If search is less
   * than minimumSearchLength, an empty search string should be applied.
   */
  const searchQueryString =
    search.trim().length < minimumSearchLength ? '' : search;

  /**
   * Creating the fetchEntries query. The function is recreated every time any
   * query param has changed, which will also trigger it to be run by the
   * useFetchEntries hook.
   */
  const fetchEntries = useCallback(() => {
    const queryParams = { page, search: searchQueryString, sort, limit };
    const query = createQuery();
    applyQueryParams(query, queryParams);

    if (applyFilters) applyFilters(query);

    return runQuery(query, page)
      .then(callback)
      .then((result) => {
        return {
          ...result,
          search: searchQueryString,
        };
      });
  }, [
    page,
    searchQueryString,
    sort,
    limit,
    createQuery,
    applyQueryParams,
    applyFilters,
    runQuery,
    callback,
  ]);

  // Running the fetchEntries query
  const queryResult = useFetchEntries({ fetchEntries, refreshCount, wait });

  return queryResult;
};
