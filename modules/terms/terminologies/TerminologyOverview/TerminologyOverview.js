import React, { useEffect } from 'react';
import Overview from 'commons/components/overview/Overview';
import { useESContext } from 'commons/hooks/useESContext';
import { useTranslation } from 'commons/hooks/useTranslation';
import esteOverviewNLS from 'terms/nls/esteOverview.nls';
import escoOverviewNLS from 'commons/nls/escoOverview.nls';
import escoListNLS from 'commons/nls/escoList.nls';
import esteSchemeNLS from 'terms/nls/esteScheme.nls';
import esteTerminologyNLS from 'terms/nls/esteTerminology.nls';
import esteTerminologyExportNLS from 'terms/nls/esteTerminologyexport.nls';
import { useParams } from 'react-router-dom';
import { useUserState } from 'commons/hooks/useUser';
import useAsync from 'commons/hooks/useAsync';
import {
  useOverviewModel,
  withOverviewModelProvider,
} from 'commons/components/overview';
import getOverviewData from 'commons/components/overview/utils/contextOverview';
import {
  DESCRIPTION_UPDATED,
  DESCRIPTION_CREATED,
  DESCRIPTION_PROJECT_TYPE,
} from 'commons/components/overview/actions';
import Loader from 'commons/components/Loader/Loader';
import useHandleError from 'commons/errors/hooks/useErrorHandler';
import { getConceptSchemeEntryInContext } from 'commons/tree/skos/util';
import { sidebarActions } from './actions';

const nlsBundles = [
  escoOverviewNLS,
  esteOverviewNLS,
  esteSchemeNLS,
  esteTerminologyNLS,
  esteTerminologyExportNLS,
  escoListNLS,
];

/**
 * TerminologyOverview
 *
 * @returns {Element}
 */
function TerminologyOverview() {
  const { userInfo, userEntry } = useUserState();
  const { data: state, runAsync, error, isLoading } = useAsync({ data: {} });

  useHandleError(error);

  const translate = useTranslation([escoOverviewNLS, esteOverviewNLS]);
  const { context } = useESContext();
  const navParams = useParams();

  const [{ refreshCount }] = useOverviewModel();

  useEffect(() => {
    const getConceptSchemeEntry = async () => {
      const conceptSchemeEntry = await getConceptSchemeEntryInContext(context);
      return conceptSchemeEntry;
    };
    runAsync(
      getConceptSchemeEntry().then((conceptSchemeEntry) => {
        return getOverviewData(
          navParams,
          conceptSchemeEntry,
          context,
          translate,
          userInfo
        );
      })
    );
  }, [context, runAsync, translate, userInfo, refreshCount, navParams]);

  const descriptionItems = [
    DESCRIPTION_CREATED,
    DESCRIPTION_UPDATED,
    {
      ...DESCRIPTION_PROJECT_TYPE,
      getValues: () =>
        state?.projectType
          ? [state?.projectType?.source.name]
          : [translate('defaultProjectType')],
    },
  ];

  if (isLoading) return <Loader />;

  return (
    <div className="escoTermsOverview">
      <Overview
        backLabel={translate('backTitle')}
        entry={state.entry}
        nlsBundles={nlsBundles}
        descriptionItems={descriptionItems}
        rowActions={state.rowActions}
        sidebarActions={sidebarActions}
        sidebarProps={{
          userEntry,
          userInfo,
          context,
        }}
      />
    </div>
  );
}

export default withOverviewModelProvider(TerminologyOverview);
