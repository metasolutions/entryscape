import Lookup from 'commons/types/Lookup';
import config from 'config';
import { Context, Entry } from '@entryscape/entrystore-js';
import { namespaces as ns } from '@entryscape/rdfjson';
import { semanticRelationsProperties } from 'commons/tree/skos/util';
import { entrystore } from 'commons/store';
import { getLabel } from 'commons/util/rdfUtils';
import { contextTypes } from 'commons/util/context';
import { ANY_FILTER_ITEM } from 'commons/components/filters/utils/filterDefinitions';
import { getModuleNameOfCurrentView } from 'commons/util/site';

export const restrictedToContext = (property, entitytype) => {
  const chooserScope = config.get('entrychooser');
  if (chooserScope[property]) {
    // restriction if chooserScope is set to 'context' explicitly
    return chooserScope[property] === 'context';
  }
  // restrict unless allContexts is specified on the entitytype
  return !(entitytype && entitytype.allContexts === true);
};

export const checkUseRemoteStore = (entityType, property) => {
  const remoteStoreConfig = entityType.getStoreConfig();
  if (!remoteStoreConfig) return false;

  // Hide remote entrystore option in case this a relation property
  return !semanticRelationsProperties.some(
    (relationProperty) => ns.expand(relationProperty) === property
  );
};

// TODO: handle this logic in EntityType class in entitytype-lookup lib
export const getPrimaryStore = (entityType) => {
  const remoteStoreConfig = entityType.getStoreConfig();
  const { includeLocal } = remoteStoreConfig;
  return includeLocal === false ? entityType.getStore() : entrystore;
};

/**
 *
 * @param {object} item
 * @param {Context} context
 * @returns {object}
 */
export const getQueryConfiguration = (item, context) => {
  const queryConfiguration = {
    searchLimit: null,
    sortFields: null,
    searchProps: null,
    explicitContext: null,
    contextRestriction: true,
    invertContextRestriction: false,
  };

  const property = item.getProperty();
  const chooserScope = config.get('entrychooser');
  const projectType = context
    ? Lookup.getProjectTypeInUse(context, true)
    : null;
  const entityType =
    Lookup.getEntityTypeByConstraints(item.getConstraints(), projectType) || {};
  const { source: entitytypeConfig } = entityType;

  switch (chooserScope[property]) {
    case 'context':
      queryConfiguration.contextRestriction = true;
      break;
    case 'notContext':
      queryConfiguration.contextRestriction = true;
      queryConfiguration.invertContextRestriction = true;
      break;
    case 'repository':
      queryConfiguration.contextRestriction = false;
      break;
    default:
      if (entitytypeConfig && entitytypeConfig.allContexts === true) {
        // when no chooserScope is set, check if allContext is set to true to relax restriction
        queryConfiguration.contextRestriction = false;
      }
  }

  if (!entitytypeConfig) {
    return queryConfiguration;
  }

  const hasRemoteStore = checkUseRemoteStore(entityType, property);
  if (hasRemoteStore) {
    queryConfiguration.remoteStoreUtil = entityType.getStoreUtil();
    queryConfiguration.remoteStore = entityType.getStore();
  }
  const primaryStore = hasRemoteStore
    ? getPrimaryStore(entityType)
    : entrystore;

  return {
    ...queryConfiguration,
    primaryStore,
    searchLimit: entitytypeConfig.searchLimit,
    sortFields: entitytypeConfig.sortFields,
    searchProps: entitytypeConfig.searchProps,
    explicitContext: entitytypeConfig.context,
  };
};

/**
 *
 * @param {string} currentContextId
 * @param {Function} translate
 * @returns {Entry[]}
 */
export const getContextFilterItems = async (currentContextId, translate) => {
  const currentModuleName = getModuleNameOfCurrentView();
  const rdfTypes = contextTypes
    .filter((contextType) => contextType.module === currentModuleName)
    .map((contextType) => contextType.rdfType);

  const contextEntries = await entrystore
    .newSolrQuery()
    .rdfType(rdfTypes)
    .limit(100)
    .list()
    .getAllEntries();

  const currentItem = {
    label: translate('currentProjectLabel'),
    value: currentContextId,
  };

  const contextItems = contextEntries
    .filter((contextEntry) => contextEntry.getId() !== currentContextId)
    .map((contextEntry) => ({
      value: contextEntry.getId(),
      label:
        getLabel(contextEntry) ||
        translate('unnamedProject', contextEntry.getId()),
    }));

  return [ANY_FILTER_ITEM, currentItem, ...contextItems];
};
