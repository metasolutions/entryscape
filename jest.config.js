module.exports = {
  testEnvironment: 'jsdom',
  globals: {
    __entryscape_config: {
      entryscape: { localBuild: '' },
    },
    DEVELOPMENT: true,
    DEVELOPMENT_HOST: 'https://localhost:8080',
    ENTRYSCAPE_STATIC: 'https://localhost:8080', // set to prevent undefined error
  },
  setupFiles: ['./test/setupTests.js'],
  moduleNameMapper: {
    // static assets
    '\\.(css|scss)$': '<rootDir>/test/__mocks__/styleMock.js',
    // webpack aliases
    '^config$': '<rootDir>/app/suite/config/config.js',
    // mapping to test utils directory
    '^@test-utils(.*)$': '<rootDir>/test/utils$1',
    '^@entryscape/entrystore-js':
      '<rootDir>/node_modules/@entryscape/entrystore-js/src/index.js',
  },
  testPathIgnorePatterns: ['/node_modules/', '/__fixtures__/'],
  modulePaths: [
    '<rootDir>/node_modules/',
    '<rootDir>/modules/',
    '<rootDir>/app/',
  ],
  transform: {
    '\\.[jt]sx?$': 'babel-jest', // default
    '\\.nls$': '<rootDir>/test/transformers/nlsTransformer.js',
  },
  // libs that should be transformed
  transformIgnorePatterns: [
    // eslint-disable-next-line max-len
    'node_modules/(?!(lodash-es|ol|@entryscape/rdforms|@entryscape/entrystore-js|entitytype-lookup))',
  ],
  resolver: '<rootDir>/test/resolver.js',
  resetMocks: true,
};
