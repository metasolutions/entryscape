import PropTypes from 'prop-types';
import React, { useState } from 'react';
import { Entry } from '@entryscape/entrystore-js';
import { getGroupWithHomeContext } from 'commons/util/store';
import Switch from 'commons/components/common/Switch';
import Tooltip from 'commons/components/common/Tooltip';
import useAsyncCallback from 'commons/hooks/useAsyncCallback';
import { ListItemAction } from 'commons/components/ListView';
import { useSnackbar } from 'commons/hooks/useSnackbar';
import { useTranslation } from 'commons/hooks/useTranslation';
import escoListNLS from 'commons/nls/escoList.nls';
import { isDraftEntry } from 'commons/util/entry';
import { ListDraftStatusIcon } from '../ListDraftStatusIcon';

export const ListPublishEntryToggle = ({ xs, ...rest }) => (
  <ListItemAction xs={xs}>
    <PublishEntryToggle {...rest} />
  </ListItemAction>
);

ListPublishEntryToggle.propTypes = { xs: PropTypes.number };

export default ListPublishEntryToggle;

const PublishEntryToggle = ({
  entry,
  publicTitle,
  privateTitle,
  publishDisabledTitle,
  draftDisabledTitle,
  toggleAriaLabel,
  highlightId,
}) => {
  const [isPublic, setIsPublic] = useState(entry.isPublic());
  const [contextIsPublic] = useAsyncCallback(
    (entityEntry) =>
      entityEntry
        .getContext()
        .getEntry()
        .then((contextEntry) => contextEntry.isPublic()),
    entry,
    false
  );
  const [addSnackbar] = useSnackbar();
  const translate = useTranslation(escoListNLS);

  const handleSwitchChange = () => {
    const entryInfo = entry.getEntryInfo();
    const acl = entryInfo.getACL(true);

    getGroupWithHomeContext(entry.getContext()).then((groupEntry) => {
      if (isPublic) {
        acl.admin = acl.admin || [];
        acl.admin.push(groupEntry.getId());
        entryInfo.setACL(acl);
      } else {
        entryInfo.setACL({});
      }
      entryInfo
        .commit()
        .then(setIsPublic(!isPublic))
        .then(() =>
          addSnackbar({
            message: translate(`${isPublic ? 'un' : ''}publishSnackbarMessage`),
            autoHideDuration: 500,
          })
        );
    });
  };

  const publishStateTitle = isPublic ? publicTitle : privateTitle;

  if (isDraftEntry(entry)) {
    return <ListDraftStatusIcon entry={entry} />;
  }

  return (
    <Tooltip
      title={
        isDraftEntry(entry)
          ? draftDisabledTitle
          : (contextIsPublic && publishStateTitle) ||
            publishDisabledTitle ||
            privateTitle
      }
    >
      <div data-highlight-id={highlightId}>
        <Switch
          disabled={!contextIsPublic || isDraftEntry(entry)}
          checked={contextIsPublic && isPublic}
          onChange={handleSwitchChange}
          name="checked"
          inputProps={{ 'aria-label': toggleAriaLabel }}
        />
      </div>
    </Tooltip>
  );
};

PublishEntryToggle.propTypes = {
  entry: PropTypes.instanceOf(Entry),
  toolTipNLS: PropTypes.shape({}),
  publicTitle: PropTypes.string,
  privateTitle: PropTypes.string,
  publishDisabledTitle: PropTypes.string,
  draftDisabledTitle: PropTypes.string,
  toggleAriaLabel: PropTypes.string,
  highlightId: PropTypes.string,
};
