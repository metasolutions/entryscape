import { useCallback } from 'react';
import { updateContextTitle } from 'commons/util/context';
import ListEditEntryDialog from 'commons/components/EntryListView/dialogs/ListEditEntryDialog';

const EditTerminologyDialog = (props) => {
  const onEdit = useCallback((entry) => {
    updateContextTitle(entry);
  }, []);

  return <ListEditEntryDialog onEntryEdit={onEdit} {...props} />;
};

export default EditTerminologyDialog;
