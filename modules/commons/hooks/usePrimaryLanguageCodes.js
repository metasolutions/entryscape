import { useUserState } from 'commons/hooks/useUser';
import { useLanguage } from 'commons/hooks/useLanguage';
import {
  getClientAcceptLanguages,
  getLocaleSupportedLanguages,
} from 'commons/locale';

const usePrimaryLanguageCodes = () => {
  const { userInfo } = useUserState();
  const [language] = useLanguage();
  return [
    ...new Set(
      [
        language,
        ...getClientAcceptLanguages(userInfo),
        ...getLocaleSupportedLanguages(),
      ].filter((code) => !code.includes('-'))
    ),
  ];
};

export default usePrimaryLanguageCodes;
