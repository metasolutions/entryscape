import PropTypes from 'prop-types';
import { useParams } from 'react-router-dom';
import { ESContextProvider } from 'commons/hooks/useESContext';
import ConditionalWrapper from 'commons/components/common/ConditionalWrapper';
import { EntryProvider } from 'commons/hooks/useEntry';

const ParameterizedViewProviders = ({ children, path, pathname }) => {
  const params = useParams();
  const { contextId, entryId } = params;

  return (
    <ESContextProvider path={path} contextId={contextId} key={pathname}>
      <ConditionalWrapper
        condition={Boolean(contextId && entryId)}
        wrapper={(wrapperChildren) => (
          <EntryProvider contextId={contextId} entryId={entryId} default>
            {wrapperChildren}
          </EntryProvider>
        )}
      >
        {children}
      </ConditionalWrapper>
    </ESContextProvider>
  );
};

ParameterizedViewProviders.propTypes = {
  children: PropTypes.node,
  path: PropTypes.string,
  pathname: PropTypes.string,
};

export default ParameterizedViewProviders;
