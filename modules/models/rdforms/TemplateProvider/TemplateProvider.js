import { createContext, useCallback, useContext } from 'react';
import PropTypes from 'prop-types';
import { Entry } from '@entryscape/entrystore-js';
import { cloneDeep } from 'lodash-es';

const TemplateContext = createContext();

export const useTemplateContext = () => {
  const context = useContext(TemplateContext);
  if (!context)
    throw new Error('Template context must be used within context provider');
  return context;
};

export const TemplateProvider = ({ entry, templates, ...props }) => {
  /**
   * @param {string} templateProperty
   * @returns {Object}
   */
  const getTemplate = useCallback(
    (templateProperty) => {
      const template = templates.find(
        ({ property }) => property === templateProperty
      );
      if (!template) {
        throw new Error(`No template found with property ${templateProperty}`);
      }
      return cloneDeep(template);
    },
    [templates]
  );

  return (
    <TemplateContext.Provider
      value={{ entry, getTemplate, templates }}
      {...props}
    />
  );
};

TemplateProvider.propTypes = {
  entry: PropTypes.instanceOf(Entry),
  templates: PropTypes.arrayOf(PropTypes.shape({})),
};
