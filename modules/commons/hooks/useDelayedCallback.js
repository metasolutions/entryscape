import { useRef, useCallback, useLayoutEffect } from 'react';
import sleep from 'commons/util/sleep';

const useDelayedCallback = () => {
  const isMounted = useRef(false);
  useLayoutEffect(() => {
    isMounted.current = true;
    return () => {
      isMounted.current = false;
    };
  });
  const delayedCallback = useCallback((callback, delay) => {
    return async (...args) => {
      await sleep(delay);
      if (isMounted.current) callback(...args);
    };
  }, []);
  return delayedCallback;
};

export default useDelayedCallback;
