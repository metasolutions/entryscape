import { STATUS_IDLE } from 'commons/hooks/useTasksList';

const TASKS_UPDATE_PROJECT_URI = [
  {
    id: 'project',
    nlsKeyLabel: 'updateTask',
    status: STATUS_IDLE,
    message: '',
  },
];

export { TASKS_UPDATE_PROJECT_URI };
