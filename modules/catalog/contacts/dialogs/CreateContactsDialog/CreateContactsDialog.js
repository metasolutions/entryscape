import PropTypes from 'prop-types';
import { createEntry } from 'commons/util/store';
import { Context, Entry } from '@entryscape/entrystore-js';
// eslint-disable-next-line max-len
import CreateEntryByTemplateDialog from 'commons/components/EntryListView/dialogs/create/CreateEntryByTemplateDialog';
import { useESContext } from 'commons/hooks/useESContext';
import { RDF_TYPE_CONTACT } from 'commons/util/entry';
import { LEVEL_MANDATORY } from 'commons/components/rdforms/LevelSelector';
import { useTranslation } from 'commons/hooks/useTranslation';
import escaContactsNLS from 'catalog/nls/escaContacts.nls';
import { useSnackbar } from 'commons/hooks/useSnackbar';
import { useState } from 'react';

/**
 *
 * @param {Context} context
 * @returns {Entry}
 */
const createPrototypeEntry = (context) => {
  const [, defaultRdfContactType] = RDF_TYPE_CONTACT;
  const prototypeEntry = createEntry(context, defaultRdfContactType);
  prototypeEntry
    .getMetadata()
    .add(prototypeEntry.getResourceURI(), 'rdf:type', defaultRdfContactType);
  return prototypeEntry;
};

const CreateContactsDialog = ({ onCreate, ...props }) => {
  const translate = useTranslation(escaContactsNLS);
  const [addSnackbar] = useSnackbar();

  const { context } = useESContext();
  const [prototypeEntry] = useState(() => createPrototypeEntry(context));

  return (
    <CreateEntryByTemplateDialog
      // eslint-disable-next-line react/jsx-props-no-spreading
      {...props}
      entry={prototypeEntry}
      editorLevel={LEVEL_MANDATORY}
      createHeaderLabel={translate('createContactpointHeader')}
      onEntryEdit={() => {
        onCreate();
        addSnackbar({ message: translate('createContactSuccess') });
      }}
    />
  );
};

CreateContactsDialog.propTypes = { onCreate: PropTypes.func };

export default CreateContactsDialog;
