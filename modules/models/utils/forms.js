import * as ns from 'models/utils/ns';

/**
 *
 * @params {{graph: Graph, resourceURI: string}} entry
 * @returns {boolean}
 */
export const isForm = ({ graph, resourceURI }) => {
  return Boolean(graph.find(resourceURI, 'rdf:type', ns.RDF_TYPE_FORM).length);
};
