import { namespaces as ns } from '@entryscape/rdfjson';
import { entrystore } from 'commons/store';
import { spreadEntry } from 'commons/util/store';
import { RDF_TYPE_DOCUMENT } from 'commons/util/entry';
import { getEntriesByPropertyPath } from './distribution';
/**
 *
 * @param query
 * @return {Promise<Set[]>}
 */
const search = async (query, context, checkContinue) => {
  const resourceURIs = new Set();
  /**
   * @param entry
   * @return {Promise<void>}
   */
  const getChildEntryResourceURI = async (entry) => {
    const { metadata, ruri } = spreadEntry(entry);
    const rdfType = metadata.findFirstValue(ruri, 'rdf:type');
    let propertyPath = [];
    switch (rdfType) {
      case ns.expand('dcat:Dataset'):
        propertyPath = ['dcat:distribution', 'dcat:accessURL'];
        break;
      case ns.expand('dcat:Distribution'):
        propertyPath = ['dcat:accessURL'];
        break;
      case ns.expand('esterms:File'): // we only need the resource URI in this case
      default:
    }

    const resourceEntries = await getEntriesByPropertyPath(
      [entry],
      context,
      propertyPath
    );
    resourceEntries.forEach((fileEntry) => {
      resourceURIs.add(fileEntry.getResourceURI());
      if (!checkContinue()) return Promise.resolve(false);
    });
    if (!checkContinue()) return Promise.resolve(false);
  };

  await entrystore
    .newSolrQuery()
    .context(context)
    .rdfType([
      'dcat:Dataset',
      'dcat:Distribution',
      'esterms:File',
      ...RDF_TYPE_DOCUMENT,
    ])
    .title(query)
    .forEach(getChildEntryResourceURI);

  return resourceURIs;
};

export default search;
