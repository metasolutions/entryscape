import { useMemo } from 'react';

// Hoc to dispatch an action for a callback function provided to a Dialog
// component. The callbackName param is used to couple the dispatch with a
// current callback prop.
const withActionCallback =
  (useModel, Dialog, callbackName, actionType) => (props) => {
    const [, dispatch] = useModel();

    const { [callbackName]: callback, ...rest } = props;

    const actionProp = useMemo(() => {
      const callbackWithAction = async (...callbackParams) => {
        if (callback) {
          await Promise.resolve(callback(...callbackParams));
        }
        dispatch({ type: actionType });
      };
      return {
        [callbackName]: callbackWithAction,
      };
    }, [callback, dispatch]);

    if (!callbackName) {
      throw new Error(
        'Required param callbackName is missing for withActionCallback'
      );
    }

    return <Dialog {...rest} {...actionProp} />;
  };

export default withActionCallback;
