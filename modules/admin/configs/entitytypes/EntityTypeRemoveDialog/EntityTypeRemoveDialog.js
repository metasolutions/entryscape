import PropTypes from 'prop-types';
import ListRemoveEntryDialog from 'commons/components/EntryListView/dialogs/ListRemoveEntryDialog';
import { Entry } from '@entryscape/entrystore-js';
import { useTranslation } from 'commons/hooks/useTranslation';
import { useListView } from 'commons/components/ListView';

const EntityTypeRemoveDialog = ({ entry, closeDialog }) => {
  const { nlsBundles } = useListView();
  const t = useTranslation(nlsBundles);

  return (
    <ListRemoveEntryDialog
      nlsBundles={nlsBundles}
      entry={entry}
      closeDialog={closeDialog}
      removeSuccessMessage={t('removeCustomEntityTypeSuccess')}
      removeFailMessage={t('removeCustomEntityTypeFail')}
    />
  );
};

EntityTypeRemoveDialog.propTypes = {
  entry: PropTypes.instanceOf(Entry),
  closeDialog: PropTypes.func,
};

export default EntityTypeRemoveDialog;
