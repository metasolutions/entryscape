import PropTypes from 'prop-types';
import React, { useState, useRef } from 'react';
import {
  Button,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  Divider,
  Grid,
} from '@mui/material';
import { useTranslation } from 'commons/hooks/useTranslation';
import escaEmbedPreviewNLS from 'catalog/nls/escaEmbedPreview.nls';
import escoListNLS from 'commons/nls/escoList.nls';
import sleep from 'commons/util/sleep';
import './index.scss';

const EmbedPreviewDialog = ({ open, onClose, embedScript, fontawesomeUrl }) => {
  const t = useTranslation([escaEmbedPreviewNLS, escoListNLS]);
  const iframeEl = useRef(null);
  const [height, setHeight] = useState('auto');
  const fontawesomeLink = fontawesomeUrl
    ? `<link rel="stylesheet" href=${fontawesomeUrl}>`
    : '';

  const updateIframeHeight = async () => {
    await sleep(300);
    const body = iframeEl.current?.contentWindow?.document?.body;

    if (!body) return;

    if (body.querySelector('.dataset')) {
      setHeight(`${body.scrollHeight}px`);
      return;
    }

    updateIframeHeight();
  };

  return (
    <Dialog
      fullWidth
      maxWidth="md"
      aria-labelledby="embed-preview-dialog"
      open={open}
      onClose={onClose}
    >
      <DialogTitle id="embed-preview-dialog">
        {t('embedPreviewHeader')}
      </DialogTitle>
      <Divider />

      <DialogContent>
        <Grid container justifyContent="center" alignItems="center">
          <iframe
            ref={iframeEl}
            title={t('embedPreviewHeader')}
            className="embedPreviewDialog__embedCode"
            onLoad={() => updateIframeHeight()}
            scrolling="auto"
            style={{ height, minHeight: '400px' }}
            srcDoc={`
                <!DOCTYPE html>
                <html>
                  <head><meta charset="utf-8">${fontawesomeLink}</head>
                  <body>${embedScript}</body>
                </html>`}
          />
        </Grid>
      </DialogContent>
      <Divider />

      <DialogActions>
        <Button autoFocus variant="text" onClick={onClose}>
          {t('close')}
        </Button>
      </DialogActions>
    </Dialog>
  );
};

EmbedPreviewDialog.propTypes = {
  open: PropTypes.bool,
  onClose: PropTypes.func,
  embedScript: PropTypes.string,
  fontawesomeUrl: PropTypes.string,
};

export default EmbedPreviewDialog;
