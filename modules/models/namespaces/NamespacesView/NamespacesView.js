import { Divider } from '@mui/material';
import { useESContext } from 'commons/hooks/useESContext';
import { useTranslation } from 'commons/hooks/useTranslation';
import { RefreshButton } from 'commons/components/EntryListView';
import {
  ListView,
  ListActions,
  ListSearch,
  List,
  ListPagination,
  ListHeader,
  ListHeaderItemText,
  ListHeaderItemSort,
  withListModelProviderAndLocation,
  useListModel,
  useListQuery,
  useFilterListActions,
  listPropsPropType,
} from 'commons/components/ListView';
import { getLabel } from 'commons/util/rdfUtils';
import { ACTION_CREATE_ID, ACTION_IMPORT_ID } from 'commons/actions/actionIds';
import {
  nlsBundles,
  CreateNamespaceButton,
  ImportNamespaceButton,
  presenterAction,
} from './actions';
import {
  getDefaultEntry,
  getNamespaceAbbreviation,
} from '../utils/namespaceMetadata';
import useGetNamespaceEntries from '../hooks/useGetNamespaceEntries';
import NamespacesListItem from '../NamespacesListItem';

const NamespacesView = ({ listProps }) => {
  const translate = useTranslation(nlsBundles);
  const { context } = useESContext();
  const [{ refreshCount, search }, dispatch] = useListModel();
  const { namespaceEntries, size, status } = useGetNamespaceEntries(
    context,
    refreshCount
  );
  const defaultEntry = getDefaultEntry(namespaceEntries);

  const { result: namespaceListItems, size: searchSize } = useListQuery({
    items: namespaceEntries.map((entry) => ({
      label: `${getNamespaceAbbreviation(entry)} - ${getLabel(entry)}`,
      entry,
    })),
    searchFields: ['label'],
  });

  const showDefaultEntry = Boolean(
    defaultEntry &&
      namespaceListItems.find(({ entry }) => entry === defaultEntry)
  );

  const defaultActions = [
    {
      id: ACTION_IMPORT_ID,
      element: (
        <ImportNamespaceButton
          entries={namespaceEntries}
          dispatch={dispatch}
          key={ACTION_IMPORT_ID}
        />
      ),
    },
    {
      id: ACTION_CREATE_ID,
      element: <CreateNamespaceButton key={ACTION_CREATE_ID} />,
    },
  ];
  const listActions = useFilterListActions({
    listActions: defaultActions,
    listActionsProps: {},
    excludeActions: listProps.excludeActions,
    nlsBundles,
  });

  return (
    <>
      <ListView
        size={size === -1 ? size : searchSize}
        search={search}
        status={status}
        nlsBundles={nlsBundles}
        renderPlaceholder={(Placeholder) => (
          <Placeholder>
            <ImportNamespaceButton
              entries={namespaceEntries}
              dispatch={dispatch}
            />
            <CreateNamespaceButton />
          </Placeholder>
        )}
      >
        <ListActions disabled={size === -1}>
          <ListSearch />
          <RefreshButton />
          {listActions.map(({ element }) => element)}
        </ListActions>

        <List>
          <ListHeader>
            <ListHeaderItemText xs={1} text="" />
            <ListHeaderItemText xs={7} text={translate('listHeaderLabel')} />
            <ListHeaderItemSort
              xs={3}
              text={translate('listHeaderModifiedDate')}
              sortBy="modified"
            />
          </ListHeader>
          {showDefaultEntry ? (
            <>
              <NamespacesListItem
                key={`${defaultEntry.getId()}`}
                entry={defaultEntry}
                nlsBundles={nlsBundles}
                excludeActions={listProps.excludeActions}
                infoAction={{
                  ...presenterAction,
                  entry: defaultEntry,
                  title: translate('infoDialogTitle'),
                }}
              />
              {namespaceListItems.length > 1 ? (
                <Divider sx={{ margin: '32px 0' }} />
              ) : null}
            </>
          ) : null}
          {namespaceListItems
            .filter(({ entry }) => entry !== defaultEntry)
            .map(({ entry }) => (
              <NamespacesListItem
                key={`${entry.getId()}`}
                entry={entry}
                dispatch={dispatch}
                nlsBundles={nlsBundles}
                excludeActions={listProps.excludeActions}
                infoAction={{
                  ...presenterAction,
                  entry,
                  title: translate('infoDialogTitle'),
                }}
              />
            ))}
        </List>
        <ListPagination />
      </ListView>
    </>
  );
};

NamespacesView.propTypes = {
  listProps: listPropsPropType,
};

export default withListModelProviderAndLocation(NamespacesView);
