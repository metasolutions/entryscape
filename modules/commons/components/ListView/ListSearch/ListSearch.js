import PropTypes from 'prop-types';
import escoListNLS from 'commons/nls/escoList.nls';
import config from 'config';
import { useTranslation } from 'commons/hooks/useTranslation';
import { useListModel } from '../hooks/useListModel';
import { ListSearchField } from './ListSearchField';
import { useListView } from '../hooks/useListView';
import './ListSearch.scss';

/**
 *
 * @param {string} search
 * @param {number} resultsSize
 * @returns {Object}
 */
const useSearchMessage = (search, resultsSize) => {
  const t = useTranslation(escoListNLS);
  const minimumSearchLength = config.get('minimumSearchLength');

  const getSearchMessage = () => {
    const searchLength = search.trim().length;
    const getMessage = (message = '', status = 'info') => ({ message, status });

    // Early exit if no search
    if (!searchLength) return getMessage();
    // Message when invalid char have been entered
    if (/["+~#()]/.test(search))
      return getMessage(t('invalidSearchMessage', 'error'));
    // Message when minimum amount chars havn't been entered
    if (searchLength > 0 && searchLength < minimumSearchLength)
      return getMessage(t('listSearchTitle'));
    // Message when there's a search result
    if (searchLength >= 3 && resultsSize > 0)
      return getMessage(t('listResultSizeText', resultsSize?.toString()));
    // Default message
    return getMessage();
  };
  return getSearchMessage();
};

export const ListSearch = ({ disabled }) => {
  const [{ search }, dispatch] = useListModel();
  const { size, status: queryStatus } = useListView();
  const { message, status } = useSearchMessage(search, size);
  const t = useTranslation([escoListNLS]);
  const isLoading = queryStatus === 'pending';

  return (
    <div className="escoListViewSearch">
      <ListSearchField
        search={search}
        placeholder={t('search')}
        dispatch={dispatch}
        disabled={disabled}
      />
      <div
        role="status"
        className={status === 'error' ? 'escoListViewSearch--failed' : ''}
      >
        {!disabled && !isLoading ? message : null}
      </div>
    </div>
  );
};

ListSearch.propTypes = {
  disabled: PropTypes.bool,
};
