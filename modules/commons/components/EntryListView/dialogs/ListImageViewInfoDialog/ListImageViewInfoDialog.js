import useRDFormsPresenter from 'commons/components/rdforms/hooks/useRDFormsPresenter';
import PropTypes from 'prop-types';
import { Entry, factory } from '@entryscape/entrystore-js';
import { entrystore } from 'commons/store';
import { spreadEntry } from 'commons/util/store';
import ListActionDialog from 'commons/components/common/dialogs/ListActionDialog';
import ContentWrapper from 'commons/components/common/ContentWrapper';
import ReferencesEntriesList from 'commons/components/EntryListView/lists/ReferenceEntriesList';
import { getLabel } from 'commons/util/rdfUtils';
import { useState, useEffect } from 'react';
import { useTranslation } from 'commons/hooks/useTranslation';
import escoContentViewNLS from 'commons/nls/escoContentview.nls';
import Alert from '@mui/material/Alert';
import ExpandButton from 'commons/components/buttons/ExpandButton';
import ResourceInfo from 'commons/components/rdforms/ResourceInfo/index';
import escoDialogs from 'commons/nls/escoDialogs.nls';
import './ListImageViewInfoDialog.scss';
import { entityTypePropType } from 'commons/types/EntityType';

const ListImageViewInfoDialog = ({
  entry,
  entityType,
  entityTypeDef,
  closeDialog,
  showReferences = true,
}) => {
  // TODO https://metasolutions.atlassian.net/browse/ES-2826
  const template = entityType
    ? entityType.get('template')
    : entityTypeDef.template;
  const contentviewers = entityType
    ? entityType.get('contentviewers')
    : entityTypeDef.contentviewers;
  const { metadata, ruri: resourceURI, info } = spreadEntry(entry);
  const { PresenterComponent } = useRDFormsPresenter(
    metadata,
    resourceURI,
    template
  );
  const [imgSrc, setImgSrc] = useState('');
  const [error, setError] = useState(false);

  const imageContentViewDef = contentviewers.find((contentView) =>
    typeof contentView === 'object'
      ? contentView.name === 'imageview'
      : contentView === 'imageview'
  );

  const translate = useTranslation(escoContentViewNLS);

  useEffect(() => {
    entry.refresh().then(() => {
      if (imageContentViewDef.property) {
        const src = metadata.findFirstValue(null, imageContentViewDef.property);
        if (src) {
          setImgSrc(src);
        } else {
          setError(translate('noImageProvided'));
        }
      } else if (entry.isLink() || info.getFormat()?.startsWith('image/')) {
        const uri = entry.getResourceURI();
        const entrystoreBaseURI = entrystore.getBaseURI();
        const isExternal = !uri.startsWith(entrystoreBaseURI);
        setImgSrc(
          isExternal ? factory.getProxyURI(entrystoreBaseURI, uri) : uri
        );
      } else {
        setError(translate('noImageProvided'));
      }
    });
  }, [entry]);

  return (
    <ListActionDialog
      id="info-entry-image"
      closeDialog={closeDialog}
      title={translate('imageDialogTitle')}
      closeDialogButtonLabel={translate('contentViewDialogCloseLabel')}
      maxWidth="sm"
    >
      <ContentWrapper md={12}>
        <div className="escoListImageViewInfoDialog__content">
          <ExpandButton
            initiallyExpanded={false}
            nlsBundles={[escoDialogs]}
            alternativeClass="escoListImageViewInfoDialog__expandableAreaButton"
          >
            {entry ? <ResourceInfo entry={entry} /> : null}
          </ExpandButton>
          {error ? (
            <Alert className="escoImageView__message" severity="error">
              {error}
            </Alert>
          ) : (
            <img
              className="escoImageView__image"
              src={imgSrc}
              alt={getLabel(entry)}
            />
          )}
          {PresenterComponent ? (
            <PresenterComponent />
          ) : (
            <span>{translate('loadingPlaceholder')}</span>
          )}
          {showReferences && <ReferencesEntriesList entry={entry} />}
        </div>
      </ContentWrapper>
    </ListActionDialog>
  );
};

ListImageViewInfoDialog.propTypes = {
  entry: PropTypes.instanceOf(Entry),
  entityType: entityTypePropType,
  entityTypeDef: PropTypes.shape({
    template: PropTypes.string,
    contentviewers: PropTypes.arrayOf(PropTypes.shape({})),
  }),
  closeDialog: PropTypes.func,
  showReferences: PropTypes.bool,
};

export default ListImageViewInfoDialog;
