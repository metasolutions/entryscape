import { namespaces, Graph } from '@entryscape/rdfjson';
import { Entry } from '@entryscape/entrystore-js';
import * as ns from 'models/utils/ns';
import { extractConstraints } from 'models/utils/constraint';
import {
  FIELD_LABEL,
  FIELD_EDIT_LABEL,
  FIELD_DESCRIPTION,
  FIELD_EDIT_DESCRIPTION,
  FIELD_HELP,
  FIELD_PLACEHOLDER,
  FIELD_PATTERN,
  FIELD_VALUE_TEMPLATE,
  FIELD_VALUE,
  FIELD_LANGUAGE,
} from 'models/utils/fieldDefinitions';
import { getProperty } from 'models/utils/property';
import { findRfsChoices } from 'models/fields/utils/options';
import { getDeps } from 'models/utils/deps';
import { findExtendedEntry } from 'models/utils/extension';
import { applyEntryProps, copyRdfType } from 'models/utils/metadata';
import { FIELD_TYPE_CHOICES } from 'models/utils/choiceDefinitions';
import { getRfsAlternativeTexts } from '../../utils/alternativeText';

const ITEM_LANGSTRING_FIELDS = [
  FIELD_LABEL,
  FIELD_EDIT_LABEL,
  FIELD_DESCRIPTION,
  FIELD_EDIT_DESCRIPTION,
  FIELD_HELP,
  FIELD_PLACEHOLDER,
];

const FIELD_TYPES = [
  ns.RDF_TYPE_TEXT_FIELD,
  ns.RDF_TYPE_DATATYPE_FIELD,
  ns.RDF_TYPE_SELECT_FIELD,
  ns.RDF_TYPE_LOOKUP_FIELD,
];

/**
 * Get type of a field Two Towers
 *
 * @param {Graph} graph
 * @param {string} resourceURI
 * @returns {string}
 */
export const getFieldType = (graph, resourceURI) =>
  FIELD_TYPES.find((type) => {
    return graph.find(resourceURI, 'rdf:type', type).length;
  });

/**
 *
 * @param {Entry} entry
 * @returns {boolean}
 */
export const isSelectField = (entry) => {
  return Boolean(
    entry
      .getMetadata()
      .find(entry.getResourceURI(), 'rdf:type', ns.RDF_TYPE_SELECT_FIELD).length
  );
};

/**
 *
 * @param {Entry} entry
 * @returns {boolean}
 */
export const isInlineField = (entry) => {
  return Boolean(
    entry
      .getMetadata()
      .find(entry.getResourceURI(), 'rdf:type', ns.RDF_TYPE_INLINE_FIELD).length
  );
};

/**
 * Finds text and language value(s) of an entry predicate
 *
 * @param {Entry} entry - graph containing metadata
 * @param {string} resourceURI - the subject of the entry graph
 * @param {string} predicate - the predicate of the entry graph
 * @returns {(object|undefined)} - an object containing the text and language value(s)
 */
const getLiteral = (entry, resourceURI, predicate) => {
  const texts = {};
  const keys = entry.getMetadata().find(resourceURI, predicate);
  for (const k of keys) {
    texts[k.getLanguage() ?? ''] = k.getValue();
  }
  if (
    Object.keys(texts).length === 0 &&
    Object.getPrototypeOf(texts) === Object.prototype
  ) {
    return undefined;
  }
  return texts;
};

const addLangStrings = (fields, obj, entry, resourceURI) => {
  fields.forEach((field) => {
    const literalMap = getLiteral(
      entry,
      resourceURI || entry.getResourceURI(),
      field.property
    );
    if (literalMap) {
      obj[field.jsonAttribute] = literalMap;
    }
  });
};

const addItemLangStrings = (itemObj, entry, resourceURI) =>
  addLangStrings(ITEM_LANGSTRING_FIELDS, itemObj, entry, resourceURI);

/**
 * Finds the cardinalities of the entry
 *
 * @param {Entry} entry - entry containing metadata
 * @param {string | undefined} resourceURI - starting point in the graph
 * @returns {object} - an object containing the cardinality value(s)
 */
const getCardinality = (entry, resourceURI) => {
  const _resourceURI = resourceURI || entry.getResourceURI();
  const metadata = entry.getMetadata();
  const min = metadata.findFirstValue(_resourceURI, ns.RDF_PROPERTY_MIN);
  const pref = metadata.findFirstValue(_resourceURI, ns.RDF_PROPERTY_PREF);
  const max = metadata.findFirstValue(_resourceURI, ns.RDF_PROPERTY_MAX);

  if (min || pref || max) {
    const cardinality = {};
    if (min) cardinality.min = parseInt(min, 10);
    if (pref) cardinality.pref = parseInt(pref, 10);
    if (max) cardinality.max = parseInt(max, 10);
    return cardinality;
  }
  return undefined;
};

/**
 * Compares two objects given their order property
 *
 * @param {object} objectA - object with an order property
 * @param {object} objectB - object with an order property
 * @returns {number} - result of comparison
 */
const compareByOrder = (objectA, objectB) => {
  if (objectA.order < objectB.order) {
    return -1;
  }
  if (objectA.order > objectB.order) {
    return 1;
  }
  return 0;
};

/**
 * Export rfdforms choices from entry
 *
 * @param {Entry} entry
 * @returns {Promise<{choices: object[], defaultVale: string}>} rdforms choices
 */
const getOptions = async (entry) => {
  const choices = await applyEntryProps(findRfsChoices, entry);
  return choices;
};

/**
 * Finds the type of the entry
 *
 * @param {Entry} entry - graph containing metadata
 * @returns {string} - the value of the type predicate
 */
const getType = (entry) => {
  let types = entry.getMetadata().find(entry.getResourceURI(), 'rdf:type');
  types = types.map((s) => s.getValue());
  if (
    types.includes(namespaces.expand(ns.RDF_TYPE_FIELD)) &&
    types.includes(namespaces.expand(ns.RDF_TYPE_LOOKUP_FIELD))
  ) {
    return 'choice';
  }
  if (
    types.includes(namespaces.expand(ns.RDF_TYPE_FIELD)) &&
    types.includes(namespaces.expand(ns.RDF_TYPE_TEXT_FIELD))
  ) {
    return 'text';
  }
  if (
    types.includes(namespaces.expand(ns.RDF_TYPE_FIELD)) &&
    types.includes(namespaces.expand(ns.RDF_TYPE_SELECT_FIELD))
  ) {
    return 'choice';
  }
  if (
    types.includes(namespaces.expand(ns.RDF_TYPE_FIELD)) &&
    types.includes(namespaces.expand(ns.RDF_TYPE_DATATYPE_FIELD))
  ) {
    return 'text';
  }
  if (types.includes(namespaces.expand(ns.RDF_TYPE_FORM))) {
    return 'group';
  }
  return undefined;
};

/**
 * Finds the nodetype of the entry
 *
 * @param {Entry} entry - graph containing metadata
 * @returns {string} - the value of the nodetype predicate
 */
export const getNodetype = (entry) => {
  const nodetypeValue = entry
    .getMetadata()
    .findFirstValue(entry.getResourceURI(), ns.RDF_PROPERTY_NODETYPE);
  // We do not have a default case since we may extend another formItem,
  // in that case we should rely on what is stated there
  // eslint-disable-next-line default-case
  switch (nodetypeValue) {
    case namespaces.expand(ns.RDF_PROPERTY_ONLY_LITERAL):
      return 'ONLY_LITERAL';
    case namespaces.expand(ns.RDF_PROPERTY_LANGUAGE_LITERAL):
      return 'LANGUAGE_LITERAL';
    case namespaces.expand(ns.RDF_PROPERTY_LITERAL):
      return 'LITERAL';
    case namespaces.expand(ns.RDF_PROPERTY_DATATYPE_LITERAL):
      return 'DATATYPE_LITERAL';
    case namespaces.expand(ns.RDF_PROPERTY_RESOURCE):
      return 'RESOURCE';
    case namespaces.expand(ns.RDF_PROPERTY_BLANK):
      return 'BLANK';
    case namespaces.expand(ns.RDF_PROPERTY_URI):
      return 'URI';
  }
};

/**
 * Extracts which entry the current entry extends, if any
 *
 * @param {Entry} entry - an entry containing metadata
 * @returns {string} - the value of the property predicate
 */
const getExtends = (entry) => {
  return entry
    .getMetadata()
    .findFirstValue(entry.getResourceURI(), ns.RDF_PROPERTY_EXTENDS);
};

/**
 * Finds the datatype values of the chosen subject
 *
 * @param {Entry} entry - an entry containing metadata
 * @returns {string[]} - the values of the property predicate
 */
const getDatatypes = (entry) => {
  const datatypes = [];
  const metadata = entry.getMetadata();
  const datatypeStatements = metadata.find(
    entry.getResourceURI(),
    ns.RDF_PROPERTY_DATATYPE
  );
  for (const datatypeStatement of datatypeStatements) {
    const datatype = {};
    const datatypeURI = datatypeStatement.getValue();
    datatype.order = metadata.findFirstValue(datatypeURI, 'rfs:order');
    datatype.value = metadata.findFirstValue(datatypeURI, 'rdf:value');
    datatypes.push(datatype);
  }
  if (datatypes.length === 1) return datatypes[0].value;
  datatypes.sort(compareByOrder);
  const datatypeValues = [];
  for (const datatype of datatypes) {
    datatypeValues.push(datatype.value);
  }
  return datatypeValues.length === 0 ? undefined : datatypeValues;
};

/**
 * Finds the datatype values of the chosen subject
 *
 * @param {Entry} entry - an entry containing metadata
 * @param {string|undefined} resourceURI - the resource uri of the intended subject
 * @returns {string[]} - the values of the property predicate
 */
const getAttributes = (entry, resourceURI) => {
  const attributes = entry
    .getMetadata()
    .find(resourceURI || entry.getResourceURI(), ns.RDF_PROPERTY_ATTRIBUTE)
    .map((stmt) => stmt.getValue());
  return attributes.length === 0 ? undefined : attributes;
};

/**
 * Finds the css classes of the chosen subject
 *
 * @param {Entry} entry - an entry containing metadata
 * @returns {string[]} - the values of the property predicate
 */
const getCSSClasses = (entry) => {
  const cssClasses = entry
    .getMetadata()
    .find(entry.getResourceURI(), ns.RDF_PROPERTY_CSS)
    .map((stmt) => stmt.getValue());
  return cssClasses.length === 0 ? undefined : cssClasses;
};

/**
 * Get deps and replace the field entry with an rdforms item id
 *
 * @param {Entry} entry
 * @param {Map} idMap
 * @returns {Promise<string[]|undefined>}
 */
const getDepsWithItemId = async (entry, idMap) => {
  const resourceURI = entry.getResourceURI();
  const graph = entry.getMetadata();
  const deps = await getDeps(graph, resourceURI);
  if (!deps) return;
  const [depsEntry, value] = deps;
  const itemId = idMap.get(depsEntry.getResourceURI());
  if (!itemId) {
    console.error(`Could not get item id for deps entry: ${depsEntry}`);
    return;
  }
  return [itemId, value];
};

/**
 *
 * @param {*} value
 * @param {string} targetProperty
 * @param {object} optionalTarget
 * @returns {object|undefined}
 */
const addValueIfDefined = (value, targetProperty, optionalTarget) => {
  if (!value) return optionalTarget;
  const target = optionalTarget || {};
  target[targetProperty] = value;
  return target;
};

/**
 * Extract text values as rdforms text
 *
 * @param {Entry} entry
 * @param {string} blankNodeId
 * @returns {object|undefined}
 */
const getText = (entry, blankNodeId) => {
  const resourceURI = blankNodeId || entry.getResourceURI();
  const title = getLiteral(entry, resourceURI, 'dcterms:title');
  const purpose = getLiteral(entry, resourceURI, ns.RDF_PROPERTY_PURPOSE);
  let text = addValueIfDefined(title, 'title');
  text = addValueIfDefined(purpose, 'purpose', text);

  const alternativeTexts = getRfsAlternativeTexts(
    entry.getMetadata(),
    entry.getResourceURI()
  );
  text = { ...text, ...alternativeTexts };
  return text;
};

/**
 * Extract template metadata
 *
 * @param {Entry} entry
 * @returns {object}
 */
const getAbout = (entry) => {
  const metadata = entry.getMetadata();
  const resourceURI = entry.getResourceURI();
  const about = {
    rdfType: metadata
      .find(resourceURI, 'rdf:type')
      .map((statement) => statement.getValue()),
  };
  const status = metadata.findFirstValue(resourceURI, ns.RDF_PROPERTY_STATUS);
  if (status) {
    about.status = status;
  }
  return about;
};

/**
 * Adds text object to the item, if any text properites detected
 *
 * @param {object} itemNode
 * @param {Entry} entry
 * @param {string} itemBlankNode
 */
const addItemText = (itemNode, entry, itemBlankNode) => {
  const text = getText(entry, itemBlankNode);
  if (text) {
    itemNode.text = text;
  }
};

/**
 * Generates an id that is required for an rdforms field. This will be handled
 * in the template export in the future.
 * Note that it is assumed that the context entry is loaded in order to get the
 * context name.
 *
 * @param {Entry} entry
 * @returns {string}
 */
export const generateRdfFormsId = (entry) => {
  const contextName = entry
    .getContext()
    .getEntry(true)
    .getEntryInfo()
    .getName();
  return `${contextName}-${entry.getId()}`;
};

/**
 * Finds the items of the entry
 *
 * @param {Entry} entry - graph containing metadata
 * @param {Map} idIndex - map to lookup identifiers from resourceURIs
 * @returns {Promise<object[]>} - an array containing item objects
 */
const getItems = async (entry, idIndex) => {
  const items = [];
  const itemEntries = entry
    .getMetadata()
    .find(entry.getResourceURI(), ns.RDF_PROPERTY_ITEM)
    .filter((statement) => {
      // Check if item is included in idIndex. If not included, it indicates
      // that the item entry is removed and should be ignored to not break the
      // template.
      const resourceURI = entry
        .getMetadata()
        .findFirstValue(statement.getValue(), ns.RDF_PROPERTY_EXTENDS);
      return idIndex.has(resourceURI);
    });
  for (const item of itemEntries) {
    const itemNode = {};
    const itemBlankNode = item.getValue();
    itemNode.order = entry
      .getMetadata()
      .findFirstValue(itemBlankNode, ns.RDF_PROPERTY_ORDER);
    if (itemNode.order) itemNode.order = parseInt(itemNode.order, 10);
    // itemNode.type = entry.getMetadata().findFirstValue(itemBlankNode, 'rdf:type');
    itemNode.extends = idIndex.get(
      entry.getMetadata().findFirstValue(itemBlankNode, ns.RDF_PROPERTY_EXTENDS)
    );
    addItemLangStrings(itemNode, entry, itemBlankNode);
    addItemText(itemNode, entry, itemBlankNode);
    const cardinality = getCardinality(entry, itemBlankNode);
    if (cardinality) itemNode.cardinality = cardinality;
    const attributes = getAttributes(entry, itemBlankNode);
    if (attributes) itemNode.styles = attributes;
    const constraints = await extractConstraints(entry, itemBlankNode);
    if (constraints) itemNode.constraints = constraints;
    items.push(itemNode);
  }
  items.sort(compareByOrder);
  return items.map(({ extends: ext, order, ...props }) =>
    Object.keys(props).length === 0 && ext ? ext : { extends: ext, ...props }
  );
};

/**
 *
 * @param {Entry} entry
 * @param {Map} idIndex
 * @returns {Promise<object>}
 */
export const getFieldTemplate = async (entry, idIndex) => {
  const formItem = {};

  formItem.id = idIndex.get(entry.getResourceURI());

  const extendsId = getExtends(entry);
  if (extendsId && idIndex.get(extendsId))
    formItem.extends = idIndex.get(extendsId);

  addItemLangStrings(formItem, entry);

  const items = await getItems(entry, idIndex);
  if (items.length > 0) formItem.items = items;

  const property = await getProperty(entry);
  if (property) formItem.property = property;

  const nodetype = getNodetype(entry);
  if (nodetype) formItem.nodetype = nodetype;

  const type = getType(entry);
  if (!extendsId && type) formItem.type = type;

  const cardinality = getCardinality(entry);
  if (cardinality) formItem.cardinality = cardinality;

  if (isSelectField(entry)) {
    const { choices, defaultValue } = await getOptions(entry);
    if (choices.length > 0) formItem.choices = choices;
    if (defaultValue) formItem.value = defaultValue;
  }

  const constraints = await extractConstraints(entry);
  if (constraints) formItem.constraints = constraints;

  const datatype = getDatatypes(entry);
  if (datatype) formItem.datatype = datatype;

  const attributes = getAttributes(entry);
  if (attributes) formItem.styles = attributes;

  const cssClasses = getCSSClasses(entry);
  if (cssClasses) formItem.cls = cssClasses;

  const deps = await getDepsWithItemId(entry, idIndex);
  if (deps) formItem.deps = deps;

  const text = getText(entry);
  if (text) formItem.text = text;
  formItem.about = getAbout(entry);

  [FIELD_PATTERN, FIELD_VALUE_TEMPLATE, FIELD_VALUE, FIELD_LANGUAGE].forEach(
    (field) => {
      const value = entry
        .getMetadata()
        .findFirstValue(entry.getResourceURI(), field.property);
      if (value) formItem[field.jsonAttribute] = value;
    }
  );

  /*
  // TODO deps, uriValueLabelProperties
  */

  return formItem;
};

/**
 * Function to get label for field type.
 *
 * @param {Entry} entry
 * @param {Function} translate
 * @returns {string|undefined}
 */
export const getFieldLabel = (entry, translate) => {
  if (!entry) return undefined;
  const { labelNlsKey } =
    FIELD_TYPE_CHOICES.find(({ value }) => {
      return entry.getMetadata().find(entry.getResourceURI(), 'rdf:type', value)
        .length;
    }) || {};
  return translate(labelNlsKey);
};

/**
 *
 * @param {Graph} graph
 * @param {string} resourceURI
 * @returns {Promise<Graph>}
 */
export const copyExtendedFieldMetadata = async (graph, resourceURI) => {
  const sourceEntry = await findExtendedEntry(graph, resourceURI);
  const sourceGraph = sourceEntry.getMetadata();
  const sourceResourceURI = sourceEntry.getResourceURI();

  copyRdfType(graph, resourceURI, sourceGraph, sourceResourceURI);

  return graph;
};
