import PropTypes from 'prop-types';
import { useOpenlayers } from '../../hooks/useOpenlayers';
import useBuiltinControl from '../../hooks/useBuiltinControl';
import './AttributionControl.scss';

const AttributionControl = ({ target }) => {
  const { createAttribution } = useOpenlayers();
  useBuiltinControl(createAttribution, target);
  return null;
};

AttributionControl.propTypes = {
  target: PropTypes.shape({}),
};

export default AttributionControl;
