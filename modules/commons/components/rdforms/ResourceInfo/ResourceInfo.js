import PropTypes from 'prop-types';
import { Entry } from '@entryscape/entrystore-js';
import Typography from '@mui/material/Typography';
import IconButton from '@mui/material/IconButton';
import Link from '@mui/material/Link';
import escoDialogs from 'commons/nls/escoDialogs.nls';
import { useTranslation } from 'commons/hooks/useTranslation';
import useGetContributors from 'commons/hooks/useGetContributors';
import { getShortDate } from 'commons/util/date';
import { getCreatorName, getModifiedDate } from 'commons/util/metadata';
import { findEntityTypeForContext } from 'commons/types/utils/entityType';
import Lookup from 'commons/types/Lookup';
import useAsync from 'commons/hooks/useAsync';
import 'commons/components/rdforms/ResourceInfo/index.scss';
import { useEffect } from 'react';
import { localize } from 'commons/locale';
import { getFullLengthLabel } from 'commons/util/rdfUtils';
import Tooltip from 'commons/components/common/Tooltip';
import DownloadIcon from '@mui/icons-material/GetApp';
import { Presenter } from 'commons/components/forms/presenters';
import Loader from 'commons/components/Loader';

const useGetEntityTypeLabel = (entry) => {
  const { runAsync, data, isLoading } = useAsync();

  useEffect(() => {
    const loadType = () => {
      if (entry.isContext()) {
        return findEntityTypeForContext(entry);
      }
      return Lookup.inUse(entry);
    };
    runAsync(loadType());
  }, [runAsync, entry]);

  if (data) {
    const refinedTypeName = data.get('refines');
    if (!refinedTypeName)
      return { data: localize(data.get('label')), isLoading };

    const refinedType = Lookup.getByName(refinedTypeName);
    return {
      data: refinedType ? localize(refinedType.label()) : '',
      isLoading,
    };
  }

  return { data, isLoading };
};

/**
 * Get file information
 *
 * @param {object} entryInfo
 * @returns {object}
 */
const getFileInfo = (entryInfo) => {
  const format = entryInfo.getFormat();
  const name = entryInfo.getLabel();
  const size = entryInfo.getSize();
  return { format, size, name };
};

const ResourceInfo = ({ entry }) => {
  const translate = useTranslation(escoDialogs);
  const entryInfo = entry.getEntryInfo();
  const isLink =
    entry.isExternal() || (entry.isLocal() && entryInfo.getSize() > 0);
  const metadataType = [
    { name: 'Turtle', url: '?format=text/turtle' },
    { name: 'RDF/XML', url: '?format=application/rdf+xml' },
    { name: 'JSON-LD', url: '?format=application/ld+json' },
    { name: 'RDF/JSON', url: '?format=application/json' },
  ];

  const { runAsync, data: creator, isLoading: isLoadingCreator } = useAsync();
  useEffect(() => {
    runAsync(getCreatorName(entry));
  }, [runAsync, entry]);

  const { contributors, loading: isLoadingContributors } =
    useGetContributors(entry);
  const modifiedDate = getShortDate(getModifiedDate(entry));
  const createdDate = getShortDate(entryInfo.getCreationDate());
  const { data: entityType, isLoading: isLoadingEntityType } =
    useGetEntityTypeLabel(entry);
  const contextLabel = getFullLengthLabel(entry.getContext().getEntry(true));
  const label = getFullLengthLabel(entry);

  const { format, name, size } = getFileInfo(entryInfo);

  const isDigital = entry.isInformationResource();
  const isLocal = entry.isLocal();
  const canDownload = isDigital && isLocal;

  const isLoading =
    isLoadingCreator || isLoadingContributors || isLoadingEntityType;

  if (isLoading) return <Loader />;

  return (
    <div>
      <Presenter
        inline
        label={translate('createdBy')}
        getValue={() => creator}
      />

      <Presenter
        inline
        label={translate('editedBy')}
        getValue={() => (contributors?.length ? contributors.join(', ') : null)}
      />

      <Presenter
        inline
        label={translate('createdLabel')}
        getValue={() => createdDate}
      />
      <Presenter
        inline
        label={translate('modifiedDate')}
        getValue={() => modifiedDate}
      />

      <Presenter
        inline
        label={translate('entityType')}
        getValue={() => entityType}
      />

      <Presenter
        inline
        label={translate('contextLabel')}
        getValue={() => (label === contextLabel ? null : contextLabel)}
      />

      <Presenter
        label={translate('fileDetails')}
        getValue={() =>
          name && format && size
            ? [
                translate('fileDetailsName', name),
                translate('fileDetailsFormat', format),
                translate('fileDetailsSize', size),
              ].join(', ')
            : null
        }
      />

      <Presenter
        inline
        label={translate('resourceType')}
        getValue={() =>
          isDigital ? translate('digitalResource') : translate('identifier')
        }
      />

      <div className="escoResourceInfoItem">
        <Typography
          variant="body1"
          classes={{ root: 'escoResourceInfoItem__title' }}
        >
          {translate('resource')}:&nbsp;
        </Typography>
        <Typography
          variant="body1"
          classes={{ root: 'escoResourceInfoItem__resourceLink' }}
        >
          {isLink ? (
            <>
              <Tooltip title={translate('linkTooltip')}>
                <Link
                  href={entry.getEntryInfo().getResourceURI()}
                  underline="always"
                  target="_blank"
                >
                  {entry.getEntryInfo().getResourceURI()}
                </Link>
              </Tooltip>
              {canDownload ? (
                <Tooltip title={translate('downloadResourceTooltip')}>
                  <IconButton
                    aria-label={translate('downloadResourceTooltip')}
                    onMouseDown={(e) => e.preventDefault()}
                    href={entry.getEntryInfo().getResourceURI()}
                    download
                    style={{ marginLeft: '12px' }}
                  >
                    <DownloadIcon />
                  </IconButton>
                </Tooltip>
              ) : null}
            </>
          ) : (
            entry.getEntryInfo().getResourceURI()
          )}
        </Typography>
      </div>
      <div className="escoResourceInfoItem">
        <Typography
          variant="body1"
          classes={{ root: 'escoResourceInfoItem__title' }}
        >
          {translate('download')}:&nbsp;
        </Typography>
        <div className="escoResourceInfoItem__downloadlinks">
          {metadataType
            .map((type) => (
              <Typography variant="body1" key={type.name}>
                <Link
                  underline="always"
                  href={entry.getEntryInfo().getMetadataURI() + type.url}
                  target="_blank"
                >
                  {type.name}
                </Link>
              </Typography>
            ))
            .reduce((joinedItems, Item) => [joinedItems, ',\u00A0', Item])}
        </div>
      </div>

      <div className="escoResourceInfoItem">
        <Typography
          variant="body1"
          classes={{ root: 'escoResourceInfoItem__title' }}
        >
          {translate('downloadURI')}:&nbsp;
        </Typography>
        <div className="escoResourceInfoItem__downloadlinks">
          {metadataType
            .map((type) => (
              <Typography variant="body1" key={type.name}>
                <Link
                  underline="always"
                  href={entry.getURI() + type.url}
                  target="_blank"
                >
                  {type.name}
                </Link>
              </Typography>
            ))
            .reduce((joinedItems, Item) => [joinedItems, ',\u00A0', Item])}
        </div>
      </div>
    </div>
  );
};

ResourceInfo.propTypes = {
  entry: PropTypes.instanceOf(Entry),
};

export default ResourceInfo;
