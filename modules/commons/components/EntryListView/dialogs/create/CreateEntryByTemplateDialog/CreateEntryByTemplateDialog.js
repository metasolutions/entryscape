import React, { useCallback, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Entry } from '@entryscape/entrystore-js';
import { ButtonGroup, Stack } from '@mui/material';
import escoListNLS from 'commons/nls/escoList.nls';
import escoErrorsNLS from 'commons/nls/escoErrors.nls';
import {
  useTranslation,
  nlsBundlesPropType,
} from 'commons/hooks/useTranslation';
import useAsync, { PENDING } from 'commons/hooks/useAsync';
import { useESContext } from 'commons/hooks/useESContext';
import useLevelProfile from 'commons/components/rdforms/hooks/useLevelProfile';
import LevelSelector, {
  LEVEL_MANDATORY,
} from 'commons/components/rdforms/LevelSelector';
import ListActionDialog from 'commons/components/common/dialogs/ListActionDialog';
import {
  RdformsDialogFormWrapper,
  RdformsStickyDialogContent,
  RdformsDialogContent,
} from 'commons/components/rdforms/RdformsDialogFormWrapper';
import DialogTwoColumnLayout, {
  PrimaryColumn,
  SecondaryColumn,
} from 'commons/components/common/dialogs/DialogTwoColumnLayout';
import RdformsOutline from 'commons/components/rdforms/RdformsOutline';
// eslint-disable-next-line max-len
import useRdformsOutlineSearch from 'commons/components/rdforms/RdformsOutline/useRdformsOutlineSearch';
import ProfileChooser from 'commons/types/ProfileChooser';
import useGetEntityTypes from 'commons/types/useGetEntityTypes';
import LoadingButton from 'commons/components/LoadingButton';
import { useRDFormsValidation } from 'commons/components/rdforms/hooks/useRDFormsValidation';
import { ErrorCatcher } from 'commons/errors/ErrorCatcher';
import { ErrorWithMessage } from 'commons/errors/utils/errors';
import { commitMetadata } from 'commons/util/entry';
import { validate } from '@entryscape/rdforms';
import useAssignDraft from 'commons/hooks/useAssignDraft';
import { useConfirmCloseAction, useCreateEntryState } from '../handlers';

const CreateEntryByTemplateDialog = ({
  // fixed props - passed by (list) SecondaryActions
  entry,
  closeDialog,
  nlsBundles = [],

  // dynamic props - to be set per edit dialog
  formTemplateId,
  createEntry = commitMetadata,
  onEntryEdit,
  editorLevel = LEVEL_MANDATORY,
  createHeaderLabel = null,
  createButtonLabel = null,
  onCreate = (createdEntry) => Promise.resolve(createdEntry),
  createErrorNls = 'createFail',
  afterCreateEntry = () => {},
}) => {
  const outlineSearchProps = useRdformsOutlineSearch();
  const { clearSearch: clearOutlineSearch } = outlineSearchProps;

  const {
    level,
    setLevel,
    editor,
    EditorComponent,
    graph,
    setButtonDisabled,
    selectedEntityType,
    onChangeEntityType,
    templateId,
  } = useCreateEntryState({
    entry,
    formTemplateId,
    editorLevel,
  });
  const { checkFormErrors, formErrorMessage, setFormErrorMessage, formErrors } =
    useRDFormsValidation({
      editor,
      onError: clearOutlineSearch,
    });
  const disabledLevels = useLevelProfile(templateId);
  const { context } = useESContext();
  const confirmClose = useConfirmCloseAction(closeDialog);
  const bundles = [escoListNLS, escoErrorsNLS, ...nlsBundles];
  const translate = useTranslation(bundles);
  const { entityTypes } = useGetEntityTypes(entry);
  const showProfileChooser =
    entityTypes.length > 1 && !formTemplateId && selectedEntityType;
  const { runAsync: runSaveChanges, status, error: createError } = useAsync();
  const { checkCanDraft, applyDraftChanges } = useAssignDraft(true);

  useEffect(() => {
    if (graph) {
      graph.onChange = () => {
        setFormErrorMessage(null);
        setButtonDisabled(false);
      };
    }
  }, [graph, setFormErrorMessage, setButtonDisabled]);

  const handleSaveForm = useCallback(async () => {
    const { errors } = validate.bindingReport(editor.binding);

    const canDraft = await checkCanDraft(errors, selectedEntityType);
    if (!canDraft) return;

    const afterMetadataUpdate = (updatedEntry) =>
      onEntryEdit ? onEntryEdit(updatedEntry) : Promise.resolve(updatedEntry);

    const saveChanges = async () => {
      return createEntry(entry, graph, context).catch((error) => {
        throw new ErrorWithMessage(translate(createErrorNls), error);
      });
    };

    runSaveChanges(
      applyDraftChanges(entry)
        .then(saveChanges)
        .then(afterMetadataUpdate)
        .then(onCreate)
        .then(afterCreateEntry)
        .then(closeDialog)
    );
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [
    entry,
    context,
    graph,
    onEntryEdit,
    createEntry,
    closeDialog,
    onCreate,
    checkFormErrors,
    runSaveChanges,
    translate,
    createErrorNls,
  ]);

  const handleCloseAction = useCallback(() => {
    const changeCondition = graph.isChanged();
    confirmClose(changeCondition);
  }, [confirmClose, graph]);

  const title = createHeaderLabel || translate('createHeader');

  const dialogActions = (
    <ButtonGroup disableElevation variant="contained">
      <LoadingButton
        autoFocus
        loading={status === PENDING}
        onClick={handleSaveForm}
      >
        {createButtonLabel || translate('createButton')}
      </LoadingButton>
    </ButtonGroup>
  );

  return (
    <>
      <ListActionDialog
        id="create-entry"
        title={title}
        actions={dialogActions}
        closeDialog={handleCloseAction}
        alert={
          formErrorMessage
            ? {
                message: formErrorMessage,
                setMessage: setFormErrorMessage,
              }
            : undefined
        }
      >
        <DialogTwoColumnLayout>
          <SecondaryColumn>
            {editor ? (
              <RdformsOutline
                editor={editor}
                root="create-entry"
                errors={formErrors}
                searchProps={outlineSearchProps}
              />
            ) : null}
          </SecondaryColumn>
          <PrimaryColumn>
            <RdformsDialogFormWrapper>
              <RdformsStickyDialogContent>
                <Stack direction="row" spacing={2}>
                  <LevelSelector
                    level={level}
                    onUpdateLevel={setLevel}
                    disabledLevels={disabledLevels}
                  />
                  {showProfileChooser ? (
                    <ProfileChooser
                      entityTypes={entityTypes}
                      selectedEntityType={selectedEntityType}
                      onChangeEntityType={onChangeEntityType}
                    />
                  ) : null}
                </Stack>
              </RdformsStickyDialogContent>
              <RdformsDialogContent>
                {editor && <EditorComponent />}
              </RdformsDialogContent>
            </RdformsDialogFormWrapper>
          </PrimaryColumn>
        </DialogTwoColumnLayout>
      </ListActionDialog>
      <ErrorCatcher error={createError} />
    </>
  );
};

CreateEntryByTemplateDialog.propTypes = {
  entry: PropTypes.instanceOf(Entry),
  closeDialog: PropTypes.func,
  nlsBundles: nlsBundlesPropType,
  formTemplateId: PropTypes.string,
  createEntry: PropTypes.func,
  onEntryEdit: PropTypes.func,
  onCreate: PropTypes.func,
  editorLevel: PropTypes.string,
  createHeaderLabel: PropTypes.string,
  createButtonLabel: PropTypes.string,
  createErrorNls: PropTypes.string,
  afterCreateEntry: PropTypes.func,
};

export default CreateEntryByTemplateDialog;
