import { Context, Entry, types as ejsTypes } from '@entryscape/entrystore-js';
import { EntityType, findEntityTypeByConstraint } from 'entitytype-lookup';
import { entrystore } from 'commons/store';
import Lookup from 'commons/types/Lookup';
import {
  CONTEXT_TYPE_TERMINOLOGY,
  CONTEXT_TYPE_WORKBENCH,
  CONTEXT_TYPE_MODELS,
  CONTEXT_TYPE_CATALOG,
} from 'commons/util/context';
import {
  CONTEXT_CONTEXTS,
  CONTEXT_ENTRY_ID_PRINCIPALS,
} from 'commons/util/userIds';
import {
  CATALOG_MODULE,
  MODELS_MODULE,
  REGISTER_MODULE,
  TERMS_MODULE,
  WORKBENCH_MODULE,
} from 'commons/util/moduleDefinitions';
import {
  ANY,
  ANY_FILTER_ITEM,
  AUTOCOMPLETE,
} from 'commons/components/filters/utils/filterDefinitions';
import { getLabel } from 'commons/util/rdfUtils';
import { localize } from 'commons/locale';
import { getPathFromViewName } from 'commons/util/site';
import { ENTITY_OVERVIEW_NAME } from 'workbench/config/config';
import { findModuleNameFromContextEntry } from 'commons/util/module';
import {
  checkEntityTypeInModules,
  findEntityTypeForContext,
  getBuiltinEntityTypeConfigs,
  getCustomEntityTypeConfigs,
} from 'commons/types/utils/entityType';

/**
 * Get context rdf types for the active modules
 *
 * @param {string[]} activeModuleNames
 * @returns {string[]}
 */
const getContextTypesByActiveModules = (activeModuleNames) => {
  const types = [];
  if (activeModuleNames.includes(CATALOG_MODULE))
    types.push(CONTEXT_TYPE_CATALOG);
  if (activeModuleNames.includes(TERMS_MODULE))
    types.push(CONTEXT_TYPE_TERMINOLOGY);
  if (activeModuleNames.includes(MODELS_MODULE))
    types.push(CONTEXT_TYPE_MODELS);
  if (activeModuleNames.includes(WORKBENCH_MODULE))
    types.push(CONTEXT_TYPE_WORKBENCH);

  return types;
};

/**
 *
 * @param {boolean} isGuest
 * @param {string[]} activeModuleNames
 * @returns {Entry[]}
 */
const getContextEntries = (isGuest, activeModuleNames) => {
  const rdfTypes = getContextTypesByActiveModules(activeModuleNames);
  const query = entrystore.newSolrQuery().rdfType(rdfTypes).limit(100);
  if (activeModuleNames.includes(REGISTER_MODULE))
    query.graphType(ejsTypes.GT_CONTEXT);
  if (isGuest) query.publicRead(true);
  return query.list().getAllEntries();
};

/**
 *
 * @param {boolean} isGuest
 * @param {string[]} activeModuleNames
 * @returns {Promise<object[]>}
 */
export const getContextItems = async (isGuest, activeModuleNames) => {
  const contextEntries = await getContextEntries(isGuest, activeModuleNames);
  const contextItems = contextEntries
    .map((contextEntry) => ({
      label: getLabel(contextEntry),
      value: contextEntry.getId(),
    }))
    .filter(({ label }) => label);
  return [ANY_FILTER_ITEM, ...contextItems];
};

/**
 *
 * @param {string[]} activeModuleNames
 * @param {Function} translate
 * @returns {object[]}
 */
export const getEntityTypeItems = (activeModuleNames, translate) => {
  const customEntityTypes = getCustomEntityTypeConfigs().map((entityType) => ({
    entityType,
    type: translate('customTypesHeader'),
  }));
  const builtinEntityTypes = getBuiltinEntityTypeConfigs().map(
    (entityType) => ({
      entityType,
      type: translate('builtinTypesHeader'),
    })
  );
  const entityTypes = [...customEntityTypes, ...builtinEntityTypes].filter(
    ({ entityType }) => {
      return (
        !entityType.refines &&
        checkEntityTypeInModules(entityType, activeModuleNames)
      );
    }
  );
  const entityTypeItems = entityTypes
    .map(({ entityType: { name, label }, type }) => ({
      value: name,
      label: localize(label),
      type,
    }))
    .sort((itemA, itemB) => {
      if (itemA.type !== itemB.type) return 0;
      if (itemA.label < itemB.label) return -1;
      if (itemA.label > itemB.label) return 1;
      return 0;
    });
  return [ANY_FILTER_ITEM, ...entityTypeItems];
};

export const CONTEXT_FILTER = {
  id: 'context-filter',
  type: AUTOCOMPLETE,
  headerNlsKey: 'contextFilterLabel',
  applyFilterParams: (query, filter) => {
    const value = filter.getSelected();
    if (value === ANY) return query;

    // the default query contains a NOT modifier on `context` to exclude users - in
    // principals context - that has to be cleared when a different context is selected
    if (query.modifiers.get('context')) {
      query.modifiers.delete('context');
    }

    return query.context(value);
  },
};

export const TYPE_FILTER = {
  id: 'type-filter',
  type: AUTOCOMPLETE,
  headerNlsKey: 'typeFilterLabel',
  applyFilterParams: (query, filter) => {
    const value = filter.getSelected();
    if (value === ANY) return query;

    const selectedEntityType = Lookup.getEntityTypes().find(
      (entityType) => entityType.getName() === value
    );
    const rdfType = selectedEntityType.getRdfType();

    // the default query contains a NOT modifier on `rdfType` to exclude comments
    // that has to be cleared when a different rdf type is selected
    if (query.modifiers.get('rdfType')) {
      query.modifiers.delete('rdfType');
    }

    // modify query to fetch untitled entries when a type has been selected but
    // no title has been specified
    if (query.params.has('title') && query.params.get('title') === '*') {
      query.params.delete('title');
    }

    query.rdfType(rdfType);
  },
};

/**
 *
 * @param {object} constraints
 * @param {Context} context
 * @returns {string|undefined}
 */
const findOverviewByConstraints = (constraints, context) => {
  const enitityTypeWithOverview = findEntityTypeByConstraint(
    Lookup.getEntityTypes().filter((entityType) => entityType.get('overview')),
    constraints,
    findModuleNameFromContextEntry(context.getEntry(true))
  );
  return enitityTypeWithOverview?.get('overview');
};

/**
 *
 * @param {EntityType} entityType
 * @param {Context} context
 * @returns {string|undefined}
 */
export const getOverviewName = (entityType, context) => {
  const { overview, refines, module: moduleName } = entityType.get();

  if (overview) return overview;

  if (refines) {
    const refinedEntityType = Lookup.getByName(refines);
    const overviewInRefines = refinedEntityType?.get('overview');
    if (overviewInRefines) return overviewInRefines;
  }

  // The same overview is always used for workbench entities, and for that
  // reason the overview property is likely not provided.
  if (moduleName?.includes(WORKBENCH_MODULE)) {
    return ENTITY_OVERVIEW_NAME;
  }

  // In case it's a custom entity type not using refines but there's a matching
  // builtin entity type with an overview property.
  return findOverviewByConstraints(entityType.constraints(), context);
};

/**
 *
 * @returns {string[]}
 */
export const getExcludedUris = () => [
  entrystore.getEntryURI(CONTEXT_CONTEXTS, CONTEXT_ENTRY_ID_PRINCIPALS),
];

/**
 * Callback that gets the entity type for each list result/item
 *
 * @param {object} fetchResults
 * @returns {Promise<object[]>} - array of entries with their entity types
 */
export const getEntityTypes = async (fetchResults) => {
  const { entries, ...results } = fetchResults;
  // `allSettled` to avoid breaking in case there's an entry with a project type
  // that's missing from configuration
  const callbackResults = await Promise.allSettled(
    entries.map((entry) => {
      if (entry.isContext()) {
        return findEntityTypeForContext(entry);
      }
      return Lookup.inUse(entry);
    })
  ).then((settledPromises) =>
    settledPromises.map(({ value: entityType }, index) => ({
      entry: entries[index],
      entityType,
    }))
  );

  return { entries, callbackResults, ...results };
};

/**
 * Find all composition relations for an entity type.
 *
 * @param {EntityType} entityType
 * @param {string} entityTypeName
 * @returns {object}
 */
const findCompositionRelations = (entityType, entityTypeName) => {
  const relations = entityType.getRelations();
  if (!relations) return [];
  return relations.filter(
    (relation) =>
      relation.entityType === entityTypeName && relation.type === 'composition'
  );
};

/**
 * Detect if the entry is dependent on a parent entry by composition relation.
 * If composition relation is found, then query for the parent entry using the
 * composition relation property.
 *
 * @param {Entry} entry
 * @param {string} entityTypeName
 * @returns {Entry|undefined}
 */
const findCompositionParentEntry = async (entry, entityTypeName) => {
  const related = [];
  Lookup.getEntityTypes().forEach((entityType) => {
    const relations = findCompositionRelations(entityType, entityTypeName).map(
      (relation) => ({ relation, entityType })
    );
    if (relations.length) {
      related.push(...relations);
    }
  });
  if (!related.length) return;
  for (const { entityType, relation } of related) {
    const { property } = relation;
    const [parentEntry] = await entrystore
      .newSolrQuery()
      .context(entry.getContext())
      .rdfType(entityType.getRdfType())
      .uriProperty(property, entry.getResourceURI())
      .getEntries();
    if (parentEntry) return parentEntry;
  }
};

/**
 * Find the overview url for an entry, in case an overview is not defined on the
 * corresponding entity type. The following special cases need to be handled.
 * 1. Entries that are dependent on a parent entry, using useWith. The entry
 *    doesn't have its own overview but should be navigated to the parent
 *    overview instead.
 * 2. Entity types that use generic router and don't have a defined overview
 *    property. In this case, composition relation must also be considered in
 *    similar way as for useWith.
 * 3. Entries that are contexts themselves which can't use entry.getContext.
 *
 * @param {EntityType} entityType
 * @param {string} overviewName
 * @param {Entry} entry
 * @returns {Promise<string>}
 */
export const getTo = async (entityType, overviewName, entry) => {
  let entryId = entry.getId();
  const useWith = entityType.useWith();
  if (useWith) {
    const parentEntry = await Lookup.findParentEntry(entry);
    entryId = parentEntry.getId();
  }
  // handle composition relation if detected
  if (overviewName === ENTITY_OVERVIEW_NAME) {
    const parentEntry = await findCompositionParentEntry(
      entry,
      entityType.getName()
    );
    if (parentEntry) {
      entryId = parentEntry.getId();
    }
  }
  const context = entry.isContext()
    ? entry.getResource(true)
    : entry.getContext();
  return getPathFromViewName(overviewName, {
    contextId: context.getId(),
    entryId,
    entityName: entityType.get('name'),
  });
};

/**
 *
 * @param {Entry} entry
 * @param {EntityType} entityType
 * @returns {boolean}
 */
export const getCanAccessOverview = (entry, entityType) => {
  const overviewName = getOverviewName(entityType, entry.getContext());
  return Boolean(overviewName && entry.canAdministerEntry());
};
