class IntervalRunner {
  constructor(items, delay, callback) {
    this._delay = delay;
    this._items = items;
    this._callback = callback;
    this._intervalRunner = null;
  }

  start() {
    if (this.isRunning()) return;
    this._intervalRunner = setInterval(() => {
      if (!this._items.length) {
        this.stop();
        return;
      }
      this._callback(this._items);
    }, this._delay);
  }

  stop() {
    clearInterval(this._intervalRunner);
    this._callback(this._items);
    this._intervalRunner = null;
  }

  isRunning() {
    return Boolean(this._intervalRunner);
  }

  setItems(items) {
    this._items = items;

    if (!this._items.length && this.isRunning()) {
      this.stop();
    } else if (this._items.length && !this.isRunning()) {
      this.start();
    }
  }
}

export default IntervalRunner;
