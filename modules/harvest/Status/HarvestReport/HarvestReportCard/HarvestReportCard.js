import PropTypes, { arrayOf } from 'prop-types';

const HarvestReportCard = ({ title, data }) => {
  return (
    <div className="esreHarvestReportCardBlock" key={title + Math.random()}>
      <div className="esreHarvestReportCardBlock__Header">{title}</div>
      <div className="esreHarvestReport__Cards">
        {data.map(({ label, value }) => (
          <div className="esreHarvestReport__Card" key={label + Math.random()}>
            <div className="esreHarvestReportCard__Header">{label}</div>
            <div>{value}</div>
          </div>
        ))}
      </div>
    </div>
  );
};

HarvestReportCard.propTypes = {
  title: PropTypes.string,
  data: arrayOf(
    PropTypes.shape({
      label: PropTypes.string,
      value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    })
  ),
};

export default HarvestReportCard;
