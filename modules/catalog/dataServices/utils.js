import { createEntry, getGroupWithHomeContext } from 'commons/util/store';

/**
 *
 * @param {Entry} entry
 * @param {Entry} groupEntry
 */
const unpublishDataService = (entry, groupEntry) => {
  const entryInformation = entry.getEntryInfo();
  const accessControl = entryInformation.getACL(true);
  accessControl.admin.push(groupEntry.getId());
  entryInformation.setACL(accessControl);
};

/**
 * Create a dataservice prototype
 * @param {Context} context
 * @param {string} rdfType
 * @return {Entry}
 */
export const createPrototypeDataServiceEntry = async (context, rdfType) => {
  const dataServicePrototype = createEntry(context, rdfType);
  const groupEntry = await getGroupWithHomeContext(
    dataServicePrototype.getContext()
  );

  // Make the dataservice entry private
  unpublishDataService(dataServicePrototype, groupEntry);

  const metadata = dataServicePrototype.getMetadata();
  metadata.add(
    dataServicePrototype.getResourceURI(),
    'rdf:type',
    rdfType,
    true
  );

  return dataServicePrototype;
};
