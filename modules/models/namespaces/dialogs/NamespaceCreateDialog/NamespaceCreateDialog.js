import PropTypes from 'prop-types';
import { useMemo } from 'react';
import { useTranslation } from 'commons/hooks/useTranslation';
import { useESContext } from 'commons/hooks/useESContext';
import esmoNamespacesNLS from 'models/nls/esmoNamespaces.nls';
import esmoCommonsNLS from 'models/nls/esmoCommons.nls';
import escoDialogsNLS from 'commons/nls/escoDialogs.nls';
import CreateDialog from 'models/components/CreateDialog';
import { EditorProvider } from 'commons/components/forms/editors';
import { fieldDefinitions } from 'models/namespaces/utils/fieldDefinitions';
import * as ns from 'models/utils/ns';
import { useSnackbar } from 'commons/hooks/useSnackbar';

const nlsBundles = [esmoCommonsNLS, esmoNamespacesNLS, escoDialogsNLS];

/**
 * Adds default namespace metadata before commit
 *
 * @param {Graph} graph
 * @param {string} resourceURI
 * @returns {Graph}
 */
const addNamespaceMetadata = (graph, resourceURI) => {
  graph.add(resourceURI, 'rdf:type', ns.RDF_TYPE_PREFIX);
  return graph;
};

const CreateNamespaceDialog = ({ closeDialog }) => {
  const translate = useTranslation(nlsBundles);
  const { context } = useESContext();
  const [addSnackbar] = useSnackbar();

  const prototypeEntry = useMemo(() => {
    return context.newEntry();
  }, [context]);

  return (
    <EditorProvider entry={prototypeEntry}>
      <CreateDialog
        title={translate('createNamespaceTitle')}
        closeDialog={closeDialog}
        fields={fieldDefinitions}
        beforeCreate={addNamespaceMetadata}
        nlsBundles={nlsBundles}
        onEditEntry={() =>
          addSnackbar({ message: translate('createNamespaceSuccess', 1) })
        }
      />
    </EditorProvider>
  );
};

CreateNamespaceDialog.propTypes = {
  closeDialog: PropTypes.func.isRequired,
};

export default CreateNamespaceDialog;
