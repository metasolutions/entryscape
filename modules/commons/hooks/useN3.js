import { useEffect } from 'react';
import useAsync from 'commons/hooks/useAsync';

const loadN3 = () => import(/* webpackChunkName: "n3" */ 'n3');

const useN3 = () => {
  const { data: N3, status, error, runAsync } = useAsync();

  useEffect(() => {
    runAsync(loadN3());
  }, [runAsync]);

  return { N3, status, error };
};

export default useN3;
