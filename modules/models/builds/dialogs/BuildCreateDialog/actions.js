import { FIELD_TITLE, FIELD_DESCRIPTION } from 'models/utils/fieldDefinitions';
import { targetLanguageField } from 'models/builds/utils/fieldDefinitions';

export const fields = [FIELD_TITLE, FIELD_DESCRIPTION, targetLanguageField];
