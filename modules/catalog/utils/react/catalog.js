import { RDF_TYPE_CATALOG } from 'commons/util/entry';
import { namespaces as ns } from '@entryscape/rdfjson';

/**
 * @param {entry}
 * @return {(boolean|undefined)}
 */
const CATALOG_EXPANDED_TYPE = ns.expand(RDF_TYPE_CATALOG);

const isCatalog = (entry) =>
  CATALOG_EXPANDED_TYPE ===
  entry.getMetadata().findFirstValue(entry.getResourceURI(), 'rdf:type');

export { isCatalog };

/**
 * @param {store/Context} catalogContext
 * @return {(boolean|undefined)}
 */
const isCatalogPublic = async (catalogContext) => {
  const contextEntry = await catalogContext.getEntry();
  return contextEntry.isPublic();
};

export { isCatalogPublic };
