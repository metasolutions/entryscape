import { namespaces as ns, Graph } from '@entryscape/rdfjson';
import { Entry } from '@entryscape/entrystore-js';
import { localize } from 'commons/locale';
import { itemStore } from 'commons/rdforms/itemstore';
import registry from 'commons/registry';
import { entrystore, entrystoreUtil } from 'commons/store';
import { getDescription, getLabel } from 'commons/util/rdfUtils';
import { getShortDate } from 'commons/util/date';
import { runAsyncInSequence } from 'commons/util/async';

// UTILS
const getChoiceLabels = (entry, property, choiceTemplateId) => {
  const choices = itemStore.getItem(choiceTemplateId).getChoices();

  return entry
    .getMetadata()
    .find(entry.getResourceURI(), property)
    .map((statement) => statement.getValue())
    .map((uri) => choices.find((choice) => choice.value === uri))
    .filter((choice) => choice)
    .map((choice) => localize(choice.label));
};
const searchRelatedSourceEntry = (entry, type) => {
  const context = entry.getContext();

  return entrystore
    .newSolrQuery()
    .rdfType(ns.expand(type))
    .uriProperty('dcterms:source', entry.getResourceURI())
    .context(context.getResourceURI())
    .getEntries();
};
// END UTILS

/**
 * Get a the title of an Entry
 *
 * @param  {Entry} An Entry
 * @returns {string}
 */
export const getTitle = getLabel;

/**
 * Get the dcat:description of an Entry`
 *
 * @param  {Entry} An Entry
 * @returns {string}
 */
export { getDescription };

/**
 * Get the last modified date of an Entry
 *
 * @param  {Entry} entry
 * @returns {Date}
 */
export const getModifiedDate = (entry) =>
  entry.getEntryInfo().getModificationDate();

/**
 * Get the last modified date of an Entry
 *
 * @param  {Entry} entry
 * @returns {string}
 */
export const getShortModifiedDate = (entry) =>
  getShortDate(entry.getEntryInfo().getModificationDate());

/**
 * Get all the theme labels for a dataset
 *
 * @param  {Entry} entry - Dataset entry
 * @returns {string}
 */
export const getThemeLabels = (entry) =>
  getChoiceLabels(entry, 'dcat:theme', 'dcat:theme');

/**
 * Get an entry's theme taxonomy URI
 *
 * @param  {Entry} entry
 * @returns {string}
 */
export const getThemeTaxonomy = (entry) =>
  entry.getMetadata().findFirstValue(null, 'dcat:themeTaxonomy');

/**
 * Get the labels of the default DCAT theme vocabulary
 *
 * @returns {object[]}
 */
export const getDefaultThemeLabels = () =>
  itemStore.getItem('dcat:theme').getChoices();

/**
 * Get the dcat:accessURL property of an entry
 *
 * @param  {Entry} entry
 * @returns {string}
 */
export const getAccessURI = (entry) => {
  const metadata = entry.getMetadata();
  const resourceURI = entry.getResourceURI();
  return metadata.findFirstValue(resourceURI, ns.expand('dcat:accessURL'));
};

/**
 * Get the dcat:downloadURL of an entry
 *
 * @param  {Entry} entry
 * @returns {string}
 */
export const getDownloadURI = (entry) => {
  const metadata = entry.getMetadata();
  const resourceURI = entry.getResourceURI();
  return metadata.findFirstValue(resourceURI, ns.expand('dcat:downloadURL'));
};

/**
 * Get the esterms:basedOn property of an entry
 *
 * @param {Entry} entry
 * @returns {string}
 */
export const getBasedOnProperty = (entry) =>
  entry.getMetadata().findFirstValue(entry.getResourceURI(), 'esterms:basedOn');

/**
 * Get all file URLs from an entry (dcat:downloadURL)
 *
 * @param  {Entry} An Entry
 * @returns {string[]}
 */

export const getFileEntries = (entry) => {
  const metadata = entry.getMetadata();
  const resourceURI = entry.getResourceURI();
  return metadata.find(resourceURI, ns.expand('dcat:downloadURL'));
};

/**
 * Get the parent catalog of a Dataset
 *
 * @param {Entry} entry - Dataset entry
 * @returns {Promise}
 */
export const getParentCatalogEntry = (entry) =>
  entrystoreUtil.getEntryByType('dcat:Catalog', entry.getContext());

/**
 *
 * @param {string} contributorEntryURI
 * @returns {Promise}
 */
const getContributor = (contributorEntryURI) => {
  return entrystore.getEntry(contributorEntryURI).catch(() => {
    console.log('Contributor not found. User entry no longer exists.');
  });
};

/**
 *
 * @param {Entry} entry
 * @returns {Promise}
 */
export const getContributors = (entry) => {
  const entryInfo = entry.getEntryInfo();
  const contributorsEntryURIs = entryInfo
    .getContributors()
    .map((contributorURI) => entrystore.getEntryURIFromURI(contributorURI));

  const asyncContributorCallbacks = contributorsEntryURIs.map(
    (contributorEntryURI) => () => getContributor(contributorEntryURI)
  );

  /**
   * with current global error handler, the calls must be run in sequnce to prevent
   * triggering error dialog for entrystore.getEntry if user no longer exists.
   */
  return runAsyncInSequence(asyncContributorCallbacks);
};

export const getSuggestions = async (entry, context) => {
  const referencedDatasetEntries = await registry
    .getEntryStore()
    .newSolrQuery()
    .rdfType('esterms:Suggestion')
    .uriProperty('dcterms:references', entry.getResourceURI())
    .context(context)
    .getEntries();

  return referencedDatasetEntries;
};

export const getIdeas = (entry) =>
  searchRelatedSourceEntry(entry, 'esterms:Idea');

export const getShowcases = (entry) =>
  searchRelatedSourceEntry(entry, 'esterms:Result');

export const getCatalogIsPublic = (entry) =>
  entry
    .getContext()
    .getEntry()
    .then((contextEntry) => contextEntry.isPublic());

export const languageUriToIsoCode = (languageURI) => {
  switch (languageURI) {
    case 'http://publications.europa.eu/resource/authority/language/SWE':
      return 'sv';
    case 'http://publications.europa.eu/resource/authority/language/DEU':
      return 'de';
    case 'http://publications.europa.eu/resource/authority/language/ENG':
    default:
      return 'en';
  }
};

/**
 *
 * @param {Graph} graph
 * @param {string} resourceURI
 * @param {string} rdfType
 * @returns {boolean}
 */
export const isType = (graph, resourceURI, rdfType) => {
  const rdfTypes = graph
    .find(resourceURI, 'rdf:type')
    .map((statement) => statement.getValue());
  return rdfTypes.includes(ns.expand(rdfType));
};

/**
 *
 * @param {Graph} creatorEntry
 * @returns {string}
 */
export const getCreatorName = async (creatorEntry) => {
  const userResourceUri = creatorEntry.getEntryInfo().getCreator();
  const userEntryURI = entrystore.getEntryURIFromURI(userResourceUri);
  const userEntry = await entrystore.getEntry(userEntryURI);
  return getLabel(userEntry)?.trim();
};
