import { LIST_ACTION_EDIT } from 'commons/components/EntryListView/actions';
import esadUserNLS from 'admin/nls/esadUser.nls';
import escoListNLS from 'commons/nls/escoList.nls';
import escoPlaceholderNLS from 'commons/nls/escoPlaceholder.nls';
import CreateUserDialog from './dialogs/CreateUserDialog';
import { replaceNameWithFirstAndLastName } from '../utils/user';

const handleUserEntryEdit = (entry) => {
  replaceNameWithFirstAndLastName(entry);
  return entry.commitMetadata();
};

export const nlsBundles = [escoListNLS, esadUserNLS, escoPlaceholderNLS];

export const listActions = [
  {
    id: 'create',
    Dialog: CreateUserDialog,
    labelNlsKey: 'createButton',
    tooltipNlsKey: 'actionTooltip',
  },
];

export const USER_EDIT_ACTION = {
  ...LIST_ACTION_EDIT,
  formTemplateId: 'esc:User',
  onEntryEdit: handleUserEntryEdit,
};
