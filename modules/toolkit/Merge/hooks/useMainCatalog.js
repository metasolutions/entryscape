import { useEffect, useState } from 'react';
import { useClipboardGraph } from 'commons/contexts/ClipboardGraphContext';
import { useTranslation } from 'commons/hooks/useTranslation';
import esreMergeNLS from 'toolkit/nls/esreMerge.nls';
import esreCatalogDetectNLS from 'toolkit/nls/esreCatalogDetect.nls';
import esreSourceNLS from 'toolkit/nls/esreSource.nls';
import { getCountByType, validateCount } from '../utils/CatalogDetect';

const useMainCatalog = () => {
  const [mainCatalog, setMainCatalog] = useClipboardGraph();
  const t = useTranslation([esreMergeNLS, esreSourceNLS, esreCatalogDetectNLS]);
  const [countByType, setCountByType] = useState(null);
  const [errorMessage, setErrorMessage] = useState(null);
  const [isMounted, setIsMounted] = useState(false); // workaround to make sure graph value is available

  useEffect(() => {
    if (!isMounted) {
      setIsMounted(true);
    }

    if (!mainCatalog || mainCatalog?.isEmpty()) {
      setErrorMessage(t('noRDF'));
      return;
    }

    const count = getCountByType(mainCatalog);
    const error = validateCount(count);

    if (error) {
      setErrorMessage(t(...error));
    }
    setCountByType(count);
  }, [mainCatalog, isMounted, t]);

  return [countByType, errorMessage, mainCatalog, setMainCatalog];
};

export default useMainCatalog;
