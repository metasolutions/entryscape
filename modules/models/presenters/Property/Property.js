import PropTypes from 'prop-types';
import { Presenter } from 'commons/components/forms/presenters';
import { graphPropType } from 'commons/util/entry';
import { getPropertyValue } from 'models/utils/property';

export const Property = ({ graph, resourceURI, ...props }) => {
  return (
    <Presenter
      direct={false}
      graph={graph}
      resourceURI={resourceURI}
      getValue={getPropertyValue}
      {...props}
    />
  );
};

Property.propTypes = {
  graph: graphPropType,
  resourceURI: PropTypes.string,
  inline: PropTypes.bool,
  children: PropTypes.node,
};
