import React from 'react';

export const renderHtmlString = (htmlString) => (
  <span dangerouslySetInnerHTML={{ __html: htmlString }} />
);

/**
 * Parses a template string into an array that can be rendered byr React. The
 * parser replaces placeholders, by matching keys of the properties object.
 *
 * @param {string} template
 * @param {object} properties
 * @returns {any[]}
 */
export const parseTemplate = (template, properties) => {
  const regex = /\$\{(\w+)\}|\$(\d+)/g; // match both $ and ${} placeholders
  const parts = [];
  let lastIndex = 0;

  template.replace(regex, (match, ...args) => {
    const index = args[args.length - 2]; // `index` is always second last argument

    // add text preceeding the match
    if (index > lastIndex) {
      parts.push(template.slice(lastIndex, index));
    }

    // find the first value capturing group for the match
    const key = args.slice(0, -2).find((group) => group !== undefined);

    if (properties[key]) {
      parts.push(<React.Fragment key={key}>{properties[key]}</React.Fragment>);
    } else {
      parts.push(match); // Keep unmatched as is
    }

    lastIndex = index + match.length;
  });

  // remaining text after the last match
  if (lastIndex < template.length) {
    parts.push(template.slice(lastIndex));
  }

  return parts;
};
