import { useCallback, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { Entry } from '@entryscape/entrystore-js';
import { useMapContext } from 'commons/components/Map/hooks/useMapContext';
import ChartPlaceholder from 'catalog/datasets/DatasetOverview/VisualizationForm/ChartPlaceholder';
// eslint-disable-next-line max-len
import MapControlField from 'catalog/datasets/DatasetOverview/VisualizationForm/WmsForm/MapControlField';
import useAsync from 'commons/hooks/useAsync';
import { useTranslation } from 'commons/hooks/useTranslation';
// eslint-disable-next-line max-len
import { useVisualizationState } from 'catalog/datasets/DatasetOverview/hooks/useVisualizationState';
import { useOpenlayers } from 'commons/components/Map';
import {
  LayerLegendControl,
  LayerSwitcherControl,
} from 'commons/components/Map/controls';
import FormSection from 'catalog/datasets/DatasetOverview/VisualizationForm/FormSection';
import escoListNLS from 'commons/nls/escoList.nls';
import escaDatasetNLS from 'catalog/nls/escaDataset.nls';
import escaVisualizationNLS from 'catalog/nls/escaVisualization.nls';
import escoGeoMapNLS from 'commons/nls/escoGeoMap.nls';
import { Box } from '@mui/material';
import ScaleLineControl from 'commons/components/Map/controls/ScaleLineControl';
import AttributionControl from 'commons/components/Map/controls/AttributionControl';
import ControlGroup from 'commons/components/Map/controls/ControlGroup';
import VisualizationMapService from '../../VisualizationMapService';

const nlsBundles = [
  escoListNLS,
  escaDatasetNLS,
  escaVisualizationNLS,
  escoGeoMapNLS,
];

const WmsForm = ({ mapOptions }) => {
  const [{ map }] = useMapContext();
  const { fieldsState, updateFieldsState, isBroken } = useVisualizationState();
  const translate = useTranslation(nlsBundles);
  const [isFirstLoad, setIsFirstLoad] = useState(true);

  const handleViewExtentChange = useCallback(
    ({ frameState }) => {
      const { extent: viewExtent } = frameState;
      updateFieldsState(
        {
          initialView: viewExtent,
        },
        isFirstLoad
      );
      if (isFirstLoad) setIsFirstLoad(false);
    },
    [updateFieldsState, isFirstLoad]
  );

  return (
    <>
      <FormSection title={translate('vizDialogMapControls')}>
        <Box sx={{ flexDirection: 'column', display: 'flex' }}>
          <MapControlField
            property="legendControl"
            label={translate('vizDialogMapLegendControlLabel')}
          />
          <MapControlField
            property="scaleLineControl"
            label={translate('vizDialogMapLegendScaleLineLabel')}
          />
        </Box>
      </FormSection>
      <FormSection title={translate('vizDialogPreview')}>
        {isBroken ? (
          <ChartPlaceholder
            label={translate('brokenVizPlaceholderLabel')}
            labelClass="escaChartPlaceHolderLabel--error"
            chartType="wms"
          />
        ) : (
          <div className="escaVisualizationFormSection__mapContainer">
            <VisualizationMapService
              mapOptions={mapOptions}
              onViewExtentChange={handleViewExtentChange}
            >
              {map ? (
                <>
                  <LayerSwitcherControl
                    handleChange={updateFieldsState}
                    layers={fieldsState.layers}
                  />
                  {fieldsState.legendControl === 'true' ? (
                    <LayerLegendControl layers={fieldsState.layers} />
                  ) : null}
                  <ControlGroup
                    position="bottom-right"
                    renderControls={(target) => (
                      <>
                        <AttributionControl target={target} />
                        {fieldsState.scaleLineControl === 'true' ? (
                          <ScaleLineControl target={target} />
                        ) : null}
                      </>
                    )}
                  />
                </>
              ) : null}
            </VisualizationMapService>
          </div>
        )}
      </FormSection>
    </>
  );
};

WmsForm.propTypes = {
  mapOptions: PropTypes.shape({}),
};

const WmsFormWithMapOptions = ({ translate, visualizationEntry }) => {
  const { getWmsData } = useOpenlayers();
  const { data: mapOptions, runAsync, isLoading } = useAsync();
  const { selectedSourceChoice, addErrors, initialFields, updateFieldsState } =
    useVisualizationState();
  const { sourceURI } = selectedSourceChoice;

  // Load map options for map component. If a new visualization is created or a
  // different map service is used, a get capabilities request is made to get
  // the map options.
  useEffect(() => {
    const getMapOptions = async () => {
      const isSavedVisualization =
        initialFields && initialFields.sourceURI === sourceURI;
      let mapFields = initialFields;

      if (!isSavedVisualization) {
        const { error, ...wmsData } = await getWmsData(sourceURI);
        if (error) {
          addErrors({ sourceURI: translate(error) });
          return;
        }
        updateFieldsState(wmsData);
        mapFields = wmsData;
      }

      const { layers, extent, wmsBaseURI, initialView, basemap } = mapFields;
      return {
        layers: layers.map((layerOptions) => ({
          ...layerOptions,
          url: wmsBaseURI,
        })),
        extent,
        initialView,
        basemapOptions: {
          visible: basemap !== 'false',
        },
        wmsBaseURI,
      };
    };

    if (!getWmsData) return;

    runAsync(getMapOptions());
  }, [
    getWmsData,
    runAsync,
    sourceURI,
    visualizationEntry,
    initialFields,
    updateFieldsState,
    addErrors,
    translate,
  ]);

  if (!getWmsData || isLoading) return null;

  return <WmsForm mapOptions={mapOptions} />;
};

WmsFormWithMapOptions.propTypes = {
  visualizationEntry: PropTypes.instanceOf(Entry),
  translate: PropTypes.func,
};

export default WmsFormWithMapOptions;
