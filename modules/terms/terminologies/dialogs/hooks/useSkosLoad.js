import {
  STATUS_PROGRESS,
  STATUS_DONE,
  STATUS_FAILED,
} from 'commons/hooks/useTasksList';
import { converters, Graph } from '@entryscape/rdfjson';
import { readFileAsText } from 'commons/util/file';
import { entrystore } from 'commons/store';
import { addIgnore } from 'commons/errors/utils/async';
import { getConceptSchemeURI, getUriSpace } from '../utils/skos';

/**
 *
 * @param {string} url
 * @param {Function} translate
 * @returns {Promise.<string>}
 */
export const linkUpload = (url, translate) => {
  addIgnore('loadViaProxy', true, true);
  return entrystore.loadViaProxy(url, 'application/rdf+xml').catch((err) => {
    const messageNls =
      err.response?.status === 504
        ? 'noResponseFromLink'
        : 'loadFromLinkProblem';
    throw Error(translate(messageNls));
  });
};

/**
 *
 * @param {HTMLInputElement} inputElement
 * @returns {Promise.<string>}
 */
export const fileUpload = (inputElement) => {
  const file = inputElement.files.item(0);
  return readFileAsText(file).catch((error) => {
    throw new Error(error);
  });
};

/**
 * Convert data to a graph or throw an error
 *
 * @param {string} data
 * @returns {Graph|object} graph or error obj
 */
const convertDataToGraph = async (data) => {
  const { graph, ...rest } = await converters.detect(data);
  if (graph) {
    return graph;
  }

  throw Error(rest.error);
};

/**
 * Get a graph from data and check for a concept scheme uri
 *
 * @param {string} data
 * @param {Function} translate
 * @returns {{graph: (*|Graph), conceptSchemeURI: (*|string)}}
 */
export const analyseData = async (data, translate) => {
  const graph = await convertDataToGraph(data);
  const conceptSchemeURI = getConceptSchemeURI(graph, translate);
  const uriSpace = getUriSpace(graph, conceptSchemeURI);
  if (uriSpace) graph.add(conceptSchemeURI, 'void:uriSpace', uriSpace);

  return { graph, conceptSchemeURI, uriSpace };
};

const skosLoad =
  ({ t, updateTask, isLink, link, file, getConfirmationDialog, external }) =>
  async () => {
    updateTask('upload', { status: STATUS_PROGRESS });

    let data;
    try {
      const upload = isLink
        ? () => linkUpload(link, t)
        : () => fileUpload(file);
      data = await upload();
      updateTask('upload', { status: STATUS_DONE });
    } catch ({ message }) {
      updateTask('upload', { status: STATUS_FAILED, message });
      return;
    }

    updateTask('analysis', { status: STATUS_PROGRESS });
    try {
      const analysis = await analyseData(data, t);
      const conceptStatements = analysis.graph.find(
        null,
        'rdf:type',
        'skos:Concept'
      );
      if (!conceptStatements.length) throw new Error(t('noConcepts'));
      if (!analysis.uriSpace && !external) throw new Error(t('noUriSpace'));

      let proceed = true;
      if (conceptStatements.length > 1000) {
        proceed = await getConfirmationDialog();
      }
      if (!proceed) return;

      updateTask('analysis', { status: STATUS_DONE });
      return analysis;
    } catch (err) {
      updateTask('analysis', { status: STATUS_FAILED, message: err.message });
    }
  };

export default skosLoad;
