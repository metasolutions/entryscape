import { getGroupWithHomeContext } from 'commons/util/store';
import useAsync, { PENDING } from 'commons/hooks/useAsync';
import useGetMainDialog from 'commons/hooks/useGetMainDialog';
import { useUserState } from 'commons/hooks/useUser';
import { useTranslation } from 'commons/hooks/useTranslation';
import escaDatasetNLS from 'catalog/nls/escaDataset.nls';
import { useSnackbar, ERROR } from 'commons/hooks/useSnackbar';
import {
  getIsAllowedPublisher,
  getHasApiDistributions,
  getHasDistributions,
  publishDataset,
  unpublishDataset,
} from '../utils/publish';

const useDatasetPublish = ({ entry, datasetStatus, onChange }) => {
  const translate = useTranslation(escaDatasetNLS);
  const { getConfirmationDialog, getAcknowledgeDialog } = useGetMainDialog();
  const { userEntry } = useUserState();
  const { isSeries, isPublished } = datasetStatus;
  const { status, runAsync } = useAsync();
  const [addSnackbar] = useSnackbar();

  const unpublish = async (groupEntry) => {
    const hasApiDistributions = await getHasApiDistributions(entry);
    if (hasApiDistributions) {
      const proceed = await getConfirmationDialog({
        content: translate('apiExistsToUnpublishDataset'),
      });
      if (!proceed) return isPublished;
    }

    return unpublishDataset(entry, groupEntry);
  };

  const publish = async () => {
    const skipDistributionCheck = getHasDistributions(entry) || isSeries;

    if (!skipDistributionCheck) {
      const proceed = await getConfirmationDialog({
        content: translate('confirmPublishWithoutDistributions'),
        rejectLabel: translate('cancelPublishWithoutDistributions'),
        affirmLabel: translate('proceedPublishWithoutDistributions'),
      });
      if (!proceed) return isPublished;
    }
    return publishDataset(entry);
  };

  const togglePublish = async () => {
    if (!getIsAllowedPublisher(userEntry)) {
      await getAcknowledgeDialog({
        content: translate(
          isPublished ? 'unpublishProhibited' : 'publishProhibited'
        ),
      });
      return isPublished;
    }

    const groupEntry = await getGroupWithHomeContext(entry.getContext());

    if (!groupEntry.canAdministerEntry()) {
      await getAcknowledgeDialog({
        content: translate('datasetSharingNoAccess'),
      });
      return isPublished;
    }

    if (isPublished) {
      return unpublish(groupEntry);
    }
    return publish();
  };

  const toggleIsPublished = () =>
    runAsync(
      togglePublish()
        .then((newValue) => {
          if (newValue === isPublished) return; // cancelling
          onChange({ isPublished: newValue, isInternallyPublished: newValue });
          const successNls = isSeries
            ? 'publishDatasetSeriesSuccess'
            : 'publishDatasetSuccess';
          addSnackbar({
            message: translate(`${newValue ? '' : 'un'}${successNls}`),
          });
        })
        .catch((error) => {
          console.error(error);
          const failNls = isSeries
            ? 'publishDatasetSeriesFail'
            : 'publishDatasetFail';
          addSnackbar({
            message: translate(`${isPublished ? 'un' : ''}${failNls}`),
            type: ERROR,
          });
        })
    );

  return { toggleIsPublished, isLoading: status === PENDING };
};

export default useDatasetPublish;
