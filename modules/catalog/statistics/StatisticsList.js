import React from 'react';
import PropTypes from 'prop-types';
import { ListItem, ListItemButton, ListItemText, Grid } from '@mui/material';
import FileIcon from '@mui/icons-material/InsertDriveFile';
import ApiIcon from '@mui/icons-material/SettingsApplications';
import Pagination from 'commons/components/Pagination';
import { useTranslation } from 'commons/hooks/useTranslation';
import escaStatistics from 'catalog/nls/escaStatistics.nls';
import { RESOURCE_TYPE_FILE } from './utils/distribution';
import './index.scss';

const StatisticsListHeader = () => {
  const t = useTranslation(escaStatistics);
  return (
    <ListItem className="escaStatisticsListHeader" key="header">
      <Grid
        container
        spacing={4}
        justifyContent="flex-start"
        alignItems="center"
      >
        <Grid
          container
          item
          xs={1}
          className="escaStatisticsListHeader__item"
        />
        <Grid container item xs={4} className="escaStatisticsListHeader__item">
          {t('tabHeaderTitle')}
        </Grid>
        <Grid container item xs={3} className="escaStatisticsListHeader__item">
          {t('tabHeaderDataset')}
        </Grid>

        <Grid
          container
          item
          xs={2}
          className="escaStatisticsListHeader__item"
          justifyContent="center"
        >
          {t('tabHeaderFormat')}
        </Grid>
        <Grid
          container
          item
          xs={2}
          className="escaStatisticsListHeader__item"
          justifyContent="center"
        >
          {t('tabHeaderDownload')}
        </Grid>
      </Grid>
    </ListItem>
  );
};

const StatisticsList = ({
  paginatedData,
  selectedURI,
  rowsPerPage,
  page,
  count,
  handleSelectionChange,
}) => {
  const translate = useTranslation(escaStatistics);

  const handleListItemClick = (uri) => {
    const newSelectionURI = uri === selectedURI ? '' : uri;
    handleSelectionChange({ selectedURI: newSelectionURI });
  };

  const handlePageChange = (_event, newPage) => {
    handleSelectionChange({ page: newPage, selectedURI: '' });
  };

  if (!paginatedData.length) return null;

  return (
    <div className="escaStatisticsList">
      <StatisticsListHeader />
      {paginatedData.map((dataItem, index) => {
        const { distributionName, resourceName } = dataItem;
        const fileOrDocumentFile =
          dataItem.type === RESOURCE_TYPE_FILE ||
          dataItem.type === 'documentFile';
        const apiCallTitle = distributionName
          ? translate('apiCallTitle', distributionName)
          : translate('apiCallTitleFallback');
        const title = fileOrDocumentFile ? resourceName : apiCallTitle;

        return (
          <ListItemButton
            // eslint-disable-next-line react/no-array-index-key
            key={`${dataItem.uri}-${index}`}
            classes={{
              root:
                dataItem.uri === selectedURI
                  ? 'escaStatisticsListItem--selected'
                  : 'escaStatisticsListItem',
            }}
            onClick={() => handleListItemClick(dataItem.uri)}
          >
            <Grid container spacing={4} style={{ alignItems: 'center' }}>
              <Grid container item xs={1}>
                {fileOrDocumentFile ? <FileIcon /> : <ApiIcon />}
              </Grid>

              <Grid container item xs={4}>
                <ListItemText
                  classes={{ root: 'escaStatisticsListItemText' }}
                  primary={title}
                  primaryTypographyProps={{
                    classes: {
                      root: 'escaStatisticsListItemText__primary',
                    },
                  }}
                />
              </Grid>
              <Grid container item xs={3}>
                {dataItem.datasetName}
              </Grid>
              <Grid container item xs={2} justifyContent="center">
                {fileOrDocumentFile
                  ? (dataItem.abbrevFormat || dataItem.format).toUpperCase()
                  : 'API'}
              </Grid>
              <Grid container item xs={2} justifyContent="center">
                {dataItem.downloadCount}
              </Grid>
            </Grid>
          </ListItemButton>
        );
      })}
      {count && (
        <Pagination
          count={count}
          page={page}
          onPageChange={handlePageChange}
          rowsPerPage={rowsPerPage}
        />
      )}
    </div>
  );
};

StatisticsList.propTypes = {
  paginatedData: PropTypes.arrayOf(PropTypes.shape({})),
  handleSelectionChange: PropTypes.func,
  selectedURI: PropTypes.string,
  rowsPerPage: PropTypes.number,
  count: PropTypes.number,
  page: PropTypes.number,
};

export { StatisticsList, StatisticsListHeader };
