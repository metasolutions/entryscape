import {
  AutoAwesomeMotionSharp as SeriesIcon,
  Edit as EditIcon,
  DeleteOutline as RemoveIcon,
  GetApp as DownloadIcon,
  Code as EmbedIcon,
  Group as GroupIcon,
  FolderShared as SharingSettingsIcon,
  Bookmark as RevisionsIcon,
  PersonAdd as EnableUserIcon,
  PersonAddDisabled as DisableUserIcon,
  LockOpen as LockOpenIcon,
  AccountCircle as UserIcon,
  Add as AddIcon,
  Publish as ImportIcon,
  Link as LinkIcon,
  InsertDriveFile as ReplaceIcon,
  Assessment as StatisticsIcon,
  Panorama as PreviewIcon,
  FileCopy as ManageFilesIcon,
  DatasetLinked as DatasetLinkedIcon,
  CheckBox as CheckBoxIcon,
  Comment as CommentsIcon,
  Archive as ArchiveIcon,
  Unarchive as UnarchiveIcon,
  Refresh as RefreshIcon,
  AddToPhotos as CreateFromDatasetTemplateIcon,
  Autorenew as ReindexIcon,
  ArrowCircleUp as UpgradeIcon,
  ArrowCircleDown as DowngradeIcon,
  Sync as SyncIcon,
  Favorite as FavoriteIcon,
  HeartBroken as HeartBrokenIcon,
  LinkOff as LinkOffIcon,
  ContentCopy as CopyIcon,
  Info as InfoIcon,
} from '@mui/icons-material';
import SvgIcon from '@mui/material/SvgIcon';

import {
  ACTION_EDIT_ID,
  ACTION_INFO_ID,
  ACTION_REMOVE_ID,
  ACTION_REVISIONS_ID,
} from './actionIds';

const styles = {
  fontSize: '24px',
};

const icons = {
  'manage-series': <SeriesIcon style={styles} />,
  create: <AddIcon style={styles} />,
  'add-file': <AddIcon style={styles} />,
  'create-from-dataset-template': (
    <CreateFromDatasetTemplateIcon style={styles} />
  ),
  import: <ImportIcon style={styles} />,
  [ACTION_EDIT_ID]: <EditIcon style={styles} />,
  [ACTION_REMOVE_ID]: <RemoveIcon style={styles} />,
  download: <DownloadIcon style={styles} />,
  embed: <EmbedIcon style={styles} />,
  'sharing-settings': <SharingSettingsIcon style={styles} />,
  [ACTION_REVISIONS_ID]: <RevisionsIcon style={styles} />,
  [ACTION_INFO_ID]: <InfoIcon style={styles} />,
  'api-info': <InfoIcon style={styles} />,
  'edit-name': <LinkIcon style={styles} />,
  'activate-api': <LinkIcon style={styles} />,
  'link-entry': <LinkIcon style={styles} />,
  'refresh-api': <RefreshIcon style={styles} />,
  username: <UserIcon style={styles} />,
  password: <LockOpenIcon style={styles} />,
  groups: <GroupIcon style={styles} />,
  'enable-user': <EnableUserIcon style={styles} />,
  'disable-user': <DisableUserIcon style={styles} />,
  'replace-file': <ReplaceIcon style={styles} />,
  'create-dataset': <DatasetLinkedIcon style={styles} />,
  checklist: <CheckBoxIcon style={styles} />,
  comments: <CommentsIcon style={styles} />,
  archive: <ArchiveIcon style={styles} />,
  unarchive: <UnarchiveIcon style={styles} />,
  statistics: <StatisticsIcon style={styles} />,
  preview: <PreviewIcon style={styles} />,
  'manage-files': <ManageFilesIcon style={styles} />,
  reindex: <ReindexIcon style={styles} />,
  upgradeToPremium: <UpgradeIcon style={styles} />,
  downgradeFromPremium: <DowngradeIcon style={styles} />,
  update: <SyncIcon style={styles} />,
  setDefault: <FavoriteIcon style={styles} />,
  unsetDefault: <HeartBrokenIcon style={styles} />,
  disconnect: <LinkOffIcon style={styles} />,
  'replace-link': <LinkIcon style={styles} />,
  'replace-patterned-link': <LinkIcon style={styles} />,
  'replace-uri': <LinkIcon style={styles} />,
  copy: <CopyIcon style={styles} />,
};

export const getIconFromActionId = (actionId) => {
  if (actionId in icons) return icons[actionId];
  return <EditIcon style={styles} />;
};

export const SearchAndCreateIcon = (props) => {
  return (
    <SvgIcon {...props}>
      <path
        fill="currentColor"
        // eslint-disable-next-line max-len
        d="M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z"
      />
      <path
        fill="currentColor"
        d="M 24,6 H 21 V 9 H 19 V 6 H 16 V 4 h 3 V 1 h 2 v 3 h 3 z"
      />
    </SvgIcon>
  );
};
