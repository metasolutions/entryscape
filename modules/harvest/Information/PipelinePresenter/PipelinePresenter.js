import PropTypes from 'prop-types';
import { List, ListItem, ListItemText } from '@mui/material';
import { Resource } from '@entryscape/entrystore-js';
import { utils } from '@entryscape/rdforms';
import config from 'config';
import { useTranslation } from 'commons/hooks/useTranslation';
import esrePipeResultListDialogNLS from 'harvest/nls/esrePipelineResultListDialog.nls';
import esreHarvestNLS from 'harvest/nls/esreHarvest.nls';
import InformationBlock from 'commons/components/InformationBlock';
import TransformsPresenter from '../TransformsPresenter';

const PipelinePresenter = ({
  organizationData: allData,
  isDcat,
  showOnlyMetadata,
}) => {
  const translate = useTranslation([
    esrePipeResultListDialogNLS,
    esreHarvestNLS,
  ]);
  const {
    errors: _errors,
    isPublic: _isPublic,
    projectType: _projectType,
    transforms,
    sourceUrl,
    ...data
  } = allData;
  const visibleData = isDcat ? { ...data, sourceUrl } : data;

  const pipelineDataNLS = {
    title: translate('cPTitle'),
    description: translate('cPDescription'),
    username: translate('cPUsernameLabel'),
    psi: translate('cpWebpage'),
    orgId: translate('cpOrgId'),
    sourceUrl: translate('epUrl'),
  };

  const textConfig = config.get('registry.contactText');
  const contactText = textConfig
    ? utils.getLocalizedValue(textConfig).value
    : '';

  const headerConfig = config.get('registry.contactTextHeader');
  const contactTextHeader = headerConfig
    ? utils.getLocalizedValue(headerConfig).value
    : '';

  return (
    <>
      <List aria-label={translate('organizationInformation')}>
        {Object.keys(visibleData)
          .filter((key) => visibleData[key] && key !== 'recipe')
          .map((key) => (
            <ListItem classes={{ root: 'esreInformation__ListItem' }} key={key}>
              <ListItemText
                classes={{ primary: 'esreInformation__Label' }}
                primary={pipelineDataNLS[key]}
              />
              <ListItemText primary={visibleData[key]} />
            </ListItem>
          ))}
      </List>
      {showOnlyMetadata ? null : (
        <>
          <InformationBlock
            header={contactTextHeader}
            info={contactText}
            className="esreInformation__infoBlock"
          />
          {!isDcat ? <TransformsPresenter transforms={transforms} /> : null}
        </>
      )}
    </>
  );
};

PipelinePresenter.propTypes = {
  showOnlyMetadata: PropTypes.bool,
  organizationData: PropTypes.shape({
    title: PropTypes.string,
    description: PropTypes.string,
    username: PropTypes.string,
    psi: PropTypes.string,
    orgId: PropTypes.string,
    sourceUrl: PropTypes.string,
    errors: PropTypes.shape({}),
    isPublic: PropTypes.bool,
    transforms: PropTypes.arrayOf(PropTypes.shape({})),
    projectType: PropTypes.string,
  }),
  pipelineResource: PropTypes.instanceOf(Resource),
  isDcat: PropTypes.bool,
};

export default PipelinePresenter;
