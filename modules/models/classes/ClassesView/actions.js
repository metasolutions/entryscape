import escoListNLS from 'commons/nls/escoList.nls';
import esmoClassesNLS from 'models/nls/esmoClasses.nls';
import esmoCommonsNLS from 'models/nls/esmoCommons.nls';
import escoDialogsNLS from 'commons/nls/escoDialogs.nls';
import escoFiltersNLS from 'commons/nls/escoFilters.nls';
import ImportEntriesDialog from 'commons/components/common/dialogs/ImportEntriesDialog';
import { RDF_TYPE_CLASS, RDF_TYPE_MODEL } from 'models/utils/ns';
import { withListRefresh } from 'commons/components/ListView';
import CreateEntryDialog from '../../dialogs/CreateEntryDialog';

export const nlsBundles = [
  escoListNLS,
  esmoClassesNLS,
  esmoCommonsNLS,
  escoDialogsNLS,
  escoFiltersNLS,
];

export const listActions = [
  {
    id: 'link-entry',
    Dialog: withListRefresh(ImportEntriesDialog, 'afterCreateEntry'),
    labelNlsKey: 'reuseHeader',
    tooltipNlsKey: 'reuseTitle',
    rdfType: RDF_TYPE_CLASS,
    contextRdfType: RDF_TYPE_MODEL,
  },
  {
    id: 'create',
    Dialog: CreateEntryDialog,
    labelNlsKey: 'createButtonLabel',
    tooltipNlsKey: 'createClassTitle',
    rdfType: 'rdfs:Class',
  },
];
