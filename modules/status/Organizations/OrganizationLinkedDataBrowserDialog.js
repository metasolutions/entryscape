import PropTypes from 'prop-types';
import React, { useCallback, useEffect } from 'react';
import { Entry } from '@entryscape/entrystore-js';
import { nlsBundlesPropType } from 'commons/hooks/useTranslation';
import LinkedDataBrowserDialog from 'commons/components/LinkedDataBrowserDialog';
import PipelinePresenter from 'harvest/Information/PipelinePresenter';
import { fetchOrganizationData } from 'harvest/util';
import useAsync from 'commons/hooks/useAsync';

const OrganizationLinkedDataBrowserDialog = ({
  entry,
  nlsBundles,
  ...rest
}) => {
  const {
    data: { entityType, organizationData },
    runAsync,
    status,
  } = useAsync({ data: {} });
  const isDcat = entityType?.get('tags')?.includes('dcat');

  useEffect(() => {
    if (status !== 'idle') return;
    const contextId = entry.getContext().getId();
    runAsync(fetchOrganizationData(contextId));
  }, [runAsync, status, entry]);

  const renderPresenter = useCallback(
    ({ stackIndex }) =>
      organizationData && stackIndex === 0 ? (
        <PipelinePresenter
          organizationData={organizationData}
          isDcat={isDcat}
          showOnlyMetadata
        />
      ) : null,
    [isDcat, organizationData]
  );

  return (
    <LinkedDataBrowserDialog
      {...rest}
      entry={entry}
      renderPresenter={renderPresenter}
    />
  );
};

OrganizationLinkedDataBrowserDialog.propTypes = {
  entry: PropTypes.instanceOf(Entry),
  nlsBundles: nlsBundlesPropType,
};

export default OrganizationLinkedDataBrowserDialog;
