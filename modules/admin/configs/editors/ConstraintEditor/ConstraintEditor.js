import { useState } from 'react';
import PropTypes from 'prop-types';
import { graphPropType } from 'commons/util/entry';
import ArrowDownwardIcon from '@mui/icons-material/ArrowDownward';
import ArrowUpwardIcon from '@mui/icons-material/ArrowUpward';
import { Grid } from '@mui/material';
import { useTranslation } from 'commons/hooks/useTranslation';
import * as ns from 'models/utils/ns';
import {
  URI,
  Field,
  Fields,
  FieldControlButton,
  Select,
  TextField,
  useFieldsEdit,
  MISSING_VALUE,
  useOrderedFieldsEdit,
  useBlankEdit,
} from 'commons/components/forms/editors';
import { RDF_PROPERTY_ORDER } from 'models/utils/ns';
import esadTypeNls from 'admin/nls/esadType.nls';
import escoFormsNls from 'commons/nls/escoForms.nls';
import {
  MISSING_PROPERTY,
  findConstraint,
  validateConstraint,
  getErrorTexts,
} from 'models/utils/constraint';
import {
  getNamespaceChoices,
  getFullURI,
  getNamespace,
} from 'models/utils/namespaceChoices';
import './ConstraintEditor.scss';

const ConstraintValuesEdit = ({
  property,
  blankNodeId,
  graph,
  onChange,
  dispatch,
  size,
  error,
  helperText,
  ...fieldsProps
}) => {
  const t = useTranslation([esadTypeNls, escoFormsNls]);
  const { addField, fieldGroup, labelId, removeField, moveField, updateValue } =
    useOrderedFieldsEdit({
      name: fieldsProps.name,
      property: ns.RDF_PROPERTY_CONSTRAINT_VALUE,
      resourceURI: blankNodeId,
      graph,
      dispatch,
      nodetype: URI,
      orderProperty: RDF_PROPERTY_ORDER,
    });
  const fields = fieldGroup.getFields();

  const update = (field, value) => {
    updateValue(field, value);
    onChange();
  };

  const remove = (field) => {
    removeField(field);
    onChange();
  };

  return (
    <Fields
      labelId={labelId}
      addField={addField}
      {...fieldsProps}
      label={t('constraintObjectLabel')}
    >
      {fields.map((field, index) => {
        const hasError = error || field.hasError();
        const fieldErrorText = field.hasError() && t(field.getError());
        const errorText = helperText || fieldErrorText;
        return (
          <Field
            key={field.getId()}
            size={fields.length}
            onRemove={() => remove(field)}
            onClear={() => update(field, '')}
          >
            <TextField
              xs={8}
              size={size}
              onChange={({ target }) => update(field, target.value)}
              value={field.getValue()}
              label=""
              error={hasError}
              helperText={hasError ? errorText : ''}
            />
            <Grid item xs={4}>
              {field.getOrder() > 1 ? (
                <FieldControlButton
                  marginLeft="0"
                  tooltip={t('moveUpTooltip')}
                  onClick={() => moveField(field, index, index - 1)}
                >
                  <ArrowUpwardIcon />
                </FieldControlButton>
              ) : null}
              {field.getOrder() < fields.length ? (
                <FieldControlButton
                  marginLeft="0"
                  tooltip={t('moveDownTooltip')}
                  onClick={() => moveField(field, index, index + 1)}
                >
                  <ArrowDownwardIcon />
                </FieldControlButton>
              ) : null}
            </Grid>
          </Field>
        );
      })}
    </Fields>
  );
};

ConstraintValuesEdit.propTypes = {
  size: PropTypes.string,
  graph: graphPropType,
  blankNodeId: PropTypes.string,
  onChange: PropTypes.func,
  dispatch: PropTypes.func,
  property: PropTypes.string,
  error: PropTypes.bool,
  helperText: PropTypes.string,
};

const ConstraintPropertyEdit = ({
  property,
  blankNodeId,
  onChange,
  graph,
  dispatch,
  size,
  error,
  helperText,
  ...fieldsProps
}) => {
  const t = useTranslation(esadTypeNls);
  const { fields, labelId, updateValue } = useFieldsEdit({
    name: fieldsProps.name,
    property,
    graph,
    resourceURI: blankNodeId,
    nodetype: URI,
    dispatch,
  });
  const [[namespaceKey, namespaceValue], setNamespace] = useState(
    getNamespace(fields[0])
  );

  const namespaceKeyError = error && !namespaceKey;
  const namespaceValueError = error && !namespaceValue;
  const [namespaceKeyErrorText, namespaceValueErrorText] = getErrorTexts(
    helperText,
    [namespaceKeyError, namespaceValueError]
  );

  const updateNamespaceKey = ({ target }) => {
    const { value: newNamespaceKey } = target;
    setNamespace(([, currentNamespaceValue]) => {
      return [newNamespaceKey, currentNamespaceValue];
    });
    updateValue(fields[0], getFullURI(newNamespaceKey, namespaceValue));
    onChange();
  };

  const updateNamespaceValue = ({ target }) => {
    const { value: newNamespaceValue } = target;
    setNamespace(([currentNamespaceKey]) => {
      return [currentNamespaceKey, newNamespaceValue];
    });
    updateValue(fields[0], getFullURI(namespaceKey, newNamespaceValue));
    onChange();
  };

  const clear = () => {
    setNamespace(['', '']);
    updateValue(fields[0], '');
    onChange();
  };

  return (
    <Fields
      max={1}
      labelId={labelId}
      label={t('constraintPropertyLabel')}
      {...fieldsProps}
    >
      <Field size={1} onClear={clear} spacing={2}>
        <Select
          xs={3}
          labelId={`namespace-edit-${property}`}
          value={namespaceKey}
          choices={getNamespaceChoices()}
          onChange={updateNamespaceKey}
          size={size}
          label={t('namespaceLabel')}
          error={namespaceKeyError}
          helperText={namespaceKeyErrorText}
        />
        <TextField
          xs={5}
          size={size}
          onChange={updateNamespaceValue}
          value={namespaceValue}
          label={t('localNameLabel')}
          error={namespaceValueError}
          helperText={namespaceValueErrorText}
        />
      </Field>
    </Fields>
  );
};

ConstraintPropertyEdit.propTypes = {
  size: PropTypes.string,
  graph: graphPropType,
  blankNodeId: PropTypes.string,
  onChange: PropTypes.func,
  dispatch: PropTypes.func,
  property: PropTypes.string,
  error: PropTypes.bool,
  helperText: PropTypes.string,
};

export const ConstraintEditor = ({
  name,
  size,
  property,
  graph,
  resourceURI,
  dispatch,
}) => {
  const t = useTranslation([escoFormsNls, esadTypeNls]);
  const { fieldGroup, labelId, addField } = useBlankEdit({
    name,
    property,
    graph,
    resourceURI,
    dispatch,
    rdfType: ns.RDF_TYPE_CONSTRAINT,
    validate: validateConstraint,
  });

  const fields = fieldGroup.getFields();

  const handleChange = (field) => {
    const [prop, value] = findConstraint(graph, field);

    if (!prop && !value && field.isAsserted()) {
      field.setAsserted(false);
    }

    if ((prop || value) && !field.isAsserted()) {
      field.setAsserted(true);
    }

    if (field.getStatus() !== 'initial') {
      field.validate();
    }
  };

  return (
    <Fields labelId={labelId} label={t('constraintsLabel')} addField={addField}>
      {fields.map((field) => {
        return (
          <div key={field.getId()} className="esadConstraintEditor">
            <ConstraintPropertyEdit
              property={ns.RDF_PROPERTY_CONSTRAINT_PROPERTY}
              blankNodeId={field.getBlankNodeId()}
              graph={graph}
              size={size}
              onChange={() => handleChange(field)}
              dispatch={dispatch}
              error={field.getError() === MISSING_PROPERTY}
              helperText={
                field.getError() === MISSING_PROPERTY ? t(MISSING_VALUE) : ''
              }
            />
            <ConstraintValuesEdit
              property={ns.RDF_PROPERTY_CONSTRAINT_VALUE}
              graph={graph}
              blankNodeId={field.getBlankNodeId()}
              size={size}
              onChange={() => handleChange(field)}
              dispatch={dispatch}
              error={field.getError() === MISSING_VALUE}
              helperText={
                field.getError() === MISSING_VALUE ? t(MISSING_VALUE) : ''
              }
            />
          </div>
        );
      })}
    </Fields>
  );
};

ConstraintEditor.propTypes = {
  name: PropTypes.string,
  size: PropTypes.string,
  graph: graphPropType,
  resourceURI: PropTypes.string,
  dispatch: PropTypes.func,
  property: PropTypes.string,
};
