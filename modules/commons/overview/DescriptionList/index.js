import React from 'react';
import PropTypes from 'prop-types';

import './style.scss';

function DescriptionList({ children }) {
  return <div className="escoDescriptionList">{children}</div>;
}

DescriptionList.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
};

export default DescriptionList;
