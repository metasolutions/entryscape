import { useCallback, createContext, useState, useContext } from 'react';
import PropTypes from 'prop-types';
import { Entry } from '@entryscape/entrystore-js';
import {
  getSelectedSourceChoice,
  getDefaultFieldsState,
} from 'catalog/datasets/DatasetOverview/utils/visualizations';

/** Manages a visualization state */
const VisualizationStateContext = createContext();

export const VisualizationStateProvider = ({
  visualizationEntry,
  sourceChoices,
  initialSourceChoice,
  initialErrors = {},
  initialFields,
  isPrototype,
  ...props
}) => {
  const [fieldsState, setFieldsState] = useState(
    initialFields || getDefaultFieldsState(initialSourceChoice?.format)
  );

  const [errors, setErrors] = useState(initialErrors);
  const [selectedSourceChoice, setSelectedSourceChoice] =
    useState(initialSourceChoice);
  const [hasChanged, setHasChanged] = useState(false);

  const addErrors = useCallback((newErrors) => {
    setErrors((currentErrors) => ({
      ...currentErrors,
      ...newErrors,
    }));
  }, []);

  const removeErrors = useCallback((errorKeys) => {
    setErrors((currentErrors) => {
      const errorKeysToRemove = errorKeys.filter(
        (errorKey) => errorKey in currentErrors
      );
      if (!errorKeysToRemove.length) return currentErrors; // no relevant errors to remove
      const newErrors = errorKeysToRemove.reduce((errorsToKeep, errorKey) => {
        if (!(errorKey in currentErrors)) {
          errorsToKeep[errorKey] = currentErrors[errorKey];
        }
        return errorsToKeep;
      }, {});
      return newErrors;
    });
  }, []);

  const resetErrors = useCallback(() => {
    setErrors({});
  }, []);

  const updateFieldsState = useCallback(
    (change, silent = false) => {
      removeErrors(Object.keys(change));
      setFieldsState((state) => ({ ...state, ...change }));
      if (!silent) setHasChanged(true);
    },
    [removeErrors]
  );

  const onSourceChange = useCallback(
    ({ target }) => {
      const newSelectedSourceChoice = getSelectedSourceChoice(
        target.value,
        sourceChoices
      );
      resetErrors();
      setSelectedSourceChoice(newSelectedSourceChoice);

      // set all values to initial values except titles state which is kept
      setFieldsState((fields) => ({
        ...getDefaultFieldsState(newSelectedSourceChoice.format),
        sourceURI: newSelectedSourceChoice.sourceURI,
        titles: fields.titles,
      }));
    },
    [sourceChoices, resetErrors]
  );

  const isBroken = Object.keys(errors).length > 0;

  return (
    <VisualizationStateContext.Provider
      value={{
        visualizationEntry,
        fieldsState,
        initialFields,
        updateFieldsState,
        onSourceChange,
        sourceChoices,
        selectedSourceChoice,
        setSelectedSourceChoice,
        hasChanged,
        isBroken,
        errors,
        addErrors,
        removeErrors,
        resetErrors,
      }}
      {...props}
    />
  );
};

VisualizationStateProvider.propTypes = {
  visualizationEntry: PropTypes.instanceOf(Entry),
  sourceChoices: PropTypes.arrayOf(PropTypes.shape({})),
  initialSourceChoice: PropTypes.shape({ format: PropTypes.string }),
  initialFields: PropTypes.shape({}),
  initialErrors: PropTypes.shape({}),
  isPrototype: PropTypes.bool,
};

export const useVisualizationState = () => {
  const context = useContext(VisualizationStateContext);
  if (context === undefined)
    throw new Error(
      'Visualizations context must be used within context provider'
    );
  return context;
};
