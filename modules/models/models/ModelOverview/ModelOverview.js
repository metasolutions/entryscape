import React, { useEffect, useRef, useCallback } from 'react';
import Overview from 'commons/components/overview/Overview';
import { useESContext } from 'commons/hooks/useESContext';
import { useTranslation } from 'commons/hooks/useTranslation';
import { useParams } from 'react-router-dom';
import useAsync from 'commons/hooks/useAsync';
import getOverviewData from 'commons/components/overview/utils/contextOverview';
import esmoModelsNLS from 'models/nls/esmoModels.nls';
import esmoOverviewNLS from 'models/nls/esmoOverview.nls';
import escoOverviewNLS from 'commons/nls/escoOverview.nls';
import escoListNLS from 'commons/nls/escoList.nls';
import esmoCommonsNLS from 'models/nls/esmoCommons.nls';
import escoDialogsNLS from 'commons/nls/escoDialogs.nls';
import { useUserState } from 'commons/hooks/useUser';
import useHandleError from 'commons/errors/hooks/useErrorHandler';
import { MODELS_LISTVIEW } from 'models/config/config';
import useNavigate from 'commons/components/router/useNavigate';
import {
  useOverviewModel,
  withOverviewModelProvider,
} from 'commons/components/overview';
import Loader from 'commons/components/Loader';
import {
  DESCRIPTION_UPDATED,
  DESCRIPTION_CREATED,
  DESCRIPTION_PROJECT_TYPE,
} from 'commons/components/overview/actions';
import { sidebarActions } from './actions';

const nlsBundles = [
  esmoModelsNLS,
  escoOverviewNLS,
  esmoOverviewNLS,
  escoListNLS,
  esmoCommonsNLS,
  escoDialogsNLS,
];

const ModelsOverview = () => {
  const { data: state, runAsync, error, isLoading } = useAsync({ data: {} });
  const { goBackToListView } = useNavigate();

  useHandleError(error);

  const viewParams = useParams();
  const viewParamsRef = useRef(viewParams);
  const { context } = useESContext();
  const translate = useTranslation(nlsBundles);
  const { userInfo, userEntry } = useUserState();

  const [{ refreshCount }] = useOverviewModel();

  useEffect(() => {
    const getModelEntry = async () => {
      const modelEntry = await context.getEntry();
      return modelEntry;
    };
    runAsync(
      getModelEntry().then((entry) => {
        return getOverviewData(
          viewParamsRef.current,
          entry,
          context,
          translate,
          userInfo
        );
      })
    );
  }, [context, runAsync, translate, userInfo, refreshCount]);

  const descriptionItems = [
    DESCRIPTION_CREATED,
    DESCRIPTION_UPDATED,
    {
      ...DESCRIPTION_PROJECT_TYPE,
      getValues: () =>
        state?.projectType
          ? [state?.projectType?.source.name]
          : [translate('defaultProjectType')],
    },
  ];

  const handleRemove = useCallback(() => {
    goBackToListView(MODELS_LISTVIEW);
  }, [goBackToListView]);

  if (isLoading) return <Loader />;

  return (
    <div>
      {state && (
        <Overview
          backLabel={translate('backTitle')}
          descriptionItems={descriptionItems}
          entry={state.entry}
          nlsBundles={nlsBundles}
          rowActions={state.rowActions}
          sidebarActions={sidebarActions}
          sidebarProps={{
            userEntry,
            userInfo,
            context,
            onRemove: handleRemove,
          }}
        />
      )}
    </div>
  );
};

export default withOverviewModelProvider(ModelsOverview);
