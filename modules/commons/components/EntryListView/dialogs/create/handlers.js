import { useState, useEffect, useCallback } from 'react';
import { Button } from '@mui/material';
import escoRdformsNLS from 'commons/nls/escoRdforms.nls';
import { useTranslation } from 'commons/hooks/useTranslation';
import { useESContext } from 'commons/hooks/useESContext';
import { changeEntryEntityType } from 'commons/util/entry';
import Lookup from 'commons/types/Lookup';
import { LEVEL_MANDATORY } from 'commons/components/rdforms/LevelSelector';
import useRDFormsEditor from 'commons/components/rdforms/hooks/useRDFormsEditor';
import { entrystore } from 'commons/store';
import { useMainDialog } from 'commons/hooks/useMainDialog';
import useAsync from 'commons/hooks/useAsync';

// variations for creation of entitytypes
const ONLY_LINK = 'link'; // link should be provided
const ONLY_FILE = 'file'; // file should be provided
const INTERNAL_LINK = 'internal-link'; // a link will be created for you
const PATTERNED_LINK = 'patterned-link'; // link should be provided based on a pattern
const LINK_AND_FILE = 'link-file'; // you can choose between link or file
const UPGRADE_LINK = 'upgrade-link'; // via upgrade entry

/**
 * this hook is added purely for convenience, to avoid duplicating state
 * in various configurations of the create entry dialog,
 * e.g create by template, enitytype, with link or file, etc.
 *
 * @param {*} param0
 * @returns {object}
 */
const useCreateEntryState = ({
  entry,
  formTemplateId = '',
  editorLevel = LEVEL_MANDATORY,
}) => {
  const [templateId, setTemplateId] = useState(formTemplateId);
  const [selectedEntityType, setSelectedEntityType] = useState(null);
  const [isEntityTypeModified, setIsEntityTypeModified] = useState(false);
  const [level, setLevel] = useState(editorLevel);
  const { editor, EditorComponent, graph } = useRDFormsEditor({
    entry,
    templateId,
    includeLevel: level,
  });
  const [buttonDisabled, setButtonDisabled] = useState(true);
  const { data: entityType, runAsync } = useAsync(null);

  useEffect(() => {
    if (formTemplateId) return;
    runAsync(Lookup.inUse(entry));
  }, [entry, formTemplateId, runAsync]);

  useEffect(() => {
    if (!entityType) return;
    setSelectedEntityType(entityType);
    setTemplateId(entityType.templateId());
    const templateLevel = entityType.templateLevel();
    if (templateLevel) setLevel(templateLevel);
  }, [entityType]);

  const onChangeEntityType = useCallback(
    async (newEntityType) => {
      // TODO conditional perhaps not needed since we're listening onChange?
      if (newEntityType.getId() !== selectedEntityType.getId()) {
        setTemplateId(newEntityType.templateId());
        setIsEntityTypeModified(true);
        setSelectedEntityType(newEntityType);
        setButtonDisabled(false);
        try {
          await changeEntryEntityType(entry, newEntityType);
        } catch (error) {
          console.error(`Could not update entity type for entry: ${error}`);
        }
      }
    },
    [selectedEntityType, entry]
  );

  return {
    templateId,
    level,
    setLevel,
    editor,
    EditorComponent,
    graph,
    isEntityTypeModified,
    selectedEntityType,
    onChangeEntityType,
    buttonDisabled,
    setButtonDisabled,
  };
};

/**
 * Convenience hook to simplify to get to the most accurate context for a specific entitytype.
 *
 * @param {object} entityType
 * @returns {object}
 */
const useEntityTypeContext = (entityType) => {
  const fromContext = useESContext();
  return entityType.context
    ? { context: entrystore.getContextById(entityType.context) }
    : fromContext;
};

/**
 * Hook that creates an action handler using main dialog
 * stating if the user wants to keep editting or close the dialog
 * if form has changes.
 */

const useConfirmCloseAction = (closeDialog) => {
  const translate = useTranslation(escoRdformsNLS);
  const {
    openMainDialog: openConfirmationDialog,
    closeMainDialog: closeConfirmationDialog,
  } = useMainDialog();

  const confirmCloseAction = useCallback(
    (hasFormChanged) => {
      if (!hasFormChanged) {
        closeDialog();
        return;
      }

      const handleConfirm = () => {
        closeConfirmationDialog();
        closeDialog();
      };

      const actionsNode = (
        <>
          <Button onClick={handleConfirm} color="primary" variant="text">
            {translate('discardMetadataChanges')}
          </Button>
          <Button onClick={closeConfirmationDialog} color="primary" autoFocus>
            {translate('keepMetadataChanges')}
          </Button>
        </>
      );

      openConfirmationDialog({
        content: translate('discardMetadataChangesWarning'),
        actions: actionsNode,
      });
    },
    [closeDialog, closeConfirmationDialog, openConfirmationDialog, translate]
  );

  return confirmCloseAction;
};

export {
  useConfirmCloseAction,
  useCreateEntryState,
  useEntityTypeContext,
  ONLY_LINK,
  ONLY_FILE,
  INTERNAL_LINK,
  PATTERNED_LINK,
  LINK_AND_FILE,
  UPGRADE_LINK,
};
