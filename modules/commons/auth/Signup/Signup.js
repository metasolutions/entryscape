import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import registry from 'commons/registry';
import {
  Alert,
  Link as MuiLink,
  Grid,
  Checkbox,
  FormControl,
  FormControlLabel,
  FormHelperText,
  TextField,
  Container,
  InputAdornment,
  IconButton,
} from '@mui/material';
import Tooltip from 'commons/components/common/Tooltip';
import {
  OpenInNew as OpenInNewIcon,
  Visibility as VisibilityIcon,
  VisibilityOff as VisibilityOffIcon,
} from '@mui/icons-material';
import Logo from 'commons/nav/components/Logo';
import configUtil, {
  getTermsAndConditionsParams,
  getPrivacyPolicyParams,
} from 'commons/util/config';
import useDelayedCallback from 'commons/hooks/useDelayedCallback';
import { validateEmail, isInputTextValid } from 'commons/util/util';
import config from 'config';
import { isEmpty } from 'lodash-es';
import { entrystore } from 'commons/store';
import escoSigninNLS from 'commons/nls/escoSignin.nls';
import { useTranslation } from 'commons/hooks/useTranslation';
import { getPathFromViewName } from 'commons/util/site';
import Recaptcha from 'commons/components/Recaptcha';
import LanguageSelect from 'commons/components/common/LanguageSelect';
import LoadingButton from 'commons/components/LoadingButton';
import usePassword from '../usePassword';
import './index.scss';

const SignUp = () => {
  const defaultPristine = {
    firstName: true,
    lastName: true,
    email: true,
    agreement: true,
    captchaOk: true,
  };
  const {
    password,
    isPasswordVisible,
    passwordError,
    clearPassword,
    handleChangePassword,
    togglePasswordVisibility,
  } = usePassword();
  const reCaptchaSiteKey = Boolean(config.get('reCaptchaSiteKey'));
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [email, setEmail] = useState('');
  const [agreement, setAgreement] = useState(false);
  const [errors, setErrors] = useState({});
  const [captchaOk, setCaptchaOk] = useState(!reCaptchaSiteKey);
  const [pristine, setPristine] = useState(defaultPristine);
  const [loading, setLoading] = useState(false);
  const [success, setSuccess] = useState(false);
  const signinPath = getPathFromViewName('signin');
  const t = useTranslation(escoSigninNLS);
  const { url: termsAndConditionsURL } = getTermsAndConditionsParams();
  const { url: privacyPolicyURL } = getPrivacyPolicyParams();

  const validate = (values) => {
    const validationErrors = {};
    if (!values.email) {
      validationErrors.email = 'emailRequired';
    } else {
      const isEmailValid = validateEmail(values.email);
      if (!isEmailValid) {
        validationErrors.email = 'signupInvalidEmail';
      }
    }

    if (passwordError) validationErrors.password = passwordError;

    if (values.firstName.trim().length === 1) {
      validationErrors.firstName = 'signupTooShortName';
    }

    if (!isInputTextValid(values.firstName)) {
      validationErrors.firstName = 'firstNameRequired';
    }

    if (values.lastName.trim().length === 1) {
      validationErrors.lastName = 'signupTooShortSurname';
    }

    if (!isInputTextValid(values.lastName)) {
      validationErrors.lastName = 'lastNameRequired';
    }

    if (!values.agreement) {
      validationErrors.agreement = 'personalInfoApprovalRequired';
    }

    if (!captchaOk) {
      validationErrors.captchaOk = 'captchaRequired';
    }
    return validationErrors;
  };

  const checkErrors = () =>
    validate({
      firstName,
      lastName,
      email,
      password,
      agreement,
      captchaOk,
    });

  useEffect(() => {
    if (success) {
      setErrors({});
      return;
    }
    const currentErrors = checkErrors();
    const errorsToSet = {
      firstName:
        pristine.firstName && !errors.firstName
          ? undefined
          : currentErrors.firstName,
      lastName:
        pristine.lastName && !errors.lastName
          ? undefined
          : currentErrors.lastName,
      agreement:
        pristine.agreement && !errors.agreement
          ? undefined
          : currentErrors.agreement,
      email: pristine.email && !errors.email ? undefined : currentErrors.email,
      password: currentErrors.password,
      captchaOk:
        pristine.captchaOk && !errors.captchaOk
          ? undefined
          : currentErrors.captchaOk,
    };
    setErrors(errorsToSet);
  }, [
    firstName,
    lastName,
    email,
    password,
    agreement,
    pristine,
    captchaOk,
    success,
    errors.firstName,
    errors.lastName,
    errors.agreement,
    errors.email,
    errors.captchaOk,
    // checkErrors, // TODO include, requires refactoring
  ]);

  const delayedCallback = useDelayedCallback();

  const handleOnBlur = (event) => {
    const { target, relatedTarget, currentTarget } = event;
    const delay = !currentTarget.contains(relatedTarget) ? 300 : 0;
    const setPristineDelayed = delayedCallback(setPristine, delay);
    setPristineDelayed({
      ...pristine,
      [target.name]: false,
    });
  };

  const resetForm = () => {
    if (reCaptchaSiteKey) window.grecaptcha.reset();
    setFirstName('');
    setLastName('');
    setEmail('');
    clearPassword();
    setAgreement(false);
    setErrors({});
  };

  const signup = async () => {
    if (!captchaOk) {
      setPristine({ ...pristine, captchaOk: false });
      return;
    }

    const urlSuccess =
      configUtil.getBaseUrl() + getPathFromViewName('start').slice(1);

    const signupInfo = {
      firstname: firstName.trim(),
      lastname: lastName.trim(),
      email,
      password: password.trim(),
      urlsuccess: urlSuccess,
    };

    if (reCaptchaSiteKey) {
      signupInfo.grecaptcharesponse = window.grecaptcha.getResponse();
    }

    entrystore
      .getREST()
      .post(`${entrystore.getBaseURI()}auth/signup`, JSON.stringify(signupInfo))
      .then(
        () => {
          registry.set('signupInfo', signupInfo);
          setSuccess(true);
          resetForm();
        },
        (err) => {
          if (err.response.status === 417) {
            const domain = signupInfo.email.substr(
              signupInfo.email.indexOf('@') + 1
            );
            setErrors({ response: t('signupWrongDomain', { domain }) });
          } else {
            setErrors({ response: t('signupErrorMessage') });
          }
        }
      );
  };

  const handleSubmit = async () => {
    const currentErrors = checkErrors();
    const grecaptcha = window.grecaptcha || null;
    if (reCaptchaSiteKey && grecaptcha && !grecaptcha.getResponse()) {
      currentErrors.captchaOk = 'captchaRequired';
    }
    if (!isEmpty(currentErrors)) {
      setErrors(currentErrors);
    } else {
      setLoading(true);
      await signup();
      setPristine(defaultPristine);
      setLoading(false);
    }
  };

  const signupCaptchaCallback = () => {
    setCaptchaOk(true);
    setErrors({ ...errors, captchaOk: '' });
  };

  const translateError = (nlsErrorKey) => (nlsErrorKey ? t(nlsErrorKey) : '');

  const captchaCallbackName = 'signupCaptchaCallback';
  window[captchaCallbackName] = signupCaptchaCallback;

  return (
    <Container
      classes={{ root: 'escoSignup__Container' }}
      component="main"
      maxWidth="sm"
    >
      <Grid container spacing={2}>
        <Grid item xs={12} sm={6} />
        <Grid item xs={12} sm={6}>
          <LanguageSelect />
        </Grid>
      </Grid>

      <div className="escoSignup__paper">
        <Link to="/">
          <Logo type="full" className="escoSignup__logo" />
        </Link>
        <form className="escoSignup__form" noValidate>
          {errors.response && (
            <Alert severity="error" style={{ marginBottom: '16px' }}>
              {errors.response}
            </Alert>
          )}
          {success && (
            <Alert severity="success" style={{ marginBottom: '16px' }}>
              {t('signupConfirmationMessage')}
            </Alert>
          )}
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <TextField
                autoComplete="given-name"
                name="firstName"
                variant="outlined"
                required
                id="firstName"
                label={t('firstName')}
                autoFocus
                value={firstName}
                InputLabelProps={{ shrink: true }}
                onChange={(e) => {
                  setFirstName(e.target.value);
                  setPristine({ ...pristine, firstName: false });
                }}
                onBlur={handleOnBlur}
                error={!!errors.firstName}
                helperText={translateError(errors.firstName)}
                FormHelperTextProps={{ 'aria-live': 'polite' }}
              />
            </Grid>

            <Grid item xs={12} sm={6}>
              <TextField
                variant="outlined"
                required
                id="lastName"
                label={t('lastname')}
                name="lastName"
                autoComplete="family-name"
                InputLabelProps={{ shrink: true }}
                value={lastName}
                onChange={(e) => {
                  setLastName(e.target.value);
                  setPristine({ ...pristine, lastName: false });
                }}
                onBlur={handleOnBlur}
                error={!!errors.lastName}
                helperText={translateError(errors.lastName)}
                FormHelperTextProps={{ 'aria-live': 'polite' }}
              />
            </Grid>

            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                id="email"
                label={t('email')}
                name="email"
                autoComplete="email"
                InputLabelProps={{ shrink: true }}
                value={email}
                onChange={(e) => {
                  setEmail(e.target.value);
                  setPristine({ ...pristine, email: false });
                }}
                onBlur={handleOnBlur}
                error={!!errors.email}
                helperText={translateError(errors.email)}
                FormHelperTextProps={{ 'aria-live': 'polite' }}
              />
            </Grid>

            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                name="password"
                label={t('password')}
                id="password"
                autoComplete="current-password"
                InputLabelProps={{ shrink: true }}
                value={password}
                onChange={({ target }) => handleChangePassword(target.value)}
                onBlur={handleOnBlur}
                type={isPasswordVisible ? 'text' : 'password'}
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="end">
                      <Tooltip title={t('passwordVisibility')}>
                        <IconButton
                          aria-label={t('passwordVisibility')}
                          onClick={togglePasswordVisibility}
                          onMouseDown={(e) => e.preventDefault()}
                        >
                          {isPasswordVisible ? (
                            <VisibilityIcon />
                          ) : (
                            <VisibilityOffIcon />
                          )}
                        </IconButton>
                      </Tooltip>
                    </InputAdornment>
                  ),
                }}
                error={!!errors.password}
                helperText={errors.password}
                FormHelperTextProps={{ 'aria-live': 'polite' }}
              />
            </Grid>

            <Grid item xs={12}>
              <FormControl>
                <FormControlLabel
                  control={
                    <Checkbox
                      name="agreement"
                      color="primary"
                      required
                      checked={agreement}
                      onChange={(e) => {
                        setAgreement(e.target.checked);
                        setPristine({ ...pristine, agreement: false });
                      }}
                      onBlur={handleOnBlur}
                    />
                  }
                  label={t('confidentialityAgree')}
                  slotProps={{ typography: { variant: 'body2' } }}
                />
                <MuiLink
                  href={termsAndConditionsURL}
                  target="_blank"
                  rel="noopener"
                  className="escoSignup__agreementLink"
                  variant="body2"
                >
                  {t('confidentialityAgreeTitle')}
                  <OpenInNewIcon className="escoSignup__agreementLinkIcon" />
                </MuiLink>
                <MuiLink
                  href={privacyPolicyURL}
                  target="_blank"
                  rel="noopener"
                  className="escoSignup__agreementLink"
                  variant="body2"
                >
                  {t('privacyPolicyLinkLabel')}
                  <OpenInNewIcon className="escoSignup__agreementLinkIcon" />
                </MuiLink>
                {!!errors.agreement && (
                  <FormHelperText aria-live="polite" error>
                    {translateError(errors.agreement)}
                  </FormHelperText>
                )}
              </FormControl>
            </Grid>
            {reCaptchaSiteKey ? (
              <Grid item xs={12}>
                <Recaptcha callbackName={captchaCallbackName} />
                {errors.captchaOk && (
                  <FormHelperText aria-live="polite" error={!!errors.captchaOk}>
                    {translateError(errors.captchaOk)}
                  </FormHelperText>
                )}
              </Grid>
            ) : null}
          </Grid>

          <div className="escoSignup__wrapper">
            <LoadingButton
              loading={loading}
              success={success}
              color={
                isInputTextValid(firstName) &&
                isInputTextValid(lastName) &&
                email &&
                isInputTextValid(password) &&
                agreement &&
                captchaOk &&
                !!errors
                  ? 'primary'
                  : 'secondary'
              }
              onClick={(e) => {
                e.preventDefault();
                handleSubmit(e);
              }}
            >
              {t('signupButton')}
            </LoadingButton>
          </div>
          <Grid container justifyContent="flex-end">
            <Grid item>
              <MuiLink component={Link} to={signinPath} variant="body2">
                {`${t('signinButtonComplementaryText')} ${t('signInButton')}`}
              </MuiLink>
            </Grid>
          </Grid>
        </form>
      </div>
    </Container>
  );
};

export default SignUp;
