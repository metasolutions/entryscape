import { Graph } from '@entryscape/rdfjson';
import { RDF_PROPERTY_VALUE } from './ns';
import {
  getURIEditorType,
  RESOURCE,
  getNamespaceEntryValue,
  NAMESPACE_ENTRY,
} from './metadata';

/**
 * @param {{graph: Graph, resourceURI: string}} props
 * @returns {Promise<string>}
 */
export const getValueURIValue = ({ graph, resourceURI }) => {
  const valueType = getURIEditorType(
    graph,
    resourceURI,
    RDF_PROPERTY_VALUE,
    NAMESPACE_ENTRY
  );

  if (valueType === RESOURCE)
    return Promise.resolve(
      graph.findFirstValue(resourceURI, RDF_PROPERTY_VALUE)
    );
  return getNamespaceEntryValue(graph, resourceURI, RDF_PROPERTY_VALUE);
};
