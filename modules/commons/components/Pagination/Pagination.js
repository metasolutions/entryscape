import PropTypes from 'prop-types';
import { TablePagination } from '@mui/material';
import { useTranslation } from 'commons/hooks/useTranslation';
import escoPaginationNLS from 'commons/nls/escoPagination.nls';

const Pagination = (props) => {
  const translate = useTranslation(escoPaginationNLS);
  const { disabled } = props;

  return (
    <TablePagination
      component="div"
      rowsPerPage={10}
      labelRowsPerPage={translate('labelRowsPerPage')}
      rowsPerPageOptions={[]}
      getItemAriaLabel={(type) => {
        switch (type) {
          case 'previous':
          case 'last':
            return translate('backIconButtonText');
          default:
            // 'first' || 'next'
            return translate('nextIconButtonText');
        }
      }}
      SelectProps={{
        disabled,
        style: { marginTop: '2px' },
        id: 'rows-per-page-select-label',
        inputProps: { 'aria-labelledby': 'rows-per-page-select-label' },
      }}
      backIconButtonProps={
        disabled
          ? {
              disabled,
            }
          : undefined
      }
      nextIconButtonProps={
        disabled
          ? {
              disabled,
            }
          : undefined
      }
      {...props}
    />
  );
};

Pagination.propTypes = {
  disabled: PropTypes.bool,
};

export default Pagination;
