import PropTypes from 'prop-types';
import { ButtonBase } from '@mui/material';
import './LinearProgressButton.scss';

const ProgressButton = ({ children, disabled, ...props }) => {
  return (
    <ButtonBase
      classes={{
        root: `escoLinearProgressButton ${
          disabled ? 'escoLinearProgressButton--disabled' : ''
        }`,
      }}
      disabled={disabled}
      focusRipple
      {...props}
    >
      {children}
    </ButtonBase>
  );
};

ProgressButton.propTypes = {
  disabled: PropTypes.bool,
  onClick: PropTypes.func,
  children: PropTypes.node,
};

export default ProgressButton;
