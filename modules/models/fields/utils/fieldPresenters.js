import { getLanguages } from 'commons/components/forms/editors';
import * as fields from 'models/utils/fieldDefinitions';

export const primaryFields = [
  {
    ...fields.FIELD_PURPOSE,
    inline: true,
    labelNlsKey: undefined,
  },
  {
    ...fields.FIELD_STATUS,
    inline: true,
  },
  {
    ...fields.FIELD_EXTENDS,
    inline: true,
  },
  {
    ...fields.FIELD_INLINE,
    inline: true,
  },
];

export const secondaryFields = [
  { ...fields.FIELD_ALTERNATIVE_TEXT },
  { ...fields.FIELD_OPTIONS },
  {
    ...fields.FIELD_PROPERTY,
  },
  {
    ...fields.FIELD_CONSTRAINT,
  },
  {
    ...fields.FIELD_NODETYPE,
  },
  {
    ...fields.FIELD_DATATYPE,
  },
  {
    ...fields.FIELD_CARDINALITY,
  },
  {
    ...fields.FIELD_LABEL,
  },
  {
    ...fields.FIELD_DESCRIPTION,
  },
  {
    ...fields.FIELD_ATTRIBUTE,
  },
  {
    ...fields.FIELD_HELP,
  },
  {
    ...fields.FIELD_PLACEHOLDER,
  },
  {
    ...fields.FIELD_EDIT_LABEL,
  },
  {
    ...fields.FIELD_EDIT_DESCRIPTION,
  },
  {
    ...fields.FIELD_CSS,
  },
  {
    ...fields.FIELD_DEPS,
  },
  {
    ...fields.FIELD_LANGUAGE,
    choices: getLanguages(),
  },
  {
    ...fields.FIELD_PATTERN,
  },
  {
    ...fields.FIELD_VALUE_TEMPLATE,
  },
];
