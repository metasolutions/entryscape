import React, { useCallback, useEffect, useRef } from 'react';
import { EntityType } from 'entitytype-lookup';
import { useESContext } from 'commons/hooks/useESContext';
import { useTranslation } from 'commons/hooks/useTranslation';
import { useParams } from 'react-router-dom';
import { entrystoreUtil } from 'commons/store';
import escaStatisticsNLS from 'catalog/nls/escaStatistics.nls';
import escaOverviewNLS from 'catalog/nls/escaOverview.nls';
import escoOverviewNLS from 'commons/nls/escoOverview.nls';
import escoListNLS from 'commons/nls/escoList.nls';
import escaCatalogNLS from 'catalog/nls/escaCatalog.nls';
import Overview from 'commons/components/overview/Overview';
import useAsync from 'commons/hooks/useAsync';
import useAsyncCallback from 'commons/hooks/useAsyncCallback';
import { isCatalogPublic } from 'catalog/utils/react/catalog';
import config from 'config';
import useHandleError from 'commons/errors/hooks/useErrorHandler';
import getOverviewData from 'commons/components/overview/utils/contextOverview';
import { useUserState } from 'commons/hooks/useUser';
import { nameToURI } from 'commons/types/utils/uri';
import { CATALOGS_LISTVIEW } from 'catalog/config/config';
import useNavigate from 'commons/components/router/useNavigate';
import {
  useOverviewModel,
  withOverviewModelProvider,
} from 'commons/components/overview';
import { Context } from '@entryscape/entrystore-js';
import {
  DESCRIPTION_UPDATED,
  DESCRIPTION_CREATED,
  DESCRIPTION_PROJECT_TYPE,
} from 'commons/components/overview/actions';
import Loader from 'commons/components/Loader/Loader';
import { getCountQuery } from '../../datasets/queries';
import Statistics from './components/Statistics';
import { sidebarActions } from './actions';

const nlsBundles = [
  escoOverviewNLS,
  escaOverviewNLS,
  escoListNLS,
  escaStatisticsNLS,
  escaCatalogNLS,
];

/**
 * Returns catalog specific counting queries when applicable
 *
 * @param {Context} _context
 * @param {EntityType} entityType
 * @returns {number}
 */
const getCatalogCountQuery = (_context, entityType) => {
  if (entityType.get('name') !== 'dataset') return null;
  return getCountQuery;
};

/**
 * Checks whether any of the entity types or the entity types they refine
 * correspond to the statistics shown in the graph.
 *
 * @param {EntityType} entityTypes
 * @returns {boolean}
 */
const getHasStatsTypes = (entityTypes = []) => {
  if (!entityTypes.length) return false;

  const statsTypeNames = ['datasetDocument', 'distribution'];
  const hasStatsTypes = entityTypes.some((entityType) =>
    statsTypeNames.includes(entityType.get('name'))
  );

  const statsTypesUris = statsTypeNames.map((typeName) =>
    nameToURI(typeName, 'entitytypes')
  );
  const refinedTypeUris = entityTypes.map((type) => type.get('refines'));
  const refinesStatsTypes = statsTypesUris.filter((uri) =>
    refinedTypeUris.includes(uri)
  );

  return hasStatsTypes || refinesStatsTypes;
};

const CatalogOverview = () => {
  const { userInfo, userEntry } = useUserState();
  const { data: state, runAsync, error, isLoading } = useAsync({ data: {} });
  const { goBackToListView } = useNavigate();

  useHandleError(error);

  const viewParams = useParams();
  const viewParamsRef = useRef(viewParams);
  const { context } = useESContext();
  const [isPublic] = useAsyncCallback(isCatalogPublic, context);
  const translate = useTranslation(nlsBundles);

  const [{ refreshCount }] = useOverviewModel();

  useEffect(() => {
    const getCatalogEntry = async () => {
      const catalogEntry = await entrystoreUtil.getEntryByType(
        'dcat:Catalog',
        context
      );
      return catalogEntry;
    };
    runAsync(
      getCatalogEntry().then((catalogEntry) => {
        return getOverviewData(
          viewParamsRef.current,
          catalogEntry,
          context,
          translate,
          userInfo,
          getCatalogCountQuery
        );
      })
    );
  }, [context, runAsync, translate, userInfo, refreshCount]);

  const descriptionItems = [
    DESCRIPTION_CREATED,
    DESCRIPTION_UPDATED,
    {
      ...DESCRIPTION_PROJECT_TYPE,
      getValues: () =>
        state?.projectType
          ? [state?.projectType?.source.name]
          : [translate('defaultProjectType')],
    },
  ];

  const handleRemove = useCallback(() => {
    goBackToListView(CATALOGS_LISTVIEW);
  }, [goBackToListView]);

  const showStatistics =
    config.get('catalog.includeStatistics') &&
    isPublic &&
    getHasStatsTypes(state?.entityTypes);

  if (isLoading) return <Loader />;

  return (
    <Overview
      backLabel={translate('backTitle')}
      entry={state.entry}
      nlsBundles={nlsBundles}
      descriptionItems={descriptionItems}
      rowActions={state.rowActions}
      sidebarActions={sidebarActions}
      sidebarProps={{ userEntry, userInfo, context, onRemove: handleRemove }}
    >
      {showStatistics ? <Statistics /> : null}
    </Overview>
  );
};

export default withOverviewModelProvider(CatalogOverview);
