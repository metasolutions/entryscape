import { Entry } from '@entryscape/entrystore-js';
import registry from 'commons/registry';
import config from 'config';
import { localize } from 'commons/locale';
import { getLabel } from 'commons/util/rdfUtils';
import {
  getConfigEntries,
  migrateTypesToStore,
} from 'admin/configs/utils/configQuery';
import {
  RDF_CLASS_ENTITY_TYPE,
  ENTITY_TYPE_BUILTIN,
  ENTITY_TYPE_CUSTOM,
  ENTITY_TYPE_ID_PREFIX,
} from 'admin/configs/utils/namespace';
import { getBuiltinEntityTypeNames } from 'commons/types/utils/entityType';
import * as fieldDefinitions from './fieldDefinitions';

/**
 * Get entity types defined in instance config
 *
 * @returns {object[]}
 */
export const getInstanceEntityTypes = () => {
  return registry.getInstanceConfig().entitytypes || [];
};

/**
 * Checks for entity types defined in instance config.
 *
 * @returns {boolean}
 */
export const hasInstanceEntityTypes = () => {
  return Boolean(getInstanceEntityTypes().length);
};

/**
 * Merges entity types defined in instance and application config. A type
 * prop is added.
 *
 * @returns {object[]}
 */
export const getInstanceAndBuiltinEntityTypes = () => {
  const systemEntityTypeIds = registry
    .getApplicationConfig()
    .entitytypes.map(({ name }) => name);

  const entityTypes = config.get('entitytypes');

  return entityTypes.map((entityType) => {
    return {
      id: entityType.name,
      label: localize(entityType.label),
      type: systemEntityTypeIds.includes(entityType.name)
        ? ENTITY_TYPE_BUILTIN
        : ENTITY_TYPE_CUSTOM,
      config: entityType,
    };
  });
};

/**
 * Finds overridden builtin entity types, by comparing instance and
 * builtin entity types. An builtin entity type is overridden if there's
 * an instance entity type with matching name.
 *
 * @returns {object[]}
 */
export const getOverriddenBuiltinEntityTypes = () => {
  const instanceEntityTypes = registry.getInstanceConfig().entitytypes || [];
  const builtinEntityTypeNames = getBuiltinEntityTypeNames();

  return instanceEntityTypes.filter((instanceEntityType) => {
    return builtinEntityTypeNames.includes(instanceEntityType.name);
  });
};

/**
 * Finds custom entity types, by comparing instance and builtin entity
 * types. It's a custom instance entity type if there's no builtin entity
 * type with matching name.
 *
 * @returns {object[]}
 */
const getCustomInstanceEntityTypes = () => {
  const builtinEntityTypeNames = getBuiltinEntityTypeNames();
  const instanceEntityTypes = registry.getInstanceConfig().entitytypes || [];

  return instanceEntityTypes.filter((instanceEntityType) => {
    return !builtinEntityTypeNames.includes(instanceEntityType.name);
  });
};

/**
 * Migrates overridden builtin entity types and custom entity types from
 * instance config. Only overridden values for builtin entity types are
 * migrated.
 *
 * @returns {Entry[]}
 */
export const migrateEntityTypesToStore = async () => {
  const overriddenEntityTypes = getOverriddenBuiltinEntityTypes();
  const customInstanceEntityTypes = getCustomInstanceEntityTypes();

  const entityTypeEntries = await migrateTypesToStore({
    types: [...overriddenEntityTypes, ...customInstanceEntityTypes],
    idPrefix: ENTITY_TYPE_ID_PREFIX,
    fieldDefinitions,
    rdfType: RDF_CLASS_ENTITY_TYPE,
  });
  return entityTypeEntries;
};

/**
 * @returns {Promise<object[]>}
 */
export const getEntityTypeEntries = () => {
  return getConfigEntries({ rdfType: RDF_CLASS_ENTITY_TYPE });
};

/**
 * Extracts builtin entity types. If an builtin entity type has a
 * matching entry it means it's overridden.
 *
 * @param {Entry[]} entityTypeEntries
 * @returns {object[]}
 */
const getBuiltinEntityTypes = (entityTypeEntries) => {
  const builtinEntityTypeConfigs = registry.getApplicationConfig().entitytypes;

  const builtinEntityTypes = builtinEntityTypeConfigs.map(
    (builtinEntityTypeConfig) => {
      const { name, label } = builtinEntityTypeConfig;
      const id = `${ENTITY_TYPE_ID_PREFIX}-${name}`;
      const builtinEntityTypeEntry = entityTypeEntries.find(
        (entityTypeEntry) => entityTypeEntry.getId() === id
      );
      if (builtinEntityTypeEntry) {
        return {
          id,
          label: getLabel(builtinEntityTypeEntry) || localize(label),
          entry: builtinEntityTypeEntry,
          type: ENTITY_TYPE_BUILTIN,
          config: builtinEntityTypeConfig,
        };
      }
      return {
        id,
        label: localize(label),
        type: ENTITY_TYPE_BUILTIN,
        config: builtinEntityTypeConfig,
      };
    }
  );

  return builtinEntityTypes;
};

/**
 * Extracts custom entity types from array of entity type entries. If there's no
 * matching builtin entity type, it's a custom entity type.
 *
 * @param {Entry[]} entityTypeEntries
 * @returns {object[]}
 */
const getCustomEntityTypes = (entityTypeEntries) => {
  // exlude builtin entity types
  const builtinEntityTypeConfigs = registry.getApplicationConfig().entitytypes;
  const customEntityTypesEntries = entityTypeEntries.filter(
    (entityTypeEntry) =>
      !builtinEntityTypeConfigs.find(
        ({ name }) =>
          entityTypeEntry.getId() === `${ENTITY_TYPE_ID_PREFIX}-${name}`
      )
  );

  return customEntityTypesEntries.map((customEntityTypeEntry) => ({
    id: customEntityTypeEntry.getId(),
    label: getLabel(customEntityTypeEntry),
    type: ENTITY_TYPE_CUSTOM,
    entry: customEntityTypeEntry,
  }));
};

/**
 * Filters entity types on type
 *
 * @param {Entry[]} entityTypeEntries
 * @param {string} typeOfEntityType
 * @returns {Entry[]}
 */
export const getEntityTypesByFilter = (entityTypeEntries, typeOfEntityType) => {
  if (!entityTypeEntries) return [];

  if (typeOfEntityType === ENTITY_TYPE_BUILTIN)
    return getBuiltinEntityTypes(entityTypeEntries);

  if (typeOfEntityType === ENTITY_TYPE_CUSTOM)
    return getCustomEntityTypes(entityTypeEntries);

  return [
    ...getBuiltinEntityTypes(entityTypeEntries),
    ...getCustomEntityTypes(entityTypeEntries),
  ];
};
