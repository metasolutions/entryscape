import config from 'config';

const contentviewers = () => {
  config
    .get('contentviewers')
    .forEach((contentViewer) =>
      console.log(`this content viewer should be implemented ${contentViewer}`)
    );
  // config.get('contentviewers').forEach((contentViewer, idx) => {
  //   import(
  //     /* webpackInclude: /.*View\.js$/ */
  //     /* webpackMode: "eager" */
  //     `commons/contentview/${contentViewer.class}.js`
  //   ).then((viewerClass) => {
  //     try {
  //       config.get('contentviewers')[idx].class = viewerClass.default;
  //     } catch (e) {
  //       throw Error(`Could not load viewer ${contentViewer.class}`);
  //     }
  //   });
  // });
};

export default contentviewers;
