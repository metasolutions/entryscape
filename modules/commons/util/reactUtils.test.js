import { render } from '@testing-library/react';
import { parseTemplate } from './reactUtils';

describe('parseTemplate', () => {
  const Link = ({ children, href }) => <a href={href}>{children}</a>;

  test('replaces placeholders with React components', () => {
    const properties = {
      1: <Link href="/page1">Link 1</Link>,
      2: <Link href="/page2">Link 2</Link>,
      name: <span>Entity</span>,
    };

    const { container } = render(
      <p>{parseTemplate('Click on $1 or ${name}', properties)}</p>
    );

    expect(container.textContent).toBe('Click on Link 1 or Entity');
    expect(container.querySelector('a[href="/page1"]')).toBeTruthy();
    expect(container.querySelector('span')).toBeTruthy();
  });

  test('handles multiple properties correctly', () => {
    const properties = {
      targetA: <Link href="/targetA">Target A</Link>,
      targetB: <Link href="/targetB">Target B</Link>,
    };

    const { container } = render(
      <p>{parseTemplate('Click on ${targetA} or ${targetB}.', properties)}</p>
    );

    expect(container.textContent).toBe('Click on Target A or Target B.');
    expect(container.querySelector('a[href="/targetA"]')).toBeTruthy();
    expect(container.querySelector('a[href="/targetB"]')).toBeTruthy();
  });

  test('preserves unmatched placeholders', () => {
    const properties = {
      1: <Link href="/page1">Link 1</Link>,
    };

    const { container } = render(
      <p>{parseTemplate('Click on $1 and $2', properties)}</p>
    );

    expect(container.textContent).toBe('Click on Link 1 and $2');
    expect(container.querySelector('a[href="/page1"]')).toBeTruthy();
  });

  test('handles text before, between, and after properties', () => {
    const properties = {
      1: <Link href="/page1">First</Link>,
      2: <Link href="/page2">Second</Link>,
    };

    const { container } = render(
      <p>{parseTemplate('Start $1, then $2, then done.', properties)}</p>
    );

    expect(container.textContent).toBe('Start First, then Second, then done.');
    expect(container.querySelector('a[href="/page1"]')).toBeTruthy();
    expect(container.querySelector('a[href="/page2"]')).toBeTruthy();
  });

  test('handles an empty string', () => {
    const properties = {
      1: <Link href="/page1">First</Link>,
    };

    const { container } = render(<p>{parseTemplate('', properties)}</p>);

    expect(container.textContent).toBe('');
  });

  test('handles no placeholders in the string', () => {
    const properties = {
      1: <Link href="/page1">Target</Link>,
    };

    const { container } = render(
      <p>{parseTemplate('This is a normal string.', properties)}</p>
    );

    expect(container.textContent).toBe('This is a normal string.');
  });
});
