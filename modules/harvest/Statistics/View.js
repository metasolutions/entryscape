import { useESContext } from 'commons/hooks/useESContext';
import Statistics from 'harvest/Statistics/index';

const StatisticsView = () => {
  const { context } = useESContext();
  const contextId = context.getId();

  return <Statistics contextId={contextId} />;
};

export default StatisticsView;
