import { useState } from 'react';
import PropTypes from 'prop-types';
import {
  Fields,
  Field,
  useBlankEdit,
  useFieldsEdit,
  Select,
  TextField,
  LITERAL,
  URI,
  useEditorContext,
  CHANGE,
  REMOVE_FIELD_GROUPS,
} from 'commons/components/forms/editors';
import SelectEditMode from 'models/components/SelectEditMode';
import { entryPropType, graphPropType } from 'commons/util/entry';
import {
  getNamespaceEntry,
  getNamespaceEntryChoices,
} from 'models/utils/namespaceChoices';
import esmoCommonsNls from 'models/nls/esmoCommons.nls';
import escoFormsNls from 'commons/nls/escoForms.nls';
import { NAMESPACE_ENTRY } from 'models/utils/metadata';
import {
  RDF_PROPERTY_NAMESPACE_ENTRY,
  RDF_PROPERTY_NAMESPACE_LOCALNAME,
  RDF_TYPE_QUALIFIED_RESOURCE,
} from 'models/utils/ns';
import { useTranslation } from 'commons/hooks/useTranslation';
import { findGroupsByResourceURI } from 'commons/components/forms/editors/utils/editorContext';

export const NamespaceEditor = ({
  property,
  nodetype,
  size,
  graph,
  resourceURI,
  namespaceEntries,
  renderInlineContent,
  onChange,
  fieldsCount,
  onClear,
  onRemove,
  renderControls,
  helperText,
  error,
  validate,
  ...fieldsProps
}) => {
  const translate = useTranslation([esmoCommonsNls, escoFormsNls]);
  const { dispatch } = useEditorContext();
  const {
    fields: [namespaceField],
    labelId,
    updateValue: updateNamespace,
    fieldGroup: namespaceFieldGroup,
    removeField: removeNamespace,
  } = useFieldsEdit({
    name: fieldsProps.name,
    property: RDF_PROPERTY_NAMESPACE_ENTRY,
    nodetype: URI,
    graph,
    resourceURI,
    mandatory: fieldsProps.mandatory,
    dispatch,
    validate,
  });
  const {
    fields: [localnameField],
    updateValue: updateLocalname,
    fieldGroup: localnameFieldGroup,
    removeField: removeLocalname,
  } = useFieldsEdit({
    name: 'localname',
    property: RDF_PROPERTY_NAMESPACE_LOCALNAME,
    nodetype: LITERAL,
    graph,
    resourceURI,
    mandatory: fieldsProps.mandatory,
    dispatch,
  });

  const [namespaceEntry, setNamespaceEntry] = useState(
    getNamespaceEntry(namespaceField, namespaceEntries) || ''
  );
  const [localname, setLocalname] = useState(localnameField.getValue() || '');

  const updateValues = (namespaceValue, localnameValue) => {
    const hasValue = namespaceValue && localnameValue;
    updateNamespace(namespaceField, hasValue ? namespaceValue.getURI() : '');
    updateLocalname(localnameField, hasValue ? localnameValue : '');
    onChange({ entry: namespaceValue, localname: localnameValue });
  };

  const handleNamespaceChange = (newNamespaceEntry) => {
    setNamespaceEntry(newNamespaceEntry);
    updateValues(newNamespaceEntry, localname);
  };

  const handleLocalnameChange = (newLocalname) => {
    setLocalname(newLocalname);
    updateValues(namespaceEntry, newLocalname);
  };

  const clear = () => {
    setNamespaceEntry('');
    setLocalname('');
    updateValues('', '');
    onChange({});
  };

  const handleRemove = () => {
    removeNamespace(namespaceField);
    removeLocalname(localnameField);
    onRemove();
  };

  const emptyHelperText =
    localnameField.getError() || namespaceField.getError() ? ' ' : '';

  return (
    <Fields labelId={labelId} {...fieldsProps}>
      {renderInlineContent
        ? renderInlineContent(namespaceFieldGroup, localnameFieldGroup)
        : null}
      <Field
        size={fieldsCount}
        onClear={clear}
        onRemove={handleRemove}
        spacing={2}
      >
        <Select
          xs={3}
          labelId={`namespace-edit-${labelId}`}
          value={namespaceEntry}
          choices={getNamespaceEntryChoices(namespaceEntries)}
          onChange={({ target }) => handleNamespaceChange(target.value)}
          size={size}
          label={translate('namespaceLabel')}
          error={error || Boolean(namespaceField.getError())}
          helperText={
            helperText ||
            (namespaceField.getError()
              ? translate(namespaceField.getError())
              : emptyHelperText)
          }
        />
        <TextField
          xs={5}
          size={size}
          onChange={({ target }) => handleLocalnameChange(target.value.trim())}
          value={localname}
          error={error || Boolean(localnameField.getError())}
          helperText={
            helperText ||
            (localnameField.getError()
              ? translate(localnameField.getError())
              : emptyHelperText)
          }
          label={translate('localNameLabel')}
        />
        {renderControls ? renderControls() : null}
      </Field>
    </Fields>
  );
};

NamespaceEditor.propTypes = {
  property: PropTypes.string,
  nodetype: PropTypes.string,
  size: PropTypes.string,
  graph: graphPropType,
  resourceURI: PropTypes.string,
  namespaceEntries: PropTypes.arrayOf(entryPropType),
  renderInlineContent: PropTypes.func,
  onChange: PropTypes.func,
  fieldsCount: PropTypes.number,
  onRemove: PropTypes.func,
  onClear: PropTypes.func,
  renderControls: PropTypes.func,
  helperText: PropTypes.string,
  error: PropTypes.bool,
  validate: PropTypes.func,
};

export const NamespaceEntryEditor = ({
  graph,
  resourceURI,
  property,
  editModeChoices,
  onEditModeChange,
  dispatch,
  ...props
}) => {
  const {
    blanks: [blank],
    fieldGroup,
    removeBlank,
  } = useBlankEdit({
    name: props.name,
    graph,
    resourceURI,
    rdfType: RDF_TYPE_QUALIFIED_RESOURCE,
    property,
    dispatch,
  });
  const { fieldSet } = useEditorContext();

  const handleSelectEditMode = (value) => {
    const groups = findGroupsByResourceURI(fieldSet, blank.getBlankNodeId());
    dispatch({
      type: REMOVE_FIELD_GROUPS,
      fieldGroups: [fieldGroup, ...groups],
    });
    onEditModeChange(value);
  };

  const handleChange = ({ entry, localname }) => {
    const hasValue = Boolean(entry && localname);
    blank.setAsserted(hasValue);
    if (dispatch) dispatch({ type: CHANGE, value: { entry, localname } });
  };

  const handleRemove = () => {
    removeBlank(blank);
  };

  return (
    <NamespaceEditor
      graph={graph}
      resourceURI={blank.getBlankNodeId()}
      renderInlineContent={() => (
        <SelectEditMode
          value={NAMESPACE_ENTRY}
          choices={editModeChoices}
          nlsBundles={esmoCommonsNls}
          onChange={(value) => handleSelectEditMode(value)}
        />
      )}
      onChange={handleChange}
      onRemove={handleRemove}
      {...props}
    />
  );
};

NamespaceEntryEditor.propTypes = {
  name: PropTypes.string,
  property: PropTypes.string,
  graph: graphPropType,
  resourceURI: PropTypes.string,
  editModeChoices: PropTypes.arrayOf(PropTypes.shape({})),
  onEditModeChange: PropTypes.func,
  dispatch: PropTypes.func,
};
