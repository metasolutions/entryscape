import { Entry } from '@entryscape/entrystore-js';
import {
  ACTION_EDIT,
  ACTION_REVISIONS,
} from 'commons/components/overview/actions';
import {
  LIST_ACTION_DOWNLOAD,
  LIST_ACTION_SHARING_SETTINGS,
} from 'commons/components/EntryListView/actions';
import { canEditContextMetadata } from 'commons/util/context';
import { shouldUseUriPattern } from 'commons/util/store';
import Lookup from 'commons/types/Lookup';
import { withOverviewRefresh } from 'commons/components/overview/hooks/useOverviewModel';
import RemoveContextDialog from 'commons/components/context/RemoveContextDialog';
import { withListModelProvider } from 'commons/components/ListView';
import EditTerminologyDialog from '../dialogs/EditTerminologyDialog';
import UpdateTerminologyDialog from '../dialogs/UpdateTerminologyDialog';
import UpdateTerminologyURIDialog from '../dialogs/UpdateTerminologyURIDialog';
import { getConceptSchemeEntityType } from '../../utils';

const WrapperEditDialog = withListModelProvider(EditTerminologyDialog);

/**
 * Checks whether the update terminology action should be available.
 *
 * @param {Entry} conceptSchemeEntry
 * @returns {boolean}
 */
const shouldTerminologyAllowUpdate = (conceptSchemeEntry) => {
  const contextEntry = conceptSchemeEntry?.getContext().getEntry(true);
  if (!contextEntry) return false;

  const statements = contextEntry
    .getEntryInfo()
    .getGraph()
    .find(
      contextEntry.getResourceURI(),
      'rdf:type',
      'esterms:EnhancedTerminologyContext'
    );
  return statements.length === 1;
};

const shouldTerminologyUseUriPattern = (conceptSchemeEntry) => {
  const projectType = Lookup.getProjectTypeInUse(
    conceptSchemeEntry.getContext(),
    true
  );

  if (conceptSchemeEntry.getMetadata().findFirstValue(null, 'void:uriSpace'))
    return true;

  return shouldUseUriPattern(
    getConceptSchemeEntityType(projectType),
    conceptSchemeEntry
  );
};

const ACTION_EDIT_CONTEXT_NAME = 'edit-name';
const sidebarActions = [
  {
    ...ACTION_EDIT,
    Dialog: withOverviewRefresh(WrapperEditDialog, 'onChange'),
    isVisible: ({ entry, userEntry }) =>
      canEditContextMetadata(entry, userEntry, true),
  },
  {
    ...ACTION_REVISIONS,
    isVisible: ({ entry, userEntry }) =>
      canEditContextMetadata(entry, userEntry),
  },
  {
    id: ACTION_EDIT_CONTEXT_NAME,
    isVisible: ({ entry }) => shouldTerminologyUseUriPattern(entry),
    Dialog: UpdateTerminologyURIDialog,
    labelNlsKey: 'schemeAliasTitle',
  },
  {
    id: 'update',
    isVisible: ({ entry }) => shouldTerminologyAllowUpdate(entry),
    Dialog: UpdateTerminologyDialog,
    labelNlsKey: 'updateTerminologyLabel',
  },
  LIST_ACTION_SHARING_SETTINGS,
  {
    ...LIST_ACTION_DOWNLOAD,
    action: {
      profile: 'conceptscheme',
    },
  },
  {
    id: 'remove',
    Dialog: RemoveContextDialog,
    labelNlsKey: 'removeButtonLabel',
    isVisible: ({ entry, userEntry }) =>
      canEditContextMetadata(entry, userEntry),
  },
];

export { sidebarActions };
