import { Entry } from '@entryscape/entrystore-js';
import {
  LIST_ACTION_EDIT,
  LIST_ACTION_REVISIONS,
  LIST_ACTION_REMOVE,
  LIST_ACTION_SHARING_SETTINGS,
  LIST_ACTION_DOWNLOAD,
} from 'commons/components/EntryListView/actions';
// eslint-disable-next-line max-len
import ListRemoveContextDialog from 'commons/components/EntryListView/dialogs/ListRemoveContextDialog';
import { canEditContextMetadata } from 'commons/util/context';
import { getConceptSchemeEntityType } from 'terms/utils';
import { shouldUseUriPattern } from 'commons/util/store';
import Lookup from 'commons/types/Lookup';
import config from 'config';
import esteSchemeNLS from 'terms/nls/esteScheme.nls';
import esteTerminologyNLS from 'terms/nls/esteTerminology.nls';
import esteTerminologyExportNLS from 'terms/nls/esteTerminologyexport.nls';
import escoListNLS from 'commons/nls/escoList.nls';
import CreateTerminologyDialog from '../dialogs/CreateTerminologyDialog';
import ImportTerminologyDialog from '../dialogs/ImportTerminologyDialog';
import UpdateTerminologyDialog from '../dialogs/UpdateTerminologyDialog';
import EditTerminologyDialog from '../dialogs/EditTerminologyDialog';
import UpdateTerminologyURIDialog from '../dialogs/UpdateTerminologyURIDialog';

// TODO there's too many nls for the actions. fix up
const nlsBundles = [
  esteSchemeNLS,
  esteTerminologyNLS,
  esteTerminologyExportNLS,
  escoListNLS,
];

const listActions = [
  {
    id: 'import',
    Dialog: ImportTerminologyDialog,
    labelNlsKey: 'importButtonLabel',
    tooltipNlsKey: 'actionTooltipImport',
  },
  {
    id: 'create',
    Dialog: CreateTerminologyDialog,
    labelNlsKey: 'createButtonLabel',
    tooltipNlsKey: 'actionTooltipTerminology',
  },
];

const shouldTerminologyUseUriPattern = async (conceptSchemeEntry) => {
  const projectType = await Lookup.getProjectTypeInUse(
    conceptSchemeEntry.getContext()
  );
  return shouldUseUriPattern(
    getConceptSchemeEntityType(projectType),
    conceptSchemeEntry
  );
};

/**
 * Checks whether the update terminology action should be available.
 *
 * @param {Entry} conceptSchemeEntry
 * @returns {boolean}
 */
const shouldTerminologyAllowUpdate = async (conceptSchemeEntry) => {
  const contextEntry = await conceptSchemeEntry?.getContext().getEntry();
  if (!contextEntry) return false;

  const statements = contextEntry
    .getEntryInfo()
    .getGraph()
    .find(
      contextEntry.getResourceURI(),
      'rdf:type',
      'esterms:EnhancedTerminologyContext'
    );
  return statements.length === 1;
};

const ACTION_EDIT_CONTEXT_NAME = 'edit-name';
const rowActions = [
  {
    ...LIST_ACTION_EDIT,
    isVisible: ({ entry, userEntry }) =>
      canEditContextMetadata(entry, userEntry),
    Dialog: EditTerminologyDialog,
  },
  {
    id: ACTION_EDIT_CONTEXT_NAME,
    isVisible: ({ entry }) => shouldTerminologyUseUriPattern(entry),
    Dialog: UpdateTerminologyURIDialog,
    labelNlsKey: 'schemeAliasTitle',
  },
  {
    id: 'update',
    isVisible: ({ entry }) => shouldTerminologyAllowUpdate(entry),
    Dialog: UpdateTerminologyDialog,
    labelNlsKey: 'updateTerminologyLabel',
  },
  {
    ...LIST_ACTION_REVISIONS,
    isVisible: ({ entry, userEntry }) =>
      canEditContextMetadata(entry, userEntry),
    nlsBundles,
  },
  {
    ...LIST_ACTION_SHARING_SETTINGS,
    sharingSettingsRestrictionPath: () =>
      config.get('terms.disallowTermCollaborationDialog'),
    nlsBundles,
  },
  {
    ...LIST_ACTION_DOWNLOAD,
    nlsBundles,
    profile: 'conceptscheme',
  },
  {
    ...LIST_ACTION_REMOVE,
    Dialog: ListRemoveContextDialog,
    isVisible: ({ entry, userEntry }) =>
      canEditContextMetadata(entry, userEntry),
  },
];

export { listActions, rowActions, nlsBundles };
