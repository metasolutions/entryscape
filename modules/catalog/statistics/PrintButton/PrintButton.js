import { Button } from '@mui/material';
import PrintIcon from '@mui/icons-material/Print';
import PropTypes from 'prop-types';
import { useTranslation } from 'commons/hooks/useTranslation';
import statisticsNLS from 'catalog/nls/escaStatistics.nls';
import './PrintButton.scss';

const PrintButton = ({ disabled }) => {
  const translate = useTranslation(statisticsNLS);

  const print = () => {
    window.print();
  };

  return (
    <Button
      color="secondary"
      startIcon={<PrintIcon />}
      onClick={print}
      disabled={disabled}
    >
      {translate('actionsPrint')}
    </Button>
  );
};

PrintButton.propTypes = {
  disabled: PropTypes.bool,
};

export default PrintButton;
