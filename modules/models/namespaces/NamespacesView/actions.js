import PropTypes from 'prop-types';
import { EDIT_ENTRY_COLUMN } from 'commons/components/EntryListView';
import { getIconFromActionId } from 'commons/actions';
import {
  LIST_ACTION_EDIT,
  LIST_ACTION_REMOVE,
} from 'commons/components/EntryListView/actions';
import { ACTION_INFO_ID } from 'commons/actions/actionIds';
import ModelsLDBDialog from 'models/dialogs/ModelsLDBDialog';
import { ListAction, withListRefresh } from 'commons/components/ListView';
import esmoNamespacesNLS from 'models/nls/esmoNamespaces.nls';
import esmoCommonsNLS from 'models/nls/esmoCommons.nls';
import escoListNLS from 'commons/nls/escoList.nls';
import escoDialogsNLS from 'commons/nls/escoDialogs.nls';
import { useTranslation } from 'commons/hooks/useTranslation';
import NamespaceImportDialog from '../dialogs/NamespaceImportDialog';
import NamespaceCreateDialog from '../dialogs/NamespaceCreateDialog';
import NamespaceEditDialog from '../dialogs/NamespaceEditDialog';
import NamespaceDefaultDialog from '../dialogs/NamespaceDefaultDialog';
import { isBuiltin, hasDefaultValue } from '../utils/namespaceMetadata';

const ACTION_SET_DEFAULT_ID = 'setDefault';
const ACTION_UNSET_DEFAULT_ID = 'unsetDefault';

const nlsBundles = [
  esmoNamespacesNLS,
  esmoCommonsNLS,
  escoListNLS,
  escoDialogsNLS,
];

export const CreateNamespaceButton = ({ disabled, ...actionProps }) => {
  const translate = useTranslation(nlsBundles);
  return (
    <ListAction
      label={translate('createButtonLabel')}
      action={{ Dialog: NamespaceCreateDialog, nlsBundles, ...actionProps }}
      icon={getIconFromActionId('create')}
      tooltip={translate('createNamespaceTitle')}
      disabled={disabled}
    />
  );
};

CreateNamespaceButton.propTypes = { disabled: PropTypes.bool };

export const ImportNamespaceButton = ({ disabled, ...actionProps }) => {
  const translate = useTranslation(nlsBundles);
  return (
    <ListAction
      label={translate('reuseButtonLabel')}
      action={{ Dialog: NamespaceImportDialog, nlsBundles, ...actionProps }}
      icon={getIconFromActionId('import')}
      tooltip={translate('reuseTitle')}
      disabled={disabled}
    />
  );
};

ImportNamespaceButton.propTypes = { disabled: PropTypes.bool };

export const presenterAction = {
  id: ACTION_INFO_ID,
  Dialog: ModelsLDBDialog,
};

export const EDIT_COLUMN = {
  ...EDIT_ENTRY_COLUMN,
  isVisible: ({ entry }) => !isBuiltin(entry),
  getProps: ({ entry, translate: t }) => ({
    action: {
      ...LIST_ACTION_EDIT,
      entry,
      Dialog: NamespaceEditDialog,
    },
    title: t('editEntry'),
  }),
};

const rowActions = [
  {
    id: ACTION_SET_DEFAULT_ID,
    Dialog: withListRefresh(NamespaceDefaultDialog, 'onConfirm'),
    labelNlsKey: 'setDefaultLabel',
    isVisible: ({ entry }) => !hasDefaultValue(entry),
  },
  {
    id: ACTION_UNSET_DEFAULT_ID,
    Dialog: withListRefresh(NamespaceDefaultDialog, 'onConfirm'),
    labelNlsKey: 'unsetDefaultLabel',
    isVisible: ({ entry }) => hasDefaultValue(entry),
  },
  LIST_ACTION_REMOVE,
];

export { rowActions, nlsBundles };
