import { useState } from 'react';
import { isDatasetPSI } from '../utils/datasets';

const useIsPSI = (entry) => {
  const [isPSI, setIsPSI] = useState(isDatasetPSI(entry));

  const toggleIsPSI = () => {
    if (isPSI) {
      entry
        .addD('http://entryscape.com/terms/psi', 'true', 'xsd:boolean')
        .commitMetadata();
    } else {
      entry
        .getMetadata()
        .findAndRemove(null, 'http://entryscape.com/terms/psi');
      entry.commitMetadata();
    }
    setIsPSI(!isPSI);
  };

  return [isPSI, toggleIsPSI];
};

export default useIsPSI;
