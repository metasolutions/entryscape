import { Context } from '@entryscape/entrystore-js';
import { entrystore } from 'commons/store';
import { BASE_URI } from './constants';

export const createContext = (contextId = 1) => {
  return new Context(
    `${BASE_URI}_contexts/entry/${contextId}`,
    `${BASE_URI}/contextId`,
    entrystore
  );
};
