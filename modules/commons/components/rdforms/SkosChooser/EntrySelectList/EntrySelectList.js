import React, { useMemo, useEffect, useState, useCallback } from 'react';
import PropTypes from 'prop-types';
import { FormControl, InputLabel, MenuItem, Select } from '@mui/material';
import { Entry } from '@entryscape/entrystore-js';
import { namespaces as ns } from '@entryscape/rdfjson';
import escoEntryChooserNLS from 'commons/nls/escoEntryChooser.nls';
import { useTranslation } from 'commons/hooks/useTranslation';
import Lookup from 'commons/types/Lookup';
import { applyEntityTypeParams } from 'commons/util/solr';
import { getLabel } from 'commons/util/rdfUtils';
import { getConceptPath } from 'commons/tree/skos/util';
import { RDF_TYPE_TERMINOLOGY } from 'commons/util/entry';
import { useESContext } from 'commons/hooks/useESContext';
import escoListNLS from 'commons/nls/escoList.nls';
import {
  checkUseRemoteStore,
  getQueryConfiguration,
  restrictedToContext,
} from 'commons/components/rdforms/EntryChooser/util';
import getChoiceFromEntry from 'commons/components/rdforms/EntryChooser/getChoiceFromEntry';
import {
  ListActions,
  List,
  ListHeader,
  ListHeaderItemText,
  ListHeaderItemSort,
  ListSearch,
  ListView,
  ListPagination,
  ListPlaceholder,
  useSolrSearch,
  withListModelProvider,
  REFRESH,
  RESET,
  useListModel,
} from 'commons/components/ListView';
import { RefreshButton } from 'commons/components/EntryListView';
import LinkedDataBrowserDialog from 'commons/components/LinkedDataBrowserDialog';
import { entrystore } from 'commons/store';
import { localize } from 'commons/locale';
import escoSkosChooserNLS from 'commons/nls/escoSkosChooser.nls';
import { EntrySelectListItem } from '../EntrySelectListItem';
import './index.scss';

const ROWS_PER_PAGE = 5;

const InfoDialog = ({ parentEntry, entry, closeDialog }) => (
  <LinkedDataBrowserDialog
    closeDialog={closeDialog}
    entry={entry}
    parentEntry={parentEntry}
    maxWidth="md"
  />
);

InfoDialog.propTypes = {
  parentEntry: PropTypes.instanceOf(Entry),
  entry: PropTypes.instanceOf(Entry),
  closeDialog: PropTypes.func,
};

const EntrySelectRow = ({ entry, conceptEntriesPaths, binding, onSelect }) => {
  const [showInfoDialog, setShowInfoDialog] = useState(false);

  const handleCloseInfoDialog = () => setShowInfoDialog(false);

  return (
    <>
      <EntrySelectListItem
        entry={entry}
        pathEntries={conceptEntriesPaths}
        binding={binding}
        onSelect={onSelect}
        setShowInfoDialog={setShowInfoDialog}
      />
      {showInfoDialog && (
        <InfoDialog
          parentEntry={entry}
          entry={entry}
          closeDialog={handleCloseInfoDialog}
        />
      )}
    </>
  );
};

EntrySelectRow.propTypes = {
  entry: PropTypes.instanceOf(Entry).isRequired,
  binding: PropTypes.shape({}),
  conceptEntriesPaths: PropTypes.arrayOf(PropTypes.shape({})),
  onSelect: PropTypes.func,
};

const inSchemeRestriction = (constraints) =>
  Object.keys(constraints)
    .map((k) => ns.shortenKnown(k))
    .includes('skos:inScheme');

const getStoreObjects = (entityType, property, translate) => {
  const localStore = {
    store: entrystore,
    label: translate('defaultStoreLabel'),
  };
  if (!checkUseRemoteStore(entityType, property)) return [localStore];

  const remoteStoreConfig = entityType.getStoreConfig();
  const remoteStore = {
    store: remoteStoreConfig.store,
    label: localize(remoteStoreConfig.label),
  };
  const { includeLocal } = remoteStoreConfig;
  return includeLocal === false ? [remoteStore] : [localStore, remoteStore];
};

const EntrySelectList = ({ binding, onClose, onSelect }) => {
  const nlsBundles = [escoEntryChooserNLS, escoListNLS, escoSkosChooserNLS];
  const translate = useTranslation(nlsBundles);
  const { context } = useESContext();
  const [, dispatch] = useListModel();
  const [terminologyContextId, setTerminologyContextId] = useState(0);
  const [terminologyEntries, setTerminologyEntries] = useState([]);
  const [conceptEntriesPaths, setConceptEntriesPaths] = useState([]);
  const item = binding.getItem();
  const constraints = item.getConstraints();
  const projectType = Lookup.getProjectTypeInUse(context, true);
  const entityType = Lookup.getEntityTypeByConstraints(
    constraints,
    projectType
  );
  const property = item.getProperty();

  const storeObjects = getStoreObjects(entityType, property, translate);
  const showStoreSelect = storeObjects.length > 1;
  const restrictedToSingleTerminology =
    restrictedToContext(property, entityType) ||
    inSchemeRestriction(constraints);

  const showTerminologySelect = !restrictedToSingleTerminology;

  const [selectedStore, setSelectedStore] = useState(storeObjects[0].store);

  const onChangeStore = ({ target: { value } }) => {
    const newlySelectedStore = storeObjects.find(
      ({ store }) => store.getBaseURI() === value
    ).store;
    setSelectedStore(newlySelectedStore);
  };

  const userSelectedContext =
    terminologyContextId === 0
      ? undefined
      : selectedStore.getContextById(terminologyContextId);

  const createQuery = useCallback(() => {
    const {
      searchProps,
      explicitContext,
      contextRestriction,
      invertContextRestriction,
    } = getQueryConfiguration(item);

    const query = applyEntityTypeParams(
      selectedStore.newSolrQuery(),
      {},
      {
        constraints: item.getConstraints(),
        searchProps,
      }
    );

    if (userSelectedContext) {
      query.context(userSelectedContext);
      return query;
    }
    if (explicitContext) {
      query.context(selectedStore.getContextById(explicitContext));
      return query;
    }
    if (contextRestriction && context) {
      query.context(context, invertContextRestriction);
      return query;
    }
    return query;
  }, [context, item, selectedStore, userSelectedContext]);

  const { entries, size, search, status } = useSolrSearch({
    createQuery,
    limit: ROWS_PER_PAGE,
  });

  useEffect(() => {
    setTerminologyContextId(0);
    dispatch({ type: RESET });
  }, [dispatch, selectedStore]);

  useEffect(() => {
    // loading all terminologies except current context
    selectedStore
      .newSolrQuery()
      .rdfType(RDF_TYPE_TERMINOLOGY)
      .context(context, true)
      .list()
      .getEntries()
      .then((termEntries) => {
        setTerminologyEntries(termEntries);
      });
  }, [context, selectedStore]);

  useEffect(() => {
    const pathPromises = entries.map((entry) => getConceptPath(entry));
    Promise.all(pathPromises).then(setConceptEntriesPaths);
  }, [entries]);

  const onSelectChoice = useMemo(
    () => (entry) => {
      onClose();
      onSelect(getChoiceFromEntry(entry));
    },
    [onClose, onSelect]
  );

  const changeTerminologyContext = (event) => {
    dispatch({ type: REFRESH });
    setTerminologyContextId(event.target.value);
  };

  return (
    <ListView
      status={status}
      search={search}
      size={size}
      nlsBundles={nlsBundles}
      showPlaceholder={Boolean(search)}
    >
      <ListActions disabled={size === -1}>
        <ListSearch />
        <div className="escoEntrySelectList__filters">
          {showStoreSelect ? (
            <FormControl
              variant="filled"
              margin="none"
              fullWidth
              className="escoEntrySelectList__formControl"
              style={{ marginRight: '12px' }}
            >
              <InputLabel id="store-select-label">
                {translate('storeSelectLabel')}
              </InputLabel>
              <Select
                className="escoEntrySelectList__select"
                id="store-select"
                value={selectedStore.getBaseURI()}
                onChange={onChangeStore}
                inputProps={{ 'aria-labelledby': 'store-select-label' }}
              >
                {storeObjects.map(({ store, label }) => (
                  <MenuItem key={label} value={store.getBaseURI()}>
                    {label}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>
          ) : null}
          {showTerminologySelect && (
            <FormControl
              variant="filled"
              margin="none"
              fullWidth
              className="escoEntrySelectList__formControl"
            >
              <InputLabel id="terminology-label" shrink>
                {translate('terminologyLabel')}
              </InputLabel>
              <Select
                labelId="terminology-label"
                className="escoEntrySelectList__select"
                value={terminologyContextId}
                onChange={changeTerminologyContext}
                inputProps={{ 'aria-labelledby': 'terminology-label' }}
              >
                <MenuItem value={0}>{translate('selectAll')}</MenuItem>
                {terminologyEntries.map((termItem) => (
                  <MenuItem
                    value={termItem.getContext().getId()}
                    key={termItem.getContext().getId()}
                  >
                    {getLabel(termItem)}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>
          )}
        </div>
        <RefreshButton />
      </ListActions>
      <ListHeader>
        <ListHeaderItemText xs={1} text="" />
        <ListHeaderItemText xs={7} text={translate('listHeaderLabel')} />
        <ListHeaderItemSort
          sortBy="modified"
          text={translate('listHeaderModifiedDate')}
        />
      </ListHeader>
      <List
        renderPlaceholder={() => (
          <ListPlaceholder label={translate('emptyMessage')} />
        )}
      >
        {entries.map((entry, idx) => (
          <EntrySelectRow
            key={`${entry.getId()}-${entry.getContext().getId()}`}
            entry={entry}
            binding={binding}
            onSelect={onSelectChoice}
            conceptEntriesPaths={conceptEntriesPaths[idx]}
          />
        ))}
      </List>
      <ListPagination />
    </ListView>
  );
};

EntrySelectList.propTypes = {
  binding: PropTypes.shape({ getItem: PropTypes.func }),
  onClose: PropTypes.func,
  onSelect: PropTypes.func,
};

export default withListModelProvider(EntrySelectList);
