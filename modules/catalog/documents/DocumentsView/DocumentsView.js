import { useCallback } from 'react';
import Lookup from 'commons/types/Lookup';
import { useESContext } from 'commons/hooks/useESContext';
import { RDF_TYPE_DOCUMENT } from 'commons/util/entry';
import { entrystore } from 'commons/store';
import { getPathFromViewName } from 'commons/util/site';
import config from 'config';
import CreateDocumentDialog from 'catalog/documents/dialogs/CreateDocumentDialog';
import ImportSpecificationDialog from 'catalog/documents/dialogs/ImportDocumentDialog';
import {
  EntryListView,
  useSolrQuery,
  TITLE_COLUMN,
  MODIFIED_COLUMN,
  PUBLIC_STATUS_COLUMN,
  ACTIONS_GROUP_COLUMN,
} from 'commons/components/EntryListView';
import {
  LIST_ACTION_INFO,
  LIST_ACTION_EDIT,
} from 'commons/components/EntryListView/actions';
import {
  withListModelProviderAndLocation,
  useListModel,
  REFRESH,
  listPropsPropType,
} from 'commons/components/ListView';
import escoListNLS from 'commons/nls/escoList.nls';
import escaDocumentsNLS from 'catalog/nls/escaDocuments.nls';
import { IMPORTED_SPEC_TAG } from '../tags';

const nlsBundles = [escaDocumentsNLS, escoListNLS];

const DocumentsView = ({ listProps }) => {
  const [, dispatch] = useListModel();
  const actionParams = {
    entityType: Lookup.getByName('datasetDocument').get(),
    refreshDocuments: () => dispatch({ type: REFRESH }),
  };

  const { context } = useESContext();
  const createQuery = useCallback(
    () => entrystore.newSolrQuery().rdfType(RDF_TYPE_DOCUMENT).context(context),
    [context]
  );
  const queryResults = useSolrQuery({ createQuery });

  const listActions = [
    ...(config.get('catalog.remoteSpecEndpoint')
      ? [
          {
            id: 'import',
            Dialog: ImportSpecificationDialog,
            labelNlsKey: 'importButton',
            highlightId: 'listActionImport',
          },
        ]
      : []),
    {
      id: 'create',
      Dialog: CreateDocumentDialog,
      labelNlsKey: 'createButtonLabel',
      highlightId: 'listActionCreate',
    },
  ];

  return (
    <EntryListView
      {...queryResults}
      {...listProps}
      nlsBundles={nlsBundles}
      getListItemProps={({ entry }) => ({
        to: getPathFromViewName('catalog__documents__document', {
          contextId: context.getId(),
          entryId: entry.getId(),
        }),
      })}
      listActions={listActions}
      listActionsProps={{
        nlsBundles,
        actionParams,
      }}
      columns={[
        { ...PUBLIC_STATUS_COLUMN },
        {
          ...TITLE_COLUMN,
          xs: 8,
          tags: IMPORTED_SPEC_TAG,
        },
        MODIFIED_COLUMN,
        {
          ...ACTIONS_GROUP_COLUMN,
          actions: [LIST_ACTION_INFO, LIST_ACTION_EDIT],
        },
      ]}
    />
  );
};

DocumentsView.propTypes = {
  listProps: listPropsPropType,
};

export default withListModelProviderAndLocation(DocumentsView);
