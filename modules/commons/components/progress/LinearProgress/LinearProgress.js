import PropTypes from 'prop-types';
import MuiLinearProgress from '@mui/material/LinearProgress';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import './LinearProgress.scss';

/**
 * @param {number} mandatoryTotalCount
 * @param {number} mandatoryProgressCount
 * @param {boolean} disabled
 * @returns {string}
 */
const getColorClass = (
  mandatoryTotalCount,
  mandatoryProgressCount,
  disabled
) => {
  if (disabled) {
    return 'escoLinearProgress__bar--gray';
  }

  return mandatoryTotalCount === mandatoryProgressCount
    ? 'escoLinearProgress__bar--green'
    : 'escoLinearProgress__bar--red';
};

const LinearProgress = ({ progress, disabled = false, className = '' }) => {
  const {
    totalCount,
    progressCount,
    mandatoryTotalCount,
    mandatoryProgressCount,
  } = progress;

  const progressPercent = totalCount
    ? Math.round((progressCount * 100) / totalCount)
    : 0;

  return (
    <Box display="flex" alignItems="center" className={className}>
      <Box width="100%" mr={1}>
        <MuiLinearProgress
          value={progressPercent}
          variant="determinate"
          classes={{
            colorPrimary: 'escoLinearProgress__background',
            root: 'escoLinearProgress',
            bar1Determinate: getColorClass(
              mandatoryTotalCount,
              mandatoryProgressCount,
              disabled
            ),
          }}
        />
      </Box>
      <Box minWidth={35}>
        <Typography
          variant="body2"
          color="textSecondary"
        >{`${progressCount}/${totalCount}`}</Typography>
      </Box>
    </Box>
  );
};

LinearProgress.propTypes = {
  progress: PropTypes.shape({
    progressCount: PropTypes.number,
    totalCount: PropTypes.number,
    mandatoryTotalCount: PropTypes.number,
    mandatoryProgressCount: PropTypes.number,
  }),
  disabled: PropTypes.bool,
  className: PropTypes.string,
};

export default LinearProgress;
