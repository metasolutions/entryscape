import { useRef, useEffect } from 'react';
import { Box } from '@mui/material';
import PropTypes from 'prop-types';
import { useOpenlayers } from './hooks/useOpenlayers';
import { useMapContext as useOpenlayersMapContext } from './hooks/useMapContext';
import { useMapCursor } from './hooks/useMapCursor';
import './Map.scss';

export const Map = ({
  height = '100%',
  width = '100%',
  mapOptions,
  children,
}) => {
  const mapContainer = useRef();
  const [, dispatch] = useOpenlayersMapContext();
  const { createMap, status } = useOpenlayers();
  useMapCursor();

  useEffect(() => {
    if (status === 'resolved') {
      dispatch({
        type: 'CREATE_MAP',
        map: createMap({
          target: mapContainer.current,
          ...mapOptions,
        }),
      });
    }
  }, [status, dispatch, createMap, mapOptions]);

  return (
    <div className="escoMap">
      <Box
        className="escoMap__container"
        ref={mapContainer}
        sx={{ height, width }}
        tabIndex={0}
      >
        {children}
      </Box>
    </div>
  );
};

Map.propTypes = {
  height: PropTypes.string,
  width: PropTypes.string,
  mapOptions: PropTypes.shape({
    addScaleLine: PropTypes.bool,
  }),
  children: PropTypes.node,
};
