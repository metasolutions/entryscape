import { useCallback, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Entry } from '@entryscape/entrystore-js';
import InfoIcon from '@mui/icons-material/Info';
import OpenInNewIcon from '@mui/icons-material/OpenInNew';
import { entrystore } from 'commons/store';
import { Grid, ListItemButton as MuiListItemButton } from '@mui/material';
import escoListNLS from 'commons/nls/escoList.nls';
import escoDialogsNLS from 'commons/nls/escoDialogs.nls';
import { useTranslation } from 'commons/hooks/useTranslation';
import {
  List,
  ListItemText,
  ListHeader,
  ListHeaderItemText,
  ListHeaderItemSort,
  ListView,
  ListPagination,
  withListModelProvider,
  ListItemIcon,
  ListItemIconButton,
} from 'commons/components/ListView';
import useAsync, { PENDING, IDLE } from 'commons/hooks/useAsync';
import Loader from 'commons/components/Loader';
import { getLabel } from 'commons/util/rdfUtils';
import { getShortModifiedDate } from 'commons/util/metadata';
import { useSolrQuery } from 'commons/components/EntryListView';
import Lookup from 'commons/types/Lookup';
import {
  getCanAccessOverview,
  getOverviewName,
  getTo,
} from 'commons/util/overview';

const nlsBundles = [escoListNLS, escoDialogsNLS];
const ROWS_PER_PAGE = 3;

const ReferencesList = ({ entry: referencedEntry, onRowClick }) => {
  const translate = useTranslation(nlsBundles);
  const { runAsync, data: items } = useAsync({ data: [] });

  const createQuery = useCallback(
    () =>
      entrystore
        .newSolrQuery()
        .objectUri([referencedEntry.getResourceURI(), referencedEntry.getURI()])
        .uri(referencedEntry.getURI(), true),
    [referencedEntry]
  );
  const { status, search, size, entries } = useSolrQuery({
    createQuery,
  });

  useEffect(() => {
    const makeItems = () =>
      Promise.all(
        entries.map(async (entry) => {
          try {
            const entityType = await Lookup.inUse(entry);
            if (!entityType) {
              console.error(
                `Could not find entity type for entry with URI: ${entry.getURI()}`
              );
              return {
                entry,
                canAccessOverview: false,
              };
            }

            const canAccessOverview = entityType
              ? getCanAccessOverview(entry, entityType)
              : false;

            const overviewName = entityType
              ? getOverviewName(entityType, entry.getContext())
              : null;

            const url = canAccessOverview
              ? await getTo(entityType, overviewName, entry)
              : null;
            return {
              entry,
              url,
              canAccessOverview,
            };
          } catch (error) {
            console.error(error);
            return {
              entry,
              canAccessOverview: false,
            };
          }
        })
      );
    runAsync(makeItems());
  }, [runAsync, entries]);

  const handleOverviewNavigate = (url) => {
    window.open(url, '_blank');
  };

  return (
    <div>
      {status === PENDING || status === IDLE ? (
        <Loader className="escoReferencesList__loader" />
      ) : (
        <ListView
          status={status}
          search={search}
          size={size}
          nlsBundles={nlsBundles}
          renderPlaceholder={(Placeholder) => (
            <Placeholder
              className="escoReferencesList__placeholder"
              label={translate('emptyReferenceListWarning')}
            />
          )}
        >
          <ListHeader>
            <ListHeaderItemText
              xs={8}
              text={translate('referencesListHeader')}
            />
            <ListHeaderItemSort
              xs={4}
              sortBy="modified"
              text={translate('referencesListHeaderModified')}
            />
          </ListHeader>
          <List>
            {items.map(({ entry, canAccessOverview, url }) => (
              <MuiListItemButton
                classes={{ root: 'escoListViewListItem' }}
                onClick={() => onRowClick({ entry })}
                disableRipple
                key={`${entry.getId()}-${entry.getContext().getId()}`}
              >
                <Grid
                  container
                  spacing={2}
                  alignItems="center"
                  className="escoListViewListItem__gridContainer"
                >
                  <ListItemText
                    xs={8}
                    primary={getLabel(entry) || translate('unnamedLabel')}
                  />
                  <ListItemText
                    xs={2}
                    secondary={getShortModifiedDate(entry)}
                  />
                  <ListItemIcon icon={<InfoIcon />} />
                  {canAccessOverview ? (
                    <ListItemIconButton
                      title={translate('overviewIconTooltip')}
                      onClick={() => handleOverviewNavigate(url)}
                      xs={1}
                    >
                      <OpenInNewIcon />
                    </ListItemIconButton>
                  ) : null}
                </Grid>
              </MuiListItemButton>
            ))}
          </List>
          <ListPagination />
        </ListView>
      )}
    </div>
  );
};

ReferencesList.propTypes = {
  entry: PropTypes.instanceOf(Entry),
  onRowClick: PropTypes.func,
};

export default withListModelProvider(ReferencesList, () => ({
  limit: ROWS_PER_PAGE,
}));
