import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { useListModel, CREATE } from 'commons/components/ListView';
import { TextField, Checkbox, FormControlLabel } from '@mui/material';
import ContentWrapper from 'commons/components/common/ContentWrapper';
import { useTranslation } from 'commons/hooks/useTranslation';
import ListActionDialog from 'commons/components/common/dialogs/ListActionDialog';
import { entrystore as store } from 'commons/store';
import { i18n } from 'esi18n';
import esadContext from 'admin/nls/esadContext.nls';
import { removeWhitespace } from 'commons/util/util';
import { useSnackbar } from 'commons/hooks/useSnackbar';
import useAsync, { PENDING } from 'commons/hooks/useAsync';
import LoadingButton from 'commons/components/LoadingButton';
import { ErrorCatcher } from 'commons/errors/ErrorCatcher';
import { ErrorWithMessage } from 'commons/errors/utils/errors';
import { useConfirmCloseAction } from 'commons/components/EntryListView/dialogs/create/handlers';

const CreateProjectDialog = ({ closeDialog }) => {
  const [, dispatch] = useListModel();
  const [contextName, setContextName] = useState('');
  const [entryId, setEntryId] = useState('');
  const [fullname, setFullname] = useState('');
  const [contextErrorMsg, setContextErrorMsg] = useState('');
  const [EntryErrorMsg, setEntryErrorMsg] = useState('');
  const [advancedOption, setAdvancedOption] = useState(false);
  const [contextTimer, setContextTimer] = useState(null);
  const [entryIdTimer, setEntryIdTimer] = useState(null);
  const translate = useTranslation(esadContext);
  const [addSnackbar] = useSnackbar();
  const { runAsync, status, error: createError } = useAsync();
  const confirmClose = useConfirmCloseAction(closeDialog);
  const close = () =>
    confirmClose(fullname !== '' || contextName !== '' || entryId !== '');

  useEffect(() => {
    if (!advancedOption) {
      setContextName('');
      setEntryId('');
      setContextErrorMsg('');
      setEntryErrorMsg('');
    }
  }, [advancedOption]);

  const contextNameValidation = async () => {
    let isValid = true;
    setContextErrorMsg('');
    if (contextName.trim()) {
      try {
        const data = await store
          .getREST()
          .get(`${store.getBaseURI()}_contexts?entryname=${contextName}`);
        if (data.length > 0) {
          setContextErrorMsg('contextnameTaken');
          isValid = false;
        }
      } catch (e) {
        console.log(e);
      }
    }
    return isValid;
  };

  const entryIdValidation = async () => {
    let isValid = true;
    setEntryErrorMsg('');
    if (entryId.trim()) {
      try {
        const data = await store
          .getREST()
          .get(`${store.getBaseURI()}_contexts/entry/${entryId}`);
        if (Object.keys(data).length > 0) {
          setEntryErrorMsg('entryIdTaken');
          isValid = false;
        }
      } catch (e) {
        console.log(e);
      }
    }
    return isValid;
  };

  useEffect(() => {
    if (contextName) {
      if (contextTimer) {
        clearTimeout(contextTimer);
      }
      const time = setTimeout(contextNameValidation, 300);
      setContextTimer(time);
    }
  }, [contextName]);

  useEffect(() => {
    if (entryId) {
      if (entryIdTimer) {
        clearTimeout(entryIdTimer);
      }
      const time = setTimeout(entryIdValidation, 300);
      setEntryIdTimer(time);
    }
  }, [entryId]);

  const submitHandler = async () => {
    const contextEntry = store.newContext(contextName, entryId || undefined);
    const language = i18n.getLocale();
    const metadata = contextEntry.getMetadata();
    metadata.addL(
      contextEntry.getResourceURI(),
      'dcterms:title',
      fullname,
      language
    );
    return contextEntry
      .commit()
      .then(closeDialog)
      .then(() => {
        clearTimeout(contextTimer);
        clearTimeout(entryIdTimer);
        setContextTimer(null);
        setEntryIdTimer(null);
      })
      .then(() => {
        dispatch({ type: CREATE });
        addSnackbar({ message: translate('createProjectSuccess') });
      })
      .catch((error) => {
        throw new ErrorWithMessage(translate('createProjectFail'), error);
      });
  };

  const dialogActions = (
    <LoadingButton
      autoFocus
      onClick={() => runAsync(submitHandler())}
      loading={status === PENDING}
      disabled={!fullname || !!EntryErrorMsg || !!contextErrorMsg}
    >
      {translate('createButton')}
    </LoadingButton>
  );

  return (
    <>
      <ListActionDialog
        id="create-entry"
        title={translate('newContextnameHeader')}
        actions={dialogActions}
        closeDialog={close}
        maxWidth="md"
      >
        <ContentWrapper>
          <form autoComplete="off">
            {advancedOption && (
              <>
                <TextField
                  id="contextName"
                  label={translate('createContextname')}
                  placeholder={translate('setNamePlaceholder')}
                  variant="filled"
                  value={contextName}
                  onChange={(e) => {
                    setContextName(e.target.value.trim());
                    setContextErrorMsg('');
                  }}
                  error={!!contextErrorMsg}
                  helperText={!!contextErrorMsg && translate(contextErrorMsg)}
                />
                <TextField
                  id="entryId"
                  label={translate('createEntryId')}
                  placeholder={translate('createEntryIdPlaceholder')}
                  variant="filled"
                  value={entryId}
                  onChange={(e) => {
                    setEntryId(e.target.value.trim());
                    setEntryErrorMsg('');
                  }}
                  error={!!EntryErrorMsg}
                  helperText={!!EntryErrorMsg && translate(EntryErrorMsg)}
                />
              </>
            )}
            <TextField
              id="fullname"
              label={translate('createFullname')}
              placeholder={translate('createFullnamePlaceholder')}
              variant="filled"
              value={fullname}
              onChange={(e) => {
                setFullname(removeWhitespace(e.target.value));
              }}
            />
            <FormControlLabel
              control={
                <Checkbox
                  color="default"
                  inputProps={{ 'aria-label': translate('advancedOptions') }}
                  onClick={() => setAdvancedOption(!advancedOption)}
                />
              }
              label={translate('advancedOptions')}
            />
          </form>
        </ContentWrapper>
      </ListActionDialog>
      <ErrorCatcher error={createError} />
    </>
  );
};

CreateProjectDialog.propTypes = {
  closeDialog: PropTypes.func,
};

export default CreateProjectDialog;
