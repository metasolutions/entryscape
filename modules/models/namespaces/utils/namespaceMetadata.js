import * as ns from 'models/utils/ns';
import { entrystore } from 'commons/store';
import { Entry } from '@entryscape/entrystore-js';

/**
 * Gets the abbreviation of the namespace entry
 *
 * @param {Entry} entry
 * @returns {string}
 */
export const getNamespaceAbbreviation = (entry) =>
  entry?.getMetadata().findFirstValue(null, ns.RDF_TYPE_PREFIX) || '';

/**
 * Gets the uri of the namespace entry
 *
 * @param {Entry} entry
 * @returns {string}
 */
export const getNamespaceUri = (entry) =>
  entry?.getMetadata().findFirstValue(null, ns.RDF_TYPE_NAMESPACE) || '';

/**
 * Determines whether or not the namespace entry has a default value
 *
 * @param {Entry} entry
 * @returns {boolean}
 */
export const hasDefaultValue = (entry) =>
  Boolean(
    entry
      ?.getMetadata()
      .findFirstValue(entry.getResourceURI(), 'esterms:default')
  );

/**
 * Finds the entry which has a default value from an array of entries
 *
 * @param {Entry[]} entries
 * @returns {Entry}
 */
export const getDefaultEntry = (entries) =>
  entries?.find((entry) => hasDefaultValue(entry)) || null;

/**
 * Determines whether or not the namespace entry is builtin
 *
 * @param {Entry} entry
 * @returns {boolean}
 */
export const isBuiltin = (entry) =>
  Boolean(
    entry
      ?.getMetadata()
      .findFirstValue(entry.getResourceURI(), 'esterms:builtin')
  );

/**
 * Finds the entry from an array of entries which has the specified abbreviation
 *
 * @param {string} abbreviation
 * @param {Entry[]} entries
 * @returns {Entry}
 */
export const getNamespaceEntryFromAbbreviation = (abbreviation, entries) => {
  return entries?.find(
    (entry) => getNamespaceAbbreviation(entry) === abbreviation
  );
};

/**
 *
 * @param {string} uri
 * @param {Entry[]} namespaceEntries
 * @returns {object}
 */
export const extractNamespaceParts = (uri, namespaceEntries) => {
  const namespace = namespaceEntries
    .map((namespaceEntry) => getNamespaceUri(namespaceEntry))
    .sort((ns1, ns2) => ns1.length - ns2.length)
    .find((namespaceUri) => uri.startsWith(namespaceUri));
  const localname = namespace && uri.replace(namespace, '');
  return { uri, namespace, localname };
};

/**
 * Gets all namespaces entries in context
 *
 * @param {object} context
 * @returns {Promise.<Entry[]>}
 */
export const getAllEntries = async (context) =>
  entrystore
    .newSolrQuery()
    .context(context)
    .rdfType(ns.RDF_TYPE_PREFIX)
    .list()
    .getAllEntries()
    .then((entries) => entries);
