import { Fragment } from 'react';
import { Link as MuiLink } from '@mui/material';
import { Fields } from 'commons/components/forms/presenters/Fields';
import { DATASET_OVERVIEW_NAME } from 'catalog/config/config';
import { useParams, Link } from 'react-router-dom';
import {
  OverviewDescription,
  useOverviewModel,
} from 'commons/components/overview';
import config from 'config';
import { useEntry } from 'commons/hooks/useEntry';
import {
  getLabel,
  getFullLengthLabel,
  getDescription,
} from 'commons/util/rdfUtils';
import { truncate } from 'commons/util/util';
import './index.scss';
import {
  getIdeas,
  getParentCatalogEntry,
  getShowcases,
  getThemeLabels,
  isType,
} from 'commons/util/metadata';
import CommentsDialog from 'commons/CommentsDialog';
import { getPathFromViewName, getViewDefFromName } from 'commons/util/site';
import escaDatasetNLS from 'catalog/nls/escaDataset.nls';
import escaVisualizationNLS from 'catalog/nls/escaVisualization.nls';
import escaCatalogNLS from 'catalog/nls/escaCatalog.nls';
import escoListNLS from 'commons/nls/escoList.nls';
import escaResultsNLS from 'catalog/nls/escaResults.nls';
import escaIdeasNLS from 'catalog/nls/escaIdeas.nls';
import escoDialogsNLS from 'commons/nls/escoDialogs.nls';
import { useTranslation } from 'commons/hooks/useTranslation';
import useAsyncCallback from 'commons/hooks/useAsyncCallback';
import useDocumentTitle from 'commons/hooks/useDocumentTitle';
import usePageTitle from 'commons/hooks/usePageTitle';
import useGetContributors from 'commons/hooks/useGetContributors';
import {
  RDF_TYPE_DATASET,
  RDF_TYPE_IDEA,
  RDF_TYPE_SHOWCASE,
} from 'commons/util/entry';
import {
  ACTION_EDIT,
  ACTION_REMOVE,
  ACTION_DIVIDER,
  DESCRIPTION_UPDATED,
  ACTION_REVISIONS,
} from 'commons/components/overview/actions';
import { localize } from 'commons/locale';
import useCommentsCount from 'commons/CommentsDialog/hooks/useCommentsCount';
import DatasetAssociatedEntriesDialog from './dialogs/DatasetAssociatedEntriesDialog';
import CopyDatasetDialog from './dialogs/CopyDatasetDialog';
import RemoveDatasetDialog from './dialogs/RemoveDatasetDialog/RemoveDatasetDialog';
import ManageSeriesDialog from './dialogs/ManageSeriesDialog';
import useGetSeriesIncludingDataset from './useGetSeriesIncludingDataset';
import {
  DatasetPSISwitch,
  PublishDataset,
  PublishDatasetInternal,
} from './PublishDataset';
import useDatasetStatus from '../hooks/useDatasetStatus';

export const nlsBundles = [
  escaDatasetNLS,
  escaCatalogNLS,
  escoListNLS,
  escaVisualizationNLS,
];

const renderPrimaryContent = (datasetEntry, datasetSeries, translate) => {
  if (!datasetSeries.length) return null; // defaults to just the description

  // TODO: remove this when we stop allowing drafts without labels
  const seriesWithLabel = datasetSeries.filter((seriesEntry) =>
    getLabel(seriesEntry)
  );

  return (
    <>
      <Fields label={translate('overviewInSeries')} inline>
        {seriesWithLabel.map((seriesEntry, index) => {
          const path = getPathFromViewName(DATASET_OVERVIEW_NAME, {
            contextId: seriesEntry.getContext().getId(),
            entryId: seriesEntry.getId(),
          });
          const label = getLabel(seriesEntry);
          if (!label) return;
          return (
            <Fragment key={path}>
              <MuiLink component={Link} to={path}>
                {label}
              </MuiLink>
              {index === seriesWithLabel.length - 1 ||
              (index === 0 && seriesWithLabel.length === 1)
                ? ''
                : ', '}
            </Fragment>
          );
        })}
      </Fields>
      <OverviewDescription label={getDescription(datasetEntry)} />
    </>
  );
};

const useDatasetOverviewCommons = () => {
  const entry = useEntry();

  const translate = useTranslation(nlsBundles);
  const translateShowIdeas = useTranslation(escaIdeasNLS);
  const translateShowcases = useTranslation(escaResultsNLS);

  const [catalogEntry] = useAsyncCallback(getParentCatalogEntry, entry);
  const [{ refreshCount }] = useOverviewModel();
  const { contributors } = useGetContributors(entry, refreshCount);
  const [ideas] = useAsyncCallback(getIdeas, entry);
  const [showcases] = useAsyncCallback(getShowcases, entry);
  const viewParams = useParams();
  const [getCommentsCount, refreshCommentsCount] = useCommentsCount(entry);
  const commentCount = getCommentsCount(entry.getResourceURI());

  const includeSuggestions = config.get('catalog.includePreparations');

  const [pageTitle] = usePageTitle();
  const viewDefinition = getViewDefFromName('catalog__datasets__dataset');
  const viewDefinitionTitle = localize(viewDefinition.title);

  const ideaCount = ideas?.length || 0;
  const showcaseCount = showcases?.length || 0;
  const themes = getThemeLabels(entry);
  const includeInternalPublish = config.get('catalog.includeInternalPublish');
  const includePSISwitch = config.get('catalog.allowInternalDatasetPublish');

  const [datasetStatus, updateStatus] = useDatasetStatus(entry);
  const { isPublished, isInternallyPublished } = datasetStatus;

  const datasetSeriesEnabled = config.get('catalog.includeDatasetSeries');
  const [datasetSeries, refreshSeries] = useGetSeriesIncludingDataset(entry);

  const descriptionItems = [
    {
      id: 'overview',
      labelNlsKey: 'overviewCatalogLabel',
      getValues: () =>
        catalogEntry ? [truncate(getFullLengthLabel(catalogEntry), 90)] : [],
    },
    {
      id: 'theme',
      labelNlsKey: 'themeTitle',
      getValues: () => (themes ? [...new Set(themes)] : []),
    },
    DESCRIPTION_UPDATED,
    {
      id: 'edited',
      labelNlsKey: 'editedTitle',
      getValues: () => contributors,
    },
  ];

  useDocumentTitle(`${pageTitle} - ${viewDefinitionTitle} ${getLabel(entry)}`);

  const rowActions = [
    {
      id: 'comments',
      Dialog: CommentsDialog,
      refreshCommentsCount,
      getProps: () => ({
        labelPrefix: commentCount ?? 0,
        label: translate('commentMenu'),
      }),
    },
    {
      id: 'ideas',
      getProps: () => ({
        labelPrefix: ideaCount,
        label: translate('showideas'),
        action: {
          Dialog: DatasetAssociatedEntriesDialog,
          datasetEntry: entry,
          associatedEntryType: RDF_TYPE_IDEA,
          associatedEntryNLSBundles: [escoDialogsNLS, escaIdeasNLS],
          dialogHeaderNLSKey: 'showIdeasDialogHeader',
          listPlaceholder: translateShowIdeas('emptyShowListWarning'),
        },
      }),
    },
    {
      id: 'showcases',
      getProps: () => ({
        labelPrefix: showcaseCount,
        label: translate('showresults'),
        action: {
          Dialog: DatasetAssociatedEntriesDialog,
          datasetEntry: entry,
          associatedEntryType: RDF_TYPE_SHOWCASE,
          associatedEntryNLSBundles: [escoDialogsNLS, escaResultsNLS],
          dialogHeaderNLSKey: 'showResultsDialogHeader',
          listPlaceholder: translateShowcases('emptyShowListWarning'),
        },
      }),
    },
  ];

  const sidebarActions = [
    ACTION_EDIT,
    ACTION_REVISIONS,
    {
      id: 'preview',
      labelNlsKey: 'previewEntry',
      getProps: () => ({
        color: 'secondary',
        target: '_blank',
        href: getPathFromViewName('catalog__datasets__preview', viewParams),
        label: translate('previewDatasetTitle'),
      }),
    },
    {
      id: 'copy',
      highlightId: 'overviewActionCopy',
      labelNlsKey: 'copyEntry',
      Dialog: CopyDatasetDialog,
      label: translate('cloneMenu'),
    },
    {
      id: 'manage-series',
      labelNlsKey: 'manageSeriesButton',
      Dialog: ManageSeriesDialog,
      action: { refreshSeries },
      // TODO: remove when we allow series inside series
      isVisible: () =>
        datasetSeriesEnabled &&
        isType(entry.getMetadata(), entry.getResourceURI(), RDF_TYPE_DATASET),
    },
    {
      ...ACTION_REMOVE,
      getProps: () => ({
        color: 'secondary',
        disabled: isPublished || isInternallyPublished,
      }),
      action: {
        removeConfirmMessage: translate('removeDatasetQuestion'),
        isPublished,
        datasetEntry: entry,
      },
      Dialog: RemoveDatasetDialog,
    },
    ACTION_DIVIDER,
    {
      id: 'publish-internal',
      Component: PublishDatasetInternal,
      isVisible: () => includeInternalPublish,
      getProps: () => ({
        entry,
        translate,
        datasetStatus,
        onChange: updateStatus,
      }),
    },
    {
      id: 'publish',
      Component: PublishDataset,
      getProps: () => ({
        entry,
        catalogEntry,
        translate,
        datasetStatus,
        includeInternalPublish,
        onChange: updateStatus,
      }),
    },
    {
      id: 'psi',
      Component: DatasetPSISwitch,
      isVisible: () => includePSISwitch && !includeInternalPublish,
      getProps: () => ({
        translate,
        entry,
      }),
    },
  ];

  return {
    entry,
    descriptionItems,
    sidebarActions,
    rowActions,
    includeSuggestions,
    backLabel: translate('backTitle'),
    translate,
    datasetSeries,
    renderPrimaryContent: () =>
      renderPrimaryContent(entry, datasetSeries, translate),
  };
};

export default useDatasetOverviewCommons;
