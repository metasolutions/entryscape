import PropTypes from 'prop-types';
import { useState, useEffect, useCallback } from 'react';
import { Entry } from '@entryscape/entrystore-js';
import Lookup from 'commons/types/Lookup';
import skosUtil from 'commons/tree/skos/util';
import { isUri } from 'commons/util/util';
import {
  constructURIFromPattern,
  spreadEntry,
  checkUriAndPatternMatch,
  isResourceURIUniqueInContext,
} from 'commons/util/store';
import { getConceptEntityType, getConceptSchemeEntityType } from 'terms/utils';
import {
  Button,
  Checkbox,
  FormControlLabel,
  TextField,
  InputAdornment,
} from '@mui/material';
import ContentWrapper from 'commons/components/common/ContentWrapper';
import ProgressList from 'commons/components/common/ProgressList';
import {
  useTasksList,
  STATUS_PROGRESS,
  STATUS_DONE,
} from 'commons/hooks/useTasksList';
import { useEntityURI } from 'commons/hooks/useEntityTitleAndURI';
import { useTranslation } from 'commons/hooks/useTranslation';
import esteTerminologyNLS from 'terms/nls/esteTerminology.nls';
import esteSchemeNLS from 'terms/nls/esteScheme.nls';
import { useMainDialog } from 'commons/hooks/useMainDialog';
import { AcknowledgeAction } from 'commons/components/common/dialogs/MainDialog';
import ListActionDialog from 'commons/components/common/dialogs/ListActionDialog';
import { TASKS_UPDATE_TERMINOLOGY_URI } from '../tasks';

const NLS_BUNDLES = [esteTerminologyNLS, esteSchemeNLS];

// form used when the terminology's metadata contain a `void:uriSpace` that
// does NOT match the entity type's uri pattern in config
const UriSpaceForm = ({
  uriSpace,
  setUriSpaceInput,

  schemeResourceUriInput,
  setSchemeResourceUriInput,
  setActionIsDisabled,
}) => {
  const translate = useTranslation(NLS_BUNDLES);
  const [pristine, setPristine] = useState(true);
  const [allowSeparateUris, setAllowSeparateUris] = useState(
    uriSpace !== schemeResourceUriInput
  );
  const [isSchemeRuriUnique, setIsSchemeRuriUnique] = useState(true);
  const baseUriError = !uriSpace || !isUri(uriSpace);
  const schemeResourceError =
    !schemeResourceUriInput ||
    !isUri(schemeResourceUriInput) ||
    !isSchemeRuriUnique;

  const handleCheckboxChange = () => setAllowSeparateUris((prev) => !prev);
  const handleUriSpaceChange = (event) => {
    const newUriSpace = event.target.value;
    setUriSpaceInput(newUriSpace);
    setPristine(false);
    if (allowSeparateUris) return;
    setSchemeResourceUriInput(newUriSpace);
  };
  const handleSchemeRuriChange = (event) => {
    setSchemeResourceUriInput(event.target.value);
    setPristine(false);
  };

  useEffect(() => {
    setActionIsDisabled(baseUriError || schemeResourceError);
  }, [baseUriError, schemeResourceError, setActionIsDisabled]);

  useEffect(() => {
    isResourceURIUniqueInContext(schemeResourceUriInput).then((isUnique) =>
      setIsSchemeRuriUnique(isUnique)
    );
  }, [schemeResourceUriInput, translate]);

  const getBaseUriHelperText = () => {
    if (pristine) return;
    if (!uriSpace) return translate('requiredFieldError');
    if (!isUri(uriSpace)) return translate('invalidUrlError');
    return '';
  };

  const getSchemeRuriHelperText = () => {
    if (pristine) return;
    if (!schemeResourceUriInput) return translate('requiredFieldError');
    if (!isUri(schemeResourceUriInput)) return translate('invalidUrlError');
    if (!isSchemeRuriUnique) return translate('unavailableURLFeedback');
  };

  return (
    <>
      <TextField
        label={translate('baseUriLabel')}
        value={uriSpace}
        onChange={handleUriSpaceChange}
        error={!pristine && baseUriError}
        helperText={getBaseUriHelperText()}
      />
      <FormControlLabel
        label={translate('separateUriCheckboxLabel')}
        control={
          <Checkbox
            checked={allowSeparateUris}
            onChange={handleCheckboxChange}
            inputProps={{
              'aria-label': translate('separateUriCheckboxLabel'),
            }}
          />
        }
      />
      <TextField
        disabled={!allowSeparateUris}
        label={translate('terminologyRuriLabel')}
        value={schemeResourceUriInput}
        onChange={handleSchemeRuriChange}
        error={!pristine && schemeResourceError}
        helperText={getSchemeRuriHelperText()}
      />
    </>
  );
};

UriSpaceForm.propTypes = {
  uriSpace: PropTypes.string,
  setUriSpaceInput: PropTypes.func,
  schemeResourceUriInput: PropTypes.string,
  setSchemeResourceUriInput: PropTypes.func,
  setActionIsDisabled: PropTypes.func,
};

// form used when the terminology's metadata contain a `void:uriSpace` that
// matches the entity type's uri pattern in config
const UriPatternForm = ({
  uriPattern,
  contextName,
  handleNameChange,
  name,
  isNameFree,
  setActionIsDisabled,
}) => {
  const translate = useTranslation(NLS_BUNDLES);
  const [pristine, setPristine] = useState(true);

  useEffect(() => {
    setActionIsDisabled(!name || !isNameFree || pristine);
  }, [isNameFree, name, pristine, setActionIsDisabled]);

  return (
    <>
      <TextField
        disabled
        label={translate('replaceEntityURLCurrentLinkLabel')}
        id="terminology-edit-name-current-link"
        InputProps={{
          startAdornment: (
            <InputAdornment position="start">
              {constructURIFromPattern(uriPattern, {
                contextName,
              })}
            </InputAdornment>
          ),
        }}
      />
      <TextField
        label={translate('replaceEntityURLNewLinkLabel')}
        id="terminology-edit-name-new-link"
        value={name}
        onChange={(evt) => {
          setPristine(false);
          handleNameChange(evt);
        }}
        error={!pristine && (!name || !isNameFree)}
        helperText={
          !pristine &&
          (!name
            ? translate('URLRequired')
            : !isNameFree && translate('unavailableURLFeedback'))
        }
        InputProps={{
          startAdornment: (
            <InputAdornment position="start">
              {constructURIFromPattern(uriPattern)}
            </InputAdornment>
          ),
        }}
      />
    </>
  );
};

UriPatternForm.propTypes = {
  uriPattern: PropTypes.string,
  contextName: PropTypes.string,
  handleNameChange: PropTypes.func,
  name: PropTypes.string,
  isNameFree: PropTypes.bool,
  setActionIsDisabled: PropTypes.func,
};

const UpdateTerminologyURIDialog = ({
  entry: conceptSchemeEntry,
  closeDialog,
}) => {
  const translate = useTranslation(NLS_BUNDLES);
  const { openMainDialog, setDialogContent, closeMainDialog } = useMainDialog();
  const { tasks, updateTask, allTasksDone } = useTasksList([
    ...TASKS_UPDATE_TERMINOLOGY_URI,
  ]);

  const projectType = Lookup.getProjectTypeInUse(
    conceptSchemeEntry.getContext(),
    true
  );
  const primaryEntityType = getConceptSchemeEntityType(projectType);
  const secondaryEntityType = getConceptEntityType(projectType);
  const { uriPattern: entityTypeUriPattern } = primaryEntityType;
  const uriSpace = conceptSchemeEntry
    .getMetadata()
    .findFirstValue(null, 'void:uriSpace');
  const uriSpacePattern = uriSpace ? `${uriSpace}/\${entryName}` : '';
  const uriPattern = uriSpacePattern || entityTypeUriPattern;
  const { name, setName, isNameFree, handleNameChange } = useEntityURI({
    uriPattern,
  });
  const [contextName, setContextName] = useState('');
  const [isActionDisabled, setActionIsDisabled] = useState(true);
  const [uriSpaceInput, setUriSpaceInput] = useState(uriSpace);
  const [schemeResourceUriInput, setSchemeResourceUriInput] = useState(
    conceptSchemeEntry.getResourceURI()
  );
  const patternMatch = checkUriAndPatternMatch(uriSpace, entityTypeUriPattern); // used to determine which form to show

  // load the context entry to get the context name (stored in the entry info graph)
  useEffect(() => {
    const conceptSchemeContext = conceptSchemeEntry.getContext();
    conceptSchemeContext.getEntry().then((contextEntry) => {
      const nameFromEntryInfo = contextEntry.getEntryInfo().getName();
      setContextName(nameFromEntryInfo);
      setName(nameFromEntryInfo);
    });
  }, [conceptSchemeEntry, setName]);

  const closeDialogs = useCallback(() => {
    closeMainDialog();
    closeDialog();
  }, [closeDialog, closeMainDialog]);

  // TODO move into a hook
  const updateMainDialogTasks = useCallback(
    (updatedTasks) => {
      setDialogContent({
        title: translate('editTerminologyURL'),
        content: (
          <ProgressList
            tasks={updatedTasks}
            nlsBundles={[esteTerminologyNLS]}
          />
        ),
        actions: (
          <AcknowledgeAction
            onDone={closeDialogs}
            isActionDisabled={!allTasksDone}
          />
        ),
      });
    },
    [allTasksDone, closeDialogs, setDialogContent, translate]
  );

  useEffect(() => {
    updateMainDialogTasks(tasks);
  }, [tasks, updateMainDialogTasks]);

  const updateConceptLinksAndProgress = async (updatesConceptPromises) => {
    let fulfilledCount = 0;
    const totalPromises = updatesConceptPromises.length;

    // eslint-disable-next-line no-unused-vars
    for await (const _conceptMdCommitted of updatesConceptPromises) {
      fulfilledCount += 1;
      updateTask('conceptScheme', {
        message: translate('namespaceUpdateConceptSchemeLinks', {
          updatedConceptCount: fulfilledCount,
          totalConcepts: totalPromises,
        }),
      });
    }

    updateTask('conceptScheme', { status: STATUS_DONE });
    updateTask('concepts', { status: STATUS_PROGRESS });
  };

  const updateConceptUrisAndProgress = async (uriChanges, totalPromises) => {
    let fulfilledCount = 0;

    for (const { conceptEntry, newResourceURI } of uriChanges) {
      // UPDATE CONCEPTS RURI based on new scheme ruri

      await skosUtil.setResourceURI(conceptEntry, newResourceURI);
      fulfilledCount += 1;
      updateTask('concepts', {
        message: translate('namespaceUpdateConceptRURI', {
          updatedConceptCount: fulfilledCount,
          totalConcepts: totalPromises,
        }),
      });
    }

    const semanticRelationsURIChanges =
      await skosUtil.getURIChangesForSemanticRelations(uriChanges);
    await skosUtil.replaceURIs(semanticRelationsURIChanges);

    updateTask('concepts', {
      status: STATUS_DONE,
    });
  };

  const updateProgressNoConcepts = () => {
    updateTask('conceptScheme', {
      status: STATUS_DONE,
    });
    updateTask('concepts', {
      status: STATUS_DONE,
      message: translate('namespaceUpdateNoConcepts'),
    });
  };

  const openProgressDialog = () =>
    openMainDialog({
      title: translate('editTerminologyURL'),
      content: <ProgressList tasks={tasks} nlsBundles={[esteTerminologyNLS]} />,
      actions: null,
    });

  // using the entity type's pattern
  const onChangePattern = async () => {
    const { ruri, context } = spreadEntry(conceptSchemeEntry);
    // update context alias
    await context.setName(name);

    // update concept scheme's void:uriSpace
    const constructedUriSpace = constructURIFromPattern(entityTypeUriPattern, {
      contextName: name,
    });
    await skosUtil.updateConceptSchemeUriSpace(
      conceptSchemeEntry,
      constructedUriSpace
    );

    // Update the concept scheme resource URI and get promises to update
    // references to the scheme resource URI inside the concept's metadata
    const conceptEntriesMap = await skosUtil.getSemanticRelations(ruri, [
      'skos:inScheme',
    ]);
    const conceptEntries = conceptEntriesMap.get('skos:inScheme');
    const newConceptSchemeRURI = constructURIFromPattern(entityTypeUriPattern, {
      contextName: name,
    });

    const updatesConceptPromises = await skosUtil.updateConceptSchemeRURI(
      conceptSchemeEntry,
      newConceptSchemeRURI,
      conceptEntries
    );
    const totalPromises = updatesConceptPromises.length;

    openProgressDialog();

    if (totalPromises) {
      // update concepts' metadata to point to the new scheme resource URI
      await updateConceptLinksAndProgress(updatesConceptPromises);

      // Track uri changes in order to delay metadata commits. Changes are made in two steps.
      // First the uri of each concept is updated, then the semantic relations for each concept.
      const uriChanges = skosUtil.getURIChangesForConcepts(
        conceptEntries,
        secondaryEntityType,
        name
      );

      // update concepts' resource URI based on new scheme resource URI
      await updateConceptUrisAndProgress(uriChanges, totalPromises);
    } else {
      updateProgressNoConcepts();
    }
  };

  const onChangeSpace = async () => {
    const { ruri } = spreadEntry(conceptSchemeEntry);

    // update concept scheme's void:uriSpace
    await skosUtil.updateConceptSchemeUriSpace(
      conceptSchemeEntry,
      uriSpaceInput
    );

    // Update the concept scheme resource URI and get promises to update
    // references to the scheme RURI inside the concept's metadata
    const conceptEntriesMap = await skosUtil.getSemanticRelations(ruri, [
      'skos:inScheme',
    ]);
    const conceptEntries = conceptEntriesMap.get('skos:inScheme');
    const updatesConceptPromises = await skosUtil.updateConceptSchemeRURI(
      conceptSchemeEntry,
      schemeResourceUriInput,
      conceptEntries
    );
    const totalPromises = updatesConceptPromises.length;

    openProgressDialog();

    if (totalPromises) {
      await updateConceptLinksAndProgress(updatesConceptPromises);

      // Track uri changes in order to delay metadata commits. Changes are made in two steps.
      // First the uri of each concept is updated, then the semantic relations for each concept.
      const originalUriSpacePattern = `${uriSpace}/\${entryName}`;
      const newUriSpacePattern = `${uriSpaceInput}/\${entryName}`;
      const uriChanges = skosUtil.getURIChangesForConcepts(
        conceptEntries,
        { uriPattern: originalUriSpacePattern },
        undefined,
        newUriSpacePattern
      );

      // update concepts' resource URI based on new scheme resource URI
      await updateConceptUrisAndProgress(uriChanges, totalPromises);
    } else {
      updateProgressNoConcepts();
    }
  };

  const handleChange = patternMatch ? onChangePattern : onChangeSpace;
  const actions = (
    <Button autoFocus disabled={isActionDisabled} onClick={handleChange}>
      {translate('editTerminologyURLButton')}
    </Button>
  );

  return (
    <ListActionDialog
      id="edit-terminology-url"
      closeDialog={closeDialog}
      actions={actions}
      title={translate('editTerminologyURL')}
    >
      <ContentWrapper>
        {patternMatch ? (
          <UriPatternForm
            uriPattern={entityTypeUriPattern}
            contextName={contextName}
            handleNameChange={handleNameChange}
            name={name}
            isNameFree={isNameFree}
            setActionIsDisabled={setActionIsDisabled}
          />
        ) : (
          <UriSpaceForm
            uriSpace={uriSpaceInput}
            setUriSpaceInput={setUriSpaceInput}
            schemeResourceUriInput={schemeResourceUriInput}
            setSchemeResourceUriInput={setSchemeResourceUriInput}
            setActionIsDisabled={setActionIsDisabled}
          />
        )}
      </ContentWrapper>
    </ListActionDialog>
  );
};

UpdateTerminologyURIDialog.propTypes = {
  entry: PropTypes.instanceOf(Entry),
  closeDialog: PropTypes.func,
};

export default UpdateTerminologyURIDialog;
