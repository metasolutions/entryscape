import { useState } from 'react';
import { useTranslation } from 'commons/hooks/useTranslation';
import { Button, TextField, InputAdornment } from '@mui/material';
import ContentWrapper from 'commons/components/common/ContentWrapper';
import PropTypes from 'prop-types';
import { useUserState } from 'commons/hooks/useUser';
import { createContext } from 'commons/util/context';
import { useEntityTitleAndURI } from 'commons/hooks/useEntityTitleAndURI';
import esmoModelsNLS from 'models/nls/esmoModels.nls';
import escoDialogsNLS from 'commons/nls/escoDialogs.nls';
import ListActionDialog from 'commons/components/common/dialogs/ListActionDialog';
import { entrystore } from 'commons/store';
import { useListModel, CREATE } from 'commons/components/ListView';
import ProjectTypeChooser from 'commons/types/ProjectTypeChooser';
import { ErrorCatcher } from 'commons/errors/ErrorCatcher';
import { ErrorWithMessage } from 'commons/errors/utils/errors';
import { useConfirmCloseAction } from 'commons/components/EntryListView/dialogs/create/handlers';

const NLS_BUNDLES = [esmoModelsNLS, escoDialogsNLS];

const CreateModelDialog = ({ closeDialog }) => {
  const translate = useTranslation(NLS_BUNDLES);
  const [, dispatch] = useListModel();
  const { userEntry, userInfo } = useUserState();
  const uriPattern = entrystore.getBaseURI();
  const { title, name, isNameFree, handleTitleChange, handleNameChange } =
    useEntityTitleAndURI({ uriPattern });
  const [description, setDescription] = useState(''); // UI: description
  const [createError, setCreateError] = useState();
  const [pristine, setPristine] = useState(true);
  const [selectedProjectType, setSelectedProjectType] = useState('');
  const formIsValid = isNameFree && name !== '' && title !== '';
  const confirmClose = useConfirmCloseAction(closeDialog);

  /**
   *
   * @param evt
   */
  const handleDescriptionChange = (evt) => {
    setDescription(evt.target.value);
  };

  const handleCreate = () => {
    createContext({
      title,
      description,
      alias: name,
      contextType: 'esterms:FormsContext',
      hasAdminRights: userInfo.hasAdminRights,
      projectType: selectedProjectType,
    })
      .then(() => {
        // todo really needed?
        userEntry.setRefreshNeeded();
        return userEntry.refresh();
      })
      .then(closeDialog)
      .then(() => dispatch({ type: CREATE }))
      .catch((error) => {
        setCreateError(
          new ErrorWithMessage(translate('createModelFail'), error)
        );
      });
  };

  const actions = (
    <Button onClick={handleCreate} autoFocus disabled={!formIsValid}>
      {translate('createButtonLabel')}
    </Button>
  );

  return (
    <>
      <ListActionDialog
        id="create-models-project"
        closeDialog={() => confirmClose(!pristine)}
        actions={actions}
        title={translate('createModelTitle')}
        maxWidth="md"
      >
        <ContentWrapper>
          <form autoComplete="off">
            <ProjectTypeChooser
              onChangeProjectType={setSelectedProjectType}
              selectedId={selectedProjectType}
              userInfo={userInfo}
            />
            <TextField
              id="create-models-project-title"
              label={translate('createProjectTitle')}
              value={title}
              onChange={(evt) => {
                setPristine(false);
                handleTitleChange(evt);
              }}
              placeholder={translate('createProjectTitlePlaceholder')}
              error={!pristine && !title}
              helperText={!pristine && !title && translate('nameRequired')}
              required
            />
            <TextField
              label={translate('createProjectName')}
              id="create-models-project-name"
              value={name}
              onChange={(evt) => {
                setPristine(false);
                handleNameChange(evt);
              }}
              error={!pristine && (!name || !isNameFree)}
              helperText={
                !pristine &&
                (!name
                  ? translate('URLRequired')
                  : !isNameFree && translate('unavailableURLFeedback'))
              }
              required
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">{uriPattern}</InputAdornment>
                ),
              }}
            />
            <TextField
              multiline
              rows={4}
              id="create-models-project-description"
              label={translate('createProjectDesc')}
              value={description}
              onChange={handleDescriptionChange}
              placeholder={translate('descriptionPlaceholder')}
            />
          </form>
        </ContentWrapper>
      </ListActionDialog>
      <ErrorCatcher error={createError} />
    </>
  );
};

CreateModelDialog.propTypes = {
  closeDialog: PropTypes.func,
};

export default CreateModelDialog;
