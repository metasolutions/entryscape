import { Tag, tagsPropType } from 'commons/components/tags';
import './OverviewTags.scss';
import { entryPropType } from 'commons/util/entry';

const OverviewTags = ({ entry, tags }) => {
  return (
    <div className="escoOverview__tags">
      {tags.map(({ id, nlsBundles, labelNlsKey, isVisible }) =>
        isVisible(entry) ? (
          <Tag
            id={id}
            key={id}
            nlsBundles={nlsBundles}
            labelNlsKey={labelNlsKey}
          />
        ) : null
      )}
    </div>
  );
};

OverviewTags.propTypes = {
  entry: entryPropType,
  tags: tagsPropType,
};

export default OverviewTags;
