import {
  DATATYPE_LITERAL,
  LANGUAGE_LITERAL,
  LITERAL,
  URI,
  ONLY_LITERAL,
} from 'commons/components/forms/utils/nodetypes';
import { Literal } from '../Literal';
import { LanguageLiteral } from '../LanguageLiteral';
import { Presenter as DefaultPresenter } from '../Presenter';

/**
 * Find a matching presenter component based on field definition. Match is found
 * in following order:
 * 1) If Presenter is provied, that customized presenter should be used.
 * 2) If getValue is provided, the default presenter will be used and getValue
 *    used to get values.
 * 3) Finally nodetype will be used to find matching presenter.
 *
 * @param {{nodetype: string, getValue: Function, hidden: boolean, Presenter:
 * Function}} fieldDefinition
 * @returns {string|undefined}
 */
export const getPresenter = ({ nodetype, getValue, hidden, Presenter }) => {
  if (hidden) return;
  if (Presenter) return Presenter;
  if (getValue) return DefaultPresenter;
  if (!nodetype) {
    throw new Error(
      'One of nodetype, Presenter or getValue is required to get a presenter'
    );
  }

  switch (nodetype) {
    case URI:
    case DATATYPE_LITERAL:
    case LITERAL:
    case ONLY_LITERAL:
      return Literal;
    case LANGUAGE_LITERAL:
      return LanguageLiteral;
    default:
      console.error('No presenter found for nodetype');
      break;
  }
};
