import { createContext, useContext, useReducer } from 'react';
import PropTypes from 'prop-types';
import { entryPropType } from 'commons/util/entry';

export const BACK = 'BACK';
export const FORWARD = 'FORWARD';
export const MOVE = 'MOVE';
export const LINK = 'LINK';
export const NAME = 'NAME';

const linkedBrowserReducer = (state, action) => {
  switch (action.type) {
    case BACK: {
      const newIndex = state.index - 1 < 0 ? state.index : state.index - 1;
      return {
        ...state,
        index: newIndex,
        entry: state.stack[newIndex].entry,
      };
    }
    case FORWARD: {
      const newIndex =
        state.index + 1 >= state.stack.length ? state.index : state.index + 1;
      return {
        ...state,
        index: newIndex,
        entry: state.stack[newIndex].entry,
      };
    }
    case MOVE:
      return {
        ...state,
        index: action.value,
        entry: state.stack[action.value].entry,
      };
    case NAME: {
      const { index, entityTypeName } = action.value;
      state.stack[index] = { ...state.stack[index], entityTypeName };
      return {
        ...state,
      };
    }
    case LINK: {
      const foundIndex = state.stack.findIndex(
        ({ entry }) => entry.getId() === action.value.getId()
      );

      if (foundIndex !== -1) {
        const newStack = state.stack.slice(0, foundIndex + 1);
        return {
          stack: newStack,
          index: newStack.length - 1,
          entry: action.value,
        };
      }

      const newStack = [
        ...state.stack.slice(0, state.index + 1),
        { entry: action.value },
      ];
      return {
        stack: newStack,
        index: newStack.length - 1,
        entry: action.value,
      };
    }
    default:
      throw new Error(
        `linkedBrowserReducer: ${action.type} is not a valid list action type`
      );
  }
};

const LinkedBrowserModelContext = createContext();

export const LinkedBrowserModelProvider = ({ children, initialEntry }) => {
  const [browserModel, dispatch] = useReducer(linkedBrowserReducer, {
    entry: initialEntry,
    stack: [{ entry: initialEntry }],
    index: 0,
  });

  return (
    <LinkedBrowserModelContext.Provider value={[browserModel, dispatch]}>
      {children}
    </LinkedBrowserModelContext.Provider>
  );
};

LinkedBrowserModelProvider.propTypes = {
  children: PropTypes.node,
  initialEntry: entryPropType,
};

export const useLinkedBrowserModel = () => {
  const context = useContext(LinkedBrowserModelContext);
  if (context === undefined)
    throw new Error(
      'LinkedBrowserModel context must be used within context provider'
    );
  return context;
};

export const withLinkedBrowserModelProvider = (Component) => (props) =>
  (
    // eslint-disable-next-line react/destructuring-assignment, react/prop-types
    <LinkedBrowserModelProvider initialEntry={props.entry}>
      <Component {...props} />
    </LinkedBrowserModelProvider>
  );
