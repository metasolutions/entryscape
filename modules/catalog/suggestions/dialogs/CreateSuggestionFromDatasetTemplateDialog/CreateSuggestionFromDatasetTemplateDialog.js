import PropTypes from 'prop-types';
import { useState, useCallback, useReducer, useMemo } from 'react';
import { OpenInNew as OpenInNewIcon } from '@mui/icons-material';
import { promiseUtil } from '@entryscape/entrystore-js';
import ListActionDialog from 'commons/components/common/dialogs/ListActionDialog';
import ContentWrapper from 'commons/components/common/ContentWrapper';
import escaDatasetTemplateNLS from 'catalog/nls/escaDatasetTemplate.nls';
import escaPreparationsNLS from 'catalog/nls/escaPreparations.nls';
import { useTranslation } from 'commons/hooks/useTranslation';
import {
  createSuggestionFromDatasetTemplate,
  getSuggestionsBasedOnTemplates,
  getDatasetTemplateStore,
  createDatasetTemplateQuery,
} from 'catalog/datasetTemplates/util';
import config from 'config';
import { useESContext } from 'commons/hooks/useESContext';
import useAsyncCallback from 'commons/hooks/useAsyncCallback';
import DatasetTemplateFilters from 'catalog/datasetTemplates/DatasetTemplateListFilters';
import { reducer, getInitialState } from 'catalog/datasetTemplates/reducer';
import useCategories from 'catalog/datasetTemplates/useCategories';
import useCatalogs from 'catalog/datasetTemplates/useCatalogs';
import {
  EntryListView,
  useSolrQuery,
  TITLE_COLUMN,
  MODIFIED_COLUMN,
  INFO_COLUMN,
} from 'commons/components/EntryListView';
import { LIST_ACTION_INFO } from 'commons/components/EntryListView/actions';
import {
  withListModelProvider,
  ListItemButton,
  ListItemIconButton,
  ListItemActionIconButton,
} from 'commons/components/ListView';
import {
  languageUriToIsoCode,
  getBasedOnProperty,
} from 'commons/util/metadata';
import { applyQueryParams } from 'commons/util/solr';
import { useSnackbar, ERROR } from 'commons/hooks/useSnackbar';
import LinkedDataBrowserDialog from 'commons/components/LinkedDataBrowserDialog';

const CreateSuggestionFromTemplateDialog = ({ closeDialog, onCreate }) => {
  const catalogConfiguration = config.get(
    'catalog.datasetTemplateDefaultCatalog'
  );
  const translate = useTranslation([
    escaDatasetTemplateNLS,
    escaPreparationsNLS,
  ]);
  const { context } = useESContext();
  const [pendingTemplateSuggestions, setPendingTemplateSuggestions] = useState(
    []
  );
  const [addSnackbar] = useSnackbar();

  const [
    { category: selectedCategory, context: selectedCatalogId },
    filtersDispatch,
  ] = useReducer(reducer, getInitialState());

  const datasetTemplateStore = useMemo(() => getDatasetTemplateStore(), []);
  const catalogEntries = useCatalogs(datasetTemplateStore);

  const selectedCatalogEntry =
    catalogEntries.find(
      (catalogEntry) => catalogEntry.getContext().getId() === selectedCatalogId
    ) || '';

  const [categories, setCategories] = useCategories(
    datasetTemplateStore,
    selectedCatalogEntry,
    filtersDispatch
  );

  const [existingTemplateSuggestions] = useAsyncCallback(
    getSuggestionsBasedOnTemplates,
    context,
    []
  );

  const createAndClose = async () => {
    closeDialog();

    if (!pendingTemplateSuggestions.length) return;

    promiseUtil
      .forEach(pendingTemplateSuggestions, (templateEntry) =>
        createSuggestionFromDatasetTemplate(templateEntry, context)
      )
      .then(onCreate)
      .then(() =>
        addSnackbar({
          message: translate(
            'createSuccessMessage',
            pendingTemplateSuggestions.length
          ),
        })
      )
      .catch((err) => {
        console.error(err);
        addSnackbar({
          message: translate(
            'createFailMessage',
            pendingTemplateSuggestions.length
          ),
          type: ERROR,
        });
      });
  };

  const selectedCatalogLanguage = selectedCatalogEntry
    ? selectedCatalogEntry
        .getMetadata()
        .findFirstValue(null, 'dcterms:language')
    : '';

  const createQuery = useCallback(
    () =>
      createDatasetTemplateQuery(
        datasetTemplateStore,
        selectedCatalogEntry,
        selectedCategory
      ),
    [datasetTemplateStore, selectedCatalogEntry, selectedCategory]
  );

  const applyParamsCustomSort = useCallback(
    (query, { sort, ...queryParams }) =>
      applyQueryParams(query, {
        ...queryParams,
        sort: {
          ...sort,
          locale: languageUriToIsoCode(selectedCatalogLanguage),
        },
      }),
    [selectedCatalogLanguage]
  );
  const { size, status, ...queryResults } = useSolrQuery({
    createQuery,
    applyQueryParams: applyParamsCustomSort,
  });

  const getCreateButtonProps = ({ entry: templateEntry, translate: t }) => {
    const basedOnTemplate = [
      ...existingTemplateSuggestions,
      ...pendingTemplateSuggestions,
    ].some(
      (entry) =>
        getBasedOnProperty(entry) === templateEntry.getResourceURI() ||
        entry.getResourceURI() === templateEntry.getResourceURI()
    );

    return {
      // eslint-disable-next-line no-unused-vars
      onClick: (_event) =>
        setPendingTemplateSuggestions([
          ...pendingTemplateSuggestions,
          templateEntry,
        ]),
      disabled: basedOnTemplate,
      label: basedOnTemplate
        ? t('createdFromTemplateDialogButton')
        : t('createFromTemplateDialogButton'),
    };
  };

  const getSourceLinkProps = ({ entry, translate: t }) => {
    const landingPageLink = entry
      .getMetadata()
      .findFirstValue(entry.getResourceURI(), 'esterms:templateLandingPage');

    if (!landingPageLink) return null;

    return {
      title: t('landingPageLinkTooltip'),
      'aria-label': t('landingPageLinkTooltip'),
      children: <OpenInNewIcon />,
      href: landingPageLink,
      target: '_blank',
    };
  };

  return (
    <ListActionDialog
      id="create-from-template-dialog"
      title={translate('createFromTemplateDialogHeader')}
      closeDialog={createAndClose}
      closeDialogButtonLabel={translate('createFromTemplateDialogCloseButton')}
      maxWidth="md"
      fixedHeight
    >
      <ContentWrapper md={12}>
        <EntryListView
          // eslint-disable-next-line react/jsx-props-no-spreading
          {...queryResults}
          size={size}
          nlsBundles={[escaDatasetTemplateNLS]}
          listPlaceholderProps={{
            label: translate('emptyListWarning'),
          }}
          columns={[
            {
              ...TITLE_COLUMN,
              headerNlsKey: 'templatesListHeaderLabel',
              xs: 6,
            },
            {
              ...MODIFIED_COLUMN,
              headerNlsKey: 'templatesListHeaderModified',
              sortBy: false,
            },
            {
              ...INFO_COLUMN,
              title: translate('templateMetadataTooltip'),
              Component: ListItemActionIconButton,
              getProps: ({ entry }) => ({
                action: {
                  ...LIST_ACTION_INFO,
                  entry,
                  Dialog: LinkedDataBrowserDialog,
                },
              }),
            },
            {
              id: 'source-link',
              Component: ListItemIconButton,
              xs: 1,
              sx: { padding: 0 },
              getProps: getSourceLinkProps,
            },
            {
              id: 'create-dataset-from-template',
              xs: 2,
              Component: ListItemButton,
              getProps: getCreateButtonProps,
            },
          ]}
        >
          <DatasetTemplateFilters
            filtersDispatch={filtersDispatch}
            includeCatalogFilter={!catalogConfiguration}
            selectedCatalogId={selectedCatalogId}
            catalogEntries={catalogEntries}
            categories={categories}
            setCategories={setCategories}
            selectedCategory={selectedCategory}
          />
        </EntryListView>
      </ContentWrapper>
    </ListActionDialog>
  );
};

CreateSuggestionFromTemplateDialog.propTypes = {
  closeDialog: PropTypes.func.isRequired,
  onCreate: PropTypes.func,
};

export default withListModelProvider(
  CreateSuggestionFromTemplateDialog,
  () => ({
    sort: { field: 'title', order: 'asc' },
  })
);
