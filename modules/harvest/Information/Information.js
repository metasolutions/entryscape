import PropTypes from 'prop-types';
import { useState, useEffect } from 'react';
import { Button } from '@mui/material';
import { useTranslation } from 'commons/hooks/useTranslation';
import esreHarvestNLS from 'harvest/nls/esreHarvest.nls';
import usePageTitle from 'commons/hooks/usePageTitle';
import EditPipelineDialog from 'harvest/dialogs/EditPipelineDialog';
import useAsync from 'commons/hooks/useAsync';
import useErrorHandler from 'commons/errors/hooks/useErrorHandler';
import { fetchOrganizationData } from 'harvest/util';
import Loader from 'commons/components/Loader';
import PipelinePresenter from './PipelinePresenter';
import './Information.scss';

const Information = ({ contextId }) => {
  const translate = useTranslation([esreHarvestNLS]);
  const [open, setOpen] = useState(false);
  const [pageTitle, setPageTitle] = usePageTitle();
  const {
    data: { pipelineEntry, entityType, organizationData },
    runAsync,
    status,
    isLoading,
    error,
  } = useAsync({ data: {} });
  useErrorHandler(error);
  const tags = entityType?.get('tags') || [];
  const isDcat = tags.includes('dcat');
  const isEditButtonVisible = pipelineEntry?.canWriteResource();

  // necessary for non-dialog public view to have a title
  useEffect(() => {
    if (pageTitle || !organizationData) return;

    const { title } = organizationData;
    setPageTitle(title);
  }, [organizationData, pageTitle, setPageTitle]);

  useEffect(() => {
    if (status !== 'idle') return;

    runAsync(fetchOrganizationData(contextId));
  }, [contextId, runAsync, status]);

  if (isLoading) return <Loader />;

  return (
    <>
      <div className="esreInformation">
        {isEditButtonVisible && (
          <div className="esreInformation__actions">
            <Button onClick={() => setOpen(true)}>
              {translate('configureButton')}
            </Button>
          </div>
        )}
        <PipelinePresenter
          organizationData={organizationData}
          isDcat={isDcat}
        />
      </div>
      {open ? (
        <EditPipelineDialog
          open
          onClose={() => setOpen(false)}
          organizationData={organizationData}
          pipelineEntry={pipelineEntry}
          tags={tags}
          onSave={() => runAsync(fetchOrganizationData(contextId))}
        />
      ) : null}
    </>
  );
};

Information.propTypes = {
  contextId: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
};

export default Information;
