import PropTypes from 'prop-types';
import React, { useEffect } from 'react';
import EntryImport from 'commons/components/entry/Import';
import useLinkOrFile from 'commons/components/entry/Import/useLinkOrFile';

const CreateEntryByTemplate = ({
  setCreateEntryParams,
  setExtraFieldIsValid,
  uniqueURI = false,
}) => {
  const {
    link,
    file,
    handleLinkChange,
    handleFileChange,
    handleTabChange,
    linkIsValid,
    buttonEnabled,
    isLink,
  } = useLinkOrFile();

  useEffect(() => {
    if (!link && !file) {
      setCreateEntryParams({ isLink });
    }
    if (link && isLink) {
      setCreateEntryParams({ link, isLink });
    }

    if (file && !isLink) {
      setCreateEntryParams({ file, isLink });
    }

    setExtraFieldIsValid(buttonEnabled);
  }, [
    link,
    file,
    isLink,
    setExtraFieldIsValid,
    buttonEnabled,
    setCreateEntryParams,
  ]);

  return (
    <EntryImport
      link={link}
      file={file}
      handleLinkChange={handleLinkChange}
      handleFileChange={handleFileChange}
      validURI={linkIsValid}
      handleTabChange={handleTabChange}
      uniqueURI={uniqueURI}
    />
  );
};

CreateEntryByTemplate.propTypes = {
  setCreateEntryParams: PropTypes.func,
  setExtraFieldIsValid: PropTypes.func,
  uniqueURI: PropTypes.bool,
};

export default CreateEntryByTemplate;
