import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { TextField } from '@mui/material';
import useLinkOrFile from 'commons/components/entry/Import/useLinkOrFile';
import RDFormsFieldWrapper from 'commons/components/rdforms/FieldWrapper';
import escoEntryTypeNLS from 'commons/nls/escoEntryType.nls';
import { isUri } from 'commons/util/util';
import { useTranslation } from 'commons/hooks/useTranslation';

const Link = ({ setCreateEntryParams, setExtraFieldIsValid }) => {
  const { link, setLink } = useLinkOrFile();
  const [pristine, setPristine] = useState(true);
  const t = useTranslation(escoEntryTypeNLS);
  const linkIsValid = link && isUri(link);

  const handleLinkChange = ({ target }) => {
    setPristine(false);
    const { value: newLink } = target;
    setLink(newLink);
    setCreateEntryParams({ link: newLink });
    setExtraFieldIsValid(newLink && isUri(newLink));
  };

  const getHelperText = () => {
    if (!pristine) {
      if (!link) {
        return t('linkRequired');
      }

      if (!isUri(link)) {
        return t('linkNotValid');
      }
    }

    return '';
  };

  return (
    <RDFormsFieldWrapper label={t('webAddressLabel')} editor>
      <TextField
        value={link}
        onChange={handleLinkChange}
        error={!pristine && !linkIsValid}
        helperText={getHelperText()}
      />
    </RDFormsFieldWrapper>
  );
};

Link.propTypes = {
  setCreateEntryParams: PropTypes.func,
  setExtraFieldIsValid: PropTypes.func,
};

export default Link;
