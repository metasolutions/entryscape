# Changelog

All notable changes to the EntryScape project will be documented in this file. This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [3.15.0](https://bitbucket.org/metasolutions/entryscape/branches/compare/3.15.0%0D3.14.1) - 2025-02-XY

### Catalog

#### Improvement

- Lists that can display entries from all contexts should by default restrict to the current and have pre-expanded filters ([ES-3733](https://metasolutions.atlassian.net/browse/ES-3733))
- Help texts for diagram axes are not used. ([ES-3610](https://metasolutions.atlassian.net/browse/ES-3610))

#### Maintenance

- Remove modules/catalog/datasets/DatasetOverview/inputs ([ES-3717](https://metasolutions.atlassian.net/browse/ES-3717))
- Keep catalog views together ([ES-3641](https://metasolutions.atlassian.net/browse/ES-3641))

#### Bug

- Dataset profile filter doesn't work as expected ([ES-3707](https://metasolutions.atlassian.net/browse/ES-3707))
- Manage files dialog breaks on large file numbers ([ES-3674](https://metasolutions.atlassian.net/browse/ES-3674))
- Improve visualization state handling ([ES-3670](https://metasolutions.atlassian.net/browse/ES-3670))
- Change message when removing a dataset series ([ES-3638](https://metasolutions.atlassian.net/browse/ES-3638))
- Incosistent filters between dataset list and table views ([ES-3630](https://metasolutions.atlassian.net/browse/ES-3630))
- Visualisation title cleared when choosing a file ([ES-3628](https://metasolutions.atlassian.net/browse/ES-3628))

#### New Feature

- Show Suggestions filters expanded by default ([ES-3697](https://metasolutions.atlassian.net/browse/ES-3697))
- Back button in context overviews ([ES-3696](https://metasolutions.atlassian.net/browse/ES-3696))
- Add dataset series to profile dropdown on table view ([ES-3606](https://metasolutions.atlassian.net/browse/ES-3606))
- Map extent is not saved for CSV visualization ([ES-3604](https://metasolutions.atlassian.net/browse/ES-3604))
- Migrate Catalog overview ([ES-3557](https://metasolutions.atlassian.net/browse/ES-3557))
- Link to user documentation from the UI ([ES-3456](https://metasolutions.atlassian.net/browse/ES-3456))
- Allow internal publish to give \_users group read rights in Catalog ([ES-3447](https://metasolutions.atlassian.net/browse/ES-3447))
- Option to hide Link/File tabs if only links should be used ([ES-2859](https://metasolutions.atlassian.net/browse/ES-2859))
- Move functionality from catalog list ellipsis into overview ([ES-419](https://metasolutions.atlassian.net/browse/ES-419))
- Describe workflow in overview ([ES-412](https://metasolutions.atlassian.net/browse/ES-412))

### Commons

#### Improvement

- Adjust registry to take cached external metadata into account ([ES-3725](https://metasolutions.atlassian.net/browse/ES-3725))
- Add a way to provide an initial value to useAsyncCallback ([ES-3716](https://metasolutions.atlassian.net/browse/ES-3716))
- Use commons overview util for accessing overviews in Search, similar to LDB ([ES-3694](https://metasolutions.atlassian.net/browse/ES-3694))
- Implement ListItemActionsGroup in list views ([ES-3685](https://metasolutions.atlassian.net/browse/ES-3685))
- Improve handling of long button labels in context overviews ([ES-3679](https://metasolutions.atlassian.net/browse/ES-3679))
- Use OverviewTileButtonGroup in context overviews ([ES-3637](https://metasolutions.atlassian.net/browse/ES-3637))
- Replace mergeColumns with actions group component ([ES-3612](https://metasolutions.atlassian.net/browse/ES-3612))
- Replace list item non-button icons with tags ([ES-3605](https://metasolutions.atlassian.net/browse/ES-3605))
- Improvements to Linked Data Browser ([ES-3588](https://metasolutions.atlassian.net/browse/ES-3588))
- Require title or corresponding label property for drafts ([ES-3532](https://metasolutions.atlassian.net/browse/ES-3532))
- Avoid scrollbars when using dropdown in table view popup ([ES-3483](https://metasolutions.atlassian.net/browse/ES-3483))
- Handle items in EntryListView ([ES-3469](https://metasolutions.atlassian.net/browse/ES-3469))
- Show list of linked datasets on showcase and idea overview ([ES-3455](https://metasolutions.atlassian.net/browse/ES-3455))
- Links to referenced entries in linked data browser ([ES-3451](https://metasolutions.atlassian.net/browse/ES-3451))
- Links to overviews of referenced entries in remove dialog ([ES-3450](https://metasolutions.atlassian.net/browse/ES-3450))
- Improve error message if user is blocked after too many login attempts ([ES-3406](https://metasolutions.atlassian.net/browse/ES-3406))
- Configuration merging changes the order of items ([ES-3394](https://metasolutions.atlassian.net/browse/ES-3394))
- Upgrade to eslint 8 ([ES-3378](https://metasolutions.atlassian.net/browse/ES-3378))
- Persist filters/search between lists and overviews ([ES-2831](https://metasolutions.atlassian.net/browse/ES-2831))
- Weird placeholder on piece of art "technique"/dcterms:subject picker ([ES-1739](https://metasolutions.atlassian.net/browse/ES-1739))

#### Bug

- Signin form error messages disappear ([ES-3711](https://metasolutions.atlassian.net/browse/ES-3711))
- Linked data browser error in registry search and catalog-search ([ES-3680](https://metasolutions.atlassian.net/browse/ES-3680))
- Top header is not updated on language switch ([ES-3662](https://metasolutions.atlassian.net/browse/ES-3662))
- Consider relations when loading solutions ([ES-3654](https://metasolutions.atlassian.net/browse/ES-3654))
- Rdforms outline doesn't take filtered predicates into consideration ([ES-3642](https://metasolutions.atlassian.net/browse/ES-3642))
- Handle rows per page as state in list model ([ES-3634](https://metasolutions.atlassian.net/browse/ES-3634))
- Remove loading dialog ([ES-3523](https://metasolutions.atlassian.net/browse/ES-3523))
- Disappearing border in edit dialog ([ES-3156](https://metasolutions.atlassian.net/browse/ES-3156))

#### New Feature

- Hide properties in LDB "About" tab when they do not have values and use loading indicator for async values ([ES-3705](https://metasolutions.atlassian.net/browse/ES-3705))
- Move edit action out of row menu in Manage files dialog ([ES-3702](https://metasolutions.atlassian.net/browse/ES-3702))
- Move Revisions from tile button to sidebar actions ([ES-3699](https://metasolutions.atlassian.net/browse/ES-3699))
- Add alternative texts for fields and forms ([ES-3608](https://metasolutions.atlassian.net/browse/ES-3608))
- Link to external preview from entry overview ([ES-3570](https://metasolutions.atlassian.net/browse/ES-3570))
- Migrate Workbench overview ([ES-3559](https://metasolutions.atlassian.net/browse/ES-3559))
- Migrate Terms overview ([ES-3558](https://metasolutions.atlassian.net/browse/ES-3558))
- Migrate context overviews to views similar to other entity overviews ([ES-3368](https://metasolutions.atlassian.net/browse/ES-3368))
- Sharing settings dialog error handling ([ES-1640](https://metasolutions.atlassian.net/browse/ES-1640))

#### Maintenance

- Rename entity type listView to listview ([ES-3704](https://metasolutions.atlassian.net/browse/ES-3704))
- Move icons to actions ([ES-3686](https://metasolutions.atlassian.net/browse/ES-3686))
- Fix inconsistencies in tooltips for creating datasets and multi create button ([ES-3633](https://metasolutions.atlassian.net/browse/ES-3633))
- Improve commit handling on creating datasets in series and setting draft ([ES-3627](https://metasolutions.atlassian.net/browse/ES-3627))

### Models

#### Bug

- Incomplete preview of inline fields ([ES-3687](https://metasolutions.atlassian.net/browse/ES-3687))
- Cardinality include level can't be cleared ([ES-3678](https://metasolutions.atlassian.net/browse/ES-3678))
- Import RDForms bundle issues ([ES-3673](https://metasolutions.atlassian.net/browse/ES-3673))
- Import rdforms templates with references to other templates are broken ([ES-3668](https://metasolutions.atlassian.net/browse/ES-3668))
- Modified header misplaced in classes and properties views ([ES-3664](https://metasolutions.atlassian.net/browse/ES-3664))
- Web address is not shown as mandatory when creating classes or properties ([ES-3663](https://metasolutions.atlassian.net/browse/ES-3663))
- Model title not updated ([ES-3525](https://metasolutions.atlassian.net/browse/ES-3525))
- Preview dialog on properties overview has an input as the rdf/json placeholder ([ES-3520](https://metasolutions.atlassian.net/browse/ES-3520))
- Namespaces overview: big delay before set/unset default becomes enabled ([ES-3517](https://metasolutions.atlassian.net/browse/ES-3517))
- Import rdforms bundle freezes on error ([ES-2003](https://metasolutions.atlassian.net/browse/ES-2003))

#### Improvement

- Rename form items to fields ([ES-3684](https://metasolutions.atlassian.net/browse/ES-3684))
- Use create instead of modified when fetching entries for building rdforms bundles ([ES-3652](https://metasolutions.atlassian.net/browse/ES-3652))
- Improve state handling on namespaces ([ES-3346](https://metasolutions.atlassian.net/browse/ES-3346))

#### New Feature

- Alternative texts presenter ([ES-3677](https://metasolutions.atlassian.net/browse/ES-3677))
- Use terminology for Lookup fields ([ES-3573](https://metasolutions.atlassian.net/browse/ES-3573))
- Externalize form items for a form ([ES-3569](https://metasolutions.atlassian.net/browse/ES-3569))
- Handle required field error on field group instead of field level ([ES-2970](https://metasolutions.atlassian.net/browse/ES-2970))

### Registry

#### Improvement

- Homogenize harvesting reports to always display the table ([ES-3703](https://metasolutions.atlassian.net/browse/ES-3703))
- Toolkit validation report should present validation results higher ([ES-3649](https://metasolutions.atlassian.net/browse/ES-3649))
- Public sector organisation toggle causes info to be lost ([ES-3328](https://metasolutions.atlassian.net/browse/ES-3328))

#### Bug

- Detailed harvesting info doesn't display correctly ([ES-3693](https://metasolutions.atlassian.net/browse/ES-3693))
- Organisations>Notifications>Edit: save button always on, even before changes are made ([ES-3330](https://metasolutions.atlassian.net/browse/ES-3330))

### Search

#### Bug

- Search module doesn't return untitled results ([ES-3723](https://metasolutions.atlassian.net/browse/ES-3723))
- Show builtin entity type label instead of overridden ([ES-3666](https://metasolutions.atlassian.net/browse/ES-3666))
- Search breaks on custom entity types and project types ([ES-3665](https://metasolutions.atlassian.net/browse/ES-3665))

#### New Feature

- Persist filters/search/pagination between Search module and overview ([ES-3572](https://metasolutions.atlassian.net/browse/ES-3572))

### Terms

#### Improvement

- Concepts view not updated after language change ([ES-3709](https://metasolutions.atlassian.net/browse/ES-3709))
- Place info button consistently ([ES-3708](https://metasolutions.atlassian.net/browse/ES-3708))
- Navigate to Terms overview instead of Concepts view ([ES-3706](https://metasolutions.atlassian.net/browse/ES-3706))
- Rename Hierarchy view to Concepts ([ES-3698](https://metasolutions.atlassian.net/browse/ES-3698))
- Use filters in Manage collection concepts ([ES-3376](https://metasolutions.atlassian.net/browse/ES-3376))

#### Bug

- Some terminology actions not visible on initial list load ([ES-3661](https://metasolutions.atlassian.net/browse/ES-3661))
- Use and prioritize URI space over entity type's URI pattern ([ES-3660](https://metasolutions.atlassian.net/browse/ES-3660))
- Empty list on manage collection concepts dialog when switching source ([ES-3508](https://metasolutions.atlassian.net/browse/ES-3508))

#### New Feature

- Move functionality from terminology list ellipsis into overview ([ES-92](https://metasolutions.atlassian.net/browse/ES-92))

### Workbench

#### Bug

- Project overview is broken for non-admins ([ES-3720](https://metasolutions.atlassian.net/browse/ES-3720))
- Change project uri dialog is failing ([ES-3701](https://metasolutions.atlassian.net/browse/ES-3701))
- Draft column broken in Workbench ([ES-3692](https://metasolutions.atlassian.net/browse/ES-3692))

#### New Feature

- Add basic filters to entity views in Workbench ([ES-3689](https://metasolutions.atlassian.net/browse/ES-3689))

## [3.14.1](https://bitbucket.org/metasolutions/entryscape/branches/compare/3.14.1%0D3.14.0) - 2024-12-04

### Common

#### Bug

- Fix outline not taking filtered predicates into consideration ([ES-3642](https://metasolutions.atlassian.net/browse/ES-3642))
- Template issues cause edit dialogs to break ([ES-3657](https://metasolutions.atlassian.net/browse/ES-3657))

### Registry

#### Bug

- Fix entity search error on registry instances ([ES-3655](https://metasolutions.atlassian.net/browse/ES-3655))

## [3.14.0](https://bitbucket.org/metasolutions/entryscape/branches/compare/3.14.0%0D3.13.2) - 2024-11-12

### Catalog

#### New feature

- Add support for dataset series ([ES-3446](https://metasolutions.atlassian.net/browse/ES-3446))
- Add pie chart as visualization chart type ([ES-1314](https://metasolutions.atlassian.net/browse/ES-1314))

#### Improvement

- Have a single list item column for non-button items on datasets view ([ES-3578](https://metasolutions.atlassian.net/browse/ES-3578))
- Use geonames' toponymName by default ([ES-3585](https://metasolutions.atlassian.net/browse/ES-3585))

#### Bug

- Copying a draft doesn't seem to carry over the `draft` status ([ES-3616](https://metasolutions.atlassian.net/browse/ES-3616))
- Creating a dataset from a suggestion makes the suggestion not appear under active ([ES-3620](https://metasolutions.atlassian.net/browse/ES-3620))

### Models

#### Bug

- Undefined values in SHACL export ([ES-3636](https://metasolutions.atlassian.net/browse/ES-3636))

### Registry

#### Improvement

- Limit shown toolkit validate class report instances ([ES-3596](https://metasolutions.atlassian.net/browse/ES-3596))

#### Bug

- Fix toolkit example ([ES-3601](https://metasolutions.atlassian.net/browse/ES-3601))

### Search

#### Bug

- Ending up in wrong module when navigating to overview ([ES-3624](https://metasolutions.atlassian.net/browse/ES-3624))

### Common

#### Maintenance

- Upgrade rdforms dependency ([ES-3577](https://metasolutions.atlassian.net/browse/ES-3577))

## [3.13.2](https://bitbucket.org/metasolutions/entryscape/branches/compare/3.13.2%0D3.13.1) - 2024-10-31

### Registry

#### Bug

- Custom toolkit example is not working ([ES-3599](https://metasolutions.atlassian.net/browse/ES-3599))
- Toolkit validation class report instances all use the same label ([ES-3600](https://metasolutions.atlassian.net/browse/ES-3600))
- Large validation report loading is slow and lacks an indicator ([ES-3611](https://metasolutions.atlassian.net/browse/ES-3611))

### Search

#### Bug

- Add missing translations ([ES-3597](https://metasolutions.atlassian.net/browse/ES-3597))

## [3.13.1](https://bitbucket.org/metasolutions/entryscape/branches/compare/3.13.1%0D3.13.0) - 2024-10-23

### Catalog

#### Bug

- Fix `high-value datasets only` filter ([ES-3568](https://metasolutions.atlassian.net/browse/ES-3568))

## [3.13.0](https://bitbucket.org/metasolutions/entryscape/branches/compare/3.13.0%0D3.12.4) - 2024-10-11

### Catalog

#### New feature

- Hide catalog types for specific contexts ([ES-2866](https://metasolutions.atlassian.net/browse/ES-2866)) ([ES-3453](https://metasolutions.atlassian.net/browse/ES-3453))
- Add filter to show publishers and contacts from all contexts on their list views ([ES-2861](https://metasolutions.atlassian.net/browse/ES-2861))
- List linked datasets on data service overview ([ES-3403](https://metasolutions.atlassian.net/browse/ES-3403)) ([ES-3481](https://metasolutions.atlassian.net/browse/ES-3481))

#### Improvement

- Make tooltips in dataset overview consistent ([ES-3253](https://metasolutions.atlassian.net/browse/ES-3253))
- Switch order of Longitude and Latitude in edit visualisation dialog ([ES-3375](https://metasolutions.atlassian.net/browse/ES-3375))
- Support language for visualisation titles ([ES-3379](https://metasolutions.atlassian.net/browse/ES-3379))
- Move template icon on datasets view to its own column ([ES-3401](https://metasolutions.atlassian.net/browse/ES-3401))
- Add oa namespace globally ([ES-3461](https://metasolutions.atlassian.net/browse/ES-3461))
- Add references to data services for catalog entry ([ES-3402](https://metasolutions.atlassian.net/browse/ES-3402))

#### Bug

- Fix replace file dialog on documents overview ([ES-3381](https://metasolutions.atlassian.net/browse/ES-3381))
- Fix warnings on visualization form about items using the same key ([ES-3431](https://metasolutions.atlassian.net/browse/ES-3431))

### Common

#### New feature

- Show new release notifications ([ES-3260](https://metasolutions.atlassian.net/browse/ES-3260))
- Allow configurable hiding of actions from list views ([ES-3433](https://metasolutions.atlassian.net/browse/ES-3433)) ([ES-3440](https://metasolutions.atlassian.net/browse/ES-3440))
- Allow configurable hiding of actions from details views ([ES-3444](https://metasolutions.atlassian.net/browse/ES-3444))
- Add language switcher to Linked Data Browser dialog ([ES-2874](https://metasolutions.atlassian.net/browse/ES-2874))
- Save metadata as drafts ([ES-2979](https://metasolutions.atlassian.net/browse/ES-2979))
- Add support for multiple SSO endpoints ([ES-3362](https://metasolutions.atlassian.net/browse/ES-3362))

#### Improvement

- Add user forum link on user menu ([ES-3387](https://metasolutions.atlassian.net/browse/ES-3387))
- Add tooltip with explanation on non-clickable list items ([ES-3423](https://metasolutions.atlassian.net/browse/ES-3423))
- Improve placeholder component with children wrapper ([ES-3457](https://metasolutions.atlassian.net/browse/ES-3457))
- Improve placeholder margins ([ES-3159](https://metasolutions.atlassian.net/browse/ES-3159)) ([ES-3148](https://metasolutions.atlassian.net/browse/ES-3148))
- Improve download file names ([ES-3072](https://metasolutions.atlassian.net/browse/ES-3072))
- Change date format to ensure uniform list item sizes ([ES-3206](https://metasolutions.atlassian.net/browse/ES-3206))
- Create new group for map controls and a generic component for OpenLayers controls ([ES-3270](https://metasolutions.atlassian.net/browse/ES-3270))
- Move LDB entity type and template handling into a hook ([ES-3293](https://metasolutions.atlassian.net/browse/ES-3293))
- Present password change form feedback using a snackbar ([ES-3313](https://metasolutions.atlassian.net/browse/ES-3313))
- Disable clear filters button when there is no active filter ([ES-3320](https://metasolutions.atlassian.net/browse/ES-3320))
- Prevent dialogs with unsaved changes being closed without warning ([ES-3347](https://metasolutions.atlassian.net/browse/ES-3347)) ([ES-3355](https://metasolutions.atlassian.net/browse/ES-3355))
- Finish info dialog migration to LDB and remove old info dialog ([ES-3361](https://metasolutions.atlassian.net/browse/ES-3361)) ([ES-3297](https://metasolutions.atlassian.net/browse/ES-3297))
- Make the publish toggle generic ([ES-3364](https://metasolutions.atlassian.net/browse/ES-3364)) ([ES-3366](https://metasolutions.atlassian.net/browse/ES-3366))
- Replace detailed info action with info icon, remove menu action ([ES-3367](https://metasolutions.atlassian.net/browse/ES-3367))
- Remove redundant overview sidebar action tooltips ([ES-3369](https://metasolutions.atlassian.net/browse/ES-3369))
- Add schema:name as valid label property ([ES-3472](https://metasolutions.atlassian.net/browse/ES-3472))
- Handle missing project type in edit entry dialog ([3482](https://metasolutions.atlassian.net/browse/ES-3482))
- Prevent inline create in SKOS chooser ([ES-3484](https://metasolutions.atlassian.net/browse/ES-3484))

#### Bug

- Fix edge case where admin configuration disrespects view permissions ([ES-3393](https://metasolutions.atlassian.net/browse/ES-3393))
- Fix table view error on language change ([ES-3411](https://metasolutions.atlassian.net/browse/ES-3411))
- Restrict fetched comments to the current context ([ES-3459](https://metasolutions.atlassian.net/browse/ES-3459))
- Keep table view changes if switching language ([ES-3425](https://metasolutions.atlassian.net/browse/ES-3425))
- Fix console error appearing in LDB when only renderPresenter is used ([ES-3323](https://metasolutions.atlassian.net/browse/ES-3323))
- Show image preview in LDB references tab ([ES-3325](https://metasolutions.atlassian.net/browse/ES-3325))
- Fix visual glitch on opening LDB dialog ([ES-3462](https://metasolutions.atlassian.net/browse/ES-3462))
- Fix rdforms dropdown options being hidden after selection ([ES-2932](https://metasolutions.atlassian.net/browse/ES-2932))

#### Maintenance

- Replace `request` dependency ([ES-2621](https://metasolutions.atlassian.net/browse/ES-2621))
- Audit and upgrade dependencies ([ES-3474](https://metasolutions.atlassian.net/browse/ES-3474))

### Models

#### New feature

- Use terminologies for select field options ([ES-3231](https://metasolutions.atlassian.net/browse/ES-3231))
- Change field and form type ([ES-3354](https://metasolutions.atlassian.net/browse/ES-3354))
- Add non-rdforms outline to be used in field and form edit dialogs ([ES-3405](https://metasolutions.atlassian.net/browse/ES-3405))
- Support SHACL as a resource ([ES-3357](https://metasolutions.atlassian.net/browse/ES-3357))

#### Improvement

- Use namespace to create Class Web address ([ES-2939](https://metasolutions.atlassian.net/browse/ES-2939))
- Use namespace to create Property Web address ([ES-2940](https://metasolutions.atlassian.net/browse/ES-2940))
- Use nodetype resource as default for object forms ([ES-3290](https://metasolutions.atlassian.net/browse/ES-3290))
- Add sections to field and form edit dialogs ([ES-3404](https://metasolutions.atlassian.net/browse/ES-3404))
- Navigate to overview when clicking on inline lists in fields and forms overviews ([ES-3356](https://metasolutions.atlassian.net/browse/ES-3356))
- Make checkbox editor label clickable ([ES-3358](https://metasolutions.atlassian.net/browse/ES-3358))
- Filter domain to only include classes from current model ([ES-3363](https://metasolutions.atlassian.net/browse/ES-3363))
- Use autocomplete for project filter ([ES-3417](https://metasolutions.atlassian.net/browse/ES-3417))
- Rename import to reuse in namespaces view ([ES-3476](https://metasolutions.atlassian.net/browse/ES-3476))
- Warn and prevent removal of used namespaces ([ES-3477](https://metasolutions.atlassian.net/browse/ES-3477))
- Use getAllMetadata for link references in Models ([ES-3489](https://metasolutions.atlassian.net/browse/ES-3489))
- Change import label to reuse for classes and properties ([ES-3438](https://metasolutions.atlassian.net/browse/ES-3438))

#### Bug

- Use proper info dialog in references for Classes and Properties ([ES-2975](https://metasolutions.atlassian.net/browse/ES-2975))
- Fix cardinality not being set after rfs import ([ES-3412](https://metasolutions.atlassian.net/browse/ES-3412))
- Fix overridden fields missing in specification ([ES-3415](https://metasolutions.atlassian.net/browse/ES-3415))
- Fix lookup fields without matching forms not shown in Diagram ([ES-3416](https://metasolutions.atlassian.net/browse/ES-3416))
- Fix adding unnecessary space in Diagram boxes ([ES-3418](https://metasolutions.atlassian.net/browse/ES-3418))
- Add placeholder for unauthorised state on extended form preview ([ES-3339](https://metasolutions.atlassian.net/browse/ES-3339))
- Form titles are shown instead of labels in diagram ([ES-3502](https://metasolutions.atlassian.net/browse/ES-3502))

#### Maintenance

- Make import namespace dialog consistent with other import dialogs ([ES-3351](https://metasolutions.atlassian.net/browse/ES-3351))

### Registry

#### New feature

- Support new search view in registry ([ES-3399](https://metasolutions.atlassian.net/browse/ES-3399))

#### Improvement

- Allow configurable toolkit source example ([ES-3427](https://metasolutions.atlassian.net/browse/ES-3427))

#### Bug

- Use foaf:name in registry groups edit dialog ([ES-3331](https://metasolutions.atlassian.net/browse/ES-3331))

### Search

#### New feature

- Redesign search module ([ES-3267](https://metasolutions.atlassian.net/browse/ES-3267)) ([ES-3421](https://metasolutions.atlassian.net/browse/ES-3421))

### Terms

#### Bug

- Fix importing large terminologies without their concepts ([ES-3485](https://metasolutions.atlassian.net/browse/ES-3485))
- Cancel import when a terminology contains no concepts ([ES-3486](https://metasolutions.atlassian.net/browse/ES-3486))

### Workbench

#### Improvement

- Fix inconsistent remove behaviour ([ES-3448](https://metasolutions.atlassian.net/browse/ES-3448))
- Add edit button to the aggregation list items ([ES-3434](https://metasolutions.atlassian.net/browse/ES-3434))

## [3.12.5](https://bitbucket.org/metasolutions/entryscape/branches/compare/3.12.4%0D3.12.3) - 2024-08-12

### Added

- Add configuration to hide actions in list views ([ES-3435](https://metasolutions.atlassian.net/browse/ES-3435))

### Changed

- Make the link check report direct link available to guests ([ES-3420](https://metasolutions.atlassian.net/browse/ES-3420))
- Make the status report's public graph configurable ([ES-3420](https://metasolutions.atlassian.net/browse/ES-3420))

### Fixed

- Hide notifications from secondary navigation for guests ([ES-3420](https://metasolutions.atlassian.net/browse/ES-3420))
- Allow URI for `geonamesStart` configuration variable ([ES-3432](https://metasolutions.atlassian.net/browse/ES-3432))

## [3.12.4](https://bitbucket.org/metasolutions/entryscape/branches/compare/3.12.4%0D3.12.3) - 2024-06-19

### Fixed

- Wrong edit dialog in Forms view ([ES-3400](https://metasolutions.atlassian.net/browse/ES-3400))
- Distribution link to EntryStore resource treated as file ([ES-3395](https://metasolutions.atlassian.net/browse/ES-3395))

## [3.12.3](https://bitbucket.org/metasolutions/entryscape/branches/compare/3.12.3%0D3.12.2) - 2024-06-14

### Fixed

- Make pipeline transforms overridable via local config ([ES-3390](https://metasolutions.atlassian.net/browse/ES-3390))
- Uri pattern should replace undefined context name with empty string ([ES-3390](https://metasolutions.atlassian.net/browse/ES-3391))
- Extend select field options not available in deps editor ([ES-3392](https://metasolutions.atlassian.net/browse/ES-3392))

## [3.12.2](https://bitbucket.org/metasolutions/entryscape/branches/compare/3.12.2%0D3.12.1) - 2024-06-11

### Fixed

- Fix broken Replace File dialog in Catalog Documents ([ES-3381](https://metasolutions.atlassian.net/browse/ES-3381))
- Don't reset list state after edit ([ES-3383](https://metasolutions.atlassian.net/browse/ES-3383))
- Exported fields/forms get undefined text key if language is not set ([ES-3384](https://metasolutions.atlassian.net/browse/ES-3384))
- Object and section forms should not be included as primary classes ([ES-3385](https://metasolutions.atlassian.net/browse/ES-3385))
- Add selectable datatypes for datatype field ([ES-3386](https://metasolutions.atlassian.net/browse/ES-3386))
- Allow all field types for field dependencies ([ES-3388](https://metasolutions.atlassian.net/browse/ES-3388))
- Bump rdforms to 10.12.1

## [3.12.1](https://bitbucket.org/metasolutions/entryscape/branches/compare/3.12.1%0D3.12.0) - 2024-05-29

### Fixed

- Fix registry status reports with entities not appearing first ([ES-3373](https://metasolutions.atlassian.net/browse/ES-3373))
- Make deps extendable and show presenter in overview ([ES-3370](https://metasolutions.atlassian.net/browse/ES-3370))
- Missing translations in discard changes dialog for Form items dialog ([ES-3371](https://metasolutions.atlassian.net/browse/ES-3371))

## [3.12.0](https://bitbucket.org/metasolutions/entryscape/branches/compare/3.12.0%0D3.11.13) - 2024-05-17

### Added

- Add configuration variable to auto-expand status report filters ([ES-3194](https://metasolutions.atlassian.net/browse/ES-3194))
- Add error boundary for dialogs ([ES-3083](https://metasolutions.atlassian.net/browse/ES-3083))
- Add info icons to registry list items ([ES-3192](https://metasolutions.atlassian.net/browse/ES-3192))
- Add helper text explaining purpose on suggestion checklist dialog([ES-3188](https://metasolutions.atlassian.net/browse/ES-3188))
- Add LDB dialog to entity types and project types in admin ([ES-3096](https://metasolutions.atlassian.net/browse/ES-3096))
- Add dedicated minimum password length configuration ([ES-3222](https://metasolutions.atlassian.net/browse/ES-3222))
- Add scale line to maps ([ES-3056](https://metasolutions.atlassian.net/browse/ES-3056))
- RDFS: import classes and properties ([ES-2968](https://metasolutions.atlassian.net/browse/ES-2968))
- Models diagram: Label type control ([ES-2969](https://metasolutions.atlassian.net/browse/ES-2969))
- Add catalog statistics view axis title and api call placeholder title ([ES-3068](https://metasolutions.atlassian.net/browse/ES-3068))
- Add support for different filter default value ([ES-3263](https://metasolutions.atlassian.net/browse/ES-3263))
- Add high-value datasets filter on datasets and data services views ([ES-3053](https://metasolutions.atlassian.net/browse/ES-3053))
- Support WMS SRVC for WMS visualization detection ([ES-3278](https://metasolutions.atlassian.net/browse/ES-3278))
- Add sorting functionality to link checker table columns ([ES-3167](https://metasolutions.atlassian.net/browse/ES-3167))
- Add control for switching between different metadata on information dialog ([ES-3043](https://metasolutions.atlassian.net/browse/ES-3043))
- Preserve title and purpose in Models import/export ([ES-3344](https://metasolutions.atlassian.net/browse/ES-3344))
- Add edit buttons to forms and fields views ([ES-3343](https://metasolutions.atlassian.net/browse/ES-3343))
- Override form items on extend ([ES-3289](https://metasolutions.atlassian.net/browse/ES-3289))
- Add LDB for catalogs and distributions in dataset preview ([ES-3333](https://metasolutions.atlassian.net/browse/ES-3333))
- Add LDB for sharing settings in catalog ([ES-3295](https://metasolutions.atlassian.net/browse/ES-3295))

### Changed

- Make removing datasets safer by improving async handling ([ES-3079](https://metasolutions.atlassian.net/browse/ES-3079))
- Change the label on unnamed entities in LDB to specify the entity type ([ES-3020](https://metasolutions.atlassian.net/browse/ES-3020))
- Change Geonames dialog UI to be more consistent with rest of application ([ES-952](https://metasolutions.atlassian.net/browse/ES-952))
- Upgrade react router to version 6 ([ES-3161](https://metasolutions.atlassian.net/browse/ES-3161))
- Improve handling of dispatch for overviews and list views ([ES-3144](https://metasolutions.atlassian.net/browse/ES-3144))
- Use loading buttons for asynchronous actions ([ES-2217](https://metasolutions.atlassian.net/browse/ES-2217))
- Make snackbars closeable and increase error snackbar duration ([ES-3210](https://metasolutions.atlassian.net/browse/ES-2217))
- Replace old info dialog with LDB on Admin and Models list views ([ES-3158](https://metasolutions.atlassian.net/browse/ES-3158))
- Replace instances of ListFieldsInfoDialog with LDB ([ES-3225](https://metasolutions.atlassian.net/browse/ES-3225))
- Enable stricter password requirements by default ([ES-3223](https://metasolutions.atlassian.net/browse/ES-3223))
- Open dialog with filters already expanded if any are active ([ES-3150](https://metasolutions.atlassian.net/browse/ES-3150))
- Change the sort method in the concept hierarchy to a natural sort ([ES-3139](https://metasolutions.atlassian.net/browse/ES-3139))
- Make snackbar save messages more generic ([ES-3186](https://metasolutions.atlassian.net/browse/ES-3186))
- Add LDB dialog to toolkit explore ([ES-3102](https://metasolutions.atlassian.net/browse/ES-3102))
- Change comment to info icon button on suggestion list items in dataset overview ([ES-3250](https://metasolutions.atlassian.net/browse/ES-3250))
- Move linked datasets from the suggestion list item to the overview ([ES-3133](https://metasolutions.atlassian.net/browse/ES-3133))
- Always allow admin to edit and delete comments ([ES-3264](https://metasolutions.atlassian.net/browse/ES-3264))
- Make filters configuration more flexible ([ES-3262](https://metasolutions.atlassian.net/browse/ES-3262))
- Use ListItemButton instead of deprecated ListItem 'button' prop ([ES-3183](https://metasolutions.atlassian.net/browse/ES-3183))
- Change user menu's contact support link ([ES-3170](https://metasolutions.atlassian.net/browse/ES-3170))
- PSI-data web page on havesting report should only be visible for DCAT orgs ([ES-3292](https://metasolutions.atlassian.net/browse/ES-3292))
- Replace Lookup with entitytype-lookup lib ([ES-3232](https://metasolutions.atlassian.net/browse/ES-3232))
- Use nodetype resource as default for object forms ([ES-3290](https://metasolutions.atlassian.net/browse/ES-3290))

### Fixed

- Occassional discrepancy between bar chart value and length on status overview ([ES-3191](https://metasolutions.atlassian.net/browse/ES-3191))
- Fallback to other language strings on suggestion checklist dialog ([ES-3032](https://metasolutions.atlassian.net/browse/ES-3032))
- Fix file name not refreshing in replace file dialog ([ES-3207](https://metasolutions.atlassian.net/browse/ES-3207))
- Show a missing entry error when there is a wrong context in the URI ([ES-3195](https://metasolutions.atlassian.net/browse/ES-3195))
- Snackbars trigger view refresh ([ES-3145](https://metasolutions.atlassian.net/browse/ES-3145))
- Fix LDB breaking on Entity types built in view ([ES-3230](https://metasolutions.atlassian.net/browse/ES-3230))
- Fix map buttons' CSS on vizualisation preview and edit dialog ([ES-3134](https://metasolutions.atlassian.net/browse/ES-3134))
- Hide banner error when the form is valid in the edit dialog ([ES-3209](https://metasolutions.atlassian.net/browse/ES-3209))
- Fix missing tooltip from some create buttons ([ES-3256](https://metasolutions.atlassian.net/browse/ES-3256))
- Set recommended as default in create distribution dialog ([ES-3218](https://metasolutions.atlassian.net/browse/ES-3218))
- Disallow users to edit and delete comments they did not post ([ES-2919](https://metasolutions.atlassian.net/browse/ES-2919))
- Handle unauthorised in dataset preview ([ES-3084](https://metasolutions.atlassian.net/browse/ES-3084))
- Fix back navigation not working after accessing registry organizations as a guest ([ES-2430](https://metasolutions.atlassian.net/browse/ES-2430))
- Constraint values included as comma separated strings instead of array ([ES-3282](https://metasolutions.atlassian.net/browse/ES-3282))
- Edit attributes button broken on forms that extend another form ([ES-3318](https://metasolutions.atlassian.net/browse/ES-3318))
- Fix not showing analyze step errors during terminology import ([ES-3284](https://metasolutions.atlassian.net/browse/ES-3284))
- Fix not showing image if accessed through LDB references ([ES-3325](https://metasolutions.atlassian.net/browse/ES-3325))
- Fix collection overview lists breaking when containing a removed concept ([ES-3298](https://metasolutions.atlassian.net/browse/ES-3298))
- Fix 'spatial reference system' field validation not working ([ES-3326](https://metasolutions.atlassian.net/browse/ES-3326))

### Security

- Upgrade and audit dependencies ([ES-3281](https://metasolutions.atlassian.net/browse/ES-3281))

## [3.11.13](https://bitbucket.org/metasolutions/entryscape/branches/compare/3.11.13%0D3.11.12) - 2024-04-15

### Fixed

- Can't create concept if concept limit is set ([ES-3271](https://metasolutions.atlassian.net/browse/ES-3271))

## [3.11.12](https://bitbucket.org/metasolutions/entryscape/branches/compare/3.11.12%0D3.11.11) - 2024-04-10

### Fixed

- Can't add or remove constraint in advanced mode ([ES-3265](https://metasolutions.atlassian.net/browse/ES-3265))
- Provide binding instead of item to search choice ([ES-3266](https://metasolutions.atlassian.net/browse/ES-3266))

## [3.11.11](https://bitbucket.org/metasolutions/entryscape/branches/compare/3.11.11%0D3.11.10) - 2024-04-02

### Fixed

- Template metadata not shown in dataset edit dialog ([ES-3258](https://metasolutions.atlassian.net/browse/ES-3258))
- Check references on contact removal ([ES-3255](https://metasolutions.atlassian.net/browse/ES-3255))

## [3.11.10](https://bitbucket.org/metasolutions/entryscape/branches/compare/3.11.10%0D3.11.9) - 2024-03-25

### Added

- Add 'profile' to merge transform arguments ([ES-3239](https://metasolutions.atlassian.net/browse/ES-3239))
- Add option to exclude default store in Skos Chooser ([ES-3235](https://metasolutions.atlassian.net/browse/ES-3235))

### Fixed

- Fix status DCAT-specific NLS ([ES-3241](https://metasolutions.atlassian.net/browse/ES-3241))
- Use view's `navbar` config to determine organization dialog's tab visibility ([ES-3240](https://metasolutions.atlassian.net/browse/ES-3240))
- Template property can't be overridden for catalog entity types ([ES-3238](https://metasolutions.atlassian.net/browse/ES-3238))
- Find entity type by constraints sorts matches in wrong order ([ES-3237](https://metasolutions.atlassian.net/browse/ES-3237))
- Remote store not implemented for inline search ([ES-3236](https://metasolutions.atlassian.net/browse/ES-3236))
- Enable remote store by default ([ES-3234](https://metasolutions.atlassian.net/browse/ES-3234))

## [3.11.9](https://bitbucket.org/metasolutions/entryscape/branches/compare/3.11.9%0D3.11.8) - 2024-03-18

### Fixed

- Toolkit validate broken ([ES-3229](https://metasolutions.atlassian.net/browse/ES-3229))

## [3.11.8](https://bitbucket.org/metasolutions/entryscape/branches/compare/3.11.8%0D3.11.7) - 2024-03-18

### Fixed

- Configure entity types dialog fails to list entity types ([ES-3226](https://metasolutions.atlassian.net/browse/ES-3226))
- Name conflict for builtin entity type admsPublisher ([ES-3227](https://metasolutions.atlassian.net/browse/ES-3227))

## [3.11.7](https://bitbucket.org/metasolutions/entryscape/branches/compare/3.11.7%0D3.11.6) - 2024-03-15

### Fixed

- Builtin configs merged with custom config ([ES-3221](https://metasolutions.atlassian.net/browse/ES-3221))
- Shared entity types breaks Workbench entity list view ([ES-3220](https://metasolutions.atlassian.net/browse/ES-3220))

## [3.11.6](https://bitbucket.org/metasolutions/entryscape/branches/compare/3.11.6%0D3.11.5) - 2024-03-07

### Fixed

- Add dcatap:applicableLegislation on distribution.
- Make dcatap:hvdCategory mandatory if the applicableLegislation is set.
- Change the wording of HVD in Swedish.
- Relax the module restriction of the concept entitytype so it is available in all modules.

## [3.11.5](https://bitbucket.org/metasolutions/entryscape/branches/compare/3.11.5%0D3.11.4) - 2024-02-13

### Fixed

- Namespaces view broken when default namespace is set ([ES-3179](https://metasolutions.atlassian.net/browse/ES-3179))
- Forms preview broken ([ES-3181](https://metasolutions.atlassian.net/browse/ES-3181))
- No tooltip on edit icons ([ES-3182](https://metasolutions.atlassian.net/browse/ES-3182))

### Fixed

- Fix slight jump when swapping between the details of a concept and the placeholder ([ES-3097](https://metasolutions.atlassian.net/browse/ES-3097))

## [3.11.4](https://bitbucket.org/metasolutions/entryscape/branches/compare/3.11.4%0D3.11.3) - 2024-02-09

### Fixed

- Dataset overview error if allowInternalDatasetPublish is enabled ([ES-3177](https://metasolutions.atlassian.net/browse/ES-3177))

## [3.11.3](https://bitbucket.org/metasolutions/entryscape/branches/compare/3.11.3%0D3.11.2) - 2024-02-09

### Fixed

- Mismatch of entity types in entry select list and entry chooser ([ES-3172](https://metasolutions.atlassian.net/browse/ES-3172))

## [3.11.2](https://bitbucket.org/metasolutions/entryscape/branches/compare/3.11.2%0D3.11.1) - 2024-02-08

### Fixed

- Reintroduce support for hash params ([ES-3171](https://metasolutions.atlassian.net/browse/ES-3171))

## [3.11.1](https://bitbucket.org/metasolutions/entryscape/branches/compare/3.11.1%0D3.11.0) - 2024-02-08

### Fixed

- Don't throw rdforms template errors ([ES-3162](https://metasolutions.atlassian.net/browse/ES-3162))

## [3.11.0](https://bitbucket.org/metasolutions/entryscape/branches/compare/3.11.0%0D3.10.4) - 2024-02-07

### Fixed

- Fix joint.js included in app bundle ([ES-2964](https://metasolutions.atlassian.net/browse/ES-2964))
- Fix cardinality not shown if not set in fields and object form ([ES-2973](https://metasolutions.atlassian.net/browse/ES-2973))
- Context name undefined in Models build item id ([ES-2992](https://metasolutions.atlassian.net/browse/ES-2992))
- Fix the number of metadata columns on overviews ([ES-2995](https://metasolutions.atlassian.net/browse/ES-2995))
- Improve linked data browser dialog UX and functionality ([ES-2990](https://metasolutions.atlassian.net/browse/ES-2990))
- Make links in the info tab open within the linked data browser dialog ([ES-3006](https://metasolutions.atlassian.net/browse/ES-3006))
- Fix error on attempting to create Project Type ([ES-3046](https://metasolutions.atlassian.net/browse/ES-3046))
- Fix visualisation preview source button area ([ES-2917](https://metasolutions.atlassian.net/browse/ES-2917))
- Prevent loading dialog from staying open after request resolved ([ES-3061](https://metasolutions.atlassian.net/browse/ES-3061))
- Add Publisher translation to inline create ([ES-3064](https://metasolutions.atlassian.net/browse/ES-3064))
- Prevent namespaces edit dialog from submitting when there is an error ([ES-3074](https://metasolutions.atlassian.net/browse/ES-3074))
- Fix list placeholders for Distributions and Visualisations ([ES-3052](https://metasolutions.atlassian.net/browse/ES-3052))
- Refresh API fails for distributions with more than 2 files ([ES-3011](https://metasolutions.atlassian.net/browse/ES-3011))
- Fix actions menu on organizations view not being visible to non-admin managers ([ES-3071](https://metasolutions.atlassian.net/browse/ES-3071))
- Refresh FieldOverview after edit ([ES-3075](https://metasolutions.atlassian.net/browse/ES-3075))
- Namespaces not included in rdforms bundle ([ES-3125](https://metasolutions.atlassian.net/browse/ES-3125))
- User and action visibility issues in Admin ([ES-3073](https://metasolutions.atlassian.net/browse/ES-3073))
- User name not refreshed when changing name in user settings ([ES-3160](https://metasolutions.atlassian.net/browse/ES-3160))
- Entity type view is broken when no local entity types ([ES-2987](https://metasolutions.atlassian.net/browse/ES-2987))
- Collection overview breaking when using a project type without a remote store configured ([ES-3127](https://metasolutions.atlassian.net/browse/ES-3127))
- Fix some lists not respecting rows per page selection ([ES-3092](https://metasolutions.atlassian.net/browse/ES-3092))
- Refresh the overview and close the dialog on revision success ([ES-3124](https://metasolutions.atlassian.net/browse/ES-3124))
- Information icon button position on some overviews ([ES-3107](https://metasolutions.atlassian.net/browse/ES-3107))
- Empty list placeholder label on group members dialog and sharing settings ([ES-3132](https://metasolutions.atlassian.net/browse/ES-3132))
- Fix color and size of visualization type icon in dataset preview ([ES-3135](https://metasolutions.atlassian.net/browse/ES-3135))
- Forms outline losing focus under specific circumastances ([ES-3154](https://metasolutions.atlassian.net/browse/ES-3154))

### Changed

- Replace createField with Field class ([ES-2963](https://metasolutions.atlassian.net/browse/ES-2963))
- Trim username and password on signin, signup, password reset and change ([ES-2997](https://metasolutions.atlassian.net/browse/ES-2997))
- Remove suggestion row title click ([ES-3024](https://metasolutions.atlassian.net/browse/ES-3024))
- Expand external metadata by default when no other data available in LDB dialog ([ES-3025](https://metasolutions.atlassian.net/browse/ES-3025))
- Drop IE 11 support in build step ([ES-3058](https://metasolutions.atlassian.net/browse/ES-3058))
- Add tag to prevent web crawlers ([ES-3065](https://metasolutions.atlassian.net/browse/ES-3065))
- Hide references section in Dataset preview ([ES-3082](https://metasolutions.atlassian.net/browse/ES-3082))
- Merge status report public/private sector lists and use a filter instead ([ES-3000](https://metasolutions.atlassian.net/browse/ES-3000))
- Minimize the risk of conflicts when creating, deleting and moving concepts ([ES-3141](https://metasolutions.atlassian.net/browse/ES-3141))
- Icon and icon button colours are now applied on theme-level ([ES-3153](https://metasolutions.atlassian.net/browse/ES-3153))

### Added

- Add snackbar to Revert action in revisions dialog ([ES-1707](https://metasolutions.atlassian.net/browse/ES-1707))
- Add ability to select how many rows appear in lists ([ES-2988](https://metasolutions.atlassian.net/browse/ES-2988))
- Add snackbars to create, edit, delete, and publish throughout app ([ES-2965](https://metasolutions.atlassian.net/browse/ES-2965))
- Introduce Linked Data Browser dialog to Catalog and Terms ([ES-2978](https://metasolutions.atlassian.net/browse/ES-2978))
- Entity and project types in registry ([ES-3008](https://metasolutions.atlassian.net/browse/ES-3008))
- New information presentation for non-DCAT pipelines ([ES-3009](https://metasolutions.atlassian.net/browse/ES-3009))
- Support for non-dataset entities on harvesting report ([ES-2976](https://metasolutions.atlassian.net/browse/ES-2976))
- Support for creating and editing non-DCAT pipelines ([ES-2729](https://metasolutions.atlassian.net/browse/ES-2729))
- Add overviews throughout the application ([ES-2322](https://metasolutions.atlassian.net/browse/ES-2322))
- Add Used by list in Object form overview ([ES-3013](https://metasolutions.atlassian.net/browse/ES-3013))
- List refresh button now animates on click ([ES-3021](https://metasolutions.atlassian.net/browse/ES-3021))
- Introduce icons to overview sidebar buttons ([ES-3039](https://metasolutions.atlassian.net/browse/ES-3039))
- Add DWG and DXF as geographical mediatypes ([ES-3033](https://metasolutions.atlassian.net/browse/ES-3033))
- Show template level in preview for Models fields ([ES-3012](https://metasolutions.atlassian.net/browse/ES-3012))
- Support prof specification in Models ([ES-2984](https://metasolutions.atlassian.net/browse/ES-2984))
- Export RDFS as a resource ([ES-3041](https://metasolutions.atlassian.net/browse/ES-3041))
- Export diagram as Prof resource ([ES-3014](https://metasolutions.atlassian.net/browse/ES-3014))
- Specification preview ([ES-3054](https://metasolutions.atlassian.net/browse/ES-3054))
- Select class edit mode for advanced constraint values ([ES-2943](https://metasolutions.atlassian.net/browse/ES-2943))
- Validation for contact email ([ES-3131](https://metasolutions.atlassian.net/browse/ES-3131))
- Add linked data browser to concept details view ([ES-3140](https://metasolutions.atlassian.net/browse/ES-3140))

### Removed

- Remove hash params based redirect from index files ([ES-3086](https://metasolutions.atlassian.net/browse/ES-3086))

### Security

- Upgrade and audit dependencies ([ES-3067](https://metasolutions.atlassian.net/browse/ES-3067))
- Limit options for loading alternate config via localconfig ([ES-3085](https://metasolutions.atlassian.net/browse/ES-3085))

## [3.10.4](https://bitbucket.org/metasolutions/entryscape/branches/compare/3.10.4%0D3.10.3) - 2023-11-23

## Fixed

- Create dataset from non-template-based suggestion doesn't work ([ES-3029](https://metasolutions.atlassian.net/browse/ES-3029))

## [3.10.3](https://bitbucket.org/metasolutions/entryscape/branches/compare/3.10.3%0D3.10.2) - 2023-11-20

## Fixed

- Fix string error in Models Diagram when reading constraints

## [3.10.2](https://bitbucket.org/metasolutions/entryscape/branches/compare/3.10.2%0D3.10.1) - 2023-11-09

## Fixed

- Update to version 2.7.5 of Rdfjson library which contains fix for CDATA in RDF/XML import
- Changed geodcat template expression to facilitate better integrations with models
- Added HVD categories that where not defined officially at the time of release
- Diagram saved bug fixed, caused by multiple URIs where saved as a single URI by mistake
- Detect owl:Class in RDF import to models
- Corrected link from inlineField to objectForm in field overview view in models
- Reference inlineField rather than objectForm in profile forms when importing
- Connect extentions in fields, forms and form items across imports in models

## [3.10.1](https://bitbucket.org/metasolutions/entryscape/branches/compare/3.10.1%0D3.10.0) - 2023-10-10

### Fixed

- Correct ACL set on user creation from Registry organization view.

## [3.10.0](https://bitbucket.org/metasolutions/entryscape/branches/compare/3.10.0%0D3.9.4) - 2023-10-02

### Added

- Change template to allow titles to be provided on downloadURL and accessURLs ([ES-2821](https://metasolutions.atlassian.net/browse/ES-2821))
- Handle "rejected" status in lists, displaying an appropriate placeholder ([ES-2817](https://metasolutions.atlassian.net/browse/ES-2817))
- Tooltips on list item icon buttons ([ES-2854](https://metasolutions.atlassian.net/browse/ES-2854))
- Helper text for metadata fields with simple web address pattern in common templates ([ES-2799](https://metasolutions.atlassian.net/browse/ES-2799))
- Add applicable Legislation to support HVD ([ES-2955](https://metasolutions.atlassian.net/browse/ES-2955))
- Show datatype editor in create field dialog ([ES-2819](https://metasolutions.atlassian.net/browse/ES-2819))
- Copy title to label in field and form create dialog ([ES-2862](https://metasolutions.atlassian.net/browse/ES-2862))
- Introduce editing of diagram metadata ([ES-2842](https://metasolutions.atlassian.net/browse/ES-2842))
- Introduce functionality to remove diagrams ([ES-2843](https://metasolutions.atlassian.net/browse/ES-2843))
- Add default value for select fields ([ES-2858](https://metasolutions.atlassian.net/browse/ES-2858))
- Link to object form should be shown in inline field overview ([ES-2954](https://metasolutions.atlassian.net/browse/ES-2954))
- Warn about unsaved changes in Form items dialog ([ES-2846](https://metasolutions.atlassian.net/browse/ES-2846))
- Use edit modes on advanced constraint editor ([ES-2820](https://metasolutions.atlassian.net/browse/ES-2820))
- Add level selector in preview form ([ES-2738](https://metasolutions.atlassian.net/browse/ES-2738))
- Edit button directly on Workbench entity list items ([ES-2853](https://metasolutions.atlassian.net/browse/ES-2853))
- Introduce linked data browser dialog to Workbench ([ES-2708](https://metasolutions.atlassian.net/browse/ES-2708))

### Fixed

- User modification date now updates on username or password change ([ES-2798](https://metasolutions.atlassian.net/browse/ES-2798))
- Entity type filter breaks after paginating ([ES-2913](https://metasolutions.atlassian.net/browse/ES-2913))
- Remove nls dependency to Models in Entity type editor ([ES-2884](https://metasolutions.atlassian.net/browse/ES-2884))
- Align suggestion linked dataset's columns with parent suggestion ([ES-2788](https://metasolutions.atlassian.net/browse/ES-2788))
- Improve visualisation popup's fit in viewport ([ES-2778](https://metasolutions.atlassian.net/browse/ES-2778))
- Fix save button being enabled in the edit dialog for WMS visualisations before any changes ([ES-2906](https://metasolutions.atlassian.net/browse/ES-2906))
- Fix placeholder flicker on distribution and visualization lists ([ES-2947](https://metasolutions.atlassian.net/browse/ES-2947))
- Fix wrong label on close button in Showcases dialog ([ES-2910](https://metasolutions.atlassian.net/browse/ES-2910))
- Fix wrong placeholder when loading all suggestions ([ES-2928](https://metasolutions.atlassian.net/browse/ES-2928))
- Placeholder flickering on list query lists ([ES-2824](https://metasolutions.atlassian.net/browse/ES-2824))
- Bounds should not be updated on opening editor ([ES-2881](https://metasolutions.atlassian.net/browse/ES-2881))
- Wrong media type for n-triples in download dialog ([ES-2880](https://metasolutions.atlassian.net/browse/ES-2880))
- ListPagination flickering ([ES-2948](https://metasolutions.atlassian.net/browse/ES-2948))
- Fix save button being enabled in Table View before any changes ([ES-2911](https://metasolutions.atlassian.net/browse/ES-2911))
- Preserve new lines in overview description and preview ([ES-2944](https://metasolutions.atlassian.net/browse/ES-2944))
- Contact telephone has (mandatory) asterisk even tho it's recommended ([ES-2922](https://metasolutions.atlassian.net/browse/ES-2922))
- Fix placeholder flicker on search module ([ES-2936](https://metasolutions.atlassian.net/browse/ES-2936))
- Include inline fields in Add form item dialog ([ES-2844](https://metasolutions.atlassian.net/browse/ES-2844))
- Adding inline field to an object form may cause circular deps ([ES-2849](https://metasolutions.atlassian.net/browse/ES-2849))
- Reset option values for select field on nodetype change ([ES-2848](https://metasolutions.atlassian.net/browse/ES-2848))
- New entries not showing up on initial refresh after creating a class or property ([ES-2887](https://metasolutions.atlassian.net/browse/ES-2887))
- Incomplete conversion of option entry values ([ES-2877](https://metasolutions.atlassian.net/browse/ES-2877))
- Inline field shown as type Object form in info dialog ([ES-2896](https://metasolutions.atlassian.net/browse/ES-2896))
- Both inline and object forms are not included as dependencies in Models build ([ES-2905](https://metasolutions.atlassian.net/browse/ES-2905))
- Can't clear language field for text field ([ES-2925](https://metasolutions.atlassian.net/browse/ES-2925))
- Hide nodetype selection for Lookup field ([ES-2931](https://metasolutions.atlassian.net/browse/ES-2931))
- Weird language clearing behaviour on text field ([ES-2950](https://metasolutions.atlassian.net/browse/ES-2950))
- Used by is not shown for fields included in profile forms ([ES-2953](https://metasolutions.atlassian.net/browse/ES-2953))
- Link inline field property to object form in Diagrams ([ES-2956](https://metasolutions.atlassian.net/browse/ES-2956))
- Improve zoom and centering on diagrams ([ES-2840](https://metasolutions.atlassian.net/browse/ES-2840))
- Improve way to switch between diagrams ([ES-2841](https://metasolutions.atlassian.net/browse/ES-2841))
- Give Import Namespace dialog a fixed height ([ES-2926](https://metasolutions.atlassian.net/browse/ES-2926))
- Display more specific error in Classes info dialog ([ES-2957](https://metasolutions.atlassian.net/browse/ES-2957))
- Handle errors on diagram view ([ES-2958](https://metasolutions.atlassian.net/browse/ES-2958))
- Import property forms as inline field/object form ([ES-2893](https://metasolutions.atlassian.net/browse/ES-2893))
- Fix wrong placeholder when creating inline field ([ES-2927](https://metasolutions.atlassian.net/browse/ES-2927))
- Prevent error when selection is empty in Builds ([ES-2959](https://metasolutions.atlassian.net/browse/ES-2959))
- Field property editor lacks clear button ([ES-2921](https://metasolutions.atlassian.net/browse/ES-2921))
- Fix diagram changes not being saved ([ES-2908](https://metasolutions.atlassian.net/browse/ES-2908))
- Registry public sector web pages and datasets checkmarks always being checked ([ES-2882](https://metasolutions.atlassian.net/browse/ES-2882))
- List loading in edit collection concepts dialog to avoid corrupt state ([ES-2637](https://metasolutions.atlassian.net/browse/ES-2637))
- Prevent placeholder flicker and show loading indicator on collection overview lists ([ES-2811](https://metasolutions.atlassian.net/browse/ES-2811))
- Hide project-type-specific entity types when not using a project type in Workbench ([ES-2767](https://metasolutions.atlassian.net/browse/ES-2767))
- Entitytype searchProps not respected when searching for sub-entitytypes ([ES-2875](https://metasolutions.atlassian.net/browse/ES-2875))
- Related entites should be hidden in metadata editor ([ES-2876](https://metasolutions.atlassian.net/browse/ES-2876))
- Hide composition entity types from overview ([ES-2930](https://metasolutions.atlassian.net/browse/ES-2930))

### Changed

- Remove search from outline sidebar when there are no fields to display ([ES-2776](https://metasolutions.atlassian.net/browse/ES-2776))
- Update the UI on OpenLayers in EntryScape ([ES-2751](https://metasolutions.atlassian.net/browse/ES-2751))
- More info dialogs now display references ([ES-2867](https://metasolutions.atlassian.net/browse/ES-2867))
- Require existing password when changing to a new one ([ES-2601](https://metasolutions.atlassian.net/browse/ES-2601))
- Use common filter action in Add form item and extends dialog ([ES-2845](https://metasolutions.atlassian.net/browse/ES-2845))

### Security

- Upgrade and audit dependencies ([ES-2868](https://metasolutions.atlassian.net/browse/ES-2868))

## [3.9.4](https://bitbucket.org/metasolutions/entryscape/branches/compare/3.9.4%0D3.9.3) - 2023-09-27

## Fixed

- Create dataset from suggestion action not working ([ES-2945](https://metasolutions.atlassian.net/browse/ES-2945))

## [3.9.3](https://bitbucket.org/metasolutions/entryscape/branches/compare/3.9.3%0D3.9.2) - 2023-09-21

## Fixed

- Datasets created from suggestions based on dataset templates not carrying over most of the metadata ([ES-2920](https://metasolutions.atlassian.net/browse/ES-2920))

## [3.9.2](https://bitbucket.org/metasolutions/entryscape/branches/compare/3.9.2%0D3.9.1) - 2023-08-29

## Fixed

- Primary navigation in registry being hidden to guests ([ES-2870](https://metasolutions.atlassian.net/browse/ES-2870))

## [3.9.1](https://bitbucket.org/metasolutions/entryscape/branches/compare/3.9.1%0D3.9.0) - 2023-08-25

## Added

- Option to load fonts from custom urls ([ES-2855](https://metasolutions.atlassian.net/browse/ES-2855))

### Fixed

- Hide primary navigation when not logged in ([ES-2851](https://metasolutions.atlassian.net/browse/ES-2851))
- Show explanatory confirmation dialog on "external" toggle ([ES-2852](https://metasolutions.atlassian.net/browse/ES-2852))
- Missing nls string invalidChar ([ES-2850](https://metasolutions.atlassian.net/browse/ES-2850))
- Entity type templateLevel is not respected in create dialog ([ES-2857](https://metasolutions.atlassian.net/browse/ES-2857))
- Using uri templates breaks Create entry dialog ([ES-2863](https://metasolutions.atlassian.net/browse/ES-2863))

## [3.9.0](https://bitbucket.org/metasolutions/entryscape/branches/compare/3.9.0%0D3.8.2) - 2023-07-05

### Added

- Add remove action to builds view under Models ([ES-2715](https://metasolutions.atlassian.net/browse/ES-2715))
- Split property form into object form and inline field ([ES-2718](https://metasolutions.atlassian.net/browse/ES-2718))
- Improved validation in form/field create/edit dialogs ([ES-2024](https://metasolutions.atlassian.net/browse/ES-2024))
- Add nodetype and property in create dialogs ([ES-2684](https://metasolutions.atlassian.net/browse/ES-2684))
- Show number of select field options in field overview ([ES-2760](https://metasolutions.atlassian.net/browse/ES-2760))
- Add filter on type in forms/fields list views ([ES-2732](https://metasolutions.atlassian.net/browse/ES-2732))
- Add resource as selectable nodetype for object form ([ES-2733](https://metasolutions.atlassian.net/browse/ES-2733))
- Use entries to link classes and properties to fields and forms ([ES-2701](https://metasolutions.atlassian.net/browse/ES-2701))
- Use entries to link namespaces to fields and forms ([ES-2700](https://metasolutions.atlassian.net/browse/ES-2700))
- Add error handling for forms/fields preview ([ES-2171](https://metasolutions.atlassian.net/browse/ES-2171))
- Add button to remove form constraints ([ES-2184](https://metasolutions.atlassian.net/browse/ES-2184))
- Add way to get asynchronous layout props, moving Workbench entity types to secondary navigation ([ES-2702](https://metasolutions.atlassian.net/browse/ES-2702))
- Add functionality for composition relations/ sub entity types in Workbench ([ES-2704](https://metasolutions.atlassian.net/browse/ES-2704))
- Add aggregation relations in Workbench ([ES-2714](https://metasolutions.atlassian.net/browse/ES-2714), [ES-2758](https://metasolutions.atlassian.net/browse/ES-2758))
- Option to load filter items in async way ([ES-2727](https://metasolutions.atlassian.net/browse/ES-2727))
- Getter for entity type definition properties ([ES-2745](https://metasolutions.atlassian.net/browse/ES-2745))
- Add current file name in replace file dialog ([ES-1040](https://metasolutions.atlassian.net/browse/ES-1040))
- Support for file URI scheme ([ES-2611](https://metasolutions.atlassian.net/browse/ES-2611))
- Add manual mode for editing uri option values ([ES-2818](https://metasolutions.atlassian.net/browse/ES-2818))

### Changed

- Display the number of entries of each entity type in the Configure entity types dialog ([ES-2713](https://metasolutions.atlassian.net/browse/ES-2713))
- Change remove link action icon ([ES-2782](https://metasolutions.atlassian.net/browse/ES-2782))
- Improve property name labels in Models ([ES-2744](https://metasolutions.atlassian.net/browse/ES-2744))
- List where a field/form is used/extended ([ES-2599](https://metasolutions.atlassian.net/browse/ES-2599))
- Upgrade to React version 18.2.0 ([ES-2693](https://metasolutions.atlassian.net/browse/ES-2693))

### Removed

- Remove typeindex and replace with lookup ([ES-2578](https://metasolutions.atlassian.net/browse/ES-2578))

### Fixed

- Handle type change in forms ([ES-2720](https://metasolutions.atlassian.net/browse/ES-2720))
- Form label missing in preview ([ES-2728](https://metasolutions.atlassian.net/browse/ES-2728))
- Filter presenter values in form/field overviews ([ES-2731](https://metasolutions.atlassian.net/browse/ES-2731))
- Mandatory marker not shown in field/form preview ([ES-2737](https://metasolutions.atlassian.net/browse/ES-2737))
- Forms/fields preview scroll as one page ([ES-2734](https://metasolutions.atlassian.net/browse/ES-2734))
- Allow extending title and purpose when extending field or form ([ES-2735](https://metasolutions.atlassian.net/browse/ES-2735))
- Extend select field throws error on edit options ([ES-2736](https://metasolutions.atlassian.net/browse/ES-2736))
- Colour for deprecated icon in toolkit validation report ([ES-2769](https://metasolutions.atlassian.net/browse/ES-2769))
- Invalid URL error with trailing/leading space on create organization dialog ([ES-2806](https://metasolutions.atlassian.net/browse/ES-2806))
- Inconsistent source terminology refresh ([ES-2785](https://metasolutions.atlassian.net/browse/ES-2785))
- Disable credentials on remote stores ([ES-2766](https://metasolutions.atlassian.net/browse/ES-2766))
- Circular dependency of section form item causes infinite loop ([ES-2813](https://metasolutions.atlassian.net/browse/ES-2813))

## [3.8.2](https://bitbucket.org/metasolutions/entryscape/branches/compare/3.8.2%0D3.8.1) - 2023-06-09

### Fixed

- Broken links between dataset and distributions ([ES-2747](https://metasolutions.atlassian.net/browse/ES-2747))
- Infinite loop form diagram ([ES-2739](https://metasolutions.atlassian.net/browse/ES-2739))
- Make distribution file template configurable ([ES-2756](https://metasolutions.atlassian.net/browse/ES-2756))
- Download dialog for entity list
- Use full width for Models diagram

## [3.8.1](https://bitbucket.org/metasolutions/entryscape/branches/compare/3.8.1%0D3.8.0) - 2023-05-12

### Fixed

- Model view breaks if field or form has been removed ([ES-2724](https://metasolutions.atlassian.net/browse/ES-2724))
- Wrong placeholder in entity types view ([ES-2725](https://metasolutions.atlassian.net/browse/ES-2725))
- Wrong detection of relations in form diagrams ([ES-2726](https://metasolutions.atlassian.net/browse/ES-2726))
- Set guest read on metadata on published context

### Changed

- Bump rdforms to 10.9.4

### Added

- Add signup information in registry

## [3.8.0](https://bitbucket.org/metasolutions/entryscape/branches/compare/3.8.0%0D3.7.7) - 2023-05-03

### Added

- Add a snackbar on catalog creation ([ES-2557](https://metasolutions.atlassian.net/browse/ES-2557))
- Add dataset overview back button tooltip ([ES-2435](https://metasolutions.atlassian.net/browse/ES-2435))
- Show feature information on csv map visualization ([ES-2037](https://metasolutions.atlassian.net/browse/ES-2037))
- Import dialog for RDF Schema in Models ([ES-2512](https://metasolutions.atlassian.net/browse/ES-2512))
- Classes based diagram in Models ([ES-2513](https://metasolutions.atlassian.net/browse/ES-2513))
- Include namespaces in Models build ([ES-2617](https://metasolutions.atlassian.net/browse/ES-2617))
- Add namespaces view under Models for creating and managing namespaces ([ES-2518](https://metasolutions.atlassian.net/browse/ES-2518))
- Show purpose as subtitle for fields and forms lists ([ES-2584](https://metasolutions.atlassian.net/browse/ES-2584))
- Profile chooser on concept edit dialog ([ES-2529](https://metasolutions.atlassian.net/browse/ES-2529))
- Add project type view under Admin for creating and managing project types ([ES-2334](https://metasolutions.atlassian.net/browse/ES-2334))
- Add icon to disabled user in groups list ([ES-2455](https://metasolutions.atlassian.net/browse/ES-2455))
- Introduce solutions as a way to handle common configurations ([ES-2505](https://metasolutions.atlassian.net/browse/ES-2505))
- Add info icon and dialog to most list items ([ES-2322](https://metasolutions.atlassian.net/browse/ES-2322))
- Add a more convenient way to check user permissions ([ES-1907](https://metasolutions.atlassian.net/browse/ES-1907))
- Show grab cursor when panning map ([ES-2606](https://metasolutions.atlassian.net/browse/ES-2606))
- Add store config in the entity types information dialog ([ES-2603](https://metasolutions.atlassian.net/browse/ES-2603))

### Changed

- Show loading indicator after editing a suggestion ([ES-2348](https://metasolutions.atlassian.net/browse/ES-2348))
- Open a linked dataset's overview on a new tab ([ES-2418](https://metasolutions.atlassian.net/browse/ES-2418))
- Replace widget icon with dataset icon in dataset overview ([ES-2340](https://metasolutions.atlassian.net/browse/ES-2340))
- Disable personal projects by default, making them available through a config variable ([ES-2548](https://metasolutions.atlassian.net/browse/ES-2548))
- Clarified terms hierarchy concept deselect area ([ES-2497](https://metasolutions.atlassian.net/browse/ES-2497))
- Use namespace selector for options and constraint object ([ES-2670](https://metasolutions.atlassian.net/browse/ES-2670))
- Migrate all lists to new composable components ([ES-2467](https://metasolutions.atlassian.net/browse/ES-2467))
- Common entry info dialog for non rdforms templates ([ES-2579](https://metasolutions.atlassian.net/browse/ES-2579))

### Fixed

- Sort by clicking on headers in group membership dialog (in admin>users) does not work ([ES-1551](https://metasolutions.atlassian.net/browse/ES-1551))
- Modified date sorting doesn't work on group members dialog ([ES-2441](https://metasolutions.atlassian.net/browse/ES-2441))
- Fix list refresh on admin list when group members are changed ([ES-2681](https://metasolutions.atlassian.net/browse/ES-2681))
- Upgrade/downgrade premium list action is not refreshed ([ES-2613](https://metasolutions.atlassian.net/browse/ES-2613))
- Set nodetype, presenter and editor for the value field when handling an Option ([ES-2546](https://metasolutions.atlassian.net/browse/ES-2546))
- Use correct field value when previewing a Lookup Field
- Add missing star to mandatory Extends field in the extend dialog ([ES-2656](https://metasolutions.atlassian.net/browse/ES-2656))
- Extended fields and forms from other models breaks preview/build ([ES-2682](https://metasolutions.atlassian.net/browse/ES-2682))
- Datasets view making double requests ([ES-2516](https://metasolutions.atlassian.net/browse/ES-2516))
- Apply include psi config for dataset filters ([ES-2547](https://metasolutions.atlassian.net/browse/ES-2547))
- Add ACL to uploaded distribution file entries ([ES-2576](https://metasolutions.atlassian.net/browse/ES-2576))
- Add missing translations on Table View ([ES-2536](https://metasolutions.atlassian.net/browse/ES-2536))
- Prevent action icon in Table View from be clickable ([ES-2683](https://metasolutions.atlassian.net/browse/ES-2683))
- Background map visibility ignored for WMS viz ([ES-2669](https://metasolutions.atlassian.net/browse/ES-2669))
- Wait for imports to complete before refreshing the documents list ([ES-2655](https://metasolutions.atlassian.net/browse/ES-2655))
- Fix placeholder placement on concepts list in collections overview ([ES-2668](https://metasolutions.atlassian.net/browse/ES-2668))
- Fix title alignment in showDialog ([ES-2689](https://metasolutions.atlassian.net/browse/ES-2689))
- Fix image preview for image created via link in Workbench ([ES-2643](https://metasolutions.atlassian.net/browse/ES-2643))
- Password criteria now apply everywhere the password can be set/changed ([ES-2596](https://metasolutions.atlassian.net/browse/ES-2596))
- Form outline wrongly shows invisible groups and labels ([ES-2595](https://metasolutions.atlassian.net/browse/ES-2595))
- Fix keynav on ActionsMenu ([ES-2466](https://metasolutions.atlassian.net/browse/ES-2466))
- Truncate list item and overview header item and add tooltip when truncated ([ES-2351](https://metasolutions.atlassian.net/browse/ES-2351))
- Fix icon colour on some list items ([ES-2654](https://metasolutions.atlassian.net/browse/ES-2654))
- Fix password reset validation error ([ES-2648](https://metasolutions.atlassian.net/browse/ES-2648))
- Change cursor for selecting Geographical area in map ([ES-2045](https://metasolutions.atlassian.net/browse/ES-2045))
- Fix long title in Search, Suggestions list, and Comment dialog ([ES-2660](https://metasolutions.atlassian.net/browse/ES-2660))

## [3.7.7](https://bitbucket.org/metasolutions/entryscape/branches/compare/3.7.7%0D3.7.6) - 2023-03-22

### Fixed

- Views not rendered if using non default language bundles ([ES-2608](https://metasolutions.atlassian.net/browse/ES-2608))
- Italian and french added in build (without actual translation beeing in place) to allow them to appear in supported languages

## [3.7.6](https://bitbucket.org/metasolutions/entryscape/branches/compare/3.7.6%0D3.7.5) - 2023-03-02

### Fixed

- Cardinality should be visible for property but not for other forms ([ES-2582](https://metasolutions.atlassian.net/browse/ES-2582))
- Order is not respected for options in select fields ([ES-2583](https://metasolutions.atlassian.net/browse/ES-2583))

## [3.7.5](https://bitbucket.org/metasolutions/entryscape/branches/compare/3.7.5%0D3.7.4) - 2023-02-24

### Fixed

- Missing nodetype for Models text field ([ES-2572](https://metasolutions.atlassian.net/browse/ES-2572))
- Duplicated templates in Models builds ([ES-2573](https://metasolutions.atlassian.net/browse/ES-2573))
- Prevent adding form to itself ([ES-2575](https://metasolutions.atlassian.net/browse/ES-2575))

## [3.7.4](https://bitbucket.org/metasolutions/entryscape/branches/compare/3.7.4%0D3.7.3) - 2023-02-21

### Fixed

- Crash in outline during removal of deeper structures
- Missing descriptions for attributes in models added
- Remove only the candidate option when cancelling an add option action ([ES-2551](https://metasolutions.atlassian.net/browse/ES-2551))

### Added

- Support latitude and longitude properties in geoChooser ([ES-2566](https://metasolutions.atlassian.net/browse/ES-2566))

## [3.7.3](https://bitbucket.org/metasolutions/entryscape/branches/compare/3.7.3%0D3.7.2) - 2023-02-13

### Fixed

- Handle missing accessURL on distribution for alternative metadata profile
- Nodetype is not considered in models Select field ([ES-2546](https://metasolutions.atlassian.net/browse/ES-2546))
- Added extend relation during import of RDForms template

## [3.7.2](https://bitbucket.org/metasolutions/entryscape/branches/compare/3.7.2%0D3.7.1) - 2023-01-31

### Fixed

- Request Header Fields Too Long error on adding user to group ([ES-2541](https://metasolutions.atlassian.net/browse/ES-2541))

## [3.7.1](https://bitbucket.org/metasolutions/entryscape/branches/compare/3.7.1%0D3.7.0) - 2023-01-16

### Changed

- Disable table view filters on edit ([ES-2460](https://metasolutions.atlassian.net/browse/ES-2460))

### Fixed

- De-select on hierachy tree doesn't work ([ES-2428](https://metasolutions.atlassian.net/browse/ES-2428))
- Manage concepts on remote terminologies always ends in a conflict ([ES-2462](https://metasolutions.atlassian.net/browse/ES-2462))
- Updated to correct namespace for properties (rdf:Property not rdfs:Property)
- Filters default visible in dataset list in Catalog
- Hide profile filter when no profiles defined in dataset list
- Table view default visible
- Remove possibility to change project type if there are none

## [3.7.0](https://bitbucket.org/metasolutions/entryscape/branches/compare/3.7.0%0D3.6.5) - 2022-12-23

### Added

- New collection overview view ([ES-2255](https://metasolutions.atlassian.net/browse/ES-2255), [ES-2265](https://metasolutions.atlassian.net/browse/ES-2265), [ES-2153](https://metasolutions.atlassian.net/browse/ES-2153), [ES-2273](https://metasolutions.atlassian.net/browse/ES-2273))
- Concepts list in collection overview ([ES-77](https://metasolutions.atlassian.net/browse/ES-77), [ES-2283](https://metasolutions.atlassian.net/browse/ES-2283))
- Source terminologies list in collection overview ([ES-2257](https://metasolutions.atlassian.net/browse/ES-2257), [ES-2282](https://metasolutions.atlassian.net/browse/ES-2282))
- Support for concepts from different terminologies in collections ([ES-2154](https://metasolutions.atlassian.net/browse/ES-2154))
- Support for remote concepts and terminologies in collections ([ES-2417](https://metasolutions.atlassian.net/browse/ES-2417))
- Manage collection concepts dialog ([ES-517](https://metasolutions.atlassian.net/browse/ES-517), [ES-2300](https://metasolutions.atlassian.net/browse/ES-2300))
- Terminology path for concepts in collections overview ([ES-2380](https://metasolutions.atlassian.net/browse/ES-2380))
- Local Entity types list ([ES-2277](https://metasolutions.atlassian.net/browse/ES-2277))
- Migrate Entity types list ([ES-2288](https://metasolutions.atlassian.net/browse/ES-2288))
- Add and edit for custom entity type ([ES-2299](https://metasolutions.atlassian.net/browse/ES-2299), [ES-2319](https://metasolutions.atlassian.net/browse/ES-2319))
- Manage Entity type view ([ES-2275](https://metasolutions.atlassian.net/browse/ES-2275))
- Add configuration to disable migrate entity types ([ES-2359](https://metasolutions.atlassian.net/browse/ES-2359))
- Option to remove custom entity type ([ES-2360](https://metasolutions.atlassian.net/browse/ES-2360))
- Add date column and sort by date on migrated entity types ([ES-2361](https://metasolutions.atlassian.net/browse/ES-2361))
- Diagram view of a forms project ([ES-2325](https://metasolutions.atlassian.net/browse/ES-2325))
- Build list and build create dialog ([ES-1767](https://metasolutions.atlassian.net/browse/ES-1767))
- Options in preview for select fields ([ES-2164](https://metasolutions.atlassian.net/browse/ES-2164))
- Handle RDFs classes and properties in Models ([ES-2338](https://metasolutions.atlassian.net/browse/ES-2338))
- Import more template values ([ES-2294](https://metasolutions.atlassian.net/browse/ES-2294))
- Upgrade to entry functionality in forms ([ES-2047](https://metasolutions.atlassian.net/browse/ES-2047))
- Add public, external and profile filters on datasets ([ES-502](https://metasolutions.atlassian.net/browse/ES-502), [ES-2328](https://metasolutions.atlassian.net/browse/ES-2328), [ES-2347](https://metasolutions.atlassian.net/browse/ES-2347), [ES-2349](https://metasolutions.atlassian.net/browse/ES-2349), [ES-2350](https://metasolutions.atlassian.net/browse/ES-2350), [ES-2306](https://metasolutions.atlassian.net/browse/ES-2306), [ES-2427](https://metasolutions.atlassian.net/browse/ES-2427))
- Add option to choose title field for csv map visualization ([ES-2183](https://metasolutions.atlassian.net/browse/ES-2183))
- Option to include map legend for wms map visualization ([ES-2292](https://metasolutions.atlassian.net/browse/ES-2292))
- Add Legend for WMS ([ES-2159](https://metasolutions.atlassian.net/browse/ES-2159))
- Change project type on catalog ([ES-2118](https://metasolutions.atlassian.net/browse/ES-2118))
- Highlight fields with errors on the outline ([ES-2097](https://metasolutions.atlassian.net/browse/ES-2097))
- Offer a way to edit entries using a different profile on TableView ([ES-1944](https://metasolutions.atlassian.net/browse/ES-1944))
- Show selected project type ([ES-2120](https://metasolutions.atlassian.net/browse/ES-2120))
- Show default when no project type is chosen in project type chooser ([ES-2332](https://metasolutions.atlassian.net/browse/ES-2332))
- Allow to configure project type drowndown in creation dialog ([ES-2327](https://metasolutions.atlassian.net/browse/ES-2327))

### Changed

- Remove collection actions from list items (now on overview) ([ES-2336](https://metasolutions.atlassian.net/browse/ES-2336))
- Collection edit action in overview ([ES-2261](https://metasolutions.atlassian.net/browse/ES-2261))
- Collection remove action in overview ([ES-2262](https://metasolutions.atlassian.net/browse/ES-2262))
- Collection download action in overview ([ES-2263](https://metasolutions.atlassian.net/browse/ES-2263))
- Collection revisions action in overview ([ES-2264](https://metasolutions.atlassian.net/browse/ES-2264))
- Hierarchy sidebar header background color ([ES-2412](https://metasolutions.atlassian.net/browse/ES-2412))
- Improve error message for web address when importing a terminology ([ES-2244](https://metasolutions.atlassian.net/browse/ES-2244))
- Change name of the dataset "PSI" flag to "External" ([ES-2316](https://metasolutions.atlassian.net/browse/ES-2316))
- Address tech debt and add placeholders on dataset overview lists ([ES-741](https://metasolutions.atlassian.net/browse/ES-741), [ES-2297](https://metasolutions.atlassian.net/browse/ES-2297), [ES-2298](https://metasolutions.atlassian.net/browse/ES-2298))
- Disable catalog statistics controls like download when no data ([ES-2222](https://metasolutions.atlassian.net/browse/ES-2222))
- Change geoname links to https ([ES-2357](https://metasolutions.atlassian.net/browse/ES-2357))
- Remove entry labels from snackbars ([ES-2253](https://metasolutions.atlassian.net/browse/ES-2253))
- Change list items to be clickable (instead of just the title) ([ES-2314](https://metasolutions.atlassian.net/browse/ES-2314))
- Improve metadata conflict message ([ES-2284](https://metasolutions.atlassian.net/browse/ES-2284))
- Version on user menu shouldn't be clickable ([ES-2191](https://metasolutions.atlassian.net/browse/ES-2191))
- Replace Leaflet with OpenLayers for csv map visualization ([ES-2174](https://metasolutions.atlassian.net/browse/ES-2174))
- Replace Leaflet with OpenLayers for bounding box map ([ES-2175](https://metasolutions.atlassian.net/browse/ES-2175))
- Improve disableRipple on ListItem ([ES-2366](https://metasolutions.atlassian.net/browse/ES-2366))

### Fixed

- Built-in "Users" group should not be shown in group lists ([ES-2320](https://metasolutions.atlassian.net/browse/ES-2320))
- Add user to group action button overflows in German ([ES-2337](https://metasolutions.atlassian.net/browse/ES-2337))
- Terms hierarchy and other views break with long concept labels ([ES-2250](https://metasolutions.atlassian.net/browse/ES-2250))
- Edit concept dialog doesn't validate inputs ([ES-2367](https://metasolutions.atlassian.net/browse/ES-2367))
- Show placeholder on empty source terminology manage concepts dialog ([ES-76](https://metasolutions.atlassian.net/browse/ES-76))
- Collection count not updating after removing included concepts ([ES-2101](https://metasolutions.atlassian.net/browse/ES-2101))
- Avoid flicker when loading concept hierarchy tree ([ES-2267](https://metasolutions.atlassian.net/browse/ES-2267))
- Hierarchy tree conflict leaves the tree in erroneous state ([ES-2339](https://metasolutions.atlassian.net/browse/ES-2339))
- Create terminology always has URI field, independent of project type ([ES-2394](https://metasolutions.atlassian.net/browse/ES-2394), [ES-2443](https://metasolutions.atlassian.net/browse/ES-2443))
- Harvesting notifications dropdown expanding in german ([ES-2439](https://metasolutions.atlassian.net/browse/ES-2439))
- Missing mandatory value error message in validation report ([ES-2396](https://metasolutions.atlassian.net/browse/ES-2396))
- Harvesting notifications dialog scrollbar placement ([ES-2397](https://metasolutions.atlassian.net/browse/ES-2397))
- Workbench entity types list view expands when search length message appears ([ES-2075](https://metasolutions.atlassian.net/browse/ES-2075))
- Workbench entity types list modified header not aligned ([ES-2402](https://metasolutions.atlassian.net/browse/ES-2402))
- Rename forms to models ([ES-2331](https://metasolutions.atlassian.net/browse/ES-2331))
- Inconsistent state in add form items dialog ([ES-2232](https://metasolutions.atlassian.net/browse/ES-2232))
- No margin between extend and create buttons in forms placeholder ([ES-2243](https://metasolutions.atlassian.net/browse/ES-2243))
- Minor fixes for forms and fields ([ES-2379](https://metasolutions.atlassian.net/browse/ES-2379))
- Consistent margins on visualization field labels ([ES-2365](https://metasolutions.atlassian.net/browse/ES-2365))
- Create suggestion from template loop ([ES-2434](https://metasolutions.atlassian.net/browse/ES-2434))
- Fix suggestion checklist not saving ([ES-2312](https://metasolutions.atlassian.net/browse/ES-2312))
- Missing accessibility labels on edit and create visualization dialogs ([ES-2368](https://metasolutions.atlassian.net/browse/ES-2368))
- Distribution file upload conflict ([ES-2353](https://metasolutions.atlassian.net/browse/ES-2353))
- Sort by modified does not work for datasets ([ES-2177](https://metasolutions.atlassian.net/browse/ES-2177))
- Move layers button inside the map ([ES-2034](https://metasolutions.atlassian.net/browse/ES-2034))
- Bounding box crossing dateline gets mirrored ([ES-2281](https://metasolutions.atlassian.net/browse/ES-2281))
- Empty string possible to save in table view ([ES-2190](https://metasolutions.atlassian.net/browse/ES-2190))
- Browser form autofill breaks the password change dialog ([ES-2291](https://metasolutions.atlassian.net/browse/ES-2291))
- Horizontal scrolling on minimum supported resolution \(or while zoomed in\) ([ES-1585](https://metasolutions.atlassian.net/browse/ES-1585))
- Chrome saved credentials fall on top of field labels on login ([ES-2386](https://metasolutions.atlassian.net/browse/ES-2386))
- Long titles break some list items ([ES-2249](https://metasolutions.atlassian.net/browse/ES-2249))
- Primary navigation state doesn't persist ([ES-2182](https://metasolutions.atlassian.net/browse/ES-2182))
- List column headers don't align on larger screens ([ES-2414](https://metasolutions.atlassian.net/browse/ES-2414))
- Prevent EntryChooser from breaking if no entity type is found ([ES-2274](https://metasolutions.atlassian.net/browse/ES-2274))
- Improvements to list pagination ([ES-2448](https://metasolutions.atlassian.net/browse/ES-2448), [ES-2458](https://metasolutions.atlassian.net/browse/ES-2458))
- Prevent OpenLayers from being bundled twice ([ES-2252](https://metasolutions.atlassian.net/browse/ES-2252))

### Security

- Upgrade and audit dependencies ([ES-2381](https://metasolutions.atlassian.net/browse/ES-2381))

## [3.6.5](https://bitbucket.org/metasolutions/entryscape/branches/compare/3.6.5%0D3.6.4) - 2022-11-30

### Fixed

- Remove dataset template rdf type on create dataset from template ([ES-2363](https://metasolutions.atlassian.net/browse/ES-2363))

## [3.6.4](https://bitbucket.org/metasolutions/entryscape/branches/compare/3.6.4%0D3.6.3) - 2022-11-10

### Fixed

- Some overridden entity type values are reset to default ([ES-2318](https://metasolutions.atlassian.net/browse/ES-2318))
- Visualization and API generators do not recognize file type vocabulary ([ES-2317](https://metasolutions.atlassian.net/browse/ES-2317))

## [3.6.3](https://bitbucket.org/metasolutions/entryscape/branches/compare/3.6.3%0D3.6.2) - 2022-11-04

### Fixed

- Use lookup instead of form template id for catalog ([ES-2315](https://metasolutions.atlassian.net/browse/ES-2315))

## [3.6.2](https://bitbucket.org/metasolutions/entryscape/branches/compare/3.6.2%0D3.6.1) - 2022-10-28

### Fixed

- Selecting geoname with no children throws error ([ES-2302](https://metasolutions.atlassian.net/browse/ES-2302))

## [3.6.1](https://bitbucket.org/metasolutions/entryscape/branches/compare/3.6.1%0D3.6.0) - 2022-09-29

### Changed

- Default support and maintenance emails

### Fixed

- Visualization creation dialog breaking if initially editing the title ([ES-2272](https://metasolutions.atlassian.net/browse/ES-2272))
- Order of WMS layers ([ES-2269](https://metasolutions.atlassian.net/browse/ES-2269))
- Link styles on rdforms editor and presenter ([ES-2270](https://metasolutions.atlassian.net/browse/ES-2270))
- Outline being aware of filter predicates ([ES-2271](https://metasolutions.atlassian.net/browse/ES-2271))
- Bounding box not visible for pre dcat2 ([ES-2279](https://metasolutions.atlassian.net/browse/ES-2279))

## [3.6.0](https://bitbucket.org/metasolutions/entryscape/branches/compare/3.6.0%0D3.5.1) - 2022-09-20

### Added

- Link to the dataset template's landing page on the list item ([ES-2168](https://metasolutions.atlassian.net/browse/ES-2168))
- Way to override the local configuration using a URL parameter ([ES-2167](https://metasolutions.atlassian.net/browse/ES-2167))
- Datatypes in Forms field preview ([ES-2165](https://metasolutions.atlassian.net/browse/ES-2165))
- Property fields in Forms now include a namespace chooser ([ES-2152](https://metasolutions.atlassian.net/browse/ES-2152))
- Constraints for Forms fields ([ES-2141](https://metasolutions.atlassian.net/browse/ES-2141))
- Attributes for Forms fields ([ES-2140](https://metasolutions.atlassian.net/browse/ES-2140))
- Field options for Form fields ([ES-2139](https://metasolutions.atlassian.net/browse/ES-2139))
- Allow selection of matched concepts from other instance ([ES-2134](https://metasolutions.atlassian.net/browse/ES-2134))
- Import of enhance-only terminologies ([ES-2125](https://metasolutions.atlassian.net/browse/ES-2125))
- Support for terminologies to read labels from external sources ([ES-2129](https://metasolutions.atlassian.net/browse/ES-2129))
- Update from source action on enhance-only terminologies ([ES-2128](https://metasolutions.atlassian.net/browse/ES-2128))
- Editing of enhance-only terminologis ([ES-2127](https://metasolutions.atlassian.net/browse/ES-2127))
- Restrictions for enhance-only terminologies ([ES-2126](https://metasolutions.atlassian.net/browse/ES-2126))
- Missing layers warning next to button on visualization creation ([ES-2206](https://metasolutions.atlassian.net/browse/ES-2206))
- Configurable maintenance view ([ES-620](https://metasolutions.atlassian.net/browse/ES-620))
- Service desk link on the user menu ([ES-29](https://metasolutions.atlassian.net/browse/ES-29))
- Preview of Forms fields ([ES-2021](https://metasolutions.atlassian.net/browse/ES-2021))
- Edit options on Forms fields ([ES-2020](https://metasolutions.atlassian.net/browse/ES-2020))
- Jest and initial unit tests ([ES-2233](https://metasolutions.atlassian.net/browse/ES-2233))

### Changed

- Now using the `editlabel` in the outline if available ([ES-2160](https://metasolutions.atlassian.net/browse/ES-2160))
- Improved override values in Forms edit dialogs ([ES-2148](https://metasolutions.atlassian.net/browse/ES-2148))
- Selected list items to be consistently marked everywhere ([ES-2147](https://metasolutions.atlassian.net/browse/ES-2147))
- The way Forms' form and field overviews are arranged ([ES-2144](https://metasolutions.atlassian.net/browse/ES-2144))
- Optional cardinality now sets minimum to 0 in Forms ([ES-2143](https://metasolutions.atlassian.net/browse/ES-2143))
- Form and field publish controls to be in the overview ([ES-2142](https://metasolutions.atlassian.net/browse/ES-2142))
- Some Forms text fields to be multiline ([ES-2138](https://metasolutions.atlassian.net/browse/ES-2138))
- Forms import to be in the project list ([ES-2133](https://metasolutions.atlassian.net/browse/ES-2133))
- Forms fields presenters to include extensions and all currently editable fields ([ES-2130](https://metasolutions.atlassian.net/browse/ES-2130))
- Margins in rdforms have been reduced ([ES-2124](https://metasolutions.atlassian.net/browse/ES-2124))
- Profile chooser is now in a separate dialog ([ES-2123](https://metasolutions.atlassian.net/browse/ES-2123))
- Type of user dropdown appearance ([ES-2116](https://metasolutions.atlassian.net/browse/ES-2116))
- Feedback when switching project type in configure entity types dialog ([ES-2103](https://metasolutions.atlassian.net/browse/ES-2103))
- Some visual details on the havesting notifications ([ES-2200](https://metasolutions.atlassian.net/browse/ES-2200))
- Forms show extended info in edit dialog for form items ([ES-2057](https://metasolutions.atlassian.net/browse/ES-2057))
- Improved bounding box validation on datasets ([ES-2053](https://metasolutions.atlassian.net/browse/ES-2053))
- Handle an HTTP URL in WMS visualizations better ([ES-2059](https://metasolutions.atlassian.net/browse/ES-2059))
- Inline scripts are now handled by webpack ([ES-2058](https://metasolutions.atlassian.net/browse/ES-2058))
- Proper state messages on Forms import ([ES-2004](https://metasolutions.atlassian.net/browse/ES-2004))
- Group members dialog now falls back to the username in case of missing metadata ([ES-1922](https://metasolutions.atlassian.net/browse/ES-1922))
- Show project name/alias in addition to the title when available, also make it searchable ([ES-1847](https://metasolutions.atlassian.net/browse/ES-1847))
- Choosing what format to preview via tabs in Forms preview ([ES-2056](https://metasolutions.atlassian.net/browse/ES-2056))

### Removed

- W3CDTF datatype from datatype selector in Forms ([ES-2137](https://metasolutions.atlassian.net/browse/ES-2137))

### Fixed

- Statistics list view show subtitles that are not in line with other list item subtitles ([ES-2215](https://metasolutions.atlassian.net/browse/ES-2215))
- Using faulty WMS url distribution for dataset vizualisation results in loadViaProxy error ([ES-2211](https://metasolutions.atlassian.net/browse/ES-2211))
- Merge doesn't work for guests in Registry Toolkit ([ES-2197](https://metasolutions.atlassian.net/browse/ES-2197))
- Dataset title issue when creating dataset from template suggestion ([ES-2192](https://metasolutions.atlassian.net/browse/ES-2192))
- Revision content not visible in showcases and ideas ([ES-2240](https://metasolutions.atlassian.net/browse/ES-2240))
- WMS parameter values are now always in uppercase ([ES-2162](https://metasolutions.atlassian.net/browse/ES-2162))
- Rdforms field dependencies not working on the outline ([ES-2156](https://metasolutions.atlassian.net/browse/ES-2156))
- Forms and fields to not be public by default ([ES-2146](https://metasolutions.atlassian.net/browse/ES-2146))
- The revisions dialog breaking after removing a relevant user ([ES-2229](https://metasolutions.atlassian.net/browse/ES-2229))
- Table view pagination changing position on error ([ES-2225](https://metasolutions.atlassian.net/browse/ES-2225))
- Adding a Technique on the Art Piece creation dialog ([ES-2223](https://metasolutions.atlassian.net/browse/ES-2223))
- Copied datasets only copying a single title ([ES-2221](https://metasolutions.atlassian.net/browse/ES-2221))
- Switching encoding on visualizations clearing fields ([ES-2218](https://metasolutions.atlassian.net/browse/ES-2218))
- Translation error on API activation ([ES-2204](https://metasolutions.atlassian.net/browse/ES-2204))
- The save button being enabled before making any changes on the visualizations edit dialog ([ES-2198](https://metasolutions.atlassian.net/browse/ES-2198))
- The status statistics link appearing when a user has no permission to view ([ES-2193](https://metasolutions.atlassian.net/browse/ES-2193))
- The success URL on signup ([ES-2187](https://metasolutions.atlassian.net/browse/ES-2187))
- Inability to close the forms preview dialog ([ES-2170](https://metasolutions.atlassian.net/browse/ES-2170))
- WMS extent not resetting if choosing a failing WMS source ([ES-2083](https://metasolutions.atlassian.net/browse/ES-2083))
- OTS parsing error when decoding fonts in Terms ([ES-2070](https://metasolutions.atlassian.net/browse/ES-2070))
- Requiring numeric context IDs when two or more route params ([ES-2049](https://metasolutions.atlassian.net/browse/ES-2049))
- Registry organization pagination for non-admin users ([ES-2680](https://metasolutions.atlassian.net/browse/ES-2680))

### Security

- Upgraded problematic dependencies after auditing for security issues ([ES-2163](https://metasolutions.atlassian.net/browse/ES-2163))

## [3.5.1](https://bitbucket.org/metasolutions/entryscape/branches/compare/3.5.1%0D3.5.0) - 2022-06-08

### Fixed

- Duplicate child bindings appearing on the rdforms outline ([ES-2117](https://metasolutions.atlassian.net/browse/ES-2117))

## [3.5.0](https://bitbucket.org/metasolutions/entryscape/branches/compare/3.5.0%0D3.4.8) - 2022-06-07

### Added

- Project types ([ES-2048](https://metasolutions.atlassian.net/browse/ES-2048))
- Support for more WMS projections by transforming coordinates from one projection to another ([ES-2035](https://metasolutions.atlassian.net/browse/ES-2035))
- Forms field preview ([ES-2054](https://metasolutions.atlassian.net/browse/ES-2054))
- Rdforms outline ([ES-1949](https://metasolutions.atlassian.net/browse/ES-1949))
- Fields to forms field edit dialog ([ES-1961](https://metasolutions.atlassian.net/browse/ES-1961))
- Easy access to the secondary navigation via keyboard ([ES-1988](https://metasolutions.atlassian.net/browse/ES-1988))
- Edit funcionality in forms ([ES-2018](https://metasolutions.atlassian.net/browse/ES-2018))
- Delete functionality for forms and form items ([ES-2025](https://metasolutions.atlassian.net/browse/ES-2025))
- Direction option to ListItemText ([ES-2033](https://metasolutions.atlassian.net/browse/ES-2033))
- Clear link to the resource URI on registry validation reports ([ES-2005](https://metasolutions.atlassian.net/browse/ES-2005))
- Drag and drop functionality on forms overview ([ES-1931](https://metasolutions.atlassian.net/browse/ES-1931))
- Service and version params to WMS URLs ([ES-2017](https://metasolutions.atlassian.net/browse/ES-2017))
- Refresh option on EntryProvider ([ES-2038](https://metasolutions.atlassian.net/browse/ES-2038))

### Changed

- Registry organization metadata fulfillment statistics ([ES-2011](https://metasolutions.atlassian.net/browse/ES-2011))
- Small accessibility improvements on table view ([ES-1968](https://metasolutions.atlassian.net/browse/ES-1968))
- Suggestion metadata now carry over to datasets created from them ([ES-2065](https://metasolutions.atlassian.net/browse/ES-2065))
- Disabled the actions column on the table view columns menu ([ES-1806](https://metasolutions.atlassian.net/browse/ES-1806))
- Set the current view on all views (including simple) ([ES-2051](https://metasolutions.atlassian.net/browse/ES-2051))
- Can now provide a port to the dev server
- Handle the rdforms language on a generic level instead of on each component ([ES-2006](https://metasolutions.atlassian.net/browse/ES-2006))
- Separate the logic for handling states of different distribution source types ([ES-1962](https://metasolutions.atlassian.net/browse/ES-1962))
- The label on the dataset overview switches to be clickable ([ES-1963](https://metasolutions.atlassian.net/browse/ES-1963))
- Accessibility improvements on the WMS layer switcher ([ES-1955](https://metasolutions.atlassian.net/browse/ES-1955))
- The way information is presented on the SKOS entry chooser dialog ([ES-2009](https://metasolutions.atlassian.net/browse/ES-2009))
- Rdforms editor in forms with custom editor components ([ES-2023](https://metasolutions.atlassian.net/browse/ES-2023))
- Consistent icon for comments ([ES-2093](https://metasolutions.atlassian.net/browse/ES-2093))

### Removed

- Redundant dependencies like selectize

### Fixed

- Loading a toolkit catalog via link after one via file ([ES-2071](https://metasolutions.atlassian.net/browse/ES-2071))
- Issues that came up after the latest CSP adjustments ([ES-2063](https://metasolutions.atlassian.net/browse/ES-2063))
- Clear the graph on leaving toolkit ([ES-2013](https://metasolutions.atlassian.net/browse/ES-2013))
- Editing the access URL of a WMS distribution now displays a warning on visualizations edit and previews ([ES-2014](https://metasolutions.atlassian.net/browse/ES-2014))
- Rdforms dropdowns not respecting the z-index ([ES-2007](https://metasolutions.atlassian.net/browse/ES-2007))
- Table view selection model warning ([ES-1925](https://metasolutions.atlassian.net/browse/ES-1925))
- Skipped heading level on visualization preview ([ES-2002](https://metasolutions.atlassian.net/browse/ES-2002))
- Persistence of the registry haverst button tooltip ([ES-1971](https://metasolutions.atlassian.net/browse/ES-1971))
- Description not getting updated on the dataset overview after editing the metadata ([ES-2039](https://metasolutions.atlassian.net/browse/ES-2039))
- Date-picker popup being inaccessible when close to the edge of the screen ([ES-1941](https://metasolutions.atlassian.net/browse/ES-1941))
- Multiple small issues on the registry organization details dialog ([ES-1996](https://metasolutions.atlassian.net/browse/ES-1996))
- Cursor jumping to end of form when editing RDF/XML in toolkit ([ES-2078](https://metasolutions.atlassian.net/browse/ES-2078))
- Configuration for privacy and terms and conditions dialog ([ES-2073](https://metasolutions.atlassian.net/browse/ES-2073))

### Security

- Upgraded problematic dependencies after auditing for security issues
- Made the build result CSP compatible ([ES-1486](https://metasolutions.atlassian.net/browse/ES-1486))
- Improved client side error handling in toolkit ([ES-2091](https://metasolutions.atlassian.net/browse/ES-2091))
- Improved client side error handling in terms and forms ([ES-2106](https://metasolutions.atlassian.net/browse/ES-2106))
- Restricted newly created users from granting guest read access to user metadata ([ES-2092](https://metasolutions.atlassian.net/browse/ES-2092))

Read more about all fixed issues in [version 3.5.0](https://metasolutions.atlassian.net/jira/software/c/projects/ES/issues/?jql=project%20%3D%20%22ES%22%20AND%20%28status%20%3D%20Resolved%20OR%20status%20%3D%20Closed%29%20AND%20fixVersion%3D3.5%20ORDER%20BY%20updatedDate%20DESC)

## [3.4.8](https://bitbucket.org/metasolutions/entryscape/branches/compare/3.4.8%0D3.4.7) - 2022-04-26

### Fixed

- Allow entering decimals for bounding box
- Bounding box field values can be cleared
- Remove bounding box edit fields from dataset preview

## [3.4.7](https://bitbucket.org/metasolutions/entryscape/branches/compare/3.4.7%0D3.4.6) - 2022-04-19

### Fixed

- Bugfix for 3.4.6

## [3.4.6](https://bitbucket.org/metasolutions/entryscape/branches/compare/3.4.6%0D3.4.5) - 2022-04-14

### Fixed

- Support for non-numeric workbench projects identifiers added.

## [3.4.5](https://bitbucket.org/metasolutions/entryscape/branches/compare/3.4.5%0D3.4.4) - 2022-03-18

### Fixed

- Made sure rdf type dcat:Dataset is added for dataset template

## [3.4.4](https://bitbucket.org/metasolutions/entryscape/branches/compare/3.4.4%0D3.4.3) - 2022-03-11

### Fixed

- Made template catalog into it's own type and made it configurable
- Changed default type of datasetTemplate to be in Entryscape namespace

## [3.4.3](https://bitbucket.org/metasolutions/entryscape/branches/compare/3.4.3%0D3.4.2) - 2022-03-04

### Fixed

- Made geographical format recommended and the last option, other formats, optional
- Support restrictions to specific terminologies in SkosChooser
- Changed so the main entitytype is always at the top in workbench

## [3.4.2](https://bitbucket.org/metasolutions/entryscape/branches/compare/3.4.2%0D3.4.1) - 2022-03-03

### Fixed

- Addressed a bug that made it impossible to remove distributions if they did not have associated visualizations.

## [3.4.1](https://bitbucket.org/metasolutions/entryscape/branches/compare/3.4.1%0D3.4.0) - 2022-03-03

### Fixed

- Added a flag to enable or disable support for creating visualizations of WMS services.

## [3.4.0](https://bitbucket.org/metasolutions/entryscape/branches/compare/3.4.0%0D3.3.3) - 2022-03-02

### Added

- Search by username or title at the admin users list ([ES-1784](https://metasolutions.atlassian.net/browse/ES-1784))
- Entry label on link check details ([ES-1938](https://metasolutions.atlassian.net/browse/ES-1938))
- Related entries section on link check details ([ES-1939](https://metasolutions.atlassian.net/browse/ES-1939))
- Table type visualizations ([ES-1901](https://metasolutions.atlassian.net/browse/ES-1901))
- Option to keep visualizations intact after replacing their source file ([ES-1917](https://metasolutions.atlassian.net/browse/ES-1917))
- Privacy policy link on signup and terms and conditions link on user menu ([ES-1916](https://metasolutions.atlassian.net/browse/ES-1916))
- Table view rows per page select ([ES-1952](https://metasolutions.atlassian.net/browse/ES-1952))
- Table view row validation after editing ([ES-1912](https://metasolutions.atlassian.net/browse/ES-1912))
- Table view row edit dialog action ([ES-1805](https://metasolutions.atlassian.net/browse/ES-1805))
- Small delay on table view click to open popover ([ES-1919](https://metasolutions.atlassian.net/browse/ES-1919))
- Rdforms field controls e.g. "clear" on table view cell editor ([ES-1841](https://metasolutions.atlassian.net/browse/ES-1841))
- New, composable list component ([ES-1905](https://metasolutions.atlassian.net/browse/ES-1905))
- Premium users ([ES-1775](https://metasolutions.atlassian.net/browse/ES-1775))
- Premium projects ([ES-1492](https://metasolutions.atlassian.net/browse/ES-1492))
- Configurable restriction for workbench projects ([ES-1889](https://metasolutions.atlassian.net/browse/ES-1899))
- Form template overview ([ES-1766](https://metasolutions.atlassian.net/browse/ES-1766))
- Form field edit dialog ([ES-1769](https://metasolutions.atlassian.net/browse/ES-1769))
- Documentation link to the swagger UI on API creation ([ES-887](https://metasolutions.atlassian.net/browse/ES-887))
- Support for visualization of WMS ([ES-1902](https://metasolutions.atlassian.net/browse/ES-1902))

### Changed

- Search by username on add users to groups dialog now matches entries that don't necessarily start with the search string ([ES-1784](https://metasolutions.atlassian.net/browse/ES-1784))
- All admin views are now visible to non-superadmins by default ([ES-1946](https://metasolutions.atlassian.net/browse/ES-1946))
- Default type used for dataset template entries ([ES-1933](https://metasolutions.atlassian.net/browse/ES-1933))
- List component on form overview ([ES-1930](https://metasolutions.atlassian.net/browse/ES-1930))
- List dispatch context now also handles sorting state ([ES-1669](https://metasolutions.atlassian.net/browse/ES-1669))
- Extra scripts are now used to load geodetect function ([ES-1506](https://metasolutions.atlassian.net/browse/ES-1506))
- Copying a dataset now also copies over the dataset profile ([ES-748](https://metasolutions.atlassian.net/browse/ES-748))
- Distribution title now recommended ([ES-1997](https://metasolutions.atlassian.net/browse/ES-1997))
- SKOS chooser dialog now has fixed height
- Search field and adornment colors throughout the application ([ES-2008](https://metasolutions.atlassian.net/browse/ES-2008))
- Visualization dialogs to a fixed height and less padding ([ES-2012](https://metasolutions.atlassian.net/browse/ES-2012))
- Tablew view search now resets the pagination ([ES-2010](https://metasolutions.atlassian.net/browse/ES-2010))

### Removed

- Option to remove an entity type entry while it has relations ([ES-1664](https://metasolutions.atlassian.net/browse/ES-1664))

### Fixed

- Misaligned theme format text in embed dialog in Safari ([ES-1979](https://metasolutions.atlassian.net/browse/ES-1979))
- Wrong placeholder shown when no search matches ([ES-1994](https://metasolutions.atlassian.net/browse/ES-1994))
- Statistics chart clipped on print ([ES-1985](https://metasolutions.atlassian.net/browse/ES-1985))
- Date as size in bytes for distribution ([ES-1989](https://metasolutions.atlassian.net/browse/ES-1989))
- Edit project uri visible in workbench projects without uri pattern enabled ([ES-1981](https://metasolutions.atlassian.net/browse/ES-1981))
- Registry validation warnings not translated ([ES-2000](https://metasolutions.atlassian.net/browse/ES-2000))
- Visualization type button labels overflowing ([ES-1948](https://metasolutions.atlassian.net/browse/ES-1948))
- Inconsistency on bar chart visualizations depending on operation and axes selection ([ES-1915](https://metasolutions.atlassian.net/browse/ES-1915))
- Telephone number inputs allowing whitespace characters ([ES-1924](https://metasolutions.atlassian.net/browse/ES-1924))
- Table view cells not covering the whole cell area ([ES-1920](https://metasolutions.atlassian.net/browse/ES-1920))
- Unexpected behaviour when clicking on a table view cell after opening the actions menu ([ES-1947](https://metasolutions.atlassian.net/browse/ES-1947))
- List sort order indicator direction ([ES-1914](https://metasolutions.atlassian.net/browse/ES-1914))
- Overriding the default set of supported languages not working ([ES-1909](https://metasolutions.atlassian.net/browse/ES-1909))
- Pagination in distribution manage files dialog list not working ([ES-1904](https://metasolutions.atlassian.net/browse/ES-1904))
- Updating the terminology URL creating revisions for each concept ([ES-1850](https://metasolutions.atlassian.net/browse/ES-1850))
- Prevent reverting concept relations that would break terminology ([ES-1897](https://metasolutions.atlassian.net/browse/ES-1897))
- Publishing a workbench project automatically switching publishable entries to public ([ES-1741](https://metasolutions.atlassian.net/browse/ES-1741))
- Handling failing bundle loader breaking the app ([ES-615](https://metasolutions.atlassian.net/browse/ES-615))
- Primary and secondary navigation links not being active in some cases ([ES-1649](https://metasolutions.atlassian.net/browse/ES-1649))
- Can now change to arbitrary appName configuration without breaking the application

### Security

- Upgraded problematic dependencies after auditing for security issues ([ES-1936](https://metasolutions.atlassian.net/browse/ES-1936))

Read more about all fixed issues in [version 3.4.0](https://metasolutions.atlassian.net/jira/software/c/projects/ES/issues/?jql=project%20%3D%20%22ES%22%20AND%20%28status%20%3D%20Resolved%20OR%20status%20%3D%20Closed%29%20AND%20fixVersion%3D3.4%20ORDER%20BY%20updatedDate%20DESC)

## [3.3.3](https://bitbucket.org/metasolutions/entryscape/branches/compare/3.3.3%0D3.3.2) - 2021-02-18

### Fixed

- Issue where distribution metadata couldn't be edited if it had more than one file

## [3.3.2](https://bitbucket.org/metasolutions/entryscape/branches/compare/3.3.2%0D3.3.1) - 2021-12-17

### Fixed

- Issue occurring after upgrading webpack ([8543b6e](https://bitbucket.org/metasolutions/entryscape/commits/8543b6e))

## [3.3.1](https://bitbucket.org/metasolutions/entryscape/branches/compare/3.3.1%0D3.3.0) - 2021-12-16

### Fixed

- Not supported browser banner on FireFox ([20f6113](https://bitbucket.org/metasolutions/entryscape/commits/20f6113))
- Prevent multiple requests for banner ([0eddb88](https://bitbucket.org/metasolutions/entryscape/commits/0eddb88))

## [3.3.0](https://bitbucket.org/metasolutions/entryscape/branches/compare/3.3.0%0D3.2.10) - 2021-12-03

### Added

- DCAT-AP2 support (first version in branch feature/dcat2-ES1014)
- Data services support (first version in branch feature/dcat2-ES1014)
- Tableview row actions
- Tableview show confirm/discard changes dialog when navigating to another view
- Ability to distinguish between documents and distribution files in Catalog statistics view
- EntryScape Forms foundation (not visible)
- Configurable restriction for catalog/dataset/terminology creation on all non-admin users
- Document list items now have visible publication status
- Incoming relations from other entries now appear on entry information dialogs
- Restrict parts of the Admin module UI to super-admins
- Configurable dataset restriction
- Search by username when adding user to group

### Changed

- Webpack to latest stable version
- MUI to latest stable version
- Tableview UI/UX (full-width columns, persistent edit popover, confirmation dialog on leaving with unsaved changes)
- Terms hierarchy UI (sticky hierarchy, new concepts appear on top)
- Performance improvement on dataset overview statistics
- Content security policy made stricter
- Public status icons to ones better representing it
- Presentation of archived suggestions
- Redundant RDForms levels now appear disabled
- Exclude admin users from restrictions
- Bumped entrystore.js version
- Bumped rdforms version

### Fixed

- Unnecessary rendering of show validation results button on harvesting job without datasets
- Reset search result if search text is shorter than required number of characters
- Browser support banner visibility and appearance on non-desktop Chrome
- Comment count not updating on datasets/suggestions after addition
- External link icon missing
- Activating api sometimes showing no columns

Read more about all fixed issues in [version 3.3.0](https://metasolutions.atlassian.net/jira/software/c/projects/ES/issues/?jql=project%20%3D%20%22ES%22%20AND%20%28status%20%3D%20Resolved%20OR%20status%20%3D%20Closed%29%20AND%20fixVersion%3D3.3%20ORDER%20BY%20updatedDate%20DESC)

## [3.2.10](https://bitbucket.org/metasolutions/entryscape/branches/compare/3.2.10%0D3.2.9) - 2021-11-08

### Fixed

- Resource name not always available for all entries ([0c61f99](https://bitbucket.org/metasolutions/entryscape/commits/0c61f99))

## [3.2.9](https://bitbucket.org/metasolutions/entryscape/branches/compare/3.2.9%0D3.2.8) - 2021-11-02

### Fixed

- Domain name for static deployment ([55f3e5b](https://bitbucket.org/metasolutions/entryscape/commits/55f3e5b))
- Stale file resource URIs ([7c2d5a2](https://bitbucket.org/metasolutions/entryscape/commits/7c2d5a2))
- Refreshing a distribution before adding a file ([353954f](https://bitbucket.org/metasolutions/entryscape/commits/353954f))

### Changed

- Max cardinality for dcat access URL ([90e37f5](https://bitbucket.org/metasolutions/entryscape/commits/90e37f5))

## [3.2.8](https://bitbucket.org/metasolutions/entryscape/branches/compare/3.2.8%0D3.2.7) - 2021-10-27

### Fixed

- Path for loading user agent parser from static ([6a1da7a](https://bitbucket.org/metasolutions/entryscape/commits/6a1da7a))

## [3.2.7](https://bitbucket.org/metasolutions/entryscape/branches/compare/3.2.7%0D3.2.6) - 2021-10-27

### Fixed

- Loading user agent parser from static
- Restrictions popping up for non-admins even with no configuration ([f938a60](https://bitbucket.org/metasolutions/entryscape/commits/f938a60))

### Changed

- Hiding validation results button on harvesting failure ([5c15daf](https://bitbucket.org/metasolutions/entryscape/commits/5c15daf))

## [3.2.6](https://bitbucket.org/metasolutions/entryscape/branches/compare/3.2.6%0D3.2.5) - 2021-10-25

### Fixed

- Wrong api warning ([ce4c761](https://bitbucket.org/metasolutions/entryscape/commits/ce4c761))
- Concept creation dialog flickering when restricted ([7f3d821](https://bitbucket.org/metasolutions/entryscape/commits/7f3d821))
- Incorrect configuration variable used for catalog restriction
- Loading user agent parser from assets ([5d5d5bc](https://bitbucket.org/metasolutions/entryscape/commits/5d5d5bc))

## [3.2.5](https://bitbucket.org/metasolutions/entryscape/branches/compare/3.2.5%0D3.2.4) - 2021-10-20

### Fixed

- Card width on start page ([bb315db](https://bitbucket.org/metasolutions/entryscape/commits/bb315db))
- Missing header on edit concept dialog ([247dd43](https://bitbucket.org/metasolutions/entryscape/commits/247dd43))
- Respect the PSI flag config option ([944bb33](https://bitbucket.org/metasolutions/entryscape/commits/944bb33))

## [3.2.4](https://bitbucket.org/metasolutions/entryscape/branches/compare/3.2.4%0D3.2.3) - 2021-10-18

### Fixed

- Issue with banner on startpage ([10f2654](https://bitbucket.org/metasolutions/entryscape/commits/10f2654))

## [3.2.3](https://bitbucket.org/metasolutions/entryscape/branches/compare/3.2.3%0D3.2.2) - 2021-10-15

### Fixed

- Make sure the global EntryScapeSuite global corresponds to a registry with stuff in it ([50647af](https://bitbucket.org/metasolutions/entryscape/commits/50647af))

## [3.2.2](https://bitbucket.org/metasolutions/entryscape/branches/compare/3.2.2%0D3.2.1) - 2021-10-15

### Added

- Support for configuration restricting the terminology publishing toggle ([20f2558](https://bitbucket.org/metasolutions/entryscape/commits/20f2558))
- Support for configuration hiding the catalog embed option ([87cdc6d](https://bitbucket.org/metasolutions/entryscape/commits/87cdc6d))
- Support for configuration restricting concepts ([09934f7](https://bitbucket.org/metasolutions/entryscape/commits/09934f7))
- Support for configuration restricting catalog sharing settings ([ac16b70](https://bitbucket.org/metasolutions/entryscape/commits/ac16b70))
- Support for configuration restricting terminologies ([5b4ffb1](https://bitbucket.org/metasolutions/entryscape/commits/5b4ffb1))

### Fixed

- Respect searchProp when searching in entrychooser dialog ([ab3ac2a](https://bitbucket.org/metasolutions/entryscape/commits/ab3ac2a))

## [3.2.1](https://bitbucket.org/metasolutions/entryscape/branches/compare/3.2.1%0D3.2.0) - 2021-10-05

### Fixed

- Trigger geonames vis styles ([806df1c](https://bitbucket.org/metasolutions/entryscape/commits/806df1c))

## [3.2.0](https://bitbucket.org/metasolutions/entryscape/branches/compare/3.2.0%0D3.1.4) - 2021-09-29

### Added

- Sorting to EntrySelectList list header
- Dataset template filters (catalog/category)
- Possibility for a default dataset template catalog
- Browser warning banner
- Scrolling to the main navigation in case the available modules don’t fit the viewport
- Table view functionality to most catalog entry types and workbench entity types
- Showing the resource URI and metadata download links on info dialogs
- Way to access concept revisions
- Proper metadata titles for every view in the app
- Publish button to entities in Workbench

### Changed

- Optimized comment loading on dataset list
- Visualizations now refresh after replacing a distribution file
- Hide password control in Edge
- Selecting an entitytype on Workbench is now reflected on the url
- The UI on the search module to be consistent with the rest of the app post-react/MUI
- Switch to react router from reach router

### Fixed

- Unnecessary list re-mounting
- Issue where duplicate catalogs appeared on the search module for non-admins
- Various accessibility improvements on Suggestions
- Search field constantly losing focus on the sharing settings dialog

Read more about all fixed issues in [version 3.2.0](https://metasolutions.atlassian.net/jira/software/c/projects/ES/issues/?jql=project%20%3D%20%22ES%22%20AND%20%28status%20%3D%20Resolved%20OR%20status%20%3D%20Closed%29%20AND%20fixVersion%3D3.2%20ORDER%20BY%20updatedDate%20DESC)

## [3.1.4](https://bitbucket.org/metasolutions/entryscape/branches/compare/3.1.4%0D3.1.3) - 2021-08-25

### Added

- Edit distribution dialog ([41e682a](https://bitbucket.org/metasolutions/entryscape/commits/41e682a))

### Fixed

- Filter predicates on create distribution

## [3.1.3](https://bitbucket.org/metasolutions/entryscape/branches/compare/3.1.3%0D3.1.2) - 2021-08-19

### Added

- Deselecting selected concept on click ([bf781a5](https://bitbucket.org/metasolutions/entryscape/commits/bf781a5))
- Deselecting selected concept on clicking outside ([a4d12b8](https://bitbucket.org/metasolutions/entryscape/commits/a4d12b8))

### Changed

- Bumped entrystore.js version ([8d780aa](https://bitbucket.org/metasolutions/entryscape/commits/8d780aa))
- Bumped rdforms version ([ae12910](https://bitbucket.org/metasolutions/entryscape/commits/ae12910))

### Fixed

- Missing status on newly created suggestions based on templates ([34240e2](https://bitbucket.org/metasolutions/entryscape/commits/34240e2))
- Manual change in uri pattern ([​​cc3f822](https://bitbucket.org/metasolutions/entryscape/commits/​​cc3f822))

## [3.1.2](https://bitbucket.org/metasolutions/entryscape/branches/compare/3.1.2%0D3.1.1) - 2021-08-13

### Fixed

- Restrict entitytype lookup by default to current module ([73d2a40](https://bitbucket.org/metasolutions/entryscape/commits/73d2a40))
- Fix add non template dataset from suggestion ([f42c48b](https://bitbucket.org/metasolutions/entryscape/commits/f42c48b))

## [3.1.1](https://bitbucket.org/metasolutions/entryscape/branches/compare/3.1.1%0D3.1.0) - 2021-08-13

### Fixed

- Forces rdforms field language to the right ([229d20c](https://bitbucket.org/metasolutions/entryscape/commits/229d20c))

### Changed

- Switched catalog overview default back to true ([ac59c84](https://bitbucket.org/metasolutions/entryscape/commits/ac59c84))

## [3.1.0](https://bitbucket.org/metasolutions/entryscape/branches/compare/3.1.0%0D3.0.0) - 2021-07-23

### Added

- Dataset templates
- Tableview
- Notification system (snackbars) ([#355](https://metasolutions.atlassian.net/browse/ES-355))
- Global error handler
- Suggestions on dataset overview
- Limitation of presented statistics to only cover most used files and APIs

Read more about all fixed issues in [version 3.1.0](https://metasolutions.atlassian.net/jira/software/c/projects/ES/issues/?jql=project%20%3D%20%22ES%22%20AND%20%28status%20%3D%20Resolved%20OR%20status%20%3D%20Closed%29%20AND%20fixVersion%3D3.1%20ORDER%20BY%20updatedDate%20DESC)

## [3.0.0](https://bitbucket.org/metasolutions/entryscape/branches/compare/3.0.0%0D2.11.13) - 2021-06-02

### Added

- Support for always visible external links, usually used for the privacy policy and accessibility statement

### Changed

- Re-design of modules Catalog, Registry, Terms, Workbench, Admin
- Migration of the UI to React and Material UI components

Read more about all fixed issues in [version 3.0.0](https://metasolutions.atlassian.net/jira/software/c/projects/ES/issues/?jql=project%20%3D%20%22ES%22%20AND%20%28status%20%3D%20Resolved%20OR%20status%20%3D%20Closed%29%20AND%20fixVersion%3D3.0%20ORDER%20BY%20updatedDate%20DESC)
