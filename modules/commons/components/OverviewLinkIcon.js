import ArrowOutwardIcon from '@mui/icons-material/ArrowOutward';
import PropTypes from 'prop-types';
import { mediumGray } from 'commons/theme/mui/colors';

const OverviewLinkIcon = ({ to, isVisible }) =>
  to || isVisible ? (
    <ArrowOutwardIcon
      style={{
        position: 'absolute',
        right: 0,
        top: 0,
        width: '20px',
        color: mediumGray.main,
      }}
    />
  ) : null;

OverviewLinkIcon.propTypes = {
  to: PropTypes.oneOfType([PropTypes.func, PropTypes.string]),
  isVisible: PropTypes.bool,
};

export default OverviewLinkIcon;
