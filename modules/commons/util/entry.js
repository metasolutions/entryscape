import PropTypes from 'prop-types';
import { Graph, namespaces as ns } from '@entryscape/rdfjson';
import {
  PrototypeEntry,
  Entry,
  EntryInfo,
  EntryStore,
} from '@entryscape/entrystore-js';
import { entrystore } from 'commons/store';
import {
  createEntry,
  createEntryFromEntityType,
  spreadEntry,
} from 'commons/util/store';
import Lookup from 'commons/types/Lookup';
import { getLabel } from './rdfUtils';
import { USERS_ENTRY_ID } from './userIds';

const RDF_TYPE_CATALOG = 'dcat:Catalog';
const RDF_TYPE_DATASET = 'dcat:Dataset';
const RDF_TYPE_DATASET_SERIES = 'dcat:DatasetSeries';
const RDF_TYPE_DATASERVICE = 'dcat:DataService';
const RDF_TYPE_DISTRIBUTION = 'dcat:Distribution';
const RDF_TYPE_TERMINOLOGY = 'skos:ConceptScheme';
const RDF_TYPE_CONCEPT_SCHEME = RDF_TYPE_TERMINOLOGY;
const RDF_TYPE_CONCEPT = 'skos:Concept';
const RDF_TYPE_COLLECTION = 'skos:Collection';
const RDF_TYPE_SHOWCASE = 'esterms:Result';
const RDF_TYPE_IDEA = 'esterms:Idea';
const RDF_TYPE_SUGGESTION = 'esterms:Suggestion';
const RDF_TYPE_DATASET_TEMPLATE = 'esterms:DatasetTemplate';

const RDF_STATUS_INVESTIGATING = 'esterms:investigating';
const RDF_STATUS_ARCHIVED = 'esterms:archived';

const RDF_PROPERTY_DATASET = 'dcat:dataset';
const RDF_PROPERTY_DISTRIBUTION = 'dcat:distribution';
const RDF_TYPE_CONTACT = [
  'vcard:Kind',
  'vcard:Individual',
  'vcard:Organization',
];
const RDF_TYPE_DOCUMENT = [
  'foaf:Document',
  'dcterms:LicenseDocument',
  'dcterms:Standard',
  'prof:Profile',
];
const RDF_TYPE_FILE = 'esterms:File';
const RDF_PROPERTY_DRAFT_STATUS = 'esterms:incompleteMandatoryMetadata';

// Entry convenience functions to be passed as inputs/callbacks
const canWriteMetadata = (entry) => entry.canWriteMetadata();
const canAdministerEntry = canWriteMetadata;
const canReadMetadata = (entry) => entry.canReadMetadata();
const hasMetadataRevisions = (entry) =>
  entry.getEntryInfo().hasMetadataRevisions();
const canWriteResource = (entry) => entry.canWriteResource();

/**
 * Request entry from entrystore
 *
 * @param {Entry} entry
 * @returns {Promise<Entry>} entry
 */
const refreshEntry = (entry) => {
  entry.setRefreshNeeded();
  return entry.refresh();
};

/**
 *
 * @param {Entry} entry
 * @param {Graph} graph
 * @returns {Promise}
 */
const commitMetadata = (entry, graph) => entry.setMetadata(graph).commit();

/**
 *
 * @param {Entry} entry
 * @param {boolean} isDraft
 * @param {boolean} isPrototype
 * @returns {Promise<Entry>}
 */
const setEntryDraftStatus = async (entry, isDraft, isPrototype = false) => {
  const entryInfo = entry.getEntryInfo();
  const currentStatus = entryInfo.getStatus();
  const expandedStatus = ns.expand(RDF_PROPERTY_DRAFT_STATUS);
  const currentlyIsDraft = currentStatus === expandedStatus;
  if (isDraft !== currentlyIsDraft) {
    entryInfo.setStatus(isDraft ? expandedStatus : '');
    if (!isPrototype) await entryInfo.commit();
  }
  return entry;
};

/**
 *
 * @param {Entry} entry
 * @param {Graph} graph
 * @returns {Promise}
 */
const commitDraftMetadata = (entry, graph) =>
  commitMetadata(entry, graph).then(setEntryDraftStatus);

/**
 *
 * @param {Entry} entry
 * @returns {boolean}
 */
const isDraftEntry = (entry) =>
  entry.getEntryInfo().getStatus() === ns.expand(RDF_PROPERTY_DRAFT_STATUS);

/**
 * An entry is consdiered internally published if signed in users can access it.
 *
 * @param {Entry} entry
 * @returns {boolean}
 */
const isInternallyPublished = (entry) => {
  const entryInfo = entry.getEntryInfo();
  const acl = entryInfo.getACL(true);
  const { rread, mread } = acl;
  return rread.includes(USERS_ENTRY_ID) && mread.includes(USERS_ENTRY_ID);
};

/**
 *
 * @param {Entry} entry
 * @param {EntityType} entityType
 * @returns {undefined}
 */
const changeEntryEntityType = async (entry, entityType) => {
  const { info: entryInfo } = spreadEntry(entry);
  const graph = entryInfo.getGraph();
  graph.findAndRemove(entryInfo.getMetadataURI(), 'esterms:entityType');
  const primaryEntityType = await Lookup.primary(entry);
  if (entityType.getId() !== primaryEntityType.getId()) {
    graph.add(
      entryInfo.getMetadataURI(),
      'esterms:entityType',
      entityType.getId()
    );
  }
};

/**
 *
 * @param {Entry} entry
 * @param {EntityType} entityType
 * @returns {Promise<EntryInfo>}
 */
const saveEntryByEntityType = async (entry, entityType) => {
  await changeEntryEntityType(entry, entityType);
  const { info: entryInfo } = spreadEntry(entry);
  return entryInfo.commit();
};

/**
 * Defaults to cached external metadata.
 * Best to be used as a render name in a list row
 *
 * @param {Entry} entry
 * @returns {string}
 */
const getEntryLabel = (entry) =>
  getLabel(entry) ||
  getLabel(entry.getCachedExternalMetadata(), entry.getResourceURI());

/**
 * Usually used to check create vs edit mode
 *
 * @param {Entry} entry
 * @returns {boolean}
 */
const isNewEntry = (entry) => entry instanceof PrototypeEntry;

/**
 * Get a new PrototypeEntry for a context.
 * Type/Resource URi are either explicit or derived from the entityType definition
 * @return {store/PrototypeEntry}
 */
const createPrototypeEntry = ({
  context,
  contextName,
  entryName,
  entityType,
  rdfType,
}) => {
  const prototypeEntry = contextName
    ? createEntryFromEntityType(context, entityType, {
        contextName,
        entryName,
      })
    : createEntry(context);

  const { metadata, ruri } = spreadEntry(prototypeEntry);
  if (rdfType || entityType.rdfType) {
    const type =
      rdfType ||
      (Array.isArray(entityType.rdfType)
        ? entityType.rdfType[0]
        : entityType.rdfType);

    metadata.add(ruri, 'rdf:type', type, true);
  } else if (entityType.constraints) {
    Object.keys(entityType.constraints).forEach((prop) => {
      const obj = entityType.constraints[prop];
      if (Array.isArray(obj)) {
        metadata.add(ruri, prop, obj[0]);
      } else {
        metadata.add(ruri, prop, obj);
      }
    });
  }

  return prototypeEntry;
};

const getTemplateId = async (entry) => {
  const entityType = await Lookup.inUse(entry);
  return entry.isLinkReference()
    ? entityType.complementaryTemplateId()
    : entityType.templateId();
};

/**
 *
 * @param {Entry} entry
 * @param {string} str
 * @returns {[]}
 */
const findResourceStatements = (entry, str) =>
  entry.getMetadata().find(entry.getResourceURI(), str) || [];

/**
 *
 * @param {Entry} distributionEntry
 * @param {string} str
 * @returns {string}
 */
const getResourceURI = (distributionEntry, str) =>
  distributionEntry
    .getMetadata()
    .findFirstValue(distributionEntry.getResourceURI(), str);

/**
 * Add a group with specific access right for an entry
 * @param {Entry} entry
 * @param {Entry || string} group
 * @param {string} right
 * @returns {Entry}
 */
const addAccessRightToGroup = (entry, group, right) => {
  const entryInformation = entry.getEntryInfo();
  const accessControl = entryInformation.getACL(true);
  const groupId = typeof group === 'string' ? group : group.getId();
  switch (right) {
    case 'mread':
    case 'mwrite':
    case 'rread':
    case 'rwrite':
      accessControl[right].push(groupId);
      break;
    case 'owner':
      accessControl.admin.push(groupId);
      break;
    default:
      return entry;
  }
  entryInformation.setACL(accessControl);
  return entry;
};

/**
 * Fetches entries that belong to the provided store.
 *
 * @param {Entry[]} entries
 * @param {EntryStore} store
 * @returns {Promise<Entry[]>}
 */
const fetchEntriesByStore = async (entries, store) => {
  if (!store) return [];

  const entryURIs = entries
    .filter((entry) => entry.getEntryStore() === store)
    .map((entry) => entry.getURI());

  if (!entryURIs.length) return [];
  return store.newSolrQuery().uri(entryURIs).list().getEntries();
};

/**
 * Checks if any of the provided entries have been modified (by others) to avoid conflicts.
 *
 * @param {Entry[]} entries
 * @param {EntryStore} remoteStore
 * @returns {Promise<boolean>}
 */
const checkEntriesHaveConflict = async (entries = [], remoteStore) => {
  if (!entries.length) return false;

  // Refresh entries before checking to decrease risk of conflicts
  for (const entry of entries) {
    await refreshEntry(entry);
  }

  const entryUriToModified = {};
  for (const entry of entries) {
    const entryInfo = entry.getEntryInfo();
    const previousModificationTime = (
      entryInfo.getModificationDate() || entryInfo.getCreationDate()
    ).getTime();
    entryUriToModified[entry.getURI()] = previousModificationTime;
  }

  const fetchedLocalEntries = await fetchEntriesByStore(entries, entrystore);
  const fetchedRemoteEntries = await fetchEntriesByStore(entries, remoteStore);
  const newlyFetchedEntries = [...fetchedLocalEntries, ...fetchedRemoteEntries];

  const hasConflict = newlyFetchedEntries.some((entry) => {
    const entryInfo = entry.getEntryInfo();
    const newModificationTime = (
      entryInfo.getModificationDate() || entryInfo.getCreationDate()
    ).getTime();
    return newModificationTime !== entryUriToModified[entry.getURI()];
  });

  return hasConflict;
};

/**
 *
 * @param {string} entryURI
 * @returns {Promise<Entry|undefined>}
 */
const getOrIgnoreEntry = async (entryURI) => {
  try {
    const entry = await entrystore.getEntry(entryURI);
    return entry;
  } catch (error) {
    console.error(`Could not find entry from entry uri ${entryURI}`);
  }
};

export const entryPropType = PropTypes.instanceOf(Entry);

export const graphPropType = PropTypes.shape({
  onChange: PropTypes.func.isRequired,
  exportRDFJSON: PropTypes.func.isRequired,
  add: PropTypes.func.isRequired,
});

export {
  canWriteMetadata,
  canAdministerEntry,
  canReadMetadata,
  getOrIgnoreEntry,
  hasMetadataRevisions,
  canWriteResource,
  commitMetadata,
  setEntryDraftStatus,
  commitDraftMetadata,
  isDraftEntry,
  isInternallyPublished,
  changeEntryEntityType,
  saveEntryByEntityType,
  refreshEntry,
  getEntryLabel,
  isNewEntry,
  createPrototypeEntry,
  getTemplateId,
  findResourceStatements,
  getResourceURI,
  addAccessRightToGroup,
  checkEntriesHaveConflict,
  RDF_TYPE_CATALOG,
  RDF_TYPE_DATASET,
  RDF_TYPE_DATASET_SERIES,
  RDF_TYPE_DATASET_TEMPLATE,
  RDF_TYPE_DATASERVICE,
  RDF_TYPE_DISTRIBUTION,
  RDF_TYPE_TERMINOLOGY,
  RDF_TYPE_CONCEPT,
  RDF_TYPE_CONCEPT_SCHEME,
  RDF_PROPERTY_DATASET,
  RDF_PROPERTY_DISTRIBUTION,
  RDF_TYPE_COLLECTION,
  RDF_TYPE_SHOWCASE,
  RDF_TYPE_IDEA,
  RDF_TYPE_CONTACT,
  RDF_TYPE_DOCUMENT,
  RDF_TYPE_SUGGESTION,
  RDF_STATUS_INVESTIGATING,
  RDF_STATUS_ARCHIVED,
  RDF_TYPE_FILE,
  RDF_PROPERTY_DRAFT_STATUS,
}; // TODO no need to export getEntryRenderName, use import { getLabel } from 'commons/util/rdfUtils'; directly
