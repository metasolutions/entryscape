/* eslint-disable no-use-before-define */

const registry = new Map(); // TODO @valentino is there a need for registryMapInital? e.g keep the first value of each key

/**
 * @returns {spa/Site}
 */
const getSiteManager = () => exports.get('siteManager');
/**
 * @returns {Object}
 */
const getSiteConfig = () => exports.get('siteConfig');
/**
 * @returns {store/EntryStore}
 */
const getEntryStore = () => exports.get('entrystore');
/**
 * @return {store/EntryStoreUtil}
 */
const getEntryStoreUtil = () => exports.get('entrystoreutil');
/**
 * @returns {store/Entry}
 */
const getEntry = () => exports.get('entry');
/**
 * @return {store/Context}
 */
const getContext = () => exports.get('context');
/**
 * @returns {rdfjson/namespaces}
 */
const getNamespaces = () => exports.get('namespaces');

const getError = () => exports.get('error');

const setError = (error) => exports.set('error', error);

const getApp = () => exports.get('app');

const setApp = (app) => exports.set('app', app);

const getCurrentView = () => exports.get('currentView');

const setCurrentView = (viewName) => exports.set('currentView', viewName);

const getApplicationConfig = () => exports.get('applicationConfig');

const setApplicationConfig = (applicationConfig) =>
  exports.set('applicationConfig', applicationConfig);

const getInstanceConfig = () => exports.get('instanceConfig');

const setInstanceConfig = (instanceConfig) =>
  exports.set('instanceConfig', instanceConfig);

const getViews = () => exports.get('views');

const getView = (viewName) => getViews().get(viewName);

const setViews = (views) => exports.set('views', views);

const getSolutions = () => exports.get('solutions');

const setSolutions = (solutions = {}) => exports.set('solutions', solutions);

// App generic registry methods.
const exports = {
  get(key) {
    return registry.get(key);
  },
  set(key, value) {
    registry.set(key, value);
  },
  onChangeOnce(key, callback) {
    return this.onChange(key, callback, false, true);
  },

  getApp,
  getCurrentView,
  getInstanceConfig,
  getError,
  getSiteManager,
  getSiteConfig,
  getEntryStore,
  getEntryStoreUtil,
  getEntry,
  getContext,
  getNamespaces,
  getSolutions,
  getApplicationConfig,
  getViews,
  getView,
  setApp,
  setCurrentView,
  setInstanceConfig,
  setError,
  setApplicationConfig,
  setSolutions,
  setViews,
};

export default exports;
