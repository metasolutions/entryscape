import SearchCatalogsList from 'catalog/search';
import SearchView from '../SearchView';

export default {
  views: [
    {
      name: 'search__list',
      class: SearchView,
      title: { en: 'Search', sv: 'Sök', de: 'Suche' },
      route: '/search',
      module: 'search',
      navbar: false,
      public: true,
    },
    // (registry) catalog search views
    {
      name: 'catalog__search',
      class: SearchCatalogsList,
      title: { en: 'Search', sv: 'Sök', de: 'Suche' },
      route: '/catalog-search',
      module: 'search',
      navbar: false,
      public: true,
    },
    {
      name: 'catalog__dataset__search',
      class: SearchCatalogsList,
      title: {
        en: 'Search datasets',
        sv: 'Sök datamängder',
        de: 'Suche nach Datensätzen',
      },
      route: '/catalog-search/:contextId',
      module: 'search',
      // By removing the parent we do not see the name of the view in the secondary navigation.
      // parent: 'catalog__search',
      navbar: false,
      public: true,
    },
  ],
};
