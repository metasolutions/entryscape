import PropTypes from 'prop-types';
import { Link, generatePath, matchPath, useParams } from 'react-router-dom';
import useUserPermission from 'commons/hooks/useUserPermission';
import { useUserState } from 'commons/hooks/useUser';
import { Chip, Link as MuiLink } from '@mui/material';
import ErrorFallbackComponent from 'commons/errors/ErrorFallbackComponent';
import ErrorBoundary from 'commons/errors/ErrorBoundary';
import useErrorHandler from 'commons/errors/hooks/useErrorHandler';
import { nlsPropType } from 'commons/hooks/useTranslation';
import { checkHasViewPermission } from 'commons/util/site';
import { getModuleFromName } from 'commons/util/module';
import config from 'config';
import { localize } from 'commons/locale';

const LayoutErrorWrapper = ({ error, children }) => {
  useErrorHandler(error);
  return children;
};

/**
 *
 * @param {boolean} primaryNavOpen
 * @param {boolean} helpDrawerOpen
 * @returns {string}
 */
const getContainerClasses = (primaryNavOpen, helpDrawerOpen) => {
  if (primaryNavOpen && helpDrawerOpen)
    return 'escoMainLayout__container escoMainLayout__container--shiftedBoth';
  if (primaryNavOpen)
    return 'escoMainLayout__container escoMainLayout__container--shifted';
  if (helpDrawerOpen)
    return 'escoMainLayout__container escoMainLayout__container--shiftedRight';

  return 'escoMainLayout__container';
};

/**
 *
 * @param {boolean} hasSecondaryNavigation
 * @param {boolean} helpDrawerOpen
 * @returns {string}
 */
const getMainContentClasses = (hasSecondaryNavigation, helpDrawerOpen) => {
  if (hasSecondaryNavigation && helpDrawerOpen)
    return 'escoMainLayout__content escoMainLayout__content--shiftedBoth';
  if (hasSecondaryNavigation)
    return 'escoMainLayout__content escoMainLayout__content--shifted';
  if (helpDrawerOpen)
    return 'escoMainLayout__content escoMainLayout__content--shiftedRight';

  return 'escoMainLayout__content';
};

const Main = ({
  primaryNavOpen,
  helpDrawerOpen,
  error,
  hasSecondaryNavigation,
  secondaryNavViews,
  location,
  viewDef,
  userEntry,
  matchedRoute,
  translate,
  children,
}) => {
  const navParams = useParams();
  const userPermission = useUserPermission(userEntry);
  const { userInfo } = useUserState();

  return (
    <div className={getContainerClasses(primaryNavOpen, helpDrawerOpen)}>
      <ErrorBoundary FallbackComponent={ErrorFallbackComponent}>
        <LayoutErrorWrapper error={error}>
          <div
            className={`escoSidebar ${
              primaryNavOpen ? 'escoSidebar--shifted' : ''
            }`}
          >
            {/* SecondaryNav */}
            {hasSecondaryNavigation ? (
              <nav
                id="secondary-navigation"
                aria-label={translate('secondaryNavigation')}
                className="escoSecondaryNav"
              >
                {secondaryNavViews.map(
                  ({ title, route, restrictTo, main, module: moduleName }) => {
                    const match =
                      matchPath(
                        {
                          path: route,
                          end: false,
                        },
                        encodeURI(location.pathname)
                      ) || viewDef.matchRoute === route;
                    const module = getModuleFromName(moduleName, userInfo);

                    return checkHasViewPermission(
                      userPermission,
                      restrictTo,
                      module
                    ) ? (
                      <MuiLink
                        onClick={(event) => {
                          if (route === matchedRoute) {
                            event.preventDefault();
                          }
                        }}
                        component={Link}
                        to={generatePath(route, navParams)}
                        key={route}
                        classes={{
                          root: match
                            ? 'escoSecondaryNav__Link escoSecondaryNav__Link--active'
                            : 'escoSecondaryNav__Link',
                        }}
                      >
                        {localize(title)}
                        {main ? (
                          <Chip
                            color="secondary"
                            size="small"
                            label="main"
                            sx={{ marginLeft: '4px' }}
                          />
                        ) : null}
                      </MuiLink>
                    ) : null;
                  }
                )}
              </nav>
            ) : (
              <div />
            )}

            <div className="escoSidebarBrand">
              <div className="escoBrand">
                <MuiLink
                  component={Link}
                  to="/"
                  className="escoBrand__Link"
                  title={translate('goHomeLink')}
                >
                  <img
                    className="escoBrand__logo"
                    src={
                      new URL(
                        'images/es-logo-grayscale.svg',
                        config.get('theme.default.themePath')
                      ).href
                    }
                    alt="EntryScape logo"
                  />
                </MuiLink>
              </div>
            </div>
          </div>

          <main
            id="main-content"
            className={getMainContentClasses(
              hasSecondaryNavigation,
              helpDrawerOpen
            )}
          >
            {children}
          </main>
        </LayoutErrorWrapper>
      </ErrorBoundary>
    </div>
  );
};

Main.propTypes = {
  children: PropTypes.node,
  primaryNavOpen: PropTypes.bool,
  helpDrawerOpen: PropTypes.bool,
  hasSecondaryNavigation: PropTypes.bool,
  error: PropTypes.shape({}),
  location: PropTypes.shape({
    pathname: PropTypes.string,
  }),
  matchedRoute: PropTypes.string,
  translate: PropTypes.func,
  viewDef: PropTypes.shape({
    matchRoute: PropTypes.string,
  }),
  userEntry: PropTypes.shape({}),
  secondaryNavViews: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string,
      title: nlsPropType,
      route: PropTypes.string,
    })
  ),
};

export default Main;
