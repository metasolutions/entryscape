import { useState, useEffect, useCallback } from 'react';
import PropTypes from 'prop-types';
import { entryPropType } from 'commons/util/entry';
import ListActionDialog from 'commons/components/common/dialogs/ListActionDialog';
import ContentWrapper from 'commons/components/common/ContentWrapper';
import useGetMainDialog from 'commons/hooks/useGetMainDialog';
import ProgressList from 'commons/components/common/ProgressList';
import { useTranslation } from 'commons/hooks/useTranslation';
import escaFilesNLS from 'catalog/nls/escaFiles.nls';
import escaApiProgressNLS from 'catalog/nls/escaApiProgress.nls';
import { STATUS_FAILED, useTasksList } from 'commons/hooks/useTasksList';
import { renderHtmlString } from 'commons/util/reactUtils';
import useValidateFiles from 'catalog/datasets/DatasetOverview/hooks/useValidateFiles';
import { TASKS_ACTIVATE_API } from './tasks';
import useActivateAPI from './useActivateAPI';
import { ignoreHandlers, stopIgnoringHandlers, handlersToIgnore } from './util';

const ActivateAPIDialog = ({
  closeDialog,
  entry: distributionEntry,
  refreshDistributions,
  datasetEntry,
}) => {
  const { getConfirmationDialog } = useGetMainDialog();
  const translate = useTranslation([escaFilesNLS, escaApiProgressNLS]);
  const { tasks, updateTask, allTasksDone } = useTasksList([
    ...TASKS_ACTIVATE_API,
  ]);
  const {
    validFiles,
    dialogType: fileValidationType,
    confirmFiles,
  } = useValidateFiles(distributionEntry);
  const [activateAPI] = useActivateAPI(
    distributionEntry,
    datasetEntry,
    updateTask
  );

  const [alertMessage, setAlertMessage] = useState(null);
  const hasFailedTask = tasks.some(({ status }) => status === STATUS_FAILED);

  useEffect(() => {
    // eslint-disable-next-line no-nested-ternary
    const newAlertMessage = allTasksDone
      ? translate('nlsProgressSuccess')
      : hasFailedTask
      ? translate('nlsProgressFailure')
      : null;

    setAlertMessage(newAlertMessage);
  }, [allTasksDone, hasFailedTask, translate]);

  /**
   * Presents a confirmation dialog in case the dataset or catalog are not public,
   * informing users that creating an API distribution would make data publicly available.
   *
   * @returns {Promise}
   */
  const confirmPublic = useCallback(async () => {
    const message = translate('activateAPINotAllowedDatasetNotPublic');
    const isCatalogPublic = datasetEntry.getContext().getEntry(true).isPublic();
    const isDatasetPublic = datasetEntry.isPublic();

    if (isCatalogPublic && isDatasetPublic) {
      return Promise.resolve(true);
    }

    return getConfirmationDialog({
      content: renderHtmlString(message),
    });
  }, [datasetEntry, getConfirmationDialog, translate]);

  const confirmAndActivateAPI = useCallback(async () => {
    let proceed = true;

    proceed = await confirmPublic();
    if (!proceed) {
      closeDialog();
      return;
    }

    proceed = await confirmFiles();
    if (fileValidationType === 'acknowledge' || !proceed) {
      closeDialog();
      return;
    }

    activateAPI();
  }, [activateAPI, confirmFiles, confirmPublic, fileValidationType]); // ! closeDialog - loop

  useEffect(() => {
    ignoreHandlers(handlersToIgnore);

    if (validFiles === null) {
      return stopIgnoringHandlers(handlersToIgnore);
    }

    confirmAndActivateAPI();
    return stopIgnoringHandlers(handlersToIgnore);
  }, [confirmAndActivateAPI, validFiles]);

  return (
    <ListActionDialog
      id="activate-api-dialog"
      closeDialog={() => {
        refreshDistributions();
        closeDialog();
      }}
      maxWidth="sm"
      title={translate('apiCreateHeader')}
      closeDialogButtonLabel={translate('activateApiDialogButton')}
      alert={{
        message: alertMessage,
        props: allTasksDone ? { severity: 'success' } : undefined,
      }}
    >
      <ContentWrapper>
        <ProgressList tasks={tasks} nlsBundles={[escaApiProgressNLS]} />
      </ContentWrapper>
    </ListActionDialog>
  );
};

ActivateAPIDialog.propTypes = {
  entry: entryPropType,
  datasetEntry: entryPropType,
  refreshDistributions: PropTypes.func,
  closeDialog: PropTypes.func,
};

export default ActivateAPIDialog;
