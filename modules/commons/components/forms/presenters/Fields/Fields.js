import PropTypes from 'prop-types';
import { FieldsLabel } from '../FieldsLabel';
import './Fields.scss';

export const FieldsWrapper = ({ children }) => {
  return <div className="esmoFieldsPresenter">{children}</div>;
};

FieldsWrapper.propTypes = {
  children: PropTypes.node,
};

export const Fields = ({ label, inline, children }) => (
  <FieldsWrapper>
    {inline ? (
      <FieldsLabel alignItems="start" label={label ? `${label}: ` : ''}>
        <span className={label ? 'esmoFieldsPresenter--inline' : ''}>
          {children}
        </span>
      </FieldsLabel>
    ) : (
      <>
        <FieldsLabel label={label} />
        <div>{children}</div>
      </>
    )}
  </FieldsWrapper>
);

Fields.propTypes = {
  label: PropTypes.string,
  inline: PropTypes.bool,
  children: PropTypes.node,
};

export default Fields;
