import PropTypes from 'prop-types';
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Typography,
} from '@mui/material';
import { WarningAmber } from '@mui/icons-material';
import ContentWrapper from 'commons/components/common/ContentWrapper';
import config from 'config';
import { i18n } from 'esi18n';
import escoErrorsNLS from 'commons/nls/escoErrors.nls';
import { useTranslation } from 'commons/hooks/useTranslation';
import './MaintenanceDialog.scss';

const MaintenanceDialog = ({ onClose, ...rest }) => {
  const t = useTranslation(escoErrorsNLS);
  const locale = i18n.getLocale();
  const { start, end, message } = config.get('entryscape.maintenance.planned');

  return (
    <Dialog open onClose={onClose} fullWidth maxWidth="sm" {...rest}>
      <DialogTitle className="escoMaintenanceDialogTitle">
        {t('maintenanceDialogTitle')}
        <WarningAmber
          className="escoMaintenanceDialogTitle__icon"
          color="warning"
          fontSize="inherit"
        />
      </DialogTitle>
      <DialogContent dividers>
        <ContentWrapper>
          <Typography gutterBottom>{message[locale]}</Typography>
          <Typography>
            <strong>{t('maintenanceDialogStart')}:</strong> {start}
          </Typography>
          <Typography>
            <strong>{t('maintenanceDialogEnd')}:</strong> {end}
          </Typography>
        </ContentWrapper>
      </DialogContent>
      <DialogActions>
        <Button onClick={onClose} variant="text">
          {t('maintenanceDialogClose')}
        </Button>
      </DialogActions>
    </Dialog>
  );
};

MaintenanceDialog.propTypes = {
  onClose: PropTypes.func,
};

export default MaintenanceDialog;
