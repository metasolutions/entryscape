import { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { terms } from '@entryscape/entrystore-js';
import { Typography, IconButton } from '@mui/material';
import { Refresh as RefreshIcon } from '@mui/icons-material';
import Button from '@mui/material/Button';
import esrePipelineResultDialogNLS from 'harvest/nls/esrePipelineResultListDialog.nls';
import escoListNLS from 'commons/nls/escoList.nls';
import { useTranslation } from 'commons/hooks/useTranslation';
import usePageTitle from 'commons/hooks/usePageTitle';
import Chart from 'commons/components/chart/Time';
import chartOptions from 'harvest/Status/chartOptions';
import Tooltip from 'commons/components/common/Tooltip';
import {
  getPipelineEntryAndResource,
  loadDatasetsOverTime,
  loadJobEntries,
} from 'harvest/util/data';
import {
  success as successColor,
  error as errorColor,
} from 'commons/theme/mui/colors';
import { getLabel } from 'commons/util/rdfUtils';
import { getData } from 'harvest/util/pipelineResult';
import Lookup from 'commons/types/Lookup';
import HarvestReport from './HarvestReport';
import './index.scss';

const Refresh = ({ refreshHandler }) => {
  const t = useTranslation(escoListNLS);
  return (
    <div className="escoFilters__actions esreHarvestList__Refresh">
      <Tooltip title={t('actionTooltipRefresh')}>
        <IconButton
          aria-label={t('refreshListLabel')}
          onClick={refreshHandler}
          color="primary"
        >
          <RefreshIcon />
        </IconButton>
      </Tooltip>
    </div>
  );
};

const Status = ({ contextId }) => {
  const [pipelineEntry, setPipelineEntry] = useState(null);
  const [chartData, setChartData] = useState(null);
  const [jobEntries, setJobEntries] = useState([]);
  const [pipelineResultData, setPipelineResultData] = useState([]);
  const [entityType, setEntityType] = useState();
  const [buttonDisabled, setButtonDisabled] = useState(false);
  const [timer, setTimer] = useState(null);
  const [pageTitle, setPageTitle] = usePageTitle();
  const isReharvestButtonVisible =
    pipelineEntry && pipelineEntry.canWriteResource();

  const t = useTranslation(esrePipelineResultDialogNLS);

  const chartNLSBundle = {
    success: t('success'),
    failure: t('failure'),
    legendLabelSuccess: t('legendLabelSuccess'),
    legendLabelFailed: t('legendLabelFailed'),
    yAxisLabel: t('yAxisLabel'),
  };

  const fetchData = async () => {
    const { pipelineEntry: pipeEntry } = await getPipelineEntryAndResource(
      contextId
    );
    setPipelineEntry(pipeEntry);

    const pipelineLabel = getLabel(pipeEntry);
    if (!pageTitle && pipelineLabel) {
      setPageTitle(pipelineLabel);
    }

    const datasetsOverTime = await loadDatasetsOverTime(
      contextId,
      chartNLSBundle
    );
    setChartData(datasetsOverTime);

    const jobs = await loadJobEntries(contextId);
    setJobEntries(jobs);

    const pipelineEntityType = await Lookup.primary(pipeEntry);
    setEntityType(pipelineEntityType);
  };

  useEffect(() => {
    fetchData();
  }, []);

  useEffect(() => {
    let entries = [];
    if (jobEntries.length > 0) {
      entries = jobEntries.map((entry) => {
        return getData(entry, entityType);
      });
    }
    setPipelineResultData(entries);
  }, [jobEntries, entityType]);

  useEffect(() => {
    if (timer) {
      clearTimeout(timer);
    }
    if (buttonDisabled) {
      const time = setTimeout(async () => {
        const jobs = await loadJobEntries(contextId);
        setJobEntries(jobs);
        setButtonDisabled(false);
      }, 2000);
      setTimer(time);
    }
  }, [buttonDisabled]);

  const isButtonDisabled = () => {
    if (jobEntries.length > 0) {
      const status = jobEntries[0].getEntryInfo().getStatus();
      return (
        status === terms.status.InProgress || status === terms.status.Pending
      );
    }
    return false;
  };

  const executeNewPipeline = async () => {
    setButtonDisabled(true);
    const pipeline = await pipelineEntry.getResource();
    await pipeline.execute();
  };

  return (
    <>
      {chartData?.datasets[0].data.length > 0 && (
        <div>
          <Typography
            variant="h2"
            gutterBottom
            classes={{ root: 'esreHarvestStatus__Header' }}
          >
            {t('chartTitle')}
          </Typography>
          <Chart
            data={chartData}
            colors={[
              {
                backgroundColor: successColor.main,
                borderColor: successColor.main,
                borderWidth: 1,
              },
              {
                backgroundColor: errorColor.light,
                borderColor: errorColor.light,
                borderWidth: 1,
              },
            ]}
            options={chartOptions(chartNLSBundle)}
            dimensions={{ height: '200px' }}
          />
        </div>
      )}
      <div className="esrePipelineResultHeader">
        {t('harvestingLatestX', pipelineResultData.length || 1)}
        <div className="esrePipelineResultHeader__actions">
          <Refresh refreshHandler={fetchData} />
          {isReharvestButtonVisible && (
            <Tooltip title={t('pRcreatePopoverTitle')}>
              <span>
                <Button
                  variant="contained"
                  color="primary"
                  aria-label={t('pRcreatePopoverTitle')}
                  onClick={() => executeNewPipeline()}
                  disabled={isButtonDisabled() || buttonDisabled}
                >
                  {pipelineResultData.length === 0
                    ? t('createButtonInitialLabel')
                    : t('pRcreateButtonLabel')}
                </Button>
              </span>
            </Tooltip>
          )}
        </div>
      </div>
      <div className="esreHarvestReportList">
        {pipelineResultData.length > 0 &&
          pipelineResultData.map((data, index) => (
            <HarvestReport
              pipelineData={data}
              jobEntry={jobEntries[index]}
              key={data.id}
            />
          ))}
      </div>
    </>
  );
};

Status.propTypes = {
  contextId: PropTypes.string.isRequired,
};

Refresh.propTypes = {
  refreshHandler: PropTypes.func.isRequired,
};

export default Status;
