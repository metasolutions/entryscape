import { Constraints } from '../../types/presenters/Constraints';
import * as fieldDefinitions from './fieldDefinitions';
import { StoreConfigPresenter } from '../../types/presenters/StoreConfigPresenter';

export const fieldPresenters = [
  { ...fieldDefinitions.label },
  { ...fieldDefinitions.template },
  { ...fieldDefinitions.description },
  { ...fieldDefinitions.rdfType },
  { ...fieldDefinitions.constraints, Presenter: Constraints },
  { ...fieldDefinitions.module },
  { ...fieldDefinitions.includeInternal },
  { ...fieldDefinitions.inlineCreation },
  { ...fieldDefinitions.includeFile },
  { ...fieldDefinitions.includeLink },
  { ...fieldDefinitions.iconId },
  { ...fieldDefinitions.useWith },
  { ...fieldDefinitions.refines },
  { ...fieldDefinitions.templateLevel },
  { ...fieldDefinitions.publishControl },
  { ...fieldDefinitions.searchProps },
  { ...fieldDefinitions.allContexts },
  { ...fieldDefinitions.context },
  { ...fieldDefinitions.uriPattern },
  { ...fieldDefinitions.restrictTo },
  { ...fieldDefinitions.global },
  { ...fieldDefinitions.main },
  { ...fieldDefinitions.storeConfig, Presenter: StoreConfigPresenter },
];
