import {
  ACTION_EDIT,
  ACTION_DIVIDER,
  ACTION_REMOVE,
} from 'commons/components/overview/actions';
import { withOverviewRefresh } from 'commons/components/overview';
import { withEditorProvider } from 'commons/components/forms/editors';
import EditFormDialog from 'models/forms/dialogs/EditFormDialog';
import EditAttributesDialog from 'models/fields/dialogs/EditAttributesDialog';
import PreviewFormDialog from 'models/components/PreviewFormDialog';
import PublishToggle from 'commons/components/entry/PublishToggle/PublishToggle';
import { FormatListNumbered, EditNote } from '@mui/icons-material';
import ChangeTypeDialog from 'models/components/ChangeTypeDialog/ChangeTypeDialog';
import editSections from '../utils/formEditors';
import FormItemsDialog from './dialogs/FormItemsDialog';
import RemoveFormButton from './RemoveFormButton';
import { RDF_TYPE_TO_RFS_FORM, RFS_FORMS } from '../utils/rfsForms';

export const sidebarActions = [
  {
    ...ACTION_EDIT,
    Dialog: withOverviewRefresh(
      withEditorProvider(EditFormDialog, editSections),
      'onEntryEdit'
    ),
  },
  {
    id: 'edit-attributes',
    Dialog: withOverviewRefresh(
      withEditorProvider(EditAttributesDialog),
      'onSave'
    ),
    labelNlsKey: 'editAttributesLabel',
    getProps: () => ({
      startIcon: <EditNote />,
    }),
  },
  {
    id: 'change-type',
    Dialog: withOverviewRefresh(ChangeTypeDialog, 'onTypeChange'),
    labelNlsKey: 'changeTypeLabel',
    getProps: () => ({
      action: {
        rfsItems: RFS_FORMS,
        rdfTypeToRfsItem: RDF_TYPE_TO_RFS_FORM,
      },
    }),
  },
  {
    ...ACTION_REMOVE,
    Component: RemoveFormButton,
    getProps: ({ entry }) => ({ entry }),
  },
  {
    id: 'preview',
    Dialog: PreviewFormDialog,
    labelNlsKey: 'preview',
  },
  {
    id: 'form-items',
    Dialog: withEditorProvider(FormItemsDialog),
    labelNlsKey: 'formItemsTitle',
    getProps: ({ entry }) => ({
      action: {
        formEntry: entry,
      },
      startIcon: <FormatListNumbered />,
    }),
  },
  ACTION_DIVIDER,
  {
    id: 'published',
    Component: PublishToggle,
    getProps: ({ nlsBundles }) => ({
      nlsBundles,
      disabledTooltipNlsKey: 'publishDisabledTitle',
    }),
  },
];
