import PropTypes from 'prop-types';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import { useTranslation } from 'commons/hooks/useTranslation';
import esreLinkCheckReportNLS from 'harvest/nls/esreLinkCheckReport.nls';

const Details = ({ data, tableHeaderNLSKey }) => {
  const t = useTranslation(esreLinkCheckReportNLS);

  return (
    <>
      <h3 className="esreTable__header">{t(tableHeaderNLSKey)}</h3>
      <TableContainer component={Paper}>
        <Table size="small" aria-label={t(tableHeaderNLSKey)}>
          <TableBody>
            {data
              .filter((item) => item.value)
              .map((item) => (
                <TableRow hover key={item.label}>
                  <TableCell
                    className="esreTableCell--bold"
                    component="th"
                    scope="row"
                  >
                    {t(item.label)}
                  </TableCell>
                  <TableCell>{item.value}</TableCell>
                </TableRow>
              ))}
          </TableBody>
        </Table>
      </TableContainer>
    </>
  );
};

Details.propTypes = {
  tableHeaderNLSKey: PropTypes.string.isRequired,
  data: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
};

export default Details;
