import PropTypes from 'prop-types';
import { useCallback, useEffect } from 'react';
import { Entry } from '@entryscape/entrystore-js';
import {
  ListItemOpenMenuButton,
  List,
  ListItem,
  ActionsMenu,
  ListItemText,
  ListView,
  withListModelProvider,
  useListQuery,
  ListPagination,
  ListItemAction,
  ListItemActionsGroup,
} from 'commons/components/ListView';
import {
  LIST_ACTION_EDIT,
  LIST_ACTION_INFO,
} from 'commons/components/EntryListView/actions';
import {
  useTranslation,
  nlsBundlesPropType,
} from 'commons/hooks/useTranslation';
import { OverviewListHeader } from 'commons/components/overview/OverviewListHeader';
import { OverviewListPlaceholder } from 'commons/components/overview';
import { localize } from 'commons/locale';
import { useESContext } from 'commons/hooks/useESContext';
import { getTitle, getShortModifiedDate } from 'commons/util/metadata';
import useAsync, { RESOLVED } from 'commons/hooks/useAsync';
import { getIconFromActionId, SearchAndCreateIcon } from 'commons/actions';
import {
  getSubEntities,
  addRelationToParent,
} from 'commons/types/utils/relations';
import InfoIcon from '@mui/icons-material/Info';
import { Edit as EditIcon } from '@mui/icons-material';
import { entityTypePropType } from 'commons/types/EntityType';
import LinkedDataBrowserDialog from 'commons/components/LinkedDataBrowserDialog';
import { getPathFromViewName } from 'commons/util/site';
import { isDraftEntry } from 'commons/util/entry';
import DraftIcon from '@mui/icons-material/StickyNote2';
import Tooltip from 'commons/components/common/Tooltip';
import OverviewLinkIcon from 'commons/components/OverviewLinkIcon';
import SearchOrCreateDialog from '../dialogs/SearchOrCreateDialog';
import { actions as defaultActions } from './actions';

const SEARCH_FIELDS = ['label'];
const ROWS_PER_PAGE = 10;

const AggregationsList = ({
  relation,
  entityType,
  entry: parentEntry,
  nlsBundles,
  getCreateProps,
  actions = defaultActions,
  to,
}) => {
  const translate = useTranslation(nlsBundles);
  const { context } = useESContext();
  const { data: subEntities, runAsync, status } = useAsync({ data: [] });
  const {
    CreateDialog,
    createActions,
    createDisabled,
    createDisabledTooltip,
    createDialogParams = {},
    createHighlightId = '',
  } = getCreateProps({
    relation,
    subEntities,
    entry: parentEntry,
    translate,
  });

  const refreshSubEntities = useCallback(
    () => runAsync(getSubEntities(parentEntry, relation, context, entityType)),
    [parentEntry, runAsync, relation, context, entityType]
  );
  const afterCreateSubEntry = (newSubEntry) =>
    addRelationToParent(parentEntry, newSubEntry, relation.property).then(
      refreshSubEntities
    );

  useEffect(() => {
    refreshSubEntities();
  }, [refreshSubEntities]);

  const { result: subItems, size } = useListQuery({
    items:
      status === RESOLVED
        ? subEntities.map((subEntityEntry) => ({
            entry: subEntityEntry,
            label: getTitle(subEntityEntry),
          }))
        : null,
    searchFields: SEARCH_FIELDS,
  });

  const label = relation.label || entityType.get('label');

  return (
    <>
      <OverviewListHeader
        heading={localize(label)}
        nlsBundles={nlsBundles}
        action={{
          Dialog: SearchOrCreateDialog,
          tooltip: createDisabled
            ? createDisabledTooltip
            : translate('addOrCreateRelationTitle'),
          disabled: createDisabled,
          highlightId: createHighlightId,
        }}
        actionIcon={<SearchAndCreateIcon />}
        actionParams={{
          entityType,
          afterCreate: afterCreateSubEntry,
          relation,
          refreshSubEntities,
          CreateDialog,
          createDialogParams,
          createActions,
          entry: parentEntry,
        }}
      />
      <ListView
        status={status}
        size={size}
        nlsBundles={nlsBundles}
        renderPlaceholder={() => (
          <OverviewListPlaceholder
            label={translate('relationsListPlaceholder')}
          />
        )}
      >
        <List renderLoader={(Loader) => <Loader height="10vh" />}>
          {subItems.map(({ entry: subEntity, label: subEntityLabel }) => {
            const actionsMenuItems = actions
              .filter(({ isVisible }) =>
                isVisible
                  ? isVisible({
                      entry: subEntity,
                    })
                  : true
              )
              .map((action) => ({
                label: translate(action.labelNlsKey),
                icon: getIconFromActionId(action.id),
                action: {
                  ...action,
                  entry: subEntity,
                  parentEntry,
                  relation,
                  refresh: refreshSubEntities,
                  nlsBundles,
                },
              }));

            return (
              <ListItem
                key={subEntity.getId()}
                to={
                  to
                    ? getPathFromViewName(to, {
                        contextId: subEntity.getContext().getId(),
                        entryId: subEntity.getId(),
                        entityName: entityType.get('name'),
                      })
                    : null
                }
              >
                <ListItemText
                  xs={5}
                  primary={
                    subEntityLabel ||
                    translate('unnamedEntity', subEntity.getId())
                  }
                />
                <ListItemAction xs={1}>
                  {isDraftEntry(subEntity) ? (
                    <Tooltip title={translate('draftTooltip')}>
                      <DraftIcon color="primary" />
                    </Tooltip>
                  ) : null}
                </ListItemAction>
                <ListItemText
                  xs={3}
                  secondary={getShortModifiedDate(subEntity)}
                />
                <ListItemActionsGroup
                  xs={2}
                  justifyContent="end"
                  actions={[
                    {
                      ...LIST_ACTION_INFO,
                      Dialog: LinkedDataBrowserDialog,
                      getProps: () => ({
                        nlsKeyLabel: 'typeInfoDialogTitle',
                        entry: subEntity,
                        entityType,
                        icon: <InfoIcon />,
                        title: translate('infoEntry'),
                      }),
                    },
                    {
                      ...LIST_ACTION_EDIT,
                      getProps: () => ({
                        entry: subEntity,
                        parentEntry,
                        icon: <EditIcon />,
                        title: translate('editEntry'),
                      }),
                    },
                  ]}
                />
                <ListItemOpenMenuButton xs={1} />
                <ActionsMenu items={actionsMenuItems} />
                <OverviewLinkIcon to={to} />
              </ListItem>
            );
          })}
        </List>
        {size > ROWS_PER_PAGE ? <ListPagination /> : null}
      </ListView>
    </>
  );
};

AggregationsList.propTypes = {
  relation: PropTypes.shape({
    entityType: PropTypes.string,
    property: PropTypes.string,
    type: PropTypes.string,
    max: PropTypes.number,
    label: PropTypes.shape({}),
  }),
  entityType: entityTypePropType,
  entry: PropTypes.instanceOf(Entry),
  nlsBundles: nlsBundlesPropType,
  getCreateProps: PropTypes.func,
  actions: PropTypes.arrayOf(PropTypes.shape({})),
  to: PropTypes.string,
};

export default withListModelProvider(AggregationsList, () => ({
  limit: ROWS_PER_PAGE,
}));
