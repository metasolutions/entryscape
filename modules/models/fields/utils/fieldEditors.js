import { EditSection } from 'commons/components/forms/editors';
import * as fields from 'models/utils/fieldDefinitions';
import { getConversionOnNodetypeChange } from 'models/utils/nodetype';

const ABOUT_SECTION = {
  labelNlsKey: 'aboutLabel',
  name: 'about',
  Editor: EditSection,
  editors: [
    fields.FIELD_TITLE,
    fields.FIELD_PURPOSE,
    fields.FIELD_ALTERNATIVE_TEXT,
    fields.FIELD_STATUS,
  ],
};

const RDF_SHAPE_SECTION = {
  labelNlsKey: 'rdfShapeLabel',
  name: 'rdf-shape',
  Editor: EditSection,
  editors: [
    fields.FIELD_PROPERTY,
    {
      ...fields.FIELD_NODETYPE,
      confirmChange: getConversionOnNodetypeChange,
    },
    fields.FIELD_DATATYPE,
    fields.FIELD_CARDINALITY,
    fields.FIELD_CONSTRAINT,
  ],
};

const PRESENTER_SECTION = {
  labelNlsKey: 'presenterLabel',
  name: 'presenter',
  Editor: EditSection,
  editors: [fields.FIELD_LABEL, fields.FIELD_DESCRIPTION],
};

const EDITOR_SECTION = {
  labelNlsKey: 'editorLabel',
  name: 'editor',
  Editor: EditSection,
  editors: [
    fields.FIELD_EDIT_LABEL,
    fields.FIELD_EDIT_DESCRIPTION,
    fields.FIELD_PLACEHOLDER,
    fields.FIELD_HELP,
    fields.FIELD_PATTERN,
    fields.FIELD_VALUE_TEMPLATE,
    fields.FIELD_DEPS,
    fields.FIELD_LANGUAGE,
  ],
};

const DESIGN_SECTION = {
  labelNlsKey: 'designLabel',
  name: 'form-settings',
  Editor: EditSection,
  editors: [fields.FIELD_CSS],
};

const editFields = [
  ABOUT_SECTION,
  RDF_SHAPE_SECTION,
  PRESENTER_SECTION,
  EDITOR_SECTION,
  DESIGN_SECTION,
];

export const extendedFieldEditors = [
  fields.FIELD_TITLE,
  fields.FIELD_PURPOSE,
  fields.FIELD_STATUS,
  fields.FIELD_CARDINALITY,
  fields.FIELD_LABEL,
  fields.FIELD_DESCRIPTION,
  fields.FIELD_HELP,
  fields.FIELD_PLACEHOLDER,
  fields.FIELD_EDIT_LABEL,
  fields.FIELD_EDIT_DESCRIPTION,
  fields.FIELD_CSS,
  fields.FIELD_DEPS,
];

export default editFields;
