import EditEntryDialog from 'commons/components/entry/EditEntryDialog';
import { withListRerender } from 'commons/components/ListView';

const ListEditEntryDialog = withListRerender(EditEntryDialog, 'onEntryEdit');

export default ListEditEntryDialog;
