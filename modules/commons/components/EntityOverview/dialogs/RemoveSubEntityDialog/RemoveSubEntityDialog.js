import PropTypes from 'prop-types';
import { entryPropType } from 'commons/util/entry';
import RemoveEntryDialog from 'commons/components/entry/RemoveEntryDialog';
import { useCallback } from 'react';
import useCheckReferences from 'commons/hooks/references/useCheckReferences';
import { RESOLVED } from 'commons/hooks/useAsync';
import escoEntityOverviewNLS from 'commons/nls/escoEntityOverview.nls';

const RemoveSubEntityDialog = ({
  entry,
  parent,
  relation,
  closeDialog,
  refresh,
  ...restProps
}) => {
  const onRemove = useCallback(() => {
    const parentMetadata = parent.getMetadata();
    parentMetadata.findAndRemove(
      null,
      relation.property,
      entry.getResourceURI()
    );
    parent.commitMetadata().then(refresh);
  }, [relation, parent, entry, refresh]);

  const { references, checkReferencesStatus } = useCheckReferences({
    entry,
    nlsBundles: [escoEntityOverviewNLS],
    callback: closeDialog,
    ignoreCallback: (reference) => reference === parent,
  });

  if (checkReferencesStatus !== RESOLVED || references.length !== 0)
    return null;

  return (
    <RemoveEntryDialog
      entry={entry}
      closeDialog={closeDialog}
      onRemoveCallback={onRemove}
      {...restProps}
    />
  );
};

RemoveSubEntityDialog.propTypes = {
  entry: entryPropType,
  parent: entryPropType,
  relation: PropTypes.shape({
    property: PropTypes.string,
  }),
  closeDialog: PropTypes.func,
  refresh: PropTypes.func,
};

export default RemoveSubEntityDialog;
