import { Entry, Context } from '@entryscape/entrystore-js';
import { unpublishDataset } from 'catalog/datasets/DatasetOverview/utils/publish';
import { createEntry, getGroupWithHomeContext } from 'commons/util/store';
import { entrystoreUtil } from 'commons/store';
import { RDF_PROPERTY_DATASET, RDF_TYPE_CATALOG } from 'commons/util/entry';
import config from 'config';
import Lookup from 'commons/types/Lookup';

/**
 * Create a dataset prototype entry
 *
 * @param {Context} context
 * @param {string} rdfType
 * @returns {Entry}
 */
export const createPrototypeDatasetEntry = async (context, rdfType) => {
  const datasetPrototype = createEntry(context, rdfType);
  const groupEntry = await getGroupWithHomeContext(
    datasetPrototype.getContext()
  );
  // Make the dataset entry private
  await unpublishDataset(datasetPrototype, groupEntry, true);
  const metadata = datasetPrototype.getMetadata();
  metadata.add(datasetPrototype.getResourceURI(), 'rdf:type', rdfType, true);
  return datasetPrototype;
};

/**
 * @param {Entry} originalDatasetEntry
 * @param {string} copyTitlePrefix
 * @returns {Promise}
 */
export const copyDataset = async (
  originalDatasetEntry,
  copyTitlePrefix = ''
) => {
  const newDatasetEntry = createEntry(
    originalDatasetEntry.getContext(),
    'dcat:Dataset'
  );
  const originalPrimaryEntityType = await Lookup.primary(originalDatasetEntry);
  const originalEntityType = await Lookup.inUse(originalDatasetEntry);

  const newDatasetMetadata = originalDatasetEntry
    .getMetadata()
    .clone()
    .replaceURI(
      originalDatasetEntry.getResourceURI(),
      newDatasetEntry.getResourceURI()
    );

  const newEntryInfo = newDatasetEntry.getEntryInfo();
  newEntryInfo.setStatus(originalDatasetEntry.getEntryInfo().getStatus());

  if (originalEntityType.getId() !== originalPrimaryEntityType.getId()) {
    const newEntryInfoGraph = newEntryInfo.getGraph();
    newEntryInfoGraph.add(
      newEntryInfo.getMetadataURI(),
      'esterms:entityType',
      originalEntityType.getId()
    );
  }

  return getGroupWithHomeContext(newDatasetEntry.getContext())
    .then((groupEntry) => {
      const entryInformation = newDatasetEntry.getEntryInfo();
      const accessControl = entryInformation.getACL(true);
      accessControl.admin.push(groupEntry.getId());
      entryInformation.setACL(accessControl);
    })
    .then(() => {
      newDatasetEntry.setMetadata(newDatasetMetadata);
      const originalTitles = newDatasetMetadata.find(null, 'dcterms:title');
      newDatasetMetadata.findAndRemove(null, 'dcterms:title');
      originalTitles.forEach((originalTitle) => {
        newDatasetMetadata.addL(
          newDatasetEntry.getResourceURI(),
          'dcterms:title',
          copyTitlePrefix + originalTitle.getValue(),
          originalTitle.getLanguage()
        );
      });
      newDatasetMetadata.findAndRemove(null, 'dcat:distribution');

      if (config.get('catalog.includeVisualizations')) {
        newDatasetMetadata.findAndRemove(null, 'schema:diagram');
      }

      return newDatasetEntry.commit().then((newEntry) =>
        entrystoreUtil
          .getEntryByType('dcat:Catalog', newEntry.getContext())
          .then((catalog) => {
            catalog
              .getMetadata()
              .add(
                catalog.getResourceURI(),
                'dcat:dataset',
                newEntry.getResourceURI()
              );

            return catalog.commitMetadata().then(() => {
              newEntry.setRefreshNeeded();
              return newEntry.refresh();
            });
          })
      );
    });
};

/**
 * Removes a dataset's broken distribution references
 *
 * @param {Entry} datasetEntry
 * @param {string[]} distributionResourceURIs
 * @returns {Promise}
 */
export const removeBrokenReferences = async (
  datasetEntry,
  distributionResourceURIs
) => {
  const datasetMetadata = datasetEntry.getMetadata();
  distributionResourceURIs.forEach((distributionResourceURI) => {
    datasetMetadata.findAndRemove(
      datasetEntry.getResourceURI(),
      'dcat:distribution',
      {
        type: 'uri',
        value: distributionResourceURI,
      }
    );
  });

  return datasetEntry.commitMetadata();
};

/**
 * Extract broken references to distributions by checking if the entries exist.
 *
 * @param {Entry} datasetEntry
 * @returns {Promise<string[]>}
 */
export const getBrokenReferences = async (datasetEntry) => {
  const datasetMetadata = datasetEntry.getMetadata();
  const datasetResourceURI = datasetEntry.getResourceURI();
  const distributionStatements = datasetMetadata.find(
    datasetResourceURI,
    'dcat:distribution'
  );

  // load entries by resource uri instead because we can't guarantee that distributions are local
  const resourceURIsNotFound = [];
  for (const distributionStatement of distributionStatements) {
    const distributionResourceURI = distributionStatement.getValue();

    // Dont' show error dialog if failing to get entry.
    try {
      await entrystoreUtil.getEntryByResourceURI(
        distributionResourceURI,
        datasetEntry.getContext()
      );
    } catch (error) {
      const { message } = error;
      // Workaround to extract the proper error for when an entry is not found.
      // The references should only be removed if the entries don't exist.
      if (message.toLowerCase().includes('resources could not be found')) {
        resourceURIsNotFound.push(distributionResourceURI);
      }
    }
  }
  return resourceURIsNotFound;
};

/**
 *
 * @param {Entry} entry
 * @returns {boolean}
 */
export const isDatasetPSI = (entry) =>
  Boolean(
    !entry.getMetadata().findFirstValue(null, 'http://entryscape.com/terms/psi')
  );

/**
 * Update the catalog entry with the new dataset
 *
 * @param {Entry} datasetEntry
 * @returns {Promise}
 */
export const updateCatalogWithDataset = (datasetEntry) => {
  return entrystoreUtil
    .getEntryByType(RDF_TYPE_CATALOG, datasetEntry.getContext())
    .then((catalog) => {
      catalog
        .getMetadata()
        .add(
          catalog.getResourceURI(),
          RDF_PROPERTY_DATASET,
          datasetEntry.getResourceURI()
        );

      return catalog.commitMetadata().then(() => {
        datasetEntry.setRefreshNeeded();
        datasetEntry.refresh();
      });
    })
    .catch((error) => {
      throw Error(
        `Unable to update the catalog with the new dataset: ${error}`
      );
    });
};
