import PropTypes from 'prop-types';
import Table from '@mui/material/Table';
import TableHead from '@mui/material/TableHead';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import Typography from '@mui/material/Typography';
import { useTranslation } from 'commons/hooks/useTranslation';
import { getPercentage } from 'harvest/util/data';
import 'harvest/Statistics/tables/index.scss';

const DistributionPerDataset = ({
  datasetCount,
  dataPerDataset,
  nlsBundle,
}) => {
  const data = Object.keys(dataPerDataset);
  const t = useTranslation(nlsBundle);

  return (
    <div>
      {data.length > 0 && (
        <>
          <Typography
            variant="h6"
            component="h2"
            className="esreStatistics__header"
          >
            {t('distributionsPerDataset')}
          </Typography>
          <TableContainer component={Paper}>
            <Table size="small" aria-label={t('distributionsPerDataset')}>
              <TableHead>
                <TableRow>
                  <TableCell align="left" className="esreTable__th--bold">
                    {t('distributionsPerDataset')}
                  </TableCell>
                  <TableCell className="esreTable__th--bold" align="center">
                    {t('numberDatasets')}
                  </TableCell>
                  <TableCell className="esreTable__th--bold" align="center">
                    {t('percentageDatasets')}
                  </TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {data.map((key) => (
                  <TableRow hover key={key}>
                    <TableCell
                      className="esreTable__th--bold"
                      component="th"
                      scope="row"
                    >
                      {key}
                    </TableCell>
                    <TableCell align="center">{dataPerDataset[key]}</TableCell>
                    <TableCell align="center">
                      {getPercentage(dataPerDataset[key], datasetCount, 1)}
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </>
      )}
    </div>
  );
};

DistributionPerDataset.propTypes = {
  datasetCount: PropTypes.number,
  dataPerDataset: PropTypes.shape(),
  nlsBundle: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
};

DistributionPerDataset.defaultProps = {
  datasetCount: 0,
  dataPerDataset: {},
};
export default DistributionPerDataset;
