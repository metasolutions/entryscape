import { shapes } from 'jointjs';
import { RDF_TYPE_OBJECT_FORM } from 'models/utils/ns';
import './AttributedBox.scss';

const { Generic } = shapes.basic;

const maybeShorten = (str) =>
  str.length > 25 ? `${str.substring(0, 23)} …` : str;

shapes.AttributedBox = Generic.define(
  'AttributedBox',
  {
    attrs: {
      body: {
        width: 'calc(w)',
        height: 'calc(h)',
        strokeWidth: 1,
        stroke: '#000000',
        fill: '#FFFFFF',
        rx: 4,
        ry: 4,
      },
      label: {
        width: 'calc(w)',
        height: 30,
        fill: 'none',
      },
      labelShape: {
        ref: 'label',
        strokeWidth: 1,
        stroke: '#000000',
        d: 'M 0,4 A 4,4 0 0 1 4,0 H calc(w-4) A 4,4 0 0 1 calc(w),4 V calc(h-4) H 0 z',
      },
      labelText: {
        ref: 'label',
        textVerticalAnchor: 'middle',
        textAnchor: 'middle',
        'ref-y': 0.5,
        'ref-x': 0.5,
        'font-weight': 'bold',
        fontSize: 12,
      },
      fields: {},
      fieldsText: {
        ref: 'fields',
        'ref-y': 5,
        'ref-x': 5,
        fontSize: 12,
        fill: 'black',
      },
      classIndicator: {
        ref: 'body',
        textVerticalAnchor: 'middle',
        textAnchor: 'end',
        y: 'calc(h-7)',
        x: 'calc(w-5)',
        'font-weight': 'bold',
        fontSize: 10,
        text: 'Class',
      },
      formIndicator: {
        ref: 'body',
        textVerticalAnchor: 'middle',
        textAnchor: 'start',
        y: 'calc(h-7)',
        x: 5,
        'font-weight': 'bold',
        fontSize: 10,
        text: 'Form',
      },
    },
    label: [],
    fields: [],
    fieldDefs: [],
  },
  {
    markup: [],
    initialize() {
      this.on(
        'change:label change:fields',
        () => {
          this.updateRectangle();
        },
        this
      );

      this.updateType();
      this.updateRectangle();

      Generic.prototype.initialize.apply(this, arguments);
    },

    implicitClass() {
      return !this.prop('formEntry') && !this.prop('classEntry');
    },
    updateType() {
      const formEntry = this.prop('formEntry');
      const objectForm = formEntry
        ? formEntry
            .getMetadata()
            .find(formEntry.getResourceURI(), 'rdf:type', RDF_TYPE_OBJECT_FORM)
            .length > 0
        : false;
      // ClassEntry and FormEntry
      if (formEntry && this.prop('classEntry')) {
        if (objectForm) {
          return this.buildMarkup('esmoClassAndObjectFormEntry');
        }
        return this.buildMarkup('esmoClassAndProfileFormEntry');
      }
      // Only FormEntry
      if (formEntry) {
        if (objectForm) {
          return this.buildMarkup('esmoObjectFormEntry');
        }
        return this.buildMarkup('esmoProfileFormEntry');
      }
      // Only ClassEntry
      if (this.prop('classEntry')) {
        return this.buildMarkup('esmoClassEntry');
      }
      // Only Class
      return this.buildMarkup('esmoClass');
    },
    buildMarkup(boxClass) {
      this.set('markup', [
        {
          tagName: 'rect',
          selector: 'body',
        },
        {
          tagName: 'g',
          className: boxClass,
          children: [
            {
              tagName: 'g',
              selector: 'labelGroup',
              className: 'esmoLabelGroup',
              children: [
                {
                  tagName: 'rect',
                  selector: 'label',
                },
                {
                  tagName: 'path',
                  className: 'esmoLabelShape',
                  selector: 'labelShape',
                },
                {
                  tagName: 'text',
                  className: 'esmoLabelText',
                  selector: 'labelText',
                },
              ],
            },
            {
              tagName: 'rect',
              selector: 'fields',
            },
            {
              tagName: 'text',
              selector: 'fieldsText',
              className: 'esmoFieldsText',
            },
            {
              tagName: 'text',
              selector: 'classIndicator',
              className: 'esmoClassIndicator',
            },
            {
              tagName: 'text',
              selector: 'formIndicator',
              className: 'esmoFormIndicator',
            },
          ],
        },
      ]);
    },

    updateRectangle() {
      const labelItems = [
        { type: 'label', text: this.get('label'), spacing: 15 },
        { type: 'fields', text: this.get('fields'), spacing: 22 },
      ];

      this.offsetY = 0;
      labelItems.forEach((rect) => {
        const lines = (Array.isArray(rect.text) ? rect.text : [rect.text]).map(
          maybeShorten
        );
        // if no lines, use fallback spacing
        const spacing = lines.length > 0 ? rect.spacing : 16;
        const rectHeight = lines.length * 12 + spacing;
        this.attr(`${rect.type}Text/text`, lines.join('\n'));
        this.attr(`${rect.type}/height`, rectHeight);

        // transform adds offset in relation to label
        if (this.offsetY > 0) {
          this.attr(`${rect.type}/transform`, `translate(0,${this.offsetY})`);
        }
        this.offsetY += rectHeight;
      });
    },
    resizeWidth(measureText) {
      let minWidth = 10;
      [this.get('label'), this.get('fields')].forEach((text) => {
        const lines = (Array.isArray(text) ? text : [text]).map(maybeShorten);
        const bbox = measureText(lines.join('\n'), {
          fill: 'black',
          'font-size': 12,
          'font-family': 'Times New Roman',
        });
        if (bbox.width > minWidth) {
          minWidth = bbox.width;
        }
      });
      minWidth = Math.ceil(minWidth / 20) * 20;
      this.set('size', { width: minWidth + 40, height: this.offsetY });
    },
  }
);

export const { AttributedBox } = shapes;
