import React from 'react';
import PropTypes from 'prop-types';
import ModelsLDBDialog from 'models/dialogs/ModelsLDBDialog';
import { entryPropType } from 'commons/util/entry';

const findEntry = (diagramEvent) => {
  if (diagramEvent) {
    switch (diagramEvent.type) {
      case 'class':
        return diagramEvent.classEntry;
      case 'propertyAttribute':
      case 'propertyRelation':
        return diagramEvent.propertyEntry;
      case 'form':
        return diagramEvent.formEntry;
      case 'fieldAttribute':
      case 'fieldRelation':
        return diagramEvent.itemEntry;
      case 'implicitClass':
      case 'implicitProperty':
      default:
      // Do nothing
    }
  }
};

const InfoDialog = ({ diagramEvent, closeDialog }) => {
  const entry = findEntry(diagramEvent);
  if (!entry) return null;
  return <ModelsLDBDialog closeDialog={closeDialog} entry={entry} />;
};

InfoDialog.propTypes = {
  diagramEvent: PropTypes.shape({
    type: PropTypes.string,
    classEntry: entryPropType,
    formEntry: entryPropType,
    itemEntry: entryPropType,
    propertyEntry: entryPropType,
  }),
  closeDialog: PropTypes.func,
};

export default InfoDialog;
