import adminSiteConfig from 'admin/config/site';
import catalogSiteConfig from 'catalog/config/site';
import merge from 'commons/merge';
import Signin from 'commons/auth/Signin';
import Signup from 'commons/auth/Signup';
import Version from 'commons/version';
import harvestSiteConfig from 'harvest/config/site';
import Start from 'commons/start';
import statusSiteConfig from 'status/config/site';
import toolkitSiteConfig from 'toolkit/config/site';
import workbenchSiteConfig from 'workbench/config/site';
import searchSiteConfig from 'search/config/site';
import { SINGLE_COLUMN_LAYOUT } from 'commons/Layout';

const siteConfigs = merge(
  adminSiteConfig,
  catalogSiteConfig,
  workbenchSiteConfig,
  statusSiteConfig,
  toolkitSiteConfig,
  harvestSiteConfig,
  searchSiteConfig,
  {
    signinView: 'signin',
    permissionView: 'permission',
    startView: 'start',
    signupView: 'signup',
    sidebar: { wide: false, always: true, replaceTabs: true },
    moduleList: ['status', 'search', 'register', 'toolkit', 'admin'],
    modules: [
      {
        name: 'search',
        productName: { en: 'Browse', sv: 'Sök', de: 'Suche' },
        startView: 'catalog__search',
        title: {
          en: 'Dataset search',
          sv: 'Datamängds\u00ADsök',
          de: 'Datensatz\u00ADsuche',
        },
        sidebar: true,
        text: {
          sv: 'Sök fram och utforska de datamängder som framgångsrikt skördats',
          en: 'Search and view the datasets that have been successfully harvested',
          de: 'Suchen und anzeigen von erfolgreich geharvesteten Datensätzen',
        },
      },
      {
        // @todo needed? should be imported by adminConfig
        name: 'admin',
        productName: { en: 'Admin', sv: 'Admin', de: 'Admin' },
        title: {
          en: 'Admini\u00ADstration',
          sv: 'Admini\u00ADstrera',
          de: 'Verwaltung',
        },
        faClass: 'cogs',
        startView: 'admin__users',
        sidebar: true,
        restrictTo: 'admin',
        text: {
          sv: 'Administrera projekt, användare och grupper',
          en: 'Manage projects, users and groups',
          de: 'Verwaltung von Projekten, Benutzern und Gruppen',
        },
      },
    ],
    views: [
      {
        name: 'start',
        class: Start,
        title: {
          en: 'Start',
          sv: 'Start',
          da: 'Start',
          de: 'Start',
        },
        route: '/start',
        public: true,
      },
      {
        name: 'signin',
        title: {
          en: 'Sign in',
          sv: 'Logga in',
          da: 'Login',
          de: 'Anmelden',
        },
        class: Signin,
        route: '/signin',
        layout: SINGLE_COLUMN_LAYOUT,
        public: true,
      },
      {
        name: 'signup',
        title: {
          en: 'Create account',
          sv: 'Skapa konto',
          de: 'Konto erstellen',
        },
        class: Signup,
        route: '/signup',
        layout: SINGLE_COLUMN_LAYOUT,
        public: true,
      },
      {
        name: 'version',
        title: { en: 'version' },
        class: Version,
        route: '/version',
        restrictTo: 'admin',
      },
    ],
  }
);

export default merge(siteConfigs, __entryscape_config.site || {});
