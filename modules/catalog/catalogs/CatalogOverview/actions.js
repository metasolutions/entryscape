import {
  ACTION_EDIT,
  ACTION_REVISIONS,
  ACTION_REMOVE,
} from 'commons/components/overview/actions';
import {
  LIST_ACTION_DOWNLOAD,
  LIST_ACTION_SHARING_SETTINGS,
} from 'commons/components/EntryListView/actions';
import ProjectTypeChangeDialog from 'commons/types/dialogs/ProjectTypeChangeDialog';
import { hasAdminRights } from 'commons/util/user';
import Lookup from 'commons/types/Lookup';
import { withOverviewRefresh } from 'commons/components/overview/hooks/useOverviewModel';
import { isContextPremium, canEditContextMetadata } from 'commons/util/context';
import config from 'config';
import EditCatalog from 'catalog/catalogs/dialogs/EditCatalog';
import RemoveCatalogDialog from 'catalog/catalogs/dialogs/RemoveCatalogDialog';
import EmbedDialog from '../dialogs/EmbedDialog/index';

const canEditProjectType = (entry, userEntry, userInfo) => {
  if (!userInfo) return false;
  return (
    canEditContextMetadata(entry, userEntry) &&
    Lookup.getProjectTypeOptions(userInfo).length
  );
};

const isExemptFromRestrictions = (userEntry, contextEntry) =>
  hasAdminRights(userEntry) ||
  config.get('catalog.includeEmbeddOption') ||
  isContextPremium(contextEntry);

const sidebarActions = [
  {
    ...ACTION_EDIT,
    Dialog: withOverviewRefresh(EditCatalog, 'onChange'),
    isVisible: ({ entry, userEntry }) =>
      canEditContextMetadata(entry, userEntry, true),
  },
  {
    ...ACTION_REVISIONS,
    isVisible: ({ entry, userEntry }) =>
      canEditContextMetadata(entry, userEntry),
    action: {
      excludeProperties: ['dcat:dataset'],
    },
  },
  {
    id: 'change-projecttype',
    Dialog: withOverviewRefresh(ProjectTypeChangeDialog, 'onChange'),
    labelNlsKey: 'changeProjectTypeLabel',
    isVisible: ({ entry, userEntry, userInfo }) =>
      canEditProjectType(entry, userEntry, userInfo),
  },
  {
    ...LIST_ACTION_SHARING_SETTINGS,
    labelNlsKey: 'catalogMember',
    action: {
      sharingSettingsRestrictionPath: () =>
        config.get('catalog.disallowCatalogCollaborationDialog'),
    },
    highlightId: 'overviewActionSharing',
  },
  {
    ...LIST_ACTION_DOWNLOAD,
    labelNlsKey: 'catalogExport',
    action: {
      profile: 'dcat',
    },
    highlightId: 'overviewActionDownload',
  },
  {
    id: 'embed',
    Dialog: EmbedDialog,
    labelNlsKey: 'catalogEmbed',
    isVisible: ({ entry, userEntry }) => {
      const contextEntry = entry.getContext().getEntry(true);
      return isExemptFromRestrictions(userEntry, contextEntry);
    },
    highlightId: 'overviewActionEmbed',
  },
  {
    ...ACTION_REMOVE,
    isVisible: ({ entry, userEntry }) =>
      canEditContextMetadata(entry, userEntry),
    Dialog: RemoveCatalogDialog,
  },
];

export { sidebarActions };
