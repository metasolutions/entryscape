import { Entry } from '@entryscape/entrystore-js';
import { spreadEntry } from 'commons/util/store';
import { RDF_PROPERTY_DATASET } from 'commons/util/entry';

/**
 *
 * @param {Entry} catalogEntry
 * @returns {number}
 */
const getNumberOfDatasetsInCatalog = (catalogEntry) => {
  const { allMetadata, ruri } = spreadEntry(catalogEntry);
  return allMetadata.find(ruri, RDF_PROPERTY_DATASET).length;
};

/**
 *
 * @param {*} catalogEntryA
 * @param {*} catalogEntryB
 * @returns {number} 1|-1
 */
const byDatasetNumberDescending = (catalogEntryA, catalogEntryB) => {
  const nrOfDatasetsA = getNumberOfDatasetsInCatalog(catalogEntryA);
  const nrOfDatasetsB = getNumberOfDatasetsInCatalog(catalogEntryB);

  return nrOfDatasetsA < nrOfDatasetsB ? 1 : -1;
};

/**
 *
 * @param {*} solrQuery
 * @returns {Promise.<{entries: Entry[], size: number}>}
 */
const fetchSortedCatalogs = async (solrQuery) => {
  const searchList = solrQuery.list();
  const catalogEntries = await searchList.getAllEntries();
  const sortedCatalogEntries = catalogEntries.sort(byDatasetNumberDescending); // sorts in place

  return { entries: sortedCatalogEntries, size: searchList.getSize() };
};

export { fetchSortedCatalogs, getNumberOfDatasetsInCatalog };
