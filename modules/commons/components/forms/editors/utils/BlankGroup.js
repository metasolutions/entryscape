import { Blank } from './Blank';
import { FieldGroup } from './FieldGroup';

/**
 * @param {{graph: Graph, resourceURI: string, property: string}} props
 * @returns {string[]}
 */
const getInitialBlankNodeIds = ({ graph, resourceURI, property }) => {
  return graph.find(resourceURI, property).map((statement) => {
    return statement.getValue();
  });
};

/**
 * @param {{graph: Graph, resourceURI: string, property: string, rdfType: string}} props
 * @returns {Blank[]}
 */
const getBlanks = ({
  graph,
  resourceURI,
  property,
  rdfType,
  validate,
  insertEmptyField,
}) => {
  const blankNodeIds = getInitialBlankNodeIds({ graph, resourceURI, property });
  if (blankNodeIds.length) {
    return blankNodeIds.map((blankNodeId) => {
      return new Blank({
        graph,
        resourceURI,
        property,
        rdfType,
        blankNodeId,
        validate,
      });
    });
  }
  return insertEmptyField
    ? [new Blank({ graph, resourceURI, property, rdfType, validate })]
    : [];
};

export class BlankGroup extends FieldGroup {
  constructor(fieldProperties) {
    super(fieldProperties);
    this.fields = getBlanks(fieldProperties);
  }

  addField() {
    const newField = new Blank(this.fieldProperties);
    this.fields = [...this.fields, newField];
    return newField;
  }
}
