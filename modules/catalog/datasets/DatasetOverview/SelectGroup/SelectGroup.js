import { createContext, useContext } from 'react';
import PropTypes from 'prop-types';
import { Button } from '@mui/material';
import Tooltip from 'commons/components/common/Tooltip';
import './SelectGroup.scss';

const SelectContext = createContext();

const useSelect = () => {
  const context = useContext(SelectContext);
  if (!context)
    throw new Error('Select context must be used within context provider');
  return context;
};

export const SelectGroup = ({ value, onChange, ...props }) => {
  return (
    <div className="escaSelectGroup">
      <SelectContext.Provider value={[value, onChange]} {...props} />
    </div>
  );
};

SelectGroup.propTypes = {
  value: PropTypes.string,
  onChange: PropTypes.func,
};

export const SelectButton = ({ value, tooltip, children, ...props }) => {
  const [selectedValue, setSelectedValue] = useSelect();
  const isSelected = value === selectedValue;
  return (
    <Tooltip title={tooltip} placement="top">
      <Button
        className={
          isSelected
            ? 'escaSelectGroup__button--selected'
            : 'escaSelectGroup__button'
        }
        {...props}
        onClick={() => {
          setSelectedValue(value);
        }}
      >
        {children}
      </Button>
    </Tooltip>
  );
};

SelectButton.propTypes = {
  value: PropTypes.string,
  tooltip: PropTypes.node,
  children: PropTypes.node,
};

export default SelectGroup;
