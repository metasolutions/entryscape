import * as ns from 'models/utils/ns';
import { Entry } from '@entryscape/entrystore-js';
import { Graph, utils } from '@entryscape/rdfjson';
import { getExtendedEntry, isExtended } from 'models/utils/extension';
import { hasValue } from 'models/components/Extends';
import { getValueURIValue } from 'models/utils/options';
import { getHasURIValue } from 'models/utils/resource';
import { getEntryProps, getRfsLanguageLiterals } from 'models/utils/metadata';
import { entrystore, entrystoreUtil } from 'commons/store';
import { getOrIgnoreEntry, RDF_TYPE_CONCEPT } from 'commons/util/entry';

const OPTION_LANGUAGE_PROPERTIES = [
  ns.RDF_PROPERTY_LABEL,
  ns.RDF_PROPERTY_EDIT_LABEL,
  ns.RDF_PROPERTY_DESCRIPTION,
  ns.RDF_PROPERTY_EDIT_DESCRIPTION,
];
const SKOS_PREF_LABEL = 'skos:prefLabel';
const SKOS_DEFINITION = 'skos:definition';
const CONCEPT_LANGUAGE_PROPERTIES = [SKOS_DEFINITION, SKOS_PREF_LABEL];

const CONCEPT_PROPERTY_MAP = {
  [SKOS_PREF_LABEL]: ns.RDF_PROPERTY_LABEL,
  [SKOS_DEFINITION]: ns.RDF_PROPERTY_DESCRIPTION,
};

/**
 * If the select field entry is extended, find and use the original entry. Since
 * select field options can't be overridden, only the original entry will have
 * options.
 *
 * @param {Graph} graph
 * @param {string} resourceURI
 * @returns {Promise<Entry>}
 */
const getSelectFieldRootEntry = async (graph, resourceURI) => {
  if (isExtended(graph, resourceURI)) {
    return getExtendedEntry(
      graph,
      resourceURI,
      ns.RDF_PROPERTY_OPTION,
      hasValue
    );
  }
};

/**
 * Determines whether or not the option has a default value
 *
 * @param {Graph} graph
 * @param {string} optionId
 * @returns {boolean}
 */
export const hasDefaultValue = (graph, optionId) =>
  Boolean(graph?.findFirstValue(optionId, 'esterms:default'));

/**
 * Finds the option which has a default value from an array of options
 *
 * @param {Graph} graph
 * @param {string} resourceURI
 * @returns {string|null}
 */
export const getDefaultOptionId = (graph, resourceURI) => {
  const options = graph
    .find(resourceURI, ns.RDF_PROPERTY_OPTION)
    .map((statement) => statement.getValue());

  return options?.find((option) => hasDefaultValue(graph, option)) || null;
};

/**
 *
 * @param {Graph} graph
 * @param {string} resourceURI
 * @returns {string[]}
 */
const getOptionIds = (graph, resourceURI) => {
  return graph
    .find(resourceURI, ns.RDF_PROPERTY_OPTION)
    .map((statement) => statement.getValue());
};

/**
 *
 * @param {Graph} graph
 * @param {string} resourceURI
 * @returns {string[]}
 */
export const getOptionIdsInOrder = (graph, resourceURI) => {
  return getOptionIds(graph, resourceURI).sort(
    (blankNodeId1, blankNodeId2) =>
      graph.findFirstValue(blankNodeId1, ns.RDF_PROPERTY_ORDER) -
      graph.findFirstValue(blankNodeId2, ns.RDF_PROPERTY_ORDER)
  );
};

/**
 *
 * @param {Graph} graph
 * @param {string} resourceURI
 * @returns {boolean}
 */
export const hasManagedOptions = (graph, resourceURI) => {
  return Boolean(getOptionIds(graph, resourceURI).length);
};

/**
 *
 * @param {Graph} graph
 * @param {string} blankNodeId
 * @returns {string}
 */
export const getOptionLabel = (graph, blankNodeId) => {
  return graph.findFirstValue(blankNodeId, ns.RDF_PROPERTY_LABEL);
};

/**
 *
 * @param {Graph} graph
 * @param {string} blankNodeId
 * @returns {string}
 */
const getManualOptionValue = (graph, blankNodeId) => {
  return graph.findFirstValue(blankNodeId, ns.RDF_PROPERTY_VALUE);
};

/**
 *
 * @param {Graph} graph
 * @param {string} optionBlankNodeId
 * @returns {Promise<string>}
 */
const getOptionValue = async (graph, optionBlankNodeId) => {
  const isURI = getHasURIValue(
    graph,
    [optionBlankNodeId],
    ns.RDF_PROPERTY_VALUE
  );
  if (isURI) return getValueURIValue({ graph, resourceURI: optionBlankNodeId });
  return getManualOptionValue(graph, optionBlankNodeId);
};

/**
 *
 * @param {Graph} graph
 * @param {string[]} reorderedOptionIds
 * @returns {Graph}
 */
export const reorderOptions = (graph, reorderedOptionIds) => {
  reorderedOptionIds.forEach((optionId, index) => {
    const [statement] = graph.find(optionId, ns.RDF_PROPERTY_ORDER);
    statement.setValue(`${index + 1}`);
  });
  return graph;
};

/**
 *
 * @param {Graph} graph
 * @param {string} resourceURI
 * @returns {object}
 */
const getManagedRfsOptions = async (graph, resourceURI) => {
  const optionIds = getOptionIdsInOrder(graph, resourceURI);

  const defaultValueBlankId = getDefaultOptionId(graph, resourceURI);

  const options = [];
  let defaultValue;
  // Get all the key values for each option saved on a blank node.
  // Each option object corresponds to an rdforms choice.
  for (const blankNodeId of optionIds) {
    const option = getRfsLanguageLiterals(
      graph,
      blankNodeId,
      OPTION_LANGUAGE_PROPERTIES
    );
    option.value = await getOptionValue(graph, blankNodeId);
    if (defaultValueBlankId === blankNodeId) {
      defaultValue = option.value;
    }
    options.push(option);
  }
  return {
    defaultValue,
    choices: options,
  };
};

/**
 *
 * @param {Graph} graph
 * @param {string} resourceURI
 * @returns {string}
 */
export const getTerminologyURI = (graph, resourceURI) => {
  return graph.findFirstValue(resourceURI, ns.RDF_PROPERTY_TERMINOLOGY);
};

/**
 *
 * @param {Graph} graph
 * @param {string} resourceURI
 * @returns {string}
 */
export const getCollectionURI = (graph, resourceURI) => {
  return graph.findFirstValue(resourceURI, ns.RDF_PROPERTY_COLLECTION);
};

/**
 *
 * @param {string} collectionURI
 * @returns {Promise<object[]>}
 */
export const getConceptEntriesFromCollection = async (collectionURI) => {
  const collectionEntry = await getOrIgnoreEntry(collectionURI);
  if (!collectionEntry) return [];
  const { graph, resourceURI } = getEntryProps({ entry: collectionEntry });
  const resourceURIs = graph
    .find(resourceURI, 'skos:member')
    .map((statement) => statement.getValue());

  const conceptEntriesAndMissing =
    await entrystoreUtil.loadEntriesByResourceURIs(resourceURIs, null, true);
  const conceptEntries = conceptEntriesAndMissing.filter(Boolean);
  return conceptEntries;
};

/**
 *
 * @param {string} terminologyURI
 * @returns {Promise<Entry[]>}
 */
const getConceptEntriesFromTerminology = async (terminologyURI) => {
  const terminologyEntry = await getOrIgnoreEntry(terminologyURI);
  if (!terminologyEntry) return [];
  const context = terminologyEntry.getContext();
  const conceptEntries = entrystore
    .newSolrQuery()
    .rdfType(RDF_TYPE_CONCEPT)
    .context(context)
    .limit(100)
    .list()
    .getEntries();
  return conceptEntries;
};

/**
 *
 * @param {Entry[]} conceptEntries
 * @returns {object}
 */
const getRfsOptionsFromConcepts = (conceptEntries) => {
  const choices = conceptEntries.map((entry) => {
    const graph = entry.getAllMetadata();
    const resourceURI = entry.getResourceURI();
    const option = getRfsLanguageLiterals(
      graph,
      resourceURI,
      CONCEPT_LANGUAGE_PROPERTIES
    );
    option.value = entry.getURI();
    return option;
  });
  return { choices };
};

/**
 *
 * @param {string} terminologyURI
 * @param {string} collectionURI
 * @returns {Promise<object[]>}
 */
export const getConceptEntries = (terminologyURI, collectionURI) => {
  if (!terminologyURI) return [];
  if (collectionURI) {
    return getConceptEntriesFromCollection(collectionURI);
  }
  return getConceptEntriesFromTerminology(terminologyURI);
};

/**
 *
 * @param {Graph} graph
 * @param {string} resourceURI
 * @param {string} terminologyURI
 * @returns {Promise<object[]>}
 */
const getRfsOptionsFromTerminology = async (
  graph,
  resourceURI,
  terminologyURI
) => {
  const collectionURI = getCollectionURI(graph, resourceURI);
  const conceptEntries = await getConceptEntries(terminologyURI, collectionURI);
  return getRfsOptionsFromConcepts(conceptEntries);
};

/**
 *
 * @param {Graph} graph
 * @param {string} resourceURI
 * @param {string} blankNodeId
 * @returns {Graph}
 */
export const removeOption = (graph, resourceURI, blankNodeId) => {
  graph.findAndRemove(resourceURI, ns.RDF_PROPERTY_OPTION, {
    value: blankNodeId,
    type: 'bnode',
  });
  utils.remove(graph, blankNodeId);
  return graph;
};

/**
 *
 * @param {Graph} graph
 * @param {string} resourceURI
 */
export const removeOptions = (graph, resourceURI) => {
  const optionIds = getOptionIds(graph, resourceURI);
  optionIds.forEach((optionId) => {
    removeOption(graph, resourceURI, optionId);
  });
};

/**
 *
 * @param {{graph: Graph, resourceURI: string }} resourceURI
 * @returns {string}
 */
export const getOptionsValue = ({ graph, resourceURI }) => {
  return `${graph.find(resourceURI, ns.RDF_PROPERTY_OPTION).length}`;
};

/**
 * Finds options from metadata and converts them to rdforms compatible choices.
 * If managed options are found, then it will be assumed to be managed.
 *
 * @param {Graph} graph
 * @param {string} resourceURI
 * @returns {Promise<object[]>}
 */
export const findRfsChoices = async (graph, resourceURI) => {
  const terminologyURI = getTerminologyURI(graph, resourceURI);
  const options = await getManagedRfsOptions(graph, resourceURI);
  const { choices } = options;
  if (terminologyURI && !choices.length) {
    return getRfsOptionsFromTerminology(graph, resourceURI, terminologyURI);
  }
  return options;
};

/**
 * Find choices recursively in case the select field is extended.
 *
 * @param {Graph} graph
 * @param {string} resourceURI
 * @returns {Promise<object[]>}
 */
export const findSelectFieldChoices = async (graph, resourceURI) => {
  const selectFieldEntry = await getSelectFieldRootEntry(graph, resourceURI);
  const { graph: rootGraph, resourceURI: rootUri } = selectFieldEntry
    ? getEntryProps(selectFieldEntry)
    : { graph, resourceURI };
  const { choices } = await findRfsChoices(rootGraph, rootUri);
  return choices;
};

/**
 *
 * @param {Graph} graph
 * @param {string} resourceURI
 * @returns {Graph}
 */
const addOptionBlank = (graph, resourceURI) => {
  return graph.add(resourceURI, ns.RDF_PROPERTY_OPTION, null, true).getValue();
};

/**
 *
 * @param {Graph} graph
 * @param {string} blankNodeId
 * @param {number} order
 */
const addOptionTypeAndOrder = (graph, blankNodeId, order) => {
  graph.add(blankNodeId, 'rdf:type', ns.RDF_CLASS_OPTION);
  graph.addD(blankNodeId, ns.RDF_PROPERTY_ORDER, `${order}`, 'xsd:integer');
};

/**
 *
 * @param {Graph} graph
 * @param {string} blankNodeId
 * @param {string} value
 */
const addOptionValue = (graph, blankNodeId, value) => {
  graph.add(blankNodeId, ns.RDF_PROPERTY_VALUE, value);
};

/**
 *
 * @param {Graph} graph
 * @param {string} blankNodeId
 * @param {Entry} conceptEntry
 */
const copyOptionValuesFromConcept = (graph, blankNodeId, conceptEntry) => {
  CONCEPT_LANGUAGE_PROPERTIES.forEach((conceptProperty) => {
    const statements = conceptEntry
      .getAllMetadata()
      .find(conceptEntry.getResourceURI(), conceptProperty);
    statements.forEach((statement) => {
      const targetProperty = CONCEPT_PROPERTY_MAP[conceptProperty];
      const { value, lang } = statement.getCleanObject();
      graph.addL(blankNodeId, targetProperty, value, lang);
    });
  });
};

/**
 *
 * @param {Graph} graph
 * @param {string} resourceURI
 * @param {Entry[]} conceptEntries
 */
export const importConcepts = (graph, resourceURI, conceptEntries) => {
  conceptEntries.forEach((conceptEntry) => {
    const blankNodeId = addOptionBlank(graph, resourceURI);
    addOptionTypeAndOrder(graph, blankNodeId);
    copyOptionValuesFromConcept(graph, blankNodeId, conceptEntry);
    addOptionValue(graph, blankNodeId, conceptEntry.getResourceURI());
  });
};
