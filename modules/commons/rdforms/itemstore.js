import { bundleLoader, ItemStore, renderingContext } from '@entryscape/rdforms';
import { getFallbackBundleUrls } from 'commons/util/bundle';
import config from 'config';
import registry from 'commons/registry';

const itemStore = new ItemStore();
itemStore.handleErrorAs = 'error';
registry.set('itemstore', itemStore);
registry.set('renderingContext', renderingContext);

/**
 * Load rdforms template bundles
 */
const initBundles = () => {
  const toLoadBundles = [];

  // Loading the default bundles separately with an extra param to getFallbackBundleUrls,
  // to avoid getting a theme template path for them
  config
    .get('itemstore.defaultBundles')
    .forEach((id) =>
      toLoadBundles.push(getFallbackBundleUrls(id, undefined, false))
    );

  config
    .get('itemstore.bundles')
    .forEach((id) => toLoadBundles.push(getFallbackBundleUrls(id)));

  /**
   * load the bundles into the itemStore,
   * TODO bad API, fix in rdforms. Should return what's needed
   * @async
   */
  return bundleLoader(itemStore, toLoadBundles);
};

const initLanguages = () => {
  renderingContext.setLanguageList(config.get('itemstore.languages'));
};

const initAppendLanguages = () => {
  renderingContext.setLanguageList([
    ...renderingContext.getLanguageList(),
    ...config.get('itemstore.appendLanguages'),
  ]);
};

const initChoosers = () => {
  config
    .get('itemstore.choosers')
    .forEach((chooser) =>
      console.log(`this chooser should be implemented ${chooser}`)
    );
  // config.get('itemstore.choosers').forEach((chooserName) => {
  //   import(
  //     /* webpackInclude: /.*Chooser\.js$/ */
  //     /* webpackMode: "eager" */
  //     `commons/rdforms/choosers/${chooserName}.js`
  //   ).then((chooser) => {
  //     try {
  //       chooser.default.registerDefaults();
  //     } catch (e) {
  //       throw Error(`Could not load chooser ${chooserName}`);
  //     }
  //   });
  // });
};

export default async () => {
  try {
    await initBundles();
  } catch (error) {
    registry.setError(error);
  }
  initAppendLanguages();
  initLanguages();
  initChoosers();
};

export { itemStore };
