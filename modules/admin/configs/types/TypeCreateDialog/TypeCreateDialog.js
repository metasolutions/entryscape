import PropTypes from 'prop-types';
import { useEffect, useState } from 'react';
import ListActionDialog from 'commons/components/common/dialogs/ListActionDialog';
import {
  useTranslation,
  nlsBundlesPropType,
} from 'commons/hooks/useTranslation';
import { useSnackbar } from 'commons/hooks/useSnackbar';
import { CREATE, useListModel } from 'commons/components/ListView';
import ContentWrapper from 'commons/components/common/ContentWrapper';
import {
  useEditorContext,
  validateFieldSet,
} from 'commons/components/forms/editors';
import useAsync, { PENDING } from 'commons/hooks/useAsync';
import LoadingButton from 'commons/components/LoadingButton';
import { ErrorCatcher } from 'commons/errors/ErrorCatcher';
import { ErrorWithMessage } from 'commons/errors/utils/errors';
import { useConfirmCloseAction } from 'commons/components/EntryListView/dialogs/create/handlers';

const TypeCreateDialog = ({
  closeDialog,
  title,
  fields,
  rdfTypes,
  nlsBundles,
  children,
}) => {
  const [, dispatch] = useListModel();
  const translate = useTranslation(nlsBundles);
  const [canSubmit, setCanSubmit] = useState(false);
  const { runAsync, status, error: createError } = useAsync();
  const {
    entry: prototypeEntry,
    graph,
    resourceURI,
    fieldSet,
  } = useEditorContext();
  const [addSnackbar] = useSnackbar();
  const confirmClose = useConfirmCloseAction(closeDialog);

  useEffect(() => {
    if (!graph) return;
    graph.onChange = () => setCanSubmit(true);
  }, [graph, resourceURI, fields]);

  const dispatchEntryCreated = () => dispatch({ type: CREATE });

  const createEntry = async () => {
    const errors = validateFieldSet(fieldSet);
    if (errors.length) {
      return;
    }

    for (const rdfType of rdfTypes) {
      graph.add(resourceURI, 'rdf:type', rdfType);
    }
    prototypeEntry.setMetadata(graph);

    runAsync(
      prototypeEntry
        .commit()
        .then(() => {
          closeDialog();
          dispatchEntryCreated();
          addSnackbar({ message: translate('createCustomEntityTypeSuccess') });
        })
        .catch((error) => {
          throw new ErrorWithMessage(
            translate('createCustomEntityTypeFail'),
            error
          );
        })
    );
  };

  const actions = (
    <LoadingButton
      onClick={createEntry}
      loading={status === PENDING}
      disabled={!canSubmit}
    >
      {translate('createButtonLabel')}
    </LoadingButton>
  );

  return (
    <>
      <ListActionDialog
        id="create-type-dialog"
        title={title}
        actions={actions}
        closeDialog={() => confirmClose(canSubmit)}
      >
        <ContentWrapper>{children}</ContentWrapper>
      </ListActionDialog>
      <ErrorCatcher error={createError} />
    </>
  );
};

TypeCreateDialog.propTypes = {
  closeDialog: PropTypes.func.isRequired,
  fields: PropTypes.arrayOf(PropTypes.shape({})),
  rdfTypes: PropTypes.arrayOf(PropTypes.string),
  title: PropTypes.string,
  nlsBundles: nlsBundlesPropType,
  children: PropTypes.node,
};

export default TypeCreateDialog;
