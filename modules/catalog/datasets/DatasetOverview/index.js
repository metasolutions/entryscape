import { useEntry } from 'commons/hooks/useEntry';
import {
  withOverviewModelProvider,
  overviewPropsPropType,
} from 'commons/components/overview';

import DatasetSeriesOverview from './DatasetSeriesOverview';
import DatasetOverview from './DatasetOverview';
import { isDatasetSeries } from '../utils';

const DatasetOverviewDecider = ({ overviewProps }) => {
  const entry = useEntry();
  const isSeries = isDatasetSeries(entry);

  if (isSeries) return <DatasetSeriesOverview overviewProps={overviewProps} />;
  return <DatasetOverview overviewProps={overviewProps} />;
};

DatasetOverviewDecider.propTypes = {
  overviewProps: overviewPropsPropType,
};

export default withOverviewModelProvider(DatasetOverviewDecider);
