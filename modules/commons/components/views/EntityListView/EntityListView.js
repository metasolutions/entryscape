import PropTypes from 'prop-types';
import {
  withListModelProviderAndLocation,
  useListModel,
  TOGGLE_TABLEVIEW,
  listPropsPropType,
} from 'commons/components/ListView';
import {
  EntryListView,
  useSolrQuery,
  MODIFIED_COLUMN,
  TITLE_COLUMN,
  PUBLIC_STATUS_COLUMN,
  DRAFT_STATUS_COLUMN,
  ACTIONS_GROUP_COLUMN,
} from 'commons/components/EntryListView';
import {
  LIST_ACTION_INFO,
  LIST_ACTION_EDIT,
} from 'commons/components/EntryListView/actions';
import { useTranslation } from 'commons/hooks/useTranslation';
import { localize } from 'commons/locale';
import TableView from 'commons/components/TableView';
import React, { useCallback, useMemo } from 'react';
import { entrystore } from 'commons/store';
import { useESContext } from 'commons/hooks/useESContext';
import Lookup from 'commons/types/Lookup';
import { getPathFromViewName } from 'commons/util/site';
import { applyEntityTypeParams } from 'commons/util/solr';
import registry from 'commons/registry';
import {
  PUBLIC_STATUS_FILTER,
  DRAFT_STATUS_FILTER,
} from 'commons/components/filters/utils/filterDefinitions';
import useSearchFilters from 'commons/components/filters/hooks/useSearchFilters';
import { getDraftsEnabled } from 'commons/types/utils/entityType';
import { nlsBundles, listActions } from './actions';

const EntityListView = ({ entityTypeName, listProps }) => {
  const { context, contextName } = useESContext();
  const translate = useTranslation(nlsBundles);
  const entityType = Lookup.getByName(entityTypeName);

  const applyQueryParams = useCallback(
    (query, queryOptions) => {
      return applyEntityTypeParams(query, queryOptions, entityType.get());
    },
    [entityType]
  );
  const publishable = entityType.get('publishControl') === true;
  const draftsEnabled = getDraftsEnabled(entityType);

  const filters = useMemo(
    () => [
      ...(publishable ? [PUBLIC_STATUS_FILTER] : []),
      ...(draftsEnabled ? [DRAFT_STATUS_FILTER] : []),
    ],
    [publishable, draftsEnabled]
  );

  const { applyFilters, isLoading, hasFilters, ...filterProps } =
    useSearchFilters(filters);

  const createQuery = useCallback(
    () =>
      entrystore
        .newSolrQuery()
        .rdfType(entityType.get('rdfType'))
        .context(context),
    [context, entityType]
  );

  const queryResults = useSolrQuery({
    createQuery,
    applyQueryParams,
    applyFilters,
  });

  const actionParams = {
    entityType,
    context,
    contextName,
    nlsBundles,
  };

  const [{ showTableView }, dispatch] = useListModel();

  return showTableView ? (
    <TableView
      setTableView={() => dispatch({ type: TOGGLE_TABLEVIEW })}
      rdfType={entityType.get('rdfType')}
      templateId={entityType.get('template')}
      filterScheme={filters}
    />
  ) : (
    <EntryListView
      {...queryResults}
      {...listProps}
      includeTableView
      includeFilters={hasFilters}
      filtersProps={hasFilters ? filterProps : null}
      nlsBundles={nlsBundles}
      listActions={listActions}
      listActionsProps={{ actionParams }}
      getListItemProps={({ entry }) => ({
        to: getPathFromViewName(`${registry.getCurrentView()}__overview`, {
          contextId: context.getId(),
          entryId: entry.getId(),
          entityName: entityTypeName,
        }),
      })}
      viewPlaceholderProps={{
        label: translate(
          'emptyMessageWithName',
          localize(entityType.get('label'))
        ),
      }}
      columns={[
        ...(publishable
          ? [
              {
                ...PUBLIC_STATUS_COLUMN,
                getProps: ({ entry }) => ({
                  entry,
                  publicTooltip: translate('publicTitle'),
                  privateTooltip: translate('privateTitle'),
                }),
              },
            ]
          : []),
        ...(!publishable && draftsEnabled ? [DRAFT_STATUS_COLUMN] : []),
        { ...TITLE_COLUMN, xs: draftsEnabled || publishable ? 7 : 8 },
        MODIFIED_COLUMN,
        {
          ...ACTIONS_GROUP_COLUMN,
          actions: [LIST_ACTION_INFO, LIST_ACTION_EDIT],
          xs: 2,
        },
      ]}
    />
  );
};

EntityListView.propTypes = {
  entityTypeName: PropTypes.string,
  listProps: listPropsPropType,
};

export default withListModelProviderAndLocation(EntityListView);
