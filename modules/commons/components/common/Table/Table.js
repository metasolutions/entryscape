import PropTypes from 'prop-types';
import {
  Table as MuiTable,
  TableRow,
  TableBody,
  TableHead,
  TableContainer,
  TableCell,
  Paper,
} from '@mui/material';

const Table = ({ headers = [], rows = [] }) => (
  <TableContainer component={Paper} style={{ maxWidth: '800px' }}>
    <MuiTable size="small">
      <TableHead>
        <TableRow>
          {headers.map((value, index) => (
            <TableCell
              key={value}
              scope="column"
              {...(index === 0 ? {} : { align: 'center' })}
            >
              {value}
            </TableCell>
          ))}
        </TableRow>
      </TableHead>
      <TableBody>
        {rows.map((row) => (
          <TableRow key={`${row[0]}-row`} hover>
            {row.map((value, index) => (
              <TableCell
                {...(index === 0
                  ? { component: 'th', scope: 'row' }
                  : { component: 'td', align: 'center' })}
                // eslint-disable-next-line react/no-array-index-key
                key={`${row[0]}-cell-${index}`}
              >
                {value}
              </TableCell>
            ))}
          </TableRow>
        ))}
      </TableBody>
    </MuiTable>
  </TableContainer>
);

Table.propTypes = {
  headers: PropTypes.arrayOf(PropTypes.string),
  rows: PropTypes.arrayOf(
    PropTypes.arrayOf(PropTypes.oneOfType(PropTypes.string, PropTypes.number))
  ),
};

export default Table;
