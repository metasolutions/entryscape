import { getLabel } from 'commons/util/rdfUtils';
import { ANY } from 'commons/components/filters/utils/filterDefinitions';
import { FIELD_STATUS_CHOICES } from './choiceDefinitions';
import { getModelEntries } from './model';

export const STATUS_FILTER = {
  headerNlsKey: 'statusLabel',
  applyFilterParams: (query, filter) => {
    const value = filter.getSelected();
    if (value === ANY) return query;
    query.uriProperty('rfs:status', value);
  },
  items: [
    {
      labelNlsKey: 'anyFilterLabel',
      value: ANY,
    },
    ...FIELD_STATUS_CHOICES,
  ],
  id: 'status-filter',
};

export const TYPE_FILTER = {
  headerNlsKey: 'typeLabel',
  applyFilterParams: (query, filter) => {
    const value = filter.getSelected();
    if (value === ANY) return query;
    query.rdfType(value);
  },
  id: 'type-filter',
};

export const getModelItems = async (currentContextId) => {
  const modelEntries = await getModelEntries();
  const modelItems = [];
  for (const modelEntry of modelEntries) {
    if (modelEntry.getId() === currentContextId) {
      modelItems.unshift({
        value: modelEntry.getId(),
        labelNlsKey: 'currentProjectLabel',
      });
    } else {
      modelItems.push({
        value: modelEntry.getId(),
        label: getLabel(modelEntry),
      });
    }
  }
  return [{ value: ANY, labelNlsKey: 'selectAll' }, ...modelItems];
};

export const MODEL_FILTER = {
  type: 'autocomplete',
  headerNlsKey: 'projectLabel',
  applyFilterParams: (query, filter) => {
    const value = filter.getSelected();
    if (value === 'all') return query;
    return query.context(value);
  },
  id: 'model-filter',
};
