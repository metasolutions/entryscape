import PropTypes from 'prop-types';
import Table from '@mui/material/Table';
import TableHead from '@mui/material/TableHead';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import Typography from '@mui/material/Typography';
import { useTranslation } from 'commons/hooks/useTranslation';
import { getPercentage } from 'harvest/util/data';
import 'harvest/Statistics/tables/index.scss';

const DistributionMetadata = ({ stats, nlsBundle }) => {
  const { totalDistributionCount } = stats;
  const distributionData = [
    {
      name: 'hasDistributionTitle',
      nlsKey: 'title',
    },
    {
      name: 'multilingualDistributionTitle',
      nlsKey: 'titleMultilingual',
    },
    {
      name: 'missingLanguageOnDistributionTitle',
      nlsKey: 'titleNoLanguage',
    },
    {
      name: 'hasDistributionDescription',
      nlsKey: 'description',
    },
    {
      name: 'multilingualDistributionDescription',
      nlsKey: 'descriptionMultilingual',
    },
    {
      name: 'missingLanguageOnDescription',
      nlsKey: 'descriptionNoLanguage',
    },
    {
      name: 'hasDownloadURL',
      nlsKey: 'downloadURL',
    },
    {
      name: 'hasAccessURL',
      nlsKey: 'accessURL',
    },
    {
      name: 'hasDistributionFormat',
      nlsKey: 'formats',
    },
    {
      name: 'hasDistributionLicense',
      nlsKey: 'licenses',
    },
  ];
  const t = useTranslation(nlsBundle);
  return (
    <div>
      <Typography
        variant="h6"
        component="h2"
        className="esreStatistics__header"
      >
        {t('metadataPerDistribution')}
      </Typography>
      <TableContainer component={Paper}>
        <Table size="small" aria-label={t('metadataPerDistribution')}>
          <TableHead>
            <TableRow>
              <TableCell align="left" className="esreTable__th--bold">
                {t('field')}
              </TableCell>
              <TableCell className="esreTable__th--bold" align="center">
                {t('numberDistributions')}
              </TableCell>
              <TableCell className="esreTable__th--bold" align="center">
                {t('percentageDistributions')}
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {distributionData.map((data) => (
              <TableRow hover key={data.name}>
                <TableCell
                  className="esreTable__th--bold"
                  component="th"
                  scope="row"
                >
                  {t(data.nlsKey)}
                </TableCell>
                <TableCell align="center">{stats[data.name]}</TableCell>
                <TableCell align="center">
                  {getPercentage(stats[data.name], totalDistributionCount, 1)}
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </div>
  );
};

DistributionMetadata.propTypes = {
  stats: PropTypes.shape().isRequired,
  nlsBundle: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
};
export default DistributionMetadata;
