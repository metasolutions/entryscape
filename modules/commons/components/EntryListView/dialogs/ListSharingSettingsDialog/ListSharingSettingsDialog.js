import PropTypes from 'prop-types';
import React, { useEffect } from 'react';
import { Entry } from '@entryscape/entrystore-js';
import ListActionDialog from 'commons/components/common/dialogs/ListActionDialog';
import { useTranslation } from 'commons/hooks/useTranslation';
import escoSharingSettingsNLS from 'commons/nls/escoSharingSettings.nls';
import esadGroupNLS from 'admin/nls/esadGroup.nls';
import ContentWrapper from 'commons/components/common/ContentWrapper';
import { loadContextEntry, isContextPremium } from 'commons/util/context';
import { namespaces as ns } from '@entryscape/rdfjson';
import { entrystore } from 'commons/store';
import useRestrictionDialog from 'commons/hooks/useRestrictionDialog';
import { useUserState } from 'commons/hooks/useUser';
import { hasUserPermission, PREMIUM, ADMIN_RIGHTS } from 'commons/util/user';
import useAsyncCallback from 'commons/hooks/useAsyncCallback';
import useAsync from 'commons/hooks/useAsync';
import useErrorHandler from 'commons/errors/hooks/useErrorHandler';
import UsersList from '../../lists/UsersList';

const NLS_BUNDLES = [escoSharingSettingsNLS, esadGroupNLS];

const ListSharingSettingsDialog = ({ entry, closeDialog, restrictionPath }) => {
  const [contextEntry] = useAsyncCallback(loadContextEntry, entry);
  const translate = useTranslation(NLS_BUNDLES);
  const { data: groupEntry, runAsync, status, error } = useAsync(null);
  useErrorHandler(error);
  const { userEntry } = useUserState();
  const isPremiumContext = contextEntry && isContextPremium(contextEntry);
  const isExemptFromRestrictions =
    hasUserPermission(userEntry, [ADMIN_RIGHTS, PREMIUM]) || isPremiumContext;
  const isRestricted =
    status === 'resolved' &&
    !isExemptFromRestrictions &&
    Boolean(restrictionPath);

  useRestrictionDialog({
    open: isRestricted,
    contentPath: restrictionPath,
    callback: closeDialog,
  });

  useEffect(() => {
    if (!contextEntry) return;

    const getGroupEntry = async () => {
      const refs = contextEntry.getReferrers(ns.expand('store:homeContext'));
      if (refs.length === 0) {
        throw new Error('Could not find home context referrer.');
      }
      if (refs.length > 1) {
        throw new Error('Found more than one home context referrer.');
      }
      return entrystore.getEntry(entrystore.getEntryURIFromURI(refs[0]));
    };

    runAsync(getGroupEntry());
  }, [contextEntry, runAsync]);

  if (isRestricted) return null;

  return (
    <ListActionDialog
      id="sharing-settings"
      closeDialog={closeDialog}
      title={translate('groupACL')}
      closeDialogButtonLabel={translate('closeButtonLabel')}
    >
      <ContentWrapper>
        <UsersList
          entry={entry}
          groupEntry={groupEntry}
          viewPlaceholderProps={{
            label: translate('emptyMessageMembers'),
          }}
        />
      </ContentWrapper>
    </ListActionDialog>
  );
};

ListSharingSettingsDialog.propTypes = {
  entry: PropTypes.instanceOf(Entry),
  closeDialog: PropTypes.func.isRequired,
  restrictionPath: PropTypes.string,
};

export default ListSharingSettingsDialog;
