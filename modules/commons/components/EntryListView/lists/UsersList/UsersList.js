import PropTypes from 'prop-types';
import { Entry } from '@entryscape/entrystore-js';
import { entrystore } from 'commons/store';
import { useUserState } from 'commons/hooks/useUser';
import { useState, useEffect, useCallback } from 'react';
import {
  withListModelProvider,
  ListItemAction,
  useListQuery,
} from 'commons/components/ListView';
import { useTranslation } from 'commons/hooks/useTranslation';
import {
  getUserRenderName,
  getIsUserDisabled,
  isManager as getIsManager,
  hasAdminRights as getHasAdminRights,
} from 'commons/util/user';
import {
  Button,
  Menu,
  MenuItem,
  ListItemIcon,
  ListItemText,
} from '@mui/material';
import {
  ArrowDropDown as ArrowDropDownIcon,
  ArrowDropUp as ArrowDropUpIcon,
  Security as SecurityIcon,
  Group as GroupIcon,
} from '@mui/icons-material';
import Tooltip from 'commons/components/common/Tooltip';
import useAsync from 'commons/hooks/useAsync';
import PersonAddDisabledIcon from '@mui/icons-material/PersonAddDisabled';
import LinkedDataBrowserDialog from 'commons/components/LinkedDataBrowserDialog';
import { LIST_ACTION_INFO } from 'commons/components/EntryListView/actions';
import { EntryListView } from '../../EntryListView';
import {
  TITLE_COLUMN,
  MODIFIED_COLUMN,
  ACTION_MENU_COLUMN,
  INFO_COLUMN,
} from '../../utils/columns';
import { listActions, rowActions, nlsBundles } from './actions';

const getUserEntries = async (groupEntry) => {
  groupEntry.setRefreshNeeded();
  await groupEntry.refresh();
  return groupEntry.getResource(true).getAllEntries();
};

/**
 *
 * @param {Entry} userEntry
 * @param {Entry} groupEntry
 * @returns {Promise}
 */
const makeManager = (userEntry, groupEntry) => {
  groupEntry.setRefreshNeeded();
  return groupEntry.refresh().then(() => {
    const grpEntryInfo = groupEntry.getEntryInfo();
    const acl = grpEntryInfo.getACL();
    acl.admin.push(userEntry.getResourceURI());
    grpEntryInfo.setACL(acl);

    return grpEntryInfo.commit().then(() => {
      const hcId = groupEntry.getResource(true).getHomeContext();
      if (typeof hcId !== 'undefined') {
        const homecontext = entrystore.getContextById(hcId);
        homecontext.getEntry().then((hcontextEntry) => {
          const hEntryInfo = hcontextEntry.getEntryInfo();
          const hACL = hEntryInfo.getACL();
          hACL.admin.push(userEntry.getResourceURI());
          hEntryInfo.setACL(hACL);
          return hEntryInfo.commit();
        });
      }
    });
  });
};

/**
 *
 * @param {Entry} userEntry
 * @param {Entry} groupEntry
 * @returns {Promise}
 */
const makeMember = (userEntry, groupEntry) => {
  groupEntry.setRefreshNeeded();
  return groupEntry.refresh().then(() => {
    const groupEntryInfo = groupEntry.getEntryInfo();
    const acl = groupEntryInfo.getACL();
    acl.admin.splice(acl.admin.indexOf(userEntry.getResourceURI()), 1);
    groupEntryInfo.setACL(acl);

    return groupEntryInfo.commit().then(() => {
      const hcId = groupEntry.getResource(true).getHomeContext();
      if (typeof hcId !== 'undefined') {
        const homecontext = entrystore.getContextById(hcId);
        homecontext.getEntry().then((hcontextEntry) => {
          const hEntryInfo = hcontextEntry.getEntryInfo();
          const hACL = hEntryInfo.getACL();
          hACL.admin.splice(hACL.admin.indexOf(userEntry.getResourceURI()), 1);
          hEntryInfo.setACL(hACL);
          return hEntryInfo.commit();
        });
      }
    });
  });
};

const ManageMemberOrManager = ({
  groupEntry,
  userEntry: liUserEntry,
  updateList,
  xs = 1,
}) => {
  const translate = useTranslation(nlsBundles);
  const [anchorEl, setAnchorEl] = useState(null);
  const { userEntry: currentUserEntry, userInfo: currentUserInfo } =
    useUserState();
  const { hasAdminRights } = currentUserInfo;
  const isCurrentUser =
    liUserEntry.getResourceURI() === currentUserEntry.getResourceURI();
  const currentUserIsManager = getIsManager(groupEntry, currentUserEntry);
  const isManager = getIsManager(groupEntry, liUserEntry);
  const isDisabled =
    (isCurrentUser && !hasAdminRights) ||
    !(currentUserIsManager || hasAdminRights);
  const iconButtonAriaLabel = isManager
    ? translate('managerTitle')
    : translate('memberTitle');

  const handleClick = (event) => setAnchorEl(event.currentTarget);
  const handleClose = (e) => {
    const selectedOption = e.currentTarget.dataset.value;

    let makeChangePromise;

    if (selectedOption) {
      // make a change only if a diff option from current selected
      if (isManager && selectedOption !== options[0].value) {
        makeChangePromise = makeMember(liUserEntry, groupEntry);
      } else if (!isManager && selectedOption !== options[1].value) {
        makeChangePromise = makeManager(liUserEntry, groupEntry);
      }
    }

    if (makeChangePromise) {
      makeChangePromise.then(updateList);
    }

    setAnchorEl(null);
  };

  let managerTooltip = translate('managerMenuTitle');
  if (isCurrentUser) {
    if (hasAdminRights) {
      managerTooltip = translate('specialManagerTitle');
    } else {
      managerTooltip = translate('managerDisabledTitle');
    }
  }
  const options = [
    { value: 'manager', label: translate('manager'), tooltip: managerTooltip },
    {
      value: 'member',
      label: translate('member'),
      tooltip: translate('memberTitle'),
    },
  ];

  return (
    <ListItemAction xs={xs}>
      <Button
        aria-label={iconButtonAriaLabel}
        aria-controls="select-acl-user-menu"
        aria-haspopup="true"
        onClick={handleClick}
        disabled={isDisabled}
        endIcon={
          anchorEl ? (
            <ArrowDropUpIcon color="secondary" />
          ) : (
            <ArrowDropDownIcon color="secondary" />
          )
        }
        variant="text"
      >
        {isManager ? (
          <SecurityIcon fontSize="small" />
        ) : (
          <GroupIcon fontSize="small" />
        )}
      </Button>
      <Menu
        id="select-acl-user-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        {options.map(({ value, label, tooltip }) => (
          <Tooltip title={tooltip} placement="right" key={`tooltip-${value}`}>
            <MenuItem
              key={value}
              data-value={value}
              selected={
                (isManager && value === 'manager') ||
                (!isManager && value === 'member')
              }
              onClick={handleClose}
            >
              <ListItemIcon>
                {value === 'manager' ? (
                  <SecurityIcon fontSize="small" />
                ) : (
                  <GroupIcon fontSize="small" />
                )}
              </ListItemIcon>
              <ListItemText primary={label} />
            </MenuItem>
          </Tooltip>
        ))}
      </Menu>
    </ListItemAction>
  );
};

ManageMemberOrManager.propTypes = {
  groupEntry: PropTypes.instanceOf(Entry),
  userEntry: PropTypes.instanceOf(Entry),
  updateList: PropTypes.func,
  xs: PropTypes.number,
};

const UsersList = ({ groupEntry, messages = {}, ...props }) => {
  const { data: userItems, status, runAsync } = useAsync();
  const { result: userItemsInPage, size } = useListQuery({
    items: userItems,
  });
  const translate = useTranslation(nlsBundles);
  const { userEntry } = useUserState();
  const isManager = groupEntry ? getIsManager(groupEntry, userEntry) : false;
  const canManage = getHasAdminRights(userEntry) || isManager;

  const updateList = useCallback(() => {
    if (!groupEntry) return;

    runAsync(
      getUserEntries(groupEntry).then((userEntries) =>
        userEntries.map((entry) => ({ entry }))
      )
    );
  }, [groupEntry, runAsync]);

  useEffect(() => {
    updateList();
  }, [updateList]);

  return (
    <EntryListView
      size={size}
      status={groupEntry && status !== 'idle' ? status : 'pending'}
      entries={userItemsInPage.map(({ entry }) => entry)}
      nlsBundles={nlsBundles}
      includeDefaultListActions={false}
      listActions={listActions}
      listActionsProps={{
        groupEntry,
        updateList,
        ignoreEntries: userItems?.map(({ entry }) => entry) || [],
        disabled: !canManage,
      }}
      columns={[
        {
          Component: ManageMemberOrManager,
          xs: 1,
          id: 'manage-member',
          getProps: ({ entry }) => ({
            userEntry: entry,
            groupEntry,
            updateList,
          }),
        },
        {
          ...TITLE_COLUMN,
          xs: 7,
          headerNlsKey: 'listHeaderUser',
          direction: 'row',
          getProps: ({ entry }) => ({
            primary: getUserRenderName(entry),
            secondary: getIsUserDisabled(entry) ? (
              <PersonAddDisabledIcon
                fontSize="small"
                title={translate('userStatusDisabled')}
              />
            ) : null,
          }),
        },
        MODIFIED_COLUMN,
        {
          ...INFO_COLUMN,
          getProps: ({ entry, translate: t }) => ({
            action: {
              ...LIST_ACTION_INFO,
              entry,
              nlsBundles,
              Dialog: LinkedDataBrowserDialog,
              formTemplateId: 'esc:User',
            },
            title: t('infoEntry'),
          }),
        },
        {
          ...ACTION_MENU_COLUMN,
          actions: rowActions,
          getProps: ({ entry }) => ({
            entry,
            groupEntry,
            updateList,
            ...messages,
          }),
        },
      ]}
      {...props}
    />
  );
};

UsersList.propTypes = {
  groupEntry: PropTypes.instanceOf(Entry),
  messages: PropTypes.shape({}),
};

export default withListModelProvider(UsersList);
