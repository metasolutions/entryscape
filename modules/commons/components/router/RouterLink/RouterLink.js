import { forwardRef } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

const RouterLink = forwardRef(({ to, ...props }, ref) => (
  <Link ref={ref} to={to} {...props} />
));

RouterLink.propTypes = {
  to: PropTypes.string,
};

export default RouterLink;
