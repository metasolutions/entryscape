import PropTypes from 'prop-types';
import { useEffect } from 'react';
import { Entry } from '@entryscape/entrystore-js';
import {
  ListItemOpenMenuButton,
  List,
  ListItem,
  ActionsMenu,
  ListItemText,
  ListView,
  withListModelProvider,
  ListItemActionsGroup,
} from 'commons/components/ListView';
import { getIconFromActionId } from 'commons/actions';
import { useTranslation } from 'commons/hooks/useTranslation';
import escaDatasetNLS from 'catalog/nls/escaDataset.nls';
import escaFilesNLS from 'catalog/nls/escaFiles.nls';
import escaManageFilesNLS from 'catalog/nls/escaManageFiles.nls';
import escoListNLS from 'commons/nls/escoList.nls';
import { getModifiedDate } from 'commons/util/metadata';
import { getShortDate } from 'commons/util/date';
import useAsync from 'commons/hooks/useAsync';
import { OverviewListHeader } from 'commons/components/overview/OverviewListHeader';
import { OverviewListPlaceholder } from 'commons/components/overview';
import LinkedDataBrowserDialog from 'commons/components/LinkedDataBrowserDialog';
import {
  LIST_ACTION_EDIT,
  LIST_ACTION_INFO,
} from 'commons/components/EntryListView/actions';
import {
  getDistributionFormat,
  getDistributionTitle,
  getAPIDistributionSourceURIs,
} from '../utils/distributions';
import CreateDistributionDialog from '../dialogs/CreateDistributionDialog';
import EditDistributionDialog from './dialogs/EditDistributionDialog';
import useDistributions from '../hooks/useDistributions';
import { rowActions, presenterAction } from './actions';

const NLS_BUNDLES = [
  escaDatasetNLS,
  escaManageFilesNLS,
  escaFilesNLS,
  escoListNLS,
];

const DistributionList = ({ entry: datasetEntry }) => {
  const { distributions, refreshDistributions, status } =
    useDistributions(datasetEntry);
  const { data: apiDistributionResourceURIs, runAsync } = useAsync({
    data: [],
  });
  const translate = useTranslation([escoListNLS, escaDatasetNLS]);

  useEffect(() => {
    runAsync(getAPIDistributionSourceURIs(datasetEntry));
  }, [datasetEntry, runAsync, distributions]);

  return (
    <>
      <OverviewListHeader
        heading={translate('distributionsTitle')}
        nlsBundles={escaDatasetNLS}
        action={{
          Dialog: CreateDistributionDialog,
          tooltip: translate('addDistributionTitle'),
          highlightId: 'distributionsListCreate',
        }}
        actionParams={{ datasetEntry, refreshDistributions }}
        highlightId="distributionsListHeader"
      />
      <ListView
        size={distributions?.length ?? -1}
        nlsBundles={NLS_BUNDLES}
        renderPlaceholder={() => (
          <OverviewListPlaceholder
            label={translate('distributionsListPlaceholder')}
          />
        )}
        status={status}
      >
        <List renderLoader={(Loader) => <Loader height="10vh" />}>
          {distributions &&
            distributions.map((distributionEntry) => {
              const apiDistributionEntry = distributions.find(
                (dist) =>
                  dist
                    .getMetadata()
                    .findFirstValue(dist.getResourceURI(), 'dcterms:source') ===
                  distributionEntry.getResourceURI()
              );
              const actionParams = {
                apiDistributionResourceURIs,
                refreshDistributions,
                datasetEntry,
                apiDistributionEntry,
                nlsBundles: NLS_BUNDLES,
              };

              const actionsMenuItems = rowActions
                .filter(({ isVisible }) =>
                  isVisible
                    ? isVisible({
                        entry: distributionEntry,
                        ...actionParams,
                      })
                    : true
                )
                .map((rowAction) => ({
                  label: translate(rowAction.labelNlsKey),
                  icon: getIconFromActionId(rowAction.id),
                  action: {
                    ...rowAction,
                    entry: distributionEntry,
                    ...actionParams,
                  },
                }));

              return (
                <ListItem key={distributionEntry.getId()}>
                  <ListItemText
                    xs={4}
                    primary={getDistributionTitle(distributionEntry, translate)}
                  />
                  <ListItemText
                    xs={3}
                    secondary={getDistributionFormat(distributionEntry)}
                  />
                  <ListItemText
                    xs={2}
                    secondary={getShortDate(getModifiedDate(distributionEntry))}
                  />
                  <ListItemActionsGroup
                    xs={2}
                    justifyContent="end"
                    actions={[
                      {
                        ...LIST_ACTION_INFO,
                        Dialog: LinkedDataBrowserDialog,
                        getProps: () => ({
                          ...presenterAction,
                          entry: distributionEntry,
                          parentEntry: datasetEntry,
                          title: translate(presenterAction.labelNlsKey),
                        }),
                      },
                      {
                        ...LIST_ACTION_EDIT,
                        Dialog: EditDistributionDialog,
                        getProps: () => ({
                          entry: distributionEntry,
                          datasetEntry,
                          refreshDistributions,
                          title: translate('editEntry'),
                        }),
                      },
                    ]}
                  />
                  <ListItemOpenMenuButton />
                  <ActionsMenu items={actionsMenuItems} />
                </ListItem>
              );
            })}
        </List>
      </ListView>
    </>
  );
};
DistributionList.propTypes = {
  entry: PropTypes.instanceOf(Entry),
};

export default withListModelProvider(DistributionList);
