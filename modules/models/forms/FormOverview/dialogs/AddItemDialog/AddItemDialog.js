import * as ns from 'models/utils/ns';
import React, { useCallback } from 'react';
import PropTypes from 'prop-types';
import { Entry } from '@entryscape/entrystore-js';
import { entrystore } from 'commons/store';
import { useESContext } from 'commons/hooks/useESContext';
import esmoFormsNLS from 'models/nls/esmoForms.nls';
import esmoCommonsNLS from 'models/nls/esmoCommons.nls';
import escoListNLS from 'commons/nls/escoList.nls';
import escoDialogsNLS from 'commons/nls/escoDialogs.nls';
import { getPurpose } from 'models/utils/metadata';
import {
  STATUS_FILTER,
  MODEL_FILTER,
  getModelItems,
} from 'models/utils/filterDefinitions';
import { FIELDS_FORMS_INFO } from 'models/utils/fieldDefinitions';
import { getLabel } from 'commons/util/rdfUtils';
import { SelectEntryDialog } from 'commons/components/forms/dialogs/SelectEntryDialog';
import { TYPE_FILTER } from './actions';

const NLS_BUNDLES = [esmoFormsNLS, esmoCommonsNLS, escoListNLS, escoDialogsNLS];
const DEFAULT_RDF_TYPES = [
  ns.RDF_TYPE_FIELD,
  ns.RDF_TYPE_PROPERTY_FORM,
  ns.RDF_TYPE_SECTION_FORM,
];

const AddFormItemDialog = ({
  closeDialog,
  selectedEntries,
  onSelect,
  entry,
}) => {
  const { context } = useESContext();

  const filters = [
    {
      ...MODEL_FILTER,
      initialSelection: context.getId(),
      loadItems: () => getModelItems(context.getId()),
    },
    STATUS_FILTER,
    TYPE_FILTER,
  ];

  const createQuery = useCallback(() => {
    const query = entrystore.newSolrQuery().rdfType(DEFAULT_RDF_TYPES);
    query.uri(entry.getURI(), true); // ignore the form entry itself
    return query;
  }, [entry]);

  return (
    <SelectEntryDialog
      closeDialog={closeDialog}
      titleNlsKey="addItems"
      filters={filters}
      createQuery={createQuery}
      selectedEntry={selectedEntries}
      onSelect={onSelect}
      nlsBundles={NLS_BUNDLES}
      infoFields={FIELDS_FORMS_INFO}
      getTitleProps={({ entry: rowEntry }) => ({
        primary: getLabel(rowEntry),
        secondary: getPurpose({ entry: rowEntry }),
        maxSecondaryLength: 50,
      })}
    />
  );
};

AddFormItemDialog.propTypes = {
  closeDialog: PropTypes.func.isRequired,
  entry: PropTypes.instanceOf(Entry),
  selectedEntries: PropTypes.arrayOf(PropTypes.instanceOf(Entry)),
  onSelect: PropTypes.func,
};

export default AddFormItemDialog;
