import { createContext, useContext } from 'react';

export const ListViewContext = createContext();

// Hook to read common props provided to the list view.
export const useListView = () => {
  const context = useContext(ListViewContext);
  if (context === undefined) {
    throw new Error('List view components must have a list view provider');
  }
  return context;
};
