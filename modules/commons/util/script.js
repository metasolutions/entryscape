/**
 * Add script tag as last child of document.head
 * @param url
 */
const affixScriptToHead = (url) => {
  const newScript = document.createElement('script');
  document.head.appendChild(newScript);
  newScript.src = url;
};

/**
 * Convert any extra script configuration into an array of relative URLs
 * @return {string[]}
 * @todo remove boolean and string option
 * @todo getBaseUrl instead of /theme...
 */
const getExtrasScriptURLs = (extrasConfig) => {
  let extras = [];
  if (Array.isArray(extrasConfig)) {
    extras = extrasConfig.map(
      (extra) =>
        `/theme/extras/${extra.endsWith('.js') ? extra : `${extra}.js`}`
    );
  } else if (extrasConfig === true) {
    extras = ['/theme/extras.js'];
  } else if (typeof extrasConfig === 'string') {
    extras = [
      `/theme/extras/${
        extrasConfig.endsWith('.js') ? extrasConfig : `${extrasConfig}.js`
      }`,
    ];
  }

  return extras;
};

/**
 * Load all extra scripts if there are any
 *
 * @param extrasConfig
 */
const loadExtras = (extrasConfig) =>
  getExtrasScriptURLs(extrasConfig).forEach(affixScriptToHead);

export { loadExtras, affixScriptToHead };
