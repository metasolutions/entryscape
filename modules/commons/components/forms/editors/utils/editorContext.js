import { FieldGroup } from './FieldGroup';

/**
 * Find field groups in a field group set by matching resource uris
 *
 * @param {Set} fieldSet
 * @param {string} resourceURI
 * @returns {FieldGroup[]}
 */
export const findGroupsByResourceURI = (fieldSet, resourceURI) => {
  const fieldGroups = [];
  fieldSet.forEach((fieldGroup) => {
    if (fieldGroup.getResourceURI() === resourceURI) {
      fieldGroups.push(fieldGroup);
    }
  });
  return fieldGroups;
};

/**
 *
 * @param {Set} fieldSet
 * @param {string} property
 * @returns {FieldGroup|undefined}
 */
export const findGroupByProperty = (fieldSet, property) => {
  return Array.from(fieldSet).find((fieldGroup) => {
    return fieldGroup.getProperty() === property;
  });
};
