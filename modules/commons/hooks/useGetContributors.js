import useAsyncCallback from 'commons/hooks/useAsyncCallback';
import { getLabel } from 'commons/util/rdfUtils';
import { getContributors } from 'commons/util/metadata';
import escoOverviewNLS from 'commons/nls/escoOverview.nls';
import { useTranslation } from 'commons/hooks/useTranslation';

const useGetContributors = (entry, refreshCount) => {
  const translate = useTranslation(escoOverviewNLS);
  const [contributors, { error, isLoading }] = useAsyncCallback(
    getContributors,
    entry,
    null,
    refreshCount
  );
  if (!contributors) return { contributors: [], isLoading, error };

  const contributorsNames = contributors
    .map((contributor) =>
      (contributor
        ? getLabel(contributor)
        : translate('editedByContributorNotFound')
      ).trim()
    )
    .filter(Boolean);

  return { contributors: [...new Set(contributorsNames)], isLoading, error };
};

export default useGetContributors;
