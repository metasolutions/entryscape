import PropTypes from 'prop-types';
import { useState, useCallback, useMemo, forwardRef, Fragment } from 'react';
import { Link, NavLink, useLocation } from 'react-router-dom';
import useNavigate from 'commons/components/router/useNavigate';
import {
  AccountCircle as AccountCircleIcon,
  ExitToApp as SignOutIcon,
  Menu as MenuIcon,
  Tune as SettingsIcon,
  OpenInNew as OpenInNewIcon,
  Help as HelpIcon,
} from '@mui/icons-material';
import {
  AppBar,
  Badge,
  Divider,
  Drawer,
  IconButton,
  Link as MuiLink,
  List,
  ListItem,
  ListItemButton,
  Menu,
  MenuItem,
  Toolbar,
  Typography,
  Grid,
} from '@mui/material';
import { useUserState, useUserDispatch } from 'commons/hooks/useUser';
import { AUTH_LOGOUT_SUCCESS } from 'commons/hooks/user/actions';
import UserSettingsDialog from 'commons/auth/UserSettingsDialog';
import { logout } from 'commons/util/user';
import { USER_ENTRY_ID_GUEST } from 'commons/util/userIds';
import {
  getActiveModules,
  getAppVersion,
  getTermsAndConditionsParams,
} from 'commons/util/config';
import { isUri } from 'commons/util/util';
import { getModuleIconFromName } from 'commons/util/module';
import {
  getRouteFromView,
  getViewDefFromRoute,
  getModuleOfView,
  getPathFromViewName,
} from 'commons/util/site';
import LanguageSelect from 'commons/components/common/LanguageSelect';
import MainDialog from 'commons/components/common/dialogs/MainDialog';
import Logo from 'commons/nav/components/Logo';
import escoLayoutNLS from 'commons/nls/escoLayout.nls';
import { i18n } from 'esi18n';
import { localize } from 'commons/locale';
import config from 'config';
import 'commons/app.scss';
import { useTranslation, nlsPropType } from 'commons/hooks/useTranslation';
import PrivacyDialog from 'commons/auth/PrivacyDialog';
import PlannedMaintenance from 'commons/errors/PlannedMaintenance';
import Icon from 'commons/components/Icon';
import registry from 'commons/registry';
import { usePrimaryNavContext } from './usePrimaryNavContext';
import Main from './Main';
import './MainLayout.scss';
import useReleaseNotification from './useReleaseNotification';
import HelpDrawer from './HelpDrawer';

const RELEASE_INFO_URL = 'https://entryscape.com/release/';

const UserMenu = ({
  anchorElement,
  setAnchorElement,
  displayName,
  setUserSettingsDialogOpen,
  updateReleaseInfoVersion,
  hasUnreadReleaseInfo,
}) => {
  const t = useTranslation(escoLayoutNLS);
  const { navigate } = useNavigate();
  const userDispatch = useUserDispatch();
  const version = getAppVersion();
  const appName = config.get('theme.appName') || 'EntryScape';
  const { url: termsConditionsURL } = getTermsAndConditionsParams();
  const userMenuOpen = Boolean(anchorElement);
  const {
    enabled: supportEnabled,
    url: supportURL,
    label: supportLabel,
  } = config.get('entryscape.support');
  const {
    enabled: communityEnabled,
    url: communityURL,
    label: communityLabel,
  } = config.get('entryscape.community');

  const handleUserMenuClose = () => {
    setAnchorElement(null);
  };

  const handleLogout = useCallback(
    () =>
      logout().then(
        ({ userEntry: guestUserEntry, userInfo: guestUserInfo }) => {
          userDispatch({
            type: AUTH_LOGOUT_SUCCESS,
            data: {
              // TODO move to reducer?
              userEntry: guestUserEntry,
              userInfo: guestUserInfo,
            },
          });
          navigate(getPathFromViewName('start'));
        }
      ),
    [navigate, userDispatch]
  );

  return (
    <Menu
      id="menu-appbar"
      anchorEl={anchorElement}
      keepMounted
      transformOrigin={{
        vertical: 'top',
        horizontal: 'right',
      }}
      open={userMenuOpen}
      onClose={handleUserMenuClose}
      PopoverClasses={{ paper: 'escoToolbarMenu__Popover' }}
    >
      <MenuItem
        classes={{
          root: 'escoToolbarMenu__Item escoToolbarMenu__Item--large',
        }}
      >
        {displayName}
      </MenuItem>
      <Divider />
      <MenuItem
        classes={{ root: 'escoToolbarMenu__Item' }}
        onClick={() => {
          setUserSettingsDialogOpen(true);
          handleUserMenuClose();
        }}
      >
        <SettingsIcon className="escoToolbarMenu__Icon" />
        {t('settingsLabel')}
      </MenuItem>
      <MenuItem
        classes={{ root: 'escoToolbarMenu__Item' }}
        onClick={handleLogout}
      >
        <SignOutIcon className="escoToolbarMenu__Icon" />
        {t('signOutLabel')}
      </MenuItem>
      {termsConditionsURL || supportEnabled || communityEnabled ? (
        <Divider />
      ) : null}
      {communityEnabled ? (
        <MenuItem
          classes={{
            root: 'escoToolbarMenu__Item escoToolbarMenu__Item--gray',
          }}
          component="a"
          href={communityURL}
          target="_blank"
          onClick={handleUserMenuClose}
        >
          {localize(communityLabel)}
          <OpenInNewIcon className="escoToolbarMenu__linkIcon" />
        </MenuItem>
      ) : null}
      {supportEnabled ? (
        <MenuItem
          classes={{
            root: 'escoToolbarMenu__Item escoToolbarMenu__Item--gray',
          }}
          component="a"
          href={supportURL}
          target="_blank"
          onClick={handleUserMenuClose}
        >
          {localize(supportLabel)}
          <OpenInNewIcon className="escoToolbarMenu__linkIcon" />
        </MenuItem>
      ) : null}
      {termsConditionsURL ? (
        <MenuItem
          classes={{
            root: 'escoToolbarMenu__Item escoToolbarMenu__Item--gray',
          }}
          component="a"
          href={termsConditionsURL}
          target="_blank"
          rel="noopener"
          onClick={handleUserMenuClose}
        >
          {t('userMenuTermsLabel')}
          <OpenInNewIcon className="escoToolbarMenu__linkIcon" />
        </MenuItem>
      ) : null}
      <Divider />
      <MenuItem
        classes={{
          root: hasUnreadReleaseInfo
            ? 'escoToolbarMenu__Item escoToolbarMenu__Item--selected'
            : 'escoToolbarMenu__Item',
        }}
        component="a"
        href={RELEASE_INFO_URL}
        onClick={async () => {
          handleUserMenuClose();
          await updateReleaseInfoVersion();
        }}
        target="_blank"
        rel="noopener"
        selected={hasUnreadReleaseInfo}
      >
        <span className="escoToolbarMenu__label">
          {t('releaseInfoLabel')}
          <OpenInNewIcon className="escoToolbarMenu__linkIcon" />
        </span>
        <Badge
          invisible={!hasUnreadReleaseInfo}
          color="primary"
          variant="dot"
          classes={{ badge: 'escoUserMenu__badge' }}
        />
      </MenuItem>
      <MenuItem
        disabled
        classes={{
          root: 'escoToolbarMenu__Item escoToolbarMenu__Item--gray escoToolbarMenu__Item--opaque',
        }}
      >
        {`${appName} ${version}`}
      </MenuItem>
    </Menu>
  );
};

UserMenu.propTypes = {
  anchorElement: PropTypes.shape({}),
  setAnchorElement: PropTypes.func,
  displayName: PropTypes.string,
  setUserSettingsDialogOpen: PropTypes.func,
  updateReleaseInfoVersion: PropTypes.func,
  hasUnreadReleaseInfo: PropTypes.bool,
};

const ListItemLink = ({ isActive, to, ...props }) => {
  const CustomLink = useMemo(
    () =>
      forwardRef(({ className, ...linkProps }, ref) => (
        <NavLink
          className={() =>
            `${className}${
              isActive ? ' escoPrimaryNav__ListItemLink--active' : ''
            }`
          }
          ref={ref}
          to={to}
          {...linkProps}
        />
      )),
    [to, isActive]
  );
  return <ListItemButton component={CustomLink} {...props} />;
};
ListItemLink.propTypes = {
  isActive: PropTypes.bool,
  to: PropTypes.string,
  className: PropTypes.string,
};

const ListItemLinkWrapper = ({ isExternal, isActive, route, children }) =>
  isExternal ? (
    <MuiLink
      href={route}
      classes={{ root: 'escoPrimaryNav__ListItemLink' }}
      target="_blank"
    >
      {children}
    </MuiLink>
  ) : (
    <ListItemLink
      classes={{ root: 'escoPrimaryNav__ListItemLink' }}
      to={route}
      isActive={isActive}
    >
      {children}
    </ListItemLink>
  );
ListItemLinkWrapper.propTypes = {
  isExternal: PropTypes.bool,
  isActive: PropTypes.bool,
  route: PropTypes.string,
  children: PropTypes.node,
};

/**
 *
 * @param {string} href
 * @param {object} location
 * @returns {boolean}
 */
const isActive = (href, location) => {
  const locationPath = location.pathname.split('/')[1];
  const hrefPath = href.split('/')[1];
  return locationPath === hrefPath;
};

/**
 *
 * @param {boolean} primaryNavOpen
 * @param {boolean} helpDrawerOpen
 * @returns {string}
 */
const getAppBarClasses = (primaryNavOpen, helpDrawerOpen) => {
  if (primaryNavOpen && helpDrawerOpen)
    return 'escoAppBar escoAppBar--shiftedBoth';
  if (primaryNavOpen) return 'escoAppBar escoAppBar--shifted';
  if (helpDrawerOpen) return 'escoAppBar escoAppBar--shiftedRight';
  return 'escoAppBar';
};

export const MainLayout = ({
  children,
  route: matchedRoute,
  viewName,
  secondaryNavViews = [],
  error,
}) => {
  const t = useTranslation(escoLayoutNLS);
  const location = useLocation();
  const { primaryNavOpen, setPrimaryNavOpen } = usePrimaryNavContext();

  const [userMenuAnchorElement, setUserMenuAnchorElement] = useState(null);
  const { userEntry, userInfo } = useUserState();
  const { displayName: userDisplayName } = userInfo;
  const modules = getActiveModules(userInfo);
  const isGuest = userEntry.getId() === USER_ENTRY_ID_GUEST;
  const app = registry.getApp();
  const { updateReleaseInfoVersion, hasUnreadReleaseInfo } =
    useReleaseNotification(userEntry);

  // User settings dialog
  const [userSettingsDialogOpen, setUserSettingsDialogOpen] = useState(false);
  const closeDialog = () => setUserSettingsDialogOpen(false);

  const viewDef = getViewDefFromRoute(matchedRoute);
  const module = getModuleOfView(viewDef);

  const hasSecondaryNavigation = secondaryNavViews.length > 1;
  const externalLinks = config.get('theme.externalLinks');

  const includeHelp = config.get('includeHelp');
  const [helpDrawerOpen, setHelpDrawerOpen] = useState(false);
  const toggleHelpDrawer = () =>
    setHelpDrawerOpen((previousOpen) => !previousOpen);
  const { helpLinks } = viewDef;

  return (
    <>
      <AppBar
        position="fixed"
        elevation={0}
        classes={{ root: getAppBarClasses(primaryNavOpen, helpDrawerOpen) }}
      >
        <MuiLink className="escoSkipLink" href="#main-content">
          {t('skipLinkLabel')}
        </MuiLink>
        {hasSecondaryNavigation ? (
          <MuiLink className="escoSkipLink" href="#secondary-navigation">
            {t('skipToSecondaryNavLabel')}
          </MuiLink>
        ) : null}
        <Toolbar classes={{ root: 'escoToolbar' }}>
          <div className="escoToolbarHeader">
            <div className="escoToolbarHeader__Menu">
              <IconButton
                color="inherit"
                aria-label={t('drawerToggleAriaLabel')}
                onClick={() => setPrimaryNavOpen(!primaryNavOpen)}
                edge="start"
              >
                <MenuIcon />
              </IconButton>
              {module && (
                <Icon aria-label={`${module.name} start page`}>
                  {getModuleIconFromName(module.name, module.icon)}
                </Icon>
              )}
              {viewName && (
                <Typography variant="h1" className="escoH1--small" noWrap>
                  {viewName}
                </Typography>
              )}
            </div>
            <div>
              <PlannedMaintenance />

              {helpLinks?.length && includeHelp ? (
                <IconButton
                  aria-label={t('helpAriaLabel')}
                  onClick={toggleHelpDrawer}
                >
                  <HelpIcon classes={{ root: 'escoToolbar__Icon--large' }} />
                </IconButton>
              ) : null}
              {!isGuest ? (
                <>
                  <IconButton
                    aria-label={t('accountButtonAriaLabel')}
                    aria-controls="menu-appbar"
                    aria-haspopup="true"
                    onClick={({ currentTarget }) => {
                      setUserMenuAnchorElement(currentTarget);
                    }}
                    color="inherit"
                    classes={{ root: 'escoToolbar__Button--right' }}
                  >
                    <Badge
                      invisible={!hasUnreadReleaseInfo}
                      color="primary"
                      overlap="circular"
                      variant="dot"
                      classes={{ badge: 'escoUserMenu__badge' }}
                    >
                      <AccountCircleIcon
                        classes={{
                          root: 'escoToolbar__Icon--large escoToolbar__Icon--orange',
                        }}
                      />
                    </Badge>
                  </IconButton>
                  <UserMenu
                    anchorElement={userMenuAnchorElement}
                    setAnchorElement={setUserMenuAnchorElement}
                    displayName={userDisplayName}
                    setUserSettingsDialogOpen={setUserSettingsDialogOpen}
                    updateReleaseInfoVersion={updateReleaseInfoVersion}
                    hasUnreadReleaseInfo={hasUnreadReleaseInfo}
                  />
                </>
              ) : null}
            </div>
          </div>
          {isGuest ? (
            <Grid container spacing={2}>
              <Grid item xs={10} sm={9} />
              <Grid item xs={2} sm={3}>
                <LanguageSelect />
              </Grid>
            </Grid>
          ) : null}
        </Toolbar>
      </AppBar>

      <Drawer
        variant="persistent"
        anchor="left"
        open={primaryNavOpen}
        PaperProps={{
          classes: { root: 'escoDrawer' },
        }}
        transitionDuration={{ enter: 305, exit: 275 }} // {{ enter: 225, exit: 195 }}
      >
        <div className="escoDrawer__brand">
          <Link to="/" title={t('goHomeLink')}>
            <Logo type="icon" className="escoDrawer__logo" />
          </Link>
        </div>

        {/* PrimaryNav */}
        {!isGuest || app === 'registry' ? (
          <List
            component="nav"
            disablePadding
            aria-label={t('primaryNavigation')}
            className="escoDrawer__navigation"
          >
            {modules.map(
              ({ name, icon, startView, title = {}, productName }) => {
                const route = getRouteFromView(startView);
                const isExternal = isUri(route);
                return (
                  <ListItemLinkWrapper
                    key={name}
                    route={route}
                    isExternal={isExternal}
                    isActive={!isExternal && isActive(route, location)}
                  >
                    <div className="escoPrimaryNav__Icon">
                      {getModuleIconFromName(name, icon)}
                      {isExternal && (
                        <OpenInNewIcon className="escoPrimaryNav__ListItemLinkOutIcon" />
                      )}
                    </div>
                    <div className="escoPrimaryNav__Text">
                      {localize(productName || title)}
                    </div>
                  </ListItemLinkWrapper>
                );
              }
            )}
          </List>
        ) : null}

        {externalLinks && (
          <List className="escoExternalLinks" disablePadding>
            {externalLinks.map(({ uri, label }, index) => {
              const locale = i18n.getLocale();
              const defaultLocale = config.get('locale.fallback');
              const localizedLabel =
                typeof label === 'string'
                  ? label
                  : label[locale] || label[defaultLocale];
              const localizedURI =
                typeof uri === 'string'
                  ? uri
                  : uri[locale] || uri[defaultLocale];

              return (
                <Fragment key={`${localizedURI}${localizedLabel}`}>
                  <ListItem
                    className="escoExternalLink"
                    component={MuiLink}
                    href={localizedURI}
                    target="_blank"
                  >
                    {localizedLabel}
                  </ListItem>
                  {index !== externalLinks.length - 1 && (
                    <Divider
                      className="escoExternalLinks__Divider"
                      variant="middle"
                    />
                  )}
                </Fragment>
              );
            })}
          </List>
        )}
      </Drawer>
      {includeHelp ? (
        <HelpDrawer
          open={helpDrawerOpen}
          toggle={toggleHelpDrawer}
          helpLinks={helpLinks}
        />
      ) : null}
      <Main
        error={error}
        hasSecondaryNavigation={hasSecondaryNavigation}
        secondaryNavViews={secondaryNavViews}
        location={location}
        viewDef={viewDef}
        userEntry={userEntry}
        matchedRoute={matchedRoute}
        primaryNavOpen={primaryNavOpen}
        helpDrawerOpen={helpDrawerOpen}
        translate={t}
      >
        {children}
      </Main>

      <div id="entryscape_dialogs" className="entryscape">
        <MainDialog />
      </div>
      <PrivacyDialog userEntry={userEntry} />
      {userSettingsDialogOpen ? (
        <UserSettingsDialog closeDialog={closeDialog} />
      ) : null}
    </>
  );
};

MainLayout.propTypes = {
  children: PropTypes.node,
  route: PropTypes.string,
  viewName: PropTypes.string,
  error: PropTypes.shape({}),
  secondaryNavViews: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string,
      title: nlsPropType,
      route: PropTypes.string,
    })
  ),
};
