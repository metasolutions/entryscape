const path = require('path');

// 94867: 'src/module/commons/nls/',
// 94869: 'src/module/catalog/nls/',
// 94871: 'src/app/registry/nls/',
// 94873: 'src/module/terms/nls/',
// 94875: 'src/module/workbench/nls/',
// 94877: 'src/module/admin/nls/',

module.exports = {
  languages: ['sv', 'de', 'en'],
  projects: [
    {
      id: '94867',
      paths: [path.resolve(path.join('modules', 'commons', 'nls'))],
    },
    {
      id: '94869',
      paths: [path.resolve(path.join('modules', 'catalog', 'nls'))],
    },
    {
      id: '94873',
      paths: [path.resolve(path.join('modules', 'terms', 'nls'))],
    },
    {
      id: '94875',
      paths: [path.resolve(path.join('modules', 'workbench', 'nls'))],
    },
    {
      id: '94877',
      paths: [path.resolve(path.join('modules', 'admin', 'nls'))],
    },
    {
      id: '94871',
      paths: [
        path.resolve(path.join('modules', 'toolkit', 'nls')),
        path.resolve(path.join('modules', 'harvest', 'nls')),
        path.resolve(path.join('modules', 'status', 'nls')),
      ],
    },
    {
      id: '537153',
      paths: [path.resolve(path.join('modules', 'models', 'nls'))],
    },
    {
      id: '740124',
      paths: [path.resolve(path.join('modules', 'search', 'nls'))],
    },
  ],
};
