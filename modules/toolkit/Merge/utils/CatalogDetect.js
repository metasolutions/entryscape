const types = [
  {
    type: 'ca', // catalog
    values: ['http://www.w3.org/ns/dcat#Catalog'],
  },
  {
    type: 'da', // dataset
    values: ['http://www.w3.org/ns/dcat#Dataset'],
  },
  {
    type: 'di', // distribution
    values: ['http://www.w3.org/ns/dcat#Distribution'],
  },
  {
    type: 'co', // contact
    values: [
      'http://xmlns.com/foaf/0.1/Agent',
      'http://xmlns.com/foaf/0.1/Person',
      'http://xmlns.com/foaf/0.1/Organization',
    ],
  },
  {
    type: 'pu', // publisher
    values: [
      'http://www.w3.org/2006/vcard/ns#Kind',
      'http://www.w3.org/2006/vcard/ns#Individual',
      'http://www.w3.org/2006/vcard/ns#Organization',
    ],
  },
];

const initialValue = {
  ca: 0,
  da: 0,
  di: 0,
  co: 0,
  pu: 0,
};

/**
 *
 * @param {rdfjson} catalog
 * @return {Object|undefined} countByType
 */
export const getCountByType = (catalog) => {
  if (!catalog) return undefined;

  const statements = catalog.find(null, 'rdf:type') || [];

  const countByType = statements.reduce(
    (counter, statement) => {
      const value = statement.getValue();
      const typeMatch = types.find(({ values }) => values.includes(value));
      if (!typeMatch) {
        return counter;
      }
      const { type } = typeMatch;
      return {
        ...counter,
        [type]: counter[type] + 1,
      };
    },
    { ...initialValue }
  );

  return countByType;
};

/**
 *
 * @param {Object} countByType
 * @return {[string, Object]} error
 */
export const validateCount = ({ ca, ...otherCounts }) => {
  if (ca > 1) return ['catalogError', ca];

  const hasSomethingToMerge = Object.keys(otherCounts).some(
    (type) => otherCounts[type]
  );
  if (!hasSomethingToMerge) return ['nothingToMergeError'];

  return undefined;
};

/**
 *
 * @param {rdfjson} catalog
 * @param {number} id
 * @return {Object} catalogDetect
 */
export const CatalogDetect = (catalog, id) => {
  const countByType = getCountByType(catalog);

  return {
    id,
    getCountByType: () => countByType,
    getCatalog: () => catalog,
    validateCount: () => validateCount(countByType),
  };
};
