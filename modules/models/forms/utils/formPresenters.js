import * as fields from 'models/utils/fieldDefinitions';
import * as forms from 'models/utils/formDefinitions';

export const primaryFields = [
  {
    ...fields.FIELD_PURPOSE,
    inline: true,
    label: '',
  },
  {
    ...fields.FIELD_STATUS,
    inline: true,
  },
  {
    ...forms.FORM_EXTENDS,
    inline: true,
  },
];

export const secondaryFields = [
  { ...fields.FIELD_ALTERNATIVE_TEXT },
  {
    ...forms.FORM_PROPERTY,
  },
  {
    ...fields.FIELD_CONSTRAINT,
  },
  {
    ...forms.FORM_NODETYPE,
  },
  {
    ...fields.FIELD_CARDINALITY,
  },
  {
    ...fields.FIELD_LABEL,
  },
  {
    ...fields.FIELD_DESCRIPTION,
  },
  {
    ...fields.FIELD_ATTRIBUTE,
  },
  {
    ...fields.FIELD_SPECIFICATION,
  },
  {
    ...fields.FIELD_CSS,
  },
  {
    ...fields.FIELD_EDIT_LABEL,
  },
  {
    ...fields.FIELD_EDIT_DESCRIPTION,
  },
  {
    ...fields.FIELD_PATTERN,
  },
  {
    ...fields.FIELD_VALUE_TEMPLATE,
  },
];
