import * as ns from 'models/utils/ns';
import RfsField from './RfsField';

const TEXT_FIELD_PROPERTIES = [
  ns.RDF_PROPERTY_NODETYPE,
  ns.RDF_PROPERTY_LANGUAGE,
  ns.RDF_PROPERTY_VALUE_TEMPLATE,
  ns.RDF_PROPERTY_PATTERN,
  ns.RDF_PROPERTY_DEPENDS_ON,
];

const TEXT_FIELD_NODETYPES = [
  ns.RDF_PROPERTY_LITERAL,
  ns.RDF_PROPERTY_ONLY_LITERAL,
  ns.RDF_PROPERTY_LANGUAGE_LITERAL,
  ns.RDF_PROPERTY_URI,
];
class RfsTextField extends RfsField {
  constructor(props) {
    super({
      ...props,
      rdfType: ns.RDF_TYPE_TEXT_FIELD,
      nlsKeyLabel: 'textFieldLabel',
      properties: TEXT_FIELD_PROPERTIES,
      compatibleTypes: [],
      nodetypes: TEXT_FIELD_NODETYPES,
    });
  }
}

export default RfsTextField;
