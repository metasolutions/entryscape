import { useCallback } from 'react';
import { useUserState } from 'commons/hooks/useUser';
import { getSearchQuery } from 'commons/util/solr/context';
import {
  RDF_CLASS_PROJECT_TYPE,
  CONFIG_CONTEXT,
} from 'admin/configs/utils/namespace';
import {
  withListModelProviderAndLocation,
  withListRerender,
} from 'commons/components/ListView';
import {
  EntryListView,
  useSolrQuery,
  MODIFIED_COLUMN,
  TITLE_COLUMN,
  ACTIONS_GROUP_COLUMN,
} from 'commons/components/EntryListView';
import {
  LIST_ACTION_INFO,
  LIST_ACTION_EDIT,
} from 'commons/components/EntryListView/actions';
import { getPathFromViewName } from 'commons/util/site';
import { nlsBundles, listActions } from './actions';
import ProjectTypeEditDialog from '../ProjectTypeEditDialog/ProjectTypeEditDialog';

const ProjectTypeView = () => {
  const { userEntry, userInfo } = useUserState();

  const createQuery = useCallback(() => {
    const [query] = getSearchQuery(
      {
        rdfType: RDF_CLASS_PROJECT_TYPE,
        contextType: CONFIG_CONTEXT,
      },
      { userEntry, userInfo }
    );
    return query;
  }, [userEntry, userInfo]);

  const queryResults = useSolrQuery({ createQuery });

  return (
    <EntryListView
      // eslint-disable-next-line react/jsx-props-no-spreading
      {...queryResults}
      nlsBundles={nlsBundles}
      listActions={listActions}
      listActionsProps={{
        userEntry,
        userInfo,
      }}
      columns={[
        { ...TITLE_COLUMN, xs: 9 },
        MODIFIED_COLUMN,
        {
          ...ACTIONS_GROUP_COLUMN,
          actions: [
            LIST_ACTION_INFO,
            {
              ...LIST_ACTION_EDIT,
              Dialog: withListRerender(ProjectTypeEditDialog, 'onEdit'),
            },
          ],
        },
      ]}
      getListItemProps={({ entry }) => ({
        to: getPathFromViewName('admin__projecttypes__projecttype', {
          entryId: entry.getId(),
          contextId: entry.getContext().getId(),
        }),
      })}
    />
  );
};

export default withListModelProviderAndLocation(ProjectTypeView);
