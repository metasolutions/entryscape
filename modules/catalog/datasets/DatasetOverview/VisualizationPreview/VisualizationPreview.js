import { useEffect } from 'react';
import PropTypes from 'prop-types';
import { Entry } from '@entryscape/entrystore-js';
import { useTranslation } from 'commons/hooks/useTranslation';
import useAsync from 'commons/hooks/useAsync';
import { Link as MuiLink, CircularProgress, Typography } from '@mui/material';
import DownloadIcon from '@mui/icons-material/GetApp';
import WmsPreview from 'catalog/datasets/DatasetOverview/VisualizationPreview/WmsPreview';
import ChartPlaceholder from 'catalog/datasets/DatasetOverview/VisualizationForm/ChartPlaceholder';
import { MapProvider } from 'commons/components/Map/hooks/useMapContext';
import useErrorHandler from 'commons/errors/hooks/useErrorHandler';
import escoListNLS from 'commons/nls/escoList.nls';
import escaDatasetNLS from 'catalog/nls/escaDataset.nls';
import escaVisualizationNLS from 'catalog/nls/escaVisualization.nls';
import escoGeoMapNLS from 'commons/nls/escoGeoMap.nls';
import VisualizationChart from '../VisualizationChart';
import VisualizationMap from '../VisualizationMap';
import VisualizationTable from '../VisualizationTable';
import {
  getVisualizationTitle,
  getPreviewData,
  WMS_FORMAT,
  CSV_FORMAT,
} from '../utils/visualizations';
import './VisualizationPreview.scss';

const nlsBundles = [
  escoListNLS,
  escaDatasetNLS,
  escaVisualizationNLS,
  escoGeoMapNLS,
];

const DownloadLink = ({ uri = '#', label = '' }) => (
  <MuiLink
    href={uri}
    color="secondary"
    target="_blank"
    className="escaPreviewVisualizationLink"
  >
    <span className="escaPreviewVisualizationLink__inner">
      {label}
      <span className="escaPreviewVisualizationLink__icon">
        <DownloadIcon />
      </span>
    </span>
  </MuiLink>
);
DownloadLink.propTypes = {
  uri: PropTypes.string,
  label: PropTypes.string,
};

const PreviewVisualization = ({ datasetEntry, visualizationEntry }) => {
  const translate = useTranslation(nlsBundles);
  const { data: previewData, runAsync, isLoading, error } = useAsync();
  useErrorHandler(error);

  useEffect(() => {
    if (!visualizationEntry) return;

    runAsync(getPreviewData(datasetEntry, visualizationEntry));
  }, [runAsync, datasetEntry, visualizationEntry]);

  if (isLoading)
    return (
      <div className="escaPreviewVisualizationSpinner">
        <CircularProgress />
      </div>
    );

  const { errors, format, fields, csvData, csvFields, fileEntry } = previewData;
  const isBroken = Boolean(errors);

  return (
    <>
      <Typography variant="h6" component="h3">
        {getVisualizationTitle(visualizationEntry) || translate('untitled')}
      </Typography>
      <div className="escaPreviewVisualization">
        {isBroken ? (
          <ChartPlaceholder
            label={translate('brokenVizPlaceholderLabel')}
            labelClass="escaChartPlaceHolderLabel--error"
          />
        ) : (
          <>
            {format === WMS_FORMAT ? (
              <WmsPreview fields={fields} wmsURI={fields.sourceURI} />
            ) : null}
            {csvData && fields.chartType === 'table' && (
              <VisualizationTable
                data={csvData.data || []}
                tableColumns={csvFields}
              />
            )}
            {csvData &&
              (fields.chartType === 'bar' ||
                fields.chartType === 'line' ||
                fields.chartType === 'pie') && (
                <VisualizationChart
                  data={csvData}
                  type={fields.chartType}
                  xField={fields.xField}
                  yField={fields.yField}
                  operation={fields.operation}
                />
              )}
            {csvData && fields.chartType === 'map' && (
              <MapProvider>
                <VisualizationMap
                  data={csvData}
                  xField={fields.xField}
                  yField={fields.yField}
                  featureTitleField={fields.featureTitle}
                  scaleLineControl={fields.scaleLineControl}
                  mapOptions={{ initialView: fields.initialView }}
                />
              </MapProvider>
            )}
          </>
        )}
      </div>
      {format === CSV_FORMAT ? (
        <DownloadLink
          uri={fileEntry.getResourceURI()}
          label={translate('previewVisualizationSource')}
        />
      ) : null}
    </>
  );
};
PreviewVisualization.propTypes = {
  datasetEntry: PropTypes.instanceOf(Entry),
  visualizationEntry: PropTypes.instanceOf(Entry),
};

export default PreviewVisualization;
