import { FIELD_TITLE, FIELD_DESCRIPTION } from 'models/utils/fieldDefinitions';
import { targetLanguageField } from 'models/builds/utils/fieldDefinitions';
import { LITERAL, RadioEditor } from 'commons/components/forms/editors';

export const TURTLE_FORMAT = 'text/turtle';
export const RDF_XML_FORMAT = 'application/rdf+xml';

const FORMAT_FIELD = {
  name: 'type',
  property: 'dcterms:format',
  labelNlsKey: 'formatLabel',
  nodetype: LITERAL,
  initialValue: TURTLE_FORMAT,
  choices: [
    {
      label: 'Turtle',
      value: TURTLE_FORMAT,
    },
    {
      label: 'RDF/XML',
      value: RDF_XML_FORMAT,
    },
  ],
  max: 1,
  Editor: RadioEditor,
  mandatory: true,
  row: true,
};

export const fields = [
  FIELD_TITLE,
  FIELD_DESCRIPTION,
  targetLanguageField,
  FORMAT_FIELD,
];
