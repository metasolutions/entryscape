import { useEffect } from 'react';
import config from 'config';
import { withinConceptLimit } from 'terms/utils';
import { useUserState } from 'commons/hooks/useUser';
import useAsync from 'commons/hooks/useAsync';
import { hasUserPermission, ADMIN_RIGHTS, PREMIUM } from 'commons/util/user';
import { isContextPremium } from 'commons/util/context';

const useConceptRestriction = (
  isCreateDialogOpen,
  context,
  conceptSchemeEntry
) => {
  const pathToConceptLimitContent = config.get('terms.conceptLimitDialog');
  const conceptLimit = config.get('terms.conceptLimit');
  const { userEntry } = useUserState();
  const contextEntry = context.getEntry(true);
  const hasPermission = hasUserPermission(userEntry, [PREMIUM, ADMIN_RIGHTS]);
  const isExemptFromRestrictions =
    hasPermission || isContextPremium(contextEntry);

  const {
    data: isWithinConceptLimit,
    runAsync,
    status,
  } = useAsync({
    data: true,
  });
  const isRestricted =
    !isExemptFromRestrictions &&
    isCreateDialogOpen &&
    status === 'resolved' &&
    !isWithinConceptLimit;

  useEffect(() => {
    if (!isCreateDialogOpen || !conceptLimit || !conceptSchemeEntry) return;
    runAsync(withinConceptLimit(conceptSchemeEntry, isExemptFromRestrictions));
  }, [
    conceptLimit,
    conceptSchemeEntry,
    isCreateDialogOpen,
    runAsync,
    isExemptFromRestrictions,
  ]);

  if (!conceptLimit) return { isRestricted: false, status: 'resolved' };
  return { isRestricted, contentPath: pathToConceptLimitContent, status };
};

export default useConceptRestriction;
