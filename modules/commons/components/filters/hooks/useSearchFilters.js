import { useCallback, useEffect, useState } from 'react';
import Filter, { createFilters } from 'commons/components/filters/utils/filter';
import useAsync, { IDLE } from 'commons/hooks/useAsync';
import { useListModel, TOGGLE_FILTERS } from 'commons/components/ListView';

/**
 * @typedef {object} FilterObject
 * @property {string} headerNlsKey - Filter label
 * @property {string} id - Filter grouping property e.g. 'public', 'psi'
 * @property {object[]} items - Filter menu items
 */
/**
 * @typedef {object} UseFilterResult
 * @property {Filter[]} filters - Array of filters
 * @property {Function} addFilter - Add new filter definition
 * @property {Function} addItems - Append items to a given filter
 * @property {Function} clearFilters - Clear all selected filters (reset back to default option)
 * @property {Function} applyFilters - Applies all filters to the given query
 * @property {Function} hasSelectedFilter  - Shows whether any filters are selected
 */
/**
 * Provides what's necessary for the search filters
 *
 * @param {FilterObject[]} filterDefinitions
 * @param {Filter[]} initialFilters
 * @returns {UseFilterResult}
 */
const useSearchFilters = (filterDefinitions, initialFilters) => {
  const [{ filter: filterSelections }] = useListModel();
  const [filters, setFilters] = useState(
    initialFilters || createFilters(filterDefinitions, filterSelections)
  );
  const { runAsync: runLoadChoices, status } = useAsync();
  const [, dispatch] = useListModel();

  const addFilter = useCallback((filterDefinition) => {
    setFilters((currentFilters) => [
      ...currentFilters,
      new Filter(filterDefinition),
    ]);
  }, []);

  const addItems = useCallback(
    (filterId, newItems) => {
      const filter = filters.find(
        (testFilter) => testFilter.getId() === filterId
      );
      if (!filter) return;
      filter.addItems(newItems);
      setFilters((currentFilters) => [...currentFilters]); // this will trigger refresh
    },
    [filters]
  );

  const clearFilters = useCallback(() => {
    filters.forEach((filter) => filter.clearSelected());
    setFilters((currentFilters) => [...currentFilters]);
  }, [filters]);

  const applyFilters = useCallback(
    (query) => {
      for (const filter of filters) filter.applyFilterParams(query);
      return query;
    },
    [filters]
  );

  const hasSelectedFilter = useCallback(
    () => filters.some((filter) => filter.hasSelected()),
    [filters]
  );
  const hasInitialSelection = filters.some(
    (filter) => filter.getInitialSelection() !== filter.getDefaultValue()
  );

  useEffect(() => {
    if (hasInitialSelection) dispatch({ type: TOGGLE_FILTERS, value: true });
  }, [dispatch, hasInitialSelection]);

  useEffect(() => {
    if (status !== IDLE) return;
    const loadItems = async () => {
      for (const filter of filters) {
        await filter.loadItems();
      }
    };

    runLoadChoices(loadItems());
  }, [runLoadChoices, status, filters]);

  const availableFilters = filters.filter(
    (filter) => filter?.getItems().length > 1 || filter.autoHide === false
  );

  return {
    filters: availableFilters,
    addFilter,
    addItems,
    clearFilters,
    applyFilters,
    hasSelectedFilter,
    hasFilters: availableFilters.length > 0,
    isLoading: status === 'idle' || status === 'pending',
  };
};

export default useSearchFilters;
