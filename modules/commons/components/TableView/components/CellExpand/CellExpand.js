import React, { useState, useRef, useEffect, useCallback } from 'react';
import PropTypes from 'prop-types';
import {
  ClickAwayListener,
  Paper,
  Popper,
  Typography,
  IconButton,
  Box,
} from '@mui/material';
import CloseIcon from '@mui/icons-material/Close';
import { makeStyles } from '@mui/styles';
import Tooltip from 'commons/components/common/Tooltip';
import useDebounce from 'commons/hooks/useDebounce';
import usePresentOrEdit from 'commons/components/TableView/usePresentOrEdit';
import { useHandleEditRowContext } from 'commons/components/TableView/HandleEditRowContext';
import { useTranslation } from 'commons/hooks/useTranslation';
import escoTableViewNLS from 'commons/nls/escoTableView.nls';
import './index.scss';
import { graphPropType } from 'commons/util/entry';

const useStyles = makeStyles(() => ({
  root: {
    alignItems: 'center',
    lineHeight: '24px',
    width: '100%',
    height: '100%',
    position: 'relative',
    display: 'flex',
    padding: '0 10px',
    '& .cellValue': {
      whiteSpace: 'nowrap',
      overflow: 'hidden',
      textOverflow: 'ellipsis',
    },
  },
}));

const GridCellExpand = React.memo(function GridCellExpand(props) {
  const { formattedValue, value, row, colDef } = props;
  const { headerName } = colDef;
  const width = 480;
  const wrapper = useRef(null);
  const cellDiv = useRef(null);
  const cellValue = useRef(null);
  const [anchorEl, setAnchorEl] = useState(null);
  const classes = useStyles();
  const [showFullCell, setShowFullCell] = useState(false);
  const [showPopper, setShowPopper] = useState(false);
  const [viewState, setViewState] = useState('');
  const Component = usePresentOrEdit(viewState, value);
  const handleEditRow = useHandleEditRowContext();
  const t = useTranslation(escoTableViewNLS);

  const closeEditor = useCallback(() => {
    if (viewState === 'edit') {
      if (row.graph.isChanged()) {
        handleEditRow(row);
      }
    }
    setShowFullCell(false);
    setViewState('');
  }, [handleEditRow, row, viewState]);

  const mouseEnterCallback = () => {
    setShowPopper(true);
    setViewState('present');
    setAnchorEl(cellDiv.current);
    setShowFullCell(true);
  };

  const debouncedMouseEnterCallback = useDebounce(mouseEnterCallback, 200);

  const handleMouseEnter = () => {
    if (viewState === 'edit') return;
    debouncedMouseEnterCallback();
  };

  const handleMouseLeave = () => {
    if (viewState === 'edit') return;
    debouncedMouseEnterCallback.cancel();
    closeEditor();
  };

  const handleClickAway = ({ currentTarget: { activeElement } }) => {
    // Necessary to prevent the popper closing. Selects portal the options at the
    // end of the document body, which fires the click away listener,
    //   see https://github.com/mui-org/material-ui/issues/12034.
    // Should either switch to native selects on rdforms or provide
    // a way to identify them or something.
    if (activeElement?.classList?.contains('MuiMenuItem-root')) {
      return;
    }

    closeEditor();
  };

  useEffect(() => {
    if (!showFullCell) {
      return undefined;
    }

    // eslint-disable-next-line jsdoc/require-jsdoc
    function handleKeyDown(nativeEvent) {
      // IE11, Edge (prior to using Bink?) use 'Esc'
      if (nativeEvent.key === 'Escape' || nativeEvent.key === 'Esc') {
        closeEditor();
      }
    }

    document.addEventListener('keydown', handleKeyDown);

    return () => {
      document.removeEventListener('keydown', handleKeyDown);
    };
  }, [closeEditor, setShowFullCell, showFullCell]);

  const clickToEdit = () => {
    if (viewState !== 'edit') {
      setShowFullCell(true);
      setViewState('edit');
    }
  };

  return (
    <ClickAwayListener
      onClickAway={viewState === 'edit' ? handleClickAway : () => {}}
    >
      <Box
        ref={wrapper}
        className={classes.root}
        onMouseEnter={handleMouseEnter}
        onMouseLeave={handleMouseLeave}
        onClick={clickToEdit}
      >
        <div
          ref={cellDiv}
          style={{
            height: 50,
            display: 'block',
            position: 'absolute',
            top: 0,
          }}
        />
        <div ref={cellValue} className="cellValue">
          {formattedValue}
        </div>
        {showPopper && (
          <Popper
            open={showFullCell && anchorEl !== null}
            anchorEl={anchorEl}
            style={{
              width: viewState === 'edit' ? width : 'inherit',
              maxWidth: width,
            }}
            placement="bottom-start"
            popperOptions={{
              modifiers: [
                {
                  name: 'offset',
                  options: { offset: [-10, 2] },
                },
              ],
            }}
          >
            <Paper
              elevation={1}
              style={{
                minHeight: wrapper.current.offsetHeight - 3,
              }}
              classes={{
                root:
                  viewState === 'edit'
                    ? 'escoCellExpand__paper'
                    : 'escoCellExpand__paper escoCellExpand__paper--clickable',
              }}
            >
              <div className="escoCellExpandHeader">
                {viewState === 'edit' ? (
                  <Tooltip title={t('closePopoverTooltip')}>
                    <IconButton
                      classes={{ root: 'escoCellExpandHeader__closeButton' }}
                      aria-label={t('closePopoverLabel')}
                      onClick={closeEditor}
                    >
                      <CloseIcon />
                    </IconButton>
                  </Tooltip>
                ) : (
                  <Typography color="primary">
                    {t('clickToEdit', headerName)}
                  </Typography>
                )}
              </div>

              {Component && (
                <div
                  className={`${
                    formattedValue ? 'escoCellExpand__prompt' : ''
                  }`}
                >
                  <Component />
                </div>
              )}
            </Paper>
          </Popper>
        )}
      </Box>
    </ClickAwayListener>
  );
});

GridCellExpand.propTypes = {
  value: PropTypes.shape({}).isRequired,
  formattedValue: PropTypes.string.isRequired,
  row: PropTypes.shape({ graph: graphPropType }),
  colDef: PropTypes.shape({ headerName: PropTypes.string }).isRequired,
};

// eslint-disable-next-line jsdoc/require-jsdoc
function renderCellExpand(params) {
  return <GridCellExpand {...params} />;
}

renderCellExpand.propTypes = {
  /**
   * The column of the row that the current cell belongs to.
   */
  colDef: PropTypes.shape({}).isRequired,
  /**
   * The cell value, but if the column has valueGetter, use getValue.
   */
  value: PropTypes.oneOfType([
    PropTypes.instanceOf(Date),
    PropTypes.number,
    PropTypes.object,
    PropTypes.string,
    PropTypes.bool,
  ]),
};

export default renderCellExpand;
