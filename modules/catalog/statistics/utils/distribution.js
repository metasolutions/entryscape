import { entrystoreUtil } from 'commons/store';
import {
  getDatasetByDistributionURI,
  getDistributionByFileResourceURIs,
  mapResourceURIsToEntries,
} from '../../utils/react/dcat';

/**
 * Retrieves the inverse property paths of the file/apis to their respective distribution and datasets.
 * Returns an array of three elements:
 *  0: A map of type <fleOrApiResourceURI, fileOrAPIEntry>
 *  1: A map of type <fleOrApiResourceURI, parentDistributionEntry>
 *  2: A map of type <fleOrApiResourceURI, parentDatasetEntry>
 *
 * @param resourceURIs
 * @param context
 * @return {Promise<Map<string, store/Entry>[]>}
 */
const getParentEntriesByResourceURI = async (resourceURIs, context) => {
  if (resourceURIs.length === 0) {
    // don't make any solr requests if no stats data found
    return [new Map(), new Map(), new Map()];
  }
  /**
   * Get the actual distribution entries from the file/api resource URI
   * @type {Map<string, store/Entry>}
   */
  const distributionEntries = await getDistributionByFileResourceURIs(
    resourceURIs,
    context
  );

  /**
   * for each distribution entry get the resource URIs
   */
  const fileRURIsToDistributionEntriesMap = new Map(); // @todo @valentino better naming
  const distributions2Resources = new Map();
  for (const [ruri, entry] of distributionEntries) {
    // eslint-disable-line
    fileRURIsToDistributionEntriesMap.set(ruri, entry);

    if (distributions2Resources.has(entry.getResourceURI())) {
      const ruris = distributions2Resources.get(entry.getResourceURI());
      ruris.push(ruri);
      distributions2Resources.set(entry.getResourceURI(), ruris);
    } else {
      distributions2Resources.set(entry.getResourceURI(), [ruri]);
    }
  }
  const distributionRURIs = Array.from(distributions2Resources.keys());
  const datasetEntries = await getDatasetByDistributionURI(
    distributionRURIs,
    context
  );
  const fileRURIsToDatasetEntriesMap = new Map();
  for (const [ruri, entry] of datasetEntries) {
    // eslint-disable-line
    const fileOrAPIRURIs = distributions2Resources.get(ruri);
    fileOrAPIRURIs.forEach((fileOrAPIRURI) =>
      fileRURIsToDatasetEntriesMap.set(fileOrAPIRURI, entry)
    );
  }

  const fileRURIsToFileEntriesMap = await mapResourceURIsToEntries(
    resourceURIs,
    context
  );

  return [
    fileRURIsToFileEntriesMap,
    fileRURIsToDistributionEntriesMap,
    fileRURIsToDatasetEntriesMap,
  ];
};

/**
 * @param property
 * @returns {function(*): *}
 */
const getPropertyValues = (property) => (entry) =>
  entry.getMetadata().objects(entry.getResourceURI(), property).values();

/**
 *
 * @param entries
 * @param context
 * @param path E.g ['dcat:distribution', 'dcat:downloadURL']
 * @return {Promise<Entry[]>}
 */
const getEntriesByPropertyPath = async (entries, context, path = []) => {
  const property = path.shift();
  if (property) {
    const urisToFollow = entries.map(getPropertyValues(property)).flat();
    const childEntries = await entrystoreUtil.loadEntriesByResourceURIs(
      urisToFollow,
      context,
      true
    );

    // since ordering doesn't really matter in this case we can afford to discard any results that were not entries,
    // i.e the resource uri could not resolve into an entry
    const filteredChildEntries = childEntries.filter((entry) => entry != null);

    return getEntriesByPropertyPath(filteredChildEntries, context, path);
  }

  return entries;
};

/**
 *
 * @param distributionEntries
 * @returns {Array}
 */
const getDistributionFileResourceURIs = (distributionEntries) =>
  distributionEntries.map((entry) =>
    entry
      .getMetadata()
      .objects(entry.getResourceURI(), 'dcat:downloadURL')
      .values()
  );

const RESOURCE_TYPE_FILE = 'file';
const RESOURCE_TYPE_API = 'api';
const RESOURCE_TYPE_ALL = 'all';

/**
 * @param {boolean} resourceTypeFile
 * @param {boolean} resourceTypeAPI
 * @return {string}
 */
const getResourceType = (
  resourceTypeFile,
  resourceTypeAPI,
  resourceTypeDocumentFile
) => {
  const isResourceTypeFile = resourceTypeFile || resourceTypeDocumentFile;
  if (isResourceTypeFile && resourceTypeAPI) {
    return RESOURCE_TYPE_ALL;
  }
  if (isResourceTypeFile) {
    return RESOURCE_TYPE_FILE;
  }
  if (resourceTypeAPI) {
    return RESOURCE_TYPE_API;
  }
};

export {
  getEntriesByPropertyPath,
  getDistributionFileResourceURIs,
  getParentEntriesByResourceURI,
  getResourceType,
  RESOURCE_TYPE_FILE,
  RESOURCE_TYPE_API,
  RESOURCE_TYPE_ALL,
};
