import PropTypes from 'prop-types';
import {
  ListItemOpenMenuButton,
  ActionsMenu,
  useListView,
} from 'commons/components/ListView';
import { useTranslation } from 'commons/hooks/useTranslation';
import useAsyncActions from 'commons/components/ListView/hooks/useAsyncActions';
import { getIconFromActionId } from 'commons/actions';

export const ListItemActionsMenu = ({
  actions: initialActions,
  id,
  excludeActions = [],
  ...actionsParams
}) => {
  const { nlsBundles } = useListView();
  const translate = useTranslation(nlsBundles);
  const filteredActions = initialActions.filter(
    ({ id: actionId }) => !excludeActions.includes(actionId)
  );

  const mergedActions = filteredActions.map(
    ({ icon, getProps, ...action }) => ({
      action: {
        nlsBundles,
        ...action,
        ...actionsParams,
        ...(getProps ? getProps(actionsParams) : {}),
      },
      icon: icon || getIconFromActionId(action.id),
      label: translate(action.labelNlsKey),
    })
  );

  const { actions, isLoading } = useAsyncActions(mergedActions);

  if (!actions.length) return null;

  return (
    <>
      <ListItemOpenMenuButton />
      <ActionsMenu items={actions} isLoading={isLoading} />
    </>
  );
};

ListItemActionsMenu.propTypes = {
  actions: PropTypes.arrayOf(PropTypes.shape({})),
  id: PropTypes.string,
  actionParams: PropTypes.shape({}),
  excludeActions: PropTypes.arrayOf(PropTypes.string),
};
