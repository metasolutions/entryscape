import { namespaces as ns } from '@entryscape/rdfjson';
// import 'commons/rdforms/linkBehaviour'; // TODO probably not needed anymore

export default () => {
  ns.add('esterms', 'http://entryscape.com/terms/');
};
