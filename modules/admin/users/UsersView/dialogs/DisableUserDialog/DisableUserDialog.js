import PropTypes from 'prop-types';
import Button from '@mui/material/Button';
import ContentWrapper from 'commons/components/common/ContentWrapper';
import { useTranslation } from 'commons/hooks/useTranslation';
import ListActionDialog from 'commons/components/common/dialogs/ListActionDialog';
import esadUserNLS from 'admin/nls/esadUser.nls';
import useUserStatus from 'admin/users/hooks/useUserStatus';

const DisableUserDialog = ({ entry, isOpen, closeDialog, onStatusChange }) => {
  const [isUserDisabled, toggleUserStatus] = useUserStatus(entry);

  const handleUserStatusChange = () => {
    toggleUserStatus().then(() => {
      closeDialog();
      onStatusChange();
    });
  };

  const t = useTranslation(esadUserNLS);

  const dialogActions = (
    <Button onClick={handleUserStatusChange}>
      {isUserDisabled
        ? t('enableUserButtonLabel')
        : t('disableUserButtonLabel')}
    </Button>
  );

  return (
    <ListActionDialog
      id="change-user-status-dialog"
      open={isOpen}
      closeDialog={closeDialog}
      actions={dialogActions}
      aria-labelledby="change-user-status-dialog-title"
      maxWidth="sm"
    >
      <ContentWrapper
        xs={10}
        justifyContent="flex-start"
        alignItems="flex-start"
      >
        {isUserDisabled
          ? t('userStatusEnableConfirmation')
          : t('userStatusDisableConfirmation')}
      </ContentWrapper>
    </ListActionDialog>
  );
};

DisableUserDialog.propTypes = {
  entry: PropTypes.shape().isRequired,
  closeDialog: PropTypes.func.isRequired,
  onStatusChange: PropTypes.func,
  isOpen: PropTypes.bool,
};

export default DisableUserDialog;
