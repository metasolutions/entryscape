import { useState } from 'react';
import PropTypes from 'prop-types';
import {
  Box,
  Button,
  Checkbox,
  Grid,
  List,
  ListItemButton,
  ListItemText,
  Paper,
} from '@mui/material';
import * as ns from 'models/utils/ns';
import {
  useEditorContext,
  Fields,
  Field,
} from 'commons/components/forms/editors';
import { ATTRIBUTES } from 'models/utils/attribute';
import { ListSearchField } from 'commons/components/ListView';
import { localize } from 'commons/locale';
import { useTranslation } from 'commons/hooks/useTranslation';
import esmoCommonsNls from 'models/nls/esmoCommons.nls';
import './AttributesEditor.scss';

export const AttributesEditor = ({ children }) => {
  const { graph, resourceURI } = useEditorContext();
  // put saved selection on top
  const [attributes, setAttributes] = useState(
    ATTRIBUTES.map((attribute) => ({
      ...attribute,
      selected: Boolean(
        graph.find(resourceURI, ns.RDF_PROPERTY_ATTRIBUTE, {
          type: 'literal',
          value: attribute.name,
        }).length
      ),
    })).sort(({ selected }) => (selected ? -1 : 0))
  );
  const t = useTranslation(esmoCommonsNls);
  const [searchInput, setSearchInput] = useState('');
  const [showAllAttributes, setShowAllAttributes] = useState(true);

  const filteredAttributes = attributes
    .filter(({ selected }) => showAllAttributes || selected)
    .filter(({ name }) =>
      name.toLowerCase().includes(searchInput.toLowerCase())
    );

  const handleSelect = ({ name, selected }) => {
    const newAttributes = attributes.map((attribute) =>
      attribute.name === name
        ? { ...attribute, selected: !selected }
        : attribute
    );
    setAttributes(newAttributes);

    // update graph
    graph.findAndRemove(resourceURI, ns.RDF_PROPERTY_ATTRIBUTE);
    newAttributes.forEach((attribute) => {
      if (!attribute.selected) return;
      graph.addL(resourceURI, ns.RDF_PROPERTY_ATTRIBUTE, attribute.name);
    });
  };

  const dispatchSearch = ({ type, value }) => {
    const newValue = type === 'SEARCH' ? value : '';
    setSearchInput(newValue);
  };

  return (
    <Fields max={1}>
      {children}
      <Field>
        <Grid item xs={12}>
          <div className="esmoAttributesEditorFilters">
            <ListSearchField
              placeholder={t('searchAttributesPlaceholder')}
              search={searchInput}
              dispatch={dispatchSearch}
            />
            <Box sx={{ marginLeft: 'auto' }}>
              <Button
                variant="text"
                onClick={() => setShowAllAttributes(!showAllAttributes)}
              >
                {showAllAttributes ? t('showSelectedLabel') : t('showAllLabel')}
              </Button>
            </Box>
          </div>
        </Grid>
        <List sx={{ width: '100%' }}>
          {filteredAttributes.length
            ? filteredAttributes.map((attribute) => {
                const { name, description, selected } = attribute;
                const listItemId = `listitem-${name}`;
                return (
                  <Paper key={name} sx={{ margin: '16px 0' }}>
                    <ListItemButton onClick={() => handleSelect(attribute)}>
                      <ListItemText
                        id={listItemId}
                        primary={name}
                        secondary={description ? localize(description) : ''}
                      />
                      <Checkbox
                        edge="end"
                        checked={selected}
                        inputProps={{ 'aria-labelledby': listItemId }}
                      />
                    </ListItemButton>
                  </Paper>
                );
              })
            : null}
        </List>
      </Field>
    </Fields>
  );
};

AttributesEditor.propTypes = {
  children: PropTypes.node,
};
