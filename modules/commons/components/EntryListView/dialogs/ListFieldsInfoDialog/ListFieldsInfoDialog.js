import PropTypes from 'prop-types';
import ListActionDialog from 'commons/components/common/dialogs/ListActionDialog';
import {
  useTranslation,
  nlsBundlesPropType,
} from 'commons/hooks/useTranslation';
import { getPresenter } from 'commons/components/forms/presenters';
import { entryPropType, graphPropType } from 'commons/util/entry';
import ExpandButton from 'commons/components/buttons/ExpandButton';
import ResourceInfo from 'commons/components/rdforms/ResourceInfo';
import escoDialogs from 'commons/nls/escoDialogs.nls';
import { localizeLabel } from 'commons/locale';

const ListFieldsInfoDialog = ({
  fields,
  closeDialog,
  title,
  titleNlsKey = 'infoDialogTitle',
  entry,
  graph,
  resourceURI,
  nlsBundles,
}) => {
  const translate = useTranslation(nlsBundles);

  const translateLabel = ({ labelNlsKey, label }) => {
    const localizedLabel = localizeLabel(label);
    if (localizedLabel === null && labelNlsKey) return translate(labelNlsKey);
    return localizedLabel;
  };

  const titleLabel = title || translate(titleNlsKey);

  return (
    <ListActionDialog
      title={titleLabel}
      closeDialog={closeDialog}
      closeDialogButtonLabel={translate('close')}
      maxWidth="sm"
    >
      {entry ? (
        <ExpandButton initiallyExpanded={false} nlsBundles={[escoDialogs]}>
          <ResourceInfo entry={entry} />
        </ExpandButton>
      ) : null}
      {fields.map((field) => {
        const Presenter = getPresenter(field);
        return Presenter ? (
          <Presenter
            key={field.name || field.property}
            graph={graph || entry?.getMetadata()}
            resourceURI={resourceURI || entry?.getResourceURI()}
            entry={entry}
            {...field}
            label={translateLabel(field)}
            translate={translate}
          />
        ) : null;
      })}
    </ListActionDialog>
  );
};

ListFieldsInfoDialog.propTypes = {
  entry: entryPropType,
  fields: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  closeDialog: PropTypes.func.isRequired,
  title: PropTypes.string,
  titleNlsKey: PropTypes.string,
  nlsBundles: nlsBundlesPropType,
  graph: graphPropType,
  resourceURI: PropTypes.string,
};

export default ListFieldsInfoDialog;
