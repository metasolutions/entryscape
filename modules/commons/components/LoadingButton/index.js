import PropTypes from 'prop-types';
import React from 'react';
import CircularProgress from '@mui/material/CircularProgress';
import Button from '@mui/material/Button';
import './index.scss';

const LoadingButton = ({
  children,
  loading,
  success,
  className = '',
  label,
  disabled,
  color = 'primary',
  ...rest
}) => {
  const buttonClassname = `LoadingButton__Button ${className} ${
    loading ? 'LoadingButton--loading' : ''
  }`;

  return (
    <div className="LoadingButton">
      <div className="LoadingButton__wrapper">
        <Button
          variant="contained"
          className={buttonClassname}
          disabled={disabled || loading}
          fullWidth
          color={success ? 'success' : color}
          {...rest}
        >
          {label || children}
        </Button>
        {loading ? (
          <CircularProgress size={24} className="LoadingButton__Progress" />
        ) : null}
      </div>
    </div>
  );
};

LoadingButton.propTypes = {
  children: PropTypes.node,
  loading: PropTypes.bool,
  success: PropTypes.bool,
  className: PropTypes.string,
  label: PropTypes.string,
  disabled: PropTypes.bool,
  color: PropTypes.string,
};

export default LoadingButton;
