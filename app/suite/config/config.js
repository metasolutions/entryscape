import adminConfig from 'admin/config/config';
import commonsConfig from 'commons/config';
import catalogConfig from 'catalog/config/config';
import termsConfig from 'terms/config/config';
import workbenchConfig from 'workbench/config/config';
import modelsConfig from 'models/config/config';
import searchConfig from 'search/config/config';
import ConfigStore from 'commons/util/ConfigStore';

/**
 * Customer config - can override other configs
 */
const instanceConfigs = [
  window.__entryscape_config,
  window.__entryscape_config_dev || {},
];

const applicationConfigs = [
  commonsConfig,
  adminConfig,
  catalogConfig,
  termsConfig,
  workbenchConfig,
  modelsConfig,
  searchConfig,
  /**
   * App theme
   *
   * @type {object}
   */
  {
    theme: {
      /**
       * Specifies app name
       *
       * @type {string}
       * @memberof theme
       */
      appName: 'EntryScape',
    },
  },
];

export const getInstanceConfigs = () => {
  return instanceConfigs;
};

export const getApplicationConfigs = () => {
  return applicationConfigs;
};

const config = new ConfigStore();

export default config;
