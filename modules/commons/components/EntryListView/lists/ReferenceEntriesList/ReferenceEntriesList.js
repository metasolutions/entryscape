import { useCallback } from 'react';
import PropTypes from 'prop-types';
import { Entry } from '@entryscape/entrystore-js';
import { Typography } from '@mui/material';
import InfoIcon from '@mui/icons-material/Info';
import { entrystore } from 'commons/store';
import { getFullLengthLabel } from 'commons/util/rdfUtils';
import { truncate } from 'commons/util/util';
import escoListNLS from 'commons/nls/escoList.nls';
import { useTranslation } from 'commons/hooks/useTranslation';
import {
  ListItemIcon,
  withListModelProvider,
} from 'commons/components/ListView';
import LinkedDataBrowserDialog from 'commons/components/LinkedDataBrowserDialog';
import { EntryListView } from '../../EntryListView';
import { useSolrQuery } from '../../hooks/useSolrQuery';
import {
  TITLE_COLUMN,
  MODIFIED_COLUMN,
  INFO_COLUMN,
} from '../../utils/columns';
import './ReferenceEntriesList.scss';

const nlsBundles = [escoListNLS];

const ReferenceEntriesList = ({
  entry: referencedEntry,
  title = '',
  titleClasses = '',
}) => {
  const translate = useTranslation(nlsBundles);

  const createQuery = useCallback(
    () => entrystore.newSolrQuery().objectUri(referencedEntry.getResourceURI()),
    [referencedEntry]
  );
  const queryResults = useSolrQuery({ createQuery });

  return queryResults.entries.length ? (
    <div className="escoEntryReferences">
      <Typography
        className={titleClasses || 'escoReferenceEntriesList__header'}
      >
        {title || translate('referencesList')}
      </Typography>
      <EntryListView
        // eslint-disable-next-line react/jsx-props-no-spreading
        {...queryResults}
        nlsBundles={nlsBundles}
        includeDefaultListActions={false}
        columns={[
          {
            ...TITLE_COLUMN,
            headerNlsKey: 'referencesListHeader',
            xs: 8,
            getProps: ({ entry }) => ({
              primary:
                truncate(getFullLengthLabel(entry), 30) ||
                translate('entryWithId', `${entry.getId()}`),
            }),
          },
          {
            ...INFO_COLUMN,
            xs: 2,
            justifyContent: 'flex-start',
            Component: ListItemIcon,
            icon: <InfoIcon className="escoReferenceEntriesList__infoIcon" />,
          },
          { ...MODIFIED_COLUMN, headerNlsKey: 'referencesListHeaderModified' },
        ]}
        getListItemProps={({ entry }) => ({
          action: {
            id: 'info',
            Dialog: LinkedDataBrowserDialog,
            entry,
            showReferences: true,
          },
        })}
      />
    </div>
  ) : null;
};

ReferenceEntriesList.propTypes = {
  entry: PropTypes.instanceOf(Entry),
  title: PropTypes.string,
  titleClasses: PropTypes.string,
};

export default withListModelProvider(ReferenceEntriesList);
