import Papa from 'papaparse';
import { readFileAsText } from 'commons/util/file';
import { convertBytesToMBytes } from 'commons/util/util';

const defaultFetchOptions = {
  credentials: 'include',
};

/**
 * See https://developer.mozilla.org/en-US/docs/Web/API/Streams_API/Using_readable_streams#Consuming_a_fetch_as_a_stream
 * @param {string} uri
 * @param {Object} options
 * @returns {Promise}
 */
export const fetchAsStream = (uri, options = {}) => {
  return fetch(uri, { ...defaultFetchOptions, ...options })
    .then(({ body }) => {
      const reader = body.getReader();
      return new ReadableStream({
        start(controller) {
          const pump = () => {
            return reader.read().then(({ done, value }) => {
              // When no more data needs to be consumed, close the stream
              if (done) {
                controller.close();
                return;
              }
              // Enqueue the next data chunk into our target stream
              controller.enqueue(value);
              return pump();
            });
          };
          return pump();
        },
      });
    })
    .then((stream) => new Response(stream));
};

/**
 * Used for debugging
 * @param {Object} results
 * @return {Object}
 */
const checkParseErrors = ({ errors }) => {
  const [error] = errors;
  if (error) console.error(`Failed to parse file caused by: ${error.message}`);
};

/**
 * @param csvData
 * @return {Promise}
 */
const parseCSVFile = (csvData) =>
  Papa.parse(csvData, {
    header: true,
    skipEmptyLines: true,
    complete: checkParseErrors,
  });

/**
 *
 * @param {string} uri
 * @param {Object} options
 * @returns {Promise}
 */
export const fetchCsv = (
  uri,
  { ignoreStats, encoding = 'UTF-8', ...fetchOptions } = {}
) => {
  const requestUri = `${uri}${ignoreStats ? '?nostats' : ''}`;

  return fetchAsStream(requestUri, fetchOptions)
    .then((response) => response.blob())
    .then((blob) => readFileAsText(blob, encoding))
    .then((csvData) => parseCSVFile(csvData));
};

const CSV_MAX_SIZE_MB = 1;

/**
 *
 * @param {number} fileSize
 * @returns {boolean}
 */
export const isWithinFileSizeLimit = (fileSize) => {
  const fileSizeMb = convertBytesToMBytes(fileSize);
  return fileSizeMb <= CSV_MAX_SIZE_MB;
};
