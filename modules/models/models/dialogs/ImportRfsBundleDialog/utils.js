import { Graph, Statement, namespaces as ns } from '@entryscape/rdfjson';
import { promiseUtil, Context, Entry } from '@entryscape/entrystore-js';
import { getUniqueId } from 'commons/hooks/useUniqueId';
import { entrystore } from 'commons/store';
import {
  RDF_TYPE_FORM,
  RDF_TYPE_PROFILE_FORM,
  RDF_TYPE_TEXT_FIELD,
  RDF_TYPE_FIELD,
  RDF_TYPE_SELECT_FIELD,
  RDF_PROPERTY_STATUS,
  RDF_PROPERTY_ORDER,
  RDF_PROPERTY_ITEM,
  RDF_PROPERTY_EXTENDS,
  RDF_CLASS_EXTENSION,
  RDF_PROPERTY_STATUS_STABLE,
  RDF_TYPE_OBJECT_FORM,
  RDF_TYPE_DATATYPE_FIELD,
  RDF_TYPE_SECTION_FORM,
  RDF_TYPE_LOOKUP_FIELD,
  RDF_PROPERTY_PROPERTY,
  RDF_TYPE_INLINE_FIELD,
  RDF_PROPERTY_INLINE,
  RDF_TYPE_PROPERTY_FORM,
} from 'models/utils/ns';
import { isInlineField } from 'models/fields/utils/fields';
import { getCardinality, setCardinality } from 'models/utils/cardinality';
import { isObjectForm } from 'models/forms/utils/forms';
import graphSetters from './graphSetters';

/**
 * @typedef {object} RfsImportItem
 * @property {Graph} graph - the metadata graph used for import on entry creation.
 * @property {string} title - the title is used both as title and identifier.
 * @property {object[]} formItems - form item templates, in case it's a form.
 * @property {Entry} entry - the imported entry
 * @property {string} resourceURI - resourceURI for the imported entry.
 * @property {string} inlineFieldResourceURI - resourceURI for the imported inline field entry.
 */

/**
 * Check if there are any extensions for any node in graph
 *
 * @param {Graph} graph - graph containing metadata
 * @returns {boolean}
 */
const hasExtensions = (graph) => {
  const extension = graph.findFirstValue(null, RDF_PROPERTY_EXTENDS);
  if (extension) {
    return true;
  }
  return false;
};

/**
 * Checks if graph contains statements matching predicate and value
 *
 * @param {Graph} graph - graph containing metadata
 * @param {string} predicate - predicate relation
 * @param {string} value - value of predicate
 * @returns {boolean}
 */
const includesValue = (graph, predicate, value) => {
  const statements = graph.find(null, predicate, value);
  return statements.length > 0;
};

/**
 * Checks if graph contains a relation to field type value
 *
 * @param {Graph} graph - graph containing metadata
 * @returns {boolean}
 */
const isField = (graph) => {
  return includesValue(graph, 'rdf:type', RDF_TYPE_FIELD);
};

/**
 * Checks if graph contains a relation to form type value
 *
 * @param {Graph} graph - graph containing metadata
 * @returns {boolean}
 */
const isForm = (graph) => {
  return includesValue(graph, 'rdf:type', RDF_TYPE_FORM);
};

/**
 * Checks if graph matches property form properties. An object form which also
 * has a property, was previously detected as the now deprecated property form.
 * Since rdforms rather understands templates corresponding to property forms,
 * than the combination of object form and inline field, this needs to be
 * handled when importing templates not exported from Models.
 *
 * @param {Graph} graph - metadata graph
 * @returns {boolean}
 */
const isPropertyFormLike = (graph) => {
  if (!isObjectForm(graph, null)) return false;
  return Boolean(graph.findFirstValue(null, RDF_PROPERTY_PROPERTY));
};

/**
 * Checks if graph is considered stable
 *
 * @param {Graph} graph - graph containing metadata
 * @returns {boolean}
 */
const isStable = (graph) => {
  return includesValue(graph, RDF_PROPERTY_STATUS, RDF_PROPERTY_STATUS_STABLE);
};

/**
 * Find a suitable title
 *
 * @param {object} template
 * @returns {string} - title
 */
const getTitle = (template) => {
  const titleKeys = ['id', 'property', 'label', 'extends'];
  const titleKey = titleKeys.find((key) => key in template);
  const getLanguage = (label) => {
    if ('en' in label) {
      return label.en;
    }
    if ('sv' in label) {
      return label.sv;
    }
  };
  switch (titleKey) {
    case 'id':
      return template.id.replaceAll(':', '_');
    case 'property':
      return template.property.replaceAll(':', '_');
    case 'label':
      return getLanguage(template.label).replaceAll(':', '_');
    case 'extends':
      return `${template.extends}-extension`.replaceAll(':', '_');
    default:
      throw new Error('No title found for form item');
  }
};

/**
 * Sets an id using title
 *
 * @param {Graph} graph - graph containing metadata
 * @param {string} title - the title used in the graph
 */
const setIdentifier = (graph, title) => {
  const uniqueID = getUniqueId(`${title}_`);
  graph.addL(title, 'dc:identifier', uniqueID);
};

let inlineIdCounter = 0;
const newInlineId = () => {
  inlineIdCounter += 1;
  return `inlineId_${inlineIdCounter}`;
};

/**
 * Add form items to graph and extract list of templates that are of type object,
 * since these needs conversion into independent graphs.
 *
 * @param {(object|string)[]} formItemTemplates
 * @param {Graph} graph
 * @param {string} title - title used in graph
 * @returns {object[]} templatesToConvert
 */
const setFormItems = (formItemTemplates, graph, title) => {
  const templatesToConvert = [];
  for (const [index, template] of Object.entries(formItemTemplates)) {
    const stmt = graph.add(title, RDF_PROPERTY_ITEM);
    const blankId = stmt.getValue();
    graph.add(blankId, 'rdf:type', RDF_CLASS_EXTENSION);
    graph.addD(blankId, RDF_PROPERTY_ORDER, index, 'xsd:integer');

    if (typeof template === 'string') {
      const extensionTitle = template.replaceAll(':', '_');
      graph.addL(blankId, RDF_PROPERTY_EXTENDS, extensionTitle);
    } else if (typeof template === 'object') {
      if (template.id && Object.keys(template).length === 1) {
        const extensionId = template.id.replaceAll(':', '_');
        graph.addL(blankId, RDF_PROPERTY_EXTENDS, extensionId);
      } else if (template.id) {
        // Assume inline definition with id
        const extensionId = template.id.replaceAll(':', '_');
        graph.addL(blankId, RDF_PROPERTY_EXTENDS, extensionId);
        templatesToConvert.push(template);
      } else if (!template.extends) {
        // Assume inline definition without id
        template.id = newInlineId();
        graph.addL(blankId, RDF_PROPERTY_EXTENDS, template.id);
        templatesToConvert.push(template);
      } else {
        // Extends
        const extensionId = template.extends.replaceAll(':', '_');
        graph.addL(blankId, RDF_PROPERTY_EXTENDS, extensionId);

        graphSetters.text(template, graph, blankId);
        graphSetters.label(template, graph, blankId);
        graphSetters.editlabel(template, graph, blankId);
        graphSetters.description(template, graph, blankId);
        graphSetters.editdescription(template, graph, blankId);
        graphSetters.placeholder(template, graph, blankId);
        graphSetters.help(template, graph, blankId);
        graphSetters.specification(template, graph, blankId);
        graphSetters.placeholder(template, graph, blankId);
        graphSetters.cardinality(template, graph, blankId);
        graphSetters.styles(template, graph, blankId);
      }
    }
  }
  return templatesToConvert;
};

const rdfTypes = [
  RDF_TYPE_TEXT_FIELD,
  RDF_TYPE_DATATYPE_FIELD,
  RDF_TYPE_SELECT_FIELD,
  RDF_TYPE_LOOKUP_FIELD,
  RDF_TYPE_INLINE_FIELD,
  RDF_TYPE_PROFILE_FORM,
  RDF_TYPE_OBJECT_FORM,
  RDF_TYPE_SECTION_FORM,
];

/**
 *
 * @param {object} about
 * @returns {string|undefined}
 */
const findTypeFromAbout = (about = {}) => {
  if (!about.rdfType) return;
  return rdfTypes.find((rdfType) => {
    return about.rdfType.includes(ns.expand(rdfType));
  });
};

/**
 * Identifies rdf type. First try to find explicit rdf type if set in about. If
 * not set explicitely, detect the type from other template properties. The
 * latter case is commonly used for templates not exported from Models, which
 * normally doesn't have the about property.
 *
 * @param {template} template - RDForms template
 * @param {object} extendedTemplate - RDForms template that has been extended
 * @returns {string} the determined form class
 */
const getRdfType = (template, extendedTemplate) => {
  const {
    about,
    type,
    property,
    choices,
    items,
    constraints,
    automatic,
    nodetype,
  } = { ...extendedTemplate, ...template };

  const definedType = findTypeFromAbout(about);
  if (definedType) return definedType;

  if (type === 'choice') {
    if (choices) return RDF_TYPE_SELECT_FIELD;
    return RDF_TYPE_LOOKUP_FIELD;
  }
  if (type === 'text') {
    if (nodetype === 'DATATYPE_LITERAL') return RDF_TYPE_DATATYPE_FIELD;
    return RDF_TYPE_TEXT_FIELD;
  }
  if (type === 'group') {
    // Note, current code cannot distinguish between header and profile.
    // Hence, header groups will be created as profiles.
    // Potential fix is to assume all profiles has a constraint and / or a nodetype.
    if (property) {
      // Handle auto generated templates as groups by mistake
      if (constraints && automatic && (!items || items.length === 0)) {
        return RDF_TYPE_LOOKUP_FIELD;
      }
      return RDF_TYPE_PROPERTY_FORM;
    }
    if (constraints || nodetype) {
      return RDF_TYPE_PROFILE_FORM;
    }
    return RDF_TYPE_SECTION_FORM;
  }
  // Fallback, if no type is given, assume it is a text field.
  return RDF_TYPE_TEXT_FIELD;
};

/**
 * Sets class for graph
 *
 * @param {object} template - RDForms template
 * @param {Graph} graph - graph containing metadata
 * @param {string} title - title used in graph
 * @param {object} extendedTemplate - RDForms template that has been extended
 */
const setRdfType = (template, graph, title, extendedTemplate) => {
  const rdfType = getRdfType(template, extendedTemplate);
  switch (rdfType) {
    case RDF_TYPE_TEXT_FIELD:
      graph.add(title, 'rdf:type', RDF_TYPE_FIELD);
      graph.add(title, 'rdf:type', RDF_TYPE_TEXT_FIELD);
      break;
    case RDF_TYPE_DATATYPE_FIELD:
      graph.add(title, 'rdf:type', RDF_TYPE_FIELD);
      graph.add(title, 'rdf:type', RDF_TYPE_DATATYPE_FIELD);
      break;
    case RDF_TYPE_PROFILE_FORM:
      graph.add(title, 'rdf:type', RDF_TYPE_FORM);
      graph.add(title, 'rdf:type', RDF_TYPE_PROFILE_FORM);
      break;
    case RDF_TYPE_OBJECT_FORM:
      graph.add(title, 'rdf:type', RDF_TYPE_FORM);
      graph.add(title, 'rdf:type', RDF_TYPE_OBJECT_FORM);
      break;
    case RDF_TYPE_PROPERTY_FORM:
      graph.add(title, 'rdf:type', RDF_TYPE_FORM);
      graph.add(title, 'rdf:type', RDF_TYPE_OBJECT_FORM);
      break;
    case RDF_TYPE_SECTION_FORM:
      graph.add(title, 'rdf:type', RDF_TYPE_FORM);
      graph.add(title, 'rdf:type', RDF_TYPE_SECTION_FORM);
      break;
    case RDF_TYPE_SELECT_FIELD:
      graph.add(title, 'rdf:type', RDF_TYPE_FIELD);
      graph.add(title, 'rdf:type', RDF_TYPE_SELECT_FIELD);
      break;
    case RDF_TYPE_LOOKUP_FIELD:
      graph.add(title, 'rdf:type', RDF_TYPE_FIELD);
      graph.add(title, 'rdf:type', RDF_TYPE_LOOKUP_FIELD);
      break;
    case RDF_TYPE_INLINE_FIELD:
      graph.add(title, 'rdf:type', RDF_TYPE_FIELD);
      graph.add(title, 'rdf:type', RDF_TYPE_INLINE_FIELD);
      break;
    default:
  }
};

/**
 *
 * @param {Graph} graph - graph containing metadata
 * @param {string} title - title used in graph
 */
const setStatus = (graph, title) => {
  graph.add(title, RDF_PROPERTY_STATUS, RDF_PROPERTY_STATUS_STABLE);
};

/**
 * Detect forms in data
 *
 * @param {object} data - JSON data from file or link
 * @returns {object[]} identified form items
 */
const detectTemplates = (data) => {
  if ('property' in data) return [data];
  const dataKeys = Object.keys(data);
  let root;
  if (dataKeys.includes('templates')) {
    root = data.templates;
  } else if (dataKeys.length === 1) {
    root = data[dataKeys[0]];
  }
  if (
    Array.isArray(root) &&
    ('property' in root[0] || 'label' in root[0] || 'id' in root[0])
  ) {
    return root;
  }
  if (typeof root === 'object' && root !== null) return [root];
  throw new Error('No templates detected in provided link or file');
};

/**
 * Generates graph from a data object
 *
 * @param {object} template - RDForms template
 * @param {object} extendedTemplate - extended RDForms template
 * @returns {object|undefined}
 */
const createImportItem = (template, extendedTemplate) => {
  const graph = new Graph();

  const title = getTitle(template); // TODO: rename to getId and use id instead of title
  graph.addL(title, 'dcterms:identifier', title);
  if (!template.text?.title) {
    graph.addL(title, 'dcterms:title', title);
  }

  setIdentifier(graph, title);
  setRdfType(template, graph, title, extendedTemplate);
  if (template.extends) {
    const extensionTitle = template.extends.replaceAll(':', '_');
    graph.addL(title, RDF_PROPERTY_EXTENDS, extensionTitle);
  }

  Object.keys(graphSetters).forEach((key) => {
    if (!(key in template) && key !== 'text') return;
    graphSetters[key](template, graph, title);
  });

  let formItems = [];
  if ('items' in template && template.items.length > 0) {
    const items = setFormItems(template.items, graph, title);
    formItems = formItems.concat(items);
  }

  setStatus(graph, title);
  graphSetters.cardinality(template, graph, title);

  return {
    graph,
    title,
    formItems,
  };
};

/**
 * Summation of details of all graphs.
 *
 * @param {RfsImportItem[]} importItems
 * @returns {object}
 */
const getStatistics = (importItems) => {
  let graphsWithExtension = 0;
  let fieldsCount = 0;
  let formsCount = 0;
  let stable = 0;

  importItems.forEach((importItem) => {
    const { graph } = importItem;
    if (hasExtensions(graph)) graphsWithExtension += 1;
    if (isField(graph)) fieldsCount += 1;
    if (isForm(graph)) formsCount += 1;
    if (isStable(graph)) stable += 1;
  });

  return {
    graphsWithExtension,
    fieldsCount,
    formsCount,
    stable,
  };
};

/**
 * Find form extensions of graphs.
 *
 * @param {RfsImportItem[]} importItems
 * @returns {object[]} formItemTemplates
 */
const getFormItemTemplates = (importItems) => {
  let formItemTemplates = [];

  importItems.forEach(({ formItems }) => {
    if (formItems.length !== 0) {
      formItemTemplates = formItemTemplates.concat(formItems);
    }
  });
  return formItemTemplates;
};

/**
 * Find parent template, in case the template is extending. If the template is
 * extending more than one, the extended templates are merged.
 *
 * @param {object} template
 * @param {object[]} templates
 * @param {object|undefined} extendedTemplates
 * @returns {undefined|object}
 */
const findExtendedTemplate = (template, templates, extendedTemplates) => {
  if (!template.extends) return extendedTemplates;
  const extendedTemplate = templates.find(({ id }) => {
    return template.extends === id;
  });
  if (!extendedTemplate) {
    console.log(`Could not find extended item ${template.extends}`);
    return extendedTemplates;
  }
  if (extendedTemplate.extends)
    return findExtendedTemplate(extendedTemplate, templates, {
      ...template,
      ...extendedTemplates,
    });
  return { ...extendedTemplate, ...extendedTemplates };
};

/**
 * Map form items into graph items. In case a template is extending another
 * template, the extended template is required to have access to all values for
 * proper detection.
 *
 * @param {object[]} templates
 * @returns {RfsImportItem[]} importItems
 */
const convertTemplatesToImportItems = (templates) => {
  const importItems = [];
  templates.forEach((template) => {
    const extendedTemplate = findExtendedTemplate(template, templates);
    const importItem = createImportItem(template, extendedTemplate);
    importItems.push(importItem);
  });
  return importItems;
};

/**
 * Collect all nested extensions (form items) by iterative process
 *
 * @param {object[]} templates
 * @returns {object[]} graphExtensionsWithMetadata - extensions converted to graphs
 */
const mapExtensionTemplatesToImportItems = (templates) => {
  const graphExtensionItems = convertTemplatesToImportItems(templates);
  let formItemTemplates = getFormItemTemplates(graphExtensionItems);

  while (formItemTemplates.length > 0) {
    const importItems = convertTemplatesToImportItems(formItemTemplates);
    formItemTemplates = getFormItemTemplates(importItems);
    graphExtensionItems.concat(importItems);
  }
  return graphExtensionItems;
};

/**
 * Convert RDForms JSON bundle into graphs
 *
 * @param {object} rdformsBundle - RDForms JSON bundle
 * @returns {object}
 */
export const convertBundleToImportData = (rdformsBundle) => {
  const templates = detectTemplates(rdformsBundle);
  let importItems = convertTemplatesToImportItems(templates);
  const formItems = getFormItemTemplates(importItems);
  const stats = getStatistics(importItems);
  const extensionItems = mapExtensionTemplatesToImportItems(formItems);
  importItems = importItems.concat(extensionItems);
  return { importItems, formItems, stats };
};

/**
 *
 * @param {Statement} extendStatement
 * @param {Entry} entry
 * @param {Map} id2ResourceURI
 * @returns {Promise}
 */
const findExtendResourceURI = async (
  extendStatement,
  entry,
  id2ResourceURI
) => {
  const resourceURI = id2ResourceURI.get(extendStatement.getValue());
  if (resourceURI) return resourceURI;

  console.log(
    `Trying to find field or form with id "${extendStatement.getValue()}"`
  );
  const extendedEntries = await entrystore
    .newSolrQuery()
    .context(entry.getContext())
    .limit(5)
    .literalProperty(
      'dcterms:identifier',
      extendStatement.getValue(),
      undefined,
      'string'
    )
    .getEntries();
  if (extendedEntries.length === 1) return extendedEntries[0].getResourceURI();
  if (extendedEntries.length > 1) {
    console.log(
      `Multiple fields or forms in current project matching "${extendStatement.getValue()}", giving up.`
    );
    return;
  }
  if (extendedEntries.length === 0) {
    const extendedEntriesOtherContexts = await entrystore
      .newSolrQuery()
      .limit(5)
      .literalProperty(
        'dcterms:identifier',
        extendStatement.getValue(),
        undefined,
        'string'
      )
      .getEntries();
    if (extendedEntriesOtherContexts.length === 1) {
      return extendedEntriesOtherContexts[0].getResourceURI();
    }

    console.log(
      `Found ${
        extendedEntriesOtherContexts.length
      } fields or forms in other project matching "${extendStatement.getValue()}", don't know which to connect to, giving up.`
    );
  }
};

/**
 * Remove current extends value and set resource uri as new value. Inline fields
 * uses a different property to reference an object form. For this case, the
 * inline property is set instead of extends.
 *
 * @param {string} uri
 * @param {Statement} extendStatement
 * @param {Entry} entry
 * @returns {Promise}
 */
const updateExtend = async (uri, extendStatement, entry) => {
  const metadata = entry.getMetadata();
  metadata.remove(extendStatement);

  if (isInlineField(entry)) {
    const extendedEntry = await entrystore.getEntry(
      entrystore.getEntryURIFromURI(uri)
    );
    // only apply inlines if the inline field extends an object form
    if (isObjectForm(extendedEntry.getMetadata(), null)) {
      metadata.add(extendStatement.getSubject(), RDF_PROPERTY_INLINE, uri);
      return;
    }
  }
  metadata.add(extendStatement.getSubject(), RDF_PROPERTY_EXTENDS, uri);
};

/**
 * Update extensions of imported entries
 *
 * @param {RfsImportItem[]} importItems
 * @returns {Promise}
 */
export const updateExtensions = async (importItems) => {
  const id2ResourceURI = new Map();
  importItems.forEach((importItem) => {
    id2ResourceURI.set(
      importItem.title,
      importItem.inlineFieldResourceURI || importItem.resourceURI
    );
  });
  let updates = 0;
  let broken = 0;
  const brokenList = [];
  await promiseUtil.forEach(importItems, async (importItem) => {
    const metadata = importItem.entry.getMetadata();
    const extendStatements = metadata.find(null, RDF_PROPERTY_EXTENDS);
    let changed = false;
    if (extendStatements.length > 0) {
      await promiseUtil.forEach(extendStatements, async (extendStatement) => {
        const extendResourceURI = await findExtendResourceURI(
          extendStatement,
          importItem.entry,
          id2ResourceURI
        );

        if (extendResourceURI) {
          await updateExtend(
            extendResourceURI,
            extendStatement,
            importItem.entry
          );
          updates += 1;
          changed = true;
          return;
        }

        broken += 1;
        brokenList.push(extendStatement.getValue());
      });
    }
    if (changed) {
      return importItem.entry.commitMetadata();
    }
  });
  return { updates, broken, brokenList };
};

/**
 * Create and commit entry from graph
 *
 * @param {RfsImportItem} importItem
 * @param {Context} context
 * @param {string} id
 * @returns {Promise<Entry>}
 */
const createEntryFromGraph = (importItem, context, id) => {
  const { graph } = importItem;
  const prototypEntry = context.newEntry();
  const prototypEntryResourceURI = prototypEntry.getResourceURI();
  graph.replaceSubject(importItem.title, prototypEntryResourceURI);
  if (id && id !== importItem.title) {
    graph.findAndRemove(null, 'dcterms:title');
    graph.addL(prototypEntryResourceURI, 'dcterms:title', id);
  }

  prototypEntry.setMetadata(graph);
  return prototypEntry.commit().then((entry) => {
    importItem.resourceURI = entry.getResourceURI();
    importItem.entry = entry;
    return entry;
  });
};

/**
 * Splits rdforms property form config to create inline field and object form
 * entry.
 *
 * @param {RfsImportItem} importItem
 * @param {Context} context
 * @returns {Promise<Entry>}
 */
const createInlineFieldAndObjectFormEntry = async (importItem, context) => {
  const { graph } = importItem;
  // extract property from form for inline field
  const property = graph.findFirstValue(null, RDF_PROPERTY_PROPERTY);
  graph.findAndRemove(null, RDF_PROPERTY_PROPERTY);

  // create and commit object form entry
  const objectFormEntry = await createEntryFromGraph(
    importItem,
    context,
    `${importItem.title}_object_form`
  );
  const resourceURI = objectFormEntry.getResourceURI();
  const { includeLevel, max } = getCardinality(graph, resourceURI);

  // create inline field entry with reference to object form
  const inlineFieldEntryPE = context.newEntry();
  inlineFieldEntryPE
    .add(RDF_PROPERTY_PROPERTY, property)
    .addL('dcterms:title', importItem.title)
    .add('rdf:type', RDF_TYPE_INLINE_FIELD)
    .add('rdf:type', RDF_TYPE_FIELD)
    .add(RDF_PROPERTY_INLINE, resourceURI)
    .add(RDF_PROPERTY_STATUS, RDF_PROPERTY_STATUS_STABLE);
  setCardinality(
    inlineFieldEntryPE.getMetadata(),
    inlineFieldEntryPE.getResourceURI(),
    includeLevel,
    max
  );
  const inlineFieldEntry = await inlineFieldEntryPE.commit();
  importItem.inlineFieldResourceURI = inlineFieldEntry.getResourceURI();
};

/**
 * Create entries from graphs
 *
 * @param {RfsImportItem[]} importItems
 * @param {Function} getUpdateProgressDialog - update progressbar for imported forms
 * @param {object} context - context object
 * @returns {Promise}
 */
export const importGraphs = async (
  importItems,
  getUpdateProgressDialog,
  context
) => {
  const updateProgressPerForm = getUpdateProgressDialog(importItems.length);
  await promiseUtil.forEach(importItems, (importItem) => {
    const { graph } = importItem;
    if (isPropertyFormLike(graph)) {
      return createInlineFieldAndObjectFormEntry(importItem, context).then(
        updateProgressPerForm
      );
    }
    if (isForm(graph) || isField(graph)) {
      return createEntryFromGraph(importItem, context).then(
        updateProgressPerForm
      );
    }
  });
};
