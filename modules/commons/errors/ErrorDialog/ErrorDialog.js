import PropTypes from 'prop-types';
import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Button,
  Dialog,
  DialogContent,
  DialogActions,
  Paper,
  Typography,
} from '@mui/material';
import {
  Warning as WarningIcon,
  ArrowDropDown as ArrowDropDownIcon,
} from '@mui/icons-material';
import { useTranslation } from 'commons/hooks/useTranslation';
import escoErrors from 'commons/nls/escoErrors.nls';
import './ErrorDialog.scss';

const ContentRow = ({ children }) => (
  <div className="escoErrorDialog__row">{children}</div>
);
ContentRow.propTypes = {
  children: PropTypes.node,
};

const ErrorAccordion = ({ errorDetails }) => {
  const t = useTranslation(escoErrors);

  return (
    <Accordion elevation={0} classes={{ root: 'escoErrorDialogAccordion' }}>
      <AccordionSummary
        expandIcon={<ArrowDropDownIcon color="secondary" />}
        aria-controls="accordion-details"
        classes={{
          root: 'escoErrorDialogAccordionSummary',
          expanded: 'escoErrorDialogAccordionSummary--expanded',
        }}
      >
        <Typography>{t('showDetails')}</Typography>
      </AccordionSummary>
      <AccordionDetails
        id="error-details"
        classes={{ root: 'escoErrorDialogAccordionDetails' }}
      >
        <Paper
          elevation={0}
          classes={{ root: 'escoErrorDialogAccordionDetails__paper' }}
        >
          <Typography className="escoErrorDialogAccordionDetails__label">
            {errorDetails}
          </Typography>
        </Paper>
      </AccordionDetails>
    </Accordion>
  );
};

ErrorAccordion.propTypes = {
  errorDetails: PropTypes.string,
};

const ErrorIcon = () => <WarningIcon className="escoErrorDialog__icon" />;

export const ErrorDialog = ({
  affirmLabel,
  closeLabel,
  errorDetails,
  errorText,
  onClose = () => {},
  open,
  errorIcon = <ErrorIcon />,
}) => {
  const t = useTranslation(escoErrors);

  return (
    <Dialog
      className="escoErrorDialog"
      fullWidth
      maxWidth="xs"
      open={open}
      onBackdropClick={onClose}
    >
      <DialogContent dividers classes={{ root: 'escoErrorDialog__content' }}>
        <ContentRow>{errorIcon}</ContentRow>
        <ContentRow>
          <Typography paragraph>{errorText}</Typography>
        </ContentRow>
        {errorDetails ? <ErrorAccordion errorDetails={errorDetails} /> : null}
      </DialogContent>
      <DialogActions className="escoErrorDialog__actions">
        <Button variant="text" onClick={onClose} autoFocus={!affirmLabel}>
          {closeLabel || t('closeDialog')}
        </Button>
        {affirmLabel ? (
          <Button onClick={() => onClose(true)} autoFocus>
            {affirmLabel}
          </Button>
        ) : null}
      </DialogActions>
    </Dialog>
  );
};

ErrorDialog.propTypes = {
  affirmLabel: PropTypes.string,
  closeLabel: PropTypes.string,
  errorDetails: PropTypes.string,
  errorText: PropTypes.string,
  onClose: PropTypes.func,
  open: PropTypes.bool,
  errorIcon: PropTypes.node,
};
