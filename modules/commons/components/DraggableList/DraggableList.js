import React from 'react';
import { DragDropContext, Droppable } from 'react-beautiful-dnd';
import PropTypes from 'prop-types';
import { List } from '@mui/material';

const reorder = (Items, sourceIndex, destinationIndex) => {
  const reorderedItems = [...Items];
  const [removed] = reorderedItems.splice(sourceIndex, 1);
  reorderedItems.splice(destinationIndex, 0, removed);
  return reorderedItems;
};

const DraggableList = ({ onDragEnd, children }) => {
  return (
    <DragDropContext
      onDragEnd={({ source, destination }) =>
        onDragEnd(reorder, source, destination)
      }
    >
      <Droppable droppableId="droppable-list">
        {(provided) => (
          <List
            ref={provided.innerRef}
            {...provided.droppableProps}
            style={{ overflow: 'auto' }}
          >
            {children}
            {provided.placeholder}
          </List>
        )}
      </Droppable>
    </DragDropContext>
  );
};

DraggableList.propTypes = {
  onDragEnd: PropTypes.func,
  children: PropTypes.node,
};

export { DraggableList };
