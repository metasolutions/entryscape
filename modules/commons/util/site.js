import config from 'config';
import { Context } from '@entryscape/entrystore-js';
import { generatePath } from 'react-router-dom';
import registry from 'commons/registry';
import { entrystore } from 'commons/store';
import Lookup from 'commons/types/Lookup';
import { EntityType } from 'entitytype-lookup';
import {
  getEntityTypesIncludingRefined,
  getBuiltinEntityTypes,
} from 'commons/types/utils/entityType';
import {
  ADMIN_PERMISSION,
  ADV_ADMIN_PERMISSION,
  GUEST_PERMISSION,
} from 'commons/hooks/useUserPermission';

/**
 *
 * @returns {string}
 */
export const getCurrentViewName = () => {
  return registry.getCurrentView();
};

/**
 *
 * @param {string} viewName
 * @returns {object} viewDef
 */
export const getViewDefFromName = (viewName) =>
  config.get('site.views').find(({ name }) => viewName === name);

/**
 *
 * @param {string} routeStr
 * @returns {object} viewDef
 */
export const getViewDefFromRoute = (routeStr) =>
  config.get('site.views').find(({ route }) => route === routeStr);

/**
 * Find the route from a view.
 *
 * @param {string} viewName
 * @returns {string|null|undefined}
 */
export const getRouteFromView = (viewName) => {
  const viewDef = getViewDefFromName(viewName);
  if (!viewDef) return;
  return viewDef.route;
};

/**
 * Get the top level view for each module. Usually to be rendered in the left sidebar
 *
 * @param {*} module
 * @returns {object[]}
 */
export const getTopViewsOfModule = (module = null) => {
  const modulesDef = module ? [module] : config.get('modules');

  /**
   * For each module get the views that are top level (i.e don't have a parent)
   *
   * @type {Array}
   */
  return modulesDef.map(({ name }) =>
    config
      .get('site.views')
      .filter(
        ({ module: moduleName, parent }) => moduleName === name && !parent
      )
  );
};

/**
 * Find in which module a certain view belongs.
 * Some views belong to no module, e.g start view
 *
 * @param {object|string} view
 * @returns {object|null}
 */
export const getModuleOfView = (view) => {
  let moduleName;
  if (typeof view === 'object') {
    moduleName = view.module;
  } else {
    const viewDef = getViewDefFromName(view);
    moduleName = viewDef?.module || '';
  }

  return (
    config.get('site.modules').find(({ name }) => name === moduleName) || null
  );
};

/**
 *
 * @returns {string|undefined}
 */
export const getModuleNameOfCurrentView = () => {
  const currentView = registry.getCurrentView();
  const viewDefinition = getViewDefFromName(currentView);
  return viewDefinition ? viewDefinition.module : undefined;
};

/**
 * Get all subviews that have a given view as parent. Use recursive for more than one level deep.
 *
 * @param {string} viewName
 * @param {boolean} recursive - defaults to false
 * @returns {Array}
 */
export const getSubviewsOfView = (viewName, recursive = false) => {
  const views = config
    .get('site.views')
    .filter(({ parent, navbar }) => parent === viewName && navbar !== false);

  return recursive && views.length > 0
    ? views.concat(views.map(getSubviewsOfView))
    : views;
};

/**
 * Top level means anything one level below the starting view level
 *
 * @param {string} matchedRoute
 * @returns {object[]}
 */
export const getTopLevelViewsByRoute = (matchedRoute) => {
  const viewDef = getViewDefFromRoute(matchedRoute);
  if (!viewDef.parent) {
    return [];
  }

  const { startView } = getModuleOfView(viewDef);
  return getSubviewsOfView(startView);
};

export const getBreadcrumbViews = ({ parent, name }, arr = []) => {
  if (parent) {
    const parentView = getViewDefFromName(parent);
    getBreadcrumbViews(parentView, arr);
  }

  arr.push(name);

  return arr;
};

/**
 * Get only the views that have a certain truthy value of a property,
 * e.g filterViewsByAttribute([], 'navbar');
 *
 * @param {object[]} views
 * @param {string } filter
 * @returns {object[]}
 */
export const filterViewsByAttribute = (views, filter) =>
  views.filter((v) => (filter in v ? v[filter] : true));

/**
 *
 * @param {string} viewName
 * @param {object} params
 * @returns {string} materialized path
 */
export const getPathFromViewName = (viewName, params = {}) =>
  generatePath(getRouteFromView(viewName), params);

/**
 *
 * @param {string} viewName
 * @param {object} params
 * @returns {string} materialized path
 */
export const getParentPathFromViewName = (viewName, params = {}) => {
  const viewDefinition = getViewDefFromName(viewName);
  return getPathFromViewName(viewDefinition?.parent, params);
};

/**
 *
 * @param {string} viewName
 * @returns {string|undefined}
 */
export const getParentViewName = (viewName) => {
  const viewDefinition = getViewDefFromName(viewName);
  return viewDefinition?.parent;
};

/**
 *
 * @param {string} route
 * @returns {number} 0|1|2
 */
export const getNrOfParamsInRoute = (route) =>
  route.startsWith('http') ? 0 : (route.match(/:/g) || []).length;

/**
 * Basic check for view rights
 *
 * @param {string} userPermission
 * @param {string} restrictTo
 * @returns {boolean}
 */
const genericCheckPermission = (userPermission, restrictTo) => {
  if (restrictTo === GUEST_PERMISSION) return true;
  if (restrictTo === userPermission) return true;
  if (
    restrictTo === ADMIN_PERMISSION &&
    userPermission === ADV_ADMIN_PERMISSION
  ) {
    return true;
  }
  return false;
};

/**
 * Check for sufficient view rights
 *
 * @param {string} userPermission
 * @param {string} restrictTo
 * @param {object} module - module definition
 * @returns {boolean}
 */
export const checkHasViewPermission = (userPermission, restrictTo, module) => {
  if (!restrictTo) return true;

  const hasPermission = genericCheckPermission(userPermission, restrictTo);
  const moduleCheckPermission = module?.checkPermission;

  if (!moduleCheckPermission) return hasPermission;
  return moduleCheckPermission(userPermission, restrictTo, hasPermission);
};

/**
 *
 * @param {string} matchedRoute
 * @returns {object[]}
 */
export const getSecondaryNavViews = (matchedRoute) => {
  const viewDef = getViewDefFromRoute(matchedRoute);
  const topLevelViews = getTopLevelViewsByRoute(matchedRoute);

  /**
   * We don't want to use the module's startView for the dataset preview,
   * which is the only child of 'catalog__datasets__dataset'
   */
  const hasSpecialTreatment = ['catalog__datasets__dataset'];
  const secondaryNavViews = hasSpecialTreatment.includes(viewDef.parent)
    ? getSubviewsOfView(viewDef.parent)
    : topLevelViews.filter((v) => v.navbar !== false);

  return secondaryNavViews;
};

/**
 * Matches entity types saved in context with available entity types. If entity
 * types are saved in context, all other available entity types should be
 * excluded. An exception is made for refined types e.g. dataset if geodataset is included.
 *
 * @param {Context} context
 * @returns {Promise<EntityType[]>}
 */
const getEntityTypesToExclude = async (context) => {
  const entityTypesInContext = await Lookup.getPrimaryInContext(context);

  // if empty, project type is not used
  if (!entityTypesInContext.length) return;

  const projectType = Lookup.getProjectTypeInUse(context, true);
  const userInfo = await entrystore.getUserInfo();
  const moduleName = getModuleNameOfCurrentView();
  const builtinEntityTypes = getBuiltinEntityTypes(userInfo, moduleName);
  const entityTypes = projectType?.getIncludeBuiltins()
    ? builtinEntityTypes
    : entityTypesInContext;
  const typesIncludingRefined = getEntityTypesIncludingRefined(entityTypes);
  const availableEntityTypes = Lookup.getEntityTypes();
  return availableEntityTypes.filter(
    (entityType) => !typesIncludingRefined.includes(entityType)
  );
};

/**
 * Exclude views that should be hidden based on entity types. Assumes site views and
 * entity types are named in consistent way.
 * For example if a view is named models__fields, the entity type name should be
 * fields, to be considered in the enity type matching.
 *
 * @param {object[]} secondaryNavViews
 * @param {EntityType[]} entityTypesToExlude
 * @returns {object[]}
 */
const filterViewsOnEntitytypes = async (
  secondaryNavViews,
  entityTypesToExlude
) => {
  if (!entityTypesToExlude) return secondaryNavViews;

  return secondaryNavViews.filter(({ name }) => {
    return !entityTypesToExlude.find(
      (entityType) => entityType.get('listview') === name
    );
  });
};

/**
 * Get avaiable secondary nav views. Exclude views that should not be available
 * based on entity and project type.
 *
 * @param {string} path
 * @param {string} contextId
 * @returns {Promise<object[]>}
 */
export const getSecondaryNavViewsFromEntityTypes = async (path, contextId) => {
  if (!contextId) return getSecondaryNavViews(path);
  const context = entrystore.getContextById(contextId);
  const entityTypesToExlude = await getEntityTypesToExclude(context);
  const secondaryNavViews = getSecondaryNavViews(path);
  const views = filterViewsOnEntitytypes(
    secondaryNavViews,
    entityTypesToExlude
  );
  return views;
};
