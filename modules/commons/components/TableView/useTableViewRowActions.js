import { useEffect } from 'react';
import useAsync from 'commons/hooks/useAsync';
import { useSnackbar } from 'commons/hooks/useSnackbar';
import { useTranslation } from 'commons/hooks/useTranslation';
import escoTableViewNLS from 'commons/nls/escoTableView.nls';
import {
  saveRow,
  discardRow,
  loadRowChoices,
  bindToRow,
} from './util/rowUtils';

const useTableViewRowActions = (dispatch) => {
  const { runAsync, error } = useAsync();
  const [addSnackbar] = useSnackbar();
  const translate = useTranslation(escoTableViewNLS);

  useEffect(() => {
    if (!error) return;
    dispatch({ type: 'ERROR', value: { error } });
  }, [dispatch, error]);

  const handleSaveRow = (row, callback = () => {}) =>
    runAsync(
      saveRow(row).then(() => {
        dispatch({ type: 'SAVE_ROW', value: { rowId: row.id } });
        callback();
        addSnackbar({ message: translate('saveRowNotification') });
      })
    );

  const handleDiscardRow = (row, callback = () => {}) =>
    runAsync(
      discardRow(row).then(() => {
        dispatch({
          type: 'DISCARD_ROW',
          value: { rowId: row.id },
        });
        callback();
        addSnackbar({ message: translate('discardRowNotification') });
      })
    );

  const saveSelectedRows = (selectedRows) => {
    runAsync(
      Promise.all(
        selectedRows.map((selectedRow) => {
          selectedRow.entry.setMetadata(selectedRow.graph);
          return selectedRow.entry
            .commitMetadata()
            .then(() => selectedRow.graph.setChanged(false));
        })
      ).then(() => {
        dispatch({ type: 'SAVE_SELECTED_ROWS' });
        addSnackbar({ message: translate('saveNotification') });
        dispatch({ type: 'REFRESH_TABLE' });
      })
    );
  };

  const discardSelectedRows = (selectedRows) => {
    selectedRows.forEach((selectedRow) => {
      selectedRow.graph = selectedRow.entry.getMetadata().clone();
      bindToRow(selectedRow);
    });

    runAsync(
      loadRowChoices(selectedRows).then(() => {
        addSnackbar({ message: translate('discardNotification') });
        dispatch({ type: 'DISCARD_SELECTED_ROWS' });
      })
    );
  };

  return {
    handleSaveRow,
    handleDiscardRow,
    saveSelectedRows,
    discardSelectedRows,
  };
};

export default useTableViewRowActions;
