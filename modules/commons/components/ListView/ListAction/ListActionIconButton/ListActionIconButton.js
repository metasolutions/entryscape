import PropTypes from 'prop-types';
import { IconButton } from '@mui/material';
import Tooltip from 'commons/components/common/Tooltip';
import ConditionalWrapper from 'commons/components/common/ConditionalWrapper';

// Generic action button with icon only
export const ListActionIconButton = ({
  ariaLabel,
  onClick,
  icon,
  disabled,
  tooltip,
  highlightId,
}) => {
  return (
    <ConditionalWrapper
      condition={Boolean(tooltip)}
      wrapper={(children) => (
        <Tooltip title={tooltip}>
          <span>{children}</span>
        </Tooltip>
      )}
    >
      <IconButton
        aria-label={ariaLabel}
        disabled={disabled}
        onClick={onClick}
        color="primary"
        data-highlight-id={highlightId}
      >
        {icon}
      </IconButton>
    </ConditionalWrapper>
  );
};

ListActionIconButton.propTypes = {
  ariaLabel: PropTypes.string,
  onClick: PropTypes.func,
  icon: PropTypes.node,
  disabled: PropTypes.bool,
  tooltip: PropTypes.string,
  highlightId: PropTypes.string,
};
