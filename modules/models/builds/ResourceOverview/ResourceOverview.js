import escoListNLS from 'commons/nls/escoList.nls';
import esmoBuildsNLS from 'models/nls/esmoBuilds.nls';
import escoOverviewNLS from 'commons/nls/escoOverview.nls';
import EntityOverview from 'commons/components/EntityOverview';
import {
  withOverviewModelProvider,
  overviewPropsPropType,
} from 'commons/components/overview';
import { sidebarActions } from './actions';

const NLS_BUNDLES = [esmoBuildsNLS, escoListNLS, escoOverviewNLS];

const ResourceOverview = ({ overviewProps }) => {
  return (
    <EntityOverview
      actions={sidebarActions}
      nlsBundles={NLS_BUNDLES}
      overviewProps={overviewProps}
    />
  );
};

ResourceOverview.propTypes = {
  overviewProps: overviewPropsPropType,
};

export default withOverviewModelProvider(ResourceOverview);
