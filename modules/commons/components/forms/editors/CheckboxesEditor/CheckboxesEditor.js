import PropTypes from 'prop-types';
import { graphPropType } from 'commons/util/entry';
import { useTranslation } from 'commons/hooks/useTranslation';
import escoFormsNls from 'commons/nls/escoForms.nls';
import {
  Checkbox,
  FormGroup,
  FormControlLabel,
  FormHelperText,
} from '@mui/material';
import { localizeLabel } from 'commons/locale';
import { useFieldsEdit } from '../hooks/useFieldsEdit';
import { Fields } from '../Fields';

export const CheckboxesEditor = ({
  property,
  nodetype,
  label,
  choices,
  row = false,
  disabled,
  children,
  graph,
  resourceURI,
  dispatch,
  ...fieldsProps
}) => {
  const { fields, addField, removeField, updateValue, error } = useFieldsEdit({
    name: fieldsProps.name,
    property,
    nodetype,
    graph,
    resourceURI,
    mandatory: fieldsProps.mandatory,
    dispatch,
    insertEmptyField: false,
  });

  const translate = useTranslation(escoFormsNls);

  const handleChange = (checked, choiceValue) => {
    if (checked) {
      const field = addField();
      updateValue(field, choiceValue);
      return;
    }
    const fieldToRemove = fields.find(
      (field) => field.getValue() === choiceValue
    );
    removeField(fieldToRemove);
  };

  return (
    // eslint-disable-next-line react/jsx-props-no-spreading
    <Fields label={label} {...fieldsProps}>
      {children}
      <FormGroup row={row}>
        {choices.map(({ value, label: choiceLabel }) => {
          return (
            <FormControlLabel
              key={value}
              control={
                <Checkbox
                  color="primary"
                  checked={Boolean(
                    fields.find((field) => field.getValue() === value)
                  )}
                  onChange={({ target }) => handleChange(target.checked, value)}
                />
              }
              label={localizeLabel(choiceLabel)}
              name={value}
            />
          );
        })}
      </FormGroup>
      {error ? <FormHelperText error>{translate(error)}</FormHelperText> : null}
    </Fields>
  );
};

CheckboxesEditor.propTypes = {
  label: PropTypes.string,
  nodetype: PropTypes.string.isRequired,
  property: PropTypes.string.isRequired,
  xs: PropTypes.number,
  graph: graphPropType,
  resourceURI: PropTypes.string,
  dispatch: PropTypes.func,
  choices: PropTypes.arrayOf(
    PropTypes.shape({
      value: PropTypes.string,
      label: PropTypes.oneOfType([PropTypes.string, PropTypes.shape({})]),
    })
  ),
  row: PropTypes.bool,
  disabled: PropTypes.bool,
  mandatory: PropTypes.bool,
  children: PropTypes.node,
};
