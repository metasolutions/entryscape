import CreateContactsDialog from 'catalog/contacts/dialogs/CreateContactsDialog';
import RemoveContactDialog from 'catalog/contacts/dialogs/RemoveContactDialog';
import escaContactsNLS from 'catalog/nls/escaContacts.nls';
import escoListNLS from 'commons/nls/escoList.nls';
import {
  LIST_ACTION_EDIT,
  LIST_ACTION_REVISIONS,
  LIST_ACTION_REMOVE,
} from 'commons/components/EntryListView/actions';
import { withListRefresh } from 'commons/components/ListView';

const nlsBundles = [escoListNLS, escaContactsNLS];

const listActions = [
  {
    id: 'create',
    Dialog: withListRefresh(CreateContactsDialog, 'onCreate'),
    labelNlsKey: 'createButtonLabel',
    tooltipNlsKey: 'actionTooltipContact',
  },
];

const rowActions = [
  LIST_ACTION_EDIT,
  LIST_ACTION_REVISIONS,
  { ...LIST_ACTION_REMOVE, Dialog: RemoveContactDialog },
];

export { listActions, rowActions, nlsBundles };
