import { OpenInNew as ExternalLinkIcon } from '@mui/icons-material';
import {
  canReplaceEntityFile,
  canReplaceEntityLink,
  canDownloadEntity,
  shouldUseUriPattern,
  constructURIFromPattern,
} from 'commons/util/store';
import ReplaceLinkDialog from 'commons/components/common/dialogs/ReplaceLinkDialog';
import DownloadResourceDialog from 'commons/components/common/dialogs/DownloadResourceDialog';
import EditEntryDialog from 'commons/components/entry/EditEntryDialog';
import { ACTION_EDIT, ACTION_DOWNLOAD, ACTION_REMOVE } from 'commons/actions';
import { ACTION_REVISIONS } from 'commons/components/overview/actions';
import { localize } from 'commons/locale';
import { getFilterPredicatesFromRelations } from 'commons/types/utils/relations';
import RemoveEntityDialog from './dialogs/RemoveEntityDialog';
import { OverviewReplaceEntityFileDialog } from './dialogs/ReplaceEntityFileDialog';
import ReplacePatternedLinkDialog from './dialogs/ReplacePatternedLinkDialog';
import { withOverviewRefresh } from '../overview';

export const SIDEBAR_ACTION_EDIT = {
  ...ACTION_EDIT,
  labelNlsKey: 'editButtonLabel',
  Dialog: withOverviewRefresh(EditEntryDialog, 'onEntryEdit'),
  getProps: () => ({
    action: {
      getFilterPredicates: getFilterPredicatesFromRelations,
    },
  }),
};

export const SIDEBAR_ACTION_REPLACE_FILE = {
  id: 'replace-file',
  isVisible: ({ entry, entityType }) =>
    entityType?.get('includeInternal') ? false : canReplaceEntityFile(entry),
  labelNlsKey: 'replaceFileButtonLabel',
  Dialog: OverviewReplaceEntityFileDialog,
  getProps: ({ translate, addSnackbar }) => ({
    action: {
      onSuccess: () =>
        addSnackbar({ message: translate('fileChangeSnackbar') }),
    },
  }),
};

export const SIDEBAR_ACTION_REPLACE_LINK = {
  id: 'replace-link',
  isVisible: ({ entry, entityType }) =>
    canReplaceEntityLink(entry, entityType?.get()),
  labelNlsKey: 'replaceLinkMenu',
  Dialog: ReplaceLinkDialog,
  getProps: ({ translate, addSnackbar }) => ({
    action: {
      onLinkChangeSuccess: () =>
        addSnackbar({ message: translate('linkChangeSnackbar') }),
    },
  }),
};

export const SIDEBAR_ACTION_REPLACE_PATTERNED_LINK = {
  id: 'replace-patterned-link',
  isVisible: ({ entityType }) => shouldUseUriPattern(entityType?.get()),
  labelNlsKey: 'replaceLinkMenu',
  Dialog: ReplacePatternedLinkDialog,
  getProps: ({ translate, addSnackbar }) => ({
    action: {
      onSuccess: () =>
        addSnackbar({ message: translate('linkChangeSnackbar') }),
    },
  }),
};

export const SIDEBAR_ACTION_DOWNLOAD = {
  ...ACTION_DOWNLOAD,
  isVisible: ({ entry, entityType }) =>
    entityType?.get('includeInternal') ? false : canDownloadEntity(entry),
  labelNlsKey: 'downloadButton',
  Dialog: DownloadResourceDialog,
};

export const SIDEBAR_ACTION_REMOVE = {
  ...ACTION_REMOVE,
  labelNlsKey: 'removeButtonLabel',
  Dialog: RemoveEntityDialog,
};

export const SIDEBAR_ACTION_PORTAL_LINK = {
  id: 'preview-link',
  startIcon: <ExternalLinkIcon />,
  isVisible: ({ entityType, entry }) =>
    entityType?.get('preview.uriTemplate') && entry.isPublic(),
  getProps: ({ entry, entityType }) => {
    const entryId = entry.getId();
    const contextId = entry.getContext().getId();
    const entryName = entry.getEntryInfo().getName();
    const contextName = entry
      .getContext()
      .getEntry(true)
      .getEntryInfo()
      .getName();
    const resourceURI = entry.getResourceURI();
    const uriPattern = entityType.get('preview.uriTemplate');
    const portalUrl = constructURIFromPattern(uriPattern, {
      entryId,
      contextId,
      entryName,
      contextName,
      resourceURI,
    });
    return {
      href: portalUrl,
      target: '_blank',
      rel: 'noreferrer',
      label: localize(entityType?.get('preview.label')),
    };
  },
};

export const sidebarActions = [
  SIDEBAR_ACTION_EDIT,
  ACTION_REVISIONS,
  SIDEBAR_ACTION_REPLACE_FILE,
  SIDEBAR_ACTION_REPLACE_LINK,
  SIDEBAR_ACTION_REPLACE_PATTERNED_LINK,
  SIDEBAR_ACTION_DOWNLOAD,
  SIDEBAR_ACTION_PORTAL_LINK,
  SIDEBAR_ACTION_REMOVE,
];
