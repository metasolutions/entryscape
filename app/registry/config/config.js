/* eslint-disable max-len */
import adminConfig from 'admin/config/config';
import catalogConfig from 'catalog/config/config';
import commonsConfig from 'commons/config';
// import harvestConfig from 'harvest/config/config';
import statusConfig from 'status/config/config';
// import toolkitConfig from 'toolkit/config/config';
import workbenchConfig from 'workbench/config/config';
import ConfigStore from 'commons/util/ConfigStore';
import searchConfig from 'search/config/config';
import { ORGANIZATION_OVERVIEW_NAME } from 'harvest/config/config';

/**
 * Customer config - can override other configs
 */
const instanceConfigs = [
  window.__entryscape_config,
  window.__entryscape_config_dev || {},
];

const applicationConfigs = [
  commonsConfig,
  adminConfig,
  catalogConfig,
  workbenchConfig,
  //  harvestConfig,
  statusConfig,
  //  toolkitConfig,
  searchConfig,
  {
    /**
     * App theme
     *
     * @type {object}
     */
    theme: {
      /**
       * App name
       *
       * @type {string}
       * @memberof theme
       */
      appName: 'Registry',

      /**
       * Toggles the module name's visibility in the header
       *
       * @type {boolean}
       * @memberof theme
       */
      showModuleNameInHeader: true,

      /**
       * The start banner consists of a header, an icon, some clarifying text,
       * and a button to view the Getting Started guide.
       *
       * @typedef {{en: string, sv: string, de: string}} TranslationObj
       * @typedef {{buttonLabel: TranslationObj, header: TranslationObj, path: string}} DetailsObj
       * @typedef {{header: TranslationObj, text: TranslationObj, icon: string, details: DetailsObj}} StartObj
       * @type {StartObj}
       * @memberof theme
       */
      startBanner: {
        header: {
          en: 'EntryScape Registry',
          sv: 'EntryScape Registry',
          de: 'EntryScape Registry',
        },
        text: {
          en: 'EntryScape Registry is a supplement to an open data portal. Here are tools that are helpful for organizations that want to make available its open data.',
          sv: 'EntryScape Registry är ett komplement till en öppen dataportal. Här finns verktyg som är till hjälp för organisationer som vill tillgängliggöra sina öppna data.',
          de: 'EntryScape Registry ist eine Ergänzung zu einem Open Data Portal. Hier finden Sie die Werkzeuge für Behörden, die Daten der öffentlichen Verwaltung zur Weiterverwendung durch Dritte bereitstellen wollen.',
        },
        icon: '',
        details: {
          buttonLabel: { en: 'Get started', sv: 'Kom igång', de: 'Anfangen' },
          header: {
            en: 'Getting started guide',
            sv: 'Kom-igång guide',
            de: 'Erste Schritte',
          },
          path: '/theme/assets/gettingstarted',
        },
      },
      expandReportsFilter: true,
    },
    registry: {
      /**
       * @typedef {{name: string, label: {en: string}}} ProfileObj
       * @type {ProfileObj[]}
       * @memberof registry
       */
      validationProfiles: [
        { name: 'dcat_ap_se', label: { en: 'Swedish DCAT-AP profile' } },
        { name: 'dcat_ap_dk', label: { en: 'Danish DCAT-AP profile' } },
      ],

      /**
       * @type {string[]}
       * @memberof registry
       */
      validationTypes: [
        'dcat:Catalog',
        'dcat:Dataset',
        'dcat:Distribution',
        'vcard:Kind',
        'vcard:Individual',
        'vcard:Organization',
        'foaf:Agent',
      ],

      /**
       * @type {string[]}
       * @memberof registry
       */
      mandatoryValidationTypes: ['dcat:Catalog', 'dcat:Dataset'],

      /**
       * @type {string[]}
       * @memberof registry
       */
      recipes: ['DCAT', 'INSPIRE', 'CKAN'],

      /**
       * Built-in transforms for the pipeline (organization) form.
       */
      transforms: [
        {
          name: 'fetch',
          args: { source: '' },
        },
        {
          name: 'validate',
          args: { profile: '' },
        },
        {
          name: 'merge',
          args: {
            name: '',
            masterType: '',
            slaveTypes: '',
            profile: '',
          },
        },
      ],

      /**
       * @type {boolean}
       * @memberof registry
       */
      includeLinkCheckReport: false,
      /**
       * Determines whether or which filters are visible in registry. Using a boolean
       * shows/hides all filters, while an array containing the names of the filters
       * to exclude can also be provided.
       *
       * @type {boolean|string[]}
       */
      excludeFilters: false,
      /**
       * Path to an rdf file containing the example for Toolkit's catalog. Must not start with a slash.
       * If empty defaults to the standard `toolkitExample.rdf` from static.
       */
      toolkitExamplePath: '',
    },
    catalog: {
      /**
       * @type {number}
       * @memberof catalog
       */
      catalogLimit: 1,

      /**
       * @type {number}
       * @memberof catalog
       */
      datasetLimit: 3,

      /**
       * @type {boolean}
       * @memberof catalog
       */
      fileuploadDistribution: false,

      /**
       * @type {boolean}
       * @memberof catalog
       */
      catalogCollaboration: false,

      /**
       * @type {boolean}
       * @memberof catalog
       */
      excludeEmptyCatalogsInSearch: true,
    },
    entitytypes: [
      {
        name: 'distribution',
        module: null, // don't restrict to 'catalog' module in registry
        overview: null, // no overviews in registry (apart from orgs/pipelines)
      },
      {
        name: 'dataset',
        module: null,
        overview: null,
      },
      {
        name: 'datasetSeries',
        module: null,
        overview: null,
      },
      {
        name: 'catalog',
        module: null,
        overview: null,
      },
      {
        name: 'contactPoint',
        module: null,
        overview: null,
      },
      {
        name: 'publisher',
        module: null,
        overview: null,
      },
      {
        name: 'datasetDocument',
        module: null,
        overview: null,
      },
      {
        name: 'dataService',
        module: null,
        overview: null,
      },
      {
        name: 'pipeline',
        label: { en: 'Pipeline' },
        rdfType: 'http://entrystore.org/terms/Pipeline',
        matchSource: 'entry', // default is to try matching on the metadata instead
        module: ['register'],
        overview: ORGANIZATION_OVERVIEW_NAME,
      },
    ],
    contentviewers: [
      {
        /**
         * @type {string}
         * @memberof contentviewers
         */
        name: 'answerview',

        /**
         * @type {string}
         * @memberof contentviewers
         */
        class: 'LinkedEntriesView', // TODO

        /**
         * @typedef {{en: string, sv: string}} LabelObj
         * @type {LabelObj}
         * @memberof contentviewers
         */
        label: { en: 'Answer', sv: 'Answer' },
      },
    ],
  },
];

export const getInstanceConfigs = () => {
  return instanceConfigs;
};

export const getApplicationConfigs = () => {
  return applicationConfigs;
};

const config = new ConfigStore();

export default config;
