import { namespaces as ns } from '@entryscape/rdfjson';
import { clone } from 'lodash-es';

/**
 * Expands all keys and values in constraints object.
 *
 * @param {Object} constraints
 * @returns {Object}
 */
export const normalizeConstraints = (constraints = {}) => {
  const constraintsClone = { ...constraints };
  const expandedConstraints = Object.entries(constraintsClone).map(
    ([key, value]) => {
      const uris = Array.isArray(value) ? value : [value];
      return [
        ns.expand(key),
        uris.map((uri) => (uri === null ? null : ns.expand(uri))),
      ];
    }
  );
  return Object.fromEntries(expandedConstraints);
};

/**
 * Expands all keys and values in constraints object. If rdfType is provided to
 * source, it's added to the contstraint. Compare normalizeConstraints.
 *
 * @param {Object} source
 * @returns {Object}
 */
export const normalizeSourceConstraints = (source) => {
  const constraintsClone = clone(source.constraints) || {};

  // if rdfType provided, add it to constraints object
  if (source.rdfType) {
    constraintsClone['rdf:type'] = source.rdfType;
  }

  return normalizeConstraints(constraintsClone);
};

/**
 * Matches two arrays. Degree of match reflects the following order and
 * corresponding number:
 * 1 All elements in both arrays match (order neglected) and both have same
 * length.
 * 2 All elements in array1 are included in array2 or array 2 includes null,
 * which matches all.
 * 3 First element in array1 matches first element in array2.
 * 4 Some element in array1 matches some element in array2.
 * @param {string[]} array1
 * @param {string[]} array2
 * @returns {number|undefined}
 */
export const matchArrays = (array1 = [], array2 = []) => {
  if (array2.includes(null)) return 2;

  const includesAll = array1.every((element) => array2.includes(element));
  if (includesAll && array1.length === array2.length) return 1;
  if (includesAll) return 2;
  if (array1[0] === array2[0]) return 3;
  if (array1.some((element) => array2.includes(element))) return 4;
};

/**
 * Matches two constraint objects and returns degree of match.
 * IMPORTANT: the order of constraints is important!
 * @param {Object} constraints1
 * @param {Object} constraints2 - entitytype constraints
 * @returns {number|undefined}
 */
export const matchConstraints = (constraints1 = {}, constraints2 = {}) => {
  let totalMatchWeight = 0;
  for (const key of Object.keys(constraints1)) {
    const matchWeight = matchArrays(constraints1[key], constraints2[key]);
    if (!matchWeight) return;
    totalMatchWeight += matchWeight;
  }
  return totalMatchWeight;
};

/**
 *  Find besting matching entity type by constraint. See matchArrays how the
 *  matching weight is calculated.
 *
 * @param {EntityType[]} entityTypes
 * @param {Object} constraint
 * @returns {EntityType|undefined}
 */
export const findEntityTypeByConstraint = (
  entityTypes,
  constraint,
  moduleName
) => {
  // Get matching weight for each entity type. Exclude no match and wrong module.
  const matches = entityTypes
    .filter(({ source }) =>
      source.module ? source.module.includes(moduleName) : true
    ) // exlude if wrong module
    .map((entityType) => {
      return {
        // param order is important!
        matchWeight: matchConstraints(
          constraint,
          entityType.source.constraints
        ),
        entityType,
      };
    }) // map entity type types with matching weight
    .filter(({ matchWeight }) => matchWeight); // exclude if no match

  if (!matches.length) return;

  // Sort matches with lowest total weight first and return first.
  // Keep order if equal weight.
  const [{ entityType }] = matches.sort((matchA, matchB) => {
    if (matchA.matchWeight < matchB.matchWeight) return -1;
    if (matchA.matchWeight > matchB.matchWeight) return 1;
    return 0;
  });
  return entityType;
};
