import PropTypes from 'prop-types';
import React, { useEffect } from 'react';
import useLinkOrFile from 'commons/components/entry/Import/useLinkOrFile';
import FileUpload from 'commons/components/common/FileUpload';
import RDFormsFieldWrapper from 'commons/components/rdforms/FieldWrapper';
import { useTranslation } from 'commons/hooks/useTranslation';
import escoEntryTypeNLS from 'commons/nls/escoEntryType.nls';

const FileField = ({ setCreateEntryParams, setExtraFieldIsValid }) => {
  const { file, handleFileChange } = useLinkOrFile();
  const t = useTranslation(escoEntryTypeNLS);

  const onFileChange = (inputEl) => {
    handleFileChange(inputEl);
    setExtraFieldIsValid(!!inputEl);
  };

  useEffect(() => {
    if (file) {
      setCreateEntryParams({ file });
    }
  }, [file, setCreateEntryParams]);

  return (
    <RDFormsFieldWrapper label={t('fileLabel')} editor>
      <FileUpload file={file} onSelectFile={onFileChange} />
    </RDFormsFieldWrapper>
  );
};

FileField.propTypes = {
  setCreateEntryParams: PropTypes.func,
  setExtraFieldIsValid: PropTypes.func,
};

export default FileField;
