import config from 'config';
import { getNlsDefinition, getLocalizedString } from './util';

config.get = jest.fn();

describe('test getLocalizedString', () => {
  const definitionAlpha = 'Alpha';
  const definitionAlphaForProfile = 'Alpha for test profile';
  const definitionBeta = 'Beta';
  const nlsBundle = {
    alpha: definitionAlpha,
    alpha__testProfile: definitionAlphaForProfile,
    beta: definitionBeta,
  };

  test('get localized string', () => {
    expect(getLocalizedString('alpha', nlsBundle)).toBe(definitionAlpha);
  });

  test('get localized string with profile', () => {
    config.get.mockReturnValue('testProfile');
    expect(getLocalizedString('alpha', nlsBundle)).toBe(
      definitionAlphaForProfile
    );
  });

  test('get localized string with profile but no corresponding profile key', () => {
    config.get.mockReturnValue('testProfile');
    expect(getLocalizedString('beta', nlsBundle)).toBe(definitionBeta);
  });
});

describe('test getNlsDefinition', () => {
  const definition = 'Alpha';
  const definitionWithClarification = 'Alpha~This is just a test';

  test('gets definition when there is no clarification', () => {
    expect(getNlsDefinition(definition)).toBe(definition);
  });

  test('gets definition without clarification', () => {
    expect(getNlsDefinition(definitionWithClarification)).toBe(definition);
  });
});
