// eslint-disable-next-line max-len
import ListDownloadEntryDialog from 'commons/components/EntryListView/dialogs/ListDownloadEntryDialog';
import esteCollectionExportNLS from 'terms/nls/esteCollectionexport.nls';

const DownloadDialog = (props) => (
  <ListDownloadEntryDialog
    {...props}
    nlsBundles={[esteCollectionExportNLS]}
    profile="skoscollection"
  />
);

export default DownloadDialog;
