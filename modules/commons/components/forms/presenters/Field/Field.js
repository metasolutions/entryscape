import PropTypes from 'prop-types';
import './Field.scss';

export const Field = ({ inline, children }) => {
  return inline ? (
    children
  ) : (
    <div className="escoPresenterField">{children}</div>
  );
};

Field.propTypes = {
  inline: PropTypes.bool,
  children: PropTypes.node,
};
