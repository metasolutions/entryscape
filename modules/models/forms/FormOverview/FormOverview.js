import React, { useMemo } from 'react';
import { CircularProgress } from '@mui/material';
import { useEntry } from 'commons/hooks/useEntry';
import { useTranslation } from 'commons/hooks/useTranslation';
import esmoFormsNLS from 'models/nls/esmoForms.nls';
import useRemoveFormDialog from 'models/forms/hooks/useRemoveFormDialog';
import { FieldPresenter } from 'models/presenters';
import {
  OverviewSection,
  OverviewDescription,
  withOverviewModelProvider,
  overviewPropsPropType,
} from 'commons/components/overview';
import { ACTION_INFO_WITH_ICON } from 'commons/components/overview/actions';
import { getDescription } from 'commons/util/rdfUtils';
import esmoCommonsNls from 'models/nls/esmoCommons.nls';
import escoListNls from 'commons/nls/escoList.nls';
import escoDialogsNLS from 'commons/nls/escoDialogs.nls';
import ReferenceList from 'models/lists/ReferenceList';
import { FIELDS_FORMS_INFO } from 'models/utils/fieldDefinitions';
import FieldsLinkedDataBrowserDialog from 'commons/components/FieldsLinkedDataBrowserDialog';
import { useSnackbar, ERROR } from 'commons/hooks/useSnackbar';
import { RDF_TYPE_INLINE_FIELD } from 'models/utils/ns';
import Overview from 'commons/components/overview/Overview';
import { getFormLabel, getFormType, isObjectForm } from '../utils/forms';
import {
  primaryFields,
  secondaryFields as fields,
} from '../utils/formPresenters';
import './FormOverview.scss';
import { sidebarActions } from './actions';

const NLS_BUNDLES = [escoListNls, esmoFormsNLS, esmoCommonsNls, escoDialogsNLS];

const FormOverview = ({ overviewProps }) => {
  const formEntry = useEntry();
  const translate = useTranslation(NLS_BUNDLES);
  const [addSnackbar] = useSnackbar();
  const { removeStatus } = useRemoveFormDialog(
    formEntry,
    () => addSnackbar({ message: translate('removeSuccessMessage') }),
    () => addSnackbar({ message: translate('removeFailMessage'), type: ERROR })
  );

  const extendedByRdfType = useMemo(() => {
    return getFormType(formEntry.getMetadata(), formEntry.getResourceURI());
  }, [formEntry]);

  if (removeStatus === 'pending')
    return (
      <div className="esmoFieldOverview__loading">
        <CircularProgress />
      </div>
    );

  return (
    <Overview
      {...overviewProps}
      backLabel={translate('backTitle')}
      headerAction={{
        ...ACTION_INFO_WITH_ICON,
        nlsBundles: NLS_BUNDLES,
        entry: formEntry,
        Dialog: FieldsLinkedDataBrowserDialog,
        fields: FIELDS_FORMS_INFO,
      }}
      headerSubLabel={getFormLabel(formEntry, translate)}
      renderPrimaryContent={() => (
        <>
          <OverviewDescription label={getDescription(formEntry)} />
          {primaryFields.map((field) => (
            <FieldPresenter
              key={field.property}
              graph={formEntry.getMetadata()}
              resourceURI={formEntry.getResourceURI()}
              {...field}
            />
          ))}
        </>
      )}
      entry={formEntry}
      nlsBundles={NLS_BUNDLES}
      sidebarActions={sidebarActions}
    >
      <OverviewSection>
        {fields.map((field) => (
          <FieldPresenter
            key={field.property}
            graph={formEntry.getMetadata()}
            resourceURI={formEntry.getResourceURI()}
            {...field}
          />
        ))}
      </OverviewSection>
      {isObjectForm(formEntry.getMetadata(), formEntry.getResourceURI()) ? (
        <OverviewSection>
          <ReferenceList
            entry={formEntry}
            nlsBundles={NLS_BUNDLES}
            rdfType={RDF_TYPE_INLINE_FIELD}
            headingNlsKey="usedByListHeader"
            placeholderProps={{
              labelNlsKey: 'usedByFieldPlaceholder',
            }}
            pathviewName="models__fields__field"
          />
        </OverviewSection>
      ) : null}
      <OverviewSection>
        <ReferenceList
          entry={formEntry}
          nlsBundles={NLS_BUNDLES}
          rdfType={extendedByRdfType}
          headingNlsKey="extendedByHeader"
          placeholderProps={{
            labelNlsKey: 'extendedByFormPlaceholder',
          }}
          pathviewName="models__forms__form"
        />
      </OverviewSection>
    </Overview>
  );
};

FormOverview.propTypes = {
  overviewProps: overviewPropsPropType,
};

export default withOverviewModelProvider(FormOverview);
