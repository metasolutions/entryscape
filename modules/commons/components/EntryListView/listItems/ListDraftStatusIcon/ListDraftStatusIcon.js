import PropTypes from 'prop-types';
import { ReportGmailerrorredOutlined as WarningIcon } from '@mui/icons-material';
import { ListItemIcon } from 'commons/components/ListView';
import { useTranslation } from 'commons/hooks/useTranslation';
import escoListNLS from 'commons/nls/escoList.nls';
import { isDraftEntry, entryPropType } from 'commons/util/entry';
import './ListDraftStatusIcon.scss';

export const ListDraftStatusIcon = ({
  entry,
  xs = 1,
  tooltipNlsKey = 'draftDisabledTitle', // default for public columns (toggle/indicator)
}) => {
  const translate = useTranslation(escoListNLS);
  const icon = isDraftEntry(entry) ? (
    <div className="escoListDraftStatusIcon__container">
      <WarningIcon color="accent" />
    </div>
  ) : null;

  return (
    <ListItemIcon
      xs={xs}
      justifyContent="flex-start"
      tooltip={icon ? translate(tooltipNlsKey) : null}
      icon={icon}
    />
  );
};

ListDraftStatusIcon.propTypes = {
  entry: entryPropType,
  xs: PropTypes.number,
  tooltipNlsKey: PropTypes.string,
};

export default ListDraftStatusIcon;
