import { useState } from 'react';
import PropTypes from 'prop-types';
import { Entry } from '@entryscape/entrystore-js';
import { useTranslation } from 'commons/hooks/useTranslation';
import esteConceptNLS from 'terms/nls/esteConcept.nls';
import { getLabel } from 'commons/util/rdfUtils';
import { useESContext } from 'commons/hooks/useESContext';
import {
  Button,
  Dialog,
  DialogContent,
  DialogActions,
  TextField,
  DialogContentText,
  InputAdornment,
} from '@mui/material';
import ContentWrapper from 'commons/components/common/ContentWrapper';
import AddIcon from '@mui/icons-material/Add';
import { useEntityTitleAndURI } from 'commons/hooks/useEntityTitleAndURI'; // there's the same file in terms, should probably delete
import { constructURIFromPattern } from 'commons/util/store';
import Tooltip from 'commons/components/common/Tooltip';
import useRestrictionDialog from 'commons/hooks/useRestrictionDialog';
import useConceptRestriction from 'terms/hooks/useConceptRestriction';
import ConditionalWrapper from 'commons/components/common/ConditionalWrapper';
import { useSnackbar } from 'commons/hooks/useSnackbar';
import useAsync, { PENDING } from 'commons/hooks/useAsync';
import LoadingButton from 'commons/components/LoadingButton';
import { ErrorCatcher } from 'commons/errors/ErrorCatcher';
import { ErrorWithMessage } from 'commons/errors/utils/errors';
import { useConfirmCloseAction } from 'commons/components/EntryListView/dialogs/create/handlers';

const CreateConceptDialog = ({
  selectedConceptEntry,
  conceptSchemeEntry,
  createConcept,
  uriPattern: entityTypeUriPattern,
  readOnly,
}) => {
  const { context, contextName } = useESContext();
  const uriSpace = conceptSchemeEntry
    ?.getMetadata()
    .findFirstValue(null, 'void:uriSpace');
  const uriSpacePattern = uriSpace ? `${uriSpace}/\${entryName}` : '';
  const uriPattern = uriSpacePattern || entityTypeUriPattern;
  const {
    title,
    name,
    isNameFree,
    handleTitleChange,
    handleNameChange,
    reinitTitleAndURIState,
    setHasNameChangedManually,
  } = useEntityTitleAndURI({ context, contextName, uriPattern });
  const [addSnackbar] = useSnackbar();
  const { runAsync, status, error: createError } = useAsync();

  const [pristine, setPristine] = useState(true);
  const [open, setOpen] = useState(false);

  const closeDialog = () => {
    setOpen(false);
    setHasNameChangedManually(false);
  };
  const confirmClose = useConfirmCloseAction(closeDialog);
  const close = () => confirmClose(!pristine);
  const openDialog = () => {
    reinitTitleAndURIState();
    setPristine(true);
    setOpen(true);
  };

  const parentLabel =
    (selectedConceptEntry || conceptSchemeEntry) &&
    getLabel(selectedConceptEntry || conceptSchemeEntry);

  const translate = useTranslation(esteConceptNLS);
  const {
    isRestricted,
    contentPath,
    status: restrictionStatus,
  } = useConceptRestriction(open, context, conceptSchemeEntry);
  useRestrictionDialog({
    open: isRestricted,
    contentPath,
    callback: closeDialog,
  });

  /**
   * Get the state of the form: valid or not
   * @todo put title in state
   * @return {boolean}
   */
  const formIsValid = () => {
    if (title) {
      return uriPattern
        ? isNameFree && name !== '' && title !== ''
        : title !== '';
    }
    return false;
  };

  const onConceptCreate = () => {
    if (isNameFree) {
      return runAsync(
        createConcept(title, uriPattern ? name : null)
          .then(closeDialog)
          .then(() =>
            addSnackbar({ message: translate('createConceptSuccess') })
          )
          .catch((error) => {
            throw new ErrorWithMessage(translate('createConceptFail'), error);
          })
      );
    }
  };

  return (
    <>
      <ConditionalWrapper
        condition={!readOnly}
        wrapper={(children) => (
          <Tooltip title={translate('createConcept')}>{children}</Tooltip>
        )}
      >
        <Button
          disabled={readOnly}
          startIcon={<AddIcon />}
          onClick={openDialog}
        >
          {translate('createButtonLabel')}
        </Button>
      </ConditionalWrapper>

      {open && restrictionStatus === 'resolved' && !isRestricted ? (
        <Dialog
          open
          onClose={close}
          fullWidth
          maxWidth="md"
          aria-labelledby="create-concept-dialog"
        >
          <DialogContent>
            <DialogContentText id="create-concept-dialog">
              {translate('selectedTermDesc')} <b>{parentLabel}</b>
            </DialogContentText>
            <ContentWrapper>
              <TextField
                autoFocus
                value={title}
                onInput={(evt) => {
                  setPristine(false);
                  handleTitleChange(evt);
                }}
                id="create-concept-title"
                label={translate('conceptNameLabel')}
                placeholder={translate('conceptNamePlaceholder')}
                error={!pristine && !title}
                helperText={!pristine && !title && translate('nameRequired')}
                required
              />
              {uriPattern && (
                <TextField
                  label={translate('URLInputLabel')}
                  id="create-terminology-name"
                  value={name}
                  onChange={(evt) => {
                    setPristine(false);
                    handleNameChange(evt);
                  }}
                  error={!pristine && (!name || !isNameFree)}
                  helperText={
                    !pristine &&
                    (!name
                      ? translate('URLRequired')
                      : !isNameFree && translate('unavailableURLFeedback'))
                  }
                  required
                  InputProps={{
                    startAdornment: (
                      <InputAdornment position="start">
                        {constructURIFromPattern(uriPattern, {
                          contextName,
                        })}
                      </InputAdornment>
                    ),
                  }}
                />
              )}
            </ContentWrapper>
          </DialogContent>
          <DialogActions>
            <Button onClick={close} variant="text">
              {translate('cancel')}
            </Button>
            <LoadingButton
              onClick={onConceptCreate}
              loading={status === PENDING}
              autoFocus
              disabled={!formIsValid()}
            >
              {translate('addConcept')}
            </LoadingButton>
          </DialogActions>
        </Dialog>
      ) : null}
      <ErrorCatcher error={createError} />
    </>
  );
};

CreateConceptDialog.propTypes = {
  selectedConceptEntry: PropTypes.instanceOf(Entry),
  conceptSchemeEntry: PropTypes.instanceOf(Entry),
  createConcept: PropTypes.func,
  uriPattern: PropTypes.string,
  readOnly: PropTypes.bool,
};

export default CreateConceptDialog;
