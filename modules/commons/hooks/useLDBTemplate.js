import { useEffect, useState } from 'react';
import useAsync from 'commons/hooks/useAsync';
import {
  useLinkedBrowserModel,
  NAME,
} from 'commons/components/LinkedDataBrowserDialog/useLinkedBrowserModel';
import { spreadEntry } from 'commons/util/store';
import Lookup from 'commons/types/Lookup';
import { findEntityTypeForContext } from 'commons/types/utils/entityType';

const LOCAL = 'local';
const EXTERNAL = 'external';

const rdfTypesHaveMatch = (givenEntry, currentEntry) => {
  const { metadata: givenMetadata, ruri: givenRuri } = spreadEntry(givenEntry);
  const { metadata: currentMetadata, ruri: currentRuri } =
    spreadEntry(currentEntry);
  const givenRdfTypes = givenMetadata
    .find(givenRuri, 'rdf:type')
    .map((statement) => statement.getValue());
  const currentRdfTypes = currentMetadata
    .find(currentRuri, 'rdf:type')
    .map((statement) => statement.getValue());

  return givenRdfTypes.some((type) => currentRdfTypes.includes(type));
};

const useLDBTemplate = (
  entry,
  parentEntry,
  formTemplateId,
  showEntityError = true
) => {
  const [state, dispatch] = useLinkedBrowserModel(entry);
  const [entityType, setEntityType] = useState();
  const [metadataVisibility, setMetadataVisiblity] = useState(LOCAL);
  const {
    data: template,
    runAsync: runAsyncTemplate,
    error,
    status,
    isLoading,
  } = useAsync(null);

  useEffect(() => {
    const getTemplate = async () => {
      if (formTemplateId) return formTemplateId;
      const parent =
        parentEntry && rdfTypesHaveMatch(entry, state.entry)
          ? parentEntry
          : null;
      const findEntityTypeInUse = () => {
        if (state.entry.isContext()) {
          return findEntityTypeForContext(state.entry);
        }
        return Lookup.inUse(state.entry, parent);
      };
      return findEntityTypeInUse().then((entityTypeInUse) => {
        if (!entityTypeInUse && showEntityError) {
          console.error(
            'Entry has no provided template Id and no entity type was found!'
          );
          console.log({ entry: state.entry, parent });
        }

        if (!entityTypeInUse) return;

        setEntityType(entityTypeInUse);
        dispatch({
          type: NAME,
          value: {
            index: state.index,
            entityTypeName: entityTypeInUse?.get('name'),
          },
        });

        const templateId = state.entry?.isLinkReference()
          ? entityTypeInUse.complementaryTemplateId()
          : entityTypeInUse.templateId();

        const isExternal = state.entry?.isLinkReference();
        const noLocalMetadata =
          state.entry === undefined || state.entry.getMetadata().isEmpty();
        setMetadataVisiblity(isExternal && noLocalMetadata ? EXTERNAL : LOCAL);

        return templateId;
      });
    };
    runAsyncTemplate(getTemplate());
  }, [
    state.entry,
    runAsyncTemplate,
    formTemplateId,
    parentEntry,
    entry,
    state.index,
    dispatch,
    entityType,
    showEntityError,
  ]);

  return {
    template,
    entityType,
    metadataVisibility,
    setMetadataVisiblity,
    error,
    status,
    isLoading,
  };
};

export default useLDBTemplate;
