import PropTypes from 'prop-types';
import { Grid, Typography } from '@mui/material';
import EmptyListIcon from '@mui/icons-material/List';
import './BrowserPlaceholder.scss';

const BrowserPlaceholder = ({ label }) => (
  <Grid
    container
    direction="column"
    alignContent="center"
    className="escoLDBPlaceholder"
    aria-live="polite"
  >
    <Grid item>
      <EmptyListIcon classes={{ root: 'escoLDBPlaceholder__icon' }} />
    </Grid>
    <Grid item>
      <Typography className="escoLDBPlaceholder__text" variant="body1">
        {label}
      </Typography>
    </Grid>
  </Grid>
);

BrowserPlaceholder.propTypes = {
  label: PropTypes.string.isRequired,
};

export default BrowserPlaceholder;
