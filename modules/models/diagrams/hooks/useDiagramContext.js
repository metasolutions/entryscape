import { useEffect, createContext, useContext, useReducer } from 'react';
import PropTypes from 'prop-types';
import useAsync from 'commons/hooks/useAsync';
import { entrystore } from 'commons/store';
import { useESContext } from 'commons/hooks/useESContext';
import { createDiagramPrototypeEntry } from '../utils/diagram';
import { LABEL_TYPE_TITLE, DEFAULT_DIAGRAM_ID } from '../utils/definitions';
import { getLabelTypeFromMetadata } from '../utils/layoutInRDF';

// action types
export const SET_DIAGRAM_ENTRY = 'diagramEntry';
export const SET_DIAGRAM_ENTRY_ID = 'diagramEntryId';
export const SET_DIAGRAM_ADAPTER = 'diagramAdapter';
export const SET_LABEL_TYPE = 'labelType';

const DiagramContext = createContext();

const diagramReducer = (state, action) => {
  switch (action.type) {
    case SET_DIAGRAM_ENTRY: {
      const { diagramEntry, isPrototype } = action;
      return {
        ...state,
        diagramEntry,
        isPrototype,
        labelType: getLabelTypeFromMetadata(diagramEntry),
      };
    }
    case SET_DIAGRAM_ENTRY_ID: {
      const { diagramEntryId } = action;
      return {
        ...state,
        diagramEntryId,
      };
    }
    case SET_DIAGRAM_ADAPTER: {
      const { diagramAdapter } = action;
      return {
        ...state,
        diagramAdapter,
      };
    }
    case SET_LABEL_TYPE: {
      const { labelType } = action;
      return {
        ...state,
        labelType,
      };
    }
    default: {
      console.log(`No matching action type ${action.type} for diagramReducer`);
      return state;
    }
  }
};

const DiagramEntryProvider = ({ children }) => {
  const { context } = useESContext();
  const [value, dispatch] = useReducer(diagramReducer, {
    labelType: LABEL_TYPE_TITLE,
    isPrototype: false,
    diagramEntry: null,
    diagramEntryId: DEFAULT_DIAGRAM_ID,
    diagramAdapter: null,
  });
  const { status, runAsync } = useAsync();
  const { diagramEntryId, diagramEntry } = value;

  useEffect(() => {
    const getDiagramEntry = async () => {
      try {
        const entryURI = context.getEntryURIbyId(diagramEntryId);
        const entry = await entrystore.getEntry(entryURI);
        dispatch({
          type: SET_DIAGRAM_ENTRY,
          diagramEntry: entry,
          isPrototype: false,
        });
        return entry;
      } catch (_error) {
        dispatch({
          type: SET_DIAGRAM_ENTRY,
          diagramEntry: createDiagramPrototypeEntry(context, diagramEntryId),
          isPrototype: true,
        });
      }
    };
    runAsync(getDiagramEntry());
  }, [context, diagramEntryId, runAsync]);

  return (
    diagramEntry && (
      <DiagramContext.Provider
        value={{
          ...value,
          status,
          dispatch,
        }}
      >
        {children}
      </DiagramContext.Provider>
    )
  );
};

DiagramEntryProvider.propTypes = {
  children: PropTypes.node,
};

const useDiagramContext = () => {
  const context = useContext(DiagramContext);
  if (context === undefined) {
    throw new Error(
      'useDiagramContext must be used within a DiagramContext Provider'
    );
  }
  return context;
};

export { DiagramEntryProvider, useDiagramContext };
