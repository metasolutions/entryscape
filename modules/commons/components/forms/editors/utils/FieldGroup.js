import { Field } from './Field';
import { REQUIRED_FIELD } from './validate';

/**
 *
 * @param {object} fieldProperties
 * @returns {object[]}
 */
const getInitialFields = (fieldProperties) => {
  const { property, resourceURI, graph, insertEmptyField } = fieldProperties;

  if (!property) {
    throw new Error('Property is mandatory');
  }
  const statements = graph.find(resourceURI, property);
  if (!statements.length) {
    return insertEmptyField ? [new Field(fieldProperties)] : [];
  }

  return statements.map(
    (statement) => new Field({ statement, ...fieldProperties })
  );
};

// Class for grouping edit fields using same type of rdf property. This makes it
// easier to for example validate the containing fields together.
export class FieldGroup {
  constructor({ validate: validateWrapper, ...fieldProperties }) {
    const { name } = fieldProperties;
    const id = name;
    this.id = name;
    this.labelId = `fields-label-${id}`;
    this.fields = getInitialFields(fieldProperties);
    this.mandatory = fieldProperties.mandatory;
    this.resetOnFormType = fieldProperties.resetOnFormType;
    this.nodetype = fieldProperties.nodetype;
    this.min = fieldProperties.min ?? 1;
    this.fieldProperties = fieldProperties;
    if (validateWrapper) {
      this.validate = validateWrapper(this.validate.bind(this), this);
    }
  }

  _setFields(fields) {
    this.fields = fields;
  }

  setError(error) {
    this.error = error;
    for (const field of this.fields) {
      field.setError(this.error);
    }
  }

  validate(checkMandatory) {
    if (checkMandatory && this.mandatory && !this.hasValue()) {
      this.setError(REQUIRED_FIELD);
    } else {
      this.setError();
    }

    if (this.error) return this.error;

    const fieldError = this.fields
      .map((field) => field.validate())
      .find((error) => error);
    return fieldError;
  }

  addField() {
    const newField = new Field(this.fieldProperties);
    this.fields = [...this.fields, newField];
    return newField;
  }

  removeField(field) {
    field.remove();
    this.fields = this.fields.filter(
      (thisField) => thisField.getId() !== field.getId()
    );
    this.validate();
    return field;
  }

  getFields() {
    return this.fields;
  }

  getResetOnFormType() {
    return this.resetOnFormType;
  }

  hasError() {
    if (this.error) return true;
    for (const field of this.fields) {
      if (field.hasError()) return true;
    }
    return false;
  }

  hasValue() {
    return Boolean(this.getFields().find((field) => field.getValue()));
  }

  clear() {
    const clearedFields = [];
    this.getFields().forEach((field, index) => {
      if (index === 0 && this.min > 0 && field.clear) {
        field.clear();
        clearedFields.push(field);
        return;
      }
      field.remove();
    });
    this.fields = clearedFields;
  }

  remove() {
    this.getFields().forEach((field) => {
      field.remove();
    });
    this.fields = [];
  }

  getResourceURI() {
    return this.fieldProperties.resourceURI;
  }

  getProperty() {
    return this.fieldProperties.property;
  }
}
