import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from '@mui/material';
import { useMainDialog, MainDialogProvider } from 'commons/hooks/useMainDialog';
import PropTypes from 'prop-types';

/**
 * Utility component to be used as the `actions` in a dialog.
 */
export const AcknowledgeAction = ({
  onDone,
  okLabel = 'OK',
  isDisabled = false,
}) => {
  const { closeMainDialog } = useMainDialog();
  const handleClose = () => {
    if (onDone) {
      Promise.resolve(onDone()).then(closeMainDialog());
    } else {
      closeMainDialog();
    }
  };

  // TODO nls
  return (
    <Button
      onClick={handleClose}
      color="primary"
      autoFocus
      disabled={isDisabled}
    >
      {okLabel}
    </Button>
  );
};

AcknowledgeAction.propTypes = {
  onDone: PropTypes.func,
  isDisabled: PropTypes.bool,
  okLabel: PropTypes.string,
};
/**
 *
 * Utility component to be used as the `actions` in a dialog.
 *
 */
export const ConfirmAction = ({
  onDone,
  onReject,
  affirmLabel,
  rejectLabel,
}) => {
  const { closeMainDialog } = useMainDialog();
  const handleClose = () => {
    if (onDone) {
      Promise.resolve(onDone()).then(closeMainDialog());
    } else {
      closeMainDialog();
    }
  };

  const handleReject = () => {
    if (onReject) {
      onReject();
    }
    closeMainDialog();
  };

  return (
    <>
      <Button onClick={handleReject} variant="text">
        {rejectLabel}
      </Button>
      <Button onClick={handleClose} color="primary" autoFocus>
        {affirmLabel}
      </Button>
    </>
  );
};

ConfirmAction.propTypes = {
  onDone: PropTypes.func,
  onReject: PropTypes.func,
  affirmLabel: PropTypes.string.isRequired,
  rejectLabel: PropTypes.string.isRequired,
};

/**
 * Main ES dialog. Should be used for generic messages like confirmation or error messages.
 *
 *  If null is passed to actions, no actions will be rendered
 */
const MainDialog = () => {
  const { open, dialogContent, closeMainDialog, dialogProps } = useMainDialog();
  const {
    title,
    content,
    actions = <ConfirmAction />,
    onBackdropClick,
  } = dialogContent;

  const { ignoreBackdropClick: _ignoreBackdropClick, ...muiDialogProps } =
    dialogProps; // ignoreBackdropClick is not a mui dialog prop

  return (
    <Dialog
      open={open}
      onClose={closeMainDialog}
      aria-labelledby="es-main-dialog-title"
      aria-describedby="es-main-dialog-description"
      fullWidth
      maxWidth="sm"
      onBackdropClick={onBackdropClick}
      {...muiDialogProps}
    >
      {title && <DialogTitle id="es-main-dialog-title">{title}</DialogTitle>}
      <DialogContent id="es-main-dialog-description">
        {typeof content === 'string' ? (
          <DialogContentText>{content}</DialogContentText>
        ) : (
          content
        )}
      </DialogContent>
      {actions && <DialogActions>{actions}</DialogActions>}
    </Dialog>
  );
};

export const MainDialogWithProvider = ({ children }) => {
  return (
    <MainDialogProvider>
      <MainDialog />
      {children}
    </MainDialogProvider>
  );
};

MainDialogWithProvider.propTypes = {
  children: PropTypes.node,
};

export default MainDialog;
