import PropTypes from 'prop-types';
import { Entry } from '@entryscape/entrystore-js';
import useAsyncCallback from 'commons/hooks/useAsyncCallback';
import { getSourceDistribution } from 'catalog/datasets/DatasetOverview/utils/distributions';
import RefreshAPIDialog from './RefreshAPIDialog';

const RefreshAPIDialogWrapper = ({ entry: apiDistribution, ...rest }) => {
  const [sourceDistribution] = useAsyncCallback(
    getSourceDistribution,
    apiDistribution
  );

  if (!sourceDistribution) {
    return null;
  }

  return (
    <RefreshAPIDialog
      apiDistribution={apiDistribution}
      sourceDistribution={sourceDistribution}
      {...rest}
    />
  );
};

RefreshAPIDialogWrapper.propTypes = {
  entry: PropTypes.instanceOf(Entry),
};

export default RefreshAPIDialogWrapper;
