import Map from 'ol/Map';
import View from 'ol/View';
import {
  Attribution,
  defaults as defaultControls,
  ScaleLine,
} from 'ol/control';
import { createDefaultBasemap, createTileWMS } from './layer';
import { MAX_ZOOM, DEFAULT_PROJECTION_CODE } from './mapDefaults';

/**
 * Create a new map
 *
 * @param {object} mapOptions
 * @param {string} mapOptions.target
 * @param {number} mapOptions.maxZoom
 * @param {string} mapOptions.projectionCode
 * @param {number[]} mapOptions.center
 * @param {object[]} mapOptions.layers
 * @param {object} mapOptions.basemapOptions
 * @param {object[]} mapOptions.controls
 * @returns {Map}
 */
export const createMap = ({
  target,
  maxZoom = MAX_ZOOM,
  projectionCode = DEFAULT_PROJECTION_CODE,
  center = [0, 0],
  layers = [],
  basemapOptions,
  controls = [],
}) => {
  const openLayerslayers = [
    createDefaultBasemap(basemapOptions),
    ...layers.map((layerConfiguration) => createTileWMS(layerConfiguration)),
  ];
  const map = new Map({
    layers: openLayerslayers,
    target,
    controls: defaultControls({ attribution: false }).extend(controls),
    view: new View({
      maxZoom,
      projection: projectionCode,
      center,
      zoom: 1,
    }),
  });

  return map;
};

/**
 * Zooms to the extent of a vector layer. Padding is used to prevent features
 * from being rendered outside the view.
 *
 * @param {Map} map
 * @param {object} layer
 * @param {number} maxZoom
 * @returns {Map}
 */
export const zoomToLayer = (map, layer, maxZoom) => {
  const layerExtent = layer.getSource().getExtent();
  if (layerExtent.filter(Number.isFinite).length !== 4) return map; // handle infinity
  map.getView().fit(layerExtent, { padding: [50, 50, 50, 50], maxZoom });
  return map;
};

/**
 * @param {Map} map
 * @param {number[]} extent
 * @returns {Map}
 */
export const zoomToExtent = (map, extent) => {
  map.getView().fit(extent);
  return map;
};

/**
 * Removes all layers that are not marked as basemap layers
 *
 * @param {Map} map
 * @returns {Map}
 */
export const clearOverlays = (map) => {
  for (const layer of map.getLayers().getArray()) {
    if (layer.get('type') !== 'basemap') {
      map.removeLayer(layer);
    }
  }
  return map;
};

/**
 * Convenience function to add multiple layers
 *
 * @param {Map} map
 * @param {object[]} layers
 * @returns {Map}
 */
export const addLayers = (map, layers) => {
  for (const layer of layers) {
    map.addLayer(layer);
  }
  return map;
};

/**
 * Creates a scale line
 *
 * @param {object} target
 * @param {object} scaleOptions
 * @param {string} scaleOptions.className
 * @param {string} scaleOptions.name
 * @returns {object}
 */
export const createScaleLine = (
  target,
  { className = 'escoMapScaleLineControl', name = 'scale-line', ...rest } = {}
) => new ScaleLine({ target, className, name, ...rest });

/**
 * Creates an attribution component
 *
 * @param {object} target
 * @param {object} attributionOptions
 * @param {string} attributionOptions.className
 * @param {string} attributionOptions.name
 * @returns {object}
 */
export const createAttribution = (
  target,
  {
    className = 'escoAttributionControl ol-attribution',
    name = 'attribution-control',
    ...rest
  } = {}
) => new Attribution({ target, className, name, ...rest });
