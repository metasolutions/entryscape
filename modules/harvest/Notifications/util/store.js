import { terms } from '@entryscape/entrystore-js';

/**
 * A helper procedure that finds an existing notification settings entry or
 * creates a new one
 * @return {Promise<Entry>}
 */
const setNotificationsSettings = async (pipelineEntry, args) => {
  const pipelineResource = await pipelineEntry.getResource();
  pipelineResource.setPipelineArguments(args, terms.argumentTypes.configuration);
  return pipelineResource.commit();
};

/**
 *
 * @param {store/Entry} pipelineEntry
 * @return {Promise<{}|Object>}
 */
const getNotificationsSettings = async (pipelineEntry) => {
  const pipelineResource = await pipelineEntry.getResource();
  return pipelineResource.getPipelineArguments(terms.argumentTypes.configuration);
};

/**
 * Get the email of the pipeline owner or the current user's one
 * @param {store/Entry} pipelineEntry
 * @return {string}
 */
const getNotificationSettingsDefaultEmail = (pipelineEntry, user) => {
  const md = pipelineEntry.getMetadata();
  const email = md.findFirstValue(pipelineEntry.getResourceURI(), 'foaf:mbox');

  return email ? email.replace('mailto:', '') : user;
};

export {
  setNotificationsSettings,
  getNotificationsSettings,
  getNotificationSettingsDefaultEmail,
};
