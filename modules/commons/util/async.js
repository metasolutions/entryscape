/**
 *  Takes an array of callbacks and run them in sequence.
 *  Returns an array of the data from callbacks, like Promise.all
 *
 * @param {[]} asyncCallbacks
 * @returns {Promise}
 */

export const runAsyncInSequence = (asyncCallbacks) => {
  const asyncReducer = async (previousAsyncCallback, asyncCallback) => {
    return previousAsyncCallback.then((dataArray) =>
      asyncCallback().then((data) => [...dataArray, data])
    );
  };
  return asyncCallbacks.reduce(asyncReducer, Promise.resolve([]));
};

/**
 * Wraps try catch to an async function.
 *
 * @param {Function} asyncFunction
 * @param {string} errorMessage
 * @returns {Function}
 */
export const tryCatchAsync = (asyncFunction, errorMessage) => {
  return async (...args) => {
    try {
      const response = await asyncFunction(...args);
      return response;
    } catch (error) {
      console.error(error, errorMessage);
    }
  };
};

/**
 *
 * @param {Response} response
 * @param {string} url
 * @param {string} name
 * @returns {Promise<object>}
 */
const validateAndParseJsonResponse = async (response, url, name = 'json') => {
  if (!(response && response.ok)) {
    console.error(`Failed fetching ${name} from ${url})`);
    return;
  }
  const contentType =
    response.headers.has('content-type') &&
    response.headers.get('content-type');
  if (!(contentType && contentType.includes('application/json'))) {
    console.error(
      `Failed fetching ${name}. Expected a JSON file and got ${contentType} for ${url})`
    );
    return;
  }
  const jsonResult = await tryCatchAsync(
    () => response.json(),
    `Failed to parse json response for ${url}`
  )();
  return jsonResult;
};

/**
 *
 * @param {string} url
 * @param {string} name
 * @returns {Promise<object>}
 */
export const fetchJson = async (url, name = 'json') => {
  const response = await tryCatchAsync(
    fetch,
    `A network error ocurred while trying to fetch ${name} from ${url}`
  )(url);
  return validateAndParseJsonResponse(response, url, name);
};
