import PropTypes from 'prop-types';
import {
  createEntry,
  constructURIFromPattern,
  createEntityTypeEntryFromFile,
  createEntityTypeEntryFromLink,
  checkURIUniquenessScope,
  shouldUseUriPattern,
} from 'commons/util/store';
import Lookup from 'commons/types/Lookup';
import { LEVEL_RECOMMENDED } from 'commons/components/rdforms/LevelSelector';
import { useListModel, CREATE } from 'commons/components/ListView';
// eslint-disable-next-line max-len
import CreateEntryByLinkOrFile from 'commons/components/EntryListView/dialogs/create/CreateEntryByLinkOrFile';
import {
  useEntityTypeContext,
  ONLY_LINK,
  ONLY_FILE,
  INTERNAL_LINK,
  PATTERNED_LINK,
  LINK_AND_FILE,
} from 'commons/components/EntryListView/dialogs/create/handlers';
import {
  useUniqueURIScopeContext,
  withURIScopeContextProvider,
} from 'commons/hooks/useUniqueURIScope';
import { useEffect, useState } from 'react';
import { useSnackbar } from 'commons/hooks/useSnackbar';
import { useTranslation } from 'commons/hooks/useTranslation';
import escaDocumentsNLS from 'catalog/nls/escaDocuments.nls';
import escoListNLS from 'commons/nls/escoList.nls';

const nlsBundles = [escaDocumentsNLS, escoListNLS];

const CreateDocumentDialog = ({ actionParams, closeDialog }) => {
  const translate = useTranslation(escaDocumentsNLS);
  const { entityType } = actionParams;
  const { context: entityTypeContext, contextName } =
    useEntityTypeContext(entityType);
  const { setIsURIUnique } = useUniqueURIScopeContext();
  const [hasUniqueURIScope, setHasUniqueURIScope] = useState(false);
  const [, dispatch] = useListModel();
  const mainEntityType = Lookup.getMainEntityType('catalog');
  const [selectorType, setSelectorType] = useState(LINK_AND_FILE);
  const hasUriPattern =
    mainEntityType && entityType ? shouldUseUriPattern(entityType) : false;
  const uriPattern = (hasUriPattern && entityType.uriPattern) || '';
  const entityTypeUriPatternBase = constructURIFromPattern(uriPattern, {
    contextName,
  });
  const [addSnackbar] = useSnackbar();

  const [prototypeEntry] = useState(
    createEntry(entityTypeContext, entityType.rdfType)
  );

  const rdfType = Array.isArray(entityType.rdfType)
    ? entityType.rdfType[0]
    : entityType.rdfType;
  prototypeEntry.add('rdf:type', rdfType);

  useEffect(() => {
    if (!entityType.uniqueURIScope) return;
    setHasUniqueURIScope(Boolean(entityType.uniqueURIScope));
  }, [entityType]);

  useEffect(() => {
    const {
      includeFile = false,
      includeLink = false,
      includeInternal = false,
    } = entityType;
    let selector = LINK_AND_FILE;
    if (!includeFile && !includeLink && !includeInternal) {
      throw Error(
        'Cannot create entity that is nothing (not file, not link, not internal URI), ' +
          'something is wrong in the configuration'
      );
    }
    if (!includeFile) {
      selector = ONLY_LINK;
    }
    if (includeFile && !includeLink && !includeInternal) {
      selector = ONLY_FILE;
    }
    // Only internal option, no link
    if (!includeLink && includeInternal) {
      selector = INTERNAL_LINK;
    }
    if (includeLink && hasUriPattern) {
      selector = PATTERNED_LINK;
    }

    setSelectorType(selector);
  }, [entityType, hasUriPattern]);

  const handleCreateEntry = async (
    entry,
    graph,
    _context,
    createEntryParams
  ) => {
    const isInputLink =
      selectorType === ONLY_LINK ||
      selectorType === PATTERNED_LINK ||
      (selectorType === LINK_AND_FILE && createEntryParams.isLink);

    let newEntry;
    if (!isInputLink) {
      newEntry = await createEntityTypeEntryFromFile({
        context: entityTypeContext,
        graph,
        ...createEntryParams,
      });
    } else {
      let isValidLink = true;
      if (hasUniqueURIScope) {
        const { link } = createEntryParams;
        isValidLink = await checkURIUniquenessScope(
          entityType.uniqueURIScope,
          link,
          entityTypeContext,
          entityType.rdfType
        );
        setIsURIUnique(isValidLink);
      }

      if (!isValidLink) throw new Error('URL not available');
      newEntry = await createEntityTypeEntryFromLink({
        context: entityTypeContext,
        prototypeEntry: entry,
        graph,
        uriPattern,
        ...createEntryParams,
      });
    }
    dispatch({ type: CREATE });
    addSnackbar({ message: translate('createDocumentSuccess') });
    return newEntry;
  };

  return (
    <CreateEntryByLinkOrFile
      {...actionParams}
      entry={prototypeEntry}
      nlsBundles={nlsBundles}
      closeDialog={closeDialog}
      createEntry={handleCreateEntry}
      editorLevel={LEVEL_RECOMMENDED}
      selectorType={selectorType}
      entityTypeUriPatternBase={entityTypeUriPatternBase}
      entityType={entityType}
      createErrorNls="createDocumentFail"
    />
  );
};

CreateDocumentDialog.propTypes = {
  actionParams: PropTypes.shape({
    entityType: PropTypes.shape({
      rdfType: PropTypes.arrayOf(PropTypes.string),
      uriPattern: PropTypes.string,
      uniqueURIScope: PropTypes.string,
      includeFile: PropTypes.bool,
      includeLink: PropTypes.bool,
      includeInternal: PropTypes.bool,
    }),
  }),
  closeDialog: PropTypes.func.isRequired,
};

export default withURIScopeContextProvider(CreateDocumentDialog);
