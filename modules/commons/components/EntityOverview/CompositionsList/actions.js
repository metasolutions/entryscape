import {
  LIST_ACTION_REMOVE,
  LIST_ACTION_REVISIONS,
} from 'commons/components/EntryListView/actions';
import {
  canReplaceEntityLink,
  canReplaceEntityFile,
  canDownloadEntity,
} from 'commons/util/store';
import DownloadResourceDialog from 'commons/components/common/dialogs/DownloadResourceDialog';
import ReplaceLinkDialog from 'commons/components/common/dialogs/ReplaceLinkDialog';
import ReplaceEntityFileDialog from '../dialogs/ReplaceEntityFileDialog';
import RemoveSubEntityDialog from '../dialogs/RemoveSubEntityDialog';

export const actions = [
  {
    id: 'replace-link',
    Dialog: ReplaceLinkDialog,
    isVisible: ({ entry }) => canReplaceEntityLink(entry),
    labelNlsKey: 'replaceLinkMenu',
    getProps: ({ translate, addSnackbar }) => ({
      onLinkChangeSuccess: () =>
        addSnackbar({ message: translate('linkChangeSnackbar') }),
    }),
  },
  {
    id: 'replace-file',
    isVisible: ({ entry }) => canReplaceEntityFile(entry),
    labelNlsKey: 'replaceFileButtonLabel',
    Dialog: ReplaceEntityFileDialog,
    getProps: ({ translate, addSnackbar }) => ({
      onSuccess: () =>
        addSnackbar({ message: translate('fileChangeSnackbar') }),
    }),
  },
  {
    id: 'download',
    isVisible: ({ entry }) => canDownloadEntity(entry),
    labelNlsKey: 'downloadButton',
    Dialog: DownloadResourceDialog,
  },
  LIST_ACTION_REVISIONS,
  { ...LIST_ACTION_REMOVE, Dialog: RemoveSubEntityDialog },
];
