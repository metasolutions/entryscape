import { useEffect } from 'react';
import PropTypes from 'prop-types';
import { graphPropType } from 'commons/util/entry';
import useAsync from 'commons/hooks/useAsync';
import { findExtendedEntry } from 'models/utils/extension';
import { getPathFromViewName } from 'commons/util/site';
import { Link as LinkPresenter } from 'commons/components/forms/presenters';
import { isForm } from 'models/utils/forms';

export const ExtendedLink = ({ graph, resourceURI, ...props }) => {
  const { data: extendedEntry, runAsync } = useAsync();

  useEffect(() => {
    if (!graph) return;

    runAsync(findExtendedEntry(graph, resourceURI));
  }, [runAsync, graph, resourceURI]);

  if (!extendedEntry) return null;

  const getPath = () => {
    if (!graph) return '';
    const viewName = isForm({ graph, resourceURI })
      ? 'models__forms__form'
      : 'models__fields__field';
    return getPathFromViewName(viewName, {
      entryId: extendedEntry.getId(),
      contextId: extendedEntry.getContext().getId(),
    });
  };

  return (
    <LinkPresenter
      {...props}
      graph={extendedEntry.getMetadata()}
      resourceURI={extendedEntry.getResourceURI()}
      property="dcterms:title"
      to={getPath()}
    />
  );
};

ExtendedLink.propTypes = {
  graph: graphPropType,
  resourceURI: PropTypes.string,
  getView: PropTypes.func,
};
