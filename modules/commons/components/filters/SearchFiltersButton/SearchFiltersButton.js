import { IconButton, Badge } from '@mui/material';
import FilterIcon from '@mui/icons-material/FilterAlt';
import Tooltip from 'commons/components/common/Tooltip';
import PropTypes from 'prop-types';
import { useTranslation } from 'commons/hooks/useTranslation';
import escoFiltersNLS from 'commons/nls/escoFilters.nls';
import { useListModel, TOGGLE_FILTERS } from 'commons/components/ListView';

const SearchFilterButton = ({ disabled, filtersActiveBadge }) => {
  const t = useTranslation(escoFiltersNLS);
  const [, dispatch] = useListModel();

  return (
    <Tooltip title={t('searchFiltersButtonTooltip')}>
      <span>
        <IconButton
          aria-label="filter"
          disabled={disabled}
          onClick={() => dispatch({ type: TOGGLE_FILTERS })}
          color="primary"
        >
          <Badge
            color="secondary"
            variant="dot"
            invisible={!filtersActiveBadge}
          >
            <FilterIcon />
          </Badge>
        </IconButton>
      </span>
    </Tooltip>
  );
};

SearchFilterButton.propTypes = {
  disabled: PropTypes.bool,
  filtersActiveBadge: PropTypes.bool,
};

export default SearchFilterButton;
