import { useState, useCallback } from 'react';

// In general it should be avoided to use this way of trigger react component
// rerender. But in some cases when working with imperative dependendencies such
// as classes this may be needed.
const useRefresh = () => {
  const [, setState] = useState({});

  const refresh = useCallback(() => {
    setState({});
  }, []);

  return refresh;
};

export default useRefresh;
