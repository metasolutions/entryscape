/* eslint-disable no-unused-vars */
module.exports = {
  process(sourceText, _sourcePath, _options) {
    return {
      code: `module.exports = ${sourceText};`,
    };
  },
};
