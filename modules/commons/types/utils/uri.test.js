import { BASE_URI, DOMAIN } from '@test-utils/constants';
import { normalizeUriPattern } from './uri';

describe('normalizeUriPattern', () => {
  test('should replace domain placeholder correctly', () => {
    expect(normalizeUriPattern('${domain}/1/entity/1/')).toBe(
      `${DOMAIN}/1/entity/1/`
    );
  });

  test('should replace baseURI placeholder correctly', () => {
    expect(normalizeUriPattern('${baseURI}1/entity/1/')).toBe(
      `${BASE_URI}1/entity/1/`
    );
  });

  test('should handle empty uri patterns', () => {
    expect(normalizeUriPattern('')).toBe('');
    expect(normalizeUriPattern(undefined)).toBe(undefined);
  });
});
