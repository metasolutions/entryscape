import { getIconFromActionId } from 'commons/actions';
import {
  LIST_ACTION_EDIT,
  LIST_ACTION_REMOVE,
  LIST_ACTION_SHARING_SETTINGS,
} from 'commons/components/EntryListView/actions';
// eslint-disable-next-line max-len
import ListRemoveContextDialog from 'commons/components/EntryListView/dialogs/ListRemoveContextDialog';
import esmoModelsNLS from 'models/nls/esmoModels.nls';
import escoListNLS from 'commons/nls/escoList.nls';
import esmoCommonsNLS from 'models/nls/esmoCommons.nls';
import escoDialogsNLS from 'commons/nls/escoDialogs.nls';
import { hasAdminRights } from 'commons/util/user';
import ProjectTypeChangeDialog from 'commons/types/dialogs/ProjectTypeChangeDialog';
import Lookup from 'commons/types/Lookup';
import ImportRfsBundleDialog from '../dialogs/ImportRfsBundleDialog';
import ImportRDFSDialog from '../dialogs/ImportRDFSDialog';
import CreateModelDialog from '../dialogs/CreateModel';

export const nlsBundles = [
  esmoModelsNLS,
  escoListNLS,
  esmoCommonsNLS,
  escoDialogsNLS,
];

export const listActions = [
  {
    id: 'create',
    Dialog: CreateModelDialog,
    labelNlsKey: 'createButtonLabel',
    tooltipNlsKey: 'createModelTitle',
  },
];

export const rowActions = [
  { ...LIST_ACTION_EDIT, formTemplateId: 'esc:Context' },
  {
    id: 'change-projecttype',
    Dialog: ProjectTypeChangeDialog,
    isVisible: ({ userEntry, userInfo }) =>
      hasAdminRights(userEntry) &&
      Lookup.getProjectTypeOptions(userInfo).length,
    labelNlsKey: 'changeProjectTypeLabel',
  },
  {
    id: 'import',
    Dialog: ImportRfsBundleDialog,
    labelNlsKey: 'importRfsButtonLabel',
    tooltipNlsKey: 'importTooltip',
  },
  {
    id: 'import-rdfs',
    icon: getIconFromActionId('import'),
    Dialog: ImportRDFSDialog,
    labelNlsKey: 'importRDFSLabel',
    tooltipNlsKey: 'importRDFSTooltip',
  },
  LIST_ACTION_SHARING_SETTINGS,
  { ...LIST_ACTION_REMOVE, Dialog: ListRemoveContextDialog },
];
