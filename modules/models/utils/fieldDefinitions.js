import * as ns from 'models/utils/ns';
import config from 'config';
import {
  LanguageLiteralEditor,
  OrderedFieldsEditor,
  RadioEditor,
  LiteralEditor,
  SelectEntryEditor,
  LANGUAGE_LITERAL,
  URI,
  LITERAL,
  SelectEditor,
  Subform,
} from 'commons/components/forms/editors';
import {
  AttributesEditor,
  CardinalityEditor,
  ConstraintEditor,
  DepsEditor,
  LanguageEditor,
  NodetypeEditor,
  PropertyEditor,
} from 'models/editors';
import {
  FIELD_TYPE_CHOICES,
  FIELD_NODETYPE_CHOICES,
  PROPERTY_EDIT_MODE_CHOICES,
  FIELD_STATUS_CHOICES,
} from 'models/utils/choiceDefinitions';
import {
  Literal,
  LanguageLiteral,
  Choices,
  Presenter,
} from 'commons/components/forms/presenters';
import { builtinNamespacesAbbreviations } from 'models/namespaces/utils/builtinNamespaces';
import { getOptionsValue } from 'models/fields/utils/options';
import { getLabel } from 'commons/util/rdfUtils';
import {
  AlternativeTexts,
  Attributes,
  Cardinality,
  ExtendedLink,
  Property,
} from '../presenters';
import { ObjectFormLink } from '../presenters/ObjectFormLink';
import { Constraints } from '../presenters/Constraints';
import { getModelName, getType } from './metadata';
import { STATUS_FILTER } from './filterDefinitions';
import { getDeps } from './deps';

const DATATYPE_NAMESPACES = [
  'xsd:decimal',
  'xsd:integer',
  'xsd:boolean',
  'xsd:duration',
  'xsd:date',
  'xsd:dateTime',
  'xsd:time',
  'xsd:gYear',
  'xsd:gYearMonth',
  'xsd:gMonthDay',
  'xsd:anyURI',
  'rdf:JSON',
  'rdf:HTML',
  'rdf:XMLLiteral',
  'gsp:wktLiteral',
];

export const FIELD_TITLE = {
  name: 'title',
  property: 'dcterms:title',
  nodetype: LANGUAGE_LITERAL,
  labelNlsKey: 'titleLabel',
  Editor: LanguageLiteralEditor,
  Presenter: LanguageLiteral,
  mandatory: true,
  extendable: true,
};

export const FIELD_DCT_DESCRIPTION = {
  name: 'dct-description',
  property: 'dcterms:description',
  nodetype: LANGUAGE_LITERAL,
  labelNlsKey: 'descriptionLabel',
  Editor: LanguageLiteralEditor,
  Presenter: LanguageLiteral,
  mandatory: false,
};

export const FIELD_ALTERNATIVE_TEXT = {
  name: 'alternative-text',
  rdfType: ns.RDF_TYPE_TEXT,
  property: ns.RDF_PROPERTY_TEXT,
  labelNlsKey: 'alternativeTextLabel',
  placeholderNlsKey: 'alternativeTextPlaceholder',
  Editor: Subform,
  Presenter: AlternativeTexts,
  editors: [
    {
      name: 'alternative-text-role',
      labelNlsKey: 'textRoleLabel',
      Editor: SelectEditor,
      nodetype: LITERAL,
      property: ns.RDF_PROPERTY_ROLE,
      getChoices: () => config.get('models.alternativeTexts'),
      mandatory: true,
      xs: 5,
      max: 1,
    },
    {
      ...FIELD_TITLE,
      property: ns.RDF_PROPERTY_VALUE,
      labelNlsKey: 'textLabel',
    },
  ],
};

export const FIELD_TYPE = {
  name: 'type',
  property: 'rdf:type',
  labelNlsKey: 'fieldType',
  nodetype: 'URI',
  typeField: true,
  choices: FIELD_TYPE_CHOICES,
  max: 1,
  Editor: RadioEditor,
  mandatory: true,
  row: true,
  isFormType: true,
  jsonAttribute: 'type',
};

export const FIELD_PROPERTY = {
  name: 'property',
  property: ns.RDF_PROPERTY_PROPERTY,
  nodetype: URI,
  labelNlsKey: 'propertyLabel',
  Editor: PropertyEditor,
  Presenter: Property,
  max: 1,
  min: 1,
  jsonAttribute: 'property',
  editChoices: PROPERTY_EDIT_MODE_CHOICES,
};

export const FIELD_NODETYPE = {
  name: 'nodetype',
  property: ns.RDF_PROPERTY_NODETYPE,
  labelNlsKey: 'nodetypeLabel',
  choices: FIELD_NODETYPE_CHOICES,
  Editor: NodetypeEditor,
  Presenter: Choices,
  jsonAttribute: 'nodetype',
  mandatory: true,
  resetOnFormType: true,
  isVisible: (graph, resourceURI) =>
    [ns.RDF_TYPE_TEXT_FIELD, ns.RDF_TYPE_SELECT_FIELD].some((type) =>
      Boolean(graph.find(resourceURI, 'rdf:type', type).length)
    ),
};

export const FIELD_DATATYPE = {
  name: 'datatype',
  property: ns.RDF_PROPERTY_DATATYPE,
  Editor: OrderedFieldsEditor,
  Presenter: Choices,
  labelNlsKey: 'datatypeLabel',
  nodetype: URI,
  choices: DATATYPE_NAMESPACES.map((namespace) => ({
    label: namespace,
    value: namespace,
  })),
  ordered: true,
  orderProperty: ns.RDF_PROPERTY_ORDER,
  resetOnFormType: true,
  isVisible: (graph, resourceURI) =>
    Boolean(
      graph.find(resourceURI, 'rdf:type', ns.RDF_TYPE_DATATYPE_FIELD).length
    ),
  jsonAttribute: 'datatype',
};

export const FIELD_STATUS = {
  name: 'status',
  property: ns.RDF_PROPERTY_STATUS,
  labelNlsKey: 'statusLabel',
  nodetype: URI,
  choices: FIELD_STATUS_CHOICES,
  max: 1,
  row: true,
  Editor: RadioEditor,
  Presenter: Choices,
  extendable: true,
};

export const FIELD_CARDINALITY = {
  name: 'cardinality',
  property: `${ns.RDF_PROPERTY_PREF} ${ns.RDF_PROPERTY_MIN} ${ns.RDF_PROPERTY_MAX}`,
  Editor: CardinalityEditor,
  Presenter: Cardinality,
  extendable: true,
  labelNlsKey: 'cardinalityLabel',
  jsonAttribute: 'cardinality',
};

export const FIELD_LABEL = {
  name: 'label',
  property: ns.RDF_PROPERTY_LABEL,
  Editor: LanguageLiteralEditor,
  Presenter: LanguageLiteral,
  nodetype: LANGUAGE_LITERAL,
  extendable: true,
  labelNlsKey: 'labelLabel',
  jsonAttribute: 'label',
};

export const FIELD_DESCRIPTION = {
  name: 'description',
  property: ns.RDF_PROPERTY_DESCRIPTION,
  Editor: LanguageLiteralEditor,
  Presenter: LanguageLiteral,
  nodetype: LANGUAGE_LITERAL,
  extendable: true,
  labelNlsKey: 'descriptionLabel',
  multiline: true,
  jsonAttribute: 'description',
};

export const FIELD_PURPOSE = {
  name: 'purpose',
  property: ns.RDF_PROPERTY_PURPOSE,
  Editor: LanguageLiteralEditor,
  Presenter: LanguageLiteral,
  nodetype: LANGUAGE_LITERAL,
  labelNlsKey: 'purposeLabel',
  multiline: true,
  extendable: true,
  jsonAttribute: 'purpose',
};

export const FIELD_HELP = {
  name: 'help',
  property: ns.RDF_PROPERTY_HELP,
  Editor: LanguageLiteralEditor,
  Presenter: LanguageLiteral,
  nodetype: LANGUAGE_LITERAL,
  extendable: true,
  labelNlsKey: 'helpLabel',
  multiline: true,
  jsonAttribute: 'help',
};

export const FIELD_PLACEHOLDER = {
  name: 'placeholder',
  property: ns.RDF_PROPERTY_PLACEHOLDER,
  Editor: LanguageLiteralEditor,
  Presenter: LanguageLiteral,
  nodetype: LANGUAGE_LITERAL,
  extendable: true,
  labelNlsKey: 'placeholderLabel',
  jsonAttribute: 'placeholder',
};

export const FIELD_SPECIFICATION = {
  name: 'specification',
  property: ns.RDF_PROPERTY_SPECIFICATION,
  nodetype: LANGUAGE_LITERAL,
  Editor: LanguageLiteralEditor,
  Presenter: LanguageLiteral,
  labelNlsKey: 'specificationLabel',
  jsonAttribute: 'specification',
};

export const FIELD_EDIT_LABEL = {
  name: 'editlabel',
  property: ns.RDF_PROPERTY_EDIT_LABEL,
  nodetype: LANGUAGE_LITERAL,
  Editor: LanguageLiteralEditor,
  Presenter: LanguageLiteral,
  extendable: true,
  labelNlsKey: 'editLabelLabel',
  jsonAttribute: 'editlabel',
};

export const FIELD_EDIT_DESCRIPTION = {
  name: 'editdescription',
  property: ns.RDF_PROPERTY_EDIT_DESCRIPTION,
  nodetype: LANGUAGE_LITERAL,
  Editor: LanguageLiteralEditor,
  Presenter: LanguageLiteral,
  extendable: true,
  labelNlsKey: 'editDescriptionLabel',
  multiline: true,
  jsonAttribute: 'editdescription',
};

export const FIELD_CSS = {
  name: 'cls',
  property: ns.RDF_PROPERTY_CSS,
  Editor: LiteralEditor,
  Presenter: Literal,
  extendable: true,
  labelNlsKey: 'cssClassLabel',
  jsonAttribute: 'cls',
};

export const FIELD_ATTRIBUTE = {
  name: 'attribute',
  property: ns.RDF_PROPERTY_ATTRIBUTE,
  Presenter: Attributes,
  Editor: AttributesEditor,
  extendable: true,
  labelNlsKey: 'attributesLabel',
  jsonAttribute: 'styles',
};

export const FIELD_CONSTRAINT = {
  name: 'constraints',
  property: ns.RDF_PROPERTY_CONSTRAINT,
  Editor: ConstraintEditor,
  Presenter: Constraints,
  labelNlsKey: 'constraintLabel',
  isVisible: (graph, resourceURI) =>
    [
      ns.RDF_TYPE_LOOKUP_FIELD,
      ns.RDF_TYPE_PROFILE_FORM,
      ns.RDF_TYPE_PROPERTY_FORM,
      ns.RDF_TYPE_OBJECT_FORM,
    ].some((type) => Boolean(graph.find(resourceURI, 'rdf:type', type).length)),
  jsonAttribute: 'constraints',
};

export const FIELD_LANGUAGE = {
  name: 'language',
  property: ns.RDF_PROPERTY_LANGUAGE,
  Editor: LanguageEditor,
  Presenter: Choices,
  labelNlsKey: 'languageLabel',
  isVisible: (graph, resourceURI) =>
    Boolean(graph.find(resourceURI, 'rdf:type', ns.RDF_TYPE_TEXT_FIELD).length),
  jsonAttribute: 'language',
};

export const FIELD_EXTENDS = {
  name: 'extends',
  property: ns.RDF_PROPERTY_EXTENDS,
  choices: FIELD_TYPE_CHOICES,
  labelNlsKey: 'extendsLabel',
  mandatory: true,
  Presenter: ExtendedLink,
  jsonAttribute: 'extends',
};

export const FIELD_VALUE = {
  name: 'value',
  property: ns.RDF_PROPERTY_VALUE,
  labelNlsKey: 'valueLabel',
  jsonAttribute: 'value',
};

export const FIELD_VALUE_TEMPLATE = {
  name: 'template',
  property: ns.RDF_PROPERTY_VALUE_TEMPLATE,
  labelNlsKey: 'valueTemplateLabel',
  max: 1,
  Editor: LiteralEditor,
  Presenter: Literal,
  isVisible: (graph, resourceURI) =>
    [ns.RDF_TYPE_TEXT_FIELD, ns.RDF_TYPE_DATATYPE_FIELD].some((type) =>
      Boolean(graph.find(resourceURI, 'rdf:type', type).length)
    ),
  jsonAttribute: 'valueTemplate',
};

export const FIELD_PATTERN = {
  name: 'pattern',
  property: ns.RDF_PROPERTY_PATTERN,
  labelNlsKey: 'patternLabel',
  max: 1,
  Editor: LiteralEditor,
  Presenter: Literal,
  isVisible: (graph, resourceURI) =>
    [
      ns.RDF_TYPE_TEXT_FIELD,
      ns.RDF_TYPE_DATATYPE_FIELD,
      ns.RDF_TYPE_LOOKUP_FIELD,
      ns.RDF_TYPE_OBJECT_FORM,
    ].some((type) => Boolean(graph.find(resourceURI, 'rdf:type', type).length)),
  jsonAttribute: 'pattern',
};

export const FIELD_ABBREVIATION = {
  name: 'abbreviation',
  property: ns.RDF_TYPE_PREFIX,
  Editor: LiteralEditor,
  Presenter: Literal,
  nodetype: LITERAL,
  labelNlsKey: 'abbreviationLabel',
  mandatory: true,
  max: 1,
  xs: 12,
  validate: (validate, fieldGroup) => {
    return (isMandatory) => {
      const validationResult = validate(isMandatory);
      if (validationResult) return validationResult;
      const [field] = fieldGroup.getFields();
      if (!field) return;
      const namespaceAbbreviationValue = field.getValue();
      const hasBuiltinNamespaceError =
        namespaceAbbreviationValue &&
        builtinNamespacesAbbreviations.includes(namespaceAbbreviationValue);
      if (hasBuiltinNamespaceError) {
        field.setError('builtinNamespaceError');
        return field.getError();
      }
    };
  },
};

export const FIELD_NAMESPACE = {
  name: 'namespace',
  property: ns.RDF_TYPE_NAMESPACE,
  Editor: LiteralEditor,
  nodetype: URI,
  labelNlsKey: 'namespaceLabel',
  mandatory: true,
  max: 1,
  xs: 12,
};

export const FIELD_MODEL = {
  name: 'model',
  direct: false,
  getValue: ({ entry }) => getModelName(entry.getContext()),
  labelNlsKey: 'modelLabel',
};

export const FIELD_FIELD_FORM_TYPE = {
  name: 'field-form-type',
  labelNlsKey: 'typeLabel',
  getValue: getType,
};

export const FIELDS_FORMS_INFO = [
  FIELD_TITLE,
  FIELD_FIELD_FORM_TYPE,
  FIELD_PURPOSE,
  FIELD_PROPERTY,
  FIELD_MODEL,
  FIELD_EXTENDS,
  FIELD_STATUS,
];

export const FIELD_INLINE = {
  name: 'field-inline',
  property: ns.RDF_PROPERTY_INLINE,
  labelNlsKey: 'inlineFormLabel',
  nodetype: URI,
  mandatory: true,
  max: 1,
  Editor: SelectEntryEditor,
  Presenter: ObjectFormLink,
  selectEntryTooltipNlsKey: 'selectInlineFormTooltip',
  dialogProps: {
    rdfType: [ns.RDF_TYPE_OBJECT_FORM, ns.RDF_TYPE_PROPERTY_FORM],
    infoFields: FIELDS_FORMS_INFO,
    filters: [STATUS_FILTER],
  },
  resetOnFormType: true,
  isVisible: (graph, resourceURI) =>
    Boolean(
      graph.find(resourceURI, 'rdf:type', ns.RDF_TYPE_INLINE_FIELD).length
    ),
};

export const FIELD_OPTIONS = {
  name: 'field-options',
  property: ns.RDF_PROPERTY_OPTION,
  labelNlsKey: 'optionsFieldLabel',
  Presenter,
  inline: true,
  getValue: getOptionsValue,
  isVisible: (graph, resourceURI) =>
    Boolean(
      graph.find(resourceURI, 'rdf:type', ns.RDF_TYPE_SELECT_FIELD).length
    ),
};

export const FIELD_DEPS = {
  name: 'deps',
  property: ns.RDF_PROPERTY_DEPENDS_ON,
  labelNlsKey: 'depsFieldLabel',
  Editor: DepsEditor,
  Presenter,
  extendable: true,
  getValue: async ({ graph, resourceURI }) => {
    const [entry, value] = await getDeps(graph, resourceURI);
    if (!entry) return '';
    return `${getLabel(entry)}: ${value}`;
  },
  direct: false,
  isVisible: (graph, resourceURI) =>
    [
      ns.RDF_TYPE_TEXT_FIELD,
      ns.RDF_TYPE_DATATYPE_FIELD,
      ns.RDF_TYPE_SELECT_FIELD,
    ].some((type) => Boolean(graph.find(resourceURI, 'rdf:type', type).length)),
};

export const FIELD_OVERRIDE_FORM_ITEMS = {
  name: 'override-formitems',
  property: ns.RDF_PROPERTY_OVERRIDE_FORM_ITEMS,
  labelNlsKey: 'extendsTypeLabel',
  max: 1,
  nodetype: LITERAL,
  Editor: RadioEditor,
  Presenter: Literal,
  mandatory: true,
  initialValue: 'replace',
  choices: [
    {
      label: 'Replace',
      value: 'replace',
    },
    {
      label: 'Append',
      value: 'append',
    },
  ],
  row: true,
};
