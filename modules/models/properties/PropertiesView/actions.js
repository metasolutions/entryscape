import escoListNLS from 'commons/nls/escoList.nls';
import esmoPropertiesNLS from 'models/nls/esmoProperties.nls';
import esmoCommonsNLS from 'models/nls/esmoCommons.nls';
import escoDialogsNLS from 'commons/nls/escoDialogs.nls';
import ImportEntriesDialog from 'commons/components/common/dialogs/ImportEntriesDialog';
import { withListRefresh } from 'commons/components/ListView';
import { RDF_TYPE_PROPERTY, RDF_TYPE_MODEL } from 'models/utils/ns';
import CreateEntryDialog from '../../dialogs/CreateEntryDialog';

export const nlsBundles = [
  esmoPropertiesNLS,
  esmoCommonsNLS,
  escoListNLS,
  escoDialogsNLS,
];

export const listActions = [
  {
    id: 'link-entry',
    Dialog: withListRefresh(ImportEntriesDialog, 'afterCreateEntry'),
    labelNlsKey: 'reuseHeader',
    tooltipNlsKey: 'reuseTitle',
    rdfType: RDF_TYPE_PROPERTY,
    contextRdfType: RDF_TYPE_MODEL,
  },
  {
    id: 'create',
    Dialog: CreateEntryDialog,
    labelNlsKey: 'createButtonLabel',
    tooltipNlsKey: 'createPropertyTitle',
    rdfType: 'rdf:Property',
  },
];
