import { useState } from 'react';
import PropTypes from 'prop-types';
import { entryPropType } from 'commons/util/entry';
import ListActionDialog from 'commons/components/common/dialogs/ListActionDialog';
import { useTranslation } from 'commons/hooks/useTranslation';
import AcknowledgeDialog from 'commons/components/common/dialogs/AcknowledgeDialog';
import useAsync, { PENDING } from 'commons/hooks/useAsync';
import {
  useVisualizationState,
  VisualizationStateProvider,
} from 'catalog/datasets/DatasetOverview/hooks/useVisualizationState';
import { ErrorCatcher } from 'commons/errors/ErrorCatcher';
import { ErrorWithMessage } from 'commons/errors/utils/errors';
import useErrorHandler from 'commons/errors/hooks/useErrorHandler';
import LoadingButton from 'commons/components/LoadingButton';
import { nlsBundles } from '../../VisualizationList/actions';
import useVisualizationData from '../../hooks/useVisualizationData';
import VisualizationForm, { isComplete } from '../../VisualizationForm';
import {
  updateVisualizationMetadata,
  createVisualization,
  createPrototypeEntry,
} from '../../utils/visualizations';

const CreateVisualizationDialog = ({
  actionParams,
  visualizationEntry,
  closeDialog,
}) => {
  const translate = useTranslation(nlsBundles);
  const [datasetEntry, refreshVisualizations] = actionParams;
  const { error: createError, runAsync, status } = useAsync();
  const { fieldsState, isBroken, hasChanged } = useVisualizationState();

  const handleCreateEntry = () => {
    updateVisualizationMetadata(visualizationEntry, fieldsState);
    runAsync(
      createVisualization(datasetEntry, visualizationEntry)
        .then(refreshVisualizations)
        .then(closeDialog)
        .catch((error) => {
          throw new ErrorWithMessage(translate('createFail'), error);
        })
    );
  };

  const createDisabled = isBroken || !hasChanged || !isComplete(fieldsState);

  const dialogActions = (
    <LoadingButton
      onClick={handleCreateEntry}
      loading={status === PENDING}
      disabled={createDisabled}
    >
      {translate('createButton')}
    </LoadingButton>
  );

  return (
    <>
      <ListActionDialog
        id="create-visualization-entry"
        title={translate('createViz')}
        actions={dialogActions}
        closeDialog={closeDialog}
        fixedHeight
      >
        <VisualizationForm datasetEntry={datasetEntry} />
      </ListActionDialog>
      <ErrorCatcher error={createError} />
    </>
  );
};

CreateVisualizationDialog.propTypes = {
  actionParams: PropTypes.arrayOf(
    PropTypes.oneOfType([entryPropType, PropTypes.func])
  ),
  visualizationEntry: entryPropType,
  closeDialog: PropTypes.func.isRequired,
};

const CreateVisualizationDialogWithProvider = ({
  actionParams,
  closeDialog,
}) => {
  const translate = useTranslation(nlsBundles);
  const [datasetEntry] = actionParams;
  const [visualizationEntry] = useState(
    createPrototypeEntry(datasetEntry.getContext())
  );
  const { data, isLoading, error } = useVisualizationData({ datasetEntry });
  const { sourceChoices } = data;

  useErrorHandler(error);

  if (isLoading) return null;

  if (!sourceChoices.length) {
    return (
      <AcknowledgeDialog
        open
        handleClose={closeDialog}
        buttonLabel={translate('vizAcknowledge')}
      >
        {translate('vizNoDistribution')}
      </AcknowledgeDialog>
    );
  }

  return (
    <VisualizationStateProvider
      visualizationEntry={visualizationEntry}
      sourceChoices={sourceChoices}
    >
      <CreateVisualizationDialog
        actionParams={actionParams}
        visualizationEntry={visualizationEntry}
        closeDialog={closeDialog}
      />
    </VisualizationStateProvider>
  );
};

CreateVisualizationDialogWithProvider.propTypes = {
  actionParams: PropTypes.arrayOf(
    PropTypes.oneOfType([entryPropType, PropTypes.func])
  ),
  closeDialog: PropTypes.func.isRequired,
};

export default CreateVisualizationDialogWithProvider;
