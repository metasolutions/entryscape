import { Icon, Style, Fill, Stroke, Circle } from 'ol/style';
import { asArray } from 'ol/color';
import { GREEN, BLUE } from './colors';

/**
 * Adds opacity to color as alpa by converting string to rgba array. This is
 * needed because opacity is not available as an option for fills and strokes.
 * @param {string} color
 * @param {number} opacity
 * @returns {string[]}
 */
const addOpacity = (color, opacity) => {
  if (opacity === 1) return color;
  const rgbaColor = asArray(color);
  return [...rgbaColor.slice(0, 3), opacity];
};

/**
 * @param {string} svg
 * @returns {string}
 */
const svgToUri = (svg) => {
  return `data:image/svg+xml;charset=utf-8,${encodeURIComponent(svg)}`;
};

const markerPath =
  // eslint-disable-next-line max-len
  '<path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z"></path>';

/**
 * Genereates an inline svg based on an svg path and optional properties
 * @param {Object} svgOptions
 * @returns {string}
 */
const createSvg = ({
  path = markerPath,
  color = GREEN,
  height = 48,
  width = 48,
} = {}) => {
  const attributes = [
    `fill="${color}"`,
    `height="${height}"`,
    `width="${width}"`,
    'version="1.1"',
    'xmlns="http://www.w3.org/2000/svg"',
    'focusable="false"',
    'viewBox="0 0 24 24"',
  ].join(' ');
  return `<svg ${attributes}>${path}</svg>`;
};

/**
 * Creates a marker style based on default marker svg
 * @param {Object} markerOptions
 * @returns {Style}
 */
export const createMarker = ({ color, path } = {}) => {
  return [
    new Style({
      image: new Icon({
        anchor: [0.5, 3],
        anchorYUnits: 'pixels',
        anchorOrigin: 'bottom-left',
        src: svgToUri(createSvg({ path, color })),
      }),
    }),
    new Style({
      image: new Circle({
        fill: new Fill({
          color: 'rgba(0,0,0,0.0)',
        }),
        radius: 5,
        displacement: [0, 27],
      }),
    }),
  ];
};

/**
 *
 * @param {Object} fillOptions
 * @returns {Fill}
 */
export const createFill = ({
  color = BLUE,
  opacity = 0.2,
  ...fillProps
} = {}) => {
  return new Fill({
    color: addOpacity(color, opacity),
    ...fillProps,
  });
};

/**
 * @param {Object} strokeOptions
 * @returns {Stroke}
 */
export const createStroke = ({
  color = BLUE,
  opacity = 1,
  width = 3,
  ...strokeProps
} = {}) => {
  return new Stroke({
    color: addOpacity(color, opacity),
    width,
    ...strokeProps,
  });
};

/**
 * @param {Object} styleOptions
 * @returns {Style}
 */
export const createPolygonStyle = ({
  fill = createFill(),
  stroke = createStroke(),
} = {}) => {
  return new Style({
    fill,
    stroke,
  });
};
