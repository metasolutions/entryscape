import { entrystore } from 'commons/store';
import { Context } from '@entryscape/entrystore-js';
import config from 'config';
import {
  getPathFromViewName,
  getModuleNameOfCurrentView,
} from 'commons/util/site';
import Lookup from 'commons/types/Lookup';
import { EntityType } from 'entitytype-lookup';
import {
  getEntityTypesIncludingRefined,
  getBuiltinEntityTypes,
} from 'commons/types/utils/entityType';

/**
 * Default query to get the number of entries of a given entity type
 *
 * @param {Context} context
 * @param {EntityType} entityType
 * @returns {number}
 */
export const defaultQuery = async (context, entityType) => {
  const searchList = entrystore
    .newSolrQuery()
    .context(context)
    .rdfType(entityType.getRdfType())
    .list('1');

  await searchList.getEntries();
  return searchList.getSize();
};

/**
 *
 * @param {EntityType} entityTypes
 * @param {object} viewParams
 * @param {Context} context
 * @param {Function} translate
 * @param {Function} getQuery
 * @returns {Promise<object[]>}
 */
const getRowActions = (
  entityTypes,
  viewParams,
  context,
  translate,
  getQuery = () => null
) => {
  const rowActionsPromises = entityTypes
    .filter((entityType) => !entityType.get('hideFromOverview'))
    .map(async (entityType) => {
      const query = getQuery(context, entityType) || defaultQuery;
      const queryCount = await query(context, entityType);

      const name = entityType.get('name');
      const link = getPathFromViewName(entityType.get('listview'), viewParams);

      return {
        getProps: () => ({
          label: translate(`${name}Label`) || `${name}Label`,
          labelPrefix: queryCount,
          to: link,
          isPrimary: entityType.get('main'),
        }),
      };
    });

  return Promise.all(rowActionsPromises);
};

/**
 * Sort entity types based on the order of views in the current module
 *
 * @param {EntityType[]} entityTypes
 * @returns {EntityType[]}
 */
const sortEntityTypesOnViews = (entityTypes) => {
  const currentModule = getModuleNameOfCurrentView();
  const views = config
    .get('site.views')
    .filter((view) => view.module === currentModule);
  const viewNames = views.map((view) => view.name);

  return entityTypes.sort((entityTypeA, entityTypeB) => {
    const listViewA = entityTypeA.get('listview');
    const listViewB = entityTypeB.get('listview');
    const indexA = viewNames.indexOf(listViewA);
    const indexB = viewNames.indexOf(listViewB);

    if (indexA === -1 || indexB === -1) return 0;
    return indexA - indexB;
  });
};

/**
 * Gets overview entity types
 *
 * @param {Context} context
 * @param {object} userInfo
 * @param {object} projectType
 * @returns {Promise<EntityType[]>}
 */
const getOverviewEntityTypes = async (context, userInfo, projectType) => {
  const moduleName = getModuleNameOfCurrentView();
  const builtinEntityTypes = getBuiltinEntityTypes(userInfo, moduleName);
  const entityTypesInContext = await Lookup.getEntityTypesInContext(
    context,
    userInfo,
    true
  );
  const entityTypes = projectType?.getIncludeBuiltins()
    ? builtinEntityTypes
    : entityTypesInContext;
  const typesIncludingRefined = getEntityTypesIncludingRefined(entityTypes);
  const overviewEntityTypes = typesIncludingRefined.filter((entityType) => {
    return (
      entityType.get('mainModule') === moduleName &&
      entityType.get('listview') &&
      !entityType.get('refines')
    );
  });
  return overviewEntityTypes;
};

/**
 * Extracts overview items from config
 *
 * @param {string} viewParams
 * @param {Context} context
 * @param {Function} translate
 * @param {Function} getQuery
 * @returns {Promise}
 */

const getOverviewData = async (
  viewParams,
  entry,
  context,
  translate,
  userInfo,
  getQuery,
  sortFunction = sortEntityTypesOnViews
) => {
  const data = {};
  data.projectType = await Lookup.getProjectTypeInUse(context);
  const entityTypes = await getOverviewEntityTypes(
    context,
    userInfo,
    data.projectType
  );
  data.entry = entry;
  data.entityTypes = sortFunction(entityTypes);
  data.rowActions = await getRowActions(
    data.entityTypes,
    viewParams,
    context,
    translate,
    getQuery
  );

  return data;
};

export default getOverviewData;
