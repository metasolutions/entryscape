import PropTypes from 'prop-types';
import { graphPropType } from 'commons/util/entry';
import { Graph } from '@entryscape/rdfjson';
import * as ns from 'models/utils/ns';
import { URI, SelectEditor } from 'commons/components/forms/editors';
import useGetMainDialog from 'commons/hooks/useGetMainDialog';

/**
 * Get available nodetypes
 *
 * @param {Graph} graph
 * @param {string} resourceURI
 * @param {object[]} choices
 * @returns {object[]}
 */
const getAvailableChoices = (graph, resourceURI, choices) => {
  return choices.filter(({ constrainedTo }) => {
    if (!constrainedTo) return true;
    return constrainedTo.find((property) => {
      return graph.find(resourceURI, 'rdf:type', property).length;
    });
  });
};

export const NodetypeEditor = ({
  name,
  label,
  size,
  choices,
  graph,
  resourceURI,
  mandatory,
  resetOnFormType,
  dispatch,
  translate,
  confirmChange,
}) => {
  const nodetypeChoices = getAvailableChoices(graph, resourceURI, choices);
  const { getConfirmationDialog } = useGetMainDialog();

  const handleConfirmChange = async (field, value) => {
    if (!confirmChange) return true;
    const conversion = await confirmChange(field, value);

    if (!conversion) return true;

    const { messageNls, converter } = conversion;
    const proceed = await getConfirmationDialog({
      rejectLabel: translate('reject'),
      affirmLabel: translate('proceed'),
      content: translate(messageNls),
    });
    if (!proceed) return false;
    if (converter) {
      await converter();
    }
    return true;
  };

  return (
    <SelectEditor
      name={name}
      label={label}
      nodetype={URI}
      choices={nodetypeChoices}
      property={ns.RDF_PROPERTY_NODETYPE}
      max={1}
      size={size}
      graph={graph}
      resourceURI={resourceURI}
      mandatory={mandatory}
      resetOnFormType={resetOnFormType}
      dispatch={dispatch}
      confirmChange={handleConfirmChange}
    />
  );
};

NodetypeEditor.propTypes = {
  name: PropTypes.string,
  label: PropTypes.string,
  size: PropTypes.string,
  choices: PropTypes.arrayOf(PropTypes.shape({})),
  graph: graphPropType,
  resourceURI: PropTypes.string,
  mandatory: PropTypes.bool,
  resetOnFormType: PropTypes.bool,
  dispatch: PropTypes.func,
  confirmChange: PropTypes.func,
  translate: PropTypes.func,
};
