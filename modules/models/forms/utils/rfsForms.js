import * as ns from 'models/utils/ns';
import RfsProfileForm from './RfsProfileForm';
import RfsObjectForm from './RfsObjectForm';
import RfsSectionForm from './RfsSectionForm';

export const RFS_FORMS_COMMON_PROPERTIES = [
  'dcterms:title',
  ns.RDF_PROPERTY_STATUS,
  ns.RDF_PROPERTY_PREF,
  ns.RDF_PROPERTY_MIN,
  ns.RDF_PROPERTY_MAX,
  ns.RDF_PROPERTY_LABEL,
  ns.RDF_PROPERTY_DESCRIPTION,
  ns.RDF_PROPERTY_PURPOSE,
  ns.RDF_PROPERTY_EDIT_LABEL,
  ns.RDF_PROPERTY_EDIT_DESCRIPTION,
  ns.RDF_PROPERTY_CSS,
  ns.RDF_PROPERTY_ATTRIBUTE,
];

export const RDF_TYPE_TO_RFS_FORM = {
  [ns.RDF_TYPE_PROFILE_FORM]: RfsProfileForm,
  [ns.RDF_TYPE_OBJECT_FORM]: RfsObjectForm,
  [ns.RDF_TYPE_SECTION_FORM]: RfsSectionForm,
};

export const RFS_FORMS = [RfsProfileForm, RfsObjectForm, RfsSectionForm];
