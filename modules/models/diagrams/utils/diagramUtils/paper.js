import { dia, shapes, linkTools } from 'jointjs';

const scale = (paper, newscale) => {
  if (newscale <= 2 && newscale >= 0.2) {
    const { width, height } = paper.getComputedSize();
    const lp = paper.paperToLocalPoint(width / 2, height / 2);
    paper.scale(newscale);
    const pp = paper.localToPaperPoint(lp);
    const { tx, ty } = paper.translate();
    paper.translate(tx + width / 2 - pp.x, ty + height / 2 - pp.y);
  }
};

export const zoomIn = (paper) => scale(paper, 1.2 * paper.scale().sx);
export const zoomOut = (paper) => scale(paper, paper.scale().sx / 1.2);

export const fitContent = (paper) =>
  paper.transformToFitContent({
    padding: 20,
    verticalAlign: 'middle',
    horizontalAlign: 'middle',
    maxScaleX: 2,
    maxScaleY: 2,
    minScaleX: 0.2,
    minScaleY: 0.2,
    preserveAspectRatio: true,
  });

/**
 *
 * @param {Node} node
 * @param {object} graph
 * @param {Function} handleClick
 * @param {boolean} editMode
 * @returns {object}
 */
export const createPaper = (node, graph, handleClick, editMode) => {
  const paper = new dia.Paper({
    el: node,
    model: graph,
    frozen: true,
    async: true,
    cellViewNamespace: shapes,
    width: null,
    height: null,
    interactive: editMode === true,
    afterRender: (stats, _options, thisPaper) => {
      const { mounted } = stats;
      if (mounted > 0) {
        fitContent(thisPaper);
      }
    },
  });

  /**
   * fieldRelation
   * fieldAttribute
   * propertyRelation
   * propertyAttribute
   * form
   * class
   * implicitClass
   * implicitProperty
   */
  paper.on('cell:pointerclick', (cellView, event) => {
    const { target } = event;
    const parent = target.parentElement;
    const property = cellView.model.prop('property');
    if (property) {
      const message = {
        formEntry: cellView.model.prop('formEntry'),
        itemEntry: cellView.model.prop('itemEntry'),
        propertyEntry: cellView.model.prop('propertyEntry'),
        property,
      };
      if (message.itemEntry) {
        message.type = 'fieldRelation';
      } else if (message.propertyEntry) {
        message.type = 'propertyRelation';
      } else {
        message.type = 'implicitProperty';
      }
      handleClick(message);
    } else if (parent.classList.contains('esmoFieldsText')) {
      const index = Array.prototype.indexOf.call(parent.children, target);
      const defs = cellView.model.get('fieldDefs');
      const attribute = defs[index];

      if (attribute.itemEntry) {
        handleClick({
          type: 'fieldAttribute',
          itemEntry: attribute.itemEntry,
          formEntry: attribute.formEntry,
        });
      } else {
        handleClick({
          type: 'propertyAttribute',
          propertyEntry: attribute.propertyEntry,
        });
      }
    } else {
      const message = {
        formEntry: cellView.model.prop('formEntry'),
        classEntry: cellView.model.prop('classEntry'),
        class: cellView.model.prop('class'),
      };
      if (parent.classList.contains('esmoFormIndicator')) {
        message.type = 'form';
      } else if (
        parent.classList.contains('esmoClassIndicator') &&
        message.classEntry
      ) {
        message.type = 'class';
      } else if (message.formEntry) {
        message.type = 'form';
      } else if (message.classEntry) {
        message.type = 'class';
      } else {
        message.type = 'implicitClass';
      }
      handleClick(message);
    }
  });

  let startX;
  let startY;
  let origin;
  paper.on('blank:pointerdown', (_event, x, y) => {
    origin = paper.options.origin;
    startX = x;
    startY = y;
  });
  paper.on('blank:pointermove', (_event, x, y) => {
    const tx = origin.x + x - startX;
    const ty = origin.y + y - startY;
    paper.translate(tx, ty);
  });

  paper.on('blank:mousewheel', (event, x, y, delta) => {
    event.preventDefault();
    const oldscale = paper.scale().sx;
    const newscale = oldscale + 0.2 * delta * oldscale;

    if (newscale > 0.2 && newscale < 5) {
      paper.scale(newscale, newscale, 0, 0);
      paper.translate(
        -x * newscale + event.offsetX,
        -y * newscale + event.offsetY
      );
    }
  });

  if (editMode) {
    paper.on('link:mouseenter', (linkView) => {
      linkView.addTools(
        new dia.ToolsView({
          tools: [new linkTools.Vertices()],
        })
      );
    });
    paper.on('link:mouseleave', (linkView) => {
      linkView.removeTools();
    });
  }

  return paper;
};
