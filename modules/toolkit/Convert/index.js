import { useState, useEffect } from 'react';
import useNavigate from 'commons/components/router/useNavigate';
import { useClipboardGraph } from 'commons/contexts/ClipboardGraphContext';
import convert from 'toolkit/Convert/utils/convertScript';
import {
  Button,
  Paper,
  Table,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TableBody,
  Typography,
} from '@mui/material';
import esreConvertNLS from 'toolkit/nls/esreConvert.nls';
import esreSourceNLS from 'toolkit/nls/esreSource.nls';
import { useTranslation } from 'commons/hooks/useTranslation';
import { namespaces as ns } from '@entryscape/rdfjson';
import AlertDialog from 'commons/components/common/dialogs/AlertDialog';
import Placeholder from 'commons/components/common/Placeholder';
import { useSnackbar } from 'commons/hooks/useSnackbar';
import withLocationChangeListener from 'toolkit/LocationChangeListener';
import './index.scss';

const Convert = () => {
  const { navigate } = useNavigate();
  const [clipboardGraph] = useClipboardGraph();
  const [conversionReport, setConversionReport] = useState(null);
  const [showAlert, setShowAlert] = useState(false);
  const [addSnackbar] = useSnackbar();

  const t = useTranslation([esreConvertNLS, esreSourceNLS]);

  useEffect(() => {
    if (clipboardGraph && !clipboardGraph.isEmpty()) {
      const report = convert(clipboardGraph, true);
      setConversionReport(report);
    } else {
      setShowAlert(true);
    }
  }, [clipboardGraph]);

  return (
    <>
      <Typography variant="h2" classes={{ root: 'esreConvert__header' }}>
        {t('convertHeader')}
      </Typography>
      {conversionReport?.count > 0 ? (
        <>
          <TableContainer
            component={Paper}
            classes={{ root: 'esreConvert__tableContainer' }}
          >
            <Table aria-label={t('tableLabel')}>
              <caption> {t('toBeConverted', conversionReport.count)}</caption>
              <TableHead>
                <TableRow>
                  <TableCell>{t('onSubject')}</TableCell>
                  <TableCell>{t('issueType')}</TableCell>
                  <TableCell>{t('from')}</TableCell>
                  <TableCell>{t('to')}</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {Object.keys(conversionReport)
                  .filter((prop) => prop !== 'count')
                  .map((prop) =>
                    conversionReport[prop]?.fixes.length
                      ? conversionReport[prop].fixes.map((fix) => (
                          <TableRow key={fix.s}>
                            <TableCell title={fix.s}>
                              {ns.shorten(fix.s)}
                            </TableCell>
                            <TableCell>
                              {fix.t === 'p'
                                ? t('predicateFix')
                                : t('objectFix')}
                            </TableCell>
                            <TableCell title={fix.from}>
                              {ns.shorten(fix.from)}
                            </TableCell>
                            <TableCell title={fix.to}>
                              {ns.shorten(fix.to)}
                            </TableCell>
                          </TableRow>
                        ))
                      : null
                  )}
              </TableBody>
            </Table>
          </TableContainer>
          <div className="esreConvert__tableActions">
            <Button
              onClick={() => {
                convert(clipboardGraph); // bad, refactor convertScript and fix
                addSnackbar({
                  message: t('conversionComplete', conversionReport?.count),
                });
                navigate('/toolkit/catalog');
              }}
            >
              {t('convertButton')}
            </Button>
          </div>
        </>
      ) : (
        <Placeholder label={t('nothingToConvert')} />
      )}
      <AlertDialog
        open={showAlert}
        handleClose={() => {
          setShowAlert(false);
          navigate('/toolkit/catalog');
        }}
        buttonLabel={t('noRDFProceed')}
      >
        {t('noRDF')}
      </AlertDialog>
    </>
  );
};

export default withLocationChangeListener(Convert);
