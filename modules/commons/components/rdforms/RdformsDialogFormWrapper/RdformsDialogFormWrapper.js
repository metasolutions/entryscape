import PropTypes from 'prop-types';
import './style.scss';

export const RdformsDialogFormWrapper = ({ children }) => {
  return <div className="escoRDFormsDialogWrapper">{children}</div>;
};

RdformsDialogFormWrapper.propTypes = {
  children: PropTypes.node,
};

export const RdformsStickyDialogContent = ({ children }) => {
  return <div className="escoRDFormsDialogWrapper__sticky">{children}</div>;
};

RdformsStickyDialogContent.propTypes = {
  children: PropTypes.node,
};

export const RdformsDialogContent = ({ children }) => {
  return <div className="escoRDFormsDialogWrapper__content">{children}</div>;
};

RdformsDialogContent.propTypes = {
  children: PropTypes.node,
};
