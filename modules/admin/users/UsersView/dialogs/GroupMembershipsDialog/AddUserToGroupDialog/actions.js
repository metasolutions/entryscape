import esadGroupNLS from 'admin/nls/esadGroup.nls';
import escoListNLS from 'commons/nls/escoList.nls';

export const nlsBundles = [esadGroupNLS, escoListNLS];
