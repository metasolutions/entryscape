import { useState, useEffect } from 'react';
import {
  Presenter as RDFormsPresenter,
  Editor as RDFormsEditor,
  engine,
} from '@entryscape/rdforms/renderers/react';

export default (view, value) => {
  const [component, setComponent] = useState(null);

  useEffect(() => {
    if (view === 'present') {
      const comp = new RDFormsPresenter({
        binding: value.parent,
        restrictToItem: value.item,
        filterTranslations: false,
        compact: false,
      });
      setComponent(comp);
    } else if (view === 'edit') {
      const valueBindings = value.parent.getChildBindingsFor(value.item);
      if (!valueBindings || valueBindings.length === 0) {
        engine.create(value.parent, value.item);
      }

      const comp = new RDFormsEditor({
        binding: value.parent,
        restrictToItem: value.item,
        includeLevel: 'optional',
        compact: false,
        renderingParams: {
          ChoiceSelector: { disablePortal: false },
          ChoiceLookupAndInlineSearch: { disablePortal: false },
        },
      });
      setComponent(comp);
    } else {
      setComponent(null);
    }
  }, [view]);
  return component ? component.domNode.component : null;
};
