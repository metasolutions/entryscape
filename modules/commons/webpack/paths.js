/* eslint-disable no-undef */
/**
 * Free variables defined in webpack on during build
 * {boolean} DEVELOPMENT
 * {string} DEVELOPMENT_HOST
 * {string} VERSION
 */

/**
 * store version build number in session
 */
// window.sessionStorage.setItem('version', VERSION);

/**
 * determine a default base path for assets
 */
const { localBuild, static: staticConfig } = __entryscape_config.entryscape;
let basePath;
if (DEVELOPMENT) {
  // no entryscape.static configuration needed
  basePath = DEVELOPMENT_HOST;
} else if (localBuild) {
  // entryscape.static.url needed in config when entryscape.localBuild = true
  basePath = staticConfig.url.replace(/\/?$/, '/');
} else {
  // all entryscape.static.<url|app|version> needed when it's not a local build and DEVELOPMENT = false
  const { url, app, version } = staticConfig;
  basePath = new URL(`${app}/${version}/`, url).href;
}

const BASE_PATH = basePath;
const DEFAULT_ASSETS_PATH = new URL('assets/', BASE_PATH).href;
const STATIC_PATH = ENTRYSCAPE_STATIC;

export { BASE_PATH, DEFAULT_ASSETS_PATH, STATIC_PATH };
