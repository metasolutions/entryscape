import PropTypes from 'prop-types';
import { RDF_STATUS_ARCHIVED, entryPropType } from 'commons/util/entry';
import ConfirmationDialog from '../ConfirmationDialog';
import { archiveOrUnarchiveSuggestion } from '../../util';

const ArchiveDialog = ({ entry, closeDialog, onArchive }) => {
  return (
    <ConfirmationDialog
      buttonLabel="archiveButtonLabel"
      dialogConfirmationQuestionNLS="archiveSuggestion"
      closeDialog={closeDialog}
      buttonAction={() =>
        archiveOrUnarchiveSuggestion(entry, RDF_STATUS_ARCHIVED)
          .then(onArchive)
          .then(closeDialog)
      }
    />
  );
};

ArchiveDialog.propTypes = {
  closeDialog: PropTypes.func,
  onArchive: PropTypes.func,
  entry: entryPropType,
};

export default ArchiveDialog;
