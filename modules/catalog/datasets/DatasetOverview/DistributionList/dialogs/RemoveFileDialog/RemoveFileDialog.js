import { useEffect } from 'react';
import PropTypes from 'prop-types';
import { Entry } from '@entryscape/entrystore-js';
import { useEntry } from 'commons/hooks/useEntry';
import ListRemoveEntryDialog from 'commons/components/EntryListView/dialogs/ListRemoveEntryDialog';
import useGetMainDialog from 'commons/hooks/useGetMainDialog';
import { removeFile } from 'catalog/datasets/DatasetOverview/utils/distributions';
import {
  getVisualizationsForFile,
  removeVisualizationsEntriesFromDataset,
} from 'catalog/datasets/DatasetOverview/utils/visualizations';
import useAsync from 'commons/hooks/useAsync';
import escaDatasetNLS from 'catalog/nls/escaDataset.nls';
import escoListNLS from 'commons/nls/escoList.nls';
import escaFilesListNLS from 'catalog/nls/escaFilesList.nls';
import { useTranslation } from 'commons/hooks/useTranslation';

const RemoveFileDialog = ({
  distributionEntry,
  closeDialog,
  entry: fileEntry,
  refreshFileEntries,
  filesCount,
  setEditedFiles,
}) => {
  const datasetEntry = useEntry();
  const translate = useTranslation([
    escaFilesListNLS,
    escaDatasetNLS,
    escoListNLS,
  ]);
  const { data: visualizationEntries, runAsync } = useAsync(null);
  const { getAcknowledgeDialog } = useGetMainDialog();

  useEffect(() => {
    runAsync(getVisualizationsForFile(fileEntry, datasetEntry));
  }, [runAsync, fileEntry, datasetEntry]);

  useEffect(() => {
    if (filesCount === 1) {
      getAcknowledgeDialog({
        content: translate('cannotRemoveSingleFile'),
      }).then(() => closeDialog());
    }
  }, [getAcknowledgeDialog, filesCount, translate, closeDialog]);

  const onRemove = async () => {
    try {
      await removeFile(fileEntry, distributionEntry);
      setEditedFiles(true);

      // Remove visualisations associated with the file being removed.
      if (visualizationEntries.length)
        await removeVisualizationsEntriesFromDataset(
          visualizationEntries,
          datasetEntry
        );
      refreshFileEntries();
    } catch (error) {
      console.error(`An error occurred when trying to remove file: ${error}`);
    } finally {
      closeDialog();
    }
  };

  return filesCount > 1 ? (
    <ListRemoveEntryDialog
      entry={fileEntry}
      closeDialog={closeDialog}
      onRemove={onRemove}
      removeConfirmMessage={
        visualizationEntries?.length
          ? translate('removeFileHasVisualization')
          : translate('confirmRemoveFile')
      }
    />
  ) : null;
};

RemoveFileDialog.propTypes = {
  distributionEntry: PropTypes.instanceOf(Entry),
  closeDialog: PropTypes.func.isRequired,
  entry: PropTypes.instanceOf(Entry),
  refreshFileEntries: PropTypes.func,
  filesCount: PropTypes.number,
  setEditedFiles: PropTypes.func,
};

export default RemoveFileDialog;
