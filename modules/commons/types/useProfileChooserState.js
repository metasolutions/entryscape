import { useState, useEffect } from 'react';
import useGetEntityTypes from 'commons/types/useGetEntityTypes';
import { saveEntryByEntityType } from 'commons/util/entry';
import Lookup from 'commons/types/Lookup';
import useAsync, { IDLE } from 'commons/hooks/useAsync';

const getTemplateId = (entry, entityType) =>
  entry.isLinkReference()
    ? entityType.complementaryTemplateId()
    : entityType.templateId();

const useProfileChooserState = ({
  entry,
  parentEntry,
  onChangeCallback,
  fallbackTemplateId,
}) => {
  const { data: originalEntityType, runAsync, status } = useAsync();
  const { entityTypes } = useGetEntityTypes(entry);
  const [selectedEntityType, setSelectedEntityType] = useState(null);
  const templateId = selectedEntityType
    ? getTemplateId(entry, selectedEntityType)
    : fallbackTemplateId;

  // Only necessary to get the saved/initial entitytype
  useEffect(() => {
    if (status !== IDLE) return;

    runAsync(
      Lookup.inUse(entry, parentEntry).then((initialEntityType) => {
        setSelectedEntityType(initialEntityType);
        return initialEntityType;
      })
    );
  }, [entityTypes, entry, parentEntry, runAsync, selectedEntityType, status]);

  const saveEntityType = async () => {
    if (originalEntityType !== selectedEntityType) {
      return saveEntryByEntityType(entry, selectedEntityType);
    }
    return Promise.resolve();
  };

  // Only called when pressing the profile chooser dialog's "switch" button
  const onChangeEntityType = (newEntityType) => {
    setSelectedEntityType(newEntityType);
    onChangeCallback(newEntityType !== selectedEntityType);
  };

  return {
    entityTypes,
    templateId,
    selectedEntityType,
    saveEntityType,
    onChangeEntityType,
  };
};

export default useProfileChooserState;
