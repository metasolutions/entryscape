import { entrystore } from 'commons/store';
import { CONTEXT_TYPE_MODELS } from 'commons/util/context';

/**
 * Get all model entries
 *
 * @returns {Promise<Entry[]>}
 */
export const getModelEntries = () => {
  return entrystore
    .newSolrQuery()
    .rdfType(CONTEXT_TYPE_MODELS)
    .list()
    .getEntries();
};
