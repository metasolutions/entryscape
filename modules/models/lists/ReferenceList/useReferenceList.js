import { entrystore } from 'commons/store';
import { useCallback } from 'react';
import { useSolrQuery } from 'commons/components/EntryListView';

const useReferenceList = ({ rdfType, entry, limit }) => {
  const createQuery = useCallback(() => {
    const query = entrystore.newSolrQuery().objectUri(entry.getResourceURI());
    if (rdfType) {
      query.rdfType(rdfType);
    }
    if (limit) {
      query.limit(limit);
    }
    return query;
  }, [rdfType, entry, limit]);

  const queryResults = useSolrQuery({ createQuery });

  return queryResults;
};

export default useReferenceList;
