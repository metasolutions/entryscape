import PropTypes from 'prop-types';
import {
  useTranslation,
  nlsBundlesPropType,
} from 'commons/hooks/useTranslation';
import {
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  LinearProgress,
} from '@mui/material';
import {
  Check as DoneIcon,
  SlowMotionVideo as IdleIcon,
  Autorenew as InProgressIcon,
  HighlightOff as FailedIcon,
} from '@mui/icons-material';
import {
  STATUS_IDLE,
  STATUS_PROGRESS,
  STATUS_DONE,
  STATUS_FAILED,
} from 'commons/hooks/useTasksList';
import escoProgressNLS from 'commons/nls/escoProgress.nls';
import { Spin } from 'commons/components/common/Spin';
import './ProgressList.scss';

const getDoneTasksPercentage = (tasks) => {
  const doneTasks = tasks.filter(({ status }) => status === STATUS_DONE).length;
  return (doneTasks / tasks.length) * 100;
};

const statusToIcon = (status) => {
  switch (status) {
    case STATUS_IDLE:
      return <IdleIcon />; // TODO better icon needed
    case STATUS_PROGRESS:
      return (
        <Spin>
          <InProgressIcon />
        </Spin>
      );
    case STATUS_DONE:
      return <DoneIcon color="primary" />;
    case STATUS_FAILED:
      return <FailedIcon color="error" />;
    default:
  }
};

const ProgressList = ({ tasks = [], nlsBundles = [] }) => {
  const localizationBundles = [escoProgressNLS, ...nlsBundles];
  const hasFailed = tasks.some((task) => task.status === STATUS_FAILED);
  const hasSucceeded = tasks.every(({ status }) => status === STATUS_DONE);
  const t = useTranslation(localizationBundles);

  const getLinearBarClasses = () => {
    if (hasSucceeded) {
      return { bar: 'escoProgressList__linearBar--success' };
    }

    if (hasFailed) {
      return { bar: 'escoProgressList__linearBar--error' };
    }

    return null;
  };

  return (
    <>
      <LinearProgress
        variant="determinate"
        value={getDoneTasksPercentage(tasks)}
        classes={getLinearBarClasses()}
      />
      <List component="nav" aria-label={t('progressListNavAriaLabel')}>
        {tasks.map((task) => (
          <ListItem key={task.id}>
            <ListItemIcon>{statusToIcon(task.status)}</ListItemIcon>
            <ListItemText
              primary={t(task.nlsKeyLabel)}
              secondary={task.message}
              secondaryTypographyProps={{
                'aria-live': 'polite',
              }}
            />
          </ListItem>
        ))}
      </List>
    </>
  );
};

ProgressList.propTypes = {
  tasks: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string,
      nlsKeyLabel: PropTypes.string,
      status: PropTypes.string,
      message: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
    })
  ),
  nlsBundles: nlsBundlesPropType,
};

export default ProgressList;
