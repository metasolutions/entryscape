import Typography from '@mui/material/Typography';
import PropTypes from 'prop-types';

const FormSection = ({ title, mandatory, children }) => (
  <div className="escaVisualizationFormSection">
    {title ? (
      <Typography className="escaVisualizationFormSection__heading">
        {title}
        {mandatory ? (
          <span className="escaVisualizationForm__fieldMarker" aria-hidden>
            *
          </span>
        ) : (
          ''
        )}
      </Typography>
    ) : null}
    {children}
  </div>
);
FormSection.propTypes = {
  title: PropTypes.string,
  mandatory: PropTypes.bool,
  children: PropTypes.node,
};

export default FormSection;
