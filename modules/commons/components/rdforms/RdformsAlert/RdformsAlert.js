import { useEffect, useRef } from 'react';
import { Alert } from '@mui/material';
import PropTypes from 'prop-types';
import './RdformsAlert.scss';

const RdformsAlert = ({
  message,
  setMessage,
  props: alertProps = { severity: 'error' },
}) => {
  const alertWrapperEl = useRef();

  useEffect(() => {
    if (message && alertWrapperEl.current) {
      alertWrapperEl.current.scrollIntoView({
        behavior: 'smooth',
      });
    }
  }, [message]);

  return message ? (
    <div className="escoRdformsAlert" ref={alertWrapperEl}>
      <Alert
        className="escoRdformsAlert__alert"
        onClose={setMessage ? () => setMessage(null) : null}
        {...alertProps}
      >
        {message}
      </Alert>
    </div>
  ) : null;
};

RdformsAlert.propTypes = {
  message: PropTypes.string,
  setMessage: PropTypes.func,
  props: PropTypes.shape({}),
};

export default RdformsAlert;
