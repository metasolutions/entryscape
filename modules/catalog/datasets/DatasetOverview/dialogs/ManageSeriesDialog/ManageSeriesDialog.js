import PropTypes from 'prop-types';
import { useCallback, useState } from 'react';
import {
  withListModelProvider,
  ListItemCheckbox,
} from 'commons/components/ListView';
import {
  EntryListView,
  useSolrQuery,
  MODIFIED_COLUMN,
  TITLE_COLUMN,
  INFO_COLUMN,
  DRAFT_STATUS_COLUMN,
} from 'commons/components/EntryListView';
import { LIST_ACTION_INFO } from 'commons/components/EntryListView/actions';
import { useConfirmCloseAction } from 'commons/components/EntryListView/dialogs/create/handlers';
import { entrystore } from 'commons/store';
import { spreadEntry } from 'commons/util/store';
// eslint-disable-next-line max-len
import DatasetLinkedDataBrowserDialog from 'catalog/datasetTemplates/DatasetLinkedDataBrowserDialog';
import CreateDatasetSeriesDialog from 'catalog/datasets/dialogs/CreateDatasetSeriesDialog';
import ListActionDialog from 'commons/components/common/dialogs/ListActionDialog';
import ContentWrapper from 'commons/components/common/ContentWrapper';
import { RDF_TYPE_DATASET_SERIES } from 'commons/util/entry';
import { useTranslation } from 'commons/hooks/useTranslation';
import escaDatasetNLS from 'catalog/nls/escaDataset.nls';
import escoListNLS from 'commons/nls/escoList.nls';
import escoDialogsNLS from 'commons/nls/escoDialogs.nls';
import useErrorHandler from 'commons/errors/hooks/useErrorHandler';
import { useESContext } from 'commons/hooks/useESContext';
import { useEntry } from 'commons/hooks/useEntry';
import useAsync, { PENDING } from 'commons/hooks/useAsync';
import { useSnackbar } from 'commons/hooks/useSnackbar';
import LoadingButton from 'commons/components/LoadingButton';
import {
  getUrisOfSeriesIncludingDataset,
  createProtoSeriesFromDatasetCallback,
} from './util';

const NLS_BUNDLES = [escaDatasetNLS, escoListNLS, escoDialogsNLS];

const listActions = [
  {
    id: 'create',
    Dialog: CreateDatasetSeriesDialog,
    labelNlsKey: 'createSeriesButtonLabel',
    tooltipNlsKey: 'createSeriesButtonTooltip',
  },
];

const ManageSeriesDialog = ({
  closeDialog,
  refreshSeries: refreshOverviewSeries,
}) => {
  const translate = useTranslation(NLS_BUNDLES);
  const { context } = useESContext();
  const datasetEntry = useEntry();
  const initialSeriesIncludingDataset =
    getUrisOfSeriesIncludingDataset(datasetEntry);
  const [selectedSeries, setSelectedSeries] = useState(
    initialSeriesIncludingDataset
  );
  const {
    runAsync: runSaveChanges,
    status: saveStatus,
    error: saveError,
  } = useAsync();
  const [addSnackbar] = useSnackbar();
  useErrorHandler(saveError);
  const confirmClose = useConfirmCloseAction(closeDialog);
  const edited = !(
    initialSeriesIncludingDataset.every((uri) =>
      selectedSeries.includes(uri)
    ) && selectedSeries.length === initialSeriesIncludingDataset.length
  );

  const createQuery = useCallback(
    () =>
      entrystore
        .newSolrQuery()
        .rdfType(RDF_TYPE_DATASET_SERIES)
        .context(context),
    [context]
  );
  const queryResults = useSolrQuery({ createQuery });

  const getCheckboxProps = ({ entry: seriesEntry }) => {
    const seriesResourceURI = seriesEntry.getResourceURI();
    const inSelectedSeries = selectedSeries.includes(seriesResourceURI);

    return {
      value: seriesResourceURI,
      checked: inSelectedSeries,
      onChange: ({ target }) => {
        const { value, checked } = target;
        const newSelection = checked
          ? [...selectedSeries, seriesResourceURI]
          : selectedSeries.filter((series) => series !== value);
        setSelectedSeries(newSelection);
      },
    };
  };

  const handleSaveChanges = () => {
    const saveChangesAndClose = async () => {
      const { metadata: datasetMetadata, ruri: datasetResourceURI } =
        spreadEntry(datasetEntry);
      datasetMetadata.findAndRemove(datasetResourceURI, 'dcat:inSeries');
      for (const seriesResourceURI of selectedSeries) {
        datasetMetadata.add(
          datasetResourceURI,
          'dcat:inSeries',
          seriesResourceURI
        );
      }
      await datasetEntry.commitMetadata();
      refreshOverviewSeries();
      closeDialog();
      addSnackbar({ message: translate('seriesChangeSuccess') });
    };

    runSaveChanges(saveChangesAndClose());
  };

  const onCreate = async (seriesEntry) => {
    // is called in create dataset dialog and create entry by template (without a param)
    if (!seriesEntry) return;

    datasetEntry
      .getMetadata()
      .add(
        datasetEntry.getResourceURI(),
        'dcat:inSeries',
        seriesEntry.getResourceURI()
      );
    await datasetEntry.commitMetadata();
  };

  const afterCreateEntry = () => {
    refreshOverviewSeries();
    closeDialog();
  };

  const actions = (
    <LoadingButton
      loading={saveStatus === PENDING}
      onClick={handleSaveChanges}
      disabled={!edited}
    >
      {translate('save')}
    </LoadingButton>
  );

  const prototypeCallback = useCallback(
    (prototypeEntry) =>
      createProtoSeriesFromDatasetCallback(datasetEntry, prototypeEntry),
    [datasetEntry]
  );

  return (
    <ListActionDialog
      id="add-to-series-dialog"
      title={translate('manageSeriesDialogTitle')}
      actions={actions}
      closeDialog={() => confirmClose(edited)}
      closeDialogButtonLabel={translate('cancel')}
      maxWidth="md"
      fixedHeight
    >
      <ContentWrapper md={12}>
        <EntryListView
          {...queryResults}
          nlsBundles={NLS_BUNDLES}
          listActions={listActions}
          listActionsProps={{
            onCreate,
            afterCreateEntry,
            actionParams: {
              prototypeCallback,
            },
          }}
          viewPlaceholderProps={{
            label: translate('seriesEmptyListPlaceholder'),
          }}
          columns={[
            DRAFT_STATUS_COLUMN,
            {
              ...TITLE_COLUMN,
              xs: 6,
              headerNlsKey: 'listHeaderDatasetSeries',
            },
            MODIFIED_COLUMN,
            {
              ...INFO_COLUMN,
              getProps: ({ entry, translate: t }) => ({
                action: {
                  ...LIST_ACTION_INFO,
                  entry,
                  nlsBundles: NLS_BUNDLES,
                  Dialog: DatasetLinkedDataBrowserDialog,
                },
                title: t('infoEntry'),
              }),
            },
            {
              id: 'manage-series-column',
              Component: ListItemCheckbox,
              getProps: getCheckboxProps,
              xs: 2,
              headerNlsKey: 'inSeriesColumnHeader',
            },
          ]}
        />
      </ContentWrapper>
    </ListActionDialog>
  );
};

ManageSeriesDialog.propTypes = {
  closeDialog: PropTypes.func,
  refreshSeries: PropTypes.func,
};

export default withListModelProvider(ManageSeriesDialog);
