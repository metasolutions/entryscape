import {
  ACTION_EDIT,
  ACTION_REMOVE,
  ACTION_REVISIONS,
} from 'commons/components/overview/actions';

const sidebarActions = [ACTION_EDIT, ACTION_REVISIONS, ACTION_REMOVE];

export { sidebarActions };
