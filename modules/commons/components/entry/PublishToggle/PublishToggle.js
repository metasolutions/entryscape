import { useState } from 'react';
import PropTypes from 'prop-types';
import {
  useTranslation,
  nlsBundlesPropType,
} from 'commons/hooks/useTranslation';
import { Public, PublicOff } from '@mui/icons-material';
import escoOverviewNLS from 'commons/nls/escoOverview.nls';
import useAsyncCallback from 'commons/hooks/useAsyncCallback';
import { getGroupWithHomeContext } from 'commons/util/store';
import { useEntry } from 'commons/hooks/useEntry';
import { OverviewSidebarToggle } from 'commons/components/overview';
import { useSnackbar } from 'commons/hooks/useSnackbar';
import { isDraftEntry as checkIsDraftEntry } from 'commons/util/entry';
import './PublishToggle.scss';

const defaultOnToggle = (entry, isPublic) => {
  const entryInfo = entry.getEntryInfo();
  const acl = entryInfo.getACL(true);

  return getGroupWithHomeContext(entry.getContext()).then((groupEntry) => {
    if (isPublic) {
      acl.admin = acl.admin || [];
      acl.admin.push(groupEntry.getId());
      entryInfo.setACL(acl);
    } else {
      entryInfo.setACL({});
    }
    return entryInfo.commit();
  });
};

const PublishToggle = ({
  nlsBundles = [],
  labelNlsKey = 'publishedTitle',
  disabledTooltipNlsKey = 'privateDisabledTitle',
  draftDisabledTooltipNlsKey = 'draftDisabledTooltipNlsKey',
  publishedStatusNlsKey = 'publishedStatus',
  unpublishedStatusNlsKey = 'unpublishedStatus',
  publishFailNlsKey = 'publishFail',
  unpublishFailNlsKey = 'unpublishFail',
  onToggle = defaultOnToggle,
}) => {
  const entry = useEntry();
  const translate = useTranslation([escoOverviewNLS, ...nlsBundles]);
  const [contextIsPublic] = useAsyncCallback(
    (entityEntry) =>
      entityEntry
        .getContext()
        .getEntry()
        .then((contextEntry) => contextEntry.isPublic()),
    entry,
    false
  );
  const [isPublic, setIsPublic] = useState(entry.isPublic());
  const [addSnackbar] = useSnackbar();
  const isDraftEntry = checkIsDraftEntry(entry);

  const onChange = () =>
    Promise.resolve(onToggle(entry, isPublic))
      .then(() => setIsPublic(!isPublic))
      .catch(() => {
        addSnackbar({
          message: translate(
            isPublic ? unpublishFailNlsKey : publishFailNlsKey
          ),
        });
      });

  const getSwitchTooltip = () => {
    if (isDraftEntry) return translate(draftDisabledTooltipNlsKey);
    if (!contextIsPublic) return translate(disabledTooltipNlsKey);
    return null;
  };

  return (
    <OverviewSidebarToggle
      onChange={onChange}
      disabled={!contextIsPublic || isDraftEntry}
      isChecked={contextIsPublic && isPublic}
      primaryLabel={translate(labelNlsKey)}
      secondaryLabel={
        entry.isPublic()
          ? translate(publishedStatusNlsKey)
          : translate(unpublishedStatusNlsKey)
      }
      icon={entry.isPublic() ? <Public /> : <PublicOff />}
      switchTooltipLabel={getSwitchTooltip()}
    />
  );
};

PublishToggle.propTypes = {
  nlsBundles: nlsBundlesPropType,
  labelNlsKey: PropTypes.string,
  disabledTooltipNlsKey: PropTypes.string,
  draftDisabledTooltipNlsKey: PropTypes.string,
  publishedStatusNlsKey: PropTypes.string,
  unpublishedStatusNlsKey: PropTypes.string,
  onToggle: PropTypes.func,
  publishFailNlsKey: PropTypes.string,
  unpublishFailNlsKey: PropTypes.string,
};

export default PublishToggle;
