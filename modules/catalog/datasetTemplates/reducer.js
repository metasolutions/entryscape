import config from 'config';

const RESET = 'LIST_RESET';
const CHANGE_CATEGORY = 'CHANGE_CATEGORY';
const CHANGE_CATALOG = 'CHANGE_CATALOG';

const getInitialState = () => {
  return {
    category: '',
    context: config.get('catalog.datasetTemplateDefaultCatalog'),
  };
};

const reducer = (state, action) => {
  switch (action.type) {
    case CHANGE_CATEGORY:
      return { ...state, category: action.value, page: 0 };
    case CHANGE_CATALOG:
      return { ...state, context: action.value, category: '', page: 0 };
    case RESET:
      return {
        ...getInitialState(),
      };
    default:
      return state;
  }
};

export { getInitialState, reducer, RESET, CHANGE_CATEGORY, CHANGE_CATALOG };
