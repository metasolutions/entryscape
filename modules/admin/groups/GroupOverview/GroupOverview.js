import { useEntry } from 'commons/hooks/useEntry';
import { getRenderName } from 'commons/util/rdfUtils';
import { getViewDefFromName } from 'commons/util/site';
import esadGroupNLS from 'admin/nls/esadGroup.nls';
import escoListNLS from 'commons/nls/escoList.nls';
import { useTranslation } from 'commons/hooks/useTranslation';
import useDocumentTitle from 'commons/hooks/useDocumentTitle';
import usePageTitle from 'commons/hooks/usePageTitle';
import useGetContributors from 'commons/hooks/useGetContributors';
import escoOverviewNLS from 'commons/nls/escoOverview.nls';
import {
  ACTION_INFO_WITH_ICON,
  DESCRIPTION_UPDATED,
} from 'commons/components/overview/actions';
import Overview from 'commons/components/overview/Overview';
import {
  withOverviewModelProvider,
  overviewPropsPropType,
  useOverviewModel,
} from 'commons/components/overview';
import LinkedDataBrowserDialog from 'commons/components/LinkedDataBrowserDialog';
import { localize } from 'commons/locale';
import { sidebarActions } from './actions';

const nlsBundles = [esadGroupNLS, escoListNLS, escoOverviewNLS];

const GroupOverview = ({ overviewProps }) => {
  const entry = useEntry();
  const translate = useTranslation(nlsBundles);
  const [{ refreshCount }] = useOverviewModel();
  const { contributors } = useGetContributors(entry, refreshCount);
  const [pageTitle] = usePageTitle();
  const viewDefinition = getViewDefFromName('admin__groups__group');
  const viewDefinitionTitle = localize(viewDefinition.title);
  const label =
    getRenderName(entry) || translate('unnamedWorkspace', entry.getId());

  const descriptionItems = [
    DESCRIPTION_UPDATED,
    {
      id: 'edited',
      labelNlsKey: 'editedByLabel',
      getValues: () => contributors,
    },
  ];

  useDocumentTitle(
    `${pageTitle} - ${viewDefinitionTitle} ${getRenderName(entry)}`
  );

  return (
    <Overview
      {...overviewProps}
      backLabel={translate('backTitle')}
      headerAction={{
        ...ACTION_INFO_WITH_ICON,
        Dialog: LinkedDataBrowserDialog,
        formTemplateId: 'esc:Group',
      }}
      entry={entry}
      nlsBundles={nlsBundles}
      descriptionItems={descriptionItems}
      sidebarActions={sidebarActions}
      returnTo="/admin/groups"
      headerLabel={label}
    />
  );
};

GroupOverview.propTypes = {
  overviewProps: overviewPropsPropType,
};

export default withOverviewModelProvider(GroupOverview);
