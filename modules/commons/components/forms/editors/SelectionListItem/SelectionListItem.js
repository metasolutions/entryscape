import { useState, Fragment } from 'react';
import PropTypes from 'prop-types';
import {
  List,
  ListItemAction,
  ListItemText,
} from 'commons/components/ListView';
import { Button, IconButton, Collapse, Grid, ListItem } from '@mui/material';
import {
  ChevronRight as ShowIcon,
  ExpandMore as HideIcon,
} from '@mui/icons-material';
import { getLabel } from 'commons/util/rdfUtils';
import Tooltip from 'commons/components/common/Tooltip';

export const SelectionListItem = ({
  translate,
  selection,
  label,
  onClick,
  disabled,
}) => {
  const [expand, setExpand] = useState(true);

  return (
    <>
      <ListItem divider sx={{ paddingLeft: 0, paddingRight: 0 }}>
        <Grid container alignItems="center">
          <Grid item xs={1}>
            <Tooltip
              title={
                expand ? translate('hideTooltip') : translate('showTooltip')
              }
            >
              <IconButton onClick={() => setExpand(!expand)}>
                {expand ? <HideIcon /> : <ShowIcon />}
              </IconButton>
            </Tooltip>
          </Grid>
          <ListItemText xs={5} primary={label} />
          <ListItemText xs={2} secondary={selection.length} />
          <ListItemAction xs={4} justifyContent="end" alignItems="center">
            <Button
              variant="text"
              disabled={disabled}
              onClick={onClick}
              sx={{ padding: 0 }}
            >
              {translate('editSelectionButton')}
            </Button>
          </ListItemAction>
        </Grid>
      </ListItem>
      <ListItem sx={{ padding: 0 }}>
        <Collapse
          in={expand}
          timeout="auto"
          unmountOnExit
          sx={{ width: '100%' }}
        >
          <List>
            <ListItem sx={{ paddingLeft: 0, paddingRight: 0 }}>
              <Grid container>
                {selection?.map((selectedEntry) => (
                  <Fragment key={selectedEntry.id || selectedEntry.getId()}>
                    <ListItemText xs={1} secondary="" />
                    <ListItemText
                      xs={11}
                      secondary={selectedEntry.label || getLabel(selectedEntry)}
                    />
                  </Fragment>
                ))}
              </Grid>
            </ListItem>
          </List>
        </Collapse>
      </ListItem>
    </>
  );
};

SelectionListItem.propTypes = {
  label: PropTypes.string,
  disabled: PropTypes.bool,
  onClick: PropTypes.func,
  selection: PropTypes.arrayOf(PropTypes.shape({})),
  translate: PropTypes.func,
};
