import { BaseField } from './BaseField';
import { BLANK } from '../../utils/nodetypes';

export class Blank extends BaseField {
  constructor({
    graph,
    resourceURI,
    property,
    blankNodeId,
    rdfType,
    validate,
    mandatory,
  }) {
    super({
      graph,
      resourceURI,
      validate,
      property,
      nodetype: BLANK,
      mandatory,
    });
    const [statement] = blankNodeId
      ? graph.find(resourceURI, property, { type: 'bnode', value: blankNodeId })
      : [graph.create(resourceURI, property, null, false)];
    this._statement = statement;
    const [rdfTypeStatement] = graph.find(
      statement.getValue(),
      'rdf:type',
      rdfType
    );
    if (rdfTypeStatement) {
      this._rdfTypeStatement = rdfTypeStatement;
    }
    if (!rdfTypeStatement && rdfType) {
      this._rdfTypeStatement = graph.create(
        statement.getValue(),
        'rdf:type',
        rdfType,
        false
      );
    }
  }

  getBlankNodeId() {
    return this._statement.getValue();
  }

  clear() {
    this.setAsserted(false);
  }

  remove() {
    if (this._rdfTypeStatement) {
      this._graph.remove(this._rdfTypeStatement);
    }
    if (this._statement) {
      this._graph.remove(this._statement);
    }
  }

  setAsserted(value) {
    this._statement.setAsserted(value);
    if (this._rdfTypeStatement) {
      this._rdfTypeStatement.setAsserted(value);
    }
  }

  isAsserted() {
    return this._statement.isAsserted() && this._rdfTypeStatement.isAsserted();
  }
}
