import { ValidationPresenter } from '@entryscape/rdforms/renderers/react';
import { useState, useEffect } from 'react';

const useRDFormsValidationPresenter = (
  graph,
  resource,
  template,
  compact = false
) => {
  const [presenter, setPresenter] = useState(null);

  useEffect(() => {
    if (template && graph && resource) {
      setPresenter(
        new ValidationPresenter({
          graph,
          template,
          resource,
          compact,
        })
      );
    }
  }, [template, graph, resource]);

  return {
    PresenterComponent: presenter?.domNode.component,
    presenter,
  };
};

export default useRDFormsValidationPresenter;
