import { createContext } from 'commons/util/context';
import { shouldUseUriPattern } from 'commons/util/store';

/**
 *
 * Creates the necessary entries for a new project
 *  - STEP 1: create group and context
 *  - STEP 2: add additional metadata on the context entry
 *  - STEP 3: administrative step, set context type and ACL fix
 *
 */
export const createProject = ({
  title,
  description,
  name: alias,
  entityType,
  projectType,
  hasAdminRights,
}) =>
  createContext({
    title,
    description,
    alias: shouldUseUriPattern(entityType) ? alias : null,
    entityType,
    projectType,
    contextType: 'esterms:WorkbenchContext',
    hasAdminRights,
  });

// const createEntityTypeEntry = (newEntry, graph) => {
//   newEntry.setMetadata(graph);
//   return createEntry(newEntry).catch((err) => {
//     if (typeof err === 'number' && err === -1) {
//       this.fileOrLink.validateLinkPattern({ valid: false, empty: true });
//       const eswoBench = i18n.getLocalization(eswoBenchNLS);
//       throw Error(eswoBench.createDialogMissingLink);
//     }
//     throw Error(err);
//   });
// };
