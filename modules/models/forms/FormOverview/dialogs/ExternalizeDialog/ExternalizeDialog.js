import PropTypes from 'prop-types';
import useAsync, { PENDING } from 'commons/hooks/useAsync';
import { useTranslation } from 'commons/hooks/useTranslation';
import { useEffect, useMemo } from 'react';
import esmoCommonsNLS from 'models/nls/esmoCommons.nls';
import escoDialogsNLS from 'commons/nls/escoDialogs.nls';
import { entryPropType, graphPropType } from 'commons/util/entry';
import {
  createExternalizedEntry,
  externalizeOverriddenValues,
} from 'models/forms/utils/util';
import ListActionDialog from 'commons/components/common/dialogs/ListActionDialog';
import ContentWrapper from 'commons/components/common/ContentWrapper';
import { ErrorCatcher } from 'commons/errors/ErrorCatcher';
import LoadingButton from 'commons/components/LoadingButton';
import {
  EditorProvider,
  Form,
  useEditorContext,
} from 'commons/components/forms/editors';
import Alert from 'commons/components/Alert';
import useGetMainDialog from 'commons/hooks/useGetMainDialog';
import { FIELD_TITLE } from 'models/utils/fieldDefinitions';
import { isForm } from 'models/forms/utils/forms';

const NLS_BUNDLES = [esmoCommonsNLS, escoDialogsNLS];

const FIELDS = [FIELD_TITLE];

const withExernalizeWrapper = (Dialog) => {
  const ExternalizeDialogWrapper = (props) => {
    const translate = useTranslation(NLS_BUNDLES);
    const { getConfirmationDialog } = useGetMainDialog();
    const { formEntry, formItemEntry, graph, closeDialog, hasChanges, onSave } =
      props;
    const { runAsync, isLoading } = useAsync();
    const prototypeEntry = useMemo(() => {
      return createExternalizedEntry(
        formEntry.getContext(),
        graph,
        formItemEntry
      );
    }, [formEntry, formItemEntry, graph]);

    useEffect(() => {
      const confirmSaveChanges = async () => {
        if (!hasChanges) return;
        const proceed = await getConfirmationDialog({
          content: translate('unsavedFormChanges'),
          rejectLabel: translate('cancel'),
          affirmLabel: translate('proceed'),
        });
        if (!proceed) {
          closeDialog();
          return;
        }
        await formEntry.setMetadata(graph).commitMetadata();
        onSave();
      };
      runAsync(confirmSaveChanges());
    }, [
      runAsync,
      closeDialog,
      getConfirmationDialog,
      translate,
      formEntry,
      graph,
      hasChanges,
      onSave,
    ]);

    if (isLoading) return null;

    return (
      <EditorProvider entry={prototypeEntry}>
        <Dialog {...props} />
      </EditorProvider>
    );
  };

  ExternalizeDialogWrapper.propTypes = {
    formEntry: entryPropType,
    formItemEntry: entryPropType,
    graph: graphPropType,
    closeDialog: PropTypes.func,
    hasChanges: PropTypes.bool,
    onSave: PropTypes.func,
  };

  return ExternalizeDialogWrapper;
};

const ExternalizeDialog = ({
  formEntry,
  formItemEntry,
  graph: formGraph,
  closeDialog,
  onSave,
}) => {
  const translate = useTranslation(NLS_BUNDLES);
  const { runAsync, status, error } = useAsync();
  const { graph: externalizedGraph, entry: prototypeEntry } =
    useEditorContext();

  const handleExternalize = () => {
    const externalize = async () => {
      const externalizedEntry = await externalizeOverriddenValues(
        formGraph,
        formItemEntry,
        externalizedGraph,
        prototypeEntry
      );
      onSave(externalizedEntry);
      await formEntry.setMetadata(formGraph).commitMetadata();
      closeDialog();
    };
    runAsync(externalize());
  };

  const actions = (
    <LoadingButton onClick={handleExternalize} loading={status === PENDING}>
      {translate('proceed')}
    </LoadingButton>
  );

  const warningNls = isForm(prototypeEntry)
    ? 'externalizeFormWarning'
    : 'externalizeFieldWarning';

  return (
    <>
      <ListActionDialog
        id="externalize-dialog"
        title="Externalize"
        maxWidth="md"
        closeDialog={closeDialog}
        actions={actions}
      >
        <ContentWrapper>
          <Form fields={FIELDS} nlsBundles={NLS_BUNDLES} />
          <Alert severity="info">{translate(warningNls)}</Alert>
        </ContentWrapper>
      </ListActionDialog>
      <ErrorCatcher error={error} />
    </>
  );
};

ExternalizeDialog.propTypes = {
  closeDialog: PropTypes.func,
  formEntry: entryPropType,
  formItemEntry: entryPropType,
  graph: graphPropType,
  onSave: PropTypes.func,
};

export default withExernalizeWrapper(ExternalizeDialog);
