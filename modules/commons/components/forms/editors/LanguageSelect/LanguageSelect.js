import { useMemo } from 'react';
import PropTypes from 'prop-types';
import { renderingContext, utils } from '@entryscape/rdforms';
import { Select } from '../Select';

const { cloneArrayWithLabels } = utils;
const DIVIDER = {
  label: '─────',
  value: '_none',
};

/**
 *
 * @returns {Object[]}
 */
const getPrimaryLanguages = () => {
  return cloneArrayWithLabels(renderingContext.getPrimaryLanguageList());
};

/**
 *
 * @returns {Object[]}
 */
const getNonPrimaryLanguages = () => {
  return cloneArrayWithLabels(renderingContext.getNonPrimaryLanguageList());
};

/**
 *
 * @returns {Object[]}
 */
export const getLanguages = () => {
  return [...getPrimaryLanguages(), ...getNonPrimaryLanguages()];
};

/**
 *
 * @returns {Object[]}
 */
export const getLanguagesWithDivider = () => {
  const primaryLanguages = getPrimaryLanguages();
  const divider = primaryLanguages.length
    ? [
        {
          ...DIVIDER,
        },
      ]
    : [];
  return [...primaryLanguages, ...divider, ...getNonPrimaryLanguages()];
};

export const LanguageSelect = ({
  xs = 4,
  labelId,
  language,
  updateLanguage,
  helperText,
  size,
}) => {
  const languages = useMemo(() => {
    return getLanguagesWithDivider();
  }, []);

  return (
    <Select
      xs={xs}
      labelId={labelId}
      value={language}
      choices={languages}
      onChange={updateLanguage}
      size={size}
      helperText={helperText}
    />
  );
};

LanguageSelect.propTypes = {
  xs: PropTypes.number,
  labelId: PropTypes.string,
  language: PropTypes.string,
  updateLanguage: PropTypes.func,
  size: PropTypes.string,
  helperText: PropTypes.string,
};

export default LanguageSelect;
