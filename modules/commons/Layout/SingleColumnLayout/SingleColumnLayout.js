import PropTypes from 'prop-types';
import './SingleColumnLayout.scss';

export const SingleColumnLayout = ({ children }) => {
  return (
    <main className="escoSingleColumnLayout__container">
      <div className="escoSingleColumnLayout__content">{children}</div>
    </main>
  );
};

SingleColumnLayout.propTypes = {
  children: PropTypes.node,
};
