import * as ns from 'models/utils/ns';
import RfsField from './RfsField';

const INLINE_FIELD_PROPERTIES = [
  ns.RDF_PROPERTY_CONSTRAINT,
  ns.RDF_PROPERTY_PATTERN,
  ns.RDF_PROPERTY_INLINE,
];

const INLINE_FIELD_NODETYPES = [ns.RDF_PROPERTY_URI];

class RfsInlineField extends RfsField {
  constructor(props) {
    super({
      ...props,
      rdfType: ns.RDF_TYPE_INLINE_FIELD,
      nlsKeyLabel: 'inlineFieldLabel',
      properties: INLINE_FIELD_PROPERTIES,
      compatibleTypes: [],
      nodetypes: INLINE_FIELD_NODETYPES,
    });
  }
}

export default RfsInlineField;
