import { useEffect } from 'react';
import PropTypes from 'prop-types';
import { Entry } from '@entryscape/entrystore-js';
import useAsync from 'commons/hooks/useAsync';
import { useSnackbar } from 'commons/hooks/useSnackbar';
import { useTranslation } from 'commons/hooks/useTranslation';
import esadUserNLS from 'admin/nls/esadUser.nls';
import { useListModel, RERENDER } from 'commons/components/ListView';
import { upgradeUserToPremium } from 'commons/util/user';

const UpgradeUserToPremiumDialog = ({ entry: userEntry }) => {
  const translate = useTranslation(esadUserNLS);
  const [, dispatch] = useListModel();
  const { runAsync } = useAsync(null);
  const [addSnackbar] = useSnackbar();

  useEffect(() => {
    runAsync(
      upgradeUserToPremium(userEntry).then(() => {
        dispatch({ type: RERENDER });
        addSnackbar({ message: translate('upgradeToPremiumNotification') });
      })
    );
  }, [addSnackbar, dispatch, runAsync, translate, userEntry]);

  return null;
};

UpgradeUserToPremiumDialog.propTypes = {
  entry: PropTypes.instanceOf(Entry),
};

export default UpgradeUserToPremiumDialog;
