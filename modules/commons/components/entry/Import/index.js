import FileUpload from 'commons/components/common/FileUpload';
import { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import RDFormsFieldWrapper from 'commons/components/rdforms/FieldWrapper';
import esteTerminologyNLS from 'terms/nls/esteTerminology.nls';
import escoEntryTypeNLS from 'commons/nls/escoEntryType.nls';
import { useTranslation } from 'commons/hooks/useTranslation';
import { Tab, Tabs } from 'commons/components/common/Tabs';
import LinkField from './LinkField';
import UniqueLinkField from './UniqueLinkField';
import './index.scss';

// TODO move to a Tabs component?
const TabPanel = ({ children, value, index }) => {
  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`tabpanel-${index}`}
      aria-labelledby={`tab-${index}`}
    >
      {value === index && <div id={`tab-${index}`}>{children}</div>}
    </div>
  );
};

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
};

// TODO rename
const EntryImport = ({
  mimicsRdformsStyle = true,
  disableLink = false,
  disableFile = false,
  link,
  file,
  handleFileChange,
  handleLinkChange,
  validURI = true,
  handleTabChange: switchTab,
  uniqueURI = false, // true for access to uniqueURI context provider
  fileUploadProps,
}) => {
  const [value, setValue] = useState(0);
  const [pristine, setPristine] = useState(true);
  const [helperText, setHelperText] = useState('');

  const t = useTranslation([escoEntryTypeNLS, esteTerminologyNLS]);
  const inputLabel = t('webAddressLabel');

  const handleTabChange = (_event, newValue) => {
    setValue(newValue);
    switchTab(newValue);
  };

  const handleLinkInputChange = (event) => {
    setPristine(false);
    handleLinkChange(event);
  };

  useEffect(() => {
    if (!pristine) {
      const validLinkText = validURI ? '' : t('linkNotValid');
      const text = link.length ? validLinkText : t('linkRequired');
      setHelperText(text);
    }
  }, [pristine, link.length, t, validURI]);

  if (disableLink) {
    return (
      <>
        {mimicsRdformsStyle ? (
          <RDFormsFieldWrapper label={t('fileLabel')} editor>
            <FileUpload file={file} onSelectFile={handleFileChange} />
          </RDFormsFieldWrapper>
        ) : (
          <FileUpload file={file} onSelectFile={handleFileChange} />
        )}
      </>
    );
  }
  return (
    <>
      <Tabs
        value={value}
        onChange={handleTabChange}
        aria-label={t('importEntryTabsAriaLabel')}
        className="escoEntryImportTabs"
      >
        <Tab label={t('linkLabel')} key="link" disabled={disableLink} />
        <Tab label={t('fileLabel')} key="file-upload" disabled={disableFile} />
      </Tabs>
      <TabPanel value={value} index={0}>
        {uniqueURI ? (
          <UniqueLinkField
            mimicsRdformsStyle={mimicsRdformsStyle}
            inputLabel={inputLabel}
            link={link}
            handleLinkInputChange={handleLinkInputChange}
            hasError={!pristine && !validURI}
            helperText={helperText}
            placeholder={t('linkPlaceholder')}
          />
        ) : (
          <LinkField
            mimicsRdformsStyle={mimicsRdformsStyle}
            inputLabel={inputLabel}
            link={link}
            handleLinkInputChange={handleLinkInputChange}
            hasError={!pristine && !validURI}
            helperText={helperText}
            placeholder={t('linkPlaceholder')}
          />
        )}
      </TabPanel>
      <TabPanel value={value} index={1}>
        {mimicsRdformsStyle ? (
          <RDFormsFieldWrapper label={t('fileLabel')} editor>
            <FileUpload
              file={file}
              onSelectFile={handleFileChange}
              {...fileUploadProps}
            />
          </RDFormsFieldWrapper>
        ) : (
          <FileUpload
            file={file}
            onSelectFile={handleFileChange}
            {...fileUploadProps}
          />
        )}
      </TabPanel>
    </>
  );
};

EntryImport.propTypes = {
  mimicsRdformsStyle: PropTypes.bool,
  disableLink: PropTypes.bool,
  disableFile: PropTypes.bool,
  link: PropTypes.string,
  file: PropTypes.instanceOf(Element),
  handleFileChange: PropTypes.func,
  handleLinkChange: PropTypes.func,
  validURI: PropTypes.bool,
  handleTabChange: PropTypes.func,
  uniqueURI: PropTypes.bool,
  fileUploadProps: PropTypes.shape({ helperText: PropTypes.string }),
};

EntryImport.defaultProps = {
  file: null,
};

export default EntryImport;
