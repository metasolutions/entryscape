import { entrystore } from 'commons/store';
import {
  STATUS_PROGRESS,
  STATUS_DONE,
  STATUS_FAILED,
} from 'commons/hooks/useTasksList';
import { promiseUtil } from '@entryscape/entrystore-js';
import { Graph, utils } from '@entryscape/rdfjson';
import { setGroupIdForContext } from 'commons/util/context';
import { fixURIs, fixSymmetry } from '../utils/skos';

/**
 *
 * @param {Graph} graph
 * @param {string} conceptSchemeURI
 * @param {boolean} external
 * @param {object} userInfo
 * @returns {Promise.<object>} creationParams
 */
const createContext = (graph, conceptSchemeURI, external, userInfo) => {
  const creationParams = { graph, conceptSchemeURI, external };

  return entrystore.createGroupAndContext().then((groupEntry) => {
    creationParams.groupEntry = groupEntry;
    creationParams.homeContextId = groupEntry
      .getResource(true)
      .getHomeContext();

    if (!userInfo.hasAdminRights) {
      setGroupIdForContext(creationParams.homeContextId, groupEntry.getId());
    }

    // Fix new context, both type and correct ACL.
    return entrystore
      .getEntry(
        entrystore.getEntryURI('_contexts', creationParams.homeContextId)
      )
      .then((contextEntry) => {
        const hcEntryInfo = contextEntry.getEntryInfo();
        hcEntryInfo
          .getGraph()
          .add(
            contextEntry.getResourceURI(),
            'rdf:type',
            'esterms:TerminologyContext'
          );
        if (external) {
          hcEntryInfo
            .getGraph()
            .add(
              contextEntry.getResourceURI(),
              'rdf:type',
              'esterms:EnhancedTerminologyContext'
            );
        }
        /* TODO remove when entrystore is changed so groups have
         read access to homecontext metadata by default.
         Start fix with missing metadata rights on context for group */
        const acl = hcEntryInfo.getACL(true);
        acl.mread.push(groupEntry.getId());
        hcEntryInfo.setACL(acl);
        // End fix
        return hcEntryInfo.commit().then(() => creationParams);
      });
  });
};

/**
 *
 * @param {object} params
 * @returns {Promise.<object>}
 */
const importConceptScheme = (params) => {
  const { graph, external, conceptSchemeURI } = params;
  const context = entrystore.getContextById(params.homeContextId);
  let prototypeEntry;

  if (params.anythingReplaced && params.newURIs[conceptSchemeURI]) {
    prototypeEntry = context.newNamedEntry(
      params.newURIs[conceptSchemeURI].newId
    );
    prototypeEntry.setMetadata(
      utils.extract(graph, new Graph(), conceptSchemeURI)
    );
  } else if (external) {
    prototypeEntry = context.newLinkRef(conceptSchemeURI, conceptSchemeURI);
    prototypeEntry.setCachedExternalMetadata(
      utils.extract(graph, new Graph(), conceptSchemeURI)
    );
  } else {
    prototypeEntry = context.newLink(conceptSchemeURI);
    prototypeEntry.setMetadata(
      utils.extract(graph, new Graph(), conceptSchemeURI)
    );
  }

  return prototypeEntry.commit().then((contextSchemeEntry) => {
    params.csEntry = contextSchemeEntry;
    return params;
  });
};

/**
 *
 * @param {object} params
 * @param {Function} updateProgress
 * @returns {Promise}
 */
const importConcepts = (params, updateProgress) => {
  const context = params.csEntry.getContext();
  const stmts = params.graph.find(null, 'rdf:type', 'skos:Concept');
  const updateProgressPerConcept = updateProgress(stmts.length);

  return promiseUtil
    .forEach(stmts, (stmt) => {
      const uri = stmt.getSubject();
      let prototypeEntry;
      if (params.anythingReplaced && params.newURIs[uri]) {
        prototypeEntry = context.newNamedEntry(params.newURIs[uri].newId);
        prototypeEntry.setMetadata(
          utils.extract(params.graph, new Graph(), uri)
        );
      } else if (params.external) {
        prototypeEntry = context.newLinkRef(uri, uri);
        prototypeEntry.setCachedExternalMetadata(
          utils.extract(params.graph, new Graph(), uri)
        );
      } else {
        prototypeEntry = context.newLink(uri);
        prototypeEntry.setMetadata(
          utils.extract(params.graph, new Graph(), uri)
        );
      }

      return (
        prototypeEntry
          .commit()
          // update UI progress with number of concepts imported
          .then(updateProgressPerConcept)
      );
    })
    .then(() => params);
};

const importData = (
  { graph, conceptSchemeURI },
  userInfo,
  updateProgress,
  external
) => {
  return createContext(graph, conceptSchemeURI, external, userInfo)
    .then(fixURIs)
    .then(fixSymmetry)
    .then(importConceptScheme)
    .then((params) => importConcepts(params, updateProgress));
};

const skosImport =
  ({ t, external, updateTask, userInfo }) =>
  async (data) => {
    updateTask('import', { status: STATUS_PROGRESS });

    let result;
    try {
      let importedConcepts = 0;
      // make sure to upate the progress dialog while in a promiseUtil.forEach loop
      const updateProgress = (totalConcepts) => () => {
        importedConcepts += 1;
        updateTask('import', {
          message: t('nlsNumberOfConceptsImported', {
            importedConcepts,
            totalConcepts,
          }),
        });
      };

      result = await importData(data, userInfo, updateProgress, external);
    } catch (err) {
      updateTask('import', { status: STATUS_FAILED, message: t(err.message) });
      return;
    }
    updateTask('import', { status: STATUS_DONE });

    // the newly created concept scheme entry
    return result.csEntry;
  };

export default skosImport;
