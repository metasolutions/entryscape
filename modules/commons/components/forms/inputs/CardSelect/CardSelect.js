import PropTypes from 'prop-types';
import {
  Card,
  CardHeader,
  FormControl,
  FormHelperText,
  FormLabel,
  Grid,
  Radio,
  Typography,
} from '@mui/material';
import { entryPropType } from 'commons/util/entry';
import './CardSelect.scss';

const CardSelectItem = ({ onChange, choice, selectedValue }) => {
  const { label, value, description, disabled } = choice;
  const isSelected = value === selectedValue;

  return (
    <Card
      key={`card-${value}`}
      classes={{
        root: isSelected
          ? 'escoCardSelect__card escoCardSelect__card--selected'
          : 'escoCardSelect__card',
      }}
      style={disabled ? { opacity: 0.38 } : { cursor: 'pointer' }}
      onClick={() => !disabled && onChange(value)}
    >
      <CardHeader
        avatar={
          <Radio
            checked={isSelected}
            disabled={disabled}
            onChange={({ target }) => onChange(target.value)}
            value={value}
            name="card-radio-buttons"
            inputProps={{
              'aria-label': label,
            }}
          />
        }
        disableTypography
        title={
          <Typography variant="h4" gutterBottom={Boolean(description)}>
            {label}
          </Typography>
        }
        subheader={
          description ? (
            <Typography variant="body2">{description}</Typography>
          ) : null
        }
      />
    </Card>
  );
};

CardSelectItem.propTypes = {
  choice: PropTypes.shape({
    value: PropTypes.oneOfType([
      PropTypes.number,
      PropTypes.string,
      entryPropType,
    ]),
    label: PropTypes.string,
    description: PropTypes.string,
    disabled: PropTypes.bool,
  }),
  onChange: PropTypes.func,
  selectedValue: PropTypes.string,
};

export const CardSelect = ({
  xs = 12,
  helperText,
  label,
  labelId = 'card-select',
  choices,
  selectedValue,
  onChange,
}) => {
  return (
    <Grid item xs={xs}>
      <FormControl fullWidth>
        <FormLabel id={labelId}>{label}</FormLabel>
        {choices.map((choice) => (
          <CardSelectItem
            key={choice.value}
            onChange={onChange}
            selectedValue={selectedValue}
            choice={choice}
          />
        ))}
        {helperText ? <FormHelperText>{helperText}</FormHelperText> : null}
      </FormControl>
    </Grid>
  );
};

CardSelect.propTypes = {
  choices: PropTypes.arrayOf(
    PropTypes.shape({
      value: PropTypes.oneOfType([
        PropTypes.number,
        PropTypes.string,
        entryPropType,
      ]),
      label: PropTypes.string,
      description: PropTypes.string,
    })
  ),
  label: PropTypes.string,
  labelId: PropTypes.string,
  onChange: PropTypes.func,
  selectedValue: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  xs: PropTypes.number,
  helperText: PropTypes.string,
};

export default CardSelect;
