import { i18n } from 'esi18n';
import config from 'config';
import { useCallback, useState } from 'react';
import usePrimaryLanguageCodes from 'commons/hooks/usePrimaryLanguageCodes';

const useLanguageControls = () => {
  const currentLang = i18n.getLocale() || config.get('locale.fallback');
  const languageOptions = config.get('itemstore.languages');
  const [language, setLanguage] = useState(currentLang);
  const primaryLanguageCodes = usePrimaryLanguageCodes();
  const [allLanguagesEnabled, setAllLanguagesEnabled] = useState(false);

  const toggleAllLanguages = useCallback(
    () => setAllLanguagesEnabled((enabled) => !enabled),
    []
  );

  return {
    language,
    changeLanguage: setLanguage,
    toggleAllLanguages,
    allLanguagesEnabled,
    languageOptions,
    primaryLanguageCodes,
  };
};

export default useLanguageControls;
