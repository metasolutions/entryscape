import { useEntry } from 'commons/hooks/useEntry';
import { getLabel } from 'commons/util/rdfUtils';
import { getViewDefFromName } from 'commons/util/site';
import escoListNLS from 'commons/nls/escoList.nls';
import esmoClassesNLS from 'models/nls/esmoClasses.nls';
import esmoCommonsNLS from 'models/nls/esmoCommons.nls';
import { useTranslation } from 'commons/hooks/useTranslation';
import useDocumentTitle from 'commons/hooks/useDocumentTitle';
import usePageTitle from 'commons/hooks/usePageTitle';
import useGetContributors from 'commons/hooks/useGetContributors';
import escoOverviewNLS from 'commons/nls/escoOverview.nls';
import { i18n } from 'esi18n';
import {
  ACTION_INFO_WITH_ICON,
  DESCRIPTION_UPDATED,
} from 'commons/components/overview/actions';
import Overview from 'commons/components/overview/Overview';
import {
  withOverviewModelProvider,
  overviewPropsPropType,
  useOverviewModel,
} from 'commons/components/overview';
import LinkedDataBrowserDialog from 'commons/components/LinkedDataBrowserDialog';
import { sidebarActions } from './actions';

const nlsBundles = [
  esmoClassesNLS,
  escoListNLS,
  escoOverviewNLS,
  esmoCommonsNLS,
];

const ClassOverview = ({ overviewProps }) => {
  const entry = useEntry();
  const t = useTranslation(nlsBundles);
  const [{ refreshCount }] = useOverviewModel();
  const { contributors } = useGetContributors(entry, refreshCount);
  const [pageTitle] = usePageTitle();
  const viewDefinition = getViewDefFromName('models__classes__class');
  const viewDefinitionTitle = viewDefinition.title[i18n.getLocale()];

  const descriptionItems = [
    DESCRIPTION_UPDATED,
    {
      id: 'edited',
      labelNlsKey: 'editedByLabel',
      getValues: () => contributors,
    },
  ];

  useDocumentTitle(`${pageTitle} - ${viewDefinitionTitle} ${getLabel(entry)}`);

  return (
    <Overview
      {...overviewProps}
      backLabel={t('backTitle')}
      headerAction={{
        ...ACTION_INFO_WITH_ICON,
        Dialog: LinkedDataBrowserDialog,
      }}
      entry={entry}
      nlsBundles={nlsBundles}
      descriptionItems={descriptionItems}
      sidebarActions={sidebarActions}
    />
  );
};

ClassOverview.propTypes = {
  overviewProps: overviewPropsPropType,
};

export default withOverviewModelProvider(ClassOverview);
