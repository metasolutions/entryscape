import React, { useState, useEffect } from 'react';
import { Link, useLocation } from 'react-router-dom';
import useNavigate from 'commons/components/router/useNavigate';
import {
  Visibility as VisibilityIcon,
  VisibilityOff as VisibilityOffIcon,
} from '@mui/icons-material';
import {
  Link as MuiLink,
  Grid,
  TextField,
  InputAdornment,
  IconButton,
  Container,
  Typography,
  Paper,
  Divider,
} from '@mui/material';
import Tooltip from 'commons/components/common/Tooltip';
import Alert from '@mui/material/Alert';
import config from 'config';
import useDelayedCallback from 'commons/hooks/useDelayedCallback';
import { isEmpty } from 'lodash-es';
import { login } from 'commons/util/user';
import { USER_ENTRY_ID_GUEST } from 'commons/util/userIds';
import { AUTH_LOGIN_SUCCESS } from 'commons/hooks/user/actions';
import { useUserDispatch, useUserState } from 'commons/hooks/useUser';
import escoSigninNLS from 'commons/nls/escoSignin.nls';
import { useTranslation } from 'commons/hooks/useTranslation';
import { useLanguage } from 'commons/hooks/useLanguage';
import Logo from 'commons/nav/components/Logo';
import { getPathFromViewName } from 'commons/util/site';
import LanguageSelect from 'commons/components/common/LanguageSelect';
import { entrystore } from 'commons/store';
import SSOButton from 'commons/auth/components/SSOButton';
import LoadingButton from 'commons/components/LoadingButton';
import { addIgnore, UNAUTHORIZED } from 'commons/errors/utils/async';
import ResetPasswordDialog from './ResetPasswordDialog';
import SecondStageLoginDialog from './SecondStageLoginDialog/SecondStageLoginDialog';
import './index.scss';

const SignIn = () => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState(''); // set gets callback as 2nd arg, could be used for validation func
  const [errors, setErrors] = useState({});
  const [pristine, setPristine] = useState({
    username: true,
    password: true,
  });
  const [loading, setLoading] = useState(false);
  const [success, setSuccess] = useState(false);
  const [showPassword, setShowPassword] = useState(false);
  const [showSecondStageLogin, setShowSecondStageLogin] = useState(false);
  const [, setLanguage] = useLanguage();
  const { userInfo } = useUserState();
  const dispatch = useUserDispatch();
  const location = useLocation();
  const t = useTranslation(escoSigninNLS);
  const [showResetPassword, setShowResetPassword] = useState(false);
  const ssoEnabled = config.get('entrystore.sso.enabled');
  const [hasSsoFocus, setHasSsoBtnFocus] = useState(true);
  const { navigate } = useNavigate();
  const storeUrl = entrystore.getBaseURI();
  const baseUrl =
    config.get('baseUrl') || storeUrl.substring(0, storeUrl.indexOf('/store'));
  let ssoUrl = `${storeUrl}auth/${config.get('entrystore.sso.path') || 'saml'}`;

  if (config.get('entrystore.sso.path') === 'cas') {
    const successURL = encodeURIComponent(
      `${baseUrl}${getPathFromViewName('start')}`
    );
    const failureURL = encodeURIComponent(
      `${baseUrl}${getPathFromViewName('signin')}`
    );
    ssoUrl = `${ssoUrl}?redirectOnSuccess=${successURL}&redirectOnFailure=${failureURL}`;
  }

  // redirect to the referrer view or the start view if the user is not _guest
  useEffect(() => {
    const pageLanguage = userInfo.language || config.get('locale.fallback');
    if (userInfo.entryId !== USER_ENTRY_ID_GUEST) {
      setLanguage(pageLanguage);
      navigate(location.state?.referrer || getPathFromViewName('start'), {
        from: config.get('site.signinView'),
      });
    }
  }, [
    navigate,
    location.state,
    setLanguage,
    userInfo.entryId,
    userInfo.language,
  ]);

  const handleSSOClick = () => {
    if (
      config.get('entrystore.sso.path') === 'saml' &&
      config.get('entrystore.sso.multi')
    ) {
      setShowSecondStageLogin(true);
    } else {
      window.location.href = ssoUrl;
    }
  };

  const validate = (values) => {
    const validationErrors = {};

    if (!values.username) {
      validationErrors.username = 'usernameRequired';
    }

    if (!values.password) {
      validationErrors.password = 'passwordRequired';
    }

    return validationErrors;
  };

  const checkErrors = () => validate({ username, password });

  useEffect(() => {
    const currentErrors = checkErrors();
    const errorsToSet = {
      username:
        pristine.username && !errors.username
          ? undefined
          : currentErrors.username,
      password:
        pristine.password && !errors.password
          ? undefined
          : currentErrors.password,
    };
    setErrors(errorsToSet);
  }, [username, password, pristine]);

  const delayedCallback = useDelayedCallback();

  const handleOnFocus = () => {
    if (hasSsoFocus) setHasSsoBtnFocus(false);
  };

  const handleOnBlur = (event) => {
    const { target, relatedTarget, currentTarget } = event;
    if (!target.name) return;

    const delay = !currentTarget.contains(relatedTarget) ? 300 : 0;
    const setPristineDelayed = delayedCallback(setPristine, delay);
    setPristineDelayed({
      ...pristine,
      [target.name]: false,
    });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    const currentErrors = checkErrors();
    if (!isEmpty(currentErrors)) {
      setErrors(currentErrors);
      return;
    }

    setLoading(true);
    addIgnore('login', UNAUTHORIZED, true);

    try {
      const { userEntry, userInfo: userInfoLogin } = await login(
        username.trim(),
        password.trim()
      );
      dispatch({
        type: AUTH_LOGIN_SUCCESS,
        data: { userInfo: userInfoLogin, userEntry },
      });
      setSuccess(true);
    } catch (err) {
      if (err.response.status === 401) {
        setErrors({ response: 'signinUnauthorized' });
      } else if (err.response.status === 403) {
        setErrors({ response: 'userStatusDisabled' });
      } else if (err.response.status === 429) {
        setErrors({ response: 'signinWrongTooManyTimes' });
      } else {
        setErrors({ response: 'signinError' });
      }
      setSuccess(false);
    }
    setLoading(false);
  };

  const translateError = (nlsErrorKey) => (nlsErrorKey ? t(nlsErrorKey) : '');

  return (
    <Container
      component="main"
      classes={{ root: 'escoSignin__container' }}
      maxWidth="sm"
    >
      <Grid container spacing={2}>
        <Grid item xs={12} sm={6} />
        <Grid item xs={12} sm={6}>
          <LanguageSelect />
        </Grid>
      </Grid>
      <Paper elevation={3} classes={{ root: 'escoSignin__formContainer' }}>
        <div className="escoSignin__block escoSignin__logoContainer">
          <Link to="/">
            <Logo type="full" className="escoSignin__logo" />
          </Link>
        </div>
        <form className="escoSignin__form" noValidate onSubmit={handleSubmit}>
          {errors.response && (
            <div className="escoSignin__block">
              <Alert className="escoSignin__message" severity="error">
                {t(errors.response)}
              </Alert>
            </div>
          )}
          {ssoEnabled && (
            <>
              <div className="escoSignin__block">
                <SSOButton
                  autofocus={ssoEnabled}
                  color={hasSsoFocus ? 'primary' : 'secondary'}
                  onClick={handleSSOClick}
                >
                  {config.get('entrystore.sso.message') || t('ssoButtonLabel')}
                </SSOButton>
              </div>
              <div className="escoSignin__dividerContainer">
                <Typography classes={{ root: 'escoSignin__dividerText' }}>
                  {t('dividerText')}
                </Typography>
                <Divider classes={{ root: 'escoSignin__divider' }} />
              </div>
            </>
          )}
          {showSecondStageLogin ? (
            <SecondStageLoginDialog
              isOpen={showSecondStageLogin}
              close={() => setShowSecondStageLogin(false)}
              url={ssoUrl}
            />
          ) : null}
          <div className="escoSignin__block" onFocus={handleOnFocus}>
            <div onBlur={handleOnBlur}>
              <TextField
                classes={{ root: 'escoSignin__input' }}
                variant="outlined"
                required
                margin="none"
                id="username"
                onChange={(e) => {
                  setUsername(e.target.value);
                  setPristine({
                    ...pristine,
                    username: false,
                  });
                }}
                InputLabelProps={{ shrink: true }}
                value={username}
                label={t('username')}
                name="username"
                autoComplete="email"
                autoFocus={!ssoEnabled}
                error={!!errors.username}
                helperText={translateError(errors.username)}
                FormHelperTextProps={{ 'aria-live': 'polite' }}
              />
              <TextField
                classes={{ root: 'escoSignin__input' }}
                variant="outlined"
                required
                margin="none"
                onChange={(event) => {
                  setPassword(event.target.value);
                  setPristine({
                    ...pristine,
                    password: false,
                  });
                }}
                value={password}
                name="password"
                label={t('password')}
                type={showPassword ? 'text' : 'password'}
                id="password"
                autoComplete="current-password"
                error={!!errors.password}
                helperText={translateError(errors.password)}
                FormHelperTextProps={{ 'aria-live': 'polite' }}
                InputLabelProps={{ shrink: true }}
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="end">
                      <Tooltip title={t('passwordVisibility')}>
                        <IconButton
                          aria-label={t('passwordVisibility')}
                          onClick={() => setShowPassword(!showPassword)}
                          onMouseDown={(e) => e.preventDefault()}
                        >
                          {showPassword ? (
                            <VisibilityIcon />
                          ) : (
                            <VisibilityOffIcon />
                          )}
                        </IconButton>
                      </Tooltip>
                    </InputAdornment>
                  ),
                }}
              />
              <div className="escoSignin__wrapper">
                <LoadingButton
                  onClick={handleSubmit}
                  loading={loading}
                  success={success}
                  type="submit"
                  color={!ssoEnabled || !hasSsoFocus ? 'primary' : 'secondary'}
                >
                  {t('signInButton')}
                </LoadingButton>
              </div>
            </div>
            <Grid container>
              <Grid item xs>
                <MuiLink
                  name="resetPasswordButton"
                  href="#"
                  variant="body2"
                  onClick={(event) => {
                    event.preventDefault();
                    setShowResetPassword(true);
                  }}
                >
                  {t('resetPasswordButton')}
                </MuiLink>
              </Grid>
              {config.get('site.signup') === false ? null : (
                <Grid item>
                  <MuiLink
                    name="signupButton"
                    href="#"
                    component={Link}
                    to="/signup"
                    variant="body2"
                  >
                    {t('signupButton')}
                  </MuiLink>
                </Grid>
              )}
            </Grid>
          </div>
        </form>
      </Paper>
      {showResetPassword ? (
        <ResetPasswordDialog
          isOpen={showResetPassword}
          close={() => setShowResetPassword(false)}
        />
      ) : null}
    </Container>
  );
};

export default SignIn;
