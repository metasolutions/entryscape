import { useEffect } from 'react';
import { parseTemplate } from 'commons/util/reactUtils';
import ReferenceList from 'commons/components/ReferenceList';
import escaReferences from 'catalog/nls/escaReferences.nls';
import { getOverviewName, getTo } from 'commons/util/overview';
import useGetMainDialog from '../useGetMainDialog';
import { useTranslation } from '../useTranslation';
import useGetReferences from './useGetReferences';

const useCheckReferences = ({
  entry,
  nlsBundles = [],
  callback,
  checkCanRemove,
  ignoreCallback = () => false,
}) => {
  const bundles = [escaReferences, ...nlsBundles];
  const translate = useTranslation(bundles);
  const { references, labels, status, entityTypes } = useGetReferences(
    entry,
    translate,
    ignoreCallback
  );
  const { getAcknowledgeDialog } = useGetMainDialog();

  useEffect(() => {
    if (!labels.length) return;
    if (checkCanRemove?.(references, status)) return;

    const buildAckownledgeDialog = async () => {
      const items = await Promise.all(
        labels.map(async (label, index) => {
          const referenceEntry = references[index];
          const entityType = entityTypes[index];
          const overview = getOverviewName(
            entityType,
            referenceEntry.getContext()
          );
          const href = await getTo(entityType, overview, referenceEntry);
          return { label, href };
        })
      );

      getAcknowledgeDialog({
        content: parseTemplate(translate('unableToRemove'), {
          1: <ReferenceList references={items} />,
        }),
      }).then(callback);
    };

    buildAckownledgeDialog().catch(console.error);
  }, [
    callback,
    checkCanRemove,
    getAcknowledgeDialog,
    labels,
    references,
    entityTypes,
    status,
    translate,
  ]);

  return { references, labels, checkReferencesStatus: status };
};

export default useCheckReferences;
