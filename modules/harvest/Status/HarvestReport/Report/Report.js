import PropTypes from 'prop-types';
import { Button, Alert } from '@mui/material';
import Table from 'commons/components/common/Table';
import { useTranslation } from 'commons/hooks/useTranslation';
import esrePipelineResult from 'harvest/nls/esrePipelineResult.nls';
import { mergeResultsToRows } from 'harvest/util/pipelineResult';
import HarvestReportCard from '../HarvestReportCard';
import '../HarvestReport.scss';

const Report = ({ body, showValidationButton, showValidationReport }) => {
  const translate = useTranslation(esrePipelineResult);
  const {
    validationResults = {},
    data = {},
    mergeResults = {},
    mergeResultsByType = {},
  } = body;
  const validationResultsArray = Object.keys(validationResults);
  const dataArray = Object.keys(data);

  const harvestDetailsHeaders = [
    translate('harvestDetailsType'),
    ...Object.keys(mergeResults).map((header) => translate(header)),
  ];
  const harvestDetailsRowsRaw = mergeResultsToRows(mergeResultsByType);

  const harvestDetailsRows = harvestDetailsRowsRaw
    .filter(Boolean)
    .sort(([label1], [label2]) => label1.localeCompare(label2));
  const showAlert = harvestDetailsRowsRaw.length !== harvestDetailsRows.length;

  return (
    <>
      <HarvestReportCard
        title={translate('harvestingResult')}
        data={dataArray.map((item) => ({
          label: translate(item),
          value: translate(body.data[item]),
        }))}
      />
      {validationResultsArray.length > 0 && (
        <HarvestReportCard
          key="validation-results"
          title={translate('validationResult')}
          data={validationResultsArray.map((item) => ({
            label: translate(item),
            value: body.validationResults[item],
          }))}
        />
      )}
      {showValidationButton ? (
        <Button
          className="esreHarvestReportCard__Button"
          variant="text"
          onClick={() => showValidationReport()}
        >
          {translate('validationReport')}
        </Button>
      ) : null}

      <div className="esreHarvestReportCardBlock">
        <div className="esreHarvestReportCardBlock__Header">
          {translate('harvestDetailsHeader')}
        </div>

        {showAlert ? (
          <Alert severity="error" className="esreHarvestReportAlert">
            {translate('missingTypesWarning')}
          </Alert>
        ) : null}

        {harvestDetailsRows.length ? (
          <Table headers={harvestDetailsHeaders} rows={harvestDetailsRows} />
        ) : null}
      </div>
    </>
  );
};

Report.propTypes = {
  body: PropTypes.shape({
    validationResults: PropTypes.shape({}),
    mergeResults: PropTypes.shape({}),
    data: PropTypes.shape({}),
    mergeResultsByType: PropTypes.shape({}),
  }),
  showValidationButton: PropTypes.bool,
  showValidationReport: PropTypes.func,
};

export default Report;
