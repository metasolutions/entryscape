import { Entry } from '@entryscape/entrystore-js';
import { RDF_TYPE_DATASET, RDF_TYPE_DATASET_SERIES } from 'commons/util/entry';
import { spreadEntry } from 'commons/util/store';

/**
 *
 * @param {Entry} datasetEntry
 * @returns {string[]}
 */
export const getUrisOfSeriesIncludingDataset = (datasetEntry) => {
  const { metadata, ruri: resourceURI } = spreadEntry(datasetEntry);
  return metadata
    .find(resourceURI, 'dcat:inSeries')
    .map((statement) => statement.getValue());
};

/**
 * Generates a function that produces a prototype entry connected to a base entry,
 * for more details check createProtoSeriesFromDatasetCallback
 *
 * @param {string} baseEntryRdfType
 * @param {string} prototypeEntryRdfType
 * @returns {Function}
 */
const makeProtoEntryFromEntryCallback =
  (baseEntryRdfType, prototypeEntryRdfType) => (baseEntry, prototypeEntry) => {
    const protoResourceURI = prototypeEntry.getResourceURI();

    const newProtoTypeMetadata = baseEntry
      .getMetadata()
      .clone()
      .replaceURI(baseEntry.getResourceURI(), protoResourceURI);
    newProtoTypeMetadata.findAndRemove(protoResourceURI, 'dcat:distribution');
    newProtoTypeMetadata.findAndRemove(protoResourceURI, 'schema:diagram');
    newProtoTypeMetadata.findAndRemove(protoResourceURI, 'dcat:inSeries');
    newProtoTypeMetadata.findAndRemove(protoResourceURI, 'esterms:basedOn');
    newProtoTypeMetadata.findAndRemove(
      protoResourceURI,
      'rdf:type',
      baseEntryRdfType
    );
    newProtoTypeMetadata.add(
      protoResourceURI,
      'rdf:type',
      prototypeEntryRdfType
    );

    prototypeEntry.setMetadata(newProtoTypeMetadata);

    return prototypeEntry;
  };

/**
 * Same process as createProtoSeriesFromDatasetCallback, applied in reverse
 *
 * @param {Entry} seriesEntry
 * @param {Entry} datasetPrototypeEntry
 * @returns {Entry}
 */
export const createProtoDatasetFromSeriesCallback =
  makeProtoEntryFromEntryCallback(RDF_TYPE_DATASET_SERIES, RDF_TYPE_DATASET);

/**
 * Modifies the dataset series prototype entry based on the original dataset entry:
 * 1. copies all metadata
 * 2. removes the distributions and visualizations metadata statements
 * 3. replaces the rdf type
 *
 * @param {Entry} datasetEntry
 * @param {Entry} seriesPrototypeEntry
 * @returns {Entry}
 */
export const createProtoSeriesFromDatasetCallback =
  makeProtoEntryFromEntryCallback(RDF_TYPE_DATASET, RDF_TYPE_DATASET_SERIES);
