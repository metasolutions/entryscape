import { targetLanguageField, titleField } from '../utils/fieldDefinitions';

export const fields = [titleField, targetLanguageField];
