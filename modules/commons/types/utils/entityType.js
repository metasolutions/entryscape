import Lookup from 'commons/types/Lookup';
import { Entry } from '@entryscape/entrystore-js';
import { EntityType } from 'entitytype-lookup';
import registry from 'commons/registry';
import { findModuleNameFromContext } from 'commons/util/module';
import { contextTypes } from 'commons/util/context';
import config from 'config';
import { filterOnRestrictions } from './restrictions';

/**
 *
 * @returns {object[]}
 */
export const getBuiltinEntityTypeConfigs = () => {
  const builtinEntityTypeConfigs = registry.getApplicationConfig().entitytypes;
  return builtinEntityTypeConfigs;
};

/**
 * @returns {string[]}
 */
export const getBuiltinEntityTypeNames = () => {
  return getBuiltinEntityTypeConfigs().map(({ name }) => name);
};

/**
 *
 * @returns {object[]}
 */
export const getCustomEntityTypeConfigs = () => {
  const builtinEntityTypeNames = getBuiltinEntityTypeNames();
  const entityTypes = config.get('entitytypes');
  return entityTypes.filter((entityType) => {
    return !builtinEntityTypeNames.includes(entityType.name);
  });
};

/**
 * Check if entity type is included in active modules.
 *
 * @param {object[]} entityType
 * @param {string[]} validModuleNames
 * @returns {object[]}
 */
export const checkEntityTypeInModules = (entityType, validModuleNames) => {
  const typeModule = entityType.module;
  if (!typeModule) return true;
  const moduleNames = Array.isArray(typeModule) ? typeModule : [typeModule];
  const isValidModule = Boolean(
    moduleNames.find((moduleName) => validModuleNames.includes(moduleName))
  );
  return isValidModule;
};

/**
 *
 * @param {EntityType} entityType
 * @returns {string}
 */
export const getOverviewName = (entityType) => {
  const { overview, refines } = entityType.get();
  if (!refines) return overview;

  const refinedEntityType = Lookup.getByName(refines);
  return refinedEntityType?.get('overview');
};

/**
 *
 * @param {EntityType[]} entityTypes
 * @returns {EntityType[]}
 */
export const getEntityTypesIncludingRefined = (entityTypes) =>
  entityTypes.reduce((types, entityType) => {
    const refinedTypeName = entityType.get('refines');
    if (!refinedTypeName) return [...types, entityType];

    const refinedType = Lookup.getByName(refinedTypeName);
    if (!refinedType || entityTypes.includes(refinedType)) return types;

    return [...types, entityType, refinedType];
  }, []);

/**
 * @param {object} userInfo
 * @param {string} module
 * @returns {EntityType[]}
 */
export const getBuiltinEntityTypes = (userInfo, module) => {
  const builtinTypeNames = getBuiltinEntityTypeNames();
  const availableEntityTypes = Lookup.getEntityTypes();
  const builtinEntityTypes = availableEntityTypes.filter((entityType) =>
    builtinTypeNames.includes(entityType.getName())
  );

  return filterOnRestrictions(builtinEntityTypes, userInfo, module, true);
};

/**
 * TODO: move this into entity lookup lib
 *
 * @param {Entry} contextEntry
 * @returns {Promise<EntityType>}
 */
export const findEntityTypeForContext = async (contextEntry) => {
  const resource = await contextEntry.getResource();
  const moduleName = await findModuleNameFromContext(resource);
  const { rdfType } = contextTypes.find(
    (contextType) => contextType.module === moduleName
  );
  await contextEntry.getContext().getEntry(); // make sure it's in the cache
  return Lookup.getEntityTypeByRdfType(rdfType, moduleName);
};

/**
 * Check whether drafts are enabled on an entity type. If not explicitly set on
 * the type itself look for the property on the base entity type (via refines).
 *
 * @param {EntityType} entityType
 * @returns {boolean}
 */
export const getDraftsEnabled = (entityType) => {
  if (!entityType) return false;

  const draftsEnabled = entityType.get('draftsEnabled');
  if (draftsEnabled === undefined) {
    const baseEntityTypeName = entityType.get('refines');
    if (!baseEntityTypeName) return false;

    const baseEntityType = Lookup.getByName(baseEntityTypeName);
    return Boolean(baseEntityType?.get('draftsEnabled'));
  }

  return draftsEnabled;
};
