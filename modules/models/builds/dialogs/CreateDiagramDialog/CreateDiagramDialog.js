import PropTypes from 'prop-types';
import { useMemo, useState } from 'react';
import ListActionDialog from 'commons/components/common/dialogs/ListActionDialog';
import {
  useTranslation,
  nlsBundlesPropType,
} from 'commons/hooks/useTranslation';
import escoListNLS from 'commons/nls/escoList.nls';
import esmoBuildsNLS from 'models/nls/esmoBuilds.nls';
import esmoCommonsNLS from 'models/nls/esmoCommons.nls';
import escoDialogsNLS from 'commons/nls/escoDialogs.nls';
import ContentWrapper from 'commons/components/common/ContentWrapper';
import { Button } from '@mui/material';
import {
  EditorProvider,
  FieldEditor,
  Select,
  Fields,
  Field,
  RadioGroup,
  useEditorContext,
} from 'commons/components/forms/editors';
import { useESContext } from 'commons/hooks/useESContext';
import useGetDiagramOptions from 'models/diagrams/hooks/useGetDiagramOptions';
import Diagram from 'models/diagrams/Diagram';
import { setBackgroundColor } from 'commons/util/svg';
import { CREATE, useListModel } from 'commons/components/ListView';
import { useSnackbar } from 'commons/hooks/useSnackbar';
import { ErrorCatcher } from 'commons/errors/ErrorCatcher';
import { ErrorWithMessage } from 'commons/errors/utils/errors';
import { useConfirmCloseAction } from 'commons/components/EntryListView/dialogs/create/handlers';
import { fields } from './actions';
import { createPngResource, createSvgResource } from './utils/diagram';

const NLS_BUNDLES = [
  esmoBuildsNLS,
  esmoCommonsNLS,
  escoListNLS,
  escoDialogsNLS,
];
const PNG = 'png';
const SVG = 'svg';
const FORMAT_CHOICES = [
  { label: PNG, value: PNG },
  { label: SVG, value: SVG },
];

const CreateDiagramDialog = ({ closeDialog }) => {
  const translate = useTranslation(NLS_BUNDLES);
  const { context } = useESContext();
  const [, dispatch] = useListModel();
  const [createError, setCreateError] = useState();
  const { options, isLoading, selected, setSelected, getOption } =
    useGetDiagramOptions(context);
  const [format, setFormat] = useState(PNG);
  const { entry: prototypeEntry, graph, canSubmit } = useEditorContext();
  const [addSnackbar] = useSnackbar();
  const confirmClose = useConfirmCloseAction(closeDialog);

  const handleCreateResource = async () => {
    const svgElement = document.querySelector('.esmoDiagram__canvas svg');
    setBackgroundColor(svgElement);
    const createResource =
      format === PNG ? createPngResource : createSvgResource;

    try {
      await createResource(svgElement, prototypeEntry, graph);
      dispatch({ type: CREATE });
      addSnackbar({ message: translate('createDiagramSuccess') });
    } catch (error) {
      setCreateError(
        new ErrorWithMessage(translate('createResourceFail'), error)
      );
    }
    closeDialog();
  };

  const actions = (
    <Button onClick={handleCreateResource} disabled={!canSubmit}>
      {translate('createButtonLabel')}
    </Button>
  );

  return (
    <>
      <ListActionDialog
        id="create-diagram-dialog"
        title={translate('createDiagramHeader')}
        actions={actions}
        closeDialog={() => confirmClose(graph.isChanged())}
        fixedHeight
      >
        <ContentWrapper>
          <form noValidate autoComplete="off">
            {fields.map(({ labelNlsKey, ...fieldProps }) => (
              <FieldEditor
                key={fieldProps.property}
                label={translate(labelNlsKey)}
                size="small"
                {...fieldProps}
              />
            ))}
            <Fields max={1} label={translate('formatLabel')} mandatory>
              <Field>
                <RadioGroup
                  labelId="select-format"
                  value={format}
                  choices={FORMAT_CHOICES}
                  onChange={({ target }) => setFormat(target.value)}
                  row
                />
              </Field>
            </Fields>
            <Fields max={1} label={translate('diagramLabel')} mandatory>
              <Field>
                <Select
                  labelId="select-diagram"
                  disabled={isLoading}
                  value={selected}
                  choices={options}
                  onChange={({ target }) => setSelected(target.value)}
                />
              </Field>
            </Fields>
          </form>
          <Diagram diagramEntry={getOption(selected)?.entry || null} />
        </ContentWrapper>
      </ListActionDialog>
      <ErrorCatcher error={createError} />
    </>
  );
};

CreateDiagramDialog.propTypes = {
  closeDialog: PropTypes.func.isRequired,
  nlsBundles: nlsBundlesPropType,
};

const CreateDiagramDialogWithEditorContext = (props) => {
  const { context } = useESContext();

  const prototypeEntry = useMemo(() => {
    return context
      .newEntry()
      .add('rdf:type', 'http://purl.org/dc/dcmitype/Image')
      .add('rdf:type', 'prof:ResourceDescriptor')
      .add('prof:hasRole', 'http://www.w3.org/ns/dx/prof/role/specification');
  }, [context]);

  return (
    <EditorProvider entry={prototypeEntry}>
      <CreateDiagramDialog {...props} />
    </EditorProvider>
  );
};

export default CreateDiagramDialogWithEditorContext;
