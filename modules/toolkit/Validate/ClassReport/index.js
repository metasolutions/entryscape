import PropTypes from 'prop-types';
import { useState } from 'react';
import {
  Alert,
  Button,
  Grid,
  Link,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Typography,
} from '@mui/material';
import Tooltip from 'commons/components/common/Tooltip';
import {
  Warning as ErrorIcon,
  Error as WarningIcon,
  OfflineBolt as DeprecatedIcon,
  CheckCircle as CheckCircleIcon,
} from '@mui/icons-material';
import esreReportNLS from 'toolkit/nls/esreReport.nls';
import { useTranslation } from 'commons/hooks/useTranslation';
import InstanceReportDialog from 'toolkit/Validate/InstanceReportDialog';
import { getLabel } from 'commons/util/rdfUtils';
import { isUri } from 'commons/util/util';
import './index.scss';

const ReportCell = ({ children, ...rest }) => (
  <TableCell classes={{ root: 'esreClassReport__TableCell' }} {...rest}>
    {children}
  </TableCell>
);

ReportCell.propTypes = {
  children: PropTypes.node,
};

const ReportRow = ({ item, icon, problemLabel }) => (
  <TableRow hover classes={{ root: 'esreClassReport__TableRow' }}>
    <ReportCell align="center">{icon}</ReportCell>
    <ReportCell>{item.path || item}</ReportCell>
    <ReportCell>{problemLabel}</ReportCell>
  </TableRow>
);

ReportRow.propTypes = {
  item: PropTypes.oneOfType([
    PropTypes.shape({ path: PropTypes.string, code: PropTypes.string }),
    PropTypes.string,
  ]),
  icon: PropTypes.node,
  problemLabel: PropTypes.string,
};

const ReportTable = ({ children }) => {
  const translate = useTranslation(esreReportNLS);

  return (
    <Table className="esreClassReport__table" size="small">
      <TableHead>
        <TableRow>
          <ReportCell align="center">{translate('severity')}</ReportCell>
          <ReportCell>{translate('path')}</ReportCell>
          <ReportCell>{translate('problem')}</ReportCell>
        </TableRow>
      </TableHead>
      <TableBody>{children}</TableBody>
    </Table>
  );
};

ReportTable.propTypes = {
  children: PropTypes.node,
};

/**
 * Compare function that sorts resources with errors first and warnings second.
 *
 * @param {object} resourceA
 * @param {object} resourceB
 * @returns {number}
 */
const compareByErrors = (resourceA, resourceB) => {
  const { errors: errorsA, warnings: warningsA } = resourceA;
  const { errors: errorsB, warnings: warningsB } = resourceB;

  if (errorsA.length > errorsB.length) return -1;
  if (errorsA.length < errorsB.length) return 1;
  if (warningsA.length > warningsB.length) return -1;
  if (warningsA.length < warningsB.length) return 1;
  return 0;
};

const INSTANCES_LIMIT = 100;

const ClassReport = ({ resources, graph, template, type }) => {
  const [instanceIndex, setInstanceIndex] = useState(null);
  const translate = useTranslation(esreReportNLS);
  resources.sort(compareByErrors);
  const instancesExceedLimit = resources.length > INSTANCES_LIMIT;
  const visibleResources = instancesExceedLimit
    ? resources.slice(0, INSTANCES_LIMIT)
    : resources;

  return (
    <>
      {instancesExceedLimit ? (
        <Alert severity="warning" className="esreClassReportAlert">
          {translate('tooManyInstancesWarning')}
        </Alert>
      ) : null}
      {visibleResources.map(({ uri, errors, warnings, deprecated }, idx) => (
        <div className="esreClassReport" key={uri}>
          <Grid container className="esreClassReportHeader">
            <Grid container item xs={9} xl={10} alignItems="center">
              <Grid item xs={12}>
                <Typography className="esreClassReportHeader__label">
                  {`${getLabel(graph, uri) || translate('untitledLabel', type)}
                    ${
                      errors.length || warnings.length
                        ? translate('reportHead', {
                            errors: errors.length,
                            warnings: warnings.length,
                          })
                        : translate('reportIsValid')
                    }
                      `}
                </Typography>
              </Grid>
              {isUri(uri) ? (
                <Grid item xs={12}>
                  <Typography variant="body2">
                    {`${translate('resourceURILabel')}: `}
                    <Link
                      className="esreClassReportHeader__link"
                      href={uri}
                      target="_blank"
                    >
                      {uri}
                    </Link>
                  </Typography>
                </Grid>
              ) : null}
            </Grid>
            <Grid
              className="esreClassReportHeader__buttonContainer"
              container
              item
              xs={3}
              xl={2}
            >
              <Button
                className="esreClassReportHeader__button"
                onClick={() => setInstanceIndex(idx)}
              >
                {translate('viewValidation')}
              </Button>
            </Grid>
          </Grid>

          {(errors.length > 0 ||
            warnings.length > 0 ||
            deprecated.length > 0) && (
            <ReportTable>
              {errors.map((error, index) => (
                <ReportRow
                  // ! This is a ready-only view, thus using the index as part of the key on the
                  // ! following row lists is acceptable
                  // eslint-disable-next-line react/no-array-index-key
                  key={index}
                  item={error}
                  problemLabel={translate(`report_${error.code}`)}
                  icon={
                    <ErrorIcon
                      titleAccess={translate('error')}
                      className="esreClassReport__ErrorIcon"
                    />
                  }
                />
              ))}
              {warnings.map((warning, index) => (
                <ReportRow
                  // eslint-disable-next-line react/no-array-index-key
                  key={index}
                  item={warning}
                  problemLabel={translate(`report_${warning.code}`)}
                  icon={
                    <WarningIcon
                      titleAccess={translate('warning')}
                      className="esreClassReport__WarningIcon"
                    />
                  }
                />
              ))}
              {deprecated.map((deprecatedValue, index) => (
                <ReportRow
                  // eslint-disable-next-line react/no-array-index-key
                  key={index}
                  item={deprecatedValue}
                  problemLabel={translate('deprecated')}
                  icon={
                    <DeprecatedIcon
                      titleAccess={translate('deprecatedIconTitle')}
                      className="esreClassReport__DeprecatedIcon"
                    />
                  }
                />
              ))}
            </ReportTable>
          )}
        </div>
      ))}
      {instanceIndex !== null ? (
        <InstanceReportDialog
          title={resources[instanceIndex]?.uri}
          open={instanceIndex !== null}
          handleClose={() => {
            setInstanceIndex(null);
          }}
          buttonLabel={translate('closeValidationDialog')}
          graph={graph}
          template={template}
          resource={resources[instanceIndex]?.uri}
        />
      ) : null}
    </>
  );
};

ClassReport.propTypes = {
  resources: PropTypes.arrayOf(
    PropTypes.shape({
      type: PropTypes.string.isRequired,
      uri: PropTypes.string.isRequired,
      template: PropTypes.string.isRequired,
      errors: PropTypes.arrayOf(
        PropTypes.shape({ path: PropTypes.string, code: PropTypes.string })
      ),
      warning: PropTypes.arrayOf(
        PropTypes.shape({ path: PropTypes.string, code: PropTypes.string })
      ),
      deprecated: PropTypes.arrayOf(PropTypes.string),
    })
  ),
  graph: PropTypes.shape({}),
  template: PropTypes.shape({}),
  type: PropTypes.string,
};

const ClassReportHeaderSecondary = ({ resources }) => {
  const translate = useTranslation(esreReportNLS);

  const errors = resources.reduce(
    (sum, resource) => sum + resource.errors.length,
    0
  );

  const warnings = resources.reduce(
    (sum, resource) => sum + resource.warnings.length,
    0
  );

  const deprecated = resources.reduce(
    (sum, resource) => sum + resource.deprecated.length,
    0
  );

  const valid = !errors && !warnings && !deprecated;

  return (
    resources && (
      <div className="esreClassReportHeaderSecondary">
        {errors ? (
          <ClassReportHeaderIndicator
            tooltipLabel={translate('errorTitle', errors)}
            value={errors}
            icon={
              <ErrorIcon className="esreClassReport__ErrorIcon esreClassReportHeaderSecondary__icon" />
            }
          />
        ) : null}
        {warnings ? (
          <ClassReportHeaderIndicator
            tooltipLabel={translate('warningTitle', warnings)}
            value={warnings}
            icon={
              <WarningIcon className="esreClassReport__WarningIcon esreClassReportHeaderSecondary__icon" />
            }
          />
        ) : null}
        {deprecated ? (
          <ClassReportHeaderIndicator
            tooltipLabel={translate('deprecatedTitle', deprecated)}
            value={deprecated}
            icon={
              <DeprecatedIcon className="esreClassReport__DeprecatedIcon esreClassReportHeaderSecondary__icon" />
            }
          />
        ) : null}
        {valid ? (
          <ClassReportHeaderIndicator
            tooltipLabel={translate('validTitle')}
            icon={
              <CheckCircleIcon className="esreClassReport__ValidIcon esreClassReportHeaderSecondary__icon" />
            }
          />
        ) : null}
      </div>
    )
  );
};

ClassReportHeaderSecondary.propTypes = {
  resources: PropTypes.arrayOf(
    PropTypes.shape({
      type: PropTypes.string,
      uri: PropTypes.string,
      template: PropTypes.string,
      errors: PropTypes.arrayOf(
        PropTypes.shape({ path: PropTypes.string, code: PropTypes.string })
      ),
      warning: PropTypes.arrayOf(
        PropTypes.shape({ path: PropTypes.string, code: PropTypes.string })
      ),
      deprecated: PropTypes.arrayOf(PropTypes.string),
    })
  ),
};

const ClassReportHeaderIndicator = ({ tooltipLabel, value = null, icon }) => (
  <Tooltip title={tooltipLabel}>
    <div className="esreClassReportHeaderSecondary__indicator">
      {value}
      {icon}
    </div>
  </Tooltip>
);

ClassReportHeaderIndicator.propTypes = {
  tooltipLabel: PropTypes.string.isRequired,
  value: PropTypes.number,
  icon: PropTypes.node,
};

export { ClassReportHeaderSecondary };
export default ClassReport;
