import PropTypes from 'prop-types';
import React, { useState, useEffect } from 'react';
import { TextField, InputAdornment } from '@mui/material';
import { useESContext } from 'commons/hooks/useESContext';
import { useEntityURI } from 'commons/hooks/useEntityTitleAndURI';
import RDFormsFieldWrapper from 'commons/components/rdforms/FieldWrapper';
import {
  shouldUseUriPattern,
  constructURIFromPattern,
} from 'commons/util/store';
import escoEntryTypeNLS from 'commons/nls/escoEntryType.nls';
import { useTranslation } from 'commons/hooks/useTranslation';

const PatternedLink = ({
  entityType,
  setCreateEntryParams,
  setExtraFieldIsValid,
}) => {
  const { context, contextName } = useESContext();
  const [pristine, setPristine] = useState(true);

  const hasUriPattern = entityType ? shouldUseUriPattern(entityType) : false;
  const uriPattern = (hasUriPattern && entityType.uriPattern) || '';
  const entityTypeUriPatternBase =
    hasUriPattern && constructURIFromPattern(uriPattern, { contextName });

  const { name, updateName, isNameFree } = useEntityURI({
    context,
    contextName,
    uriPattern,
  });

  const nameIsValid = !!name && isNameFree;
  const t = useTranslation(escoEntryTypeNLS);

  const handleUpdateURI = (evt) => {
    const newName = evt.target.value;

    setPristine(false);
    updateName(newName);

    // update parent with the right params to create an entry, i.e `link`
    setCreateEntryParams({
      link: constructURIFromPattern(uriPattern, {
        contextName,
        entryName: newName,
      }),
    });
  };

  useEffect(() => {
    setExtraFieldIsValid(nameIsValid);
  }, [name, isNameFree, setExtraFieldIsValid, nameIsValid]);

  const getHelperText = () => {
    if (!pristine) {
      if (!name) {
        return t('linkRequired');
      }

      if (!isNameFree) {
        return t('linkPatternUnavailableURL');
      }
    }

    return '';
  };

  return (
    <RDFormsFieldWrapper label={t('webAddressLabel')} editor>
      <TextField
        value={name}
        onChange={handleUpdateURI}
        error={!pristine && !nameIsValid}
        helperText={getHelperText()}
        InputProps={{
          startAdornment: (
            <InputAdornment position="start">
              {entityTypeUriPatternBase}
            </InputAdornment>
          ),
        }}
      />
    </RDFormsFieldWrapper>
  );
};

PatternedLink.propTypes = {
  entityType: PropTypes.shape({ uriPattern: PropTypes.string }),
  setCreateEntryParams: PropTypes.func,
  setExtraFieldIsValid: PropTypes.func,
};

export default PatternedLink;
