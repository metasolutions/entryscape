import {
  URI,
  LANGUAGE_LITERAL,
  LITERAL,
  CheckboxesEditor,
} from 'commons/components/forms/editors';
import * as ns from 'models/utils/ns';
import { BuildSelectionEditor } from '../editors/BuildSelectionEditor';
import { DependencyEditor } from '../editors/DependencyEditor';

//  Get target language choices. Currently uses default languages but will be
//  configurable.
const getLanguageChoices = () => {
  const defaultLanguageChoices = [
    {
      value: 'en',
      defaultValue: 'en',
      label: {
        en: 'English',
        sv: 'Engelska',
      },
    },
    {
      value: 'sv',
      defaultValue: 'sv',
      label: {
        en: 'Swedish',
        sv: 'Svenska',
      },
    },
  ];

  return defaultLanguageChoices;
};

export const titleField = {
  nodetype: LANGUAGE_LITERAL,
  property: 'dcterms:title',
  labelNlsKey: 'titleLabel',
  mandatory: true,
};

export const targetLanguageField = {
  nodetype: LITERAL,
  property: ns.RDF_PROPERTY_TARGET_LANGUAGE,
  labelNlsKey: 'targetLanguageLabel',
  choices: getLanguageChoices(),
  mandatory: true,
  max: 1,
  Editor: CheckboxesEditor,
  row: true,
};

export const buildSelectionField = {
  nodetype: URI,
  property: ns.RDF_PROPERTY_BUILD_SELECTION,
  buildSelectionLabel: 'Build selection',
  labelNlsKey: 'buildSelectionLabel',
  mandatory: true,
  Editor: BuildSelectionEditor,
};

export const dependenciesField = {
  nodetype: URI,
  property: 'build:Dependencies',
  labelNlsKey: 'buildDependenciesLabel',
  mandatory: true,
  Editor: DependencyEditor,
};
