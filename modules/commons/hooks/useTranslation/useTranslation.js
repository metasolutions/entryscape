import PropTypes from 'prop-types';
import { useCallback, useRef, useEffect, useState } from 'react';
import { i18n } from 'esi18n';
import { useLanguage } from 'commons/hooks/useLanguage';
import { getLocalizedString, getNlsDefinition } from './util';

/**
 *
 * @param {object[]} nlsBundles
 * @returns {object[]}
 */
const getLocalizationBundles = (nlsBundles) => {
  const bundles =
    Array.isArray(nlsBundles) && Array.isArray(nlsBundles[0])
      ? nlsBundles
      : [nlsBundles];
  return bundles.map((NLS) => {
    return i18n.getLocalization(NLS);
  });
};

/**
 * Provides a utility function to retrieve the value of an nlsKey from an array of nlsBundles
 *
 * @param {object[]} nlsBundles
 * @returns {Function}
 */
export const useTranslation = (nlsBundles) => {
  const [language] = useLanguage();
  const nlsBundlesRef = useRef(nlsBundles);
  const hasLocalizedBundle = useRef(false);
  const [localizedBundles, setLocalizedBundles] = useState(
    getLocalizationBundles(nlsBundlesRef.current)
  );

  useEffect(() => {
    if (!language) return;

    if (language && !hasLocalizedBundle.current) {
      hasLocalizedBundle.current = true;
      return;
    }
    setLocalizedBundles(getLocalizationBundles(nlsBundlesRef.current));
  }, [language]);

  const translate = useCallback(
    (nlsKey, attrs) => {
      const containingBundle = localizedBundles.find(
        (bundle) => nlsKey in bundle
      );

      if (containingBundle) {
        const localizedString = getLocalizedString(nlsKey, containingBundle);
        const stringOrTemplate = getNlsDefinition(localizedString);
        return attrs !== undefined && attrs !== null
          ? i18n.renderNLSTemplate(stringOrTemplate, attrs)
          : stringOrTemplate;
      }

      console.error(`Could not find translation for '${nlsKey}'`);
      return nlsKey;
    },
    [localizedBundles]
  );

  return translate;
};

const nlsBundlePropType = PropTypes.shape({
  lang: PropTypes.string,
  path: PropTypes.string,
  nls: PropTypes.shape({}),
});

export const nlsBundlesPropType = PropTypes.oneOfType([
  PropTypes.array,
  nlsBundlePropType,
]);

export const nlsPropType = PropTypes.shape({
  en: PropTypes.string,
  de: PropTypes.string,
  sv: PropTypes.string,
});
