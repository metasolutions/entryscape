import PropTypes from 'prop-types';
import { entryPropType } from 'commons/util/entry';
import { useParams } from 'react-router-dom';
import useNavigate from 'commons/components/router/useNavigate';
import { useTranslation } from 'commons/hooks/useTranslation';
import escaDatasetNLS from 'catalog/nls/escaDataset.nls';
import escoDialogsNLS from 'commons/nls/escoDialogs.nls';
import { getPathFromViewName } from 'commons/util/site';
import sleep from 'commons/util/sleep';
import useAsync, { PENDING } from 'commons/hooks/useAsync';
import ListActionDialog from 'commons/components/common/dialogs/ListActionDialog';
import { ErrorWithMessage } from 'commons/errors/utils/errors';
import useErrorHandler from 'commons/errors/hooks/useErrorHandler';
import LoadingButton from 'commons/components/LoadingButton';
import { copyDataset } from '../../utils/datasets';

const NLS_BUNDLES = [escaDatasetNLS, escoDialogsNLS];

const CopyDatasetDialog = ({ closeDialog, entry: datasetEntry }) => {
  const translate = useTranslation(NLS_BUNDLES);
  const viewParams = useParams();
  const { navigate } = useNavigate();

  const { runAsync, error: copyError, status } = useAsync();
  useErrorHandler(copyError);

  const handleCopyDataset = async () => {
    const datasetsPath = getPathFromViewName('catalog__datasets', viewParams);
    runAsync(
      copyDataset(datasetEntry, translate('cloneCopy'))
        .then(async () => {
          await sleep(2500);
          navigate(datasetsPath);
        })
        .catch((error) => {
          throw new ErrorWithMessage(translate('copyDatasetFail'), error);
        })
    );
  };

  const actions = (
    <LoadingButton onClick={handleCopyDataset} loading={status === PENDING}>
      {translate('proceed')}
    </LoadingButton>
  );

  return (
    <ListActionDialog maxWidth="sm" closeDialog={closeDialog} actions={actions}>
      {translate('cloneDatasetQuestion')}
    </ListActionDialog>
  );
};

CopyDatasetDialog.propTypes = {
  closeDialog: PropTypes.func,
  entry: entryPropType,
};

export default CopyDatasetDialog;
