import { withStyles } from '@mui/styles';
import MuiSwitch from '@mui/material/Switch';
import { success } from 'commons/theme/mui/colors';
import './index.scss';

const Switch = withStyles({
  switchBase: {
    color: '#FFFFFF',
    '&$checked': {
      color: success.main,
      '&$disabled': {
        color: success.light,
      },
    },
    '&$checked + $track': {
      backgroundColor: success.light,
    },
  },
  checked: {},
  track: { backgroundColor: '#BCBDBD' },
  disabled: {},
})(({ classes, ...props }) => {
  return (
    <MuiSwitch
      focusVisibleClassName={'escoSwitch--focused'}
      {...props}
      classes={{
        root: classes.root,
        switchBase: classes.switchBase,
        thumb: classes.thumb,
        track: classes.track,
        checked: classes.checked,
        disabled: classes.disabled,
      }}
    />
  );
});

export default Switch;
