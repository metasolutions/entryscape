import config from 'config';
import * as siteUtils from 'commons/util/site';
import { getIncludedFilters, getNoResultsNLS } from './filter';
import {
  PROFILE_FILTER,
  PUBLIC_STATUS_FILTER,
  EXTERNAL_STATUS_FILTER,
} from './filterDefinitions';

const FILTERS = [PROFILE_FILTER, PUBLIC_STATUS_FILTER, EXTERNAL_STATUS_FILTER];

siteUtils.getModuleNameOfCurrentView = jest.fn(() => 'catalog');
config.get = jest.fn();

describe('filter utility functions', () => {
  test('getIncludedFilters w/ config to exclude all filters', () => {
    config.get.mockReturnValue(true);
    expect(getIncludedFilters(FILTERS)).toEqual([]);
  });

  test('getIncludedFilters w/ config to exclude no filters', () => {
    config.get.mockReturnValue(false);
    expect(getIncludedFilters(FILTERS)).toEqual(FILTERS);
  });

  test('getIncludedFilters w/ config to exclude specific filters', () => {
    config.get.mockReturnValue([
      PUBLIC_STATUS_FILTER.id,
      EXTERNAL_STATUS_FILTER.id,
    ]);
    expect(getIncludedFilters(FILTERS)).toEqual([PROFILE_FILTER]);
  });

  test('getNoResultsNLS', () => {
    expect(getNoResultsNLS(true, true)).toBe('noSearchResultsFiltersAndQuery');
    expect(getNoResultsNLS(true, false)).toBe('noSearchResults');
    expect(getNoResultsNLS(false, true)).toBe('noSearchResultsFilters');
    expect(getNoResultsNLS(false, false)).toBe('noResults');
  });
});
