import PropTypes from 'prop-types';
import { Entry } from '@entryscape/entrystore-js';
import { OverviewRemoveEntryDialog } from 'commons/components/overview';
import useCheckReferences from 'commons/hooks/references/useCheckReferences';
import escaContactsNLS from 'catalog/nls/escaContacts.nls';
import { RESOLVED } from 'commons/hooks/useAsync';

const RemoveContactDialog = ({ entry: contactEntry, closeDialog, ...rest }) => {
  const { references, checkReferencesStatus } = useCheckReferences({
    entry: contactEntry,
    nlsBundles: [escaContactsNLS],
    callback: closeDialog,
  });

  if (checkReferencesStatus !== RESOLVED || references.length !== 0)
    return null;

  return (
    <OverviewRemoveEntryDialog
      entry={contactEntry}
      closeDialog={closeDialog}
      {...rest}
    />
  );
};

RemoveContactDialog.propTypes = {
  entry: PropTypes.instanceOf(Entry),
  closeDialog: PropTypes.func,
};

export default RemoveContactDialog;
