import {
  Alert,
  Button,
  FormControl,
  FormHelperText,
  IconButton,
  MenuItem,
  Select,
  TextField,
  Typography,
} from '@mui/material';
import { useTranslation } from 'commons/hooks/useTranslation';
// eslint-disable-next-line max-len
import { useVisualizationState } from 'catalog/datasets/DatasetOverview/hooks/useVisualizationState';
import ChartPlaceholder from 'catalog/datasets/DatasetOverview/VisualizationForm/ChartPlaceholder';
import { MapProvider } from 'commons/components/Map/hooks/useMapContext';
import escoListNLS from 'commons/nls/escoList.nls';
import escaDatasetNLS from 'catalog/nls/escaDataset.nls';
import escaVisualizationNLS from 'catalog/nls/escaVisualization.nls';
import escoGeoMapNLS from 'commons/nls/escoGeoMap.nls';
import RemoveCircleIcon from '@mui/icons-material/RemoveCircle';
import AddIcon from '@mui/icons-material/Add';
import config from 'config';
import { i18n } from 'esi18n';
import FormSection from './FormSection';
import CsvForm from './CsvForm';
import WmsForm from './WmsForm';
import InputFieldsLabel from './InputFieldsLabel';
import './VisualizationForm.scss';

const nlsBundles = [
  escoListNLS,
  escaDatasetNLS,
  escaVisualizationNLS,
  escoGeoMapNLS,
];

/**
 *
 * @param {object[]} fieldsState
 * @returns {boolean}
 */

export const isComplete = (fieldsState) => {
  if (!fieldsState) return false;
  const { chartType, titles, ...rest } = fieldsState;
  const hasValidTitles = titles.some(({ title }) => title.length);

  if (chartType === 'wms') {
    return hasValidTitles;
  }

  const { encoding, xField, yField, columns, operation } = rest;

  const commonCsvFields = {
    encoding,
    xField,
  };
  const hasYFieldIfNeeded = yField || operation === 'count';
  const hasValidCsvFields =
    hasValidTitles &&
    Object.entries(commonCsvFields).every(([, value]) => value);

  if (chartType === 'line' || chartType === 'map') {
    return hasValidCsvFields && xField !== '' && yField !== '';
  }

  if (chartType === 'bar' || chartType === 'pie') {
    return hasValidCsvFields && hasYFieldIfNeeded && xField !== '';
  }
  return hasValidTitles && columns?.length;
};

const VisualizationForm = () => {
  const translate = useTranslation(nlsBundles);

  const {
    visualizationEntry,
    fieldsState,
    sourceChoices,
    selectedSourceChoice,
    updateFieldsState,
    onSourceChange,
    errors,
    isBroken,
  } = useVisualizationState();
  const sourceError = errors.sourceURI || '';

  const {
    titles = [
      { title: '', lang: i18n.getLocale() || config.get('locale.fallback') },
    ],
  } = fieldsState || {};

  const languageItems = config.get('locale.supported');

  const onTitleChange =
    (index) =>
    ({ target }) => {
      titles[index].title = target.value.trim().length ? target.value : '';
      updateFieldsState({ titles: [...titles] });
    };

  const onLangChange =
    (index) =>
    ({ target }) => {
      titles[index].lang = target.value;
      updateFieldsState({ titles: [...titles] });
    };

  const onCloseClick = (index) => () => {
    if (titles.length > 1) {
      titles.splice(index, 1);
      updateFieldsState({ titles: [...titles] });
    }
  };

  const onAddClick = () => {
    updateFieldsState({
      titles: [
        ...titles,
        { lang: i18n.getLocale() || config.get('locale.fallback'), title: '' },
      ],
    });
  };

  return (
    <>
      {isBroken ? (
        <Alert severity="error">{translate('brokenVizError')}</Alert>
      ) : null}
      <form autoComplete="off">
        <FormSection>
          <FormControl variant="filled" fullWidth error={Boolean(sourceError)}>
            <InputFieldsLabel
              id="source-select"
              label={translate('vizDialogDistributionTitle')}
              mandatory
            />
            <Select
              displayEmpty
              inputProps={{ 'aria-labelledby': 'source-select' }}
              value={selectedSourceChoice?.sourceURI || ''}
              renderValue={() => {
                if (!selectedSourceChoice) {
                  return <span>{translate('vizDialogDistributionUse')}</span>;
                }
                return selectedSourceChoice.name;
              }}
              onChange={onSourceChange}
            >
              {sourceChoices.map((sourceItem, index) => (
                <MenuItem
                  // eslint-disable-next-line react/no-array-index-key
                  key={`${sourceItem.sourceURI}-${index}`}
                  value={sourceItem.sourceURI}
                >
                  <Typography
                    variant="inherit"
                    noWrap
                  >{`${sourceItem.name} - ${sourceItem.format}`}</Typography>
                </MenuItem>
              ))}
            </Select>
            {sourceError && (
              <FormHelperText error>{sourceError}</FormHelperText>
            )}
          </FormControl>
        </FormSection>
        <FormSection>
          <InputFieldsLabel
            label={translate('vizDialogNameviz')}
            id="title-field-label"
            mandatory
          />
          {titles.map(({ title, lang: currentLang }, index) => (
            <FormControl
              variant="filled"
              fullWidth
              style={{ flexDirection: 'row', gap: '1em' }}
              // eslint-disable-next-line react/no-array-index-key
              key={`lang-select-${currentLang}-${index}`}
            >
              <TextField
                inputProps={{ 'aria-labelledby': 'title-field-label' }}
                value={title}
                onChange={onTitleChange(index)}
                required
              />
              <Select
                displayEmpty
                value={currentLang}
                className="escaVisualizationForm__langSelect"
                onChange={onLangChange(index)}
              >
                {languageItems.map(({ lang, label }) => (
                  <MenuItem key={lang} value={lang}>
                    <Typography variant="inherit" noWrap>
                      {label}
                    </Typography>
                  </MenuItem>
                ))}
              </Select>
              <IconButton
                className="escaVisualizationForm__closeButton"
                onClick={onCloseClick(index)}
                disabled={titles.length <= 1}
              >
                <RemoveCircleIcon />
              </IconButton>
            </FormControl>
          ))}
          <Button
            disableElevation
            variant="text"
            color="primary"
            onClick={onAddClick}
            startIcon={<AddIcon />}
          >
            {translate('addTitleLabel')}
          </Button>
        </FormSection>
        {!selectedSourceChoice?.format && (
          <FormSection title={translate('vizDialogPreview')}>
            <ChartPlaceholder
              label={
                isBroken
                  ? translate('brokenVizPlaceholderLabel')
                  : translate('placeholderLabel')
              }
              labelClass={isBroken ? 'escaChartPlaceHolderLabel--error' : ''}
            />
          </FormSection>
        )}
        {fieldsState && selectedSourceChoice?.format === 'wms' && (
          <MapProvider>
            <WmsForm
              visualizationEntry={visualizationEntry}
              translate={translate}
            />
          </MapProvider>
        )}
        {selectedSourceChoice?.format === 'csv' ? (
          <CsvForm translate={translate} />
        ) : null}
      </form>
    </>
  );
};
VisualizationForm.propTypes = {};

export default VisualizationForm;
