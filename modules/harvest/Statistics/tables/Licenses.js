import PropTypes from 'prop-types';
import Table from '@mui/material/Table';
import TableHead from '@mui/material/TableHead';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import Link from '@mui/material/Link';
import Typography from '@mui/material/Typography';
import { useTranslation } from 'commons/hooks/useTranslation';
import { makeStyles } from '@mui/styles';
import 'harvest/Statistics/tables/index.scss';

const useStyles = makeStyles(() => ({
  root: {
    fontSize: 'inherit',
    fontWeight: 600,
    '&:hover': {
      textDecoration: 'underline !important',
    },
  },
}));
const Licenses = ({ licensesData, nlsBundle }) => {
  const licenseKeysSortedByVal = Object.keys(licensesData).sort(
    (a, b) => licensesData[b] - licensesData[a]
  );
  const classes = useStyles();
  const t = useTranslation(nlsBundle);

  return (
    <div>
      {licenseKeysSortedByVal.length > 0 && (
        <>
          <Typography
            variant="h6"
            component="h2"
            className="esreStatistics__header"
          >
            {t('licenses')}
          </Typography>
          <TableContainer component={Paper}>
            <Table size="small" aria-label={t('licenses')}>
              <TableHead>
                <TableRow>
                  <TableCell align="left" className="esreTable__th--bold">
                    {t('url')}
                  </TableCell>
                  <TableCell className="esreTable__th--bold" align="center">
                    {t('timesUsed')}
                  </TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {licenseKeysSortedByVal.map((key) => (
                  <TableRow hover key={key}>
                    <TableCell
                      className="esreTable__th--bold"
                      component="th"
                      scope="row"
                    >
                      <Typography className={classes.root}>
                        <Link
                          href={key}
                          color="inherit"
                          target="_blank"
                          rel="noopener"
                        >
                          {key}
                        </Link>
                      </Typography>
                    </TableCell>
                    <TableCell align="center">{licensesData[key]}</TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </>
      )}
    </div>
  );
};

Licenses.propTypes = {
  licensesData: PropTypes.shape(),
  nlsBundle: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
};

Licenses.defaultProps = {
  licensesData: {},
};
export default Licenses;
