import PropTypes from 'prop-types';
import useErrorHandler from 'commons/errors/hooks/useErrorHandler';
import { entryPropType } from 'commons/util/entry';
import ListActionDialog from 'commons/components/common/dialogs/ListActionDialog';
import { useTranslation } from 'commons/hooks/useTranslation';
import useAsync, { PENDING } from 'commons/hooks/useAsync';
import LoadingButton from 'commons/components/LoadingButton';
import {
  useVisualizationState,
  VisualizationStateProvider,
} from 'catalog/datasets/DatasetOverview/hooks/useVisualizationState';
import { ErrorCatcher } from 'commons/errors/ErrorCatcher';
import { ErrorWithMessage } from 'commons/errors/utils/errors';
import escoListNLS from 'commons/nls/escoList.nls';
import escaDatasetNLS from 'catalog/nls/escaDataset.nls';
import escaVisualizationNLS from 'catalog/nls/escaVisualization.nls';
import escoGeoMapNLS from 'commons/nls/escoGeoMap.nls';
import VisualizationForm, { isComplete } from '../../VisualizationForm';
import { updateVisualizationMetadata } from '../../utils/visualizations';
import useVisualizationData from '../../hooks/useVisualizationData';

const nlsBundles = [
  escoListNLS,
  escaDatasetNLS,
  escaVisualizationNLS,
  escoGeoMapNLS,
];

const EditVisualizationDialog = ({
  datasetEntry,
  visualizationEntry,
  refreshVisualizations,
  closeDialog,
  translate,
}) => {
  const { error: createError, runAsync, status } = useAsync();
  const { fieldsState, isBroken, hasChanged } = useVisualizationState();

  const handleEditEntry = () => {
    updateVisualizationMetadata(visualizationEntry, fieldsState);
    runAsync(
      visualizationEntry
        .commitMetadata()
        .then(() => {
          refreshVisualizations();
          closeDialog();
        })
        .catch((error) => {
          throw new ErrorWithMessage(translate('saveEditsFail'), error);
        })
    );
  };

  const dialogActions = (
    <LoadingButton
      onClick={handleEditEntry}
      loading={status === PENDING}
      disabled={isBroken || !hasChanged || !isComplete(fieldsState)}
    >
      {translate('vizDialogFooter')}
    </LoadingButton>
  );

  return (
    <>
      <ListActionDialog
        id="edit-visualization-entry"
        title={translate('editDialogTitle')}
        actions={dialogActions}
        closeDialog={closeDialog}
        fixedHeight
      >
        <VisualizationForm datasetEntry={datasetEntry} />
      </ListActionDialog>
      <ErrorCatcher error={createError} />
    </>
  );
};
EditVisualizationDialog.propTypes = {
  datasetEntry: entryPropType,
  visualizationEntry: entryPropType,
  refreshVisualizations: PropTypes.func,
  translate: PropTypes.func,
  closeDialog: PropTypes.func.isRequired,
};

const EditVisualizationWithProvider = ({
  entry: datasetEntry,
  visualizationEntry,
  ...props
}) => {
  const translate = useTranslation(nlsBundles);
  const { data, isLoading, error } = useVisualizationData({
    datasetEntry,
    visualizationEntry,
    translate,
  });
  const { sourceChoices, selectedSourceChoice, initialFields, errors } = data;
  useErrorHandler(error);

  if (isLoading) return null;

  return (
    <VisualizationStateProvider
      visualizationEntry={visualizationEntry}
      sourceChoices={sourceChoices}
      initialSourceChoice={selectedSourceChoice}
      initialFields={initialFields}
      initialErrors={errors}
    >
      <EditVisualizationDialog
        datasetEntry={datasetEntry}
        visualizationEntry={visualizationEntry}
        translate={translate}
        {...props}
      />
    </VisualizationStateProvider>
  );
};

EditVisualizationWithProvider.propTypes = {
  entry: entryPropType,
  visualizationEntry: entryPropType,
};

export default EditVisualizationWithProvider;
