import { namespaces as ns } from '@entryscape/rdfjson';
import Lookup from 'commons/types/Lookup';
// import TextAsChoiceRenderer from 'commons/rdforms/choosers/TextAsChoiceRenderer'; // TODO ?
import config from 'config';
import { getViewDefFromName } from 'commons/util/site';

export default () => {
  ns.add('dcat', 'http://www.w3.org/ns/dcat#');
  ns.add('esterms', 'http://entryscape.com/terms/');
  ns.add('oa', 'http://www.w3.org/ns/oa#');

  const entities = [
    'publisher',
    'catalog',
    'dataset',
    'dataService',
    'distribution',
    'contactPoint',
    'datasetResult',
    'datasetDocument',
    'datasetIdea',
    'catalogSuggestion',
  ];
  // Copy over templates from catalog config to corresponding entitytypes
  entities.forEach((entityName) => {
    const templateId = config.get('catalog')[`${entityName}TemplateId`];
    const entityType = Lookup.getByName(entityName);
    if (templateId && entityType.get()) {
      entityType.set('template', templateId);
    }
  });

  // Copy over include settings to view so secondary navigation is hidden for these views (navbar:false)
  const includes2Name = {
    includeStatistics: 'catalog__statistics',
    includeShowcases: 'catalog__showcases',
    includeIdeas: 'catalog__ideas',
    includePreparations: 'catalog__suggestions',
    includePublishers: 'catalog__publishers',
    includeContacts: 'catalog__contacts',
    includeDocuments: 'catalog__documents',
    includeOverview: 'catalog__overview',
  };

  Object.keys(includes2Name).forEach((key) => {
    if (config.get(`catalog.${key}`) === false) {
      const viewDef = getViewDefFromName(includes2Name[key]);
      viewDef.navbar = false;
    }
  });
  // TextAsChoiceRenderer.registerDefaults(); // TODO
};
