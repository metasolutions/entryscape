import { useContext, createContext, useState, useEffect } from 'react';
import PropTypes from 'prop-types';

let rdfContext;
let rdfContextName;

/**
 * Manages the context (entry) of an rdforms
 */
const RdformsContext = createContext();

/**
 * A context provider for rdforms
 * @param {node} children
 */
export const RdformsContextProvider = ({ children }) => {
  const [rdformsContext, setRdformsContext] = useState(null);
  const [rdformsContextName, setRdformsContextName] = useState('');

  rdfContext = rdformsContext;
  rdfContextName = rdformsContextName;

  useEffect(() => {
    if (rdformsContext) {
      rdformsContext.getEntry().then((contextEntry) => {
        setRdformsContextName(contextEntry.getEntryInfo().getName());
      });
    }

    return () => {
      rdfContext = null;
      rdfContextName = '';
    };
  }, [rdformsContext]);

  return (
    <RdformsContext.Provider value={{ rdfContext, setRdformsContext }}>
      {children}
    </RdformsContext.Provider>
  );
};

RdformsContextProvider.propTypes = {
  children: PropTypes.node,
};

export const useRdformsContext = () => {
  const context = useContext(RdformsContext);
  if (!context) {
    console.error('useRdformsContext must be used within a ContextProvider');
  }
  return context;
};

/**
 * Use this method in callbacks when useRdformsContext cannot be used.
 * @return {Context}
 */
export const getRdformsContext = () => {
  return rdfContext;
};

export const getRdformsContextAndName = () => {
  return { context: rdfContext, contextName: rdfContextName };
};

/**
 * Wraps a component with RdformsContextProvider
 * @return {node}
 */
export const withRdformsContextProvider = (Component) => {
  return (props) => {
    return (
      <RdformsContextProvider>
        <Component {...props} />
      </RdformsContextProvider>
    );
  };
};
