import { useEntry } from 'commons/hooks/useEntry';
import { getLabel, getFullLengthLabel } from 'commons/util/rdfUtils';
import { truncate } from 'commons/util/util';
import { getParentCatalogEntry } from 'commons/util/metadata';
import { getViewDefFromName } from 'commons/util/site';
import escaCatalogNLS from 'catalog/nls/escaCatalog.nls';
import escaContactsNLS from 'catalog/nls/escaContacts.nls';
import escoListNLS from 'commons/nls/escoList.nls';
import { useTranslation } from 'commons/hooks/useTranslation';
import useAsyncCallback from 'commons/hooks/useAsyncCallback';
import useDocumentTitle from 'commons/hooks/useDocumentTitle';
import usePageTitle from 'commons/hooks/usePageTitle';
import useGetContributors from 'commons/hooks/useGetContributors';
import escoOverviewNLS from 'commons/nls/escoOverview.nls';
import {
  ACTION_INFO_WITH_ICON,
  DESCRIPTION_UPDATED,
} from 'commons/components/overview/actions';
import Overview from 'commons/components/overview/Overview';
import {
  withOverviewModelProvider,
  overviewPropsPropType,
  useOverviewModel,
} from 'commons/components/overview';
import LinkedDataBrowserDialog from 'commons/components/LinkedDataBrowserDialog';
import { localize } from 'commons/locale';
import { sidebarActions } from './actions';

const nlsBundles = [
  escaCatalogNLS,
  escoListNLS,
  escaContactsNLS,
  escoOverviewNLS,
];

const ContactOverview = ({ overviewProps }) => {
  const entry = useEntry();
  const t = useTranslation(nlsBundles);
  const [catalogEntry] = useAsyncCallback(getParentCatalogEntry, entry);
  const [{ refreshCount }] = useOverviewModel();
  const { contributors } = useGetContributors(entry, refreshCount);
  const [pageTitle] = usePageTitle();
  const viewDefinition = getViewDefFromName('catalog__contacts__contact');
  const viewDefinitionTitle = localize(viewDefinition.title);

  const descriptionItems = [
    {
      id: 'catalog',
      labelNlsKey: 'overviewCatalogLabel',
      getValues: () =>
        catalogEntry ? [truncate(getFullLengthLabel(catalogEntry), 90)] : [],
    },
    DESCRIPTION_UPDATED,
    {
      id: 'edited',
      labelNlsKey: 'editedByLabel',
      getValues: () => contributors,
    },
  ];

  useDocumentTitle(`${pageTitle} - ${viewDefinitionTitle} ${getLabel(entry)}`);

  return (
    <Overview
      {...overviewProps}
      backLabel={t('backTitle')}
      headerAction={{
        ...ACTION_INFO_WITH_ICON,
        Dialog: LinkedDataBrowserDialog,
      }}
      entry={entry}
      nlsBundles={nlsBundles}
      descriptionItems={descriptionItems}
      sidebarActions={sidebarActions}
    />
  );
};

ContactOverview.propTypes = {
  overviewProps: overviewPropsPropType,
};

export default withOverviewModelProvider(ContactOverview);
