import PropTypes from 'prop-types';
import CreateEntityDialog from 'commons/components/EntityOverview/dialogs/CreateEntityDialog';
import { useListView } from 'commons/components/ListView';
import { useESContext } from 'commons/hooks/useESContext';
import Lookup from 'commons/types/Lookup';

const RESOURCE_ENTITY_NAME = 'resources';

const CreateResourceDialog = ({ closeDialog }) => {
  const { nlsBundles } = useListView();
  const { context, contextName } = useESContext();

  const entityType = Lookup.getByName(RESOURCE_ENTITY_NAME);

  const actionParams = {
    nlsBundles,
    entityType,
    context,
    contextName,
  };

  return (
    <CreateEntityDialog actionParams={actionParams} closeDialog={closeDialog} />
  );
};

CreateResourceDialog.propTypes = {
  closeDialog: PropTypes.func,
};

export default CreateResourceDialog;
