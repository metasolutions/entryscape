import { useState } from 'react';
import PropTypes from 'prop-types';
import { graphPropType } from 'commons/util/entry';
import * as ns from 'models/utils/ns';
import {
  DATATYPE_LITERAL,
  Fields,
  Field,
  useFieldsEdit,
  TextField,
  RadioGroup,
} from 'commons/components/forms/editors';
import {
  MANDATORY,
  RECOMMENDED,
  OPTIONAL,
  getIncludeLevel,
  setCardinality,
} from 'models/utils/cardinality';
import { useTranslation } from 'commons/hooks/useTranslation';
import esmoCommonsNls from 'models/nls/esmoCommons.nls';

export const CardinalityEditor = ({
  label,
  graph,
  resourceURI,
  dispatch,
  children,
  ...fieldsProps
}) => {
  const t = useTranslation(esmoCommonsNls);
  const [includeLevel, setIncludeLevel] = useState(() =>
    getIncludeLevel(resourceURI, graph)
  );

  const { fields, labelId, updateValue } = useFieldsEdit({
    name: fieldsProps.name,
    property: ns.RDF_PROPERTY_MAX,
    nodetype: DATATYPE_LITERAL,
    datatype: 'xsd:integer',
    graph,
    resourceURI,
    dispatch,
  });
  const [maxField] = fields;

  const onClear = () => {
    setIncludeLevel('');
    updateValue(maxField, '');
    setCardinality(graph, resourceURI, '');
  };

  const updateIncludeLevel = (level) => {
    setIncludeLevel(level);
    setCardinality(graph, resourceURI, level);
  };

  const choices = [
    { label: t('mandatoryLabel'), value: MANDATORY },
    { label: t('recommendedLabel'), value: RECOMMENDED },
    { label: t('optionalLabel'), value: OPTIONAL },
  ];

  return (
    <Fields labelId={labelId} label={label} max={1} {...fieldsProps}>
      {children}
      <Field size={1} onClear={onClear}>
        <RadioGroup
          xs="auto"
          labelId={labelId}
          choices={choices}
          onChange={({ target }) => updateIncludeLevel(target.value)}
          value={includeLevel}
          row
        />
        <TextField
          id={labelId}
          label={t('maxLabel')}
          xs={2}
          value={maxField.getValue()}
          size="small"
          onChange={({ target }) => updateValue(maxField, target.value)}
        />
      </Field>
    </Fields>
  );
};

CardinalityEditor.propTypes = {
  label: PropTypes.string,
  graph: graphPropType,
  resourceURI: PropTypes.string,
  dispatch: PropTypes.func,
  children: PropTypes.node,
};
