import PropTypes from 'prop-types';
import { StyledEngineProvider, ThemeProvider } from '@mui/material/styles';
import { UserProvider } from 'commons/hooks/useUser';
import { MainDialogProvider } from 'commons/hooks/useMainDialog';
import customTheme from 'commons/theme/mui/mui-theme';

const AppProviders = ({ children }) => (
  <UserProvider>
    {/* Inject Mui styles first so that our (are last) taking precedence */}
    <StyledEngineProvider injectFirst>
      <ThemeProvider theme={customTheme}>
        <MainDialogProvider>{children} </MainDialogProvider>
      </ThemeProvider>
    </StyledEngineProvider>
  </UserProvider>
);

AppProviders.propTypes = {
  children: PropTypes.node,
};

export default AppProviders;
