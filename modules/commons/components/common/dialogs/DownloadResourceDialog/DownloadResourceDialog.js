import PropTypes from 'prop-types';
import { useEffect } from 'react';
import { entryPropType } from 'commons/util/entry';

// Mimics a dialog by closing immediately after opening
const DownloadResourceDialog = ({ entry, closeDialog }) => {
  useEffect(() => {
    const target = entry.isLinkReference() ? '_blank' : '_self';
    window.open(`${entry.getResourceURI()}?download`, target);
    closeDialog();
  }, [closeDialog, entry]);

  return null;
};

DownloadResourceDialog.propTypes = {
  entry: entryPropType,
  closeDialog: PropTypes.func,
};

export default DownloadResourceDialog;
