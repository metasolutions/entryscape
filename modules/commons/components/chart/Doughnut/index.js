import React, { useEffect, useRef, useState } from 'react';
import { i18n } from 'esi18n';
import escoChartNLS from '../../../nls/escoChart.nls';
import { COLOURS_PIE } from '../colors';
import '../style.css';

export default ({
  options,
  type = 'doughnut',
  data,
  colors = [],
  dimensions = {},
}) => {
  const { width = '100%', height = '100%' } = dimensions;
  const chartContainer = useRef(null);
  const [chartInstance, setChartInstance] = useState(null);

  useEffect(() => {
    if (chartContainer && chartContainer.current) {
      import(/* webpackChunkName: "chart-js" */ 'chart.js')
        .then((Chart) => Chart.default)
        .then((Chart) => {
          const hardcodedOptions =
            type === 'doughnut'
              ? {
                  circumference: Math.PI, // half doughnut
                  rotation: -Math.PI,
                }
              : {
                  legend: {
                    position: 'right',
                  },
                };
          setChartInstance(
            new Chart(chartContainer.current, {
              type,
              options: Object.assign(hardcodedOptions, options),
            })
          );
        });
    }
  }, [chartContainer]);

  const dataExists =
    chartInstance &&
    data &&
    data.datasets &&
    data.datasets.length > 0 &&
    data.datasets[0].data.length > 0;
  if (dataExists) {
    // update chart data if needed
    data.datasets[0].backgroundColor = [...colors, ...COLOURS_PIE];
    chartInstance.data = data;
    chartInstance.update();
  }

  const containerStyle = {
    position: 'relative',
    height: `${height}`,
    width: `${width}`,
  };
  const escoChart = i18n.getLocalization(escoChartNLS);
  return (
    <div className="chart-container" style={containerStyle}>
      <div className="no-data" className={dataExists ? 'escoChart__hidden' : ''}>{escoChart.noDataMessage}</div>
      <canvas aria-label="Statistics chart" role="img" className={dataExists ? '': 'escoChart__hidden'} ref={chartContainer} />
    </div>
  );
};
