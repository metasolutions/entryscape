import { types } from '@entryscape/entrystore-js';
import { useUserState } from 'commons/hooks/useUser';
import { getSearchQuery } from 'commons/util/solr/entry';
import React, { useCallback } from 'react';
import {
  EntryListView,
  TITLE_COLUMN,
  MODIFIED_COLUMN,
  useSolrQuery,
  ACTIONS_GROUP_COLUMN,
} from 'commons/components/EntryListView';
import {
  LIST_ACTION_INFO,
  LIST_ACTION_EDIT,
} from 'commons/components/EntryListView/actions';
import {
  withListModelProviderAndLocation,
  listPropsPropType,
} from 'commons/components/ListView';
import { useTranslation } from 'commons/hooks/useTranslation';
import { getRenderName } from 'commons/util/rdfUtils';
import { applyUsersGroupParams } from 'admin/utils';
import { getPathFromViewName } from 'commons/util/site';
import LinkedDataBrowserDialog from 'commons/components/LinkedDataBrowserDialog';
import { listActions, nlsBundles } from './actions';

const entryType = types.ET_LOCAL;
const graphType = types.GT_GROUP;
const resourceType = 'InformationResource'; // types.RT_INFORMATIONRESOURCE); // TODO: in entrystore-js

const GroupsView = ({ listProps }) => {
  const { userEntry, userInfo } = useUserState();
  const t = useTranslation(nlsBundles);
  const entryTypeName = t('createEntryName');

  const createQuery = useCallback(() => {
    const [query] = getSearchQuery(
      { entryType, graphType, resourceType },
      { userEntry, userInfo }
    );
    return query;
  }, [userEntry, userInfo]);

  const queryResults = useSolrQuery({
    createQuery,
    applyQueryParams: applyUsersGroupParams,
  });

  return (
    <EntryListView
      {...queryResults}
      {...listProps}
      nlsBundles={nlsBundles}
      listPlaceholderProps={{
        primaryText: t('emptyMessageWithName', entryTypeName),
      }}
      getListItemProps={({ entry }) => ({
        to: getPathFromViewName('admin__groups__group', {
          entryId: entry.getId(),
          contextId: '_principals',
        }),
      })}
      listActions={listActions}
      listActionsProps={{ nlsBundles }}
      columns={[
        {
          ...TITLE_COLUMN,
          xs: 9,
          getProps: ({ entry }) => ({
            primary:
              getRenderName(entry) || t('unnamedWorkspace', entry.getId()),
          }),
        },
        { ...MODIFIED_COLUMN },
        {
          ...ACTIONS_GROUP_COLUMN,
          actions: [
            {
              ...LIST_ACTION_INFO,
              formTemplateId: 'esc:Group',
              Dialog: LinkedDataBrowserDialog,
            },
            {
              ...LIST_ACTION_EDIT,
              formTemplateId: 'esc:Group',
            },
          ],
        },
      ]}
    />
  );
};

GroupsView.propTypes = {
  listProps: listPropsPropType,
};

export default withListModelProviderAndLocation(GroupsView);
