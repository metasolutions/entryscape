import { STATUS_IDLE } from 'commons/hooks/useTasksList';

export default [
  {
    id: 'parse',
    nlsKeyLabel: 'parseTask',
    status: STATUS_IDLE,
    message: '',
  },
  {
    id: 'analysis',
    nlsKeyLabel: 'analysisTask',
    status: STATUS_IDLE,
    message: '',
  },
  {
    id: 'importClasses',
    nlsKeyLabel: 'importClassesTask',
    status: STATUS_IDLE,
    message: '',
  },
  {
    id: 'importProperties',
    nlsKeyLabel: 'importPropertiesTask',
    status: STATUS_IDLE,
    message: '',
  },
];
