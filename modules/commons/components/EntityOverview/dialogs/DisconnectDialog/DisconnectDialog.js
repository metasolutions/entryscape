import { useEffect } from 'react';
import PropTypes from 'prop-types';
import { entryPropType } from 'commons/util/entry';
import useGetMainDialog from 'commons/hooks/useGetMainDialog';
import {
  nlsBundlesPropType,
  useTranslation,
} from 'commons/hooks/useTranslation';
import useAsync from 'commons/hooks/useAsync';
import { removeRelationFromParent } from 'commons/types/utils/relations';
import escoDialogsNLS from 'commons/nls/escoDialogs.nls';
import escoEntityOverviewNLS from 'commons/nls/escoEntityOverview.nls';

const DisconnectDialog = ({
  parentEntry,
  entry: connectedEntry,
  relation,
  closeDialog,
  refresh,
  nlsBundles = [],
}) => {
  const translate = useTranslation([
    ...nlsBundles,
    escoDialogsNLS,
    escoEntityOverviewNLS,
  ]);
  const { runAsync } = useAsync();
  const { getConfirmationDialog } = useGetMainDialog();

  useEffect(() => {
    runAsync(
      getConfirmationDialog({
        content: translate('disconnectDialogPrompt'),
        rejectLabel: translate('cancel'),
        affirmLabel: translate('proceed'),
      }).then((proceed) => {
        if (proceed) {
          removeRelationFromParent(
            parentEntry,
            connectedEntry,
            relation.property
          ).then(() => {
            closeDialog();
            refresh();
          });
          return;
        }

        closeDialog();
      })
    );
  }, [
    runAsync,
    getConfirmationDialog,
    translate,
    closeDialog,
    parentEntry,
    connectedEntry,
    relation,
    refresh,
  ]);

  return null;
};

DisconnectDialog.propTypes = {
  parentEntry: entryPropType,
  entry: entryPropType,
  relation: PropTypes.shape({ property: PropTypes.string }),
  closeDialog: PropTypes.func,
  refresh: PropTypes.func,
  nlsBundles: nlsBundlesPropType,
};

export default DisconnectDialog;
