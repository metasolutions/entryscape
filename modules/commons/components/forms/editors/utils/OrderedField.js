import { Blank } from './Blank';
import { Field } from './Field';
import { BaseField } from './BaseField';

/**
 * @param {Graph} graph
 * @param {string} blankNodeId
 * @param {string} orderProperty
 * @param {number} order
 * @returns {Statement}
 */
const getOrderStatement = (graph, blankNodeId, orderProperty, order = 1) => {
  const [orderStatement] = graph.find(blankNodeId, orderProperty);
  if (orderStatement) return orderStatement;
  return graph.create(
    blankNodeId,
    orderProperty,
    {
      type: 'literal',
      value: `${order}`,
      datatype: 'xsd:integer',
    },
    false
  );
};

export class OrderedField extends BaseField {
  constructor({
    graph,
    resourceURI,
    property,
    blankNodeId,
    orderProperty,
    order,
    valueProperty = 'rdf:value',
    rdfType,
    nodetype,
    includeValue = true,
    ...fieldProperties
  }) {
    super({ graph, resourceURI, nodetype, property });
    this._blank = new Blank({
      graph,
      resourceURI,
      property,
      blankNodeId,
      rdfType,
    });
    this._blankNodeId = this._blank.getBlankNodeId();
    this._orderStatement = getOrderStatement(
      graph,
      this._blankNodeId,
      orderProperty,
      order
    );
    this._valueField = includeValue
      ? new Field({
          ...fieldProperties,
          statement: graph.find(this._blankNodeId, valueProperty)[0],
          graph,
          resourceURI: this._blankNodeId,
          property: valueProperty,
          nodetype,
        })
      : null;
  }

  remove() {
    if (this._valueField) {
      this._valueField.remove();
    }
    this._blank.remove();
    this._graph.remove(this._orderStatement);
  }

  getOrder() {
    return parseInt(this._orderStatement.getValue(), 10);
  }

  setOrder(orderValue) {
    this._orderStatement.setValue(`${orderValue}`);
  }

  getValue() {
    if (this._valueField) return this._valueField.getValue();
  }

  setValue(value) {
    if (this._valueField) {
      this._valueField.setValue(value);
    }
    if (value) {
      this._blank.setAsserted(true);
      this._orderStatement.setAsserted(true);
    }
    if (!value) {
      this._blank.setAsserted(false);
      this._orderStatement.setAsserted(false);
    }
  }

  getBlank() {
    return this._blank;
  }

  getValueField() {
    return this._valueField;
  }

  setAsserted(asserted) {
    this._orderStatement.setAsserted(asserted);
    this._blank.setAsserted(asserted);
  }

  validate() {
    if (!this._valueField) return;
    this._valueField.validate();
  }

  getError() {
    return this?._valueField.getError();
  }

  getStatus() {
    return this?._valueField.getStatus();
  }

  hasError() {
    if (!this._valueField) return false;
    return this._valueField.hasError();
  }
}
