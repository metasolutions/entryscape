import PropTypes from 'prop-types';
import { Entry } from '@entryscape/entrystore-js';
import { useEffect } from 'react';
import { useParams } from 'react-router-dom';
import useNavigate from 'commons/components/router/useNavigate';
import useAsync from 'commons/hooks/useAsync';
import { getPathFromViewName } from 'commons/util/site';
import escaDataServicesNLS from 'catalog/nls/escaServices.nls';
import { useTranslation } from 'commons/hooks/useTranslation';
import { ErrorWithMessage } from 'commons/errors/utils/errors';
import useErrorHandler from 'commons/errors/hooks/useErrorHandler';
import useRemoveDataService from 'catalog/dataServices/useRemoveDataService';
import ErrorBoundary from 'commons/errors/ErrorBoundary';
import RemoveEntryDialog from 'commons/components/entry/RemoveEntryDialog';
import DialogFallbackComponent from 'commons/errors/DialogFallbackComponent';

const RemoveDataServiceDialog = ({
  entry: dataserviceEntry,
  closeDialog,
  ...rest
}) => {
  const [checkHasReferences, removeDataService] =
    useRemoveDataService(dataserviceEntry);
  const { navigate } = useNavigate();
  const viewParams = useParams();
  const dataServicesPath = getPathFromViewName(
    'catalog__dataservices',
    viewParams
  );
  const translate = useTranslation(escaDataServicesNLS);
  const {
    data: canRemove,
    runAsync,
    error: checkError,
  } = useAsync({ data: false });
  useErrorHandler(checkError);

  const handleRemove = async () => {
    try {
      await removeDataService();
      navigate(dataServicesPath);
    } catch (error) {
      throw new ErrorWithMessage(translate('unableToRemoveDataservice'), error);
    }
  };

  useEffect(() => {
    const handleCheck = async () => {
      try {
        const isRemovable = await checkHasReferences();
        if (!isRemovable) closeDialog();
        return isRemovable;
      } catch (error) {
        throw new ErrorWithMessage(
          translate('failedToCheckDataServices'),
          error
        );
      }
    };
    runAsync(handleCheck());
  }, [checkHasReferences, closeDialog, translate, runAsync]);

  return canRemove ? (
    <ErrorBoundary
      FallbackComponent={DialogFallbackComponent}
      onReset={closeDialog}
    >
      <RemoveEntryDialog
        {...rest}
        onRemove={handleRemove}
        closeDialog={closeDialog}
      />
    </ErrorBoundary>
  ) : null;
};

RemoveDataServiceDialog.propTypes = {
  entry: PropTypes.instanceOf(Entry),
  closeDialog: PropTypes.func,
};

export default RemoveDataServiceDialog;
