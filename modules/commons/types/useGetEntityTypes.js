import { useEffect } from 'react';
import Lookup from 'commons/types/Lookup';
import { Entry } from '@entryscape/entrystore-js';
import useAsync from 'commons/hooks/useAsync';

/**
 *
 * @param {Entry} entry
 * @returns {Promise<object[]>}
 */
export const loadEntityTypeOptions = (entry) => {
  return Lookup.options(entry).then(({ primary, secondary }) => {
    return [primary, ...secondary];
  });
};

const useGetEntityTypes = (entry) => {
  const { runAsync, data: entityTypes, status } = useAsync({ data: [] });
  const isLoading = status === 'idle' || status === 'pending';

  useEffect(() => {
    runAsync(loadEntityTypeOptions(entry));
  }, [entry, runAsync]);

  return { entityTypes, status, isLoading };
};

export default useGetEntityTypes;
