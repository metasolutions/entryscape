import { createTheme } from '@mui/material/styles';
import {
  primary,
  canvas,
  lightGray,
  mediumGray,
  darkGray,
  info,
  success,
  error,
  warning,
  white,
  accent,
} from 'commons/theme/mui/colors';

const theme = createTheme({
  palette: {
    primary: { main: primary.main },
    secondary: { main: darkGray.main },
    error: { main: error.main },
    success: { main: success.main },
    warning: { main: warning.main },
    info: { main: info.main },
    accent: { main: accent.main },
  },

  typography: {
    fontFamily: [
      'Source Sans Pro',
      'Helvetica Neue',
      'Arial',
      'sans-serif',
    ].join(','),

    h1: { fontWeight: 600, fontSize: '28px', color: darkGray.main },
    h2: { fontWeight: 600, fontSize: '21px', color: darkGray.main },
    h3: { fontWeight: 600, fontSize: '18px', color: darkGray.main },
    h4: { fontWeight: 600, fontSize: '16px', color: darkGray.main },
    h5: { fontWeight: 600, color: darkGray.main },
    h6: { fontWeight: 600, color: darkGray.main },
    body1: {
      fontSize: '16px',
      fontWeight: 400,
      color: darkGray.main,
    },
    body2: { fontSize: '14px' },
    button: {
      fontSize: '16px', // ! button icons need to be larger
      fontWeight: 600,
      whiteSpace: 'nowrap',
    },
    // remaining Typography elements:
    // subtitle1
    // subtitle2
    // caption
    // overline
  },
  components: {
    MuiAccordion: {
      styleOverrides: {
        root: {
          '&:before': {
            height: '0px',
          },
        },
      },
    },
    MuiAccordionSummary: {
      styleOverrides: {
        root: {
          '&$expanded': {
            minHeight: 'unset',
          },
        },
      },
    },
    MuiAvatar: {
      styleOverrides: {
        colorDefault: {
          backgroundColor: darkGray.main,
        },
      },
    },
    MuiBackdrop: {
      styleOverrides: {
        root: {
          backgroundColor: 'rgba(223, 228, 230, 0.8)', // lightGray.main in rgba
        },
        invisible: {
          backgroundColor: 'transparent',
        },
      },
    },
    MuiButton: {
      defaultProps: {
        color: 'primary',
        variant: 'contained',
        disableElevation: true,
      },
      styleOverrides: {
        root: {
          textTransform: 'none',
          height: '48px',
        },
      },
    },
    MuiCardContent: {
      styleOverrides: {
        root: {
          '&:last-child': {
            paddingBottom: '16px',
          },
        },
      },
    },
    MuiDataGrid: {
      styleOverrides: {
        cell: {
          '&:hover': {
            outline: `1px solid ${mediumGray.main}`,
            outlineOffset: '-1px',
            cursor: 'pointer',
          },
        },
      },
    },
    MuiDialog: {
      defaultProps: { maxWidth: 'md' },
      styleOverrides: {
        paper: {
          background: canvas.main,
        },
      },
    },
    MuiDialogActions: {
      styleOverrides: {
        root: {
          height: '72px',
          padding: '12px',
        },
      },
    },
    MuiDialogContent: {
      styleOverrides: {
        root: {
          padding: '24px 6%',
        },
        dividers: {
          padding: '24px 6%',
        },
      },
    },
    MuiToggleButton: {
      styleOverrides: {
        root: ({ ownerState }) => ({
          ...(ownerState.color === 'standard' ? { color: darkGray.main } : {}),
        }),
      },
    },
    MuiFilledInput: {
      styleOverrides: {
        root: {
          color: darkGray.main,
          backgroundColor: mediumGray.light,
          '&:hover': { backgroundColor: mediumGray.light },
          '&$focused': { backgroundColor: mediumGray.lightest },
        },
        underline: {
          '&:before': { borderBottomColor: mediumGray.main },
        },
      },
    },
    MuiFormControl: {
      defaultProps: { margin: 'normal' },
      styleOverrides: {},
    },
    MuiFormControlLabel: {
      styleOverrides: {
        root: {
          color: darkGray.main,
        },
      },
    },
    MuiIcon: {
      styleOverrides: {
        root: ({ ownerState }) => ({
          ...(ownerState.color === 'inherit' ? { color: darkGray.main } : {}),
        }),
      },
    },
    MuiIconButton: {
      styleOverrides: {
        root: ({ ownerState }) => ({
          ...(ownerState.color === 'default' ? { color: darkGray.main } : {}),
        }),
      },
    },
    MuiInputLabel: {
      styleOverrides: {
        root: {
          color: darkGray.main,
        },
      },
    },
    MuiLink: {
      styleOverrides: {
        root: {
          textDecoration: 'none',
        },
      },
    },
    MuiListItem: {
      styleOverrides: {
        button: {
          '&.Mui-selected': {
            backgroundColor: lightGray.light,
            '&:hover': {
              backgroundColor: lightGray.light,
            },
          },
        },
      },
    },
    MuiMenu: {
      defaultProps: {
        BackdropProps: {
          invisible: true,
        },
      },
    },
    MuiMenuItem: {
      styleOverrides: {
        root: {
          '&.Mui-selected': {
            backgroundColor: lightGray.light,
            '&.Mui-focusVisible': {
              backgroundColor: lightGray.light,
            },
            '&:hover': {
              backgroundColor: lightGray.light,
            },
          },
        },
      },
    },
    MuiSnackbarContent: {
      styleOverrides: {
        root: {
          backgroundColor: info.main,
          fontSize: 'unset',
          padding: '4px 16px',
          boxShadow: '0px 6px 10px #00000029',
        },
      },
    },
    MuiSvgIcon: {
      styleOverrides: {
        root: {
          fontSize: '24px',
        },
      },
    },
    MuiTab: {
      styleOverrides: { textColorPrimary: { color: darkGray.main } },
    },
    MuiTableCell: {
      styleOverrides: {
        head: {
          fontSize: 16,
          fontWeight: 600,
          color: darkGray.main,
        },
        body: { color: darkGray.main },
        root: {
          border: `1px solid ${lightGray.light}`,
          color: darkGray.main,
        },
        sizeSmall: {
          paddingTop: 11,
          paddingBottom: 11,
        },
      },
    },
    MuiTableContainer: {
      styleOverrides: {
        root: {
          marginBottom: 36,
        },
      },
    },
    MuiTextField: {
      defaultProps: {
        variant: 'filled',
        margin: 'normal',
        fullWidth: true,
        autoComplete: 'off',
      },
    },
    MuiTooltip: {
      styleOverrides: {
        tooltip: {
          backgroundColor: darkGray.main,
          fontSize: 14,
          borderRadius: '4px',
          text: {
            color: white.main,
          },
        },
      },
    },
  },
});

export default theme;
