import { useLocation } from 'react-router-dom';
import { useEffect } from 'react';
import { useClipboardGraph } from 'commons/contexts/ClipboardGraphContext';

// Clears the clipboard graph on exiting the toolkit module.
const withLocationChangeListener =
  (Component) =>
  ({ ...props }) => {
    const location = useLocation();
    const [, setClipboardGraph] = useClipboardGraph();

    useEffect(() => {
      if (location.pathname.includes('toolkit')) return;
      setClipboardGraph('');
    }, [location, setClipboardGraph]);

    return <Component {...props} />;
  };

export default withLocationChangeListener;
