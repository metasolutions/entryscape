import useNavigate from 'commons/components/router/useNavigate';
import PropTypes from 'prop-types';
import { useEffect } from 'react';
import { CheckCircle as CheckCircleIcon } from '@mui/icons-material';
import { useClipboardGraph } from 'commons/contexts/ClipboardGraphContext';
import { getActiveModules } from 'commons/util/config';
import { useTranslation } from 'commons/hooks/useTranslation';
import useAsync from 'commons/hooks/useAsync';
import esrePipelineResult from 'harvest/nls/esrePipelineResult.nls';
import ExpandableListItem from 'commons/components/common/ExpandableListItem';
import { Graph, converters } from '@entryscape/rdfjson';
import { entrystore } from 'commons/store';
import { success } from 'commons/theme/mui/colors';
import { Entry, terms } from '@entryscape/entrystore-js';
import Report from './Report';
import './HarvestReport.scss';

const toolkitAvailable = () =>
  // not sending actual userinfo since toolkit is never restricted to a specific user
  getActiveModules({}).find((module) => module.name === 'toolkit');

/**
 * Get source data from the job entry. The file entry's resource can be either
 * json (usually) or xml.
 *
 * @param {Entry} jobEntry
 * @returns {Promise<Graph>}
 */
const getSourceData = async (jobEntry) => {
  const fileEntryURI = jobEntry
    .getMetadata()
    .findFirstValue(null, 'dcterms:source');

  if (!fileEntryURI) return Promise.resolve(null);

  const resource = await entrystore
    .getEntry(fileEntryURI)
    .then((fileEntry) => fileEntry.getResource(true));
  const data = await resource.getJSON().then(async (jsonData) => {
    if (jsonData) return jsonData;

    const xmlData = await resource.getXML();
    if (xmlData) return xmlData;

    console.error('No data on file entry ', fileEntryURI);
  });

  const results = await converters.detect(data);
  return results?.graph;
};

const HarvestReport = ({ pipelineData, jobEntry }) => {
  const { navigate } = useNavigate();
  const translate = useTranslation(esrePipelineResult);
  const [, setClipboardGraph] = useClipboardGraph();
  const { data: sourceData, runAsync } = useAsync();

  const { id, title, date, body, status } = pipelineData;
  const { validationResults = {}, mergeResultsByType = {} } = body;
  const isDcat = !Object.keys(mergeResultsByType).length;
  const isPending =
    status === terms.status.InProgress || status === terms.status.Pending;

  useEffect(() => {
    runAsync(getSourceData(jobEntry));
  }, [jobEntry, runAsync]);

  const showValidationButton = Boolean(
    toolkitAvailable() &&
      status !== terms.status.Failed &&
      Object.keys(validationResults).length > 0 &&
      sourceData
  );

  const showValidationReport = () => {
    setClipboardGraph(sourceData);
    navigate('/toolkit/validate');
  };

  const getTitle = () => {
    if (typeof title !== 'number') return translate(title);
    return isDcat
      ? translate('titleDatasets__dcat', title)
      : translate('titleDatasets', title);
  };

  return (
    <ExpandableListItem
      primary={getTitle()}
      secondary={
        <div className="esreHarvestReport__BlockTime">
          {status === terms.status.Succeeded && (
            <CheckCircleIcon style={{ color: success.main }} />
          )}
          <div className="esreHarvestReport__Time">
            {translate('modifiedDateTitle', date)}
          </div>
        </div>
      }
      key={id}
    >
      <div className="esreHarvestReport">
        {isPending ? (
          <div className="esreHarvestReportCardBlock">
            {translate('noData')}
          </div>
        ) : (
          <Report
            body={body}
            showValidationButton={showValidationButton}
            showValidationReport={showValidationReport}
          />
        )}
      </div>
    </ExpandableListItem>
  );
};

HarvestReport.propTypes = {
  pipelineData: PropTypes.shape({
    title: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    date: PropTypes.string.isRequired,
    body: PropTypes.oneOfType([PropTypes.shape({}), PropTypes.string])
      .isRequired,
    status: PropTypes.string,
    id: PropTypes.string,
  }).isRequired,
  jobEntry: PropTypes.shape(),
};

export default HarvestReport;
