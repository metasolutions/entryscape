import PropTypes from 'prop-types';
import { useState, useEffect } from 'react';
import { Entry } from '@entryscape/entrystore-js';
import { useTranslation } from 'commons/hooks/useTranslation';
import esteConceptNLS from 'terms/nls/esteConcept.nls';
import { Edit as EditIcon } from '@mui/icons-material';
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  Button,
  IconButton,
  Stack,
} from '@mui/material';
import { validate } from '@entryscape/rdforms';
import useRDFormsEditor from 'commons/components/rdforms/hooks/useRDFormsEditor';
import useLevelProfile from 'commons/components/rdforms/hooks/useLevelProfile';
import LevelSelector from 'commons/components/rdforms/LevelSelector';
import {
  RdformsDialogFormWrapper,
  RdformsStickyDialogContent,
  RdformsDialogContent,
} from 'commons/components/rdforms/RdformsDialogFormWrapper';
import DialogTwoColumnLayout, {
  PrimaryColumn,
  SecondaryColumn,
} from 'commons/components/common/dialogs/DialogTwoColumnLayout';
import RdformsOutline from 'commons/components/rdforms/RdformsOutline';
// eslint-disable-next-line max-len
import useRdformsOutlineSearch from 'commons/components/rdforms/RdformsOutline/useRdformsOutlineSearch';
import { useFormErrorMessage } from 'commons/components/rdforms/hooks/useRDFormsValidation';
import Tooltip from 'commons/components/common/Tooltip';
import RdformsAlert from 'commons/components/rdforms/RdformsAlert';
import ProfileChooser from 'commons/types/ProfileChooser';
import useProfileChooserState from 'commons/types/useProfileChooserState';
import config from 'config';
import { useSnackbar, SUCCESS_EDIT } from 'commons/hooks/useSnackbar';
import LoadingButton from 'commons/components/LoadingButton';
import useAsync, { PENDING } from 'commons/hooks/useAsync';
import { ErrorCatcher } from 'commons/errors/ErrorCatcher';
import { ErrorWithMessage } from 'commons/errors/utils/errors';
import { useConfirmCloseAction } from 'commons/components/EntryListView/dialogs/create/handlers';

const EditConceptDialog = ({ conceptEntry, parentEntry, onEdit }) => {
  const translate = useTranslation(esteConceptNLS);
  const [buttonDisabled, setButtonDisabled] = useState(true);
  const {
    entityTypes,
    templateId,
    selectedEntityType,
    saveEntityType,
    onChangeEntityType,
  } = useProfileChooserState({
    entry: conceptEntry,
    parentEntry,
    onChangeCallback: (isModified) => setButtonDisabled(!isModified),
    fallbackTemplateId: config.get('terms.conceptTemplateId'),
  });
  const [level, setLevel] = useState('mandatory');
  const [open, setOpen] = useState(false);
  const { EditorComponent, graph, editor } = useRDFormsEditor({
    entry: conceptEntry,
    templateId,
    includeLevel: level,
    open,
  });
  const disabledLevels = useLevelProfile(templateId);
  const getFormErrorMessage = useFormErrorMessage();
  const [formError, setFormError] = useState(null);
  const [errors, setErrors] = useState([]);
  const outlineSearchProps = useRdformsOutlineSearch();
  const showProfileChooser = entityTypes.length > 1 && selectedEntityType;
  const [addSnackbar] = useSnackbar();
  const { runAsync, status, error: editError } = useAsync();

  useEffect(() => {
    if (graph) {
      graph.onChange = () => {
        setButtonDisabled(false);
      };
    }
  }, [graph]);

  const closeDialog = () => {
    setButtonDisabled(true);
    setOpen(false);
  };
  const confirmClose = useConfirmCloseAction(closeDialog);
  const close = () => confirmClose(!buttonDisabled);

  const handleSaveAction = async () => {
    const { errors: validationErrors } = validate.bindingReport(editor.binding);
    if (validationErrors.length > 0) {
      const errorMessage = getFormErrorMessage(validationErrors);
      setFormError(errorMessage);
      setErrors(validationErrors);
      outlineSearchProps.clearSearch();
      return;
    }

    return runAsync(
      onEdit(graph)
        .then(saveEntityType)
        .then(closeDialog)
        .then(() => addSnackbar({ type: SUCCESS_EDIT }))
        .catch((error) => {
          throw new ErrorWithMessage(translate('editConceptFail'), error);
        })
    );
  };

  return (
    <>
      <Tooltip title={translate('editConcept')}>
        <IconButton
          onClick={() => setOpen(true)}
          aria-label={translate('editConcept')}
        >
          <EditIcon />
        </IconButton>
      </Tooltip>
      <Dialog
        open={open}
        onClose={close}
        fullWidth
        maxWidth="lg"
        PaperProps={{ id: 'edit-concept-dialog' }}
      >
        <DialogTitle>{translate('editConcept')}</DialogTitle>
        <DialogContent dividers>
          <DialogTwoColumnLayout>
            <SecondaryColumn>
              {editor ? (
                <RdformsOutline
                  editor={editor}
                  root="edit-concept-dialog"
                  errors={errors}
                  searchProps={outlineSearchProps}
                />
              ) : null}
            </SecondaryColumn>
            <PrimaryColumn>
              {EditorComponent ? (
                <RdformsDialogFormWrapper>
                  <RdformsStickyDialogContent>
                    <Stack direction="row" spacing={2}>
                      <LevelSelector
                        level={level}
                        onUpdateLevel={setLevel}
                        disabledLevels={disabledLevels}
                      />
                      {showProfileChooser ? (
                        <ProfileChooser
                          entityTypes={entityTypes}
                          selectedEntityType={selectedEntityType}
                          onChangeEntityType={onChangeEntityType}
                        />
                      ) : null}
                    </Stack>
                  </RdformsStickyDialogContent>
                  <RdformsDialogContent>
                    <EditorComponent />
                  </RdformsDialogContent>
                </RdformsDialogFormWrapper>
              ) : null}
            </PrimaryColumn>
          </DialogTwoColumnLayout>
          <RdformsAlert message={formError} setMessage={setFormError} />
        </DialogContent>
        <DialogActions>
          <Button onClick={close} variant="text">
            {translate('cancel')}
          </Button>
          <LoadingButton
            disabled={buttonDisabled}
            color="primary"
            onClick={handleSaveAction}
            loading={status === PENDING}
            autoFocus
          >
            {translate('saveButton')}
          </LoadingButton>
        </DialogActions>
      </Dialog>
      <ErrorCatcher error={editError} />
    </>
  );
};

EditConceptDialog.propTypes = {
  conceptEntry: PropTypes.instanceOf(Entry),
  parentEntry: PropTypes.instanceOf(Entry),
  onEdit: PropTypes.func,
};

export default EditConceptDialog;
