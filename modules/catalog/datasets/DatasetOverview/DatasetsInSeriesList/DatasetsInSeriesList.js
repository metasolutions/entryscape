import { useCallback } from 'react';
import { Edit as EditIcon, Info as InfoIcon } from '@mui/icons-material';
import { DATASET_OVERVIEW_NAME } from 'catalog/config/config';
import config from 'config';
import {
  ActionsMenu,
  List,
  ListItem,
  ListItemText,
  ListView,
  useSolrSearch,
  withListModelProvider,
  ListItemActionIconButton,
  ListItemOpenMenuButton,
  ListPagination,
  useListModel,
  withListRefresh,
  REFRESH,
} from 'commons/components/ListView';
import { ListDraftStatusIcon } from 'commons/components/EntryListView';
import EntryListItemText from 'commons/components/EntryListView/listItems/EntryListItemText';
import {
  LIST_ACTION_EDIT,
  LIST_ACTION_REVISIONS,
} from 'commons/components/EntryListView/actions';
import {
  ACTION_REMOVE,
  getIconFromActionId,
  SearchAndCreateIcon,
} from 'commons/actions';
import { useTranslation } from 'commons/hooks/useTranslation';
import escaDatasetNLS from 'catalog/nls/escaDataset.nls';
import escoListNLS from 'commons/nls/escoList.nls';
import { entrystore } from 'commons/store';
import { getPathFromViewName } from 'commons/util/site';
import {
  RDF_TYPE_DATASET,
  RDF_TYPE_DATASET_SERIES,
  canWriteMetadata,
} from 'commons/util/entry';
import { getShortModifiedDate } from 'commons/util/metadata';
import { useESContext } from 'commons/hooks/useESContext';
import { useEntry } from 'commons/hooks/useEntry';
import { OverviewListHeader } from 'commons/components/overview/OverviewListHeader';
import { OverviewListPlaceholder } from 'commons/components/overview';
import OverviewLinkIcon from 'commons/components/OverviewLinkIcon';
// eslint-disable-next-line max-len
import DatasetLinkedDataBrowserDialog from 'catalog/datasetTemplates/DatasetLinkedDataBrowserDialog';
import DisconnectDialog from 'commons/components/EntityOverview/dialogs/DisconnectDialog';
import { SERIES_TAG } from 'catalog/datasets/tags';
import DatasetSearchOrCreateDialog from '../DatasetSearchOrCreateDialog';
import RemoveDatasetDialog from '../dialogs/RemoveDatasetDialog/RemoveDatasetDialog';

const ROWS_PER_PAGE = 5;
const NLS_BUNDLES = [escoListNLS, escaDatasetNLS];

const DatasetsInSeriesList = () => {
  const translate = useTranslation(NLS_BUNDLES);
  const seriesEntry = useEntry();
  const { context } = useESContext();
  const [, dispatch] = useListModel();
  const refreshDatasets = () => dispatch({ type: REFRESH });

  const includeSubSeries = config.get('catalog.includeSubSeries');

  const createQuery = useCallback(
    () =>
      entrystore
        .newSolrQuery()
        .rdfType(
          includeSubSeries
            ? [RDF_TYPE_DATASET, RDF_TYPE_DATASET_SERIES]
            : RDF_TYPE_DATASET
        )
        .uriProperty('dcat:inSeries', seriesEntry.getResourceURI())
        .context(context),
    [context, seriesEntry, includeSubSeries]
  );
  const {
    entries: datasetEntries,
    size,
    search,
    status,
  } = useSolrSearch({ createQuery });

  const actions = [
    {
      id: 'disconnect',
      Dialog: DisconnectDialog,
      isVisible: ({ entry }) => canWriteMetadata(entry),
      labelNlsKey: 'disconnectMenuItemLabel',
    },
    LIST_ACTION_REVISIONS,
    {
      ...ACTION_REMOVE,
      isVisible: ({ entry }) => !entry.isPublic(),
      Dialog: withListRefresh(RemoveDatasetDialog, 'onRemove'),
      removeConfirmMessage: translate('removeDatasetQuestion'),
      stopNavigation: true,
    },
  ];

  return (
    <>
      <OverviewListHeader
        heading={translate('seriesOverviewDatasets')}
        nlsBundles={NLS_BUNDLES}
        action={{
          Dialog: DatasetSearchOrCreateDialog,
          tooltip: includeSubSeries
            ? translate('addOrCreateRelationTitleWithSeries')
            : translate('addOrCreateRelationTitle'),
        }}
        actionIcon={<SearchAndCreateIcon />}
        actionParams={{
          refreshDatasets,
          numberOfDatasets: datasetEntries.length,
        }}
      />
      <ListView
        size={size}
        search={search}
        nlsBundles={NLS_BUNDLES}
        status={status}
        renderPlaceholder={() => (
          <OverviewListPlaceholder
            label={translate('relationsListPlaceholder')}
          />
        )}
      >
        <List renderLoader={(Loader) => <Loader height="10vh" />}>
          {datasetEntries.map((datasetEntry) => {
            const to = getPathFromViewName(DATASET_OVERVIEW_NAME, {
              contextId: datasetEntry.getContext().getId(),
              entryId: datasetEntry.getId(),
            });

            const actionsMenuItems = actions
              .filter(({ isVisible }) =>
                isVisible ? isVisible({ entry: datasetEntry }) : true
              )
              .map((action) => ({
                label: translate(action.labelNlsKey),
                icon: getIconFromActionId(action.id),
                action: {
                  ...action,
                  refresh: refreshDatasets,
                  nlsBundles: NLS_BUNDLES,
                  // dataset as parent for the disconnect dialog
                  parentEntry: datasetEntry,
                  entry: seriesEntry,
                  relation: { property: 'dcat:inSeries' },
                  // for remove dataset dialog
                  isPublished: datasetEntry.isPublic(),
                  datasetEntry,
                },
              }));

            return (
              <ListItem key={datasetEntry.getId()} to={to}>
                <ListDraftStatusIcon
                  entry={datasetEntry}
                  tooltipNlsKey="draftTooltip"
                />
                <EntryListItemText
                  xs={6}
                  tags={SERIES_TAG}
                  entry={datasetEntry}
                  translate={translate}
                />
                <ListItemText
                  xs={2}
                  secondary={getShortModifiedDate(datasetEntry)}
                />
                <ListItemActionIconButton
                  title={translate('infoEntry')}
                  action={{
                    Dialog: DatasetLinkedDataBrowserDialog,
                    entry: datasetEntry,
                  }}
                  xs={1}
                >
                  <InfoIcon />
                </ListItemActionIconButton>
                <ListItemActionIconButton
                  title={translate('editEntry')}
                  action={{
                    ...LIST_ACTION_EDIT,
                    entry: datasetEntry,
                  }}
                  xs={1}
                >
                  <EditIcon />
                </ListItemActionIconButton>
                <ListItemOpenMenuButton xs={1} />
                <ActionsMenu items={actionsMenuItems} />
                <OverviewLinkIcon to={to} />
              </ListItem>
            );
          })}
        </List>
        {size > ROWS_PER_PAGE ? <ListPagination /> : null}
      </ListView>
    </>
  );
};

export default withListModelProvider(DatasetsInSeriesList, () => ({
  limit: ROWS_PER_PAGE,
}));
