import PropTypes from 'prop-types';
import { Entry } from '@entryscape/entrystore-js';
import Lookup from 'commons/types/Lookup';
import {
  Button,
  FormLabel,
  FormControl,
  FormGroup,
  FormControlLabel,
  Checkbox,
  Typography,
} from '@mui/material';
import Tooltip from 'commons/components/common/Tooltip';
import ConditionalWrapper from 'commons/components/common/ConditionalWrapper';
import ContentWrapper from 'commons/components/common/ContentWrapper';
import { useState, useEffect, useCallback } from 'react';
import eswoConfigureEntityTypesNLS from 'workbench/nls/eswoConfigureEntityTypes.nls';
import {
  updateSelectedEntityTypes,
  getEntityURIFromName,
  getSelectedEntityTypeURIs,
  getAvailableEntityTypes,
  getEntitytypeCountQuery,
} from 'workbench/utils/entitytypes';
import { localize } from 'commons/locale';
import { useTranslation } from 'commons/hooks/useTranslation';
import { useUserState } from 'commons/hooks/useUser';
import ListActionDialog from 'commons/components/common/dialogs/ListActionDialog';
import useAsync from 'commons/hooks/useAsync';
import ProjectTypeChooser from 'commons/types/ProjectTypeChooser';
import { ErrorCatcher } from 'commons/errors/ErrorCatcher';
import { ErrorWithMessage } from 'commons/errors/utils/errors';
import './ConfigureEntityTypes.scss';

// Configures entity types in two ways:
// 1. Select/change project type. Specific entity types can't be selected.
// 2. Select specific entity types, when no project type is selected.
const ConfigureEntityTypesDialog = ({ entry, closeDialog, onChange }) => {
  const { userInfo } = useUserState();
  const [selectedEntityTypesURIs, setSelectedEntityTypesURIs] = useState([]);
  const [hasSomeSelected, setHasSomeSelected] = useState(true);
  const [selectedProjectTypeId, setSelectedProjectTypeId] = useState('');
  const [entityTypeItems, setEntityTypeItems] = useState([]);
  const translate = useTranslation(eswoConfigureEntityTypesNLS);
  const [configureError, setConfigureError] = useState();

  const { runAsync: runProjectTypeInContext, data: savedProjectTypeId } =
    useAsync({
      data: '',
    });

  const updateEntityTypeItems = useCallback(
    async (projectTypeId) => {
      const entityTypes = getAvailableEntityTypes(projectTypeId, userInfo);
      const entitytypeCountQueries = entityTypes.map((entityType) =>
        getEntitytypeCountQuery(entry, entityType)
      );
      const entitytypeCounts = await Promise.all(entitytypeCountQueries);
      const entityTypeItemsWithCount = entityTypes.map((entityType, id) => ({
        entityType,
        count: entitytypeCounts[id] || 0,
      }));
      setEntityTypeItems(entityTypeItemsWithCount);
    },
    [entry, userInfo]
  );

  /**
   * Get selected entity types. Only applicable if no project type, since entity
   * types can't be selected in this case.
   */
  useEffect(() => {
    if (!entityTypeItems.length) return;

    // get already saved entitytypes
    const selectedEntityTypeURIs = getSelectedEntityTypeURIs(entry);
    setSelectedEntityTypesURIs(
      selectedEntityTypeURIs ||
        entityTypeItems.map(({ entityType }) =>
          getEntityURIFromName(entityType.get('name'))
        )
    );
  }, [entry, entityTypeItems]);

  /**
   * Check for saved project type in context. Then set available entity
   * types.
   */
  useEffect(() => {
    if (!entry) return;
    runProjectTypeInContext(
      Lookup.getProjectTypeInUse(entry.getResource(true)).then(
        async (projectType) => {
          const projectTypeId = projectType?.getId() || '';
          if (projectTypeId) {
            setSelectedProjectTypeId(projectTypeId);
          }
          await updateEntityTypeItems(projectTypeId);
          return projectTypeId;
        }
      )
    );
  }, [entry, runProjectTypeInContext, userInfo, updateEntityTypeItems]);

  const handleEntityTypeSelect = (event) => {
    // update state
    const { checked, name } = event.target;
    const entityTypeURI = getEntityURIFromName(name);
    const entityTypeState = checked
      ? [...selectedEntityTypesURIs, entityTypeURI]
      : selectedEntityTypesURIs.filter((uri) => uri !== entityTypeURI);
    setSelectedEntityTypesURIs(entityTypeState);
    setHasSomeSelected(entityTypeState.length);
  };

  const onChangeProjectType = async (projectTypeId) => {
    setSelectedProjectTypeId(projectTypeId);
    await updateEntityTypeItems(projectTypeId);
  };

  const handleSaveAction = () => {
    updateSelectedEntityTypes(
      entry,
      selectedEntityTypesURIs,
      selectedProjectTypeId
    )
      .then(() => {
        closeDialog();
        onChange();
      })
      .catch((error) => {
        setConfigureError(
          new ErrorWithMessage(translate('entityTypeConfigureFail'), error)
        );
      });
  };

  const actions = (
    <Button autoFocus disabled={!hasSomeSelected} onClick={handleSaveAction}>
      {translate('entityTypesFooter')}
    </Button>
  );

  return (
    <>
      <ListActionDialog
        closeDialog={closeDialog}
        id="configure-entitytypes"
        title={translate('entityTypesTitle')}
        actions={actions}
        maxWidth="sm"
      >
        <ContentWrapper xs={10}>
          <ProjectTypeChooser
            onChangeProjectType={onChangeProjectType}
            selectedId={selectedProjectTypeId}
            userInfo={userInfo}
          />
          {selectedProjectTypeId !== savedProjectTypeId ? (
            <Typography aria-live="polite" variant="body1">
              {translate('projectTypeChangeWarning')}
            </Typography>
          ) : null}
          <FormControl component="fieldset">
            <FormLabel focused={false} component="legend">
              {selectedProjectTypeId
                ? translate('entityTypesFromProjectTypeHeader')
                : translate('entityTypesHeader')}
            </FormLabel>
            <FormGroup>
              {entityTypeItems.map(({ entityType, count }) => {
                const { name, label, id } = entityType.get();
                return (
                  <ConditionalWrapper
                    key={`${id}`}
                    condition={Boolean(selectedProjectTypeId)}
                    wrapper={(children) => (
                      <Tooltip title={translate('projectTypeTooltip')}>
                        {children}
                      </Tooltip>
                    )}
                  >
                    <FormControlLabel
                      control={
                        <Checkbox
                          checked={
                            selectedEntityTypesURIs.includes(
                              getEntityURIFromName(name)
                            ) || Boolean(selectedProjectTypeId)
                          }
                          disabled={Boolean(selectedProjectTypeId)}
                          onChange={handleEntityTypeSelect}
                          name={getEntityURIFromName(name)}
                        />
                      }
                      label={`${localize(label)} (${count} ${translate(
                        'entry',
                        count
                      )})`}
                    />
                  </ConditionalWrapper>
                );
              })}
            </FormGroup>
          </FormControl>
        </ContentWrapper>
      </ListActionDialog>
      <ErrorCatcher error={configureError} />{' '}
    </>
  );
};

ConfigureEntityTypesDialog.propTypes = {
  entry: PropTypes.instanceOf(Entry),
  closeDialog: PropTypes.func,
  onChange: PropTypes.func,
};

export default ConfigureEntityTypesDialog;
