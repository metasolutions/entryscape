import { Entry, Context } from '@entryscape/entrystore-js';
import {
  constructURIFromPattern,
  getValueFromURIByPattern,
  spreadEntry,
  shouldUseUriPattern,
} from 'commons/util/store';
import { entrystore } from 'commons/store';
import Lookup from 'commons/types/Lookup';
import { refreshEntry } from 'commons/util/entry';

/**
 * @typedef {object} URIUpdateItem
 * @property {Entry} entry - entry that needs to update resourceURI
 * @property {string} resourceURI - the current resourceURI.
 * @property {string} newResourceURI - the new resourceURI to change to.
 */

/**
 *
 * @param {Entry} contextEntry
 * @param {string} contextName
 * @returns {Promise<Context>}
 */
export const updateContextName = async (contextEntry, contextName) => {
  const context = entrystore.getContextById(contextEntry.getId());
  await context.setName(contextName);
  return context;
};

/**
 * Update the void:uriSpace in metadata
 *
 * @param {Entry} contextEntry
 * @param {string} contextName
 * @param {string} uriPattern
 * @returns {Promise<Entry>}
 */
export const updateURISpace = async (contextEntry, contextName, uriPattern) => {
  await refreshEntry(contextEntry);
  const { ruri, metadata } = spreadEntry(contextEntry);
  const uriSpace = constructURIFromPattern(uriPattern, {
    contextName,
  });
  metadata.findAndRemove(ruri, 'void:uriSpace', null);
  metadata.add(ruri, 'void:uriSpace', uriSpace);
  return contextEntry.commitMetadata();
};

/**
 * Get all entries in context that needs to update the resource uri. Extract the
 * old and new resourceURI to help update in next step.
 *
 * @param {Context} context
 * @param {string} contextName
 * @returns {Promise<URIUpdateItem[]>}
 */
export const getItemsToUpdate = async (context, contextName) => {
  const entries = await entrystore
    .newSolrQuery()
    .context(context)
    .list()
    .getAllEntries();

  const items = [];
  entries.forEach((entry) => {
    const { metadata, ruri: resourceURI } = spreadEntry(entry);
    const rdfType = metadata.findFirstValue(null, 'rdf:type');
    const entityType = Lookup.getEntityTypeByRdfType(rdfType, 'workbench');
    if (!shouldUseUriPattern(entityType)) return;
    const entryName = getValueFromURIByPattern(
      resourceURI,
      entityType.get('uriPattern'),
      'entryName'
    );
    const newResourceURI = constructURIFromPattern(
      entityType.get('uriPattern'),
      {
        contextName,
        entryName,
      }
    );
    if (!newResourceURI) return;
    items.push({
      entry,
      resourceURI,
      newResourceURI,
    });
  });
  return items;
};

/**
 * Update the resource uri based on the pattern and new project name for all
 * entries in the project/context. Also update the metadata of any entry (in
 * context) that link to an entry for which the resource URI will change.
 *
 * @param {URIUpdateItem[]} items
 * @returns {Promise[]}
 */
export const updateResourceURIs = (items) => {
  return items.map(async ({ entry, newResourceURI }) => {
    const { info: entryInfo } = spreadEntry(entry);
    entryInfo.setResourceURI(newResourceURI);
    await entryInfo.commit();
    await refreshEntry(entry);

    // update uris for all references
    items.forEach(
      ({
        resourceURI: linkedResourceURI,
        newResourceURI: newLinkedResourceURI,
      }) => {
        const metadata = entry.getMetadata();
        const [statement] = metadata.find(
          newResourceURI,
          null,
          linkedResourceURI
        );
        if (statement) {
          metadata.replaceURI(linkedResourceURI, newLinkedResourceURI);
        }
      }
    );

    return entry.commitMetadata();
  });
};
