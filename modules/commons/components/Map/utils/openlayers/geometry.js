import { Point, Polygon } from 'ol/geom';
import { fromExtent } from 'ol/geom/Polygon';

/**
 * Wrapper function to create point
 * @param {Coordinate} coordinate
 * @returns {Point}
 */
export const createPoint = (coordinate) => {
  return new Point(coordinate);
};

/**
 * Wrapper function to create polygon
 * @param {Coordinate[]} coordinate
 * @returns {Polygon}
 */
export const createPolygon = (coordinates) => {
  return new Polygon(coordinates);
};

/**
 * Creates polygon from coordinate extent
 * @param {Coordinate[]} extent
 * @returns {Polygon}
 */
export const createPolygonFromExtent = (extent) => {
  return fromExtent(extent);
};
