import PropTypes from 'prop-types';
import Link from 'commons/components/router/RouterLink';
import { Grid, ListItemText as MuiListItemText } from '@mui/material';
import { truncateLabel } from 'commons/util/util';
import Tooltip from 'commons/components/common/Tooltip';
import './ListItemText.scss';

// List item component with different text styling for
// primary and secondary props.
// Navigation can be enabled by the to prop.
export const ListItemText = ({
  primary,
  secondary,
  tags = null,
  direction = 'column',
  to,
  xs = 3,
  xl = false,
  classes,
  maxPrimaryLength = 70,
  maxSecondaryLength = 70,
}) => {
  const isColumn = direction === 'column';
  const [primaryText, tooltipText] = truncateLabel(primary, maxPrimaryLength);
  const [secondaryText] = truncateLabel(secondary, maxSecondaryLength);

  return (
    <Grid
      item
      xs={xs}
      xl={xl}
      component={to ? Link : null}
      to={to}
      className="escoListViewListItemTextGrid"
    >
      <Tooltip title={tooltipText}>
        <MuiListItemText
          classes={{ root: 'escoListViewListItemText', ...classes }}
          style={{
            flexDirection: direction,
            [isColumn ? 'justifyContent' : 'alignItems']: 'center',
          }}
          primary={primaryText}
          primaryTypographyProps={{
            classes: {
              root: 'escoListViewListItemText__primary',
            },
          }}
          secondary={secondaryText}
          secondaryTypographyProps={{
            classes: {
              root: isColumn
                ? 'escoListViewListItemText__secondary'
                : 'escoListViewListItemText__secondary--margin-left',
            },
          }}
        />
      </Tooltip>
      {tags}
    </Grid>
  );
};

ListItemText.propTypes = {
  direction: PropTypes.string,
  primary: PropTypes.string,
  secondary: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  tags: PropTypes.node,
  to: PropTypes.string,
  xs: PropTypes.oneOfType([PropTypes.number, PropTypes.bool]),
  xl: PropTypes.oneOfType([PropTypes.number, PropTypes.bool]),
  maxPrimaryLength: PropTypes.number,
  maxSecondaryLength: PropTypes.number,
  classes: PropTypes.shape({}),
};
