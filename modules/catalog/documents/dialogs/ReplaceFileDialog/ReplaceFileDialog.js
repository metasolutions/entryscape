import PropTypes from 'prop-types';
import { Button } from '@mui/material';
import { Presenter } from 'commons/components/forms/presenters';
import { Entry } from '@entryscape/entrystore-js';
import ContentWrapper from 'commons/components/common/ContentWrapper';
import ListActionDialog from 'commons/components/common/dialogs/ListActionDialog';
import { useTranslation } from 'commons/hooks/useTranslation';
import escaDocumentsNLS from 'catalog/nls/escaDocuments.nls';
import useLinkOrFile from 'commons/components/entry/Import/useLinkOrFile';
import FileUpload from 'commons/components/common/FileUpload';
import { refreshEntry } from 'commons/util/entry';

const ReplaceFileDialog = ({ entry, closeDialog, onSave }) => {
  const translate = useTranslation([escaDocumentsNLS]);
  const { file, handleFileChange } = useLinkOrFile();

  const handleSaveForm = async () => {
    await entry.getResource(true).putFile(file);
    await refreshEntry(entry);

    onSave();
    closeDialog();
  };

  const dialogActions = (
    <Button autoFocus onClick={handleSaveForm} disabled={!file}>
      {translate('replaceFileFooterButton')}
    </Button>
  );

  return (
    <ListActionDialog
      id="replace-document-file"
      title={translate('replaceFileHeader')}
      actions={dialogActions}
      closeDialog={closeDialog}
      maxWidth="sm"
    >
      <Presenter
        label={translate('currentFileLabel')}
        getValue={() =>
          entry.getEntryInfo()?.getLabel() || translate('fileNameNotFound')
        }
      />
      <ContentWrapper md={12}>
        <FileUpload onSelectFile={handleFileChange} />
      </ContentWrapper>
    </ListActionDialog>
  );
};

ReplaceFileDialog.propTypes = {
  entry: PropTypes.instanceOf(Entry).isRequired,
  closeDialog: PropTypes.func.isRequired,
  onSave: PropTypes.func,
};

export default ReplaceFileDialog;
