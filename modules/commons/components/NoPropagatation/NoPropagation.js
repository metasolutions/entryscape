import PropTypes from 'prop-types';
import { Box } from '@mui/material';

const NoPropagation = ({ children, ...props }) => {
  const stopPropagation = (evt) => evt.stopPropagation();
  return (
    // eslint-disable-next-line react/jsx-props-no-spreading
    <Box onClick={stopPropagation} {...props}>
      {children}
    </Box>
  );
};
NoPropagation.propTypes = {
  children: PropTypes.node,
};

export default NoPropagation;
