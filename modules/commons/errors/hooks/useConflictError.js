import useGetMainDialog from 'commons/hooks/useGetMainDialog';
import { useTranslation } from 'commons/hooks/useTranslation';
import { refreshEntry } from 'commons/util/entry';
import escoRdformsNLS from 'commons/nls/escoRdforms.nls';

const useConflictError = ({ entry, closeDialog }) => {
  const translate = useTranslation(escoRdformsNLS);
  const { getConfirmationDialog } = useGetMainDialog();

  const handleConflictError = async (error) => {
    if (error.response?.status !== 412) return;
    const confirmConflict = async () => {
      const proceed = await getConfirmationDialog({
        rejectLabel: translate('metadataConflictCancel'),
        affirmLabel: translate('metadataConflictLoadChanges'),
        content: translate('metadataConflictMessage'),
      });
      if (proceed) {
        await refreshEntry(entry);
        closeDialog();
      }
    };

    confirmConflict();
  };

  return { handleConflictError };
};

export default useConflictError;
