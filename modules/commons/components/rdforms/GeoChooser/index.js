import { renderingContext } from '@entryscape/rdforms';
import GeoEditor from './GeoEditor';

const geoChooser = ({ node, ...geoEditorProps }) => {
  const element = <GeoEditor key="geo-editor-map" {...geoEditorProps} />;
  node.appendChild(element);
};

renderingContext.editorRegistry
  .datatype('http://www.opengis.net/ont/geosparql#wktLiteral')
  .register((node, binding, context) =>
    geoChooser({ node, binding, context, editable: true })
  );
renderingContext.presenterRegistry
  .datatype('http://www.opengis.net/ont/geosparql#wktLiteral')
  .register((node, binding, context) =>
    geoChooser({ node, binding, context, editable: false })
  );
renderingContext.editorRegistry
  .style('geoPoint')
  .register((node, binding, context) =>
    geoChooser({ node, binding, context, editable: true })
  );
renderingContext.presenterRegistry
  .style('geoPoint')
  .register((node, binding, context) =>
    geoChooser({ node, binding, context, editable: false })
  );
