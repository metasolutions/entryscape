import React, { useRef, useEffect } from 'react';
import { entryPropType } from 'commons/util/entry';
import { Box, CircularProgress } from '@mui/material';
import { useESContext } from 'commons/hooks/useESContext';
import useAsync, { RESOLVED } from 'commons/hooks/useAsync';
import useErrorHandler from 'commons/errors/hooks/useErrorHandler';
import { useTranslation } from 'commons/hooks/useTranslation';
import esmoDiagramsNLS from 'models/nls/esmoDiagrams.nls';
import esmoCommonsNLS from 'models/nls/esmoCommons.nls';
import Placeholder from 'commons/components/common/Placeholder';
import { useDiagramUtils } from '../hooks/useDiagramUtils';
import '../DiagramView/DiagramView.scss';
import { DiagramAdapter } from '../utils/diagram';

const Diagram = ({ diagramEntry }) => {
  const translate = useTranslation([esmoDiagramsNLS, esmoCommonsNLS]);
  const { context } = useESContext();
  const {
    data,
    status: diagramStatus,
    isLoading,
    runAsync,
    error,
  } = useAsync();
  const {
    diagramUtils,
    status: chunkLoadingStatus,
    error: chunkLoadingError,
  } = useDiagramUtils();
  const canvasRef = useRef();

  useErrorHandler(error || chunkLoadingError);

  useEffect(() => {
    if (!diagramEntry || chunkLoadingStatus !== RESOLVED) return;
    const canvas = canvasRef.current;
    const newDiagramAdapter = new DiagramAdapter({
      diagramUtils,
      canvas,
      context,
      diagramEntry,
      translate,
    });
    runAsync(newDiagramAdapter.init());
  }, [
    diagramEntry,
    runAsync,
    diagramUtils,
    chunkLoadingStatus,
    translate,
    context,
  ]);

  const showPlaceholder = diagramStatus === RESOLVED && !data;
  return (
    <>
      {!showPlaceholder ? (
        <div className="esmoDiagram">
          <div className="esmoDiagram__canvas" ref={canvasRef} />
          {isLoading ? (
            <Box className="esmoDiagram__progress">
              <CircularProgress />
            </Box>
          ) : null}
        </div>
      ) : (
        <Placeholder label={translate('diagramPlaceholder')} />
      )}
    </>
  );
};

Diagram.propTypes = {
  diagramEntry: entryPropType,
};

export default Diagram;
