import {
  FIELD_TYPE_CHOICES,
  PROPERTY_FORM_CHOICE,
  SECTION_FORM_CHOICE,
} from 'models/utils/choiceDefinitions';
import { TYPE_FILTER as TYPE_DEFAULT_FILTER } from 'models/utils/filterDefinitions';
import { ANY_FILTER_ITEM } from 'commons/components/filters/utils/filterDefinitions';

const FORM_ITEM_TYPE_CHOICES = [
  ...FIELD_TYPE_CHOICES,
  SECTION_FORM_CHOICE,
  PROPERTY_FORM_CHOICE,
];

export const TYPE_FILTER = {
  ...TYPE_DEFAULT_FILTER,
  applyFilterParams: (query, filter) => {
    const value = filter.getSelected();
    if (value) return query.rdfType(value);
    return query;
  },
  items: [ANY_FILTER_ITEM, ...FORM_ITEM_TYPE_CHOICES],
};
