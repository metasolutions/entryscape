import { getQueryFromItem } from 'commons/components/rdforms/EntryChooser/queryFromItem';
import getChoiceFromEntry from 'commons/components/rdforms/EntryChooser/getChoiceFromEntry';
import { getESContext } from 'commons/hooks/useESContext';
import { getRdformsContext } from 'commons/hooks/useRdformsContext';

const searchChoice = (binding, term) => {
  const context = getESContext() || getRdformsContext();

  // note that all choices should be shown when there's a current selection
  const [query] = getQueryFromItem(
    binding.getItem(),
    binding.getChoice()?.label === term ? '' : term,
    context
  );
  return query
    .getEntries()
    .then((entries) => entries.map((entry) => getChoiceFromEntry(entry)));
};

export default searchChoice;
