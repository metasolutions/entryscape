import { Component as ReactComponent } from 'react';
import PropTypes from 'prop-types';
import { ErrorWithMessage } from '../utils/errors';

class ErrorBoundary extends ReactComponent {
  constructor(props) {
    super(props);
    this.state = { error: null };

    if (!props?.FallbackComponent) {
      throw new Error(
        'No fallback component was provided for the error boundary'
      );
    }
  }

  componentDidCatch(error, info) {
    const { onError } = this.props;

    if (onError) {
      onError();
    }
    const errorToLog =
      error instanceof ErrorWithMessage ? error.originalError : error;
    console.error('Error caught by ErrorBoundary:', errorToLog);
    console.error('Error info:', info.componentStack);
  }

  static getDerivedStateFromError(error) {
    return { error };
  }

  resetError = () => {
    const { onReset } = this.props;
    this.state = { error: null };

    if (onReset) {
      onReset();
    }
  };

  render() {
    const { error } = this.state;
    const { children } = this.props;

    if (!error) return children;

    return (
      <this.props.FallbackComponent resetError={this.resetError} error={error}>
        {children}
      </this.props.FallbackComponent>
    );
  }
}

ErrorBoundary.propTypes = {
  error: PropTypes.shape({}),
  FallbackComponent: PropTypes.func,
  fallbackProps: PropTypes.shape({}),
  renderChildrenOnError: PropTypes.bool,
  children: PropTypes.node,
};

export default ErrorBoundary;
