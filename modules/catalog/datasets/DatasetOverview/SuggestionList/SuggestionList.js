import { useEffect, useState, useCallback } from 'react';
import PropTypes from 'prop-types';
import {
  List,
  ListItem,
  ListItemText,
  ListView,
  useSolrSearch,
  withListModelProvider,
  ListItemActionIconButton,
  ListItemActionsGroup,
} from 'commons/components/ListView';
import { LIST_ACTION_INFO } from 'commons/components/EntryListView/actions';
import EntryListItemText from 'commons/components/EntryListView/listItems/EntryListItemText';
import { useTranslation } from 'commons/hooks/useTranslation';
import { nlsBundles } from 'catalog/suggestions/SuggestionsView/actions';
import { isArchived, getChecklistProgress } from 'catalog/suggestions/util';
import { Entry } from '@entryscape/entrystore-js';
import { entrystore } from 'commons/store';
import { RDF_TYPE_SUGGESTION } from 'commons/util/entry';
import { useESContext } from 'commons/hooks/useESContext';
import 'commons/components/progress/LinearProgressButton/LinearProgressButton.scss';
import LinearProgress from 'commons/components/progress/LinearProgress';
import { getModifiedDate } from 'commons/util/metadata';
import { getShortDate } from 'commons/util/date';
import escaPreparationsNLS from 'catalog/nls/escaPreparations.nls';
import escoListNLS from 'commons/nls/escoList.nls';
import { OverviewListHeader } from 'commons/components/overview/OverviewListHeader';
import LinkedDataBrowserDialog from 'commons/components/LinkedDataBrowserDialog';
import { getPathFromViewName } from 'commons/util/site';
import OverviewLinkIcon from 'commons/components/OverviewLinkIcon';
import { ARCHIVED_TAG } from 'catalog/suggestions/tags';
import { rowActions } from './actions';
import './SuggestionList.scss';

const SuggestionListItem = ({ entry, disabled }) => {
  const translate = useTranslation([escoListNLS, escaPreparationsNLS]);
  const [progress, setProgress] = useState({
    totalCount: 0,
    progressCount: 0,
    mandatoryTotalCount: 0,
    mandatoryProgressCount: 0,
  });

  useEffect(() => {
    setProgress(getChecklistProgress(entry));
  }, [entry]);

  return (
    <ListItem
      className="escaDatasetOverviewSuggestionList"
      key={entry.getId()}
      action={{
        ...rowActions[0],
        entry,
      }}
      to={getPathFromViewName('catalog__suggestions__suggestion', {
        contextId: entry.getContext().getId(),
        entryId: entry.getId(),
      })}
    >
      <EntryListItemText
        entry={entry}
        translate={translate}
        tags={ARCHIVED_TAG}
        xs={6}
      />
      <ListItemActionIconButton
        xs={3}
        disabled={disabled}
        action={{
          ...rowActions[1],
          entry,
        }}
        title={translate(rowActions[1].labelNlsKey)}
        classes={{
          root: `escoLinearProgressButton ${
            disabled ? 'escoLinearProgressButton--disabled' : ''
          }`,
        }}
      >
        <LinearProgress
          progress={progress}
          disabled={disabled}
          className="escaSuggestionList__linearProgress"
        />
      </ListItemActionIconButton>
      <ListItemText xs={2} secondary={getShortDate(getModifiedDate(entry))} />
      <ListItemActionsGroup
        xs={1}
        actions={[
          {
            ...LIST_ACTION_INFO,
            Dialog: LinkedDataBrowserDialog,
            getProps: () => ({
              entry,
              actionParams: [entry],
            }),
          },
        ]}
      />
      <OverviewLinkIcon isVisible />
    </ListItem>
  );
};

SuggestionListItem.propTypes = {
  entry: PropTypes.instanceOf(Entry),
  disabled: PropTypes.bool,
};

const SuggestionList = ({ entry: datasetEntry }) => {
  const translate = useTranslation(escaPreparationsNLS);
  const { context } = useESContext();
  const createQuery = useCallback(
    () =>
      entrystore
        .newSolrQuery()
        .rdfType(RDF_TYPE_SUGGESTION)
        .context(context)
        .uriProperty('dcterms:references', datasetEntry.getResourceURI()),
    [context, datasetEntry]
  );

  const { entries, size, search, status } = useSolrSearch({ createQuery });

  if (!entries?.length) return null;

  return (
    <>
      <OverviewListHeader
        heading={translate('suggestionListTitle')}
        nlsBundles={escaPreparationsNLS}
      />
      <ListView
        size={size}
        search={search}
        nlsBundles={nlsBundles}
        status={status}
      >
        <List
          renderLoader={(Loader) => (
            <Loader sx={{ height: 'auto !important' }} />
          )}
        >
          <div className="escaDatasetOverviewSuggestionList">
            {entries &&
              entries.map((suggestionEntry) => {
                return (
                  <SuggestionListItem
                    key={`${suggestionEntry.getId()}`}
                    entry={suggestionEntry}
                    disabled={isArchived(suggestionEntry)}
                  />
                );
              })}
          </div>
        </List>
      </ListView>
    </>
  );
};

SuggestionList.propTypes = {
  entry: PropTypes.instanceOf(Entry),
};

export default withListModelProvider(SuggestionList);
