/* eslint-disable react/jsx-props-no-spreading */
import PropTypes from 'prop-types';
import { useCallback, useMemo } from 'react';
import { entryPropType } from 'commons/util/entry';
import {
  ListActions,
  ListAction,
  List,
  ListItem,
  ListHeader,
  ListSearch,
  ListView,
  ListPagination,
  useListModel,
  useFilterListActions,
} from 'commons/components/ListView';
import { FILTER, CLEAR_FILTER } from 'commons/actions/actionTypes';
import {
  useTranslation,
  nlsBundlesPropType,
} from 'commons/hooks/useTranslation';
import config from 'config';
import SearchFilters from 'commons/components/filters';
import { getNoResultsNLS } from 'commons/components/filters/utils/filter';
import { REJECTED } from 'commons/hooks/useAsync';
import { ListHeaderItem } from './listItems/ListHeaderItem';
import { TableViewButton, RefreshButton, ToggleFiltersButton } from './buttons';

// High level component for creating list view with standard layout. If a
// customized layout is required, ListView components can be used for
// composition.¸
// The columns prop defines how each column should be rendered, including header
// and corresponding list items. Definitions for commonly used columns, such as
// modify date and title are available in the columns utility.
export const EntryListView = ({
  size,
  status,
  search,
  entries,
  items: providedItems,
  nlsBundles,
  columns,
  viewPlaceholderProps = {},
  renderViewPlaceholder,
  showViewPlaceholder = true,
  listPlaceholderProps = {},
  listRejectedPlaceholderProps = {},
  includeDefaultListActions = true,
  includeTableView = false,
  includeFilters,
  filtersProps,
  includeHeader = true,
  includePagination = true,
  rowsPerPageOptions,
  getListItemProps = () => ({}),
  listActions = [],
  listActionsProps = {},
  excludeActions = [],
  children,
}) => {
  const translate = useTranslation(nlsBundles);
  const [, dispatch] = useListModel();
  const handleFilterSelect = useCallback(
    (filter, value) => {
      if (filter) {
        dispatch({ type: FILTER, filter: { [filter.getId()]: value } });
        return;
      }
      dispatch({ type: CLEAR_FILTER });
    },
    [dispatch]
  );
  const hasSelectedFilter = filtersProps?.hasSelectedFilter();
  const items = useMemo(() => {
    if (providedItems) return providedItems;
    if (entries) return entries.map((entry) => ({ entry }));
    return [];
  }, [providedItems, entries]);

  // common list actions above list as well as empty list placeholder
  const actions = useFilterListActions({
    listActions,
    listActionsProps,
    excludeActions,
    nlsBundles,
  });

  const disableActions =
    size === -1 || listActionsProps.disabled || status === REJECTED;

  return (
    <ListView
      status={status}
      search={search}
      size={size}
      nlsBundles={nlsBundles}
      showPlaceholder={
        showViewPlaceholder ? !hasSelectedFilter : showViewPlaceholder
      }
      renderPlaceholder={
        renderViewPlaceholder ||
        ((Placeholder) => (
          <Placeholder {...viewPlaceholderProps}>
            {actions.length ? (
              <ListActions justifyContent="center" disabled={disableActions}>
                {actions.map(
                  ({ label, tooltip, disabled, icon, ...action }) => {
                    return (
                      <ListAction
                        key={action.id}
                        label={label}
                        tooltip={tooltip}
                        action={action}
                        disabled={disabled}
                        icon={icon}
                      />
                    );
                  }
                )}
              </ListActions>
            ) : null}
          </Placeholder>
        ))
      }
    >
      <ListActions disabled={disableActions}>
        {includeDefaultListActions ? (
          <>
            <ListSearch disabled={disableActions} />
            <ListActions compact>
              {includeFilters ? (
                <ToggleFiltersButton
                  active={hasSelectedFilter}
                  disabled={disableActions}
                />
              ) : null}
              {includeTableView && config.get('catalog.includeTableView') ? (
                <TableViewButton disabled={disableActions} />
              ) : null}
              <RefreshButton status={status} />
            </ListActions>
          </>
        ) : null}
        {actions.map(
          ({
            label,
            tooltip,
            icon,
            disabled,
            Component = ListAction,
            ...action
          }) => {
            return (
              <Component
                key={action.id}
                icon={icon}
                label={label}
                tooltip={tooltip}
                action={action}
                disabled={disabled}
              />
            );
          }
        )}
      </ListActions>
      {includeFilters ? (
        <SearchFilters
          disabled={size === -1 || listActionsProps.disabled}
          filterProps={filtersProps}
          nlsBundles={nlsBundles}
          onFilterSelect={handleFilterSelect}
        />
      ) : null}
      {children}
      <List
        renderPlaceholder={(Placeholder) => (
          <Placeholder
            label={translate(
              getNoResultsNLS(Boolean(search), hasSelectedFilter),
              search
            )}
            {...listPlaceholderProps}
          />
        )}
        renderRejectedPlaceholder={(Placeholder, defaultProps) => (
          <Placeholder {...defaultProps} {...listRejectedPlaceholderProps} />
        )}
      >
        {includeHeader ? (
          <ListHeader>
            {columns
              .filter((column) => column)
              .map((column) => (
                <ListHeaderItem
                  key={column.id}
                  translate={translate}
                  {...column}
                />
              ))}
          </ListHeader>
        ) : null}

        {items.map(({ entry, ...rest }) => {
          return (
            <ListItem
              key={`listitem-${entry.getContext().getId()}-${entry.getId()}`}
              {...getListItemProps({ entry, translate, ...rest })}
            >
              {columns
                .filter((column) => column)
                .map(
                  ({
                    Component,
                    getProps = () => ({}),
                    headerNlsKey: _,
                    ...column
                  }) => (
                    <Component
                      key={column.id}
                      {...(column.actions ? { excludeActions } : {})}
                      {...column}
                      {...getProps({ entry, translate, ...rest })}
                    />
                  )
                )}
            </ListItem>
          );
        })}
      </List>
      {includePagination ? (
        <ListPagination rowsPerPageOptions={rowsPerPageOptions} />
      ) : null}
    </ListView>
  );
};

EntryListView.propTypes = {
  size: PropTypes.number,
  status: PropTypes.string,
  search: PropTypes.string,
  entries: PropTypes.arrayOf(entryPropType),
  items: PropTypes.arrayOf(PropTypes.shape({ entry: entryPropType })),
  nlsBundles: nlsBundlesPropType,
  columns: PropTypes.arrayOf(PropTypes.shape({})),
  viewPlaceholderProps: PropTypes.shape({}),
  renderViewPlaceholder: PropTypes.func,
  showViewPlaceholder: PropTypes.bool,
  listPlaceholderProps: PropTypes.shape({}),
  listRejectedPlaceholderProps: PropTypes.shape({}),
  includeDefaultListActions: PropTypes.bool,
  includeTableView: PropTypes.bool,
  includeFilters: PropTypes.bool,
  filtersProps: PropTypes.shape({ hasSelectedFilter: PropTypes.func }),
  includeHeader: PropTypes.bool,
  includePagination: PropTypes.bool,
  rowsPerPageOptions: PropTypes.arrayOf(PropTypes.number),
  getListItemProps: PropTypes.func,
  listActions: PropTypes.arrayOf(PropTypes.shape({})),
  listActionsProps: PropTypes.shape({}),
  excludeActions: PropTypes.arrayOf(PropTypes.string),
  children: PropTypes.node,
};

export default EntryListView;
