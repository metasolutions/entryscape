import PropTypes from 'prop-types';
import { useReducer, useState } from 'react';
import {
  Button,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  Divider,
  Grid,
} from '@mui/material';
import esreHarvestNLS from 'harvest/nls/esreHarvest.nls';
import { useTranslation } from 'commons/hooks/useTranslation';
import { useUserState } from 'commons/hooks/useUser';
import { updatePipelineEntry } from 'harvest/util';
import useAsync, { PENDING } from 'commons/hooks/useAsync';
import LoadingButton from 'commons/components/LoadingButton';
import { reducer, validateForm } from '../../forms/PipelineFormModel';
import PipelineForm from '../../forms/PipelineForm';
import TransformsForm from '../../forms/TransformsForm';

const EditPipelineDialog = ({
  open,
  onClose,
  organizationData,
  pipelineEntry,
  tags,
  onSave,
}) => {
  const translate = useTranslation(esreHarvestNLS);
  const { userEntry, userInfo } = useUserState();
  const { runAsync, status } = useAsync();
  const [data, formDispatch] = useReducer(reducer, {
    ...organizationData,
    isPublic: Boolean(organizationData.psi || organizationData.orgId),
    errors: {},
  });
  const [hasChanged, setHasChanged] = useState(false);
  const isDcat = tags.includes('dcat');
  const isValid = validateForm(data, userEntry, isDcat);
  const saveEnabled = isValid && hasChanged;

  const handleSave = async () =>
    runAsync(
      updatePipelineEntry(
        pipelineEntry,
        userEntry,
        userInfo,
        data,
        translate('replaceUserErrorMessage'),
        tags
      )
        .then(onSave)
        .finally(onClose)
    );

  const dispatch = (...args) => {
    formDispatch(...args);
    setHasChanged(true);
  };

  return (
    <Dialog
      fullWidth
      maxWidth="md"
      aria-labelledby="edit-pipeline-dialog"
      open={open}
      onClose={onClose}
    >
      <DialogTitle id="edit-pipeline-dialog">
        {translate('ePHeader')}
      </DialogTitle>
      <Divider />

      <DialogContent>
        <Grid container justifyContent="center" alignItems="center">
          <Grid item xs={8}>
            <PipelineForm
              userEntry={userEntry}
              data={data}
              dispatch={dispatch}
              isDcat={isDcat}
            />
            {!isDcat ? (
              <TransformsForm
                transforms={data.transforms}
                dispatch={dispatch}
              />
            ) : null}
          </Grid>
        </Grid>
      </DialogContent>
      <Divider />

      <DialogActions>
        <Button autoFocus variant="text" onClick={onClose}>
          {translate('cancel')}
        </Button>
        <LoadingButton
          onClick={handleSave}
          loading={status === PENDING}
          disabled={!saveEnabled}
        >
          {translate('epEdit')}
        </LoadingButton>
      </DialogActions>
    </Dialog>
  );
};

EditPipelineDialog.propTypes = {
  open: PropTypes.bool,
  onClose: PropTypes.func,
  organizationData: PropTypes.shape({
    title: PropTypes.string,
    description: PropTypes.string,
    username: PropTypes.string,
    psi: PropTypes.string,
    orgId: PropTypes.string,
    sourceUrl: PropTypes.string,
  }).isRequired,
  pipelineEntry: PropTypes.shape({}).isRequired,
  tags: PropTypes.arrayOf(PropTypes.string),
  onSave: PropTypes.func,
};

export default EditPipelineDialog;
