import PropTypes from 'prop-types';
import { useEffect } from 'react';
import { getDescription } from 'commons/util/metadata';
import DatasetsList from 'catalog/search/list/datasets';
import { entrystoreUtil } from 'commons/store';
import { getDCATCatalogInContext } from 'commons/util/context';
import useAsync, { RESOLVED } from 'commons/hooks/useAsync';
import useNavigate from 'commons/components/router/useNavigate';
import { useTranslation } from 'commons/hooks/useTranslation';
import escaDatasetNLS from 'catalog/nls/escaDataset.nls';
import escaCatalogSearchNLS from 'catalog/nls/escaCatalogSearch.nls';
import NavigateBefore from '@mui/icons-material/NavigateBefore';
import { IconButton, Grid } from '@mui/material';
import CatalogInfo from './CatalogInfo';
import './index.scss';

const CatalogPresenter = ({ context }) => {
  const { goBack } = useNavigate();
  const translate = useTranslation([escaDatasetNLS, escaCatalogSearchNLS]);
  const { data: publisher, runAsync } = useAsync();
  const {
    data: catalogEntry,
    runAsync: runGetCatalogEntry,
    status: getCatalogEntryStatus,
  } = useAsync();

  useEffect(() => {
    if (!catalogEntry) return;

    const publisherResourceURI = catalogEntry
      .getAllMetadata()
      .findFirstValue(null, 'dcterms:publisher');
    if (!publisherResourceURI) return;

    runAsync(entrystoreUtil.getEntryByResourceURI(publisherResourceURI));
  }, [catalogEntry, context, runAsync]);

  useEffect(() => {
    if (!context) return;
    runGetCatalogEntry(getDCATCatalogInContext(context));
  }, [context, runGetCatalogEntry]);

  if (context && getCatalogEntryStatus !== RESOLVED) return null;

  return catalogEntry ? (
    <Grid
      container
      spacing={2}
      justifyContent="flex-start"
      wrap="nowrap"
      alignItems="flex-start"
      direction="row"
    >
      <Grid item xs={1}>
        <IconButton
          aria-label={translate('backTitle')}
          onClick={goBack}
          className="escaCatalogPresenter__backButton"
        >
          <NavigateBefore />
        </IconButton>
      </Grid>
      <Grid item container xs={11} direction="column">
        <CatalogInfo
          entry={catalogEntry}
          description={getDescription(catalogEntry)}
          publisher={publisher}
          nlsBundles={escaCatalogSearchNLS}
        />
        <Grid item className="escaCatalogPresenter__list">
          <DatasetsList showFilters={!context} context={context} />
        </Grid>
      </Grid>
    </Grid>
  ) : (
    <DatasetsList context={context} />
  );
};

CatalogPresenter.propTypes = {
  context: PropTypes.string,
};

export default CatalogPresenter;
