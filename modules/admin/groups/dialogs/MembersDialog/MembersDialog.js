import PropTypes from 'prop-types';
import { Entry } from '@entryscape/entrystore-js';
import ListActionDialog from 'commons/components/common/dialogs/ListActionDialog';
import ContentWrapper from 'commons/components/common/ContentWrapper';
import esadGroupNLS from 'admin/nls/esadGroup.nls';
import escoPlaceholder from 'commons/nls/escoPlaceholder.nls';
import { getRenderName } from 'commons/util/rdfUtils';
import { useTranslation } from 'commons/hooks/useTranslation';
import UsersList from 'commons/components/EntryListView/lists/UsersList';

const MembersDialog = ({ entry: groupEntry, closeDialog }) => {
  const translate = useTranslation([esadGroupNLS, escoPlaceholder]);

  return (
    <ListActionDialog
      id="members-dialog"
      title={translate('memberHeader', getRenderName(groupEntry))}
      closeDialog={closeDialog}
      closeDialogButtonLabel={translate('close')}
    >
      <ContentWrapper md={12}>
        <UsersList
          groupEntry={groupEntry}
          messages={{
            removeSuccessMessage: translate('removeSuccessMessage'),
            removeFailMessage: translate('removeFailMessage'),
          }}
          viewPlaceholderProps={{
            label: translate('emptyMessageMembers'),
          }}
        />
      </ContentWrapper>
    </ListActionDialog>
  );
};

MembersDialog.propTypes = {
  closeDialog: PropTypes.func.isRequired,
  entry: PropTypes.instanceOf(Entry),
};
export default MembersDialog;
