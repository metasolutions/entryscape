import { useCallback } from 'react';
import { Group as GroupIcon } from '@mui/icons-material';
import Lookup from 'commons/types/Lookup';
import TableView from 'commons/components/TableView';
import { useESContext } from 'commons/hooks/useESContext';
import { entrystore } from 'commons/store';
import {
  EntryListView,
  TITLE_COLUMN,
  TITLE_WITH_CONTEXT_COLUMN,
  MODIFIED_COLUMN,
  ACTIONS_GROUP_COLUMN,
  useSolrQuery,
  getContextEntriesCallback,
  getItemsCallback,
} from 'commons/components/EntryListView';
import {
  LIST_ACTION_EDIT,
  LIST_ACTION_INFO,
} from 'commons/components/EntryListView/actions';
import {
  useListModel,
  withListModelProviderAndLocation,
  TOGGLE_TABLEVIEW,
  ListItemIcon,
  listPropsPropType,
} from 'commons/components/ListView';
import { getPathFromViewName } from 'commons/util/site';
import useSearchFilters from 'commons/components/filters/hooks/useSearchFilters';
import {
  CONTEXT_CHECKBOX_FILTER,
  ANY_FILTER_ITEM,
} from 'commons/components/filters/utils/filterDefinitions';
import { getFullLengthLabel } from 'commons/util/rdfUtils';
import { getOverviewName } from 'commons/types/utils/entityType';
import { useTranslation } from 'commons/hooks/useTranslation';
import { listActions, nlsBundles } from './actions';

const rdfTypes = ['foaf:Agent', 'foaf:Person', 'foaf:Organization'];

const getAllContextsEnabled = () => {
  const entityType = Lookup.getByName('publisher');
  return Boolean(entityType?.get('allContexts'));
};

const PublishersView = ({ listProps }) => {
  const { context } = useESContext();
  const contextId = context.getId();
  const [{ showTableView }, dispatch] = useListModel();
  const translate = useTranslation(nlsBundles);

  const entityType = Lookup.getByName('publisher');
  const includeContextFilter = getAllContextsEnabled();
  const filters = includeContextFilter
    ? [
        {
          ...CONTEXT_CHECKBOX_FILTER,
          headerNlsKey: 'contextFilterHeaderCatalog',
          loadItems: () => [ANY_FILTER_ITEM, { value: contextId }],
          defaultValue: contextId,
          initialSelection: contextId,
        },
      ]
    : [];
  const { applyFilters, isLoading, hasFilters, ...filterProps } =
    useSearchFilters(filters);
  const createQuery = useCallback(() => {
    const query = entrystore.newSolrQuery().rdfType(rdfTypes);
    if (!includeContextFilter) query.context(context);
    return query;
  }, [context, includeContextFilter]);
  const { callbackResults: items = [], ...queryResults } = useSolrQuery({
    createQuery,
    applyFilters,
    callback: includeContextFilter
      ? getContextEntriesCallback
      : getItemsCallback,
  });

  const getTitleWithContextProps = ({ entry }) => {
    if (contextId === entry.getContext().getId()) {
      return { primary: getFullLengthLabel(entry) };
    }

    return {
      primary: getFullLengthLabel(entry),
      secondary: getFullLengthLabel(entry.getContext().getEntry(true)),
    };
  };

  const getCanAccessOverview = (entry) => {
    const overviewName = getOverviewName(entityType);
    return Boolean(overviewName && entry.canAdministerEntry());
  };

  return showTableView ? (
    <TableView
      setTableView={() => dispatch({ type: TOGGLE_TABLEVIEW })}
      rdfType={rdfTypes}
      templateId={entityType.complementaryTemplateId()}
    />
  ) : (
    <EntryListView
      {...queryResults}
      {...listProps}
      items={items}
      nlsBundles={nlsBundles}
      listActions={listActions}
      listActionsProps={{ nlsBundles }}
      getListItemProps={({ entry }) =>
        getCanAccessOverview(entry)
          ? {
              to: getPathFromViewName('catalog__publishers__publisher', {
                contextId: entry.getContext().getId(),
                entryId: entry.getId(),
              }),
            }
          : { tooltip: translate('noAccessTooltip') }
      }
      includeTableView
      includeFilters={hasFilters}
      filtersProps={hasFilters ? filterProps : null}
      columns={[
        {
          id: 'group',
          xs: 1,
          Component: ListItemIcon,
          icon: <GroupIcon color="primary" />,
        },
        includeContextFilter
          ? {
              ...TITLE_WITH_CONTEXT_COLUMN,
              getProps: getTitleWithContextProps,
            }
          : {
              ...TITLE_COLUMN,
              xs: 8,
            },
        MODIFIED_COLUMN,
        {
          ...ACTIONS_GROUP_COLUMN,
          actions: [
            LIST_ACTION_INFO,
            {
              ...LIST_ACTION_EDIT,
              isVisible: ({ entry }) => entry.canAdministerEntry(),
            },
          ],
        },
      ]}
    />
  );
};

PublishersView.propTypes = {
  listProps: listPropsPropType,
};

export default withListModelProviderAndLocation(PublishersView, () => ({
  showFilters: getAllContextsEnabled(),
}));
