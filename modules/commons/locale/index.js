import { utils } from '@entryscape/rdforms';
import config from 'config';
import { i18n } from 'esi18n';

const nlsOverride = () => {
  // If there are NLS overrides, register them early.
  // if (config.locale.NLSOverridePath) {
  // } else
  if (config.get('locale.NLSOverrides')) {
    i18n.setOverrides(config.get('locale.NLSOverrides'));
  }
};

/**
 * @returns {string}
 */
export const getBestLanguage = (userLanguage = '') => {
  const fallbackLanguage = config.get('locale.fallback');
  const supportedLocales = config.get('locale.supported');
  const currentLanguage = userLanguage || i18n.getLocale();
  const idealLanguage = supportedLocales.find(
    (locale) => locale.lang === currentLanguage
  )?.lang;

  return idealLanguage || fallbackLanguage;
};

// TODO `.value` seems unsafe
export const localize = (lang2val) => utils.getLocalizedValue(lang2val).value;

/**
 *
 * @param {string|object} label
 * @returns {string|undefined}
 */
export const localizeLabel = (label) => {
  if (typeof label === 'string') return label;
  if (typeof label === 'object' && label !== null) return localize(label);
  return null;
};

// TODO make hook
export const locale = () => {
  // override nls
  // TODO check if still functional
  nlsOverride();
};

/**
 * Sorts the languages based on their label.
 * @param supportedLangNames
 * @returns {Array}
 */
export const sortLanguages = (supportedLangNames) => {
  const label2langNames = supportedLangNames.map((supportedLang) => ({
    lang: supportedLang,
    label: supportedLang.label.toLowerCase(),
  }));

  // sort by label
  label2langNames.sort((a, b) => {
    if (a.label < b.label) {
      return -1;
    }
    if (a.label > b.label) {
      return 1;
    }
    return 0;
  });

  return label2langNames.map((label2langName) => label2langName.lang);
};

/**
 *
 * @param {Object} userInfo
 * @returns {string[]}
 */
export const getClientAcceptLanguages = (userInfo = {}) => {
  const { clientAcceptLanguage } = userInfo;
  if (!clientAcceptLanguage) return [];
  const clientAcceptLanguages = Object.entries(clientAcceptLanguage)
    .sort(([, value1], [, value2]) => (value1 < value2 ? 1 : -1))
    .map(([language]) => language);
  return clientAcceptLanguages;
};

/**
 * @returns {string[]}
 */
export const getLocaleSupportedLanguages = () =>
  config.get('locale.supported')?.map((item) => item.lang) || [];

export default locale;
