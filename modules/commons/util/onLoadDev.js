/* eslint-disable */

/**
 * Legacy code that should be checked at some point
 */

var paramsToMap = function (params, arr, separator) {
  separator = separator || '&';
  var vars = params.split(separator);

  for (var i = 0; i < vars.length; i++) {
    var pair = vars[i].split('=');
    arr[pair[0]] = pair[1] || true;
  }
};

/**
 * get query params
 *
 * {String} queryParams
 * {Map} queryParamsMap
 */
window.hashParams = window.location.hash;
window.hashParamsMap = {};
window.queryParams = window.location.search;
window.queryParamsMap = {};

paramsToMap(hashParams.substring(1), hashParamsMap);
paramsToMap(queryParams.substring(1), queryParamsMap);

/** HACK redirect any hash params that look like
 * #view=datasets&resource=http://www.entryscape.com/someEntryURI
 * to /catalog/:context/datasets/:dataset
 */

if (hashParamsMap.view === 'dataset' && hashParamsMap.resource) {
  var resourceURI = hashParamsMap.resource;
  var entryPart = resourceURI.split('/store/');
  var earr = entryPart[1].split('/'); // TODO huge guess
  var context = earr[0];
  var resourceString = earr[1];
  var dataset = earr[2];
  location.href = '/catalog/' + context + '/datasets/' + dataset + queryParams;
}
