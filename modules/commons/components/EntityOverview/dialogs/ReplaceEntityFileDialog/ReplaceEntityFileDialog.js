import PropTypes from 'prop-types';
import { Button } from '@mui/material';
import { entryPropType } from 'commons/util/entry';
import ContentWrapper from 'commons/components/common/ContentWrapper';
import ListActionDialog from 'commons/components/common/dialogs/ListActionDialog';
import { useTranslation } from 'commons/hooks/useTranslation';
import eswoSpacesNLS from 'workbench/nls/eswoSpaces.nls';
import eswoReplaceDialogNLS from 'workbench/nls/eswoReplaceDialog.nls';
import useLinkOrFile from 'commons/components/entry/Import/useLinkOrFile';
import FileUpload from 'commons/components/common/FileUpload';
import { Presenter } from 'commons/components/forms/presenters';
import { withOverviewRefresh } from 'commons/components/overview';
import { useState } from 'react';
import { ErrorCatcher } from 'commons/errors/ErrorCatcher';
import { ErrorWithMessage } from 'commons/errors/utils/errors';

const ReplaceEntityFileDialog = ({ entry, closeDialog, onSuccess }) => {
  const { file, handleFileChange } = useLinkOrFile();
  const [fileError, setFileError] = useState();
  const translate = useTranslation([eswoSpacesNLS, eswoReplaceDialogNLS]);

  const handleSaveForm = async () => {
    try {
      await entry.getResource(true).putFile(file);

      entry.setRefreshNeeded();
      await entry.refresh();

      await Promise.resolve(onSuccess?.());
      closeDialog();
    } catch (error) {
      setFileError(new ErrorWithMessage(translate('saveFail'), error));
    }
  };

  const dialogActions = (
    <Button autoFocus onClick={handleSaveForm} disabled={!file}>
      {translate('replaceFileFooterButton')}
    </Button>
  );

  return (
    <>
      <ListActionDialog
        id="replace-entity-file"
        title={translate('replaceFileHeader')}
        actions={dialogActions}
        closeDialog={closeDialog}
        maxWidth="sm"
      >
        <Presenter
          label={translate('currentFileLabel')}
          getValue={() =>
            entry.getEntryInfo()?.getLabel() || translate('fileNameNotFound')
          }
        />
        <ContentWrapper md={12}>
          <FileUpload onSelectFile={handleFileChange} />
        </ContentWrapper>
      </ListActionDialog>
      <ErrorCatcher error={fileError} />
    </>
  );
};

ReplaceEntityFileDialog.propTypes = {
  entry: entryPropType,
  closeDialog: PropTypes.func,
  onSuccess: PropTypes.func,
};

export const OverviewReplaceEntityFileDialog = withOverviewRefresh(
  ReplaceEntityFileDialog,
  'onSuccess'
);

export default ReplaceEntityFileDialog;
