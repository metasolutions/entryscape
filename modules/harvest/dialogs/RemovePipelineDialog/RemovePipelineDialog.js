import PropTypes from 'prop-types';
import { Entry } from '@entryscape/entrystore-js';
import {
  useTranslation,
  nlsBundlesPropType,
} from 'commons/hooks/useTranslation';
import { useMainDialog } from 'commons/hooks/useMainDialog';
import { AcknowledgeAction } from 'commons/components/common/dialogs/MainDialog';
import ListRemoveEntryDialog from 'commons/components/EntryListView/dialogs/ListRemoveEntryDialog';
import { getGroupWithHomeContext } from 'commons/util/store';

const RemovePipelineDialog = ({
  entry,
  nlsBundles,
  closeDialog,
  ...restProps
}) => {
  const { openMainDialog } = useMainDialog();
  const translate = useTranslation(nlsBundles);

  const removePipelineRow = async (context, homeContext) => {
    try {
      const groupEntry = await getGroupWithHomeContext(context);
      if (!groupEntry) throw new Error(translate('removePipelineErrorMessage'));
      await homeContext.del();
      await groupEntry.del();
    } catch (error) {
      openMainDialog({
        content: error.message,
        actions: <AcknowledgeAction onDone={closeDialog} />,
      });
    }
  };
  const onRemove = async () => {
    const context = await entry.getContext();
    const homeContextEntry = await context.getEntry();
    await removePipelineRow(context, homeContextEntry);
  };

  return (
    <ListRemoveEntryDialog
      // eslint-disable-next-line react/jsx-props-no-spreading
      {...restProps}
      onRemove={onRemove}
      closeDialog={closeDialog}
      removeConfirmMessage={translate('confirmRemovePipeline')}
    />
  );
};

RemovePipelineDialog.propTypes = {
  entry: PropTypes.instanceOf(Entry),
  nlsBundles: nlsBundlesPropType,
  closeDialog: PropTypes.func,
};

export default RemovePipelineDialog;
