import { useEffect } from 'react';
import LinkedDataBrowserDialog from 'commons/components/LinkedDataBrowserDialog';
import { findModuleNameFromContext } from 'commons/util/module';
import { entryPropType } from 'commons/util/entry';
import useAsync from 'commons/hooks/useAsync';
import { CATALOG_MODULE, MODELS_MODULE } from 'commons/util/moduleDefinitions';
import ModelsLDBDialog from 'models/dialogs/ModelsLDBDialog';
import CatalogLDBDialog from 'catalog/dialogs/CatalogLDBDialog';

const MODULE_LDBD_MAP = {
  [MODELS_MODULE]: ModelsLDBDialog,
  [CATALOG_MODULE]: CatalogLDBDialog,
};

const SearchLDBDialog = (props) => {
  const { entry } = props;
  const { runAsync, data: LDBDialog, isLoading } = useAsync();

  useEffect(() => {
    const findLDBDialog = async () => {
      const moduleName = await findModuleNameFromContext(entry.getContext());
      return MODULE_LDBD_MAP[moduleName] || LinkedDataBrowserDialog;
    };
    runAsync(findLDBDialog());
  }, [runAsync, entry]);

  if (isLoading) return null;

  return <LDBDialog {...props} />;
};

SearchLDBDialog.propTypes = {
  entry: entryPropType,
};

export default SearchLDBDialog;
