const chartFontSize = 13;
const ticksFontSize = {
  ticks: {
    fontSize: chartFontSize,
  },
};
const chartOptions = {
  scales: {
    yAxes: [
      {
        ...ticksFontSize,
      },
    ],
    xAxes: [ticksFontSize],
  },
};

const timeChartBar = {
  legend: {
    labels: {
      fontSize: chartFontSize,
    },
  },
  scales: {
    yAxes: [ticksFontSize],
    xAxes: [
      {
        ...ticksFontSize,
        gridLines: {
          display: false,
        },
      },
    ],
  },
};

const pieChartOptions = {
  legend: {
    position: 'right',
    labels: {
      fontSize: chartFontSize,
    },
  },
};

const barChartOptions = {
  maintainAspectRatio: false,
  ...chartOptions,
};

export { timeChartBar, pieChartOptions, barChartOptions };
