import PropTypes from 'prop-types';
import { Link as LinkIcon } from '@mui/icons-material';
import { entryPropType } from 'commons/util/entry';
import {
  EntryListView,
  TITLE_COLUMN,
  MODIFIED_COLUMN,
  INFO_COLUMN,
} from 'commons/components/EntryListView';
import FieldsLinkedDataBrowserDialog from 'commons/components/FieldsLinkedDataBrowserDialog';
import { ACTION_INFO } from 'commons/actions';
import {
  ListItemIcon,
  withListModelProvider,
} from 'commons/components/ListView';
import { getLabel } from 'commons/util/rdfUtils';
import { getPurpose } from 'models/utils/metadata';
import {
  OverviewListPlaceholder,
  OverviewListHeader,
} from 'commons/components/overview';
import {
  nlsBundlesPropType,
  useTranslation,
} from 'commons/hooks/useTranslation';
import { getPathFromViewName } from 'commons/util/site';
import { FIELDS_FORMS_INFO } from 'models/utils/fieldDefinitions';
import { useUserState } from 'commons/hooks/useUser';
import { hasAdminRights } from 'commons/util/user';
import useReferenceList from './useReferenceList';

const ReferenceList = ({
  entry,
  nlsBundles,
  rdfType,
  headingNlsKey,
  placeholderProps,
  pathviewName,
}) => {
  const translate = useTranslation(nlsBundles);
  const { userEntry } = useUserState();

  const { entries, ...queryResults } = useReferenceList({ entry, rdfType });

  return (
    <div>
      <OverviewListHeader
        heading={translate(headingNlsKey)}
        warningText={
          hasAdminRights(userEntry) ? '' : translate('includedUsageWarning')
        }
      />
      <EntryListView
        {...queryResults}
        entries={entries}
        nlsBundles={nlsBundles}
        includeDefaultListActions={false}
        includeHeader={false}
        includePagination={entries?.length > 5}
        getListItemProps={({ entry: listEntry }) => ({
          to: getPathFromViewName(pathviewName, {
            contextId: listEntry.getContext().getId(),
            entryId: listEntry.getId(),
          }),
        })}
        renderViewPlaceholder={() => (
          <OverviewListPlaceholder
            label={
              placeholderProps.labelNlsKey
                ? translate(placeholderProps.labelNlsKey)
                : ''
            }
          />
        )}
        columns={[
          {
            ...TITLE_COLUMN,
            xs: 8,
            getProps: ({ entry: usedByEntry }) => ({
              primary: getLabel(usedByEntry),
              secondary: getPurpose({ entry: usedByEntry }),
            }),
          },
          {
            id: 'model-icon-column',
            xs: 1,
            Component: ListItemIcon,
            getProps: ({ entry: formEntry }) => ({
              ...(formEntry.getContext() === entry.getContext()
                ? {}
                : {
                    Icon: LinkIcon,
                    tooltip: translate('otherContextTooltip'),
                  }),
            }),
          },
          MODIFIED_COLUMN,
          {
            ...INFO_COLUMN,
            getProps: ({ entry: formEntry }) => ({
              action: {
                ...ACTION_INFO,
                entry: formEntry,
                Dialog: FieldsLinkedDataBrowserDialog,
                fields: FIELDS_FORMS_INFO,
                titleNlsKey: 'infoDialogTitle',
                nlsBundles,
              },
            }),
          },
        ]}
      />
    </div>
  );
};

ReferenceList.propTypes = {
  entry: entryPropType,
  nlsBundles: nlsBundlesPropType,
  rdfType: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.arrayOf(PropTypes.string),
  ]),
  headingNlsKey: PropTypes.string,
  placeholderProps: PropTypes.shape({ labelNlsKey: PropTypes.string }),
  pathviewName: PropTypes.string,
};

export default withListModelProvider(ReferenceList, () => ({ limit: 5 }));
