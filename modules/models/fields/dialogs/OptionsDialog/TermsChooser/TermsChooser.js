import PropTypes from 'prop-types';
import { Entry } from '@entryscape/entrystore-js';
import { Graph } from '@entryscape/rdfjson';
import { useState, useEffect, useMemo } from 'react';
import { entrystore } from 'commons/store';
import { Autocomplete, Box, TextField } from '@mui/material';
import { List as EmptyListIcon } from '@mui/icons-material';
import {
  ListActionButton,
  useListQuery,
  withListModelProvider,
} from 'commons/components/ListView';
import Alert from 'commons/components/Alert';
import { EntryListView, TITLE_COLUMN } from 'commons/components/EntryListView';
import { getIconFromActionId } from 'commons/actions';
import {
  nlsBundlesPropType,
  useTranslation,
} from 'commons/hooks/useTranslation';
import useGetMainDialog from 'commons/hooks/useGetMainDialog';
import { getFullLengthLabel, getLabel } from 'commons/util/rdfUtils';
import useAsync, { PENDING } from 'commons/hooks/useAsync';
import { RDF_TYPE_COLLECTION, RDF_TYPE_TERMINOLOGY } from 'commons/util/entry';
import esmoFieldsNLS from 'models/nls/esmoFields.nls';
import esmoCommonsNLS from 'models/nls/esmoCommons.nls';
import escoDialogsNLS from 'commons/nls/escoDialogs.nls';
import escoFormsNLS from 'commons/nls/escoForms.nls';
import escoListNLS from 'commons/nls/escoList.nls';
import Placeholder from 'commons/components/common/Placeholder';
import { useEditorContext } from 'commons/components/forms/editors';
import {
  RDF_PROPERTY_COLLECTION,
  RDF_PROPERTY_TERMINOLOGY,
} from 'models/utils/ns';
import {
  getConceptEntries,
  importConcepts,
  removeOptions,
} from 'models/fields/utils/options';
import { ErrorWithMessage } from 'commons/errors/utils/errors';
import { ErrorCatcher } from 'commons/errors/ErrorCatcher';
import './TermsChooser.scss';

const NLS_BUNDLES = [
  esmoFieldsNLS,
  esmoCommonsNLS,
  escoFormsNLS,
  escoDialogsNLS,
  escoListNLS,
];

const SEARCH_FIELDS = ['title'];

/**
 *
 * @param {Entry} conceptEntry
 * @returns {string}
 */
const getDescription = (conceptEntry) => {
  return getFullLengthLabel(conceptEntry, null, ['skos:definition']);
};

/**
 *
 * @param {string} terminologyURI
 * @returns {Promise<Entry[]>}
 */
const getCollections = async (terminologyURI) => {
  if (!terminologyURI) return [];
  const entry = await entrystore.getEntry(terminologyURI);
  return entrystore
    .newSolrQuery()
    .context(entry.getContext())
    .rdfType(RDF_TYPE_COLLECTION)
    .limit(100)
    .list()
    .getEntries();
};

/**
 *
 * @param {Graph} graph
 * @param {string} resourceURI
 * @param {string} property
 * @param {string} newURI
 */
const updateURI = (graph, resourceURI, property, newURI) => {
  graph.findAndRemove(resourceURI, property);
  if (newURI) {
    graph.add(resourceURI, property, newURI);
  }
};

/**
 *
 * @param {Graph} graph
 * @param {string} resourceURI
 * @param {string} newURI
 */
const updateTerminology = (graph, resourceURI, newURI) => {
  updateURI(graph, resourceURI, RDF_PROPERTY_TERMINOLOGY, newURI);
};

/**
 *
 * @param {Graph} graph
 * @param {string} resourceURI
 * @param {string} newURI
 */
const updateCollection = (graph, resourceURI, newURI) => {
  updateURI(graph, resourceURI, RDF_PROPERTY_COLLECTION, newURI);
};

/**
 * Get stored values for terminology and collection. Verify that the entries
 * haven't been removed. If removed an exception will be thrown.
 *
 * @param {Graph} graph
 * @param {string} resourceURI
 * @returns {Promise<object>}
 */
const getAndVerifyStoredSelections = async (graph, resourceURI) => {
  const termsURI =
    graph.findFirstValue(resourceURI, RDF_PROPERTY_TERMINOLOGY) || null;
  const collectionURI =
    graph.findFirstValue(resourceURI, RDF_PROPERTY_COLLECTION) || null;
  if (termsURI) {
    await entrystore.getEntry(termsURI); // verify terms entry
  }
  if (collectionURI) {
    await entrystore.getEntry(collectionURI); // verify collection entry
  }
  return { termsURI, collectionURI };
};

const AutocompleteAction = ({ id, label, loading = false, action = {} }) => {
  const translate = useTranslation(escoFormsNLS);
  const { entries, onSelectTerm, value } = action;
  const options = useMemo(() => {
    return entries?.length
      ? entries.map((entry) => ({
          label: getLabel(entry),
          value: entry.getURI(),
        }))
      : [];
  }, [entries]);

  const handleChange = (_event, newOption) => {
    onSelectTerm(newOption?.value || null);
  };

  return (
    <Autocomplete
      id={`options-${id}`}
      options={options}
      disablePortal
      loading={loading}
      loadingText={translate('loadingText')}
      getOptionKey={(option) => option.value}
      onChange={handleChange}
      value={options.find((option) => option.value === value) || null}
      renderInput={(params) => (
        <TextField
          {...params}
          label={label}
          className="escoSearchFilter__formControl"
        />
      )}
    />
  );
};

AutocompleteAction.propTypes = {
  id: PropTypes.string,
  label: PropTypes.string,
  loading: PropTypes.bool,
  action: PropTypes.shape({}),
};

const ConceptsList = withListModelProvider(({ conceptItems, status }) => {
  const { result: listItems, size } = useListQuery({
    items: conceptItems,
    searchFields: SEARCH_FIELDS,
  });

  return (
    <EntryListView
      entries={listItems.map(({ entry }) => entry)}
      size={size}
      status={status}
      nlsBundles={NLS_BUNDLES}
      columns={[
        {
          ...TITLE_COLUMN,
          xs: 12,
          maxSecondaryLength: 500,
          headerNlsKey: 'labelLabel',
          getProps: ({ entry }) => ({
            primary: getLabel(entry),
            secondary: getDescription(entry),
          }),
        },
      ]}
      showViewPlaceholder={false}
    />
  );
});

const TermsChooser = ({ nlsBundles, onChange, onImport }) => {
  const translate = useTranslation(nlsBundles);
  const [selectedTerm, setSelectedTerm] = useState(null);
  const [selectedCollection, setSelectedCollection] = useState(null);
  const { graph, resourceURI } = useEditorContext();
  const { runAsync: runGetInitialTerminology, error } = useAsync();
  const {
    data: terminologyEntries,
    runAsync,
    isPending: termsIsPending,
  } = useAsync({ data: [] });
  const {
    data: collectionEntries,
    runAsync: runGetCollections,
    isPending: collectionsIsPending,
  } = useAsync({ data: [] });
  const {
    data: conceptItems,
    runAsync: runGetConceptEntries,
    status: conceptStatus,
  } = useAsync({ data: [] });
  const { getConfirmationDialog } = useGetMainDialog();

  useEffect(() => {
    const getAndSetInitialSelections = async () => {
      const { termsURI, collectionURI } = await getAndVerifyStoredSelections(
        graph,
        resourceURI
      );
      setSelectedTerm(termsURI);
      setSelectedCollection(collectionURI);

      if (termsURI) {
        runGetCollections(getCollections(termsURI));
      }
    };
    runGetInitialTerminology(
      getAndSetInitialSelections().catch((originalError) => {
        throw new ErrorWithMessage(
          translate('getSavedTermError'),
          originalError
        );
      })
    );
  }, [
    graph,
    resourceURI,
    runGetInitialTerminology,
    runGetCollections,
    translate,
  ]);

  useEffect(() => {
    const getConceptItems = async () => {
      const entries = await getConceptEntries(selectedTerm, selectedCollection);
      return entries.map((entry) => ({
        entry,
        title: getLabel(entry),
      }));
    };
    runGetConceptEntries(getConceptItems(selectedTerm, selectedCollection));
  }, [selectedTerm, selectedCollection, runGetConceptEntries]);

  const handleSelectTerm = (newTermURI) => {
    setSelectedTerm(newTermURI);
    setSelectedCollection(null);
    updateTerminology(graph, resourceURI, newTermURI);
    onChange(true);
    runGetCollections(getCollections(newTermURI));
  };

  const handleSelectCollection = (newColectionURI) => {
    setSelectedCollection(newColectionURI);
    updateCollection(graph, resourceURI, newColectionURI);
    onChange(true);
  };

  useEffect(() => {
    runAsync(
      entrystore
        .newSolrQuery()
        .rdfType(RDF_TYPE_TERMINOLOGY)
        .limit(100)
        .list()
        .getEntries()
    );
  }, [runAsync]);

  const handleImport = async () => {
    const proceed = await getConfirmationDialog({
      content: translate('confirmImportOptions'),
      affirmLabel: translate('proceed'),
      rejectLabel: translate('cancel'),
    });
    if (!proceed) return;
    removeOptions(graph, resourceURI);
    const conceptEntries = conceptItems.map(({ entry }) => entry);
    importConcepts(graph, resourceURI, conceptEntries);
    onImport();
  };

  return (
    <>
      <div className="esmoTermsChooser__alert">
        <Alert severity="info">{translate('terminologyOptionsInfo')}</Alert>
      </div>
      <div className="esmoTermsChooser__actions">
        <AutocompleteAction
          id="select-term"
          label={translate('selectTermLabel')}
          loading={termsIsPending}
          action={{
            entries: terminologyEntries,
            onSelectTerm: handleSelectTerm,
            value: selectedTerm,
          }}
        />
        <AutocompleteAction
          id="select-collection"
          label={translate('selectCollectionLabel')}
          loading={termsIsPending || collectionsIsPending}
          action={{
            entries: collectionEntries,
            onSelectTerm: handleSelectCollection,
            value: selectedCollection,
          }}
        />
        <Box sx={{ marginLeft: 'auto' }}>
          <ListActionButton
            icon={getIconFromActionId('import')}
            label={translate('import')}
            tooltip={translate('importOptionTooltip')}
            onClick={handleImport}
            disabled={!selectedTerm}
          />
        </Box>
      </div>
      {selectedTerm ? (
        <ConceptsList
          conceptItems={conceptItems}
          status={
            termsIsPending || collectionsIsPending ? PENDING : conceptStatus
          }
        />
      ) : (
        <Placeholder
          variant="dialog"
          icon={<EmptyListIcon sx={{ fontSize: '60px' }} />}
          label={translate('optionConceptsPlaceholder')}
        />
      )}
      <ErrorCatcher error={error} />
    </>
  );
};

TermsChooser.propTypes = {
  nlsBundles: nlsBundlesPropType,
  onChange: PropTypes.func,
  onImport: PropTypes.func,
};

export default TermsChooser;
