/* eslint-disable max-len */
export const TERMINOLOGY_OVERVIEW_NAME = 'terminology__overview';
export const COLLECTION_OVERVIEW_NAME = 'terminology__collections__collection';
export const TERMINOLOGY_COLLECTIONS_LISTVIEW = 'terminology__collections';
export const TERMINOLOGY_CONCEPTS_VIEW_NAME = 'terminology__concepts';

export default {
  terms: {
    /**
     * @type {string}
     */
    conceptTemplateId: 'skosmos:concept',

    /**
     * empty string works as no limit
     *
     * @type {string|number}
     */
    schemeLimit: '',

    /**
     * empty string works as no limit
     *
     * @type {string|number}
     */
    conceptLimit: '',

    /**
     * A pointer to a text file in the configuration directory.
     *
     * @type {string}
     */
    conceptLimitDialog: '',

    /**
     * A pointer to a text file in the configuration directory.
     *
     * @type {string}
     */
    disallowTermCollaborationDialog: '',

    /**
     * A pointer to a text file in the configuration directory.
     *
     * @type {string}
     */
    disallowSchemePublishingDialog: '',

    /**
     * Select a publisher on terminology creation or not
     *
     * @type {boolean}
     */
    createWithoutPublisher: true,

    /**
     * If createWithoutPublisher is false then restrict selection of publishers from specific context
     *
     * @type {string}
     */
    publisherFromContext: '',

    /**
     * @type {boolean}
     */
    includeOverview: true,

    /**
     * @type {boolean}
     */
    includeCollections: true,

    /**
     * @type {string}
     */
    startView: TERMINOLOGY_OVERVIEW_NAME,

    /**
     * @type {object}
     */
    replace: null,

    /**
     * Helper text which appears at the top of the terminology creation dialog.
     * It's shown only if there a non empty string in the active locale.
     */
    createHelperText: {
      en: '',
      sv: '',
      de: '',
    },
  },
  itemstore: {
    /**
     * Classes that manage the listing/searching of values in rdforms selects.
     * The value of choosers is an array of strings,
     * the string refer to the filename in a specific path,
     * commons/rdforms/choosers
     *
     * @type {string[]}
     */
    choosers: ['SkosChooser'],
  },
  entrychooser: {
    'http://www.w3.org/2004/02/skos/core#related': 'context',
    'http://www.w3.org/2004/02/skos/core#exactMatch': 'notContext',
    'http://www.w3.org/2004/02/skos/core#closeMatch': 'notContext',
    'http://www.w3.org/2004/02/skos/core#relatedMatch': 'notContext',
    'http://www.w3.org/2004/02/skos/core#narrowMatch': 'notContext',
    'http://www.w3.org/2004/02/skos/core#broadMatch': 'notContext',
  },
  entitytypes: [
    {
      name: 'concept',
      label: {
        en: 'Concept',
      },
      /**
       * A constraints object for the entitytype, more flexible than using the rdfType configuration.
       * Providing a skos:inScheme with a value of null means it will match any value.
       * The null in skos:inScheme allows matching this entitytype against expressions in RDForms with explicit
       * restrictions to a specific terminology (by expressing a skos:inScheme to a specific concept scheme).
       *
       * @type {object}
       */
      constraints: {
        'skos:inScheme': null,
        'rdf:type': 'skos:Concept',
      },
      mainModule: 'terms',
      module: ['terms', 'catalog', 'workbench', 'models'],
      template: 'skosmos:concept',
      includeInternal: true,
      includeFile: false,
      includeLink: true,
      inlineCreation: false,
      faClass: 'cube',
      allContexts: true,
      listview: TERMINOLOGY_CONCEPTS_VIEW_NAME,
      overview: TERMINOLOGY_CONCEPTS_VIEW_NAME,
    },
    {
      name: 'conceptcollection',
      label: {
        en: 'Collection',
      },
      rdfType: ['http://www.w3.org/2004/02/skos/core#Collection'],
      mainModule: 'terms',
      module: 'terms',
      template: 'skosmos:conceptScheme',
      includeInternal: true,
      includeFile: false,
      includeLink: true,
      inlineCreation: false,
      faClass: 'cubes',
      listview: TERMINOLOGY_COLLECTIONS_LISTVIEW,
      overview: COLLECTION_OVERVIEW_NAME,
    },
    {
      name: 'conceptscheme',
      label: {
        en: 'Terminology',
      },
      rdfType: ['http://www.w3.org/2004/02/skos/core#ConceptScheme'],
      mainModule: 'terms',
      module: 'terms',
      template: 'skosmos:conceptScheme',
      includeInternal: true,
      includeFile: false,
      includeLink: true,
      inlineCreation: false,
      overview: TERMINOLOGY_OVERVIEW_NAME,
    },
  ],
};
