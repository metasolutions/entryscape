import PropTypes from 'prop-types';
import Table from '@mui/material/Table';
import TableHead from '@mui/material/TableHead';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import Typography from '@mui/material/Typography';
import { useTranslation } from 'commons/hooks/useTranslation';
import 'harvest/Statistics/tables/index.scss';

const Formats = ({ formatData, nlsBundle }) => {
  const formatDataKeysSortedByVal = Object.keys(formatData).sort(
    (a, b) => formatData[b] - formatData[a]
  );
  const t = useTranslation(nlsBundle);

  return (
    <div>
      {formatDataKeysSortedByVal.length > 0 && (
        <>
          <Typography
            variant="h6"
            component="h2"
            className="esreStatistics__header"
          >
            {t('formats')}
          </Typography>
          <TableContainer component={Paper}>
            <Table size="small" aria-label={t('formats')}>
              <TableHead>
                <TableRow>
                  <TableCell align="left" className="esreTable__th--bold">
                    {t('mediaType')}
                  </TableCell>
                  <TableCell className="esreTable__th--bold" align="center">
                    {t('timesUsed')}
                  </TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {formatDataKeysSortedByVal.map((key) => (
                  <TableRow hover key={key}>
                    <TableCell
                      className="esreTable__th--bold"
                      component="th"
                      scope="row"
                    >
                      {key}
                    </TableCell>
                    <TableCell align="center">{formatData[key]}</TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </>
      )}
    </div>
  );
};

Formats.propTypes = {
  formatData: PropTypes.shape(),
  nlsBundle: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
};

Formats.defaultProps = {
  formatData: {},
};
export default Formats;
