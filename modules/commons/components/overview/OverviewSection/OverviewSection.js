import PropTypes from 'prop-types';
import './OverviewSection.scss';

const OverViewSection = ({ children }) => {
  return <div className="escoOverviewSection">{children}</div>;
};

OverViewSection.propTypes = {
  children: PropTypes.node,
};

export default OverViewSection;
