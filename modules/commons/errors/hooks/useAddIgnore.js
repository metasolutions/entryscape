import { useEffect } from 'react';
import { addIgnore, removeIgnore } from 'commons/errors/utils/async';

const useAddIgnore = (callType, status, forNextRequest) => {
  useEffect(() => {
    addIgnore(callType, status, forNextRequest);
    return () => removeIgnore(callType);
  }, [callType, status, forNextRequest]);
};

export default useAddIgnore;
