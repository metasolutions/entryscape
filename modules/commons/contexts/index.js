import { createContext } from 'react';

/**
 * Manages the user in session. Root level context.
 * TODO add type
 */
const UserStateContext = createContext();

/**
 * Manages the dispatch actions for the user in session. Root level context.
 */
const UserDispatchContext = createContext();

/**
 * Manages the context (entry) of an context aware view
 */
const ESContextContext = createContext();

/**
 * Manages the  entry of an entry aware view
 */
const EntryContext = createContext();

/**
 * Manages the main alert dialog of the application.
 * @see commons/components/dialogs/MainDialog
 */
const MainDialogContext = createContext();

/**
 * Manages the page title (on the app bar).
 */
const PageTitleContext = createContext(['', () => {}]);

export {
  UserStateContext,
  UserDispatchContext,
  ESContextContext,
  EntryContext,
  MainDialogContext,
  PageTitleContext,
};
