import { STATUS_IDLE } from 'commons/hooks/useTasksList';

const TASKS_UPDATE_TERMINOLOGY_URI = [
  {
    id: 'conceptScheme',
    nlsKeyLabel: 'namespaceTaskUploadConceptScheme',
    status: STATUS_IDLE,
    message: '',
  },
  {
    id: 'concepts',
    nlsKeyLabel: 'namespaceTaskUploadConcepts',
    status: STATUS_IDLE,
    message: '',
  },
];

const TASKS_IMPORT_TERMINOLOGY = [
  {
    id: 'upload',
    nlsKeyLabel: 'uploadTask',
    status: STATUS_IDLE,
    message: '',
  },
  {
    id: 'analysis',
    nlsKeyLabel: 'analysisTask',
    status: STATUS_IDLE,
    message: '',
  },
  {
    id: 'import',
    nlsKeyLabel: 'importTask',
    status: STATUS_IDLE,
    message: '',
  },
];

const TASKS_UPDATE_TERMINOLOGY = [
  {
    id: 'upload',
    nlsKeyLabel: 'uploadTask',
    status: STATUS_IDLE,
    message: '',
  },
  {
    id: 'analysis',
    nlsKeyLabel: 'analysisTask',
    status: STATUS_IDLE,
    message: '',
  },
  {
    id: 'update',
    nlsKeyLabel: 'updateTask',
    status: STATUS_IDLE,
    message: '',
  },
];

export {
  TASKS_UPDATE_TERMINOLOGY_URI,
  TASKS_IMPORT_TERMINOLOGY,
  TASKS_UPDATE_TERMINOLOGY,
};
