import { useState, useEffect } from 'react';
import { itemStore } from 'commons/rdforms/itemstore';
import { engine } from '@entryscape/rdforms';
import {
  LEVEL_RECOMMENDED,
  LEVEL_OPTIONAL,
} from 'commons/components/rdforms/LevelSelector';

const getProfile = (templateId) => {
  const template = itemStore.getItem(templateId);
  return engine.levelProfile(template);
};

const useLevelProfile = (templateId) => {
  const [disabledLevels, setDisabledLevels] = useState([]);

  useEffect(() => {
    if (!templateId) return;

    const levelProfile = getProfile(templateId);
    setDisabledLevels([
      ...(!levelProfile.recommended ? [LEVEL_RECOMMENDED] : []),
      ...(!levelProfile.optional ? [LEVEL_OPTIONAL] : []),
    ]);
  }, [templateId]);

  return disabledLevels;
};

export default useLevelProfile;
