import { useState } from 'react';
import { shouldUseUriPattern } from 'commons/util/store';
import { useTranslation } from 'commons/hooks/useTranslation';
import { TextField, InputAdornment } from '@mui/material';
import ContentWrapper from 'commons/components/common/ContentWrapper';
import PropTypes from 'prop-types';
import { useUserState } from 'commons/hooks/useUser';
import { createProject } from 'workbench/utils';
import { useEntityTitleAndURI } from 'commons/hooks/useEntityTitleAndURI';
import Lookup from 'commons/types/Lookup';
import eswoSpacesNLS from 'workbench/nls/eswoSpaces.nls';
import ListActionDialog from 'commons/components/common/dialogs/ListActionDialog';
import { CREATE, useListModel } from 'commons/components/ListView';
import useRestrictionDialog from 'commons/hooks/useRestrictionDialog';
import ProjectTypeChooser from 'commons/types/ProjectTypeChooser';
import { useSnackbar } from 'commons/hooks/useSnackbar';
import useAsync, { PENDING } from 'commons/hooks/useAsync';
import LoadingButton from 'commons/components/LoadingButton';
import { ErrorCatcher } from 'commons/errors/ErrorCatcher';
import { ErrorWithMessage } from 'commons/errors/utils/errors';
import escoListNLS from 'commons/nls/escoList.nls';
import useProjectRestriction from './useProjectRestriction';

const CreateProjectDialog = ({ closeDialog, numberOfProjects }) => {
  const [, dispatch] = useListModel();
  const { userEntry, userInfo } = useUserState();
  const [selectedProjectType, setSelectedProjectType] = useState('');
  const entityType = Lookup.getMainEntityType('workbench');
  const hasEntityTypeURIPattern = shouldUseUriPattern(entityType);
  const entityTypeUriPatternBase =
    hasEntityTypeURIPattern && entityType.getUriPatternBase();
  const uriPattern = (hasEntityTypeURIPattern && entityType.uriPattern) || '';
  const [addSnackbar] = useSnackbar();
  const { runAsync, status, error: createError } = useAsync();

  const { title, name, isNameFree, handleTitleChange, handleNameChange } =
    useEntityTitleAndURI({
      uriPattern,
    });

  const [description, setDescription] = useState('');
  const [pristine, setPristine] = useState(true);
  const translate = useTranslation([eswoSpacesNLS, escoListNLS]);

  const { isRestricted, contentPath } = useProjectRestriction(numberOfProjects);
  useRestrictionDialog({
    open: isRestricted,
    contentPath,
    callback: closeDialog,
  });

  const handleDescriptionChange = (evt) => {
    setDescription(evt.target.value);
  };

  const handleCreate = async () => {
    return createProject({
      title,
      description,
      name,
      entityType,
      projectType: selectedProjectType,
      hasAdminRights: userInfo.hasAdminRights,
    })
      .then(() => {
        // todo really needed?
        userEntry.setRefreshNeeded();
        return userEntry.refresh();
      })
      .then(closeDialog)
      .then(() => dispatch({ type: CREATE }))
      .then(() => addSnackbar({ message: translate('createProjectSuccess') }))
      .catch((error) => {
        throw new ErrorWithMessage(translate('createProjectFail'), error);
      });
  };

  const formIsValid = hasEntityTypeURIPattern
    ? isNameFree && name !== '' && title !== ''
    : title !== '';

  const actions = (
    <LoadingButton
      onClick={() => runAsync(handleCreate())}
      loading={status === PENDING}
      disabled={!formIsValid}
      autoFocus
    >
      {translate('createButtonLabel')}
    </LoadingButton>
  );

  if (isRestricted) return null;

  return (
    <>
      <ListActionDialog
        id="create-project"
        closeDialog={closeDialog}
        actions={actions}
        title={translate('createEntityHeader')}
        maxWidth="md"
      >
        <ContentWrapper>
          <form autoComplete="off">
            <ProjectTypeChooser
              onChangeProjectType={setSelectedProjectType}
              selectedId={selectedProjectType}
              userInfo={userInfo}
            />
            <TextField
              id="create-project-title"
              label={translate('createEntityTitle')}
              value={title}
              onChange={(evt) => {
                setPristine(false);
                handleTitleChange(evt);
              }}
              placeholder={translate('createEntityTitlePlaceholder')}
              error={!pristine && !title}
              helperText={!pristine && !title && translate('nameRequired')}
              required
            />

            {hasEntityTypeURIPattern && (
              <TextField
                label={translate('createEntityName')}
                id="create-project-name"
                value={name}
                onChange={(evt) => {
                  setPristine(false);
                  handleNameChange(evt);
                }}
                error={!pristine && (!name || !isNameFree)}
                helperText={
                  !pristine &&
                  (!name
                    ? translate('URLRequired')
                    : !isNameFree && translate('unavailableURLFeedback'))
                }
                required
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      {entityTypeUriPatternBase}
                    </InputAdornment>
                  ),
                }}
              />
            )}
            <TextField
              multiline
              rows={4}
              id="create-project-description"
              label={translate('createEntityDesc')}
              value={description}
              onChange={handleDescriptionChange}
              placeholder={translate('descriptionPlaceholder')}
            />
          </form>
        </ContentWrapper>
      </ListActionDialog>
      <ErrorCatcher error={createError} />
    </>
  );
};

CreateProjectDialog.propTypes = {
  closeDialog: PropTypes.func,
  numberOfProjects: PropTypes.number,
};

export default CreateProjectDialog;
