import escoListNLS from 'commons/nls/escoList.nls';
import escaDatasetNLS from 'catalog/nls/escaDataset.nls';
import ListEditEntryDialog from 'commons/components/EntryListView/dialogs/ListEditEntryDialog';
import SuggestionCheckListDialog from 'catalog/suggestions/dialogs/SuggestionChecklistDialog';
import CommentsDialog from 'commons/CommentsDialog';
import { withListRefresh } from 'commons/components/ListView';

export const nlsBundles = [escaDatasetNLS, escoListNLS];

export const rowActions = [
  {
    id: 'edit-suggestion',
    Dialog: ListEditEntryDialog,
    nlsKeyLabel: 'edit',
  },
  {
    id: 'progressMenu',
    Dialog: withListRefresh(SuggestionCheckListDialog, 'onSave'),
    labelNlsKey: 'progressMenu',
  },
  {
    id: 'comments',
    Dialog: CommentsDialog,
    labelNlsKey: 'commentMenu',
  },
];
