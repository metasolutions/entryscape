import { Binding } from '@entryscape/rdforms';
import { entrystoreUtil } from 'commons/store';
import { getESContext, getESContextAndName } from 'commons/hooks/useESContext';
import { getRdformsContext } from 'commons/hooks/useRdformsContext';
import CreateDialog from 'commons/components/EntityOverview/dialogs/CreateEntityDialog';
import { getLabel } from 'commons/util/rdfUtils';
import Lookup from 'commons/types/Lookup';
import eswoBenchNLS from 'workbench/nls/eswoBench.nls';
import getChoiceFromEntry from './getChoiceFromEntry';
import { getQueryConfiguration } from './util';

/**
 * Implementation for the choice upgrade function.
 *
 * @param {Binding} binding
 * @param {Function} callback
 * @param {object} field
 * @param {string} value
 * @param {object} choiceObject
 */
const upgradeToEntry = (binding, callback, field, value, choiceObject) => {
  const { context, contextName } = getESContextAndName();
  const constraints = binding.getItem().getConstraints();
  const { source: entityType } = Lookup.getEntityTypeByConstraints(constraints);
  const rdfType = entityType.rdfType[0];

  const closeDialog = () => field.removeChild(child);
  const afterCreateEntry = (entry) => {
    choiceObject.label.en = getLabel(entry);
    choiceObject.mismatch = false;
    delete choiceObject.upgrade;

    closeDialog();
    callback(choiceObject);
  };

  const child = (
    <CreateDialog
      key="create-entity-dialog"
      actionParams={{
        entityType,
        afterCreateEntry,
        context,
        contextName,
        rdfType,
        nlsBundles: [eswoBenchNLS],
      }}
      closeDialog={closeDialog}
      upgradeLink={value}
    />
  );

  field.appendChild(child);
};

const getChoice = (item, value) => {
  const context = getESContext() || getRdformsContext();
  const choiceObject = {
    value,
    load(onSuccess = () => {}) {
      const onError = () => {
        choiceObject.upgrade = (...params) =>
          upgradeToEntry(...params, value, choiceObject);
        choiceObject.label = { en: value };
        choiceObject.mismatch = true; // TODO replace with something else
        onSuccess();
      };

      const constructChoiceFromEntry = (entry) => {
        const { ...cleanChoice } = getChoiceFromEntry(entry, choiceObject);
        choiceObject.entry = entry;
        // Hack to prevent JSON.stringify from failing on the choice. MUI will
        // run this to log a warning when matching choice is not found, which in
        // turn will cause the rdforms editor to break.
        choiceObject.toJSON = () => {
          return cleanChoice;
        };
        delete choiceObject.load;
        return choiceObject;
      };

      const {
        explicitContext,
        contextRestriction,
        invertContextRestriction,
        remoteStoreUtil,
      } = getQueryConfiguration(item, context);

      let choiceContext = null;
      if (explicitContext) {
        choiceContext = explicitContext;
      } else if (contextRestriction && !invertContextRestriction) {
        choiceContext = context;
      }

      /**
       * 1. Looks in the default store's cache
       * 2. Tries to fetch the entry from the default store
       *
       * If a remote store has been configured for the entitytype:
       * 3. Looks in the remote store's cache
       * 4. Tries to fetch the entry from the remote store
       *
       * If all the above were unsuccessful
       * 5. Calls onError and returns the resourceURI as the label
       *
       * Note: looking into the cache, otherwise fetching the entry, are both
       * done via the store utility `getEntryByResourceURI`.
       */
      return entrystoreUtil
        .getEntryByResourceURI(value, choiceContext)
        .then(constructChoiceFromEntry, () =>
          remoteStoreUtil
            .getEntryByResourceURI(value)
            .then(constructChoiceFromEntry)
            .then(onSuccess, onError)
        )
        .then(onSuccess, onError);
    },
  };

  return choiceObject;
};

export default getChoice;
