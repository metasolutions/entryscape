import { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Entry, Context } from '@entryscape/entrystore-js';
import {
  shouldUseUriPattern,
  constructURIFromPattern,
  createEntityTypeEntryFromLink,
  createEntityTypeEntryFromFile,
  checkURIUniquenessScope,
  getGroupWithHomeContext,
} from 'commons/util/store';
import { createPrototypeEntry } from 'commons/util/entry';
// eslint-disable-next-line max-len
import CreateEntryByLinkOrFile from 'commons/components/EntryListView/dialogs/create/CreateEntryByLinkOrFile';
import {
  ONLY_LINK,
  ONLY_FILE,
  INTERNAL_LINK,
  PATTERNED_LINK,
  LINK_AND_FILE,
  UPGRADE_LINK,
} from 'commons/components/EntryListView/dialogs/create/handlers';
import {
  useUniqueURIScopeContext,
  withURIScopeContextProvider,
} from 'commons/hooks/useUniqueURIScope';
import { LEVEL_MANDATORY } from 'commons/components/rdforms/LevelSelector';
import { CREATE, useListModel } from 'commons/components/ListView';
import { entityTypePropType } from 'commons/types/EntityType';
import { EntityType } from 'entitytype-lookup';
import {
  nlsBundlesPropType,
  useTranslation,
} from 'commons/hooks/useTranslation';
import { useSnackbar, ERROR } from 'commons/hooks/useSnackbar';

/**
 * Explicitly sets the ACL of an entityType entry so that it's not public by default
 *
 * @param {Entry} entry
 * @param {Context} context
 * @param {object} entityTypeConfig
 * @returns {Promise}
 */
const setEntititypeEntryNonPublic = async (
  entry,
  context,
  entityTypeConfig
) => {
  const publishable = entityTypeConfig.publishControl === true;
  if (!publishable) return;

  const groupEntry = await getGroupWithHomeContext(context);
  const entryInformation = entry.getEntryInfo();
  const accessControl = entryInformation.getACL(true);
  accessControl.admin.push(groupEntry.getId());
  entryInformation.setACL(accessControl);
  return entryInformation.commit();
};

const CreateEntityDialog = ({
  closeDialog,
  actionParams = {},
  upgradeLink = '',
}) => {
  const {
    entityType,
    afterCreateEntry,
    context,
    contextName,
    rdfType,
    label,
    nlsBundles,
  } = actionParams;
  // TODO https://metasolutions.atlassian.net/browse/ES-2827
  // only use entity type instance eventually
  const entityTypeConfig =
    entityType instanceof EntityType ? entityType.get() : entityType;
  const [, dispatch] = useListModel();
  const [prototypeEntry, setPrototypeEntry] = useState(null);
  const { setIsURIUnique } = useUniqueURIScopeContext();
  const [hasUniqueURIScope, setHasUniqueURIScope] = useState(false);
  const hasUriPattern = entityTypeConfig
    ? shouldUseUriPattern(entityTypeConfig)
    : false;
  const [selectorType, setSelectorType] = useState(LINK_AND_FILE);
  const uriPattern = (hasUriPattern && entityTypeConfig.uriPattern) || '';
  const entityTypeUriPatternBase =
    hasUriPattern && constructURIFromPattern(uriPattern, { contextName });
  const translate = useTranslation(nlsBundles);
  const [addSnackbar] = useSnackbar();

  useEffect(() => {
    const uniqueUriScope = entityTypeConfig.uniqueURIScope;
    if (!uniqueUriScope) return;
    setHasUniqueURIScope(Boolean(uniqueUriScope));
  }, [entityTypeConfig]);

  useEffect(() => {
    const {
      includeFile = false,
      includeLink = false,
      includeInternal = false,
    } = entityTypeConfig;
    let selector = LINK_AND_FILE;
    if (!includeFile && !includeLink && !includeInternal) {
      throw Error(
        'Cannot create entity that is nothing (not file, not link, not internal URI), ' +
          'something is wrong in the configuration'
      );
    }

    // TODO old code
    /*
      An entitytype can be allowed to be a link, a file or internal.
      If all three are enabled you should be able to create an entity
      without providing a uri or an uploaded file.
      Right now, you can't without providing one of those (uri or file).
    */

    // if (includeLink && !includeInternal) {
    // linkInputEl.setAttribute(
    //   'placeholder',
    //   this.NLSLocalized0.linkInputExternal_placeholder
    // );
    // } else {
    // linkInputEl.setAttribute(
    //   'placeholder',
    //   this.NLSLocalized0.linkInput_placeholder
    // );
    // }

    if (!includeFile) selector = ONLY_LINK;
    if (includeFile && !includeLink && !includeInternal) selector = ONLY_FILE;
    if (!includeLink && includeInternal) selector = INTERNAL_LINK; // Only internal option, no link
    if (includeLink && hasUriPattern) selector = PATTERNED_LINK;
    if (upgradeLink) selector = UPGRADE_LINK;

    setSelectorType(selector);
  }, [entityTypeConfig, hasUriPattern, upgradeLink]);

  useEffect(() => {
    setPrototypeEntry(
      createPrototypeEntry({
        context,
        contextName,
        entityType: entityTypeConfig,
        rdfType,
      })
    );
  }, [context, contextName, entityTypeConfig, rdfType]);

  const handleCreateEntry = async (
    entry,
    graph,
    _currentContext,
    createEntryParams
  ) => {
    try {
      const isInputLink =
        selectorType === ONLY_LINK ||
        selectorType === PATTERNED_LINK ||
        selectorType === UPGRADE_LINK ||
        (selectorType === LINK_AND_FILE && createEntryParams.isLink);
      let newEntry;
      if (!isInputLink) {
        newEntry = await createEntityTypeEntryFromFile({
          context,
          graph,
          ...createEntryParams,
        });
      } else {
        const createParams = {
          context,
          prototypeEntry: entry,
          graph,
          ...createEntryParams,
        };

        if (selectorType === UPGRADE_LINK) {
          newEntry = await createEntityTypeEntryFromLink(createParams);
          await setEntititypeEntryNonPublic(
            newEntry,
            context,
            entityTypeConfig
          );
          dispatch({ type: CREATE });
          return newEntry;
        }

        let isValidLink = true;
        if (hasUniqueURIScope) {
          isValidLink = await checkURIUniquenessScope(
            entityTypeConfig.uniqueURIScope,
            createEntryParams.link,
            context,
            entityTypeConfig.rdfType
          );
          setIsURIUnique(isValidLink);
        }

        if (!isValidLink) throw new Error('URL not available');

        newEntry = await createEntityTypeEntryFromLink({
          uriPattern,
          ...createParams,
        });
      }

      await setEntititypeEntryNonPublic(newEntry, context, entityTypeConfig);
      dispatch({ type: CREATE });
      addSnackbar({ message: translate('createEntityTypeSuccess') });

      return newEntry;
    } catch (error) {
      addSnackbar({ message: translate('createEntityTypeFail'), type: ERROR });
      throw Error(error);
    }
  };

  return prototypeEntry ? (
    <CreateEntryByLinkOrFile
      label={label}
      entry={prototypeEntry}
      closeDialog={closeDialog}
      createEntry={handleCreateEntry}
      afterCreateEntry={afterCreateEntry}
      formTemplateId={entityTypeConfig.template}
      editorLevel={entityTypeConfig.templateLevel || LEVEL_MANDATORY}
      selectorType={selectorType}
      entityTypeUriPatternBase={entityTypeUriPatternBase}
      entityType={entityTypeConfig}
      upgradeLink={upgradeLink}
    />
  ) : null;
};

CreateEntityDialog.propTypes = {
  actionParams: PropTypes.shape({
    entityType: PropTypes.oneOfType([entityTypePropType, PropTypes.shape({})]),
    afterCreateEntry: PropTypes.func,
    context: PropTypes.shape({}),
    contextName: PropTypes.string,
    rdfType: PropTypes.string,
    label: PropTypes.oneOfType([PropTypes.string, PropTypes.shape({})]),
    nlsBundles: nlsBundlesPropType,
  }),
  closeDialog: PropTypes.func,
  upgradeLink: PropTypes.string,
};

export default withURIScopeContextProvider(CreateEntityDialog);
