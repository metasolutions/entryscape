import PropTypes from 'prop-types';
import { entryPropType } from 'commons/util/entry';
import {
  isAPIDistribution,
  isDistributionUploaded,
} from 'catalog/datasets/DatasetOverview/utils/distributions';
import EditEntryDialog from 'commons/components/entry/EditEntryDialog';
import 'commons/components/rdforms/GeoChooser';

const EditDistributionDialog = ({
  closeDialog,
  entry: distributionEntry,
  refreshDistributions,
  datasetEntry,
}) => {
  const filterPredicates =
    isDistributionUploaded(distributionEntry) ||
    isAPIDistribution(distributionEntry)
      ? {
          'http://www.w3.org/ns/dcat#accessURL': true,
          'http://www.w3.org/ns/dcat#downloadURL': true,
        }
      : null;

  return (
    <EditEntryDialog
      entry={distributionEntry}
      parentEntry={datasetEntry}
      closeDialog={closeDialog}
      onEntryEdit={refreshDistributions}
      filterPredicates={filterPredicates}
    />
  );
};

EditDistributionDialog.propTypes = {
  closeDialog: PropTypes.func,
  entry: entryPropType,
  refreshDistributions: PropTypes.func,
  datasetEntry: entryPropType,
};

export default EditDistributionDialog;
