import moment from 'moment';

const TIME_RANGE_TODAY = 'today';
const TIME_RANGE_YESTERDAY = 'yesterday';
const TIME_RANGE_THIS_MONTH = 'this-month';
const TIME_RANGE_LAST_MONTH = 'last-month';
const TIME_RANGE_THIS_YEAR = 'this-year';
const TIME_RANGE_LAST_YEAR = 'last-year';

/**
 * @return {*[]}
 */
const getTimeRanges = () => {
  return [
    {
      value: TIME_RANGE_TODAY,
      label: 'timeRangeToday',
    },
    {
      value: TIME_RANGE_YESTERDAY,
      label: 'timeRangeYesterday',
    },
    {
      value: TIME_RANGE_THIS_MONTH,
      label: 'timeRangeThisMonth',
    },
    {
      value: TIME_RANGE_LAST_MONTH,
      label: 'timeRangeLastMonth',
    },
    {
      value: TIME_RANGE_THIS_YEAR,
      label: 'timeRangeThisYear',
    },
    {
      value: TIME_RANGE_LAST_YEAR,
      label: 'timeRangeLastYear',
    },
  ];
};

/**
 *
 * @param selected
 * @return {*}
 */
const toDateObject = (selected) => {
  const date = new Date();
  switch (selected) {
    case TIME_RANGE_TODAY:
      break;
    case TIME_RANGE_YESTERDAY:
      date.setDate(date.getDate() - 1);
      break;
    case TIME_RANGE_THIS_MONTH:
      return {
        year: date.getFullYear(),
        month: date.getMonth(),
      };
    case TIME_RANGE_LAST_MONTH:
      date.setMonth(date.getMonth() - 1);
      return {
        year: date.getFullYear(),
        month: date.getMonth(),
      };
    case TIME_RANGE_THIS_YEAR:
      return {
        year: date.getFullYear(),
      };
    case TIME_RANGE_LAST_YEAR:
      date.setFullYear(date.getFullYear() - 1);
      return {
        year: date.getFullYear(),
      };
    default:
  }
  return {
    year: date.getFullYear(),
    month: date.getMonth(),
    date: date.getDate(),
  };
};

/**
 *
 * @param value
 * @return {string}
 */
const serializeSelection = (value) => {
  const dateObj = moment(toDateObject(value));
  switch (value) {
    case TIME_RANGE_THIS_MONTH:
    case TIME_RANGE_LAST_MONTH:
      return dateObj.format('MMMM YYYY');
    case TIME_RANGE_THIS_YEAR:
    case TIME_RANGE_LAST_YEAR:
      return dateObj.format('YYYY');
    default:
      return dateObj.format('Do MMMM YYYY');
  }
};

/**
 *
 * @param selected
 * @param data
 * @return {Array}
 */
const normalizeChartData = (selected, data) => {
  const date = new Date();
  const wholeData = [];
  let max;
  let day;
  let month;
  let year;

  // having index day as 0 refers to the previous month
  const daysInMonth = (m, y) => new Date(y, m + 1, 0).getDate();

  switch (selected) {
    case TIME_RANGE_TODAY:
      max = 24;
      day = date.getDate();
      month = date.getMonth();
      year = date.getFullYear();
      break;
    case TIME_RANGE_YESTERDAY:
      date.setDate(date.getDate() - 1); // update date
      max = 24;
      day = date.getDate();
      month = date.getMonth();
      year = date.getFullYear();
      break;
    case TIME_RANGE_THIS_MONTH:
      max = daysInMonth(date.getMonth(), date.getFullYear());
      day = date.getDate();
      month = date.getMonth();
      year = date.getFullYear();
      break;
    case TIME_RANGE_LAST_MONTH:
      date.setMonth(date.getMonth() - 1); // update date
      max = daysInMonth(date.getMonth(), date.getFullYear());
      day = date.getDate();
      month = date.getMonth();
      year = date.getFullYear();
      break;
    case TIME_RANGE_THIS_YEAR:
      max = 12;
      day = date.getDate();
      month = date.getMonth();
      year = date.getFullYear();
      break;
    case TIME_RANGE_LAST_YEAR:
      date.setFullYear(date.getFullYear() - 1);
      max = 12;
      month = date.getMonth();
      year = date.getFullYear();
      break;
    case 'custom':
      break;
    default:
  }

  let hasApiDiffIndex = false;
  let i;
  if (selected === TIME_RANGE_THIS_YEAR || selected === TIME_RANGE_LAST_YEAR) {
    i = 0;
    hasApiDiffIndex = true;
  } else {
    i = 1;
    max += 1;
  }

  // loop through either one of hours, days or months
  for (; i < max; i++) {
    let x; // datetime
    let y = 0; // count

    // try to extract count from data if it exists
    // take care of monthIndex inconsistency between JS and the api data
    const datetimeApiIndex = hasApiDiffIndex ? i + 1 : i;
    const datetimeProperty =
      datetimeApiIndex < 10 ? `0${datetimeApiIndex}` : datetimeApiIndex;
    if (datetimeProperty in data) {
      y = data[datetimeProperty].count;
    }

    switch (selected) {
      case TIME_RANGE_TODAY:
      case TIME_RANGE_YESTERDAY:
        x = new Date(year, month, day, i);
        break;
      case TIME_RANGE_THIS_MONTH:
      case TIME_RANGE_LAST_MONTH:
        x = new Date(year, month, i);
        break;
      case TIME_RANGE_THIS_YEAR:
      case TIME_RANGE_LAST_YEAR:
        x = new Date(year, i, 1);
        break;
      default:
    }

    wholeData.push({ x, y });
  }

  return wholeData;
};

export default {
  getTimeRanges,
  toDateObject,
  serializeSelection,
  normalizeChartData,
};

export {
  TIME_RANGE_TODAY,
  TIME_RANGE_YESTERDAY,
  TIME_RANGE_THIS_MONTH,
  TIME_RANGE_LAST_MONTH,
  TIME_RANGE_THIS_YEAR,
  TIME_RANGE_LAST_YEAR,
};
