import { useEffect } from 'react';
import { Entry } from '@entryscape/entrystore-js';
import { entrystore } from 'commons/store';
import { getLabel } from 'commons/util/rdfUtils';
import { getResourceURI } from 'commons/util/entry';
import { DATASET_OVERVIEW_NAME } from 'catalog/config/config';
import { getPathFromViewName } from 'commons/util/site';
import Lookup from 'commons/types/Lookup';
import { Link } from 'react-router-dom';
import { parseTemplate } from 'commons/util/reactUtils';
import useAsync from '../useAsync';

const getEntryType = (rdfType) => {
  const types = [
    'Dataset',
    'DatasetSeries',
    'Catalog',
    'Distribution',
    'DataService',
    'Suggestion',
  ];
  return types.find((type) => rdfType.endsWith(type));
};

const getNlsKey = (entryType) => {
  if (entryType) {
    return `${entryType.toLowerCase()}AsRemoveItem`;
  }
  return 'otherAsRemoveItem';
};

const getDatasetWithReference = async (entry) =>
  entrystore
    .newSolrQuery()
    .uriProperty('dcat:distribution', entry.getResourceURI())
    .getEntries();

const getLabelsForDistribution = async (reference, t) => {
  const [datasetEntry] = await getDatasetWithReference(reference);
  const distributionLabel = getLabel(reference);
  const format = getResourceURI(reference, 'dcterms:format');

  const href = getPathFromViewName(DATASET_OVERVIEW_NAME, {
    contextId: datasetEntry.getContext().getId(),
    entryId: datasetEntry.getId(),
  });

  const datasetLabel = (
    <Link data-test="butt" href={href} label={getLabel(datasetEntry)} />
  );
  const properties = {
    format,
    datasetLabel,
  };

  if (!distributionLabel && format) {
    return parseTemplate(t('distributionWithoutLabel'), properties);
  }
  if (distributionLabel) {
    return parseTemplate(t('distributionWithLabel'), properties);
  }
  return parseTemplate(t('distributionWithoutLabelOrFormat'), {
    datasetLabel,
  });
};

export const getLabels = async (referenceEntries, t) => {
  const labels = await Promise.all(
    referenceEntries.map(async (reference) => {
      const type = reference
        .getMetadata()
        .findFirstValue(reference.getResourceURI(), 'rdf:type');

      const entryType = getEntryType(type);
      if (entryType === 'Distribution') {
        const labelForDistribution = await getLabelsForDistribution(
          reference,
          t
        );
        return labelForDistribution;
      }
      const label =
        getLabel(reference) || t('entryWithId', `'${reference.getId()}'`);

      return t(getNlsKey(entryType), label);
    })
  );

  return labels;
};

/**
 *
 * @param {Entry} entry
 * @param {Function} ignoreCallback
 * @returns {Promise<Entry[]>}
 */
export const getReferences = async (entry, ignoreCallback = () => false) => {
  const references = await entrystore
    .newSolrQuery()
    .objectUri([entry.getResourceURI(), entry.getURI()])
    .getEntries();
  return references.filter((reference) => !ignoreCallback(reference));
};

/**
 *
 * @param {Entry} entry
 * @returns {Promise<object>}
 */
const getPrimaryEntityType = (entry) =>
  Lookup.options(entry).then(({ primary }) => primary);

/**
 *
 * @param {Entry} entry
 * @param {Function} translate
 * @param {Function} ignoreCallback
 * @returns {Promise<object>}
 */
const getReferencesAndLabels = async (
  entry,
  translate,
  ignoreCallback = () => false
) => {
  const referenceEntries = await getReferences(entry, ignoreCallback);
  const labels = await getLabels(referenceEntries, translate);
  const entityTypes = await Promise.all(
    referenceEntries.map(getPrimaryEntityType)
  );
  return { references: referenceEntries, labels, entityTypes };
};

const useGetReferences = (entry, translate, ignoreCallback = () => false) => {
  const {
    data: { references, labels, entityTypes },
    runAsync,
    status,
  } = useAsync({
    data: { references: [], labels: [] },
  });

  useEffect(() => {
    if (status !== 'idle') return;
    runAsync(getReferencesAndLabels(entry, translate, ignoreCallback));
  }, [entry, runAsync, status, translate, ignoreCallback]);

  return { references, labels, entityTypes, status };
};

export default useGetReferences;
