import PropTypes from 'prop-types';
import { graphPropType } from 'commons/util/entry';
import { LITERAL } from 'commons/components/forms/utils/nodetypes';
import { useTranslation } from 'commons/hooks/useTranslation';
import escoFormsNls from 'commons/nls/escoForms.nls';
import { useFieldsEdit } from '../hooks/useFieldsEdit';
import { Fields } from '../Fields';
import { Field } from '../Field';
import { TextField } from '../TextField';

export const LiteralEditor = ({
  property,
  xs = 8,
  size,
  children,
  multiline,
  graph,
  resourceURI,
  dispatch,
  nodetype = LITERAL,
  validate,
  fieldsCount,
  renderInlineContent,
  renderControls,
  disabled,
  error,
  helperText,
  ...fieldsProps
}) => {
  const { fields, fieldGroup, labelId, addField, removeField, updateValue } =
    useFieldsEdit({
      name: fieldsProps.name,
      property,
      nodetype,
      graph,
      resourceURI,
      dispatch,
      mandatory: fieldsProps.mandatory,
      validate,
    });
  const t = useTranslation(escoFormsNls);

  return (
    <Fields labelId={labelId} addField={addField} {...fieldsProps}>
      {renderInlineContent ? renderInlineContent(fieldGroup) : null}
      {children}
      {fields.map((field) => {
        return (
          <Field
            key={field.getId()}
            size={fieldsCount || fields.length}
            onRemove={() => removeField(field)}
            onClear={() => updateValue(field, '')}
          >
            <TextField
              xs={xs}
              disabled={disabled}
              inputProps={{ 'aria-labelledby': labelId }}
              onChange={({ target }) => updateValue(field, target.value)}
              value={field.getValue() || ''}
              size={size}
              multiline={multiline}
              error={error || field.hasError()}
              helperText={
                helperText || (field.hasError() ? t(field.getError()) : '')
              }
            />
            {renderControls ? renderControls() : null}
          </Field>
        );
      })}
    </Fields>
  );
};

LiteralEditor.propTypes = {
  property: PropTypes.string,
  xs: PropTypes.number,
  size: PropTypes.string,
  multiline: PropTypes.bool,
  nodetype: PropTypes.string,
  graph: graphPropType,
  resourceURI: PropTypes.string,
  dispatch: PropTypes.func,
  children: PropTypes.node,
  validate: PropTypes.func,
  fieldsCount: PropTypes.number,
  renderInlineContent: PropTypes.func,
  renderControls: PropTypes.func,
  error: PropTypes.bool,
  helperText: PropTypes.string,
  disabled: PropTypes.bool,
};
