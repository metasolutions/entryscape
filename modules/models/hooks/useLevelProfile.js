import { useMemo, useState } from 'react';
import {
  LEVEL_RECOMMENDED,
  LEVEL_OPTIONAL,
  LEVEL_MANDATORY,
} from 'commons/components/rdforms/LevelSelector';
import { getLevels } from '../utils/levels';

const useLevelProfile = (template) => {
  const [level, setLevel] = useState(LEVEL_MANDATORY);
  const levels = useMemo(() => getLevels(template), [template]);

  const disabledLevels = useMemo(() => {
    return [
      ...(!levels?.includes(LEVEL_RECOMMENDED) ? [LEVEL_RECOMMENDED] : []),
      ...(!levels?.includes(LEVEL_OPTIONAL) ? [LEVEL_OPTIONAL] : []),
    ];
  }, [levels]);

  return { disabledLevels, level, setLevel };
};

export default useLevelProfile;
