import { Fragment } from 'react';
import PropTypes from 'prop-types';
import { Entry } from '@entryscape/entrystore-js';
import { Grid, ListItemIcon, Button } from '@mui/material';
import InfoIcon from '@mui/icons-material/Info';
import AccountTreeIcon from '@mui/icons-material/AccountTree';
import DoubleArrowIcon from '@mui/icons-material/DoubleArrow';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import { getRenderName, getFullLengthLabel } from 'commons/util/rdfUtils';
import { truncate } from 'commons/util/util';
import { getShortModifiedDate } from 'commons/util/metadata';
import { useTranslation } from 'commons/hooks/useTranslation';
import escoEntryChooserNLS from 'commons/nls/escoEntryChooser.nls';
import {
  ListItem,
  ListItemAction,
  ListItemText,
  ListItemIconButton,
} from 'commons/components/ListView';
import { entrystore } from 'commons/store';
import './index.scss';

export const TreePath = ({ conceptEntry, entries = [] }) => {
  const entryURI = conceptEntry.getURI();
  // Avoid presenting the tree path for entries in external stores
  if (entryURI.indexOf(entrystore.getBaseURI()) !== 0) return null;

  return (
    <span className="escoEntrySelectListItem__treePath">
      {entries.length > 0 &&
        entries
          .slice()
          .reverse()
          .map((entry, idx) => (
            <Fragment key={entry.getId()}>
              <span className="escoEntrySelectListItem__treePathNode">
                {getRenderName(entry)}
              </span>
              {idx === 0 && entries.length > 1 && (
                <DoubleArrowIcon className="escoEntrySelectListItem__treePathIcon" />
              )}
              {idx > 0 && idx < entries.length - 1 && (
                <ChevronRightIcon className="escoEntrySelectListItem__treePathIcon" />
              )}
            </Fragment>
          ))}
    </span>
  );
};

TreePath.propTypes = {
  conceptEntry: PropTypes.instanceOf(Entry),
  entries: PropTypes.arrayOf(PropTypes.instanceOf(Entry)),
};

const EntrySelectButton = ({ entry, binding, onSelect }) => {
  const selected = binding.getValue() === entry.getResourceURI();
  const onClick = () => onSelect(entry);
  const t = useTranslation(escoEntryChooserNLS);
  return (
    <Button
      color="primary"
      disabled={selected}
      variant="text"
      onClick={onClick}
    >
      {selected ? t('selectedEntityLabel') : t('selectEntity')}
    </Button>
  );
};

EntrySelectButton.propTypes = {
  entry: PropTypes.instanceOf(Entry).isRequired,
  binding: PropTypes.shape({ getValue: PropTypes.func }),
  onSelect: PropTypes.func,
};

export const EntrySelectListItem = ({
  entry,
  pathEntries,
  binding,
  onSelect,
  setShowInfoDialog,
}) => {
  const t = useTranslation(escoEntryChooserNLS);
  return (
    <>
      <ListItem key={`${entry.getId()}-${entry.getContext().getId()}`}>
        <Grid item xs={1}>
          <ListItemIcon>
            <AccountTreeIcon />
          </ListItemIcon>
        </Grid>
        <ListItemText
          xs={7}
          direction="column"
          primary={truncate(getFullLengthLabel(entry), 60)}
          secondary={<TreePath conceptEntry={entry} entries={pathEntries} />}
        />
        <ListItemText xs={2} secondary={getShortModifiedDate(entry)} />
        <ListItemIconButton
          title={t('infoTooltip')}
          onClick={setShowInfoDialog}
        >
          <InfoIcon />
        </ListItemIconButton>
        <ListItemAction>
          <EntrySelectButton
            entry={entry}
            binding={binding}
            onSelect={onSelect}
          />
        </ListItemAction>
      </ListItem>
    </>
  );
};

EntrySelectListItem.propTypes = {
  entry: PropTypes.instanceOf(Entry).isRequired,
  pathEntries: PropTypes.arrayOf(PropTypes.shape({})),
  binding: PropTypes.shape({}),
  onSelect: PropTypes.func,
  setShowInfoDialog: PropTypes.func,
};
