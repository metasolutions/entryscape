import {
  REFRESH,
  useFetchEntries,
  useListModel,
  withListModelProvider,
  listPropsPropType,
} from 'commons/components/ListView';
import config from 'config';
import { IDLE, PENDING } from 'commons/hooks/useAsync';
import Loader from 'commons/components/Loader';
import EntityTypeInstanceList from '../EntityTypeInstanceList';
import EntityTypeEntryList from '../EntityTypeEntryList';
import { getEntityTypeEntries } from '../utils/entityTypeQuery';

// By default entity type entries are shown.
// Instance entity types are shown if defined and there are no entity type entries.
const EntityTypeListView = ({ listProps }) => {
  const [{ refreshCount }, dispatch] = useListModel();
  const { entries: entityTypeEntries, status } = useFetchEntries({
    fetchEntries: getEntityTypeEntries,
    refreshCount,
  });

  if (status === IDLE || status === PENDING) return <Loader />;

  if (
    !entityTypeEntries ||
    config.get('admin.enableEntrystoreConfigurations') === false
  )
    return (
      <EntityTypeInstanceList
        onMigrate={() => dispatch({ type: REFRESH })}
        listProps={listProps}
      />
    );

  return (
    <EntityTypeEntryList
      entityTypeEntries={entityTypeEntries}
      status={status}
      listProps={listProps}
    />
  );
};

EntityTypeListView.propTypes = {
  listProps: listPropsPropType,
};

export default withListModelProvider(EntityTypeListView);
