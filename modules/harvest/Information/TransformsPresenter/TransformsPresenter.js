import PropTypes from 'prop-types';
import {
  Table,
  TableBody,
  TableCell,
  TableRow,
  Typography,
} from '@mui/material';
import { useTranslation } from 'commons/hooks/useTranslation';
import { RESOLVED } from 'commons/hooks/useAsync';
import useTimelineComponents from './useTimelineComponents';
import esreHarvestNLS from '../../nls/esreHarvest.nls';
import { labelOverrides } from '../../forms/PipelineFormModel';
import './TransformsPresenter.scss';

const TransformsPresenter = ({ transforms }) => {
  const translate = useTranslation([esreHarvestNLS]);
  const {
    status: componentLoadingStatus,
    Timeline,
    TimelineItem,
    TimelineDot,
    TimelineConnector,
    TimelineContent,
    TimelineOppositeContent,
    TimelineSeparator,
  } = useTimelineComponents();

  if (componentLoadingStatus !== RESOLVED) return null;

  return transforms.length ? (
    <>
      <Typography variant="h2" className="esreTransformsPresenter__header">
        {translate('genericPipelineHeader')}
      </Typography>
      <Timeline>
        {transforms.map(({ id, type, priority, args = {} }, index) => {
          const argumentPairs = Object.entries(args).filter(
            ([, value]) => value
          );
          return (
            <TimelineItem key={id}>
              <TimelineOppositeContent
                classes={{
                  root: 'esreTransformsPresenter__timelineOppositeContent',
                }}
              >
                {priority - 1}
              </TimelineOppositeContent>
              <TimelineSeparator>
                <TimelineDot />
                {index !== transforms.length - 1 ? <TimelineConnector /> : null}
              </TimelineSeparator>
              <TimelineContent component="div">
                <Typography className="esreTransformsPresenter__name">
                  {type}
                </Typography>
                {argumentPairs.length ? (
                  <Table
                    className="esreTransformsPresenter__Table"
                    size="small"
                  >
                    <TableBody>
                      {argumentPairs.map(([key, value]) => (
                        <TableRow key={key}>
                          <TableCell className="esreTransformsPresenter__TableCell--first">
                            {labelOverrides[key] || key}
                          </TableCell>
                          <TableCell>{value}</TableCell>
                        </TableRow>
                      ))}
                    </TableBody>
                  </Table>
                ) : null}
              </TimelineContent>
            </TimelineItem>
          );
        })}
      </Timeline>
    </>
  ) : null;
};

TransformsPresenter.propTypes = {
  transforms: PropTypes.arrayOf(PropTypes.shape({})),
};

export default TransformsPresenter;
