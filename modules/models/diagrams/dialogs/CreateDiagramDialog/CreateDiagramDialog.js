import PropTypes from 'prop-types';
import { useEffect } from 'react';
import { useESContext } from 'commons/hooks/useESContext';
import useAsync from 'commons/hooks/useAsync';
import { LEVEL_MANDATORY } from 'commons/components/rdforms/LevelSelector';
// eslint-disable-next-line max-len
import CreateEntryByTemplateDialog from 'commons/components/EntryListView/dialogs/create/CreateEntryByTemplateDialog';
import useHandleError from 'commons/errors/hooks/useErrorHandler';
import { useTranslation } from 'commons/hooks/useTranslation';
import esmoDiagramsNLS from 'models/nls/esmoDiagrams.nls';
import escoDialogsNLS from 'commons/nls/escoDialogs.nls';
import { DEFAULT_DIAGRAM_ID } from 'models/diagrams/utils/definitions';

const NLS_BUNDLES = [esmoDiagramsNLS, escoDialogsNLS];

/**
 * Create a dataservice prototype
 *
 * @param {Context} context
 * @param {string} rdfType
 * @param entryId
 * @returns {Entry}
 */
const createPrototypeDiagram = async (context, entryId) =>
  context.newGraph({}, entryId).add('rdf:type', 'esterms:Diagram');

const CreateDiagramDialog = (props) => {
  const translate = useTranslation(NLS_BUNDLES);
  const { entryId } = props;
  const { context } = useESContext();
  const { data: prototypeEntry, runAsync, error } = useAsync(null);

  useHandleError(error);

  useEffect(() => {
    runAsync(createPrototypeDiagram(context, entryId));
  }, [context, runAsync]);

  return prototypeEntry ? (
    <CreateEntryByTemplateDialog
      {...props}
      entry={prototypeEntry}
      editorLevel={LEVEL_MANDATORY}
      createHeaderLabel={
        entryId === DEFAULT_DIAGRAM_ID
          ? translate('editEntry')
          : translate('createHeader')
      }
      createButtonLabel={
        entryId === DEFAULT_DIAGRAM_ID ? translate('save') : null
      }
    />
  ) : null;
};

CreateDiagramDialog.propTypes = {
  actionParams: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string,
    })
  ),
  entryId: PropTypes.string,
};
export default CreateDiagramDialog;
