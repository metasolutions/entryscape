import PropTypes from 'prop-types';
import { DataGrid } from '@mui/x-data-grid';
import { ColumnHeader } from 'commons/components/TableView/TableView';
import './index.scss';

const VisualizationTable = ({ data, tableColumns }) => {
  const rows = data.map((item, index) => ({
    id: index + 1,
    ...item,
  }));

  const columns = [
    { field: 'id', headerName: 'ID', hide: true },
  ];

  tableColumns.forEach((field) => {
    const newColumnField = {
      field,
      headerName: field,
      renderHeader: () => <ColumnHeader name={field} />,
      width: '130',
    };
    columns.push(newColumnField);
  });

  return (
    <div className="escaVisualizationTable__container">
      <DataGrid
        rows={rows}
        columns={columns}
        pageSize={5}
        rowsPerPageOptions={[5]}
        disableColumnMenu
        disableSelectionOnClick
      />
    </div>
  );
};

VisualizationTable.propTypes = {
  data: PropTypes.arrayOf(PropTypes.shape({})),
  tableColumns: PropTypes.arrayOf(PropTypes.string),
};
export default VisualizationTable;
