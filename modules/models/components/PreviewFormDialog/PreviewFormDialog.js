import { useEffect, useState, useMemo } from 'react';
import { Entry } from '@entryscape/entrystore-js';
import { Graph, converters } from '@entryscape/rdfjson';
import ListActionDialog from 'commons/components/common/dialogs/ListActionDialog';
import PropTypes from 'prop-types';
import { useTranslation } from 'commons/hooks/useTranslation';
import esmoCommonsNLS from 'models/nls/esmoCommons.nls';
import escoDialogsNLS from 'commons/nls/escoDialogs.nls';
import { RdformsEditorItem } from 'models/rdforms/EditorItem';
import { PreviewPresenterItem } from 'models/rdforms/PresenterItem';
import useAsync from 'commons/hooks/useAsync';
import { TextField, Tabs, Tab, Typography } from '@mui/material';
import TabPanel from 'toolkit/Catalog/TabPanel';
import DialogTwoColumnLayout, {
  PrimaryColumn,
  SecondaryColumn,
} from 'commons/components/common/dialogs/DialogTwoColumnLayout';
import { generateJsonBuild } from 'models/builds/utils/build';
import { generateRdfFormsId, isInlineField } from 'models/fields/utils/fields';
import { ItemStore } from '@entryscape/rdforms';
import { useESContext } from 'commons/hooks/useESContext';
import { getBuildItems } from 'models/builds/utils/dependencies';
import ErrorBoundary from 'commons/errors/ErrorBoundary/ErrorBoundary';
import ErrorPlaceholder from 'commons/errors/ErrorPlaceholder';
import './PreviewFormDialog.scss';
import { isForm } from 'models/forms/utils/forms';
import useErrorHandler from 'commons/errors/hooks/useErrorHandler';
import { CircularError } from 'models/utils/errors';
import LevelSelector from 'commons/components/rdforms/LevelSelector';
import { renderHtmlString } from 'commons/util/reactUtils';
import useEntrystoreError from 'commons/errors/hooks/useEntrystoreError';
import useLevelProfile from '../../hooks/useLevelProfile';
import { getLevelFromDisabledLevels } from '../../utils/levels';

const NLS_BUNDLES = [esmoCommonsNLS, escoDialogsNLS];

const PreviewDataFormat = ({ value }) => {
  const translate = useTranslation(NLS_BUNDLES);
  return (
    <TextField
      value={value}
      placeholder={translate('noDataToPreview')}
      multiline
      variant="outlined"
      margin="dense"
      autoComplete="off"
      autoCorrect="off"
      autoCapitalize="off"
      spellCheck="false"
    />
  );
};

PreviewDataFormat.propTypes = {
  value: PropTypes.string,
};

const NotAuthorizedFallback = () => {
  const translate = useTranslation(NLS_BUNDLES);

  return <ErrorPlaceholder header={translate('previewNotAuthorizedError')} />;
};

const PreviewFallback = ({ error }) => {
  const translate = useTranslation(NLS_BUNDLES);
  const getMessage = () => {
    if (error instanceof CircularError) {
      return renderHtmlString(
        translate('previewCircularError', `<ul><li>${error.message}</li></ul>`)
      );
    }
    return translate('previewTemplateError');
  };

  return (
    <ErrorPlaceholder
      message={getMessage()}
      header={translate('previewErrorHeading')}
    />
  );
};

PreviewFallback.propTypes = {
  error: PropTypes.instanceOf(Error),
};

const Preview = ({ resourceURI, bundle, itemId, entry, bundleError }) => {
  const [selectedTab, setSelectedTab] = useState(0);
  const t = useTranslation(NLS_BUNDLES);
  const [graph] = useState(new Graph());
  const [, refresh] = useState();
  const [rdfJSON, setRdfJSONValue] = useState('');
  const [rdfXML, setRdfXML] = useState('');
  const handleTabChange = (_event, newValue) => {
    setSelectedTab(newValue);
  };
  useErrorHandler(bundleError);
  const entryIsForm = isForm(entry) || isInlineField(entry); // inline field always depends on a form

  const template = useMemo(() => {
    if (bundle == null) {
      return null;
    }
    const itemStore = new ItemStore();
    itemStore.registerBundle({ source: bundle });
    const newTemplate = itemStore.getTemplate(itemId);

    if (entryIsForm) {
      return newTemplate;
    }

    return itemStore.createTemplateFromChildren([newTemplate.getId()]);
  }, [bundle, itemId, entryIsForm]);

  useEffect(() => {
    graph.onChange = async () => {
      refresh(Math.random());
      const rdfjson = graph.exportRDFJSON();
      const jsonString = JSON.stringify(rdfjson, 0, 2);
      setRdfJSONValue(jsonString);
      const rdfxml = await converters.rdfjson2rdfxml(graph);
      setRdfXML(rdfxml);
    };
  }, [graph]);

  const { disabledLevels, level, setLevel } = useLevelProfile(template);

  useEffect(() => {
    setLevel(getLevelFromDisabledLevels(disabledLevels));
  }, [setLevel, disabledLevels]);
  return template ? (
    <DialogTwoColumnLayout>
      <PrimaryColumn xs={7}>
        <LevelSelector
          level={level}
          onUpdateLevel={setLevel}
          disabledLevels={disabledLevels}
        />
        <RdformsEditorItem
          graph={graph}
          resourceURI={resourceURI}
          template={template}
          level={level}
        />
      </PrimaryColumn>
      <SecondaryColumn xs={5}>
        <div>
          <Tabs
            value={selectedTab}
            onChange={handleTabChange}
            aria-label="tabs-aria-label"
          >
            <Tab label="Presenter" />
            <Tab label="RDF/XML" />
            <Tab label="RDF/JSON" />
          </Tabs>
          <TabPanel value={selectedTab} index={0} className="esmoTab__tab">
            {graph.isEmpty() ? (
              <Typography variant="body1">{t('noDataToPreview')}</Typography>
            ) : (
              <PreviewPresenterItem
                graph={graph}
                resourceURI={resourceURI}
                template={template}
              />
            )}
          </TabPanel>
          <TabPanel value={selectedTab} index={1} className="esmoTab__tab">
            {graph.isEmpty() ? (
              <Typography variant="body1">{t('noDataToPreview')}</Typography>
            ) : (
              <PreviewDataFormat value={rdfXML} />
            )}
          </TabPanel>
          <TabPanel value={selectedTab} index={2} className="esmoTab__tab">
            {graph.isEmpty() ? (
              <Typography variant="body1">{t('noDataToPreview')}</Typography>
            ) : (
              <PreviewDataFormat value={rdfJSON} />
            )}
          </TabPanel>
        </div>
      </SecondaryColumn>
    </DialogTwoColumnLayout>
  ) : null;
};

Preview.propTypes = {
  resourceURI: PropTypes.string,
  bundle: PropTypes.shape({}),
  itemId: PropTypes.string,
  entry: PropTypes.instanceOf(Entry),
  bundleError: PropTypes.instanceOf(Error),
};

const PreviewFormDialog = ({ closeDialog, entry }) => {
  const translate = useTranslation(NLS_BUNDLES);
  const { context } = useESContext();
  const { data: bundle, runAsync, error } = useAsync({ data: null });
  const { error: entrystoreError } = useEntrystoreError();
  const isNotAuthorized = entrystoreError?.message?.includes('Not authorized');
  const resourceURI = 'http://example.com';

  useEffect(() => {
    if (!entry) return;

    const getBundle = async () => {
      const buildItems = await getBuildItems([entry]);
      return generateJsonBuild(buildItems);
    };

    runAsync(getBundle());
  }, [context, entry, runAsync]);

  return (
    <ListActionDialog
      closeDialog={closeDialog}
      id="preview-field-dialog"
      title={translate('preview')}
      closeDialogButtonLabel={translate('close')}
      fixedHeight
      maxWidth="xl"
    >
      <ErrorBoundary
        FallbackComponent={
          isNotAuthorized ? NotAuthorizedFallback : PreviewFallback
        }
      >
        <Preview
          resourceURI={resourceURI}
          itemId={generateRdfFormsId(entry)}
          bundle={bundle}
          entry={entry}
          bundleError={error}
        />
      </ErrorBoundary>
    </ListActionDialog>
  );
};

PreviewFormDialog.propTypes = {
  entry: PropTypes.instanceOf(Entry),
  closeDialog: PropTypes.func,
};

export default PreviewFormDialog;
