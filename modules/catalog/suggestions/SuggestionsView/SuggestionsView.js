import PropTypes from 'prop-types';
import { useCallback } from 'react';
import { namespaces } from '@entryscape/rdfjson';
import config from 'config';
import Lookup from 'commons/types/Lookup';
import { useESContext } from 'commons/hooks/useESContext';
import {
  RDF_TYPE_SUGGESTION,
  RDF_STATUS_INVESTIGATING,
  RDF_STATUS_ARCHIVED,
} from 'commons/util/entry';
import { useTranslation } from 'commons/hooks/useTranslation';
import { entrystore } from 'commons/store';
import TableView from 'commons/components/TableView';
import useCommentsCount from 'commons/CommentsDialog/hooks/useCommentsCount';
import { getPathFromViewName } from 'commons/util/site';
import {
  useListModel,
  withListModelProviderAndLocation,
  TOGGLE_TABLEVIEW,
  withListRefresh,
  ListItemButtonAction,
  listPropsPropType,
  getIncludeEdit,
} from 'commons/components/ListView';
import LinearProgress from 'commons/components/progress/LinearProgress';
import {
  useSolrQuery,
  EntryListView,
  TITLE_COLUMN,
  MODIFIED_COLUMN,
  ACTIONS_GROUP_COLUMN,
} from 'commons/components/EntryListView';
import {
  LIST_ACTION_INFO,
  LIST_ACTION_EDIT,
} from 'commons/components/EntryListView/actions';
import { PENDING } from 'commons/hooks/useAsync';
import { Badge, ListItemButton as MuiListItemButton } from '@mui/material';
import { Comment as CommentIcon } from '@mui/icons-material';
import { getTitle } from 'commons/util/metadata';
import CommentsDialog from 'commons/CommentsDialog';
import {
  ANY_FILTER_ITEM,
  ANY,
} from 'commons/components/filters/utils/filterDefinitions';
import useSearchFilters from 'commons/components/filters/hooks/useSearchFilters';
import CreateFromDatasetTemplateDialog from '../dialogs/CreateSuggestionFromDatasetTemplateDialog';
import { nlsBundles, listActions as defaultListActions } from './actions';
import { getChecklistProgress, isArchived as isEntryArchived } from '../util';
import SuggestionCheckListDialog from '../dialogs/SuggestionChecklistDialog';
import { ARCHIVED_TAG } from '../tags';

const STATUS_FILTER = {
  headerNlsKey: 'statusFilterHeader',
  applyFilterParams: (query, filter) => {
    const value = filter.getSelected();
    return value === ANY ? query : query.status(namespaces.expand(value));
  },
  items: [
    ANY_FILTER_ITEM,
    {
      labelNlsKey: 'active',
      value: RDF_STATUS_INVESTIGATING,
    },
    {
      labelNlsKey: 'archived',
      value: RDF_STATUS_ARCHIVED,
    },
  ],
  id: 'archived-filter',
  initialSelection: RDF_STATUS_INVESTIGATING,
  defaultValue: RDF_STATUS_INVESTIGATING,
};

const CommentIconWithBadge = ({ commentsCount }) => (
  <Badge badgeContent={commentsCount} color="primary">
    <CommentIcon fontSize="small" color="secondary" />
  </Badge>
);

CommentIconWithBadge.propTypes = {
  commentsCount: PropTypes.number,
};

const SuggestionsView = ({ listProps }) => {
  const translate = useTranslation(nlsBundles);
  const { context } = useESContext();
  const [{ showTableView }, dispatch] = useListModel();
  const showCreateDatasetTemplate = config.get(
    'catalog.datasetTemplateEndpoint'
  );
  const includeEdit = getIncludeEdit(listProps);

  const { applyFilters, isLoading, hasFilters, ...filterProps } =
    useSearchFilters([STATUS_FILTER]);
  const createQuery = useCallback(
    () =>
      entrystore.newSolrQuery().rdfType(RDF_TYPE_SUGGESTION).context(context),
    [context]
  );
  const { status, ...queryResults } = useSolrQuery({
    createQuery,
    applyFilters,
    wait: isLoading,
  });
  const [getCommentsCount, refreshCommentsCount] = useCommentsCount(
    queryResults.entries
  );

  const listActions = [
    ...(showCreateDatasetTemplate
      ? [
          {
            id: 'create-from-dataset-template',
            Dialog: withListRefresh(
              CreateFromDatasetTemplateDialog,
              'onCreate'
            ),
            labelNlsKey: 'createFromTemplateButton',
            tooltipNlsKey: 'createSuggestionFromTemplateButtonTooltip',
            highlightId: 'listActionCreate',
          },
        ]
      : []),
    ...defaultListActions,
  ];

  return showTableView ? (
    <TableView
      setTableView={() => dispatch({ type: TOGGLE_TABLEVIEW })}
      rdfType={RDF_TYPE_SUGGESTION}
      templateId={Lookup.getByName('catalogSuggestion').templateId()}
    />
  ) : (
    <EntryListView
      {...queryResults}
      {...listProps}
      status={isLoading ? PENDING : status}
      nlsBundles={nlsBundles}
      listActions={listActions}
      includeTableView
      includeFilters={hasFilters}
      filtersProps={hasFilters ? filterProps : null}
      // avoid view placeholder when filters active and no active suggestions found
      showViewPlaceholder={!hasFilters}
      getListItemProps={({ entry }) => ({
        to: getPathFromViewName('catalog__suggestions__suggestion', {
          contextId: entry.getContext().getId(),
          entryId: entry.getId(),
        }),
      })}
      columns={[
        {
          id: 'checklist',
          headerNlsKey: 'checkListHeaderLabel',
          xs: 2,
          Component: ListItemButtonAction,
          ButtonComponent: MuiListItemButton,
          disableGutters: true,
          classes: { root: 'escoLinearProgressButton' },
          'data-highlight-id': 'listItemChecklist',
          getProps: ({ entry }) => {
            const isArchived = isEntryArchived(entry);
            const progress = getChecklistProgress(entry);

            return {
              action: {
                entry,
                Dialog: withListRefresh(SuggestionCheckListDialog, 'onSave'),
                refreshCommentsCount,
              },
              disabled: isArchived,
              children: (
                <LinearProgress progress={progress} disabled={isArchived} />
              ),
            };
          },
        },
        {
          ...TITLE_COLUMN,
          xs: includeEdit ? 6 : 7,
          tags: ARCHIVED_TAG,
        },
        MODIFIED_COLUMN,
        {
          ...ACTIONS_GROUP_COLUMN,
          xs: 2,
          actions: [
            LIST_ACTION_INFO,
            { ...LIST_ACTION_EDIT },
            {
              id: 'comments',
              labelNlsKey: 'comments',
              Dialog: CommentsDialog,
              getProps: ({ entry }) => ({
                entry,
                icon: (
                  <CommentIconWithBadge
                    commentsCount={getCommentsCount(entry.getResourceURI())}
                  />
                ),
                dialogTitle: translate('commentHeader', getTitle(entry)),
                refreshCommentsCount,
                nlsBundles,
              }),
            },
          ],
        },
      ]}
    />
  );
};

SuggestionsView.propTypes = {
  listProps: listPropsPropType,
};

export default withListModelProviderAndLocation(SuggestionsView, () => ({
  showFilters: true,
}));
