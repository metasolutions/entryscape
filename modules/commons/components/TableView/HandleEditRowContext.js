import PropTypes from 'prop-types';
import { createContext, useContext } from 'react';

const HandleEditRowContext = createContext(null);

export const HandleEditRowContextProvider = ({ children, value }) => (
  <HandleEditRowContext.Provider value={value}>
    {children}
  </HandleEditRowContext.Provider>
);

HandleEditRowContextProvider.propTypes = {
  children: PropTypes.node,
  value: PropTypes.func,
};

export const useHandleEditRowContext = () => {
  const context = useContext(HandleEditRowContext);

  if (context === undefined) {
    throw new Error(
      'useHandleEditRowContext must be used within a ContextProvider'
    );
  }

  return context;
};
