import { useCallback } from 'react';
import { getDistributionFileRURIs } from 'catalog/datasets/DatasetOverview/utils/distributions';
import { useTranslation } from 'commons/hooks/useTranslation';
import escaApiProgressNLS from 'catalog/nls/escaApiProgress.nls';
import { getPipelineResource } from 'catalog/datasets/DatasetOverview/utils/pipelines';
import { checkStatusOnRepeat } from 'catalog/datasets/DatasetOverview/utils/api';
import { entrystore, entrystoreUtil } from 'commons/store';
import {
  STATUS_DONE,
  STATUS_FAILED,
  STATUS_PROGRESS,
} from 'commons/hooks/useTasksList';
import { INIT_TASK_ID, FILEPROCESS_TASK_ID } from './tasks';

const useRefreshAPI = (distributionEntry, updateTask, apiDistributionEntry) => {
  const t = useTranslation(escaApiProgressNLS);

  /**
   * For each of the file URIs execute the pipeline with the append transform sequentially.
   *
   * @param {string[]} fileResourceURIs
   * @param {store/Pipeline} pipelineResource
   * @returns {Promise}
   */
  const processFiles = useCallback(
    async (fileResourceURIs, pipelineResource, totalFiles) => {
      let processedFiles = 1; // One already done in refreshAPI()
      for (const fileResourceURI of fileResourceURIs) {
        await entrystoreUtil
          .getEntryByResourceURI(fileResourceURI)
          .then((fileEntry) => pipelineResource.execute(fileEntry, {}))
          .then((result) => checkStatusOnRepeat(result[0], t))
          .catch((error) => {
            updateTask(FILEPROCESS_TASK_ID, {
              status: STATUS_FAILED,
              message: t('apiProgressError'),
            });
            throw error;
          });
        processedFiles += 1;
        updateTask(FILEPROCESS_TASK_ID, {
          status: STATUS_PROGRESS,
          message: t('apiFileProcessed', {
            number: processedFiles,
            totalFiles,
          }),
        });
      }

      updateTask(FILEPROCESS_TASK_ID, {
        status: STATUS_DONE,
      });
    },
    [t, updateTask]
  );

  /**
   * Update distribution metadata
   *  dcterms:modified, dcat:accessURL, dcterms:conformsTo
   * @returns {Promise<store/Entry>}
   */
  const updateApiDistribution = useCallback(() => {
    const apiDistributionMetadata = apiDistributionEntry.getMetadata();
    const apiDistributionResourceURI = apiDistributionEntry.getResourceURI();

    apiDistributionMetadata.findAndRemove(
      apiDistributionResourceURI,
      'dcterms:modified'
    );

    apiDistributionMetadata.addD(
      apiDistributionResourceURI,
      'dcterms:modified',
      new Date().toISOString(),
      'xsd:date'
    );

    const apiURI = apiDistributionMetadata.findFirstValue(
      apiDistributionResourceURI,
      'dcat:accessURL'
    );

    if (apiURI) {
      // Should never fail for API distributions.
      const swaggerURI = `${apiURI}/swagger`;

      apiDistributionMetadata.findAndRemove(
        apiDistributionResourceURI,
        'dcterms:conformsTo',
        swaggerURI
      );

      apiDistributionMetadata.add(
        apiDistributionResourceURI,
        'dcterms:conformsTo',
        swaggerURI
      );
    }

    return apiDistributionEntry.commitMetadata();
  }, [apiDistributionEntry]);

  /**
   *  Refreshes/replaces an API.
   *
   * @returns {Promise}
   */
  const refreshAPI = useCallback(async () => {
    try {
      distributionEntry.setRefreshNeeded();
      await distributionEntry.refresh();
      apiDistributionEntry.setRefreshNeeded();
      await apiDistributionEntry.refresh();

      const fileURIs = getDistributionFileRURIs(distributionEntry);
      const totalNoFiles = fileURIs.length;

      updateTask(INIT_TASK_ID, {
        status: STATUS_PROGRESS,
      });

      const pipelineResource = await getPipelineResource(
        distributionEntry.getContext()
      );

      const transformId = pipelineResource.getTransformForType(
        pipelineResource.transformTypes.ROWSTORE
      );
      const apiAccessURI = apiDistributionEntry
        .getMetadata()
        .findFirstValue(null, 'dcat:accessURL');

      pipelineResource.setTransformArguments(transformId, {});
      pipelineResource.setTransformArguments(transformId, {
        // TODO Improve performance by using action 'append' if only file additions have been done
        action: 'replace',
        datasetURL: apiAccessURI,
      });
      await pipelineResource.commit();

      updateTask(INIT_TASK_ID, {
        status: STATUS_DONE,
      });

      updateTask(FILEPROCESS_TASK_ID, {
        status: STATUS_PROGRESS,
      });

      const fileEntry = await entrystoreUtil.getEntryByResourceURI(fileURIs[0]);

      const pipelineResultURIs = await pipelineResource.execute(fileEntry, {});
      await checkStatusOnRepeat(pipelineResultURIs[0], t);
      updateTask(FILEPROCESS_TASK_ID, {
        status: STATUS_PROGRESS,
        message: t('apiFileProcessed', {
          number: 1,
          totalFiles: totalNoFiles,
        }),
      });

      // First file uri has been used to initialize the API, so doesn't need
      // to go through the following file processing steps
      const tempFileURIs = fileURIs.slice(1);

      if (tempFileURIs.length > 0) {
        const pipelineResultEntry = await entrystore.getEntry(
          pipelineResultURIs[0]
        );
        pipelineResource.setTransformArguments(transformId, {});
        pipelineResource.setTransformArguments(transformId, {
          action: 'append',
          datasetURL: pipelineResultEntry.getResourceURI(),
        });
        await pipelineResource.commit();
        await processFiles(tempFileURIs, pipelineResource, totalNoFiles);
      }

      await updateApiDistribution();
      updateTask(FILEPROCESS_TASK_ID, {
        status: STATUS_DONE,
      });
    } catch (error) {
      updateTask(FILEPROCESS_TASK_ID, {
        status: STATUS_FAILED,
        message: t('apiProgressError'),
      });
      console.error(error);
    }
  }, [
    updateTask,
    distributionEntry,
    apiDistributionEntry,
    updateApiDistribution,
    processFiles,
    t,
  ]);

  return [refreshAPI];
};

export default useRefreshAPI;
