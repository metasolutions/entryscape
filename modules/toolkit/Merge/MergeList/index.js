/* eslint-disable react/forbid-prop-types */
/* eslint-disable react/jsx-props-no-spreading */
import PropTypes from 'prop-types';
import { IconButton, Typography } from '@mui/material';
import { CheckCircle } from '@mui/icons-material';
import './index.scss';

export const MergeList = ({ title, children }) => (
  <>
    <Typography
      variant="subtitle1"
      component="h4"
      className="esreMergeList__header"
    >
      {title}
    </Typography>
    {children}
  </>
);
MergeList.propTypes = {
  title: PropTypes.string.isRequired,
  children: PropTypes.node,
};

const ActionButton = ({ icon, ...props }) => (
  <IconButton {...props}>{icon}</IconButton>
);
ActionButton.propTypes = {
  icon: PropTypes.node.isRequired,
  children: PropTypes.node,
};

export const MergeListItem = ({ value, actionButtonProps }) => (
  <div className="esreMergeListItem">
    <div className="esreMergeListItem__container">
      <CheckCircle className="esreMergeListItem__icon" />
      <Typography aria-live="polite">{value}</Typography>
    </div>
    {actionButtonProps && (
      <ActionButton
        className="esreMergeListItem__button"
        {...actionButtonProps}
      />
    )}
  </div>
);
MergeListItem.propTypes = {
  value: PropTypes.string.isRequired,
  actionButtonProps: PropTypes.object,
};
