import PropTypes from 'prop-types';
import React from 'react';
import { IconButton } from '@mui/material';
import Tooltip from 'commons/components/common/Tooltip';
import './ControlButton.scss';

export const ControlButton = ({
  Icon,
  active,
  ariaLabel,
  title,
  onClick,
  ...props
}) => (
  <Tooltip title={title}>
    <IconButton
      className={
        active
          ? 'escoControlButton escoControlButton--active'
          : 'escoControlButton'
      }
      onClick={onClick}
      aria-label={ariaLabel}
      {...props}
    >
      <Icon sx={{ height: '18px', width: '18px' }} />
    </IconButton>
  </Tooltip>
);

ControlButton.propTypes = {
  Icon: PropTypes.elementType,
  iconUrl: PropTypes.string,
  active: PropTypes.bool,
  ariaLabel: PropTypes.string,
  title: PropTypes.string,
  onClick: PropTypes.func,
};
