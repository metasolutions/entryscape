import { useUserState } from 'commons/hooks/useUser';
import { getContextSearchQuery } from 'commons/util/solr/context';
import { CONTEXT_TYPE_WORKBENCH } from 'commons/util/context';
import {
  EntryListView,
  useSolrQuery,
  TITLE_COLUMN,
  MODIFIED_COLUMN,
  TOGGLE_CONTEXT_COLUMN,
  ACTIONS_GROUP_COLUMN,
} from 'commons/components/EntryListView';
import {
  LIST_ACTION_INFO,
  LIST_ACTION_EDIT,
} from 'commons/components/EntryListView/actions';
import { getPathFromViewName } from 'commons/util/site';
import {
  withListModelProviderAndLocation,
  listPropsPropType,
} from 'commons/components/ListView';
import React, { useMemo } from 'react';
import { applyTitleOrContextnameParams } from 'commons/util/solr';
import ListEditEntryDialog from 'commons/components/EntryListView/dialogs/ListEditEntryDialog';
import { nlsBundles, listActions } from './actions';

const ProjectsView = ({ listProps }) => {
  const { userEntry, userInfo } = useUserState();
  const queryParams = useMemo(
    () =>
      getContextSearchQuery(
        { contextType: CONTEXT_TYPE_WORKBENCH },
        { userEntry, userInfo }
      ),
    [userEntry, userInfo]
  );

  const { entries, ...queryResults } = useSolrQuery({
    ...queryParams,
    applyQueryParams: applyTitleOrContextnameParams,
  });

  return (
    <EntryListView
      {...queryResults}
      {...listProps}
      entries={entries}
      nlsBundles={nlsBundles}
      getListItemProps={({ entry }) => ({
        to: getPathFromViewName('workbench__overview', {
          contextId: entry.getId(),
        }),
      })}
      listActions={listActions}
      listActionsProps={{
        nlsBundles,
        numberOfProjects: entries.length,
      }}
      columns={[
        {
          ...TOGGLE_CONTEXT_COLUMN,
          getProps: ({ entry, translate }) => ({
            entry,
            publicTooltip: translate('publicWorkspaceTitle'),
            privateTooltip: translate('privateWorkspaceTitle'),
            noAccess: translate('workspaceSharingNoAccess'),
          }),
        },
        { ...TITLE_COLUMN, xs: 7 },
        MODIFIED_COLUMN,
        {
          ...ACTIONS_GROUP_COLUMN,
          actions: [
            LIST_ACTION_INFO,
            {
              ...LIST_ACTION_EDIT,
              nlsBundles,
              formTemplateId: 'esc:Context',
              Dialog: ListEditEntryDialog,
            },
          ],
        },
      ]}
    />
  );
};

ProjectsView.propTypes = {
  listProps: listPropsPropType,
};

export default withListModelProviderAndLocation(ProjectsView);
