import OrganizationsView from 'harvest/OrganizationsView';
import InformationView from 'harvest/Information/InformationView';
import LinkCheckReportView from 'harvest/LinkCheckReport/View';
import StatisticsView from 'harvest/Statistics/View';
import StatusView from 'harvest/Status/View';
import NotificationsView from 'harvest/Notifications/View';
import { ORGANIZATION_OVERVIEW_NAME } from './config';

export default {
  modules: [
    {
      name: 'register',
      productName: { en: 'Organizations', sv: 'Organisationer', de: 'Quellen' },
      title: {
        en: 'Organizations',
        sv: 'Organisationer',
        de: 'Quellen',
      },
      startView: 'harvest__list', // compulsory,
      asCrumb: true,
      sidebar: true,
      text: {
        sv: 'Registera organisationer och deras kataloger som ska skördas',
        en: 'Register organizations and their catalogs to be harvested',
        de: 'Registrieren von Katalogen die geharvested werden sollen',
      },
    },
  ],
  views: [
    {
      name: 'harvest__list',
      class: OrganizationsView,
      title: {
        en: 'Harvesting sources',
        sv: 'Skördningskällor',
        de: 'Harvesting-Quellen',
      },
      hidden: true,
      route: '/organization',
      module: 'register',
    },
    {
      name: ORGANIZATION_OVERVIEW_NAME,
      class: StatusView,
      title: {
        en: 'Status',
        sv: 'Status',
        de: 'Status',
      },
      parent: 'harvest__list',
      route: '/organization/:contextId/status',
      module: 'register',
      // constructorParams: { inDialog: false, tab: 'status' },
      // navbar: false,
      public: true,
    },
    {
      name: 'harvest__org__information',
      class: InformationView,
      title: {
        en: 'Information',
        sv: 'Information',
        de: 'Information',
      },
      parent: 'harvest__list',
      route: '/organization/:contextId/information',
      module: 'register',
      // constructorParams: { inDialog: false, tab: 'information' },
      // navbar: false,
      public: true,
    },
    {
      name: 'harvest__org__statistics',
      class: StatisticsView,
      title: {
        en: 'Statistics',
        sv: 'Statistik',
        de: 'Statistik',
      },
      parent: 'harvest__list',
      route: '/organization/:contextId/statistics',
      module: 'register',
      // constructorParams: { inDialog: false, tab: 'statistics' },
      // navbar: false,
      public: true,
    },
    {
      name: 'harvest__org__linkCheckReport',
      class: LinkCheckReportView,
      title: {
        en: 'Link check report',
        sv: 'Länkkontroll\u00ADrapport',
        de: 'Link-Prüfbericht',
      },
      parent: 'harvest__list',
      route: '/organization/:contextId/linkchecker',
      module: 'register',
      // constructorParams: { inDialog: false, tab: 'linkCheckReport' },
      // navbar: false,
      // ! disabled: !config.registry?.includeLinkCheckReport, // this is needed
      public: true,
    },
    {
      name: 'harvest__org__notifications',
      class: NotificationsView,
      title: {
        en: 'Notifications',
        sv: 'Notifieringar',
        de: 'Benach\u00ADrichtigungen',
      },
      parent: 'harvest__list',
      route: '/organization/:contextId/notifications',
      module: 'register',
      restrictTo: 'admin',
    },
  ],
};
