import PropTypes from 'prop-types';
import { ListItemText } from 'commons/components/ListView';
import { getFullLengthLabel } from 'commons/util/rdfUtils';
import { Tag, tagsPropType } from 'commons/components/tags';
import { entryPropType } from 'commons/util/entry';

const EntryListItemText = ({
  entry,
  translate,
  primary: primaryProp,
  tags = [],
  ...props
}) => {
  const primary =
    primaryProp ||
    getFullLengthLabel(entry) ||
    translate('unnamedEntity', entry.getId());
  const tagsArray = Array.isArray(tags) ? tags : [tags];
  const tagElements = tagsArray.map(
    ({ id, nlsBundles, labelNlsKey, isVisible }) =>
      isVisible(entry) ? (
        <Tag
          id={id}
          key={id}
          nlsBundles={nlsBundles}
          labelNlsKey={labelNlsKey}
        />
      ) : null
  );
  return <ListItemText primary={primary} tags={tagElements} {...props} />;
};

EntryListItemText.propTypes = {
  entry: entryPropType,
  translate: PropTypes.func,
  primary: PropTypes.string,
  tags: tagsPropType,
};

export default EntryListItemText;
