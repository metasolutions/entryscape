import { useCallback } from 'react';
import { useParams } from 'react-router-dom';
import useNavigate from 'commons/components/router/useNavigate';
import useGetMainDialog from 'commons/hooks/useGetMainDialog';
import { getPathFromViewName } from 'commons/util/site';
import { useTranslation } from 'commons/hooks/useTranslation';
import esmoCommonsNLS from 'models/nls/esmoCommons.nls';
import esmoFormsNLS from 'models/nls/esmoForms.nls';
import escoDialogsNLS from 'commons/nls/escoDialogs.nls';
import useAsync from 'commons/hooks/useAsync';

const NLS_BUNDLES = [esmoFormsNLS, esmoCommonsNLS, escoDialogsNLS];

const useRemoveFormDialog = (
  formEntry,
  onSuccess = () => {},
  onFail = () => {}
) => {
  const translate = useTranslation(NLS_BUNDLES);
  const { navigate } = useNavigate();
  const viewParams = useParams();
  const { status: removeStatus, runAsync } = useAsync();
  const { getConfirmationDialog } = useGetMainDialog();
  const formsPath = getPathFromViewName('models__forms', viewParams);

  const removeFormEntry = useCallback(async () => {
    const navigateToFormsOverview = () => navigate(formsPath);

    const proceed = await getConfirmationDialog({
      content: translate('confirmRemoveMessage'),
      affirmLabel: translate('remove'),
      rejectLabel: translate('cancel'),
    });
    if (!proceed) return;
    runAsync(
      formEntry
        .del()
        .then(() => navigateToFormsOverview())
        .then(onSuccess)
        .catch(onFail)
    );
  }, [
    formEntry,
    getConfirmationDialog,
    translate,
    navigate,
    formsPath,
    runAsync,
  ]);

  return { removeFormEntry, removeStatus };
};

export default useRemoveFormDialog;
