import PropTypes from 'prop-types';
import { Paper, Button, Typography, Grid, Breadcrumbs } from '@mui/material';
import {
  useTranslation,
  nlsBundlesPropType,
} from 'commons/hooks/useTranslation';
import {
  List,
  ListHeader,
  ListHeaderItemText,
  ListView,
  ListItem,
  ListItemText,
  useListQuery,
  ListActions,
  ListSearch,
  ListPagination,
} from 'commons/components/ListView';
import { getGeonameLabelProperty } from './util';
import './showDialog.scss';

const LocationHierarchy = ({
  hierarchy,
  handleLocationChange,
  geonameId,
  translate,
}) => {
  const labelProperty = getGeonameLabelProperty();
  return (
    <Breadcrumbs aria-label={translate('geographicalHierarchy')}>
      {hierarchy.map((item, index) =>
        index !== hierarchy.length - 1 ? (
          <Button
            key={item.geonameId}
            variant="text"
            color="primary"
            onClick={() => handleLocationChange(item.geonameId)}
          >
            {item[labelProperty]}
          </Button>
        ) : (
          <Typography
            key={item.geonameId}
            aria-current={item[labelProperty]}
            id={geonameId}
            aria-live="polite"
            color="secondary"
            variant="body1"
          >
            {item[labelProperty]}
          </Typography>
        )
      )}
    </Breadcrumbs>
  );
};

LocationHierarchy.propTypes = {
  hierarchy: PropTypes.arrayOf(
    PropTypes.shape({
      geonameId: PropTypes.number,
      name: PropTypes.string,
    })
  ),
  handleLocationChange: PropTypes.func,
  geonameId: PropTypes.number,
  translate: PropTypes.func,
};

const SubdivisionsList = ({
  subdivisions,
  currentLocationName,
  handleLocationChange,
  translate,
  status,
}) => {
  const labelProperty = getGeonameLabelProperty();
  const {
    result: items,
    size,
    search,
  } = useListQuery({
    items: subdivisions,
    searchFields: ['name'],
  });
  return (
    <ListView size={size} search={search} status={status}>
      <ListActions>
        <ListSearch />
      </ListActions>
      <ListHeader>
        <ListHeaderItemText
          text={translate('geoNameSubdivision', currentLocationName)}
          xs={12}
        />
      </ListHeader>
      <List>
        {items.map((item) => (
          <ListItem key={item.geonameId}>
            <ListItemText primary={item[labelProperty]} xs={11} />
            <Grid item xs={1} xl={false}>
              <Button
                variant="text"
                onClick={() => handleLocationChange(item.geonameId)}
              >
                {translate('geoNameSelect')}
              </Button>
            </Grid>
          </ListItem>
        ))}
      </List>
      <ListPagination />
    </ListView>
  );
};

SubdivisionsList.propTypes = {
  subdivisions: PropTypes.arrayOf(
    PropTypes.shape({
      geonameId: PropTypes.number,
      name: PropTypes.string,
    })
  ),
  handleLocationChange: PropTypes.func,
  currentLocationName: PropTypes.string,
  status: PropTypes.string,
  translate: PropTypes.func,
};

const CurrentLocationDetails = ({ currentLocation: [current], translate }) => {
  const labelProperty = getGeonameLabelProperty();
  return (
    <Paper elevation={0}>
      <Grid
        container
        classes={{ root: 'escoGeonamesChooser__expandedGrid' }}
        direction="row"
        alignItems="flex-end"
      >
        <Grid item container direction="column" xs={10}>
          <Grid item xs={12}>
            <Typography color="primary" variant="h1">
              {current[labelProperty]}
            </Typography>
          </Grid>
          <Grid item xs={12}>
            <Typography variant="body1">
              <span>{`${translate('latitude')}: `}</span>
              {current.lat}
            </Typography>
          </Grid>
          <Grid item xs={12}>
            <Typography variant="body1">
              <span>{`${translate('longitude')}: `}</span>
              {current.lng}
            </Typography>
          </Grid>
          <Grid item xs={12}>
            <Typography variant="body1">
              <span>{`${translate('population')}: `}</span>
              {current.population}
            </Typography>
          </Grid>
        </Grid>
      </Grid>
    </Paper>
  );
};

CurrentLocationDetails.propTypes = {
  currentLocation: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string,
      lat: PropTypes.string,
      lng: PropTypes.string,
      population: PropTypes.number,
    })
  ),
  translate: PropTypes.func,
};

const LocationSelect = ({
  locations,
  geonameId,
  handleLocationChange,
  nlsBundles,
  status,
}) => {
  const { currentSelection, hierarchy, children } = locations;
  const translate = useTranslation(nlsBundles);
  const hasSubdivisions = Boolean(children?.length);
  const labelProperty = getGeonameLabelProperty();

  return (
    <>
      <div className="escoGeonamesChooser__paper">
        {hierarchy && (
          <LocationHierarchy
            hierarchy={hierarchy}
            currentLocation={currentSelection}
            handleLocationChange={handleLocationChange}
            geonameId={geonameId}
            translate={translate}
          />
        )}
        {currentSelection && (
          <CurrentLocationDetails
            currentLocation={currentSelection}
            translate={translate}
          />
        )}
      </div>
      {currentSelection && hasSubdivisions && (
        <SubdivisionsList
          subdivisions={children}
          currentLocationName={currentSelection[0][labelProperty]}
          handleLocationChange={handleLocationChange}
          translate={translate}
          status={status}
        />
      )}
    </>
  );
};

LocationSelect.propTypes = {
  handleLocationChange: PropTypes.func,
  geonameId: PropTypes.number,
  locations: PropTypes.shape({
    currentSelection: PropTypes.arrayOf(
      PropTypes.shape({ toponymName: PropTypes.string })
    ),
    hierarchy: PropTypes.arrayOf(PropTypes.shape({})),
    children: PropTypes.arrayOf(PropTypes.shape({})),
  }),
  nlsBundles: nlsBundlesPropType,
  status: PropTypes.string,
};

export default LocationSelect;
