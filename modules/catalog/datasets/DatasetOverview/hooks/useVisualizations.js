import { useCallback, useEffect, createContext, useContext } from 'react';
import PropTypes from 'prop-types';
import { Entry } from '@entryscape/entrystore-js';
import config from 'config';
import useAsync from 'commons/hooks/useAsync';
import { getVisualizations } from '../utils/visualizations';

const VisualizationsContext = createContext();

export const VisualizationsProvider = ({ datasetEntry, ...props }) => {
  const { data: visualizations, runAsync, status } = useAsync(null);

  const refreshVisualizations = useCallback(() => {
    if (config.get('catalog.includeVisualizations')) {
      runAsync(getVisualizations(datasetEntry));
    }
  }, [datasetEntry, runAsync]);

  useEffect(() => {
    refreshVisualizations();
  }, [refreshVisualizations]);

  return (
    <VisualizationsContext.Provider
      value={{ visualizations, refreshVisualizations, status }}
      {...props}
    />
  );
};

VisualizationsProvider.propTypes = {
  datasetEntry: PropTypes.instanceOf(Entry),
};

export const useVisualizations = () => {
  const context = useContext(VisualizationsContext);
  if (context === undefined)
    throw new Error(
      'Visualizations context must be used within context provider'
    );
  return context;
};

export const withVisualizationsProvider = (Component) => ({
  entry,
  ...props
}) => (
  <VisualizationsProvider datasetEntry={entry}>
    <Component entry={entry} {...props} />
  </VisualizationsProvider>
);
