import { AddToPhotos as ExtendsIcon } from '@mui/icons-material';
import * as ns from 'models/utils/ns';
import escoListNLS from 'commons/nls/escoList.nls';
import esmoFieldsNLS from 'models/nls/esmoFields.nls';
import esmoCommonsNLS from 'models/nls/esmoCommons.nls';
import escoDialogsNLS from 'commons/nls/escoDialogs.nls';
import { FIELD_TITLE, FIELD_EXTENDS } from 'models/utils/fieldDefinitions';
import { ExtendsEditor } from 'models/editors/ExtendsEditor';
import ExtendDialog from 'models/components/ExtendDialog';
import { TYPE_FILTER } from 'models/utils/filterDefinitions';
import { FIELD_TYPE_CHOICES } from 'models/utils/choiceDefinitions';
import { ANY_FILTER_ITEM } from 'commons/components/filters/utils/filterDefinitions';
import CreateFieldDialog from '../dialogs/CreateFieldDialog';
import { copyExtendedFieldMetadata } from '../utils/fields';

export const nlsBundles = [
  esmoFieldsNLS,
  esmoCommonsNLS,
  escoListNLS,
  escoDialogsNLS,
];

const EXTEND_FIELDS = [
  { ...FIELD_TITLE },
  {
    ...FIELD_EXTENDS,
    Editor: ExtendsEditor,
    rdfType: ns.RDF_TYPE_FIELD,
    mandatory: true,
  },
];

export const listActions = [
  {
    id: 'extend',
    Dialog: ExtendDialog,
    beforeCreate: copyExtendedFieldMetadata,
    labelNlsKey: 'extend',
    tooltipNlsKey: 'extendFieldTitle',
    icon: <ExtendsIcon />,
    fields: EXTEND_FIELDS,
    headerNlsKey: 'extendFieldTitle',
    nlsBundles,
  },
  {
    id: 'create',
    Dialog: CreateFieldDialog,
    labelNlsKey: 'createButtonLabel',
    tooltipNlsKey: 'createFieldTitle',
  },
];

export const filters = [
  {
    ...TYPE_FILTER,
    items: [ANY_FILTER_ITEM, ...FIELD_TYPE_CHOICES],
    id: 'fields-filter',
  },
];
