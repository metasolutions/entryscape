import { PropTypes } from 'prop-types';
import BuiltinIcon from '@mui/icons-material/Settings';
import CustomIcon from '@mui/icons-material/Widgets';
import { entryPropType } from 'commons/util/entry';
import {
  LIST_ACTION_EDIT,
  LIST_ACTION_INFO,
} from 'commons/components/EntryListView/actions';
import {
  MODIFIED_COLUMN,
  TITLE_COLUMN,
  ACTIONS_GROUP_COLUMN,
} from 'commons/components/EntryListView';
import { ListItem, ListItemIcon, REFRESH } from 'commons/components/ListView';
import { getLabel } from 'commons/util/rdfUtils';
import { getPathFromViewName } from 'commons/util/site';
import {
  nlsBundlesPropType,
  useTranslation,
} from 'commons/hooks/useTranslation';
import { useCallback } from 'react';
import {
  getNamespaceAbbreviation,
  getNamespaceUri,
  isBuiltin,
} from '../utils/namespaceMetadata';
import NamespaceEditDialog from '../dialogs/NamespaceEditDialog';

const NamespaceListItemIcon = ({ entry, translate }) => {
  const builtin = isBuiltin(entry);
  return (
    <ListItemIcon
      Icon={builtin ? BuiltinIcon : CustomIcon}
      tooltip={
        builtin
          ? translate('builtinTooltipLabel')
          : translate('customTooltipLabel')
      }
    />
  );
};
NamespaceListItemIcon.propTypes = {
  entry: entryPropType,
  translate: PropTypes.func,
};

const renderColumn = (
  {
    Component,
    getProps = () => ({}),
    isVisible = () => true,
    headerNlsKey: _,
    ...column
  },
  entry,
  translate
) =>
  isVisible({ entry }) ? (
    <Component
      key={column.id}
      {...column}
      {...getProps({ entry, translate })}
    />
  ) : null;

const NamespacesListItem = ({
  entry: namespaceEntry,
  infoAction,
  nlsBundles,
  dispatch,
}) => {
  const translate = useTranslation(nlsBundles);
  const refresh = useCallback(() => dispatch({ type: REFRESH }), [dispatch]);

  const columns = [
    {
      id: 'icon',
      xs: 1,
      Component: NamespaceListItemIcon,
      getProps: ({ entry }) => ({ entry, translate }),
    },
    {
      ...TITLE_COLUMN,
      getProps: ({ entry }) => ({
        primary: `${getNamespaceAbbreviation(entry)} - ${getLabel(entry)}`,
        secondary: getNamespaceUri(entry),
      }),
      xs: 8,
    },
    MODIFIED_COLUMN,
    {
      ...ACTIONS_GROUP_COLUMN,
      justifyContent: 'flex-start',
      actions: [
        {
          ...LIST_ACTION_INFO,
          nlsBundles,
          ...infoAction,
        },
        {
          ...LIST_ACTION_EDIT,
          nlsBundles,
          isVisible: ({ entry }) => !isBuiltin(entry),
          Dialog: NamespaceEditDialog,
          onEntryEdit: refresh,
        },
      ],
    },
  ];
  return (
    <ListItem
      key={`${namespaceEntry.getId()}-${namespaceEntry.getContext().getId()}`}
      to={getPathFromViewName('models__namespaces__namespace', {
        contextId: namespaceEntry.getContext().getId(),
        entryId: namespaceEntry.getId(),
      })}
    >
      {columns.map((column) => renderColumn(column, namespaceEntry, translate))}
    </ListItem>
  );
};

NamespacesListItem.propTypes = {
  entry: entryPropType,
  dispatch: PropTypes.func,
  infoAction: PropTypes.shape({}),
  nlsBundles: nlsBundlesPropType,
};

export default NamespacesListItem;
