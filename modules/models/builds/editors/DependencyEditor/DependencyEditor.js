import { Fragment, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import {
  IconButton,
  Collapse,
  Grid,
  ListItem,
  Select,
  MenuItem,
} from '@mui/material';
import {
  ChevronRight as ShowIcon,
  ExpandMore as HideIcon,
} from '@mui/icons-material';
import { Entry, Context } from '@entryscape/entrystore-js';
import { useTranslation } from 'commons/hooks/useTranslation';
import esmoBuildsNLS from 'models/nls/esmoBuilds.nls';
import { Fields } from 'commons/components/forms/editors';
import {
  List,
  ListItemAction,
  ListItemText,
  ListView,
} from 'commons/components/ListView';
import { getLabel } from 'commons/util/rdfUtils';
import Tooltip from 'commons/components/common/Tooltip';
import './DependencyEditor.scss';
import { OverviewListPlaceholder } from 'commons/components/overview';
import useAsync from 'commons/hooks/useAsync';
import { getDependencyItems } from '../../utils/dependencies';

const choices = [
  {
    labelNlsKey: 'includeLabel',
    value: true,
  },
  {
    labelNlsKey: 'excludeLabel',
    value: false,
  },
];

const DependencyListItem = ({ translate, onChange, dependency }) => {
  const [expand, setExpand] = useState(false);
  const { include, items, contextEntry, labelNlsKey } = dependency;

  const getBuildItemLabel = ({ entry, inlineEntry }) => {
    if (!inlineEntry) return getLabel(entry);
    return `${getLabel(entry)} + ${getLabel(inlineEntry)}`;
  };

  return (
    <>
      <ListItem divider sx={{ paddingLeft: 0, paddingRight: 0 }}>
        <Grid container>
          <Grid item xs={1}>
            <Tooltip
              title={
                expand ? translate('hideTooltip') : translate('showTooltip')
              }
            >
              <IconButton onClick={() => setExpand(!expand)}>
                {expand ? <HideIcon /> : <ShowIcon />}
              </IconButton>
            </Tooltip>
          </Grid>
          <ListItemText
            xs={5}
            primary={
              labelNlsKey ? translate(labelNlsKey) : getLabel(contextEntry)
            }
          />
          <ListItemText xs={2} secondary={items.length} />
          <ListItemAction xs={4} justifyContent="end">
            {!labelNlsKey ? (
              <Select
                value={include}
                size="small"
                onChange={({ target }) =>
                  onChange(contextEntry.getResource(true), !target.value)
                }
              >
                {choices.map((choice) => (
                  <MenuItem key={choice.value} value={choice.value}>
                    {translate(choice.labelNlsKey)}
                  </MenuItem>
                ))}
              </Select>
            ) : (
              ''
            )}
          </ListItemAction>
        </Grid>
      </ListItem>
      <ListItem sx={{ padding: 0 }}>
        <Collapse
          in={expand}
          timeout="auto"
          unmountOnExit
          sx={{ width: '100%' }}
        >
          <List>
            <ListItem sx={{ paddingLeft: 0, paddingRight: 0 }}>
              <Grid container>
                {items.map((item) => (
                  <Fragment key={item.entry.getId()}>
                    <ListItemText xs={1} secondary="" />
                    <ListItemText xs={11} secondary={getBuildItemLabel(item)} />
                  </Fragment>
                ))}
              </Grid>
            </ListItem>
          </List>
        </Collapse>
      </ListItem>
    </>
  );
};

DependencyListItem.propTypes = {
  translate: PropTypes.func,
  onChange: PropTypes.func,
  dependency: PropTypes.shape({
    items: PropTypes.arrayOf(PropTypes.shape({})),
    contextEntry: PropTypes.instanceOf(Entry),
    include: PropTypes.bool,
    labelNlsKey: PropTypes.string,
  }),
};

export const DependencyEditor = ({
  label,
  mandatory,
  buildItems,
  currentContext,
  excludedContexts,
  buildItemsStatus,
  onChange,
}) => {
  const translate = useTranslation(esmoBuildsNLS);
  const { data: dependencies, runAsync, status } = useAsync();

  useEffect(() => {
    if (buildItemsStatus !== 'resolved') return;
    runAsync(getDependencyItems(buildItems, currentContext, excludedContexts));
  }, [
    buildItemsStatus,
    runAsync,
    buildItems,
    currentContext,
    excludedContexts,
  ]);

  return (
    <Fields
      mandatory={mandatory}
      labelId="select-dependency-editor"
      label={label}
      max={1}
    >
      <ListView
        renderPlaceholder={() => (
          <OverviewListPlaceholder
            label={translate('dependenciesListPlaceholder')}
          />
        )}
        status={buildItemsStatus === 'resolved' ? status : buildItemsStatus}
        size={dependencies?.length ?? -1}
      >
        <List renderLoader={(Loader) => <Loader height="10vh" />}>
          {dependencies?.map((dependency) => (
            <DependencyListItem
              key={dependency.contextEntry.getId()}
              translate={translate}
              onChange={onChange}
              dependency={dependency}
            />
          ))}
        </List>
      </ListView>
    </Fields>
  );
};

DependencyEditor.propTypes = {
  label: PropTypes.string,
  mandatory: PropTypes.bool,
  currentContext: PropTypes.instanceOf(Context),
  excludedContexts: PropTypes.arrayOf(PropTypes.instanceOf(Context)),
  buildItems: PropTypes.arrayOf(PropTypes.shape({})),
  buildItemsStatus: PropTypes.string,
  onChange: PropTypes.func,
};
