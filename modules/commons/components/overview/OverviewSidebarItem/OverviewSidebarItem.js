import { ListItem } from '@mui/material';
import PropTypes from 'prop-types';
import './OverviewSidebarItem.scss';

const OverviewSidebarItem = ({ children }) => (
  <ListItem className="escoOverviewSidebarItem">{children}</ListItem>
);

OverviewSidebarItem.propTypes = {
  children: PropTypes.node.isRequired,
};

export default OverviewSidebarItem;
