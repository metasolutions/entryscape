import PropTypes from 'prop-types';
import { graphPropType } from 'commons/util/entry';
import { useState, useMemo } from 'react';
import { Grid } from '@mui/material';
import { Entry } from '@entryscape/entrystore-js';
import { useTranslation } from 'commons/hooks/useTranslation';
import esadEntityTypeNLS from 'admin/nls/esadEntityType.nls';
import {
  Field,
  Fields,
  SearchButton,
  TextField,
} from 'commons/components/forms/editors';
import { CONFIG_CONTEXT } from 'admin/configs/utils/namespace';
import { nameToURI } from 'commons/types/utils/uri';
import { getEntityTypesByFilter } from 'admin/configs/entitytypes/utils/entityTypeQuery';
import EntityTypeChooser from './EntityTypeChooser';

export const EntityTypeSelectEditor = ({
  graph,
  resourceURI,
  property,
  label,
  mandatory,
  entityTypeEntries,
  dialogProps,
}) => {
  const entityTypes = useMemo(() => {
    return getEntityTypesByFilter(entityTypeEntries);
  }, [entityTypeEntries]);
  const [selectedEntityType, setSelectedEntityType] = useState(
    entityTypes.find(
      (entityType) =>
        nameToURI(entityType.id, CONFIG_CONTEXT) ===
        graph.findFirstValue(resourceURI, property)
    )
  );
  const [showEntityTypeChooser, setShowEntityTypeChooser] = useState(false);
  const t = useTranslation(esadEntityTypeNLS);

  const onSelect = (entityTypeId) => {
    graph.findAndRemove(resourceURI, property);
    graph.add(resourceURI, property, nameToURI(entityTypeId, CONFIG_CONTEXT));
    setSelectedEntityType(
      entityTypes.find((entityType) => entityType.id === entityTypeId)
    );
  };

  const onClear = () => {
    graph.findAndRemove(resourceURI, property);
    setSelectedEntityType('');
  };

  return (
    <Fields
      required={mandatory}
      labelId="select-entitytype-editor"
      label={label}
      max={1}
    >
      <Field size={1} disabled={!selectedEntityType} onClear={onClear}>
        <TextField
          disabled
          required={mandatory}
          xs={8}
          inputProps={{ 'aria-labelledby': 'select-entitytype-editor' }}
          size="small"
          value={selectedEntityType ? selectedEntityType.label : ''}
        />
        <Grid item xs={4}>
          <SearchButton
            tooltip={t('selectEntityTypeTooltip')}
            onClick={() => setShowEntityTypeChooser(true)}
          />
        </Grid>
      </Field>
      {showEntityTypeChooser ? (
        <EntityTypeChooser
          closeDialog={() => {
            setShowEntityTypeChooser(false);
          }}
          onSelect={onSelect}
          entityTypes={entityTypes}
          {...dialogProps}
        />
      ) : null}
    </Fields>
  );
};

EntityTypeSelectEditor.propTypes = {
  graph: graphPropType,
  resourceURI: PropTypes.string,
  property: PropTypes.string,
  label: PropTypes.string,
  mandatory: PropTypes.bool,
  entityTypeEntries: PropTypes.arrayOf(PropTypes.instanceOf(Entry)),
  dialogProps: PropTypes.shape({}),
};
