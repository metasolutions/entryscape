import PropTypes from 'prop-types';
import { Checkbox, FormControlLabel } from '@mui/material';
// eslint-disable-next-line max-len
import { useVisualizationState } from 'catalog/datasets/DatasetOverview/hooks/useVisualizationState';

const MapControlField = ({ property, label, onChange = () => {} }) => {
  const { fieldsState, updateFieldsState } = useVisualizationState();

  return (
    <FormControlLabel
      id={`map-${property}-control-label`}
      control={
        <Checkbox
          checked={fieldsState?.[property] === 'true' || false}
          onChange={({ target }) => {
            updateFieldsState({
              [property]: target.checked.toString(),
            });
            onChange(target.checked.toString());
          }}
        />
      }
      label={label}
    />
  );
};

MapControlField.propTypes = {
  property: PropTypes.string.isRequired,
  label: PropTypes.string,
  onChange: PropTypes.func,
};

export default MapControlField;
