import PropTypes from 'prop-types';
import {
  useMapContext,
  withMapProvider,
} from 'commons/components/Map/hooks/useMapContext';
import {
  LayerSwitcherControl,
  LayerLegendControl,
} from 'commons/components/Map/controls';
import { useMemo, useState } from 'react';
import ScaleLineControl from 'commons/components/Map/controls/ScaleLineControl';
import AttributionControl from 'commons/components/Map/controls/AttributionControl';
import ControlGroup from 'commons/components/Map/controls/ControlGroup';
import VisualizationMapService from '../../VisualizationMapService';
import './WmsPreview.scss';

const WmsPreview = ({ fields }) => {
  const [{ map }] = useMapContext();
  const [, refresh] = useState(0);

  const mapOptions = useMemo(() => {
    if (!fields) return null;
    return {
      extent: fields.extent,
      initialView: fields.initialView,
      basemapOptions: { visible: fields.basemap !== 'false' },
      layers: fields.layers.map((layerOptions) => ({
        ...layerOptions,
        url: fields.wmsBaseURI,
      })),
    };
  }, [fields]);

  return (
    <div className="escaWmsPreview">
      {mapOptions ? (
        <VisualizationMapService mapOptions={mapOptions}>
          {fields && map ? (
            <>
              <LayerSwitcherControl
                handleChange={refresh}
                layers={fields.layers}
              />
              {fields.legendControl === 'true' ? (
                <LayerLegendControl
                  layers={fields.layers}
                  initialExpand={false}
                />
              ) : null}
              <ControlGroup
                position="bottom-right"
                renderControls={(target) => (
                  <>
                    <AttributionControl target={target} />
                    {fields.scaleLineControl === 'true' ? (
                      <ScaleLineControl target={target} />
                    ) : null}
                  </>
                )}
              />
            </>
          ) : null}
        </VisualizationMapService>
      ) : null}
    </div>
  );
};

WmsPreview.propTypes = {
  fields: PropTypes.shape({
    extent: PropTypes.arrayOf(PropTypes.number),
    initialView: PropTypes.arrayOf(PropTypes.number),
    baseMap: PropTypes.string,
    layers: PropTypes.arrayOf(PropTypes.shape({})),
    legendControl: PropTypes.string,
    scaleLineControl: PropTypes.string,
    wmsBaseURI: PropTypes.string,
    basemap: PropTypes.string,
  }),
};

export default withMapProvider(WmsPreview);
