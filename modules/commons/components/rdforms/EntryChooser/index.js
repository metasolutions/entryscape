import { renderingContext } from '@entryscape/rdforms';
import Lookup from 'commons/types/Lookup';
import show from './showDialog';
import search from './searchChoice';
import getChoice from './getChoice';

const chooserConfiguration = {
  show,
  supportsInlineCreate(binding) {
    try {
      const item = binding.getItem();
      const constraints = item.getConstraints();
      const entityType = Lookup.getEntityTypeByConstraints(constraints)?.get();
      return entityType?.inlineCreation ?? false;
    } catch (error) {
      console.error(error);
      return false;
    }
  },
  search,
  getChoice,
};
renderingContext.chooserRegistry
  .itemtype('choice')
  .register(chooserConfiguration);
