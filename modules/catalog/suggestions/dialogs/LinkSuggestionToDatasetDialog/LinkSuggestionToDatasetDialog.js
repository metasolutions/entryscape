import PropTypes from 'prop-types';
import { useCallback } from 'react';
import ContentWrapper from 'commons/components/common/ContentWrapper';
import ListActionDialog from 'commons/components/common/dialogs/ListActionDialog';
import { useTranslation } from 'commons/hooks/useTranslation';
import escoListNLS from 'commons/nls/escoList.nls';
import { entrystore } from 'commons/store';
import { useESContext } from 'commons/hooks/useESContext';
import { RDF_TYPE_DATASET, entryPropType } from 'commons/util/entry';
import DatasetPreview from 'catalog/datasets/preview/DatasetPreview';
import {
  EntryListView,
  useSolrQuery,
  TITLE_COLUMN,
  MODIFIED_COLUMN,
  INFO_COLUMN,
} from 'commons/components/EntryListView';
import { ACTION_INFO } from 'commons/actions';
import {
  ListModelProvider,
  ListItemActionIconButton,
  ListItemButton,
} from 'commons/components/ListView';
import escaPreparations from 'catalog/nls/escaPreparations.nls';
import escaDatasetTemplateNLS from 'catalog/nls/escaDatasetTemplate.nls';
import { islinked } from '../../util';

export const nlsBundles = [
  escaPreparations,
  escaDatasetTemplateNLS,
  escoListNLS,
];

const InfoDialog = ({ entry, closeDialog }) => {
  const translate = useTranslation(nlsBundles);
  return (
    <ListActionDialog
      id="information-dialog"
      title={translate('datasetPreviewHeaderLabel')}
      closeDialog={closeDialog}
      maxWidth="md"
    >
      <ContentWrapper>
        <DatasetPreview entry={entry} />
      </ContentWrapper>
    </ListActionDialog>
  );
};

InfoDialog.propTypes = {
  entry: entryPropType,
  closeDialog: PropTypes.func,
};

const DatasetList = ({
  suggestionEntry,
  closeDialog: closeLinkToDatasetDialog,
  linkedDatasets,
  refreshSuggestions,
}) => {
  const translate = useTranslation([...nlsBundles, escoListNLS]);

  const { context } = useESContext();
  const createQuery = useCallback(
    () => entrystore.newSolrQuery().rdfType(RDF_TYPE_DATASET).context(context),
    [context]
  );
  const queryResults = useSolrQuery({ createQuery });

  const linkSuggestionToDataset = async (datasetEntry) => {
    const datasetRURI = datasetEntry.getResourceURI();
    const metadata = suggestionEntry.getMetadata();
    const suggestionRURI = suggestionEntry.getResourceURI();
    metadata.add(suggestionRURI, 'dcterms:references', datasetRURI);

    try {
      await suggestionEntry.setMetadata(metadata).commitMetadata();
      refreshSuggestions();
    } catch (err) {
      console.log(err);
    }
    closeLinkToDatasetDialog();
  };

  const getLinkedEntryProps = ({ entry: datasetEntry }) => {
    const isLinked = islinked(datasetEntry, linkedDatasets);

    return {
      entry: datasetEntry,
      onClick: () => linkSuggestionToDataset(datasetEntry),
      disabled: isLinked,
      label: isLinked
        ? translate('selectedEntityLabel')
        : translate('selectEntity'),
    };
  };

  return (
    <EntryListView
      // eslint-disable-next-line react/jsx-props-no-spreading
      {...queryResults}
      nlsBundles={nlsBundles}
      viewPlaceholderProps={{ label: translate('linkDatasetEmptyList') }}
      getListItemProps={({ entry }) => ({
        action: { ...ACTION_INFO, entry, Dialog: InfoDialog },
      })}
      columns={[
        TITLE_COLUMN,
        MODIFIED_COLUMN,
        {
          ...INFO_COLUMN,
          title: translate('infoEntry'),
          Component: ListItemActionIconButton,
          getProps: ({ entry }) => ({
            action: { ...ACTION_INFO, entry, Dialog: InfoDialog },
          }),
        },
        {
          id: 'link-to-dataset',
          xs: 2,
          Component: ListItemButton,
          getProps: getLinkedEntryProps,
        },
      ]}
    />
  );
};

DatasetList.propTypes = {
  closeDialog: PropTypes.func,
  suggestionEntry: entryPropType,
  linkedDatasets: PropTypes.arrayOf(PropTypes.shape({})),
  refreshSuggestions: PropTypes.func,
};

const LinkDatasetDialog = ({
  entry,
  closeDialog,
  linkedDatasets,
  onRefresh,
}) => {
  const translate = useTranslation(nlsBundles);

  return (
    <ListActionDialog
      id="select-linked-dataset"
      closeDialog={closeDialog}
      title={translate('linkDatasetTitle')}
      fixedHeight
    >
      <ContentWrapper>
        <ListModelProvider>
          <DatasetList
            suggestionEntry={entry}
            closeDialog={closeDialog}
            linkedDatasets={linkedDatasets}
            refreshSuggestions={onRefresh}
          />
        </ListModelProvider>
      </ContentWrapper>
    </ListActionDialog>
  );
};

LinkDatasetDialog.propTypes = {
  entry: entryPropType,
  closeDialog: PropTypes.func,
  onRefresh: PropTypes.func,
  linkedDatasets: PropTypes.arrayOf(entryPropType),
};

export default LinkDatasetDialog;
