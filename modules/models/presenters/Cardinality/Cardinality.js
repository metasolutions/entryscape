import PropTypes from 'prop-types';
import { Field, Fields, Text } from 'commons/components/forms/presenters';
import { graphPropType } from 'commons/util/entry';
import { getCardinality } from 'models/utils/cardinality';
import { useTranslation } from 'commons/hooks/useTranslation';
import esmoCommonsNls from 'models/nls/esmoCommons.nls';

export const Cardinality = ({
  graph,
  resourceURI,
  inline,
  children,
  ...fieldsProps
}) => {
  const t = useTranslation(esmoCommonsNls);
  const { includeLevel, max } = getCardinality(graph, resourceURI);
  const maxText = parseInt(max, 10) >= 0 ? `, max ${max}` : '';

  return (
    <Fields inline={inline} {...fieldsProps}>
      <Field inline={inline}>
        <Text text={`${t(`${includeLevel}Label`)}${maxText}`} />
      </Field>
      {children}
    </Fields>
  );
};

Cardinality.propTypes = {
  graph: graphPropType,
  resourceURI: PropTypes.string,
  inline: PropTypes.bool,
  children: PropTypes.node,
};
