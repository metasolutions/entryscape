import PropTypes from 'prop-types';
import { createEntry } from 'commons/util/store';
import esteCollectionNLS from 'terms/nls/esteCollection.nls';
// eslint-disable-next-line max-len
import CreateEntryByTemplateDialog from 'commons/components/EntryListView/dialogs/create/CreateEntryByTemplateDialog';
import { namespaces as ns } from '@entryscape/rdfjson';
import { useTranslation } from 'commons/hooks/useTranslation';
import { useESContext } from 'commons/hooks/useESContext';
import { RDF_TYPE_COLLECTION } from 'commons/util/entry';
import { LEVEL_RECOMMENDED } from 'commons/components/rdforms/LevelSelector';
import { useSnackbar } from 'commons/hooks/useSnackbar';

const CreateCollectionDialog = ({ onCreate, ...props }) => {
  const translate = useTranslation(esteCollectionNLS);
  const [addSnackbar] = useSnackbar();

  const { context } = useESContext();
  const prototypeEntry = createEntry(context, RDF_TYPE_COLLECTION);
  prototypeEntry
    .getMetadata()
    .add(
      prototypeEntry.getResourceURI(),
      ns.expand('rdf:type'),
      ns.expand(RDF_TYPE_COLLECTION)
    );

  return (
    <CreateEntryByTemplateDialog
      {...props}
      entry={prototypeEntry}
      formTemplateId="skosmos:conceptScheme"
      editorLevel={LEVEL_RECOMMENDED}
      nlsBundles={[esteCollectionNLS]}
      createHeaderLabel={translate('createCollectionHeader')}
      onEntryEdit={() => {
        onCreate();
        addSnackbar({ message: translate('createCollectionSuccess') });
      }}
    />
  );
};

CreateCollectionDialog.propTypes = { onCreate: PropTypes.func };

export default CreateCollectionDialog;
