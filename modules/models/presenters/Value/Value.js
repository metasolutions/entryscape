import PropTypes from 'prop-types';
import { Presenter } from 'commons/components/forms/presenters';
import { graphPropType } from 'commons/util/entry';
import { getValueURIValue } from 'models/utils/options';

export const Value = ({ graph, resourceURI, ...props }) => {
  return (
    <Presenter
      direct={false}
      graph={graph}
      resourceURI={resourceURI}
      getValue={getValueURIValue}
      {...props}
    />
  );
};

Value.propTypes = {
  graph: graphPropType,
  resourceURI: PropTypes.string,
  inline: PropTypes.bool,
  children: PropTypes.node,
};
