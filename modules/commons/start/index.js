import { Paper, Divider } from '@mui/material';
import { useUserState } from 'commons/hooks/useUser';
import { useLanguage } from 'commons/hooks/useLanguage';
import Cards from 'commons/nav/components/Cards';
import StartBanner from 'commons/nav/components/StartBanner';
import utils from 'commons/nav/utils';
import escoModules from 'commons/nls/escoModules.nls';
import { getActiveModules } from 'commons/util/config';
import { getRouteFromView } from 'commons/util/site';
import registry from 'commons/registry';
import config from 'config';

const getLabelAndTooltip = (card, language) => {
  let tooltip;

  const getProductNameLocalized = (defaultLang) => {
    if (defaultLang) {
      return typeof card.productName === 'object'
        ? card.productName[defaultLang]
        : card.productName;
    }

    if (typeof card.productName === 'object' && language in card.productName) {
      return card.productName[language];
    }

    return utils.getModuleProp(card, escoModules, 'title');
  };

  const label = getProductNameLocalized();
  if (label) {
    tooltip = `EntryScape ${getProductNameLocalized('en')}`;
  } else {
    tooltip = utils.getModuleProp(card, escoModules, 'tooltip');
  }

  return { label, tooltip };
};

const Start = () => {
  const banner = config.get('theme.startBanner');
  const theme = registry.getApp() || 'suite';
  const [language] = useLanguage();
  const { userInfo } = useUserState();
  const modules = getActiveModules(userInfo);
  const cards = modules.map((module) => ({
    ...module,
    ...getLabelAndTooltip(module, language),
    text: utils.getModuleProp(module, escoModules, 'text', true),
    name: module.name,
    icon: module.icon,
    link: module.link,
    path: getRouteFromView(module.startView),
  }));
  const bannerPresent = banner !== undefined;
  const gap = theme === 'registry' ? 0 : 10;
  const cols = theme === 'registry' ? 2 : 3;

  return (
    <Paper elevation={bannerPresent ? 3 : 0}>
      {bannerPresent && (
        <>
          <StartBanner banner={banner} />
          <Divider />
        </>
      )}
      <Cards cards={cards} gap={gap} cols={cols} appTheme={theme} />
    </Paper>
  );
};

export default Start;
