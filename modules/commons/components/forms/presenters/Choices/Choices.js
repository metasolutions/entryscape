import PropTypes from 'prop-types';
import { graphPropType } from 'commons/util/entry';
import { Graph, Statement, namespaces as ns } from '@entryscape/rdfjson';
import { RDF_PROPERTY_ORDER } from 'models/utils/ns';
import { localizeLabel } from 'commons/locale';
import { Fields } from '../Fields';
import { Field } from '../Field';
import { Text } from '../Text';

/**
 *
 * @param {object[]} choices
 * @param {string} value
 * @param {Function} translate
 * @returns {string}
 */
const getChoiceLabel = (choices, value, translate) => {
  const { label, labelNlsKey } =
    choices.find(
      ({ value: choiceValue }) => value === ns.expand(choiceValue)
    ) || {};
  return localizeLabel(label) ?? translate(labelNlsKey);
};

/**
 *
 * @param {Statement[]} statements
 * @param {Graph} graph
 * @returns {string[]}
 */
const getOrderedValues = (statements, graph) => {
  const orderedValues = statements
    .sort((statement1, statement2) => {
      return (
        graph.findFirstValue(statement1.getValue(), RDF_PROPERTY_ORDER) -
        graph.findFirstValue(statement2.getValue(), RDF_PROPERTY_ORDER)
      );
    })
    .map((statement) =>
      graph.findFirstValue(statement.getValue(), 'rdf:value')
    );
  return orderedValues;
};

export const Choices = ({
  graph,
  resourceURI,
  property,
  choices,
  ordered,
  nodetype,
  inline,
  children,
  translate,
  ...fieldsProps
}) => {
  const statements = graph.find(resourceURI, property);
  const values = ordered
    ? getOrderedValues(statements, graph)
    : statements.map((statement) => statement.getValue());

  return statements.length ? (
    <Fields inline={inline} {...fieldsProps}>
      {values.map((value, index) => {
        return (
          // eslint-disable-next-line react/no-array-index-key
          <Field key={index} inline={inline}>
            <Text
              text={choices ? getChoiceLabel(choices, value, translate) : value}
            />
          </Field>
        );
      })}
      {children}
    </Fields>
  ) : null;
};

Choices.propTypes = {
  graph: graphPropType,
  resourceURI: PropTypes.string,
  property: PropTypes.string,
  nodetype: PropTypes.string,
  inline: PropTypes.bool,
  choices: PropTypes.arrayOf(PropTypes.shape({})),
  ordered: PropTypes.bool,
  children: PropTypes.node,
  translate: PropTypes.func,
};
