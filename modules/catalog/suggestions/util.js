import config from 'config';
import { Entry } from '@entryscape/entrystore-js';
import { namespaces } from '@entryscape/rdfjson';
import { updateCatalogWithDataset } from 'catalog/datasets/DatasetOverview/utils/datasets';

/**
 * Progress bar's status
 *
 * @param {object} entry - suggestion entry.
 * @returns {object}
 */
const getChecklistProgress = (entry) => {
  if (config.get('catalog.checklist')) {
    const checklistSteps = config.get('catalog.checklist');
    let completedChecklistSteps = [];
    let mandatoryChecklistSteps = [];
    const infoEntryGraph = entry.getEntryInfo().getGraph();
    const tasks = infoEntryGraph.find(
      entry.getResourceURI(),
      'http://entrystore.org/terms/progress'
    );

    completedChecklistSteps = tasks.map((task) => task.getObject().value);
    const noOfTasksCompleted = completedChecklistSteps.length;

    mandatoryChecklistSteps = checklistSteps.filter(
      (checklistStep) => checklistStep.mandatory === true
    );
    const mandatoryTotalCount = mandatoryChecklistSteps.length;

    const mandatoryCompleted = mandatoryChecklistSteps.filter(
      (mandatoryChecklistStep) =>
        completedChecklistSteps.includes(mandatoryChecklistStep.name)
    );
    const mandatoryProgressCount = mandatoryCompleted.length;

    return {
      totalCount: checklistSteps.length,
      progressCount: noOfTasksCompleted,
      mandatoryTotalCount,
      mandatoryProgressCount,
    };
  }
  return {};
};

/**
 * progress bar's status for runtime changes
 *
 * @param {array} checklistSteps
 * @param {object} updatedTasks
 * @returns {object}
 */

const showChecklistProgress = (checklistSteps, updatedTasks) => {
  const completedChecklistSteps = Object.keys(updatedTasks).filter(
    (updatedTask) => updatedTasks[updatedTask]
  );

  const noOfTasksCompleted = completedChecklistSteps.length;

  const mandatoryChecklistSteps = checklistSteps.filter(
    (checklistStep) => checklistStep.mandatory === true
  );
  const mandatoryTotalCount = mandatoryChecklistSteps.length;

  const mandatoryCompleted = mandatoryChecklistSteps.filter(
    (mandatoryChecklistStep) =>
      completedChecklistSteps.includes(mandatoryChecklistStep.name)
  );
  const mandatoryProgressCount = mandatoryCompleted.length;

  return {
    totalCount: checklistSteps.length,
    progressCount: noOfTasksCompleted,
    mandatoryTotalCount,
    mandatoryProgressCount,
  };
};

/**
 * Loading checklist from catalog config
 *
 * @returns {Array}
 */

const getChecklistSteps = () => {
  return config.get('catalog.checklist') || [];
};

/**
 * if the entry is archived
 *
 * @param {object} entry - suggestion entry.
 * @returns {boolean}
 */

const isArchived = (entry) =>
  entry
    .getEntryInfo()
    .getGraph()
    .findFirstValue(entry.getURI(), 'store:status') ===
  namespaces.expand('esterms:archived');

/**
 * Checking if the dataset already linked with the suggestion
 *
 * @param {Entry} entry
 * @param {Array} linkedDatasets
 * @returns {boolean}
 */

const islinked = (entry, linkedDatasets) => {
  return linkedDatasets?.some(
    (linkedDataset) => linkedDataset.getId() === entry.getId()
  );
};

/**
 * Gets the checked tasks from the suggestion entry
 *
 * @param {Entry} entry
 * @param {Array} checklistSteps
 * @returns {object}
 */

const getCheckedTasks = (entry, checklistSteps) => {
  const newMetadataTasks = {};
  const infoEntryGraph = entry.getEntryInfo().getGraph();
  const tasks = infoEntryGraph.find(
    entry.getResourceURI(),
    'http://entrystore.org/terms/progress'
  );
  checklistSteps.forEach((checklistStep) => {
    const { name } = checklistStep;
    const checkedTask = tasks.find((task) => task.getObject().value === name);
    newMetadataTasks[name] = Boolean(checkedTask);
  });

  return newMetadataTasks;
};

/**
 * Adding or removing checklist tasks from entry
 *
 * @param {Entry} entry
 * @param {object} metadataTasks
 */

const addOrRemoveEntryTasks = (entry, metadataTasks) => {
  const infoEntryGraph = entry.getEntryInfo().getGraph();

  infoEntryGraph.findAndRemove(
    entry.getResourceURI(),
    'http://entrystore.org/terms/progress'
  );

  Object.keys(metadataTasks).forEach((task) => {
    if (metadataTasks[task]) {
      infoEntryGraph.addL(
        entry.getResourceURI(),
        'http://entrystore.org/terms/progress',
        task
      );
    }
  });
};

/**
 * Change the status of entry to archive/Unarchive
 *
 * @param {Entry} entry
 * @param {string} newStatus
 */

const archiveOrUnarchiveSuggestion = (entry, newStatus) => {
  const entryInfo = entry.getEntryInfo().getGraph();
  entryInfo.findAndRemove(entry.getURI(), 'store:status');
  entryInfo.add(entry.getURI(), 'store:status', newStatus);
  try {
    return entry.getEntryInfo().commit();
  } catch (err) {
    console.error(err);
  }
};

/**
 * Creates a new dataset entry and link it to the suggestion entry
 *
 * @param {*} entry
 * @param {*} graph
 * @param {*} suggestionEntry
 */
const createLinkedDatasetEntry = async (entry, graph, suggestionEntry) => {
  let datasetEntry;
  try {
    datasetEntry = await entry.setMetadata(graph).commit();
    await updateCatalogWithDataset(datasetEntry);
  } catch (error) {
    console.error('Failed to create dataset');
    throw Error(error);
  }
  suggestionEntry
    .getMetadata()
    .add(
      suggestionEntry.getResourceURI(),
      'dcterms:references',
      datasetEntry.getResourceURI()
    );
  try {
    await suggestionEntry.commitMetadata();
    return datasetEntry;
  } catch (error) {
    console.error('Failed to link to newly created data');
    throw Error(error);
  }
};

export {
  getChecklistProgress,
  getChecklistSteps,
  isArchived,
  islinked,
  showChecklistProgress,
  getCheckedTasks,
  addOrRemoveEntryTasks,
  archiveOrUnarchiveSuggestion,
  createLinkedDatasetEntry,
};
