import PropTypes from 'prop-types';
import { Grid, Typography } from '@mui/material';
import GridListTile from 'commons/components/common/GridListTile';
import './OverviewMetadataListItem.scss';

const OverviewMetadataListItem = ({ label, values }) => {
  return values.length === 0 ? null : (
    <GridListTile>
      <Grid container direction="column">
        <Grid item>
          <Typography
            variant="h6"
            component="h2"
            className="escoOverviewMetadataListItem__header"
          >
            {label}
          </Typography>
        </Grid>
        {values.map((value) => (
          <Grid key={value} item>
            <Typography>{value}</Typography>
          </Grid>
        ))}
      </Grid>
    </GridListTile>
  );
};

OverviewMetadataListItem.propTypes = {
  label: PropTypes.string,
  values: PropTypes.arrayOf(PropTypes.string),
};

export default OverviewMetadataListItem;
