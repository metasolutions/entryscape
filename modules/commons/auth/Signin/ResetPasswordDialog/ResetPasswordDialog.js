import React, { useState, useEffect, useCallback } from 'react';
import PropTypes from 'prop-types';
import {
  Button,
  TextField,
  InputAdornment,
  IconButton,
  FormHelperText,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
} from '@mui/material';
import Alert from '@mui/material/Alert';
import {
  Visibility as VisibilityIcon,
  VisibilityOff as VisibilityOffIcon,
} from '@mui/icons-material';
import config from 'config';
import escoSigninNLS from 'commons/nls/escoSignin.nls';
import { useTranslation } from 'commons/hooks/useTranslation';
import Recaptcha from 'commons/components/Recaptcha';
import Tooltip from 'commons/components/common/Tooltip';
import { entrystore } from 'commons/store';
import LoadingButton from 'commons/components/LoadingButton';
import { getPathFromViewName } from 'commons/util/site';
import configUtil from 'commons/util/config';
import { validateEmail } from 'commons/util/util';
import usePassword from '../../usePassword';

const ResetPasswordDialog = ({ isOpen, close }) => {
  const reCaptchaSiteKey = Boolean(config.get('reCaptchaSiteKey'));
  const [email, setEmail] = useState('');

  const {
    password,
    isPasswordVisible,
    passwordError,
    handleChangePassword,
    togglePasswordVisibility,
  } = usePassword();

  const [triedSubmit, setTriedSubmit] = useState(false);
  const initPristineState = {
    email: true,
    password: true,
    captcha: true,
  };
  const [pristine, setPristine] = useState(initPristineState);
  const [loading, setLoading] = useState(false);
  const [success, setSuccess] = useState(false);
  const [errors, setErrors] = useState({});

  const t = useTranslation(escoSigninNLS);

  // Separate declaration required since it needs to be named
  const passwordResetCaptchaCallback = () => {
    setPristine({ ...pristine, captcha: false });
    setErrors({ ...errors, captcha: '' });
  };

  const captchaCallbackName = 'passwordResetCaptchaCallback';
  window[captchaCallbackName] = passwordResetCaptchaCallback;

  const validate = useCallback(
    () => ({
      // eslint-disable-next-line no-nested-ternary
      email: email
        ? validateEmail(email)
          ? null
          : t('signupInvalidEmail')
        : t('emailRequired'),
    }),
    [t, email]
  );

  const validateCaptcha = useCallback(
    () => ({
      captcha:
        reCaptchaSiteKey && !window.grecaptcha.getResponse()
          ? t('captchaRequired')
          : null,
    }),
    [reCaptchaSiteKey, t]
  );

  useEffect(() => {
    if (!pristine.email) {
      setErrors((currentErrors) => ({
        ...currentErrors,
        ...validate(),
      }));
    }

    if (!pristine.password) {
      setErrors((currentErrors) => ({
        ...currentErrors,
        password: passwordError,
      }));
    }

    // Case of submitting without tinkering w/ fields
    if (triedSubmit) {
      setErrors((currentErrors) => ({
        ...currentErrors,
        ...validateCaptcha(),
        ...validate(),
        password: passwordError,
      }));
    }
  }, [
    email,
    password,
    pristine.email,
    pristine.password,
    triedSubmit,
    validateCaptcha,
    validate,
    passwordError,
  ]);

  const canRecaptchaBePristine = reCaptchaSiteKey
    ? !pristine.captcha
    : pristine.captcha;

  const canSubmit =
    !errors.email &&
    !errors.password &&
    !errors.captcha &&
    !pristine.email &&
    !pristine.password &&
    canRecaptchaBePristine;

  const resetForm = () => {
    setPristine(initPristineState);
    setErrors({});
  };

  const handleSubmit = () => {
    // password state is now handled elsewhere, this is a workaround to trigger
    // password validation if they try to submit without editing the fields
    if (!triedSubmit) handleChangePassword(password);

    setTriedSubmit(true);

    if (!canSubmit) return;

    const pwInfo = {
      email,
      password: password.trim(),
      urlsuccess:
        configUtil.getBaseUrl() + getPathFromViewName('signin').slice(1),
    };

    if (reCaptchaSiteKey) {
      pwInfo.grecaptcharesponse = window.grecaptcha.getResponse();
    }

    setLoading(true);
    entrystore
      .getREST()
      .post(`${entrystore.getBaseURI()}auth/pwreset`, JSON.stringify(pwInfo))
      .then(
        () => {
          setSuccess(true);
          resetForm();
        },
        (err) => {
          setErrors({ ...errors, response: t('passwordResetErrorMessage') });
          console.error(err);
        }
      )
      .finally(() => setLoading(false));
  };

  const passwordAdornment = {
    endAdornment: (
      <InputAdornment position="end">
        <Tooltip title={t('passwordVisibility')}>
          <IconButton
            aria-label="toggle password visibility"
            onClick={togglePasswordVisibility}
            onMouseDown={(e) => e.preventDefault()}
          >
            {isPasswordVisible ? <VisibilityIcon /> : <VisibilityOffIcon />}
          </IconButton>
        </Tooltip>
      </InputAdornment>
    ),
  };

  const loadingButtonAction = success ? close : handleSubmit;
  const loadingButtonText = success ? t('done') : t('resetPasswordHeader');
  return (
    <Dialog
      id="forgot-password-dialog"
      open={isOpen}
      onClose={close}
      aria-labelledby="forgot-password-dialog-title"
      maxWidth="sm"
      fullWidth
    >
      <DialogTitle id="forgot-password-dialog-title">
        {t('resetPasswordHeader')}
      </DialogTitle>
      <DialogContent dividers>
        <TextField
          autoFocus
          required
          id="email"
          label={t('email')}
          name="email"
          autoComplete="email"
          value={email}
          error={!!errors.email}
          helperText={errors.email}
          onChange={(event) => {
            setEmail(event.target.value);
            setPristine({ ...pristine, email: false });
          }}
        />
        <TextField
          required
          id="password-input"
          aria-describedby="password-input"
          label={t('resetToNewPassword')}
          error={Boolean(errors.password)}
          helperText={errors.password}
          onChange={({ target }) => {
            handleChangePassword(target.value);
            setPristine({ ...pristine, password: false });
          }}
          type={isPasswordVisible ? 'text' : 'password'}
          InputProps={passwordAdornment}
        />
        {reCaptchaSiteKey ? (
          <>
            <Recaptcha callbackName={captchaCallbackName} />
            {errors.captcha ? (
              <FormHelperText error>{errors.captcha}</FormHelperText>
            ) : null}
          </>
        ) : null}
        {success ? (
          <Alert severity="success" style={{ marginTop: '12px' }}>
            {t('passwordResetConfirmationMessage')}
          </Alert>
        ) : null}
        {!success && errors.response && (
          <Alert severity="error" style={{ marginTop: '12px' }}>
            {errors.response}
          </Alert>
        )}
      </DialogContent>
      <DialogActions>
        {!success ? (
          <Button
            style={{ marginRight: '12px' }}
            variant="text"
            onClick={close}
          >
            {t('cancel')}
          </Button>
        ) : null}
        <LoadingButton
          loading={loading}
          success={success}
          onClick={(event) => {
            event.preventDefault();
            loadingButtonAction();
          }}
        >
          {loadingButtonText}
        </LoadingButton>
      </DialogActions>
    </Dialog>
  );
};

ResetPasswordDialog.propTypes = {
  isOpen: PropTypes.bool,
  close: PropTypes.func,
};

export default ResetPasswordDialog;
