import { Entry, Context } from '@entryscape/entrystore-js';
import { namespaces } from '@entryscape/rdfjson';
import { getLabel } from 'commons/util/rdfUtils';
import { LABEL_TYPE_URI } from './definitions';

/**
 *
 * @param {Context} context
 * @param {string} type
 * @returns {Entry[]}
 */
export const getTypedEntries = async (context, type) => {
  const typedEntries = [];
  await context
    .getEntryStore()
    .newSolrQuery()
    .context(context)
    .rdfType(type)
    .forEach((typedEntry) => typedEntries.push(typedEntry));
  return typedEntries;
};

const xsdns = namespaces.expand('xsd:');
const Literal = namespaces.expand('rdfs:Literal');
const LangString = namespaces.expand('rdf:langString');
const HTML = namespaces.expand('rdf:langString');
const XML = namespaces.expand('rdf:XMLLiteral');

/**
 *
 * @param {string[]} ranges
 * @returns {boolean}
 */
export const hasLiteralRange = (ranges) =>
  ranges.filter((range) => range === Literal || range === LangString).length >
  0;

/**
 *
 * @param {string[]} ranges
 * @returns {boolean}
 */
export const hasDatatypeRange = (ranges) =>
  ranges.filter(
    (range) => range.startsWith(xsdns) || range === HTML || range === XML
  ).length > 0;

/**
 *
 * @param {Entry[]} propertyEntries
 * @param {Entry} classEntry
 * @returns {Entry[]}
 */
export const domainProperties = (propertyEntries, classEntry) => {
  const propertiesInDomain = [];
  const classURI = classEntry.getResourceURI();
  for (const propertyEntry of propertyEntries) {
    if (
      propertyEntry
        .getAllMetadata()
        .find(propertyEntry.getResourceURI(), 'rdfs:domain')
        .map((stmt) => stmt.getValue())
        .filter((uri) => uri === classURI).length > 0
    ) {
      propertiesInDomain.push(propertyEntry);
    }
  }
  return propertiesInDomain;
};

/**
 *
 * @param {Entry} entry
 * @param {string} labelType
 * @param {object} namespaceStore
 * @returns {object}
 */
export const getRdfsLabel = (entry, labelType, namespaceStore) => {
  if (labelType !== LABEL_TYPE_URI) return getLabel(entry);
  return namespaceStore.shortenKnown(entry.getResourceURI());
};
