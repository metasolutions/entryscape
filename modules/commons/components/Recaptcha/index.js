import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import config from 'config';
import { affixScriptToHead } from 'commons/util/script';

const Recaptcha = ({ callbackName }) => {
  const siteKey = config.get('reCaptchaSiteKey');

  useEffect(() => {
    if (siteKey) {
      // Might pose problems if we use multiple grecaptchas
      affixScriptToHead('https://www.google.com/recaptcha/api.js');
    } else {
      console.error(
        'Cannot add recaptcha since no site key is provided in config.reCaptchaSiteKey.'
      );
    }
  }, [siteKey]);

  useEffect(() => {
    // Reset grecaptcha every 5 mins (unless checked) to avoid expiration issues
    const resetTimer = setInterval(() => {
      if (!window.grecaptcha.getResponse()) {
        window.grecaptcha.reset();
      }
    }, 5 * 60 * 1000);

    return () => clearInterval(resetTimer);
  });

  return (
    <div
      className="g-recaptcha"
      data-sitekey={siteKey}
      data-callback={callbackName}
    />
  );
};

Recaptcha.propTypes = {
  callbackName: PropTypes.string.isRequired,
};

export default Recaptcha;
