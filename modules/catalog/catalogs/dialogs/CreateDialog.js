import PropTypes from 'prop-types';
import { useState } from 'react';
import config from 'config';
import { TextField } from '@mui/material';
import escaCatalog from 'catalog/nls/escaCatalog.nls';
import { entrystore } from 'commons/store';
import { useTranslation } from 'commons/hooks/useTranslation';
import { RDF_TYPE_CATALOG } from 'commons/util/entry';
import ListActionDialog from 'commons/components/common/dialogs/ListActionDialog';
import {
  createPublisher,
  createCatalogEntry,
} from 'catalog/catalogs/utils/catalog';
import { useUserState } from 'commons/hooks/useUser';
import ProjectTypeChooser from 'commons/types/ProjectTypeChooser';
import useRestrictionDialog from 'commons/hooks/useRestrictionDialog';
import { CREATE, useListModel } from 'commons/components/ListView';
import { useSnackbar, ERROR } from 'commons/hooks/useSnackbar';
import useAsync, { PENDING } from 'commons/hooks/useAsync';
import LoadingButton from 'commons/components/LoadingButton';
import { useConfirmCloseAction } from 'commons/components/EntryListView/dialogs/create/handlers';
import useCatalogRestriction from '../useCatalogRestriction';

const CreateDialog = ({ numberOfCatalogs, closeDialog }) => {
  const { userEntry, userInfo } = useUserState();
  const [name, setName] = useState({
    value: '',
    pristine: true,
  });
  const [description, setDescription] = useState({
    value: '',
    pristine: true,
  });
  const [publisher, setPublisher] = useState({
    value: '',
    pristine: true,
  });
  const [selectedProjectType, setSelectedProjectType] = useState('');
  const [, dispatch] = useListModel();
  const updateParentList = () => dispatch({ type: CREATE });
  const createWithPublisher = !config.get('catalog.createWithoutPublisher');
  const t = useTranslation(escaCatalog);
  const { runAsync, status } = useAsync();
  const confirmClose = useConfirmCloseAction(closeDialog);
  const close = () =>
    confirmClose(
      !name.pristine || !description.pristine || !publisher.pristine
    );

  const { isRestricted, contentPath } = useCatalogRestriction(numberOfCatalogs);
  useRestrictionDialog({
    open: isRestricted,
    contentPath,
    callback: closeDialog,
  });

  const isInputValid = (inputValue) => !!inputValue.trim().length;

  const isFormValid = () => {
    if (createWithPublisher) {
      return (
        isInputValid(name.value) &&
        isInputValid(description.value) &&
        isInputValid(publisher.value)
      );
    }
    return isInputValid(name.value) && isInputValid(description.value);
  };

  const [addSnackbar] = useSnackbar();

  const showSnackbar = () => {
    addSnackbar({
      message: t('createCatalogSuccess'),
    });
  };

  const showFailSnackbar = () => {
    addSnackbar({
      message: t('createCatalogFail'),
      type: ERROR,
    });
  };

  const handleCreate = async () => {
    const groupEntry = await entrystore.createGroupAndContext();
    const homeContextId = groupEntry.getResource(true).getHomeContext();
    const homeContext = entrystore.getContextById(homeContextId);
    const contextEntry = await homeContext.getEntry();
    let publisherEntry;

    // Create publisher if required
    if (createWithPublisher) {
      publisherEntry = await createPublisher(homeContext, publisher);
    }

    const metadata = {
      name: name.value.trim(),
      description: description.value.trim(),
      publisherResourceRURI: publisherEntry?.getResourceURI(),
    };

    // Create catalog entry and update context and group with catalog name
    return createCatalogEntry({
      homeContext,
      homeContextId,
      rdfType: RDF_TYPE_CATALOG,
      metadata,
      userEntry,
      userInfo,
      groupEntry,
      contextEntry,
      projectType: selectedProjectType,
    })
      .then(() => {
        updateParentList();
        closeDialog();
        showSnackbar();
      })
      .catch((err) => {
        console.error(err);
        closeDialog();
        showFailSnackbar();
      });
  };

  const isDisabled = !isFormValid();
  const actions = (
    <LoadingButton
      onClick={() => runAsync(handleCreate())}
      loading={status === PENDING}
      autoFocus
      disabled={isDisabled}
    >
      {t('createButtonLabel')}
    </LoadingButton>
  );

  if (isRestricted) return null;

  return (
    <ListActionDialog
      id="create-catalog"
      closeDialog={close}
      title={t('createCatalogHeader')}
      actions={actions}
      maxWidth="md"
    >
      <form autoComplete="off">
        <ProjectTypeChooser
          onChangeProjectType={setSelectedProjectType}
          selectedId={selectedProjectType}
          userInfo={userInfo}
        />
        <TextField
          id="create-catalog-name"
          label={t('createCatalogName')}
          value={name.value}
          onChange={(event) =>
            setName({ value: event.target.value, pristine: false })
          }
          placeholder={t('createCatalogNamePlaceholder')}
          error={!name.pristine && !isInputValid(name.value)}
          helperText={
            !name.pristine && !isInputValid(name.value) && t('nameRequired')
          }
          required
        />
        <TextField
          id="create-catalog-description"
          label={t('createCatalogDesc')}
          value={description.value}
          onChange={(event) =>
            setDescription({ value: event.target.value, pristine: false })
          }
          placeholder={t('createCatalogDescPlaceholder')}
          error={!description.pristine && !isInputValid(description.value)}
          helperText={
            !description.pristine &&
            !isInputValid(description.value) &&
            t('descriptionRequired')
          }
          required
        />
        {createWithPublisher && (
          <TextField
            id="create-catalog-publisher"
            label={t('createAgentName')}
            value={publisher.value}
            onChange={(event) =>
              setPublisher({ value: event.target.value, pristine: false })
            }
            placeholder={t('createAgentNamePlaceholder')}
            error={!publisher.pristine && !isInputValid(publisher.value)}
            helperText={
              !publisher.pristine &&
              !isInputValid(publisher.value) &&
              t('publisherRequired')
            }
            required={createWithPublisher}
          />
        )}
      </form>
    </ListActionDialog>
  );
};

CreateDialog.propTypes = {
  numberOfCatalogs: PropTypes.number,
  closeDialog: PropTypes.func,
};

export default CreateDialog;
