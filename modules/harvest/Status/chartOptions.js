const options = (nlsBundle) => ({
  maintainAspectRatio: false,
  tooltips: {
    callbacks: {
      afterTitle(ttp, chart) {
        const el = chart.datasets[ttp[0].datasetIndex].data[ttp[0].index];
        const subtitle =
          el.type === 'inferred' ? nlsBundle.failure : nlsBundle.success;
        return subtitle;
      },
    },
  },
  legend: {
    display: true,
    position: 'bottom',
    labels: {
      usePointStyle: true,
      borderRadius: 5,
      padding: 25,
    },
  },
  scales: {
    x: {
      stacked: true,
    },
    y: {
      stacked: true,
    },
    xAxes: [
      {
        type: 'time',
        time: {
          unit: 'day',
          displayFormats: {
            hour: 'MMM DD',
          },
        },
        ticks: {
          source: 'data',
          autoSkip: true,
          maxTicksLimit: 15,
        },
        offset: true,
      },
    ],
    yAxes: [
      {
        ticks: {
          min: 0,
          precision: 0,
        },
        scaleLabel: {
          display: true,
          labelString: nlsBundle.yAxisLabel,
        },
      },
    ],
  },
});

export default options;
