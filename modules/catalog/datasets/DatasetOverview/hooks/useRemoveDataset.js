import { entrystore, entrystoreUtil } from 'commons/store';
import { useTranslation } from 'commons/hooks/useTranslation';
import escaDatasetNLS from 'catalog/nls/escaDataset.nls';
import { getResourceURI, findResourceStatements } from 'commons/util/entry';
import { getLabel } from 'commons/util/rdfUtils';
import { parseTemplate } from 'commons/util/reactUtils';
import { useCallback } from 'react';
import useGetMainDialog from 'commons/hooks/useGetMainDialog';
import {
  getReferences,
  getLabels,
} from 'commons/hooks/references/useGetReferences';
import escaReferencesNLS from 'catalog/nls/escaReferences.nls';
import { isCatalog } from 'catalog/utils/react/catalog';
import { isDatasetSeries } from 'catalog/datasets/utils';
import {
  DATASET_OVERVIEW_NAME,
  DATA_SERVICE_OVERVIEW_NAME,
} from 'catalog/config/config';
import { getPathFromViewName } from 'commons/util/site';
import ReferenceList from 'commons/components/ReferenceList';
import { getDataServices } from '../utils/dataServices';

const useRemoveDataset = (datasetEntry) => {
  const translate = useTranslation([escaDatasetNLS, escaReferencesNLS]);
  const { getAcknowledgeDialog } = useGetMainDialog();
  const isSeries = isDatasetSeries(datasetEntry);

  /**
   *  Removes the actual dataset and dependent distribution, visualization and
   *  comment entries.
   *
   *  @returns {Promise<void>}
   */
  const removeDataset = useCallback(async () => {
    const cache = entrystore.getCache();
    const distributionStatements = findResourceStatements(
      datasetEntry,
      'dcat:distribution'
    );
    const distributions = distributionStatements
      .map((statement) => {
        const resourceURI = statement.getValue();
        return Array.from(cache.getByResourceURI(resourceURI));
      })
      .filter((distribution) => distribution.length > 0);

    const allComments = await entrystore
      .newSolrQuery()
      .uriProperty('oa:hasTarget', datasetEntry.getResourceURI())
      .rdfType('oa:Annotation')
      .list()
      .getEntries(0);

    await Promise.all(allComments.map((comment) => comment.del()));
    await Promise.all(
      distributions.map((distribution) => distribution[0].del())
    );
    await datasetEntry.del();

    const resourceURI = datasetEntry.getResourceURI();
    const catalog = await entrystoreUtil.getEntryByType(
      'dcat:Catalog',
      datasetEntry.getContext()
    );

    catalog.getMetadata().findAndRemove(null, 'dcat:dataset', {
      value: resourceURI,
      type: 'uri',
    });
    return catalog.commitMetadata();
  }, [datasetEntry]);

  /**
   * Checks for dataset dependencies and asks for confirmation or acknowledgment
   * to proceed with the actual dataset removal.
   *
   * @returns {Promise<boolean>}
   */
  const checkCanRemove = useCallback(async () => {
    const distributionStatements = findResourceStatements(
      datasetEntry,
      'dcat:distribution'
    );

    const fileDistributionURIs = [];
    // Should use getAPIDistributionSourceURIs util function for these and do
    // something similar for downloadURL
    const apiDistributionURIs = [];
    const baseURI = entrystore.getBaseURI();

    await Promise.all(
      distributionStatements.map((distributionStatement) => {
        const distributionResourceURI = distributionStatement.getValue();
        return entrystoreUtil
          .getEntryByResourceURI(distributionResourceURI)
          .then(
            (entry) => {
              const source = getResourceURI(entry, 'dcterms:source');
              const downloadURI = getResourceURI(entry, 'dcat:downloadURL');

              if (source) {
                apiDistributionURIs.push(source);
              }

              if (downloadURI?.indexOf(baseURI) > -1) {
                fileDistributionURIs.push(downloadURI);
              }
            },
            (error) => {
              console.error(error);
            } // Fail silently
          );
      })
    );

    const dataServicesEntries = await getDataServices(datasetEntry);
    if (dataServicesEntries.length) {
      const items = dataServicesEntries.map((dataServiceEntry) => {
        const label = getLabel(dataServiceEntry);
        const href = getPathFromViewName(DATA_SERVICE_OVERVIEW_NAME, {
          contextId: dataServiceEntry.getContext().getId(),
          entryId: dataServiceEntry.getId(),
        });
        return { href, label };
      });
      await getAcknowledgeDialog({
        content: parseTemplate(translate('unableToRemoveDataset'), {
          1: <ReferenceList references={items} />,
        }),
      });
      return false;
    }

    if (apiDistributionURIs.length || fileDistributionURIs.length) {
      await getAcknowledgeDialog({
        content: translate('unableToRemoveDatasetNoDataServices'),
      });
      return false;
    }

    const references = await getReferences(datasetEntry, isCatalog);
    const labels = await getLabels(references, translate);
    const hrefs = references.map((entry) =>
      getPathFromViewName(DATASET_OVERVIEW_NAME, {
        contextId: entry.getContext().getId(),
        entryId: entry.getId(),
      })
    );
    if (references.length) {
      await getAcknowledgeDialog({
        content: parseTemplate(
          translate(
            isSeries ? 'unableToRemoveDatasetSeries' : 'unableToRemoveOther'
          ),
          {
            1: (
              <ReferenceList
                references={labels.map((label, index) => ({
                  label,
                  href: hrefs[index],
                }))}
              />
            ),
          }
        ),
      });
      return false;
    }

    return true;
  }, [datasetEntry, getAcknowledgeDialog, translate, isSeries]);

  return [checkCanRemove, removeDataset];
};

export default useRemoveDataset;
