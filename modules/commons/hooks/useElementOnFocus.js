import { useContext, createContext, useState } from 'react';
import PropTypes from 'prop-types';

const ElementOnFocusContext = createContext();

export const ElementOnFocusProvider = ({ children }) => {
  const [elementToFocus, setElementToFocus] = useState('');
  return (
    <ElementOnFocusContext.Provider
      value={{ elementToFocus, setElementToFocus }}
    >
      {children}
    </ElementOnFocusContext.Provider>
  );
};

ElementOnFocusProvider.propTypes = {
  children: PropTypes.node,
};

export const useElementOnFocus = () => {
  const context = useContext(ElementOnFocusContext);
  if (!context) {
    console.error(
      'ElementOnFocusContext must be used within a ContextProvider'
    );
  }
  return context;
};
