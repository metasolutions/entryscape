import PropTypes from 'prop-types';
import React, { useState } from 'react';
import ClipboardGraphContext from 'commons/contexts/ClipboardGraphContext';
import { LanguageProvider } from 'commons/hooks/useLanguage';
import { SnackbarProvider } from 'commons/hooks/useSnackbar';

const ViewProviders = ({ children }) => {
  const [clipboardGraph, setClipboardGraph] = useState('');

  return (
    <LanguageProvider>
      <SnackbarProvider>
        <ClipboardGraphContext.Provider
          value={[clipboardGraph, setClipboardGraph]}
        >
          {children}
        </ClipboardGraphContext.Provider>
      </SnackbarProvider>
    </LanguageProvider>
  );
};

ViewProviders.propTypes = {
  children: PropTypes.node,
};

export default ViewProviders;
