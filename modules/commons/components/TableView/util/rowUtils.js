import { engine } from '@entryscape/rdforms';

/**
 *
 * @param {object[]} rows
 * @returns {Promise}
 */
export const loadRowChoices = (rows) => {
  const promises = [];
  rows.forEach((row) => {
    row.columns.forEach((column) => {
      const data = row[column.field];
      if (data?.item.getType() !== 'choice') return;

      data.parent.getChildBindingsFor(data.item).forEach((binding) => {
        const choice = binding.getChoice();
        if (!choice.load) return;

        promises.push(choice.load());
      });
    });
  });

  return Promise.all(promises);
};

/**
 *
 * @param {object} row
 */
export const bindToRow = (row) => {
  const groupBinding = engine.match(
    row.graph,
    row.entry.getResourceURI(),
    row.template
  );

  row.columns.forEach((column) => {
    const columnKey = column.rdformsitem?._internalId
      ? `${column.rdformsitem._internalId}`
      : 'actions';

    row[columnKey] = {
      parent: groupBinding,
      item: column.rdformsitem,
    };
  });
};

/**
 * Maps entries to table view rows.
 *
 * @param {object[]} rowData
 * @param {Group} template
 * @param {object[]} columns
 * @returns {object[]}
 */
export const addRowBindings = (rowData, template, columns) =>
  rowData.map((rowItem) => {
    const row = {
      ...rowItem,
      template,
      columns,
    };
    bindToRow(row);
    return row;
  });

/**
 * Saves a table view row's changes.
 *
 * @param {object} row
 * @returns {Promise}
 */
export const saveRow = (row) => {
  row.entry.setMetadata(row.graph);
  return row.entry.commitMetadata().then(() => {
    row.graph.setChanged(false);
  });
};

/**
 * Discards a table view row's changes.
 *
 * @param {object} row
 * @returns {Promise}
 */
export const discardRow = (row) => {
  row.graph = row.entry.getMetadata().clone();
  bindToRow(row);
  return loadRowChoices([row]);
};

/**
 * Assigns CSS classes to a cell depending on its validation state.
 *
 * @param {@mui/x-data-grid/GridCellParams} gridCellParams
 * @param {Object<string, Object[]>} errors
 * @returns {string}
 */
export const getTableViewCellClassName = (gridCellParams, errors) => {
  const {
    value: cellValue,
    row: { id: rowId },
  } = gridCellParams;
  if (!cellValue) return;

  const rowErrors = errors[rowId];
  if (!rowErrors) return;

  const cellHasError = rowErrors.some(
    ({ item: errorItem }) =>
      errorItem._internalId === cellValue.item._internalId
  );
  if (!cellHasError) return;

  return 'escoTableView__cell--error';
};

/**
 * Assigns CSS classes to a row depending on its validation state.
 *
 * @param {@mui/x-data-grid/GridRowParams} gridRowParams
 * @param {Object<string, Object[]>} errors
 * @returns {string}
 */
export const getTableViewRowClassName = (
  gridRowParams,
  errors,
  selectionModel
) => {
  const { id: rowId } = gridRowParams;
  if (errors[rowId]) return 'escoTableView__row--error';
  if (selectionModel.includes(rowId)) return 'escoTableView__row--edited';
};
