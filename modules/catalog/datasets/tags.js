import escaDatasetNLS from 'catalog/nls/escaDataset.nls';
import escaDatasetTemplateNLS from 'catalog/nls/escaDatasetTemplate.nls';
import { isDatasetSeries, isBasedOnTemplate } from './utils';

export const SERIES_TAG = {
  id: 'seriesTag',
  labelNlsKey: 'seriesIconTooltip',
  nlsBundles: escaDatasetNLS,
  isVisible: (entry) => isDatasetSeries(entry),
};

export const TEMPLATE_TAG = {
  id: 'templateTag',
  labelNlsKey: 'templateTag',
  nlsBundles: escaDatasetTemplateNLS,
  isVisible: (entry) => isBasedOnTemplate(entry),
};
