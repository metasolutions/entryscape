import { STATUS_IDLE } from 'commons/hooks/useTasksList';

const TASKS_IMPORT_FORMS = [
  {
    id: 'parse',
    nlsKeyLabel: 'parseTask',
    status: STATUS_IDLE,
    message: '',
  },
  {
    id: 'analyze',
    nlsKeyLabel: 'analysisTask',
    status: STATUS_IDLE,
    message: '',
  },
  {
    id: 'import',
    nlsKeyLabel: 'importTask',
    status: STATUS_IDLE,
    message: '',
  },
  {
    id: 'update',
    nlsKeyLabel: 'updateTask',
    status: STATUS_IDLE,
    message: '',
  },
];

export { TASKS_IMPORT_FORMS };
