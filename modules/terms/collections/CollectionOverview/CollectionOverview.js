import { useEntry } from 'commons/hooks/useEntry';
import escoListNLS from 'commons/nls/escoList.nls';
import escoOverviewNLS from 'commons/nls/escoOverview.nls';
import esteCollectionExportNLS from 'terms/nls/esteCollectionexport.nls';
import esteCollection from 'terms/nls/esteCollection.nls';
import { useTranslation } from 'commons/hooks/useTranslation';
import { getModifiedDate, getThemeLabels } from 'commons/util/metadata';
import { getShortDate } from 'commons/util/date';
import useGetContributors from 'commons/hooks/useGetContributors';
import DownloadDialog from 'terms/collections/dialogs/DownloadDialog';
import LinkedDataBrowserDialog from 'commons/components/LinkedDataBrowserDialog';
import Overview from 'commons/components/overview/Overview';
import {
  withOverviewModelProvider,
  OverviewSection,
  overviewPropsPropType,
  useOverviewModel,
} from 'commons/components/overview';
import {
  ACTION_EDIT,
  ACTION_REMOVE,
  ACTION_INFO_WITH_ICON,
  ACTION_REVISIONS,
} from 'commons/components/overview/actions';
import CollectionConceptsList from './CollectionConceptsList';
import SourceTerminologiesList from './CollectionSourceTerminologiesList';
import useGetConceptEntries from './hooks/useGetConceptEntries';
import './index.scss';

const nlsBundles = [escoListNLS, escoOverviewNLS, esteCollection];

const CollectionOverview = ({ overviewProps }) => {
  const collectionEntry = useEntry();
  const translate = useTranslation(nlsBundles);
  const [collectionConcepts, onConceptsChange, conceptsStatus] =
    useGetConceptEntries(collectionEntry);

  const themes = getThemeLabels(collectionEntry);
  const [{ refreshCount }] = useOverviewModel();
  const { contributors } = useGetContributors(collectionEntry, refreshCount);

  const descriptionItems = [
    {
      id: 'title',
      labelNlsKey: 'themeTitle',
      getValues: () => (themes ? [...new Set(themes)] : []),
    },
    {
      id: 'last-update',
      labelNlsKey: 'lastUpdateLabel',
      getValues: () =>
        collectionEntry ? [getShortDate(getModifiedDate(collectionEntry))] : [],
    },
    {
      id: 'edited',
      labelNlsKey: 'editedTitle',
      getValues: () => contributors,
    },
  ];

  return (
    <Overview
      {...overviewProps}
      backLabel={translate('backTitle')}
      headerAction={{
        ...ACTION_INFO_WITH_ICON,
        Dialog: LinkedDataBrowserDialog,
      }}
      entry={collectionEntry}
      nlsBundles={nlsBundles}
      descriptionItems={descriptionItems}
      sidebarActions={[
        ACTION_EDIT,
        {
          id: 'download',
          getProps: () => ({
            label: translate('collectionDownload'),
            action: {
              Dialog: DownloadDialog,
              entry: collectionEntry,
              nlsBundles: [esteCollectionExportNLS],
              profile: 'skoscollection',
            },
          }),
        },
        ACTION_REVISIONS,
        ACTION_REMOVE,
      ]}
    >
      <>
        <OverviewSection>
          <CollectionConceptsList
            collectionConcepts={collectionConcepts}
            collectionEntry={collectionEntry}
            onConceptsChange={onConceptsChange}
            conceptsStatus={conceptsStatus}
          />
        </OverviewSection>
        <OverviewSection>
          <SourceTerminologiesList
            collectionConcepts={collectionConcepts}
            collectionEntry={collectionEntry}
            onConceptsChange={onConceptsChange}
            conceptsStatus={conceptsStatus}
          />
        </OverviewSection>
      </>
    </Overview>
  );
};

CollectionOverview.propTypes = {
  overviewProps: overviewPropsPropType,
};

export default withOverviewModelProvider(CollectionOverview);
