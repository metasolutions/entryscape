import PropTypes from 'prop-types';
import ListActionDialog from 'commons/components/common/dialogs/ListActionDialog';
import {
  ListActions,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  ListHeader,
  ListHeaderItemText,
  ListHeaderItemSort,
  ListSearch,
  ListView,
  ListPagination,
  useListModel,
  useListQuery,
  withListModelProvider,
  ListItemActionIconButton,
  useFetchEntries,
  ListItemCheckbox,
  PAGINATE,
} from 'commons/components/ListView';
import { getNoResultsNLS } from 'commons/components/filters/utils/filter';
import { Box, Button } from '@mui/material';
import BuiltinIcon from '@mui/icons-material/Settings';
import CustomIcon from '@mui/icons-material/Widgets';
import InfoIcon from '@mui/icons-material/Info';
import { useState, useMemo } from 'react';
import {
  ENTITY_TYPE_ID_PREFIX,
  ENTITY_TYPE_BUILTIN,
} from 'admin/configs/utils/namespace';
import { RefreshButton } from 'commons/components/EntryListView';
import FieldsLinkedDataBrowserDialog from 'commons/components/FieldsLinkedDataBrowserDialog';
import { useTranslation } from 'commons/hooks/useTranslation';
import esadTypeNLS from 'admin/nls/esadType.nls';
import esadEntityTypeSelectNLS from 'admin/nls/esadEntityTypeSelect.nls';
import esadEntityTypeNLS from 'admin/nls/esadEntityType.nls';
import esadProjectTypeNLS from 'admin/nls/esadProjectType.nls';
import {
  getEntityTypeEntries,
  getEntityTypesByFilter,
} from 'admin/configs/entitytypes/utils/entityTypeQuery';
import { fieldPresenters } from 'admin/configs/entitytypes/utils/entityTypePresenters';
import { typeConfigToGraph } from 'admin/configs/utils/exporters';

const nlsBundles = [
  esadEntityTypeSelectNLS,
  esadTypeNLS,
  esadProjectTypeNLS,
  esadEntityTypeNLS,
];

const SEARCH_FIELDS = ['label'];

const EntityTypeSelectDialog = ({
  selection,
  closeDialog,
  onSelect,
  isDisabled,
}) => {
  const [selectedEntityTypes, setSelectedEntityTypes] = useState(selection);
  const [showAllEntityTypes, setShowAllEntityTypes] = useState(true);
  const [{ search, refreshCount }, dispatch] = useListModel();

  const { entries: entityTypeEntries, status } = useFetchEntries({
    fetchEntries: getEntityTypeEntries,
    refreshCount,
  });

  const instanceAndBuiltinEntityTypes = useMemo(() => {
    return getEntityTypesByFilter(entityTypeEntries);
  }, [entityTypeEntries]);

  const { result: entityTypes, size } = useListQuery({
    items: showAllEntityTypes
      ? instanceAndBuiltinEntityTypes
      : selectedEntityTypes,
    searchFields: SEARCH_FIELDS,
  });

  const onChange = (event, entityType) => {
    const { checked } = event.target;
    if (checked) {
      setSelectedEntityTypes([...selectedEntityTypes, entityType]);
    } else {
      setSelectedEntityTypes(
        selectedEntityTypes.filter((item) => item.id !== entityType.id)
      );
    }
  };

  const translate = useTranslation(nlsBundles);

  const presenterAction = {
    Dialog: FieldsLinkedDataBrowserDialog,
    idPrefix: ENTITY_TYPE_ID_PREFIX,
    fields: fieldPresenters,
    nlsKeyLabel: 'typeInfoDialogTitle',
    nlsBundles,
  };

  const selectAction = (
    <Button onClick={() => onSelect(selectedEntityTypes)}>
      {translate('entityTypeSelectDialogButtonLabel')}
    </Button>
  );

  const onShowAllToggle = () => {
    setShowAllEntityTypes(!showAllEntityTypes);
    dispatch({ type: PAGINATE, value: 0 });
  };

  return (
    <ListActionDialog
      title={translate('entityTypeSelectDialogTitle')}
      maxWidth="lg"
      fixedHeight
      closeDialog={closeDialog}
      actions={selectAction}
    >
      <ListView
        search={search}
        size={size}
        nlsBundles={nlsBundles}
        status={size === 0 && status === 'resolved' ? 'idle' : status}
      >
        <ListActions>
          <ListSearch />
          <RefreshButton />
          <Box sx={{ marginLeft: 'auto' }}>
            <Button
              disabled={showAllEntityTypes && !selectedEntityTypes.length}
              variant="text"
              onClick={onShowAllToggle}
            >
              {showAllEntityTypes
                ? translate('showSelectedLabel')
                : translate('showAllLabel')}
            </Button>
          </Box>
        </ListActions>
        <List
          renderPlaceholder={(Placeholder) => (
            <Placeholder
              label={translate(
                getNoResultsNLS(Boolean(search), !showAllEntityTypes),
                search
              )}
            />
          )}
        >
          <ListHeader>
            <ListHeaderItemText xs={1} text="" />
            <ListHeaderItemSort
              xs={11}
              sortBy="label"
              text={translate('labelListHeader')}
            />
          </ListHeader>
          {entityTypes.map((entityType) => {
            const { id, label, type, entry } = entityType;
            const disabled = isDisabled(id);
            const isBuiltin = type === ENTITY_TYPE_BUILTIN;
            const title = translate(presenterAction.nlsKeyLabel);
            const typeInfoAction = {
              ...presenterAction,
              entry,
              title,
              ...typeConfigToGraph({
                entry,
                typeConfig: entityType.config,
                idPrefix: ENTITY_TYPE_ID_PREFIX,
                fields: fieldPresenters,
              }),
            };
            return (
              <ListItem key={id}>
                <ListItemIcon
                  Icon={isBuiltin ? BuiltinIcon : CustomIcon}
                  tooltip={
                    isBuiltin
                      ? translate('builtinLabel')
                      : translate('customLabel')
                  }
                />
                <ListItemText xs={8} primary={label} />
                <ListItemActionIconButton
                  title={translate(presenterAction.nlsKeyLabel)}
                  action={typeInfoAction}
                  xs={2}
                >
                  <InfoIcon />
                </ListItemActionIconButton>
                <ListItemCheckbox
                  ariaLabel={label}
                  checked={Boolean(
                    selectedEntityTypes.find(
                      (item) => item.id === entityType.id
                    )
                  )}
                  disabled={disabled}
                  onChange={(event) => onChange(event, entityType)}
                  tooltip={
                    disabled ? translate('disabledSelectionTooltip') : null
                  }
                />
              </ListItem>
            );
          })}
        </List>
        <ListPagination />
      </ListView>
    </ListActionDialog>
  );
};

EntityTypeSelectDialog.propTypes = {
  selection: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  closeDialog: PropTypes.func,
  onSelect: PropTypes.func,
  isDisabled: PropTypes.func,
};

export default withListModelProvider(EntityTypeSelectDialog, () => ({
  sort: { field: 'label', order: 'asc' },
}));
