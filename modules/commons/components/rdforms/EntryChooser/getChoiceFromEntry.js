import { getLabel, getDescription } from 'commons/util/rdfUtils';

export default (entry, obj) => {
  const o = obj || {};
  o.value = entry.getResourceURI();
  o.label =
    getLabel(entry) ||
    getLabel(entry.getCachedExternalMetadata(), entry.getResourceURI());
  o.description =
    getDescription(entry) ||
    getDescription(entry.getCachedExternalMetadata(), entry.getResourceURI());
  return o;
};
