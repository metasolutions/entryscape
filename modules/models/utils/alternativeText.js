import {
  RDF_PROPERTY_TEXT,
  RDF_PROPERTY_ROLE,
  RDF_PROPERTY_VALUE,
} from 'models/utils/ns';
import { Graph } from '@entryscape/rdfjson';
import {
  addLanguageLiterals,
  getBlankNodeIds,
  getLanguageLiterals,
} from './metadata';

/**
 *
 * @param {Graph} graph
 * @param {string} blankNodeId
 * @returns {object}
 */
const getAlternativeText = (graph, blankNodeId) => {
  const role = graph.findFirstValue(blankNodeId, RDF_PROPERTY_ROLE);
  const values = getLanguageLiterals(graph, blankNodeId, RDF_PROPERTY_VALUE);
  return {
    role,
    values,
  };
};

/**
 * Retrieves alternative texts as an rdforms compatible text object. There may
 * be multiple alternative texts using the same role and in this case all values
 * for the role are merged.
 *
 * @param {Graph} graph
 * @param {string} resourceURI
 * @returns {object}
 */
export const getRfsAlternativeTexts = (graph, resourceURI) => {
  const blankNodeIds = getBlankNodeIds(graph, resourceURI, RDF_PROPERTY_TEXT);
  const roleValuePairs = blankNodeIds.map((blankNodeId) => {
    return getAlternativeText(graph, blankNodeId);
  });

  // texts with same role are merged into an array of text values
  const alternativeTexts = roleValuePairs.reduce((texts, nextRoleValue) => {
    const { role, values } = nextRoleValue;
    if (!(role in texts)) {
      texts[role] = [];
    }
    texts[role].push(values);
    return texts;
  }, {});
  return alternativeTexts;
};

/**
 * Add alternative text to metadata
 *
 * @param {Graph} graph
 * @param {string} resourceURI
 * @param {string} role
 * @param {object} lang2val
 */
export const addAlternativeText = (graph, resourceURI, role, lang2val) => {
  const blankNodeId = graph.add(resourceURI, RDF_PROPERTY_TEXT).getValue();
  graph.addL(blankNodeId, RDF_PROPERTY_ROLE, role);
  addLanguageLiterals(graph, blankNodeId, RDF_PROPERTY_VALUE, lang2val);
};
