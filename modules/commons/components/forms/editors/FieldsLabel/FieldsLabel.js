import PropTypes from 'prop-types';
import { FieldsLabel as Label } from 'commons/components/forms/presenters';

export const FieldsLabel = ({ label, id, mandatory = false, onClick }) => {
  return (
    <Label label={label} id={id} onClick={onClick}>
      {mandatory && label ? (
        <span className="escoFieldsLabel_marker">
          <span aria-hidden>*</span>
        </span>
      ) : null}
    </Label>
  );
};

FieldsLabel.propTypes = {
  label: PropTypes.string,
  id: PropTypes.string,
  mandatory: PropTypes.bool,
  onClick: PropTypes.func,
};

export default FieldsLabel;
