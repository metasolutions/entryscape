import { useMemo } from 'react';
import { Entry } from '@entryscape/entrystore-js';
import {
  isAPIDistribution,
  isAccessDistribution,
  isFileDistributionWithoutAPI,
  removeDistribution,
  deactivateAPIAndRemoveDistribution,
  filterVisualizationsByDistribution,
  isWMSDistribution,
  getRelatedWMSVisualizations,
  removeWMSDistributionAndVisualization,
} from 'catalog/datasets/DatasetOverview/utils/distributions';
import escaDatasetNLS from 'catalog/nls/escaDataset.nls';
import { useTranslation } from 'commons/hooks/useTranslation';
import { useVisualizations } from '../../../hooks/useVisualizations';

/**
 *
 * @param {Entry} distributionEntry
 * @param {Entry} datasetEntry
 * @param {string[]} apiDistributionResourceURIs
 * @param {Entry[]} visualizations
 * @param {Function} translate
 * @returns {object}
 */
const getRemoveDistributionParams = (
  distributionEntry,
  datasetEntry,
  apiDistributionResourceURIs,
  visualizations,
  translate
) => {
  /*
    WMS distributions can have dependent visualizations
  */
  if (isWMSDistribution(distributionEntry)) {
    const relatedWMSVisualizationEntries = getRelatedWMSVisualizations(
      visualizations,
      distributionEntry
    );

    if (relatedWMSVisualizationEntries?.length)
      return {
        actionType: 'confirm',
        message: translate('removeDistributionWithVisualizationQuestion', {
          1: relatedWMSVisualizationEntries.length,
        }),
        remove: removeWMSDistributionAndVisualization,
        removeParams: [
          distributionEntry,
          datasetEntry,
          relatedWMSVisualizationEntries,
        ],
      };
  }

  if (isAPIDistribution(distributionEntry)) {
    return {
      actionType: 'confirm',
      message: translate('removeDistributionQuestion'),
      remove: deactivateAPIAndRemoveDistribution,
      removeParams: [distributionEntry, datasetEntry],
    };
  }
  if (isAccessDistribution(distributionEntry)) {
    return {
      actionType: 'confirm',
      message: translate('removeDistributionQuestion'),
      remove: removeDistribution,
      removeParams: [distributionEntry, datasetEntry],
    };
  }

  /*
    File distributions can have dependent visualizations
    and api:s. These cases are handled differently.
  */
  if (
    isFileDistributionWithoutAPI(distributionEntry, apiDistributionResourceURIs)
  ) {
    const relatedVisualizationEntries = filterVisualizationsByDistribution(
      visualizations,
      distributionEntry
    );

    return {
      actionType: 'confirm',
      message: relatedVisualizationEntries?.length
        ? translate('removeDistributionWithVisualizationQuestion', {
            1: relatedVisualizationEntries.length,
          })
        : translate('removeDistributionQuestion'),
      remove: removeDistribution,
      removeParams: [distributionEntry, datasetEntry],
    };
  }

  // file distribution with api
  return {
    actionType: 'acknowledge',
    message: translate('removeFileDistWithAPI'),
  };
};

const useGetRemoveDistributionParams = (
  distributionEntry,
  datasetEntry,
  apiDistributionResourceURIs
) => {
  const { visualizations } = useVisualizations(datasetEntry);
  const translate = useTranslation(escaDatasetNLS);

  const removeDistributionParams = useMemo(
    () =>
      getRemoveDistributionParams(
        distributionEntry,
        datasetEntry,
        apiDistributionResourceURIs,
        visualizations,
        translate
      ),
    [
      datasetEntry,
      distributionEntry,
      apiDistributionResourceURIs,
      visualizations,
      translate,
    ]
  );

  return removeDistributionParams;
};

export default useGetRemoveDistributionParams;
