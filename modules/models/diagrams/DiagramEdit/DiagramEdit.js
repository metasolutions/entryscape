import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import { useTranslation } from 'commons/hooks/useTranslation';
import esmoDiagramsNLS from 'models/nls/esmoDiagrams.nls';
import escoDialogsNLS from 'commons/nls/escoDialogs.nls';
import DiagramControl from '../DiagramControl';
import DiagramEditPanel from './DiagramEditPanel';
import './DiagramEdit.scss';
import { useDiagramContext } from '../hooks/useDiagramContext';

const NLS_BUNDLES = [esmoDiagramsNLS, escoDialogsNLS];

const DiagramEdit = ({ open, diagramUtils, onDone }) => {
  const translate = useTranslation(NLS_BUNDLES);
  const { diagramAdapter } = useDiagramContext();
  const [paper, setPaper] = useState();
  const [node, setNode] = useState();
  useEffect(() => {
    if (!node) return;
    const newPaper = diagramAdapter.createPaper(node, true);
    newPaper.unfreeze();
    setPaper(newPaper);
  }, [node, diagramAdapter]);

  return (
    <Dialog
      fullScreen
      open={open}
      onClose={onDone}
      aria-labelledby="diagram-control-heading"
    >
      <div className="esmoDiagram__TwoCol">
        <div className="esmoDiagram__Main">
          <DiagramControl diagramUtils={diagramUtils} paper={paper} edit />
          <div className="esmesmoDiagram__fullCanvas" ref={setNode} />
        </div>
        <div className="esmoDiagram__Sidebar">
          <DiagramEditPanel paper={paper} onRemove={onDone} />
        </div>
      </div>
      <Button
        onClick={onDone}
        variant="text"
        autoFocus
        sx={{ position: 'absolute', bottom: '5px', right: '15px' }}
      >
        {translate('close')}
      </Button>
    </Dialog>
  );
};

DiagramEdit.propTypes = {
  open: PropTypes.bool,
  onDone: PropTypes.func,
  diagramUtils: PropTypes.shape({}),
};

export default DiagramEdit;
