import PropTypes from 'prop-types';
import { useReducer } from 'react';
import { Grid } from '@mui/material';
import ListActionDialog from 'commons/components/common/dialogs/ListActionDialog';
import { CREATE, useListModel } from 'commons/components/ListView';
import ProjectTypeChooser from 'commons/types/ProjectTypeChooser';
import esreHarvestNLS from 'harvest/nls/esreHarvest.nls';
import { useTranslation } from 'commons/hooks/useTranslation';
import { useUserState } from 'commons/hooks/useUser';
import { createContextAndGroup } from 'harvest/util';
import Lookup from 'commons/types/Lookup';
import useAsync, { PENDING } from 'commons/hooks/useAsync';
import LoadingButton from 'commons/components/LoadingButton';
import {
  reducer,
  CHANGE_PROJECT_TYPE,
  validateForm,
  initialState as initialFormState,
} from '../../forms/PipelineFormModel';
import PipelineForm from '../../forms/PipelineForm';
import TransformsForm from '../../forms/TransformsForm';

const CreatePipelineDialog = ({ closeDialog }) => {
  const translate = useTranslation([esreHarvestNLS]);
  const { runAsync, status } = useAsync();
  const [, listDispatch] = useListModel();
  const { userEntry, userInfo } = useUserState();
  const [data, dispatch] = useReducer(reducer, initialFormState);
  const projectType = Lookup.getProjectType(data.projectType);
  const entityType = projectType
    ? Lookup.getPrimaryForRdfType(
        projectType,
        'http://entrystore.org/terms/Pipeline'
      )
    : Lookup.getEntityTypeByRdfType(
        'http://entrystore.org/terms/Pipeline',
        'register'
      );

  const tags = entityType?.get('tags') || [];
  const isDcat = tags.includes('dcat');
  const isValid = validateForm(data, userEntry, isDcat);

  const handleCreate = () =>
    runAsync(
      createContextAndGroup(
        userEntry,
        userInfo,
        data,
        translate('replaceUserErrorMessage'),
        tags
      )
        .then(() => {
          listDispatch({ type: CREATE });
        })
        .then(closeDialog)
    );

  const handleChangeProjectType = (selectedProjectType) =>
    dispatch({
      type: CHANGE_PROJECT_TYPE,
      projectType: selectedProjectType,
    });

  const actions = (
    <LoadingButton
      onClick={handleCreate}
      loading={status === PENDING}
      autoFocus
      disabled={!isValid}
    >
      {translate('cPButton')}
    </LoadingButton>
  );

  return (
    <ListActionDialog
      id="create-pipeline"
      closeDialog={closeDialog}
      title={translate('cOHeader')}
      actions={actions}
      fixedHeight
    >
      <Grid container justifyContent="center" alignItems="center">
        <Grid item xs={8}>
          <ProjectTypeChooser
            onChangeProjectType={handleChangeProjectType}
            selectedId={data.projectType}
            userInfo={userInfo}
          />
          <PipelineForm
            userEntry={userEntry}
            data={data}
            dispatch={dispatch}
            isDcat={isDcat}
          />
          {!isDcat ? (
            <TransformsForm transforms={data.transforms} dispatch={dispatch} />
          ) : null}
        </Grid>
      </Grid>
    </ListActionDialog>
  );
};

CreatePipelineDialog.propTypes = {
  closeDialog: PropTypes.func.isRequired,
};

export default CreatePipelineDialog;
