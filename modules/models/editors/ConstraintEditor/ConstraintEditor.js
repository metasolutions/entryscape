import { useState } from 'react';
import PropTypes from 'prop-types';
import {
  entryPropType,
  graphPropType,
  RDF_TYPE_TERMINOLOGY,
} from 'commons/util/entry';
import {
  nlsBundlesPropType,
  useTranslation,
} from 'commons/hooks/useTranslation';
import * as ns from 'models/utils/ns';
import {
  URI,
  Field,
  Fields,
  MISSING_VALUE,
  useOrderedFieldsEdit,
  FieldControlButton,
  REFRESH,
  useEditorContext,
  REMOVE_FIELD_GROUPS,
  useBlankEdit,
  getAvailableChoices,
} from 'commons/components/forms/editors';
import esmoFieldsNls from 'models/nls/esmoFields.nls';
import esmoCommonsNls from 'models/nls/esmoCommons.nls';
import escoDialogsNLS from 'commons/nls/escoDialogs.nls';
import escoFormsNls from 'commons/nls/escoForms.nls';
import escoListNLS from 'commons/nls/escoList.nls';
import {
  ADVANCED_CONSTRAINT,
  CLASS_ENTRY,
  MAIN_EDIT_MODE_CHOICES,
  MISSING_PROPERTY,
  USE_TERMINOLOGY,
  findConstraint,
  findConstraintFieldGroups,
  getConstraintType,
} from 'models/utils/constraint';
import useGetNamespaceEntries from 'models/namespaces/hooks/useGetNamespaceEntries';
import { useESContext } from 'commons/hooks/useESContext';
import './ConstraintEditor.scss';
import SelectEditMode from 'models/components/SelectEditMode/SelectEditMode';
import { getLabel } from 'commons/util/rdfUtils';
import {
  CONSTRAINT_VALUE_EDIT_CHOICES,
  PROPERTY_EDIT_MODE_CHOICES,
} from 'models/utils/choiceDefinitions';
import {
  NAMESPACE_ENTRY,
  RESOURCE,
  RESOURCE_ENTRY,
  getURIEditorType,
} from 'models/utils/metadata';
import { Grid } from '@mui/material';
import ArrowDownwardIcon from '@mui/icons-material/ArrowDownward';
import ArrowUpwardIcon from '@mui/icons-material/ArrowUpward';
import { ResourceEntryEditor } from '../ResourceEntryEditor';
import { ConstraintPropertyEditor } from './ConstraintPropertyEditor';
import { ConstraintValueEditor } from './ConstraintValueEditor';

const NLS_BUNDLES = [
  esmoFieldsNls,
  esmoCommonsNls,
  escoDialogsNLS,
  escoFormsNls,
  escoListNLS,
];

const ConstraintValuesEdit = ({
  property,
  blankNodeId,
  graph,
  onChange,
  dispatch,
  size,
  error,
  helperText,
  namespaceEntries,
  validate,
  ...fieldsProps
}) => {
  const translate = useTranslation(NLS_BUNDLES);
  const { addField, fieldGroup, labelId, removeField, moveField } =
    useOrderedFieldsEdit({
      property,
      resourceURI: blankNodeId,
      graph,
      dispatch,
      nodetype: URI,
      orderProperty: ns.RDF_PROPERTY_ORDER,
      includeValue: false, // the value is set on the resource entry by default
    });
  const orderedFields = fieldGroup.getFields();

  const editModeChoices = CONSTRAINT_VALUE_EDIT_CHOICES.map(
    ({ value, labelNlsKey }) => ({
      label: translate(labelNlsKey),
      value,
    })
  );

  const update = (orderedField, value) => {
    const editMode = orderedField.get('editMode');
    if (editMode === RESOURCE) {
      orderedField.setAsserted(Boolean(value));
    } else if (editMode === NAMESPACE_ENTRY) {
      orderedField.setAsserted(Boolean(value?.entry && value?.localname));
    } else if (editMode === RESOURCE_ENTRY) {
      orderedField.setAsserted(Boolean(value?.entry));
    }
    onChange();
  };
  const remove = (field) => {
    removeField(field);
    onChange();
  };

  const handleEditModeChange = (editMode, field) => {
    field.set('editMode', editMode);
    dispatch({ type: REFRESH });
  };

  const getOrSetEditMode = (orderedField) => {
    if (!orderedField.get('editMode')) {
      const editMode = getURIEditorType(
        graph,
        orderedField.getBlank().getBlankNodeId(),
        'rdf:value',
        RESOURCE_ENTRY
      );
      orderedField.set('editMode', editMode);
    }
    return orderedField.get('editMode');
  };

  return (
    <Fields
      labelId={labelId}
      addField={addField}
      {...fieldsProps}
      label={translate('constraintObjectLabel')}
    >
      {orderedFields.map((orderedField, index) => {
        return (
          <ConstraintValueEditor
            key={orderedField.getId()}
            graph={graph}
            resourceURI={orderedField.getBlank().getBlankNodeId()}
            property="rdf:value"
            onEditModeChange={(editMode) =>
              handleEditModeChange(editMode, orderedField)
            }
            editMode={getOrSetEditMode(orderedField)}
            editModeChoices={editModeChoices}
            dispatch={dispatch}
            nlsBundles={NLS_BUNDLES}
            max={1}
            labelId={labelId}
            size={size}
            spacing={2}
            error={error}
            helperText={helperText}
            onChange={update}
            onRemove={remove}
            fieldsCount={orderedFields.length}
            field={orderedField}
            validate={validate}
            mandatory
            renderControls={() => (
              <Grid item xs={4}>
                {orderedField.getOrder() > 1 ? (
                  <FieldControlButton
                    marginLeft="0"
                    tooltip={translate('moveUpTooltip')}
                    onClick={() => moveField(orderedField, index, index - 1)}
                  >
                    <ArrowUpwardIcon />
                  </FieldControlButton>
                ) : null}
                {orderedField.getOrder() < orderedFields.length ? (
                  <FieldControlButton
                    marginLeft="0"
                    tooltip={translate('moveDownTooltip')}
                    onClick={() => moveField(orderedField, index, index + 1)}
                  >
                    <ArrowDownwardIcon />
                  </FieldControlButton>
                ) : null}
              </Grid>
            )}
          />
        );
      })}
    </Fields>
  );
};

ConstraintValuesEdit.propTypes = {
  size: PropTypes.string,
  graph: graphPropType,
  blankNodeId: PropTypes.string,
  onChange: PropTypes.func,
  dispatch: PropTypes.func,
  property: PropTypes.string,
  errorText: PropTypes.string,
  namespaceEntries: PropTypes.arrayOf(entryPropType),
  helperText: PropTypes.string,
  validate: PropTypes.func,
  error: PropTypes.bool,
};

const AdvancedConstraintEditor = ({
  size,
  nodetype,
  property,
  graph,
  resourceURI,
  dispatch,
  namespaceEntries,
  editModeChoices,
  onEditModeChange,
  ...fieldsProps
}) => {
  const translate = useTranslation([esmoCommonsNls, escoFormsNls]);
  const { labelId, addBlank, fieldGroup } = useBlankEdit({
    name: fieldsProps.name,
    property,
    graph,
    resourceURI,
    dispatch,
    rdfType: ns.RDF_TYPE_CONSTRAINT,
  });
  const fields = fieldGroup.getFields();
  const { fieldSet } = useEditorContext();

  const handleChange = (field) => {
    const [prop, value] = findConstraint(graph, field);

    if (!prop && !value && field.isAsserted()) {
      field.setAsserted(false);
    }

    if ((prop || value) && !field.isAsserted()) {
      field.setAsserted(true);
    }
    if (!field.isAsserted()) {
      // TODO: improve on this
      const fieldGroups = findConstraintFieldGroups(
        fieldSet,
        field.getBlankNodeId()
      );
      fieldGroups.forEach((f) => {
        f.setError();
      });
    }
  };

  const handleRemove = (field) => {
    const fieldGroups = findConstraintFieldGroups(
      fieldSet,
      field.getBlankNodeId()
    );
    fieldGroup.removeField(field);
    dispatch({
      type: REMOVE_FIELD_GROUPS,
      fieldGroups,
    });
  };

  const handleClear = (field) => {
    field.clear();
  };

  const handleSelectEditMode = (value, field) => {
    const fieldGroups = findConstraintFieldGroups(
      fieldSet,
      field.getBlankNodeId()
    );
    dispatch({
      type: REMOVE_FIELD_GROUPS,
      fieldGroups: [fieldGroup, ...fieldGroups],
    });
    onEditModeChange(value);
  };

  const validateConstraint = (validate, subform) => {
    return (isMandatory) => {
      return validate(isMandatory && subform.isAsserted());
    };
  };

  return (
    <Fields
      labelId={labelId}
      label={translate('constraintLabel')}
      addField={addBlank}
    >
      {fields.map((field) => {
        return (
          <Field
            key={field.getId()}
            size={fields.length}
            onClear={() => handleClear(field)}
            onRemove={() => handleRemove(field)}
          >
            <SelectEditMode
              value={ADVANCED_CONSTRAINT}
              choices={editModeChoices}
              nlsBundles={esmoCommonsNls}
              onChange={(value) => handleSelectEditMode(value, field)}
            />
            <div className="esmoConstraintEditor">
              <ConstraintPropertyEditor
                graph={graph}
                resourceURI={field.getBlankNodeId()}
                property={ns.RDF_PROPERTY_CONSTRAINT_PROPERTY}
                editChoices={PROPERTY_EDIT_MODE_CHOICES}
                nlsBundles={NLS_BUNDLES}
                mandatory
                dispatch={dispatch}
                max={1}
                label={translate('constraintPropertyLabel')}
                error={field.getError() === MISSING_PROPERTY}
                size={size}
                fieldsCount={1}
                onChange={() => handleChange(field)}
                validate={(validate) => validateConstraint(validate, field)}
                helperText={
                  field.getError() === MISSING_PROPERTY
                    ? translate(MISSING_VALUE)
                    : ''
                }
              />
              <ConstraintValuesEdit
                {...fieldsProps}
                property={ns.RDF_PROPERTY_CONSTRAINT_VALUE}
                graph={graph}
                blankNodeId={field.getBlankNodeId()}
                size={size}
                onChange={() => handleChange(field)}
                validate={(validate) => validateConstraint(validate, field)}
                mandatory
                dispatch={dispatch}
                error={field.getError() === MISSING_VALUE}
                helperText={
                  field.getError() === MISSING_VALUE
                    ? translate(MISSING_VALUE)
                    : ''
                }
                namespaceEntries={namespaceEntries}
              />
            </div>
          </Field>
        );
      })}
    </Fields>
  );
};

AdvancedConstraintEditor.propTypes = {
  size: PropTypes.string,
  nodetype: PropTypes.string,
  graph: graphPropType,
  resourceURI: PropTypes.string,
  dispatch: PropTypes.func,
  property: PropTypes.string,
  namespaceEntries: PropTypes.arrayOf(entryPropType),
  editModeChoices: PropTypes.arrayOf(PropTypes.shape({})),
  onEditModeChange: PropTypes.func,
};

const classEntryProps = {
  editModeValue: CLASS_ENTRY,
  resourceRdfType: ns.RDF_TYPE_CLASS,
  selectTooltipNlsKey: 'selectClassLabel',
  emptyListNlsKey: 'noClassesWarning',
  dialogProps: {
    titleNlsKey: 'selectClassLabel',
  },
};

const terminologyProps = {
  editModeValue: USE_TERMINOLOGY,
  resourceRdfType: RDF_TYPE_TERMINOLOGY,
  resourceProperty: ns.RDF_PROPERTY_TERMINOLOGY,
  selectTooltipNlsKey: 'selectTerminologyLabel',
  emptyListNlsKey: 'noTerminologiesWarning',
  dialogProps: {
    allContexts: true,
    filters: [],
    titleNlsKey: 'selectTerminologyLabel',
  },
};

/**
 *
 * @param {string} editMode
 * @param {Function} translate
 * @returns {object}
 */
const getCustomEditorProps = (editMode, translate) => {
  if (editMode === ADVANCED_CONSTRAINT) {
    return {
      EditorComponent: AdvancedConstraintEditor,
    };
  }
  // ResourceEntryEditor is used for all other edit modes, but with some
  // specific props
  const { emptyListNlsKey, ...props } =
    editMode === CLASS_ENTRY ? classEntryProps : terminologyProps;
  return {
    EditorComponent: ResourceEntryEditor,
    getTitleProps: ({ entry }) => ({
      primary: getLabel(entry),
      secondary: entry.getResourceURI(),
    }),
    placeholderProps: { label: translate(emptyListNlsKey) },
    ...props,
  };
};

const ConstraintEditor = ({
  graph,
  resourceURI,
  namespaceEntries,
  property,
  ...props
}) => {
  const translate = useTranslation(props.nlsBundles);
  const [editMode, setEditMode] = useState(
    getConstraintType(graph, resourceURI) || CLASS_ENTRY
  );

  const { EditorComponent, ...customEditorProps } = getCustomEditorProps(
    editMode,
    translate
  );

  const editModeChoices = getAvailableChoices(
    graph,
    resourceURI,
    MAIN_EDIT_MODE_CHOICES
  ).map(({ value, labelNlsKey }) => ({
    label: translate(labelNlsKey),
    value,
  }));

  return (
    <EditorComponent
      key={editMode}
      graph={graph}
      resourceURI={resourceURI}
      onEditModeChange={setEditMode}
      editModeChoices={editModeChoices}
      property={property}
      namespaceEntries={
        editMode === ADVANCED_CONSTRAINT ? namespaceEntries : null
      }
      {...customEditorProps}
      {...props}
    />
  );
};

ConstraintEditor.propTypes = {
  property: PropTypes.string,
  graph: graphPropType,
  resourceURI: PropTypes.string,
  namespaceEntries: PropTypes.arrayOf(entryPropType),
  nlsBundles: nlsBundlesPropType,
};

const ConstraintEditorWithGetNamespaceEntries = (props) => {
  const { context } = useESContext();
  const { namespaceEntries, status } = useGetNamespaceEntries(context);
  return status === 'resolved' ? (
    <ConstraintEditor namespaceEntries={namespaceEntries} {...props} />
  ) : null;
};

export { ConstraintEditorWithGetNamespaceEntries as ConstraintEditor };
