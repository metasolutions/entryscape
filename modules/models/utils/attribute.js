import { localize } from 'commons/locale';

/* eslint-disable max-len */
export const ATTRIBUTES = [
  {
    name: 'atLeastOneChild',
    description: {
      en: 'Requires that at least one of the children of an object form has a value without specifying which.',
    },
  },
  {
    name: 'atMostOneChild',
    description: {
      en: 'Requires that at most one of the children of an object form has a single value.',
    },
  },
  {
    name: 'exactlyOneChild',
    description: {
      en: 'Requires that at exactly one of the children of an object form has a single value.',
    },
  },
  {
    name: 'autoUUID',
    description: {
      en: 'Automatically initializes a UUID value if no value is present.',
    },
  },
  {
    name: 'autoInitDate',
    description: {
      en: 'Automatically initializes a date value if no value is present.',
    },
  },
  {
    name: 'autoUpdateDate',
    description: { en: 'Automatically set a new date on every edit.' },
  },
  {
    name: 'autoValue',
    description: {
      en: 'Automatically set the value based on an other field as defined via the dependency construction. Note that autoValue can be combined with a valuePattern.',
    },
  },
  {
    name: 'card',
    description: {
      en: 'Provides a card like appearance for an object form in both edit and present modes. Typically displayed with a background color, some indents and a border.',
    },
  },
  {
    name: 'cardInEdit',
    description: {
      en: 'Provides a card like appearance for an object form in edit mode. Typically displayed with a background color, some indents and a border.',
    },
  },
  {
    name: 'cardInPresent',
    description: {
      en: 'Provides a card like appearance for an object form in present mode. Typically displayed with a background color, some indents and a border.',
    },
  },
  {
    name: 'deprecated',
    description: {
      en: 'Marks this field or object form as deprecated so that does not show up unless a value is matched, matched values are clearly marked in editing and validation as deprecated.',
    },
  },
  {
    name: 'dropDown',
    description: {
      en: '	Forces the choices to be in a dropdown, e.g. by default fewer choices than five when max cardinality is one are shown with radiobuttons, this behaviour is prohibited by setting this attribute.',
    },
  },
  {
    name: 'externalLink',
    description: {
      en: 'Forces URI values to be rendered as links that open in a separate window in presentation mode.',
    },
  },
  {
    name: 'image',
    description: { en: 'Renders URI values as images.' },
  },
  {
    name: 'invisible',
    description: {
      en: 'Hides the form-item, if applied to a group it hides the entire form-item tree below as well.',
    },
  },
  {
    name: 'invisibleGroup',
    description: {
      en: 'Flattens deeper structures of RDF realized by object forms by removing the label and indentation. Can only be used when the object form has a max cardinality of 1.',
    },
  },
  {
    name: 'nonEditable',
    description: {
      en: 'Makes an field or object form read-only in edit mode.',
    },
  },
  {
    name: 'multiline',
    description: {
      en: 'Represent the text field with a multi line input field in edit mode.',
    },
  },
  {
    name: 'noLabelInPresent',
    description: {
      en: 'Hides the field or object form label in present mode.',
    },
  },
  {
    name: 'noLink',
    description: {
      en: 'Forces the lookup field to show the value as a URI or a label for the URI if it can be found instead of generating a link / popup dialog in present mode.',
    },
  },
  {
    name: 'compact',
    description: {
      en: 'Forces compact mode for the editor / presenter, i.e. render input values (on children of the top-level group) of the editor / presenter to be rendered on the same row as the label for non-groups.',
    },
  },
  {
    name: 'nonCompact',
    description: {
      en: '	Prohibits compact mode, e.g. if the default of the surrounding implementation is to use compact mode an individual template may override.',
    },
  },
  {
    name: 'preserveOrderOfChoices',
    description: {
      en: 'Prevent options to be alphabetically sorted in dropdowns or when shown as radiobuttons or checkboxes. Instead the specified order of the options are preserved.',
    },
  },
  {
    name: 'showDescriptionInEdit',
    description: {
      en: 'Render the description as a text below the label in edit mode.',
    },
  },
  {
    name: 'showDescriptionInPresent',
    description: {
      en: 'Render the description as a text below the label in present mode.',
    },
  },
  {
    name: 'showValue',
    description: {
      en: 'Show the value in present mode rather than attempt to show a label for a URI. For a field with a value template, show the whole value including the value template.',
    },
  },
  {
    name: 'strictmatch',
    description: {
      en: "Match only explicitly given options in the select field, don't allow a match to be made only based on the property and mark it with an error.",
    },
  },
  {
    name: 'relaxedDatatypeMatch',
    description: {
      en: "Match only values with correct datatype in the datatype field, don't allow a match to be made only based on the property and mark it with an error.",
    },
  },
  {
    name: 'table',
    description: {
      en: 'Renders a table where each row corresponds to a resource matching the group with the table style and each distinct child field / form gets its own column.',
    },
  },
  {
    name: 'linkWithLabel',
    description: {
      en: 'Used on object forms to present URI as links in presentation mode. Use together with child text fields marked with the attributes label and tooltip.',
    },
  },
  {
    name: 'label',
    description: {
      en: 'Used together with the attribute linkWithLabel to generate a link in presentation mode. Marks the current field to be treated as the label for the link.',
    },
  },
  {
    name: 'tooltip',
    description: {
      en: 'Used together with the attribute linkWithLabel to generate a link in presentation mode. Marks the current field to be treated as the tooltip for the link.',
    },
  },
  {
    name: 'verticalCheckBoxes',
    description: {
      en: 'Options are shown for this field as checkboxes oriented vertically rather than repeated dropdowns.',
    },
  },
  {
    name: 'horizontalCheckBoxes',
    description: {
      en: 'Options are shown for this field as checkboxes oriented horizontally rather than repeated dropdowns.',
    },
  },
  {
    name: 'verticalRadioButtons',
    description: {
      en: 'Forces choices to be rendered as radiobuttons in a vertically oriented fashion.',
    },
  },
  {
    name: 'horizontalRadioButtons',
    description: {
      en: 'Forces choices to be rendered as radiobuttons in a horizontally oriented fashion, if radiobuttons are triggered automatically, horizontal is the default orientation.',
    },
  },
  {
    name: 'filterTranslations',
    description: {
      en: 'Treats multiple values as translations in presentation mode if the current field has a nodetype corresponding to language literal. If triggered, it tries to use explicit language tags to filter out a single value corresponding to the most appropriate language.',
    },
  },
  {
    name: 'viewAllTranslations',
    description: {
      en: 'Shows all values for the current field, i.e. overrides any default setting in presentation mode for filtering out translations.',
    },
  },
  {
    name: 'geoPoint',
    description: {
      en: 'Forces the field to be edited / rendered as a map, only supports schema and geo namespaces. This attribute is supported in EntryScape, no generic support is provided in RDForms.',
    },
  },
  {
    name: 'geonames',
    description: {
      en: 'Provides a dialog for selecting values from geonames. This attribute is supported in EntryScape, no generic support is provided in RDForms.',
    },
  },
  // The tree attribute is historical, has no implementation currently.
  /*  {
    name: 'tree',
    description: {
      en: 'Forces choices to be selected from a tree structure in a popup.',
    },
  }, */
  // No implementation in RDForms or EntryScape, unclear why
  /*  { name: 'textAsChoice', description: { en: '' } }, */
  // Nonsensical implementation in RDForms.
  /* {
    name: 'stars',
    description: {
      en: 'Renders N stars where N is a value in the form of a positive integer.',
    },
  }, */
  // No implementation, and unclear how it was used
  /* { name: 'commentOn', description: { en: '' } }, */
  // No implementation in react for email attribute
  /* { name: 'email', description: { en: '' } }, */
  // No implementation in react for firstcolumnfixedtable
  /* {
    name: 'firstcolumnfixedtable',
    description: {
      en: 'A variant of the table style where the first child item is assumed to be a choice. A table is generated with a row for each of the choices of the first choice item.',
    },
  }, */
  // Heading is not shown as an attribute in models as we have section form.
  /* { name: 'heading', description: { en: '' } }, */

  // Expandable sections not supported in react yet.
  /* {
    name: 'expandable',
    description: { en: 'Makes a group and its children expandable.' },
  }, */
  // Unclear if there is a scenario where this is used, also not implemented
  /* { name: 'internalLink', description: { en: '' } }, */
  // Showing URI as text or link at the top of a form, not supported in react yet.
  /* { name: 'showLink', description: { en: '' } },
  { name: 'showURI', description: { en: '' } }, */
];

/**
 *
 * @param {string} attributeName
 * @returns {string}
 */
export const getDescription = (attributeName) => {
  const attribute = ATTRIBUTES.find(({ name }) => name === attributeName);
  return attribute?.description ? localize(attribute.description) : '';
};
