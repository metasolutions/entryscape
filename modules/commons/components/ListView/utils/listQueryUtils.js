import { getTitle, getShortModifiedDate } from 'commons/util/metadata';

export const entriesToListQueryItems = (entries, projectionMapping = {}) =>
  entries.map((entry) => {
    return {
      entry,
      label: getTitle(entry),
      modified: getShortModifiedDate(entry),
      ...entry.projection(projectionMapping),
    };
  });
