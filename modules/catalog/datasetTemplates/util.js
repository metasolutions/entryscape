import config from 'config';
import {
  Entry,
  Context,
  Graph,
  SolrQuery,
  EntryStore,
} from '@entryscape/entrystore-js';
import {
  createEntry,
  getGroupWithHomeContext,
  spreadEntry,
} from 'commons/util/store';
import { entrystore } from 'commons/store';
import {
  RDF_TYPE_DATASET,
  RDF_TYPE_SUGGESTION,
  RDF_STATUS_INVESTIGATING,
  RDF_TYPE_CONCEPT,
  RDF_TYPE_DATASET_TEMPLATE,
} from 'commons/util/entry';
import {
  languageUriToIsoCode,
  getDefaultThemeLabels,
} from 'commons/util/metadata';

const DEFAULT_THEME_URI =
  'http://publications.europa.eu/resource/authority/data-theme';

/**
 *
 * @param {Entry} datasetTemplateEntry
 * @param {Context} context
 * @returns {Promise}
 */
export const createSuggestionFromDatasetTemplate = (
  datasetTemplateEntry,
  context
) => {
  const { metadata: templateMetadata, ruri: templateResourceURI } =
    spreadEntry(datasetTemplateEntry);

  const datasetTemplateTitles = templateMetadata.find(
    templateResourceURI,
    'dcterms:title'
  );
  const datasetTemplateDescriptions = templateMetadata.find(
    templateResourceURI,
    'dcterms:description'
  );

  const newSuggestionEntry = createEntry(context, RDF_TYPE_SUGGESTION);
  const { metadata: newSuggestionMetadata, ruri: newSuggestionResourceURI } =
    spreadEntry(newSuggestionEntry);

  newSuggestionEntry
    .getEntryInfo()
    .getGraph()
    .add(newSuggestionEntry.getURI(), 'store:status', RDF_STATUS_INVESTIGATING);

  newSuggestionMetadata.add(
    newSuggestionEntry.getResourceURI(),
    'rdf:type',
    RDF_TYPE_SUGGESTION
  );

  datasetTemplateTitles.forEach((titleStatement) => {
    newSuggestionMetadata.addL(
      newSuggestionResourceURI,
      'dcterms:title',
      titleStatement.getValue(),
      titleStatement.getLanguage()
    );
  });

  datasetTemplateDescriptions.forEach((descriptionStatement) => {
    newSuggestionMetadata.addL(
      newSuggestionResourceURI,
      'dcterms:description',
      descriptionStatement.getValue(),
      descriptionStatement.getLanguage()
    );
  });

  newSuggestionMetadata.add(
    newSuggestionResourceURI,
    'esterms:basedOn',
    templateResourceURI
  );

  return getGroupWithHomeContext(newSuggestionEntry.getContext()).then(
    (groupEntry) => {
      const entryInformation = newSuggestionEntry.getEntryInfo();
      const accessControl = entryInformation.getACL(true);
      accessControl.admin.push(groupEntry.getId());
      entryInformation.setACL(accessControl);
      return newSuggestionEntry.commit();
    }
  );
};

/**
 *
 * @param {Context} context
 * @returns {Promise}
 */
export const getSuggestionsBasedOnTemplates = async (context) =>
  entrystore
    .newSolrQuery()
    .rdfType([RDF_TYPE_SUGGESTION])
    .uriProperty('esterms:basedOn', '*')
    .context(context)
    .list()
    .getEntries();

/**
 *
 * @param {Context} context
 * @returns {Promise}
 */
export const getDatasetsBasedOnTemplates = async (context) =>
  entrystore
    .newSolrQuery()
    .rdfType([RDF_TYPE_DATASET])
    .uriProperty('esterms:basedOn', '*')
    .context(context)
    .list()
    .getEntries();

/**
 *
 * @param {Entry} templateEntry
 * @param {Entry} newEntry
 * @returns {Graph}
 */
export const getMetadataFromDatasetTemplate = (templateEntry, newEntry) => {
  const newEntryResourceURI = newEntry.getResourceURI();
  const newEntryMetadata = templateEntry
    .getMetadata()
    .clone()
    .replaceURI(templateEntry.getResourceURI(), newEntryResourceURI);
  newEntryMetadata.add(
    newEntryResourceURI,
    'esterms:basedOn',
    templateEntry.getResourceURI()
  );

  const extraTemplatePropertyRelation = config.get(
    'catalog.extraTemplatePropertyRelation'
  );
  if (extraTemplatePropertyRelation) {
    newEntryMetadata.add(
      newEntryResourceURI,
      extraTemplatePropertyRelation,
      templateEntry.getResourceURI()
    );
  }

  newEntryMetadata.findAndRemove(
    newEntryResourceURI,
    'rdf:type',
    RDF_TYPE_DATASET_TEMPLATE
  );
  newEntryMetadata.findAndRemove(newEntryResourceURI, 'dcat:contactPoint');
  newEntryMetadata.findAndRemove(newEntryResourceURI, 'foaf:page');
  newEntryMetadata.findAndRemove(newEntryResourceURI, 'dcterms:publisher');

  // since the metadata is cloned from template, rdf type needs to be readded
  newEntryMetadata.add(newEntryResourceURI, 'rdf:type', RDF_TYPE_DATASET);

  return newEntryMetadata;
};

/**
 *
 * @param {Entry[]} conceptEntries
 * @returns {object[]}
 */
export const getCategoriesFromConcepts = (conceptEntries) => {
  if (!conceptEntries) return [];
  return conceptEntries.map((contextEntry) => {
    const metadata = contextEntry.getMetadata();
    const labelStatements = metadata.find(null, 'skos:prefLabel');
    const labelsByLanguage = labelStatements.reduce(
      (accumulatorObject, statement) => {
        accumulatorObject[statement.getLanguage()] = statement.getValue();
        return accumulatorObject;
      },
      {}
    );
    return { label: labelsByLanguage, value: contextEntry.getResourceURI() };
  });
};

/**
 * Fetches concepts belonging to a terminology.
 *
 * @param {EntryStore} remoteEntryStore
 * @param {string} terminologyURI
 * @param {Entry} catalogEntry
 * @returns {Promise}
 */
export const fetchConcepts = (
  remoteEntryStore,
  terminologyURI,
  catalogEntry
) => {
  const language = catalogEntry
    ? catalogEntry.getMetadata().findFirstValue(null, 'dcterms:language')
    : '';
  const sortOrder = `title.${languageUriToIsoCode(language)}+asc`;

  return remoteEntryStore
    .newSolrQuery()
    .rdfType(RDF_TYPE_CONCEPT)
    .uriProperty('skos:inScheme', terminologyURI)
    .sort(sortOrder)
    .list()
    .getAllEntries();
};

/**
 * Fetches dataset templates.
 *
 * @param {object} queryProperties
 * @param {EntryStore} queryProperties.store
 * @param {Entry} queryProperties.selectedCatalogEntry
 * @param {string} queryProperties.selectedCategory
 * @param {string} queryProperties.searchQuery
 * @param {number} queryProperties.page
 * @param {number} queryProperties.offset
 * @param {number} queryProperties.limit
 * @returns {Promise} A promise that resolves to an object containing an array of entries and the results' size
 */
export const fetchTemplates = async ({
  store,
  selectedCatalogEntry,
  selectedCategory,
  searchQuery,
  page,
  offset,
  limit,
}) => {
  const catalogRURI = selectedCatalogEntry
    ? selectedCatalogEntry.getContext().getResourceURI()
    : null;

  const language = selectedCatalogEntry
    ? selectedCatalogEntry
        .getMetadata()
        .findFirstValue(null, 'dcterms:language')
    : '';
  const sortOrder = `title.${languageUriToIsoCode(language)}+asc`;
  const datasetTemplateRDFType = config.get('catalog.datasetTemplateRDFType');
  const query = store
    .newSolrQuery()
    .rdfType([datasetTemplateRDFType])
    .context(catalogRURI)
    .sort(sortOrder)
    .limit(limit)
    .publicRead(true);

  if (selectedCategory) query.uriProperty('dcat:theme', selectedCategory);
  if (searchQuery) query.title(searchQuery);

  const searchList = query.list();
  const queryPage = page % 10 === 9 ? Math.floor(page / 10) : offset / limit;
  const entries = await searchList.getEntries(queryPage);

  return { entries, size: searchList.getSize() };
};

/**
 * Fetches dataset template categories. These can be either the default DCAT theme
 * taxonomy categories, or concepts belonging to a terminology.
 *
 * @param {EntryStore} store
 * @param {string} taxonomyURI
 * @param {Entry} catalogEntry
 * @returns {Promise} - A promise that resolves to an array of objects containing a string URI 'value', and a localization object 'label'
 */
export const fetchCategories = async (store, taxonomyURI, catalogEntry) => {
  if (!taxonomyURI || taxonomyURI === DEFAULT_THEME_URI) {
    return getDefaultThemeLabels();
  }

  return fetchConcepts(store, taxonomyURI, catalogEntry).then(
    (conceptEntries) => getCategoriesFromConcepts(conceptEntries)
  );
};

/**
 * Fetches catalogs.
 *
 * @param {EntryStore} store
 * @returns {Promise}
 */
export const fetchCatalogs = async (store) =>
  store
    .newSolrQuery()
    .rdfType(config.get('catalog.catalogTemplateRDFType'))
    .publicRead(true)
    .list()
    .getEntries();

/**
 * Get the configurable dataset templates entrystore.
 *
 * @returns {EntryStore}
 */
export const getDatasetTemplateStore = () => {
  const datasetTemplateStoreURI = config.get('catalog.datasetTemplateEndpoint');
  const store = new EntryStore(datasetTemplateStoreURI);
  store.getREST().disableJSONP();
  store.getREST().disableCredentials();

  return store;
};

/**
 *
 * @param {EntryStore} store
 * @param {Entry} catalogEntry
 * @param {string} theme
 * @returns {SolrQuery}
 */
export const createDatasetTemplateQuery = (store, catalogEntry, theme) => {
  const catalogRURI = catalogEntry
    ? catalogEntry.getContext().getResourceURI()
    : null;

  const datasetTemplateRDFType = config.get('catalog.datasetTemplateRDFType');
  const query = store
    .newSolrQuery()
    .rdfType([datasetTemplateRDFType])
    .context(catalogRURI)
    .limit(50)
    .publicRead(true);

  if (theme) query.uriProperty('dcat:theme', theme);

  return query;
};
