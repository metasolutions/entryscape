import {
  ACTION_EDIT,
  ACTION_REMOVE,
  ACTION_REVISIONS,
} from 'commons/components/overview/actions';
import { Edit as EditIcon, Delete as RemoveIcon } from '@mui/icons-material';
import { withOverviewRefresh } from 'commons/components/overview';
import ProjectTypeEditDialog from '../ProjectTypeEditDialog';
import ProjectTypeRemoveDialog from '../ProjectTypeRemoveDialog';

export const sidebarActions = [
  {
    ...ACTION_EDIT,
    Dialog: withOverviewRefresh(ProjectTypeEditDialog, 'onEdit'),
    getProps: () => ({
      action: {
        formTemplateId: 'esc:Group',
      },
      startIcon: <EditIcon />,
    }),
  },
  ACTION_REVISIONS,
  {
    ...ACTION_REMOVE,
    Dialog: ProjectTypeRemoveDialog,
    getProps: () => ({
      startIcon: <RemoveIcon />,
    }),
  },
];

export const rowActions = [ACTION_REVISIONS];
