import PropTypes from 'prop-types';
import { entryPropType } from 'commons/util/entry';
import ListActionDialog from 'commons/components/common/dialogs/ListActionDialog';
import { useTranslation } from 'commons/hooks/useTranslation';
import escoListNLS from 'commons/nls/escoList.nls';
import escaDatasetNLS from 'catalog/nls/escaDataset.nls';
import escaVisualizationNLS from 'catalog/nls/escaVisualization.nls';
import escoGeoMapNLS from 'commons/nls/escoGeoMap.nls';
import VisualizationPreview from '../../VisualizationPreview';

const nlsBundles = [
  escoListNLS,
  escaDatasetNLS,
  escaVisualizationNLS,
  escoGeoMapNLS,
];

const PreviewVisualizationDialog = ({
  entry,
  visualizationEntry,
  closeDialog,
}) => {
  const translate = useTranslation(nlsBundles);

  return (
    <ListActionDialog
      id="preview-visualization-entry"
      closeDialogButtonLabel={translate('previewDialogButtonLabel')}
      maxWidth="md"
      title={translate('previewDialogTitle')}
      closeDialog={closeDialog}
    >
      <VisualizationPreview
        datasetEntry={entry}
        visualizationEntry={visualizationEntry}
      />
    </ListActionDialog>
  );
};

PreviewVisualizationDialog.propTypes = {
  entry: entryPropType,
  visualizationEntry: entryPropType,
  closeDialog: PropTypes.func.isRequired,
};

export default PreviewVisualizationDialog;
