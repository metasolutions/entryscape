import PropTypes from 'prop-types';
import './index.scss';

export const RDFormsFields = ({
  editor = false,
  label = '',
  isRequiredMark = true,
  children,
}) => {
  return (
    <div className={editor ? 'rdforms rdformsEditor' : 'rdforms'}>
      <div className="rdformsRow rdformsTopLevel">
        <div className="rdformsLabelRow">
          <span className="rdformsLabel">{label}</span>
          {isRequiredMark && (
            <span className="rdformsMark rdformsMandatoryMark">*</span>
          )}
        </div>
        <div className="rdformsFields">{children}</div>
      </div>
    </div>
  );
};

RDFormsFields.propTypes = {
  editor: PropTypes.bool,
  label: PropTypes.string,
  isRequiredMark: PropTypes.bool,
  children: PropTypes.node,
};

export const RDFormsField = ({ children }) => {
  return <div className="rdformsField">{children}</div>;
};

RDFormsField.propTypes = {
  children: PropTypes.node,
};

const RDFormsFieldWrapper = ({ label, isRequiredMark, children, ...rest }) => {
  return (
    <RDFormsFields label={label} isRequiredMark={isRequiredMark} {...rest}>
      <RDFormsField>{children}</RDFormsField>
    </RDFormsFields>
  );
};

RDFormsFieldWrapper.propTypes = {
  label: PropTypes.string,
  children: PropTypes.node,
  isRequiredMark: PropTypes.bool,
};

export default RDFormsFieldWrapper;
