import {
  LIST_ACTION_SHARING_SETTINGS,
  LIST_ACTION_EDIT,
  LIST_ACTION_REVISIONS,
  LIST_ACTION_REMOVE,
} from 'commons/components/EntryListView/actions';
import esadContextNLS from 'admin/nls/esadContext.nls';
import escoListNLS from 'commons/nls/escoList.nls';
import escoPlaceholderNLS from 'commons/nls/escoPlaceholder.nls';
import { premiumContextsEnabled, isContextPremium } from 'commons/util/context';

import RemoveProjectDialog from './dialogs/RemoveProjectDialog';
import CreateProjectDialog from './dialogs/CreateProjectDialog';
import ReindexProjectDialog from './dialogs/ReindexProjectDialog';
import UpgradeProjectToPremiumDialog from './dialogs/UpgradeProjectToPremiumDialog';
import DowngradeProjectFromPremiumDialog from './dialogs/DowngradeProjectFromPremiumDialog';

const LIST_ACTION_MAKE_PREMIUM_ID = 'upgradeToPremium';
const LIST_ACTION_REMOVE_PREMIUM_ID = 'downgradeFromPremium';
const LIST_ACTION_REINDEX_ID = 'reindex';

export const nlsBundles = [esadContextNLS, escoListNLS, escoPlaceholderNLS];

export const listActions = [
  {
    id: 'create',
    Dialog: CreateProjectDialog,
    labelNlsKey: 'createButton',
    tooltipNlsKey: 'actionTooltip',
  },
];

export const rowActions = [
  { ...LIST_ACTION_EDIT, formTemplateId: 'esc:Context' },
  LIST_ACTION_REVISIONS,
  {
    id: LIST_ACTION_REINDEX_ID,
    Dialog: ReindexProjectDialog,
    labelNlsKey: 'projectReindex',
  },
  {
    id: LIST_ACTION_SHARING_SETTINGS,
    Dialog: '',
    isVisible: () => false, // no restriction when we develop sharing settings for admin
    labelNlsKey: 'contextACL',
  },
  {
    id: LIST_ACTION_MAKE_PREMIUM_ID,
    Dialog: UpgradeProjectToPremiumDialog,
    isVisible: ({ entry: contextEntry }) =>
      premiumContextsEnabled() && !isContextPremium(contextEntry),
    labelNlsKey: 'makePremium',
  },
  {
    id: LIST_ACTION_REMOVE_PREMIUM_ID,
    Dialog: DowngradeProjectFromPremiumDialog,
    isVisible: ({ entry: contextEntry }) =>
      premiumContextsEnabled() && isContextPremium(contextEntry),
    labelNlsKey: 'unmakePremium',
  },
  { ...LIST_ACTION_REMOVE, Dialog: RemoveProjectDialog },
];
