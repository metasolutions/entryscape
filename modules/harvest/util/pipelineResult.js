import { Graph } from '@entryscape/rdfjson';
import { Entry, terms } from '@entryscape/entrystore-js';
import { EntityType } from 'entitytype-lookup';
import { getMultipleDateFormats } from 'commons/util/date';
import Lookup from 'commons/types/Lookup';
import { localize } from 'commons/locale';
import { checkIsDcatEntityType } from './data';

const validationProperties = [
  {
    nls: 'validationError',
    property: 'storepr:validateErrors',
  },
  {
    nls: 'validationWarning',
    property: 'storepr:validateWarnings',
  },
  {
    nls: 'validationDeprecated',
    property: 'storepr:validateDeprecated',
  },
];

const mergeProperties = [
  {
    nls: 'mergeAdded',
    property: 'storepr:mergeAdded',
  },
  {
    nls: 'mergeUpdated',
    property: 'storepr:mergeUpdated',
  },
  {
    nls: 'mergeRemoved',
    property: 'storepr:mergeRemoved',
  },
  {
    nls: 'mergeUnchanged',
    property: 'storepr:mergeUnchanged',
  },
];

/**
 *
 * @param {Entry} entry
 * @returns {string|number} - nls key or the actual count
 */
const getTitle = (entry) => {
  switch (entry.getEntryInfo().getStatus()) {
    case terms.status.InProgress:
      return 'jobRunning';
    case terms.status.Pending:
      return 'jobPending';
    case terms.status.Failed:
      return 'jobFailed';
    default: {
      const count = parseInt(
        entry
          .getMetadata()
          .findFirstValue(entry.getResourceURI(), 'storepr:mergeResourceCount'),
        10
      );
      if (typeof count === 'number' && count > 0) {
        return count;
      }
      return 'noDatasets';
    }
  }
};

/**
 *
 * @param {Entry} entry
 * @returns {string}
 */
const getDate = (entry) => {
  let modDate = entry.getEntryInfo().getCreationDate();
  try {
    modDate = getMultipleDateFormats(modDate).short;
  } catch (e) {
    console.log('Failed to format modification date');
    modDate = '';
  }
  return modDate;
};

/**
 * Get the merge results for each entity type found on the pipeline entry's
 * metadata graph.
 *
 * @param {Graph} graph - metadata graph of the pipeline entry
 * @returns {object}
 */
const getMergeResultsByType = (graph) => {
  const blanks = graph
    .find(null, 'storepr:mergeEntityType')
    .map((statement) => statement.getValue());

  const resultsByType = {};
  blanks.forEach((blank) => {
    mergeProperties.forEach(({ property, nls: propertyNLS }) => {
      const type = graph.findFirstValue(blank, 'storepr:entityType');
      const value = parseInt(graph.findFirstValue(blank, property) || 0, 10);
      resultsByType[type] = {
        ...(resultsByType[type] || {}),
        ...{ [propertyNLS]: value },
      };
    });
  });

  return resultsByType;
};

/**
 * Gets merge, validation and status data from the pipeline entry.
 *
 * @param {Entry} entry
 * @param {EntityType} entityType
 * @returns {object}
 */
const getBodyData = (entry, entityType) => {
  const body = {
    data: {},
    validationResults: {},
    mergeResults: {},
  };
  switch (entry.getEntryInfo().getStatus()) {
    case terms.status.InProgress:
    case terms.status.Pending:
      return body;
    default:
      {
        const md = entry.getMetadata();
        const isDcat = checkIsDcatEntityType(entityType);

        // psi data page status
        if (isDcat) {
          const psidataPage = md.findFirstValue(null, 'storepr:check');
          body.data.psiDataPageStatus = psidataPage
            ? 'foundText'
            : 'notFoundText';
        }

        // dcat source
        const dcatSource =
          md.findFirstValue(null, 'storepr:fetchSource') === 'true';
        body.data.dcatStatus = dcatSource ? 'foundText' : 'notFoundText';

        if (dcatSource) {
          const dcatRdf =
            md.findFirstValue(null, 'storepr:fetchRDF') === 'true';
          if (!dcatRdf) {
            const rdfError = md.findFirstValue(null, 'storepr:fetchRDFError');
            if (rdfError) {
              body.data.dcatStatus = 'invalidRDFStatus';
            } else {
              body.data.dcatStatus = 'formatStatus';
            }
            return body;
          }

          // add validation messages
          validationProperties.forEach((item) => {
            body.validationResults[item.nls] = parseInt(
              md.findFirstValue(null, item.property) || 0,
              10
            );
          });
          mergeProperties.forEach((item) => {
            body.mergeResults[item.nls] = parseInt(
              md.findFirstValue(null, item.property) || 0,
              10
            );
          });

          body.mergeResultsByType = getMergeResultsByType(md);

          // if any validation failed then provide different data layout
          body.validationFailed = Object.keys(body.validationResults).some(
            (name) => body.validationResults[name] > 0
          );
        } else {
          // dont display anything
          body.data.dcatStatus = 'notFoundText';
        }
      }
      return body;
  }
};

/**
 * Gets pipeline entry data.
 *
 * @param {Entry} entry
 * @param {EntityType} entityType
 * @returns {object}
 */
export const getData = (entry, entityType) => {
  const renderTitle = getTitle(entry);
  const date = (getDate(entry) || '').toString();
  const body = getBodyData(entry, entityType);
  const status = entry.getEntryInfo().getStatus();

  return {
    id: entry.getId(),
    title: renderTitle,
    date,
    body,
    status,
  };
};

/**
 *
 * @param {object} mergeResultsByType
 * @returns {Array}
 */
export const mergeResultsToRows = (mergeResultsByType) =>
  Object.keys(mergeResultsByType).map((type) => {
    const results = mergeResultsByType[type];
    const mergePropKeys = Object.keys(results);
    const entityType = Lookup.getEntityTypeByRdfType(type, 'register');

    if (!entityType) {
      console.error(`No entity type found for: ${type}`);
      return null;
    }

    return [
      localize(entityType.get('label')),
      ...mergePropKeys.map((mergeKey) => results[mergeKey]),
    ];
  });
