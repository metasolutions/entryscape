import {
  ACTION_EDIT,
  ACTION_REMOVE,
  ACTION_REVISIONS,
} from 'commons/components/overview/actions';
import {
  canReplaceEntityFile,
  canReplaceEntityLink,
  canDownloadEntity,
} from 'commons/util/store';
import ReplaceLinkDialog from 'commons/components/common/dialogs/ReplaceLinkDialog';
import DownloadResourceDialog from 'commons/components/common/dialogs/DownloadResourceDialog';
import { withOverviewRefresh } from 'commons/components/overview';
import ReplaceFileDialog from '../dialogs/ReplaceFileDialog';

const sidebarActions = [
  ACTION_EDIT,
  ACTION_REVISIONS,
  {
    id: 'replace-file',
    Dialog: withOverviewRefresh(ReplaceFileDialog, 'onSave'),
    isVisible: ({ entry }) => canReplaceEntityFile(entry),
    labelNlsKey: 'replaceFileMenu',
  },
  {
    id: 'replace-link',
    Dialog: ReplaceLinkDialog,
    isVisible: ({ entry }) => canReplaceEntityLink(entry),
    labelNlsKey: 'replaceLinkMenu',
  },
  {
    id: 'download',
    Dialog: DownloadResourceDialog,
    isVisible: ({ entry }) => canDownloadEntity(entry),
    labelNlsKey: 'downloadButton',
    highlightId: 'overviewActionDownload',
  },
  ACTION_REMOVE,
];

export { sidebarActions };
