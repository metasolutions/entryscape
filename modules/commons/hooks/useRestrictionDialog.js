import config from 'config';
import useExternalContentDialog from './useExternalContentDialog';

const useRestrictionDialog = ({ contentPath, ...rest }) => {
  const templatePath =
    contentPath || config.get('theme.commonRestrictionTextPath');
  useExternalContentDialog({ ...rest, contentPath: templatePath });
};

export default useRestrictionDialog;
