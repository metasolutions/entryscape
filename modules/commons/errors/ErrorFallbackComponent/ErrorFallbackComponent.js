import PropTypes from 'prop-types';
import { useTranslation } from 'commons/hooks/useTranslation';
import escoErrors from 'commons/nls/escoErrors.nls';
import ErrorView from 'commons/view/Error';
import { UNAUTHORIZED } from '../utils/async';

const ErrorFallbackComponent = ({ error, children }) => {
  const t = useTranslation(escoErrors);
  const status = error?.status;
  const errorMessage = error?.message || t('genericProblem');

  if (status === UNAUTHORIZED) return children;

  return (
    <ErrorView
      header={t('errorHeader')}
      body={DEVELOPMENT ? errorMessage : ''}
    />
  );
};

ErrorFallbackComponent.propTypes = {
  error: PropTypes.shape({
    name: PropTypes.string,
    message: PropTypes.string,
    status: PropTypes.number,
  }),
  children: PropTypes.node,
};

export default ErrorFallbackComponent;
