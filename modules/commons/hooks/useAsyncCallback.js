import { useEffect, useRef } from 'react';
import useAsync from 'commons/hooks/useAsync';

const useAsyncCallback = (
  callback,
  prop,
  initialValue = null,
  refreshCount
) => {
  const callbackRef = useRef(callback);
  const { runAsync, data, error, isLoading, status } = useAsync({
    data: initialValue,
  });

  useEffect(() => {
    runAsync(Promise.resolve(callbackRef.current(prop)));
  }, [runAsync, prop, refreshCount]);

  return [data, { error, isLoading, status }];
};

export default useAsyncCallback;
