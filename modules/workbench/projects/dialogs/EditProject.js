import { updateContextTitle } from 'commons/util/context';
import ListEditEntryDialog from 'commons/components/EntryListView/dialogs/ListEditEntryDialog';

const templateId = 'esc:Context'; // TODO const

const EditProject = (actionsProps) => {
  return (
    <ListEditEntryDialog
      {...actionsProps}
      formTemplateId={templateId}
      onEntryEdit={updateContextTitle}
    />
  );
};

export default EditProject;
