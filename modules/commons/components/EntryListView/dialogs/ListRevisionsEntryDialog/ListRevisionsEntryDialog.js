import PropTypes from 'prop-types';
import { useCallback } from 'react';
import RevisionsEntryDialog from 'commons/components/entry/RevisionsEntryDialog';
import { REFRESH, useListModel } from 'commons/components/ListView';

const ListRevisionsEntryDialog = ({ closeDialog, ...rest }) => {
  const [, dispatch] = useListModel();

  const updateParentList = useCallback(() => {
    closeDialog();
    dispatch({ type: REFRESH });
  }, [closeDialog, dispatch]);

  return (
    <RevisionsEntryDialog
      onRevertCallback={updateParentList}
      closeDialog={closeDialog}
      {...rest}
    />
  );
};

ListRevisionsEntryDialog.propTypes = {
  closeDialog: PropTypes.func,
};

export default ListRevisionsEntryDialog;
