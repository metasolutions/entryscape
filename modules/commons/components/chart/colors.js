const blue = '#1395BA';
const blueLight = '#A0D4E3';
const orange = '#F26522';
const orangeLight = '#F9AA7B';
const yellow = '#FFCE2B';
const yellowLight = '#FEE8AF';
const gray = '#5F6A6F';
const grayLight = '#AEB4B6';

const COLOURS_BAR_CHART = [
  {
    borderColor: blue,
    backgroundColor: blue,
    borderWidth: 3,
  },
  {
    borderColor: yellow,
    backgroundColor: yellow,
    borderWidth: 3,
  },
  {
    borderColor: gray,
    backgroundColor: gray,
    borderWidth: 3,
  },
  {
    borderColor: orange,
    backgroundColor: orange,
    borderWidth: 3,
  },
  {
    borderColor: orangeLight,
    backgroundColor: orangeLight,
    borderWidth: 3,
  },
];

const COLOURS_TIME_CHART = [
  {
    borderColor: orange,
    backgroundColor: orange,
    borderWidth: 3,
  },
  {
    borderColor: blue,
    backgroundColor: blue,
    borderWidth: 3,
  },
];

const COLOURS_PIE = [blue, yellow, gray, orange];

// Used in cases where we want the colours to represent status.
// Eg. broken, excluded or successful link checks.
const COLOURS_PIE_STATUS = ['#f27173', '#c7d048', '#36A2EB'];

const COLOURS_PLACEHOLDER = [
  {
    backgroundColor: 'rgba(0, 131, 143,0.05)',
  },
  {
    backgroundColor: 'rgba(22, 91, 152,0.05)',
  },
];

export {
  COLOURS_BAR_CHART,
  COLOURS_PIE,
  COLOURS_PIE_STATUS,
  COLOURS_PLACEHOLDER,
  COLOURS_TIME_CHART,
};
