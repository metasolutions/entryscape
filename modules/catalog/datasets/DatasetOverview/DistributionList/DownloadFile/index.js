import PropTypes from 'prop-types';
import { Entry } from '@entryscape/entrystore-js';
import { useEffect } from 'react';

const DownloadFile = ({ closeDialog, entry: fileEntry }) => {
  useEffect(() => {
    const downloadURI = fileEntry.getResourceURI();
    window.open(`${downloadURI}?download`, '_self');
    closeDialog();
  }, [fileEntry, closeDialog]);

  return null;
};

DownloadFile.propTypes = {
  closeDialog: PropTypes.func.isRequired,
  entry: PropTypes.instanceOf(Entry),
};
export default DownloadFile;
