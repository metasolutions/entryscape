import {
  SIDEBAR_ACTION_EDIT,
  SIDEBAR_ACTION_DOWNLOAD,
  SIDEBAR_ACTION_REMOVE,
} from 'commons/components/EntityOverview/actions';

export const sidebarActions = [
  SIDEBAR_ACTION_EDIT,
  SIDEBAR_ACTION_DOWNLOAD,
  SIDEBAR_ACTION_REMOVE,
];
