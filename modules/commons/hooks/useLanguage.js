import { createContext, useContext, useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { i18n } from 'esi18n';
import { renderingContext } from '@entryscape/rdforms';
import escoRdformsNLS from 'commons/nls/escoRdforms.nls';
import { useUserState } from 'commons/hooks/useUser';
import {
  getBestLanguage,
  getClientAcceptLanguages,
  getLocaleSupportedLanguages,
} from 'commons/locale';

const LanguageContext = createContext([i18n.getLocale(), () => {}]);

const useLanguage = () => {
  const languageContext = useContext(LanguageContext);

  if (languageContext === undefined) {
    throw new Error('useLanguage must be used within a ContextProvider');
  }

  return languageContext;
};

const LanguageProvider = ({ children }) => {
  const { userInfo } = useUserState();
  const [language, setLanguage] = useState(getBestLanguage(userInfo.language));

  // Whenever lanugage is changed, language needs to be updated for:
  // 1. locale for i18n. This step is done outside useEffect to avoid race
  //    condition in useTranslation
  // 2. pimrary language codes for renderingContext
  // 3. language set in document
  useEffect(() => {
    document.documentElement.lang = language;

    renderingContext.setPrimaryLanguageCodes([
      ...new Set([
        language,
        ...getClientAcceptLanguages(userInfo),
        ...getLocaleSupportedLanguages(),
      ]),
    ]);
    renderingContext.setMessages(i18n.getLocalization(escoRdformsNLS));
  }, [language, userInfo]);

  if (language !== i18n.getLocale()) {
    i18n.setLocale(language);
  }

  return (
    <LanguageContext.Provider value={[language, setLanguage]}>
      {children}
    </LanguageContext.Provider>
  );
};

LanguageProvider.propTypes = {
  children: PropTypes.node,
};

export default useLanguage;
export { useLanguage, LanguageProvider };
