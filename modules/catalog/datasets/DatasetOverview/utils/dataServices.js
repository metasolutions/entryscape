import { entrystore, entrystoreUtil } from 'commons/store';
import { RDF_TYPE_DATASERVICE, RDF_TYPE_CATALOG } from 'commons/util/entry';

/**
 *
 * @param {*} datasetEntry
 * @returns {Promise.<Entry[]>}
 */
export const getDataServices = async (datasetEntry) =>
  entrystore
    .newSolrQuery()
    .rdfType(RDF_TYPE_DATASERVICE)
    .context(datasetEntry.getContext())
    .uriProperty('dcat:servesDataset', datasetEntry.getResourceURI())
    .list()
    .getEntries();

/**
 * Update the catalog entry with the new dataservice
 *
 * @param {Entry} dataService
 * @returns {Promise}
 */
export const updateCatalogWithDataService = (dataService) => {
  return entrystoreUtil
    .getEntryByType(RDF_TYPE_CATALOG, dataService.getContext())
    .then((catalog) => {
      catalog
        .getMetadata()
        .add(
          catalog.getResourceURI(),
          RDF_TYPE_DATASERVICE,
          dataService.getResourceURI()
        );

      return catalog.commitMetadata().then(() => {
        dataService.setRefreshNeeded();
        dataService.refresh();
      });
    })
    .catch((error) => {
      throw Error(
        `Unable to update the catalog with the new data service: ${error}`
      );
    });
};
