import React from 'react';
import PropTypes from 'prop-types';
import { Card, CardContent } from '@mui/material';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import { Link } from 'react-router-dom';
import './style.scss';

const addToClass = (add) => (className) => `${className}--${add}`;
function IndexCard({ label, value, link, isEmpty = false, color = 'red' }) {
  const addColor = addToClass(color);
  return (
    <Card
      classes={{
        root:
          addColor('escoIndexCard') +
          (isEmpty ? ' escoIndexCard--semitransparent' : ''),
      }}
    >
      <CardContent classes={{ root: 'escoIndexCard__content' }}>
        <Link to={link} className="escoIndexCard__link">
          <div className="escoIndexCard__container">
            <div className="escoIndexCard__number">{value}</div>
            <div className={addColor('escoIndexCard__text')}>
              <p>
                {label}
                <ChevronRightIcon classes={{ root: 'escoIndexCard__icon' }} />
              </p>
            </div>
          </div>
        </Link>
      </CardContent>
    </Card>
  );
}

IndexCard.propTypes = {
  label: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  link: PropTypes.string.isRequired,
  isEmpty: PropTypes.bool,
  color: PropTypes.string,
};

export default IndexCard;
