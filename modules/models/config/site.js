import ModelsView from 'models/models/ModelsView';
import FieldsView from 'models/fields/FieldsView';
import Overview from 'models/models/ModelOverview';
import DiagramView from 'models/diagrams/DiagramView';
import FormsView from 'models/forms/FormsView';
import ResourcesView from 'models/builds/ResourcesView';
import FormOverview from 'models/forms/FormOverview';
import FieldOverview from 'models/fields/FieldOverview';
import ResourceOverview from 'models/builds/ResourceOverview';
import NamespaceOverview from 'models/namespaces/NamespaceOverview';
import ClassOverview from 'models/classes/ClassOverview';
import ClassesView from 'models/classes/ClassesView';
import PropertiesView from 'models/properties/PropertiesView';
import PropertyOverview from 'models/properties/PropertyOverview';
import NamespacesView from 'models/namespaces/NamespacesView';
import { getSecondaryNavViewsFromEntityTypes } from 'commons/util/site';
import SpecificationsView from '../specifications/SpecificationsView';
import SpecificationsOverview from '../specifications/SpecificationsOverview';
import {
  NAMESPACES_LISTVIEW,
  CLASSES_LISTVIEW,
  PROPERTIES_LISTVIEW,
  FORMS_LISTVIEW,
  FIELDS_LISTVIEW,
  DIAGRAMS_LISTVIEW,
  RESOURCES_LISTVIEW,
  SPECIFICATIONS_LISTVIEW,
  NAMESPACE_OVERVIEW_NAME,
  FIELD_OVERVIEW_NAME,
  FORM_OVERVIEW_NAME,
  SPECIFICATION_OVERVIEW_NAME,
  CLASS_OVERVIEW_NAME,
  PROPERTY_OVERVIEW_NAME,
  RESOURCE_OVERVIEW_NAME,
} from './config';

export default {
  modules: [
    {
      name: 'models',
      productName: { en: 'Models', sv: 'Models', de: 'Models' },
      sidebar: true,
      startView: 'models__list',
      getLayoutProps: getSecondaryNavViewsFromEntityTypes,
    },
  ],
  views: [
    {
      name: 'models__list',
      class: ModelsView,
      title: { en: 'Models', sv: 'Models', de: 'Models' },
      route: '/models',
      module: 'models',
    },
    {
      name: 'models__overview',
      class: Overview,
      title: { en: 'Overview', sv: 'Översikt', de: 'Überblick' },
      route: '/models/:contextId/overview',
      parent: 'models__list',
      module: 'models',
    },
    {
      name: NAMESPACES_LISTVIEW,
      class: NamespacesView,
      title: { en: 'Namespaces', sv: 'Namnrymder', de: 'Namensräume' },
      route: '/models/:contextId/namespaces',
      parent: 'models__list',
      module: 'models',
    },
    {
      name: NAMESPACE_OVERVIEW_NAME,
      class: NamespaceOverview,
      title: { en: 'Namespace', sv: 'Namnrymd', de: 'Namensraum' },
      route: '/models/:contextId/namespaces/:entryId/overview',
      parent: 'models__namespaces',
      module: 'models',
    },
    {
      name: CLASSES_LISTVIEW,
      class: ClassesView,
      title: { en: 'Classes', sv: 'Klasser', de: 'Klassen' },
      route: '/models/:contextId/classes',
      parent: 'models__list',
      module: 'models',
    },
    {
      name: CLASS_OVERVIEW_NAME,
      class: ClassOverview,
      title: { en: 'Class', sv: 'Klass', de: 'Klasse' },
      route: '/models/:contextId/classes/:entryId/overview',
      parent: 'models__classes',
      module: 'models',
    },
    {
      name: PROPERTIES_LISTVIEW,
      class: PropertiesView,
      title: { en: 'Properties', sv: 'Egenskaper', de: 'Eigenschaften' },
      route: '/models/:contextId/properties',
      parent: 'models__list',
      module: 'models',
    },
    {
      name: PROPERTY_OVERVIEW_NAME,
      class: PropertyOverview,
      title: { en: 'Property', sv: 'Egenskap', de: 'Eigenschaft' },
      route: '/models/:contextId/properties/:entryId/overview',
      parent: 'models__properties',
      module: 'models',
    },
    {
      name: FORMS_LISTVIEW,
      class: FormsView,
      title: { en: 'Forms', sv: 'Formulär', de: 'Formulare' },
      route: '/models/:contextId/forms',
      parent: 'models__list',
      module: 'models',
    },
    {
      name: FORM_OVERVIEW_NAME,
      class: FormOverview,
      title: { en: 'Form', sv: 'Formulär', de: 'Formular' },
      route: '/models/:contextId/forms/:entryId/overview',
      parent: 'models__forms',
      module: 'models',
    },
    {
      name: FIELDS_LISTVIEW,
      class: FieldsView,
      title: { en: 'Fields', sv: 'Fält', de: 'Felder' },
      route: '/models/:contextId/fields',
      parent: 'models__list',
      module: 'models',
    },
    {
      name: FIELD_OVERVIEW_NAME,
      class: FieldOverview,
      title: { en: 'Field', sv: 'Fält', de: 'Feld' },
      route: '/models/:contextId/fields/:entryId/overview',
      parent: 'models__fields',
      module: 'models',
    },
    {
      name: DIAGRAMS_LISTVIEW,
      class: DiagramView,
      title: { en: 'Diagram', sv: 'Diagram', de: 'Diagramm' },
      route: '/models/:contextId/diagram',
      parent: 'models__list',
      module: 'models',
    },
    {
      name: RESOURCES_LISTVIEW,
      class: ResourcesView,
      title: { en: 'Resources', sv: 'Resurser', de: 'Ressourcen' },
      route: '/models/:contextId/resources',
      parent: 'models__list',
      module: 'models',
    },
    {
      name: RESOURCE_OVERVIEW_NAME,
      class: ResourceOverview,
      title: {
        en: 'Resource',
        sv: 'Resurs',
        de: 'Ressource',
      },
      route: '/models/:contextId/resources/:entryId/overview',
      parent: 'models__resources',
      module: 'models',
    },
    {
      name: SPECIFICATIONS_LISTVIEW,
      class: SpecificationsView,
      title: {
        en: 'Specifications',
        sv: 'Specifikationer',
        de: 'Spezifikationen',
      },
      route: '/models/:contextId/specifications',
      parent: 'models__list',
      module: 'models',
    },
    {
      name: SPECIFICATION_OVERVIEW_NAME,
      class: SpecificationsOverview,
      title: { en: 'Specification', sv: 'Specifikation', de: 'Spezifikation' },
      route: '/models/:contextId/specifications/:entryId/overview',
      parent: 'models__specifications',
      module: 'models',
    },
  ],
};
