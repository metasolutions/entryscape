import PropTypes from 'prop-types';
import ListActionDialog from 'commons/components/common/dialogs/ListActionDialog';
import {
  useTranslation,
  nlsBundlesPropType,
} from 'commons/hooks/useTranslation';
import { CREATE, useListModel } from 'commons/components/ListView';
import ContentWrapper from 'commons/components/common/ContentWrapper';
import { FieldEditor } from 'models/editors';
import { Form, useEditorContext } from 'commons/components/forms/editors';
import useAsync, { PENDING } from 'commons/hooks/useAsync';
import { addAccessRightToGroup } from 'commons/util/entry';
import { getGroupWithHomeContext } from 'commons/util/store';
import useErrorHandler from 'commons/errors/hooks/useErrorHandler';
import LoadingButton from 'commons/components/LoadingButton';
import { useConfirmCloseAction } from 'commons/components/EntryListView/dialogs/create/handlers';

const CreateDialog = ({
  closeDialog,
  title,
  fields,
  beforeCreate,
  nlsBundles,
  onEditEntry,
}) => {
  const [, dispatch] = useListModel();
  const translate = useTranslation(nlsBundles);
  const { runAsync, error, status } = useAsync();
  useErrorHandler(error);
  const {
    entry: prototypeEntry,
    graph,
    resourceURI,
    validate,
    canSubmit,
  } = useEditorContext();
  const confirmClose = useConfirmCloseAction(closeDialog);

  const dispatchEntryCreated = () => dispatch({ type: CREATE });

  const handleCreateEntry = () => {
    const errors = validate();
    if (errors.length) {
      return;
    }

    const createEntry = async () => {
      try {
        await Promise.resolve(beforeCreate(graph, resourceURI));
        prototypeEntry.setMetadata(graph);
        const group = await getGroupWithHomeContext(
          prototypeEntry.getContext()
        );
        addAccessRightToGroup(prototypeEntry, group, 'owner');
        await prototypeEntry.commit();
        dispatchEntryCreated();
        closeDialog();
        onEditEntry?.();
      } catch (createError) {
        throw new Error(translate('createFailMessage'));
      }
    };

    runAsync(createEntry());
  };

  const actions = (
    <LoadingButton
      onClick={handleCreateEntry}
      loading={status === PENDING}
      disabled={!canSubmit}
    >
      {translate('createButtonLabel')}
    </LoadingButton>
  );

  if (!graph) return null;

  return (
    <ListActionDialog
      id="models-create-dialog"
      title={title}
      actions={actions}
      closeDialog={() => confirmClose(graph.isChanged())}
      maxWidth="md"
    >
      <ContentWrapper>
        <Form
          fields={fields}
          size="small"
          nlsBundles={nlsBundles}
          FieldGroupEditor={FieldEditor}
        />
      </ContentWrapper>
    </ListActionDialog>
  );
};

CreateDialog.propTypes = {
  closeDialog: PropTypes.func.isRequired,
  fields: PropTypes.arrayOf(PropTypes.shape({})),
  title: PropTypes.string,
  beforeCreate: PropTypes.func,
  onEditEntry: PropTypes.func,
  nlsBundles: nlsBundlesPropType,
};

export default CreateDialog;
