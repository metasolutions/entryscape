import PropTypes from 'prop-types';
import { useEffect, useState } from 'react';
import config from 'config';
import CircularProgress from '@mui/material/CircularProgress';
import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import DialogContent from '@mui/material/DialogContent';
import DialogActions from '@mui/material/DialogActions';
import Button from '@mui/material/Button';
import escoEntryChooserNlS from 'commons/nls/escoEntryChooser.nls';
import escoDialogsNlS from 'commons/nls/escoDialogs.nls';
import escoRdformsNLS from 'commons/nls/escoRdforms.nls';
import { useTranslation } from 'commons/hooks/useTranslation';
import useAsync from 'commons/hooks/useAsync';
import {
  getChildren,
  getHierarchy,
  getGeonameURI,
  getGeonameId,
} from 'commons/components/rdforms/GeonamesChooser/util';
import {
  useListModel,
  REFRESH_AND_CLEAR,
  LIST_MODEL_DEFAULT_VALUE,
  withListModelProvider,
} from 'commons/components/ListView';
import { isUri } from 'commons/util/util';
import LocationSelect from './LocationSelect';
import './showDialog.scss';

/**
 * Get data for a location specified by a geoname id as well as its hiearchy and children
 *
 * @param {number} id geonames id
 * @returns {object}
 */
const getGeonameData = async (id) => {
  const hierarchy = await getHierarchy(id);
  const children = await getChildren(id);
  const currentSelection = hierarchy.filter((item) => item.geonameId === id);
  return {
    currentSelection,
    hierarchy,
    children,
  };
};

const GeonamesChooserDialogInitial = ({ binding, onClose, onSelect }) => {
  const itemLabel = binding.getItem().getLabel();
  const itemValue = binding.getValue();
  const [open, setOpen] = useState(true);
  const isGeonameAPIConfigured = Boolean(config.get('itemstore.geonamesAPI'));
  const geonamesStartConfig = config.get('itemstore.geonamesStart');
  const [geonameId, setGeonameId] = useState(getGeonameId(itemValue));
  const [hasChange, setHasChange] = useState(!getGeonameId(itemValue));
  const [, dispatch] = useListModel();

  const { data: geonameData, runAsync, status } = useAsync(null);

  useEffect(() => {
    if (!isGeonameAPIConfigured) {
      console.error('No geonames API configuration found');
    }
  }, [isGeonameAPIConfigured]);

  useEffect(() => {
    if (!geonameId && !geonamesStartConfig) {
      console.error('Missing geonameId');
      return;
    }

    if (!geonameId) {
      const id = isUri(geonamesStartConfig)
        ? geonamesStartConfig
        : parseInt(geonamesStartConfig, 10);
      setGeonameId(id);
    }
  }, [isGeonameAPIConfigured, geonameId, geonamesStartConfig]);

  useEffect(() => {
    if (geonameId) runAsync(getGeonameData(geonameId));
  }, [geonameId, runAsync]);

  const handleLocationChange = (id) => {
    dispatch({ type: REFRESH_AND_CLEAR });
    setHasChange(true);
    setGeonameId(id);
  };

  const onDialogClose = () => {
    setOpen(false);
    setTimeout(onClose, 500);
  };

  const handleSelect = async (id, name) => {
    const value = await getGeonameURI(id);
    const choice = {
      value,
      label: { en: name },
      inlineLabel: true,
    };
    onSelect(choice);
    onDialogClose();
  };

  const translate = useTranslation([escoEntryChooserNlS, escoDialogsNlS]);
  const nlsBundles = [escoEntryChooserNlS, escoRdformsNLS];
  const currentSelection = geonameData?.currentSelection?.[0];

  return isGeonameAPIConfigured ? (
    <Dialog
      classes={{ paperScrollPaper: 'escoGeonamesChooser__dialog' }}
      fullWidth
      maxWidth="lg"
      open={open}
      onClose={onClose}
    >
      <DialogTitle>{itemLabel}</DialogTitle>
      <DialogContent classes={{ root: 'escoGeonamesChooser__dialogContent' }}>
        {geonameData ? (
          <LocationSelect
            locations={geonameData}
            geonameId={geonameId}
            handleLocationChange={handleLocationChange}
            nlsBundles={nlsBundles}
            status={status}
          />
        ) : (
          <div className="escoGeonamesChooser__loading">
            <CircularProgress />
          </div>
        )}
      </DialogContent>
      <DialogActions>
        <Button autoFocus={!currentSelection} variant="text" onClick={onClose}>
          {translate('cancel')}
        </Button>
        {currentSelection ? (
          <Button
            autoFocus
            disabled={!hasChange}
            onClick={() =>
              handleSelect(
                currentSelection.geonameId,
                currentSelection.toponymName
              )
            }
          >
            {translate('save')}
          </Button>
        ) : null}
      </DialogActions>
    </Dialog>
  ) : null;
};

GeonamesChooserDialogInitial.propTypes = {
  binding: PropTypes.shape({
    getItem: PropTypes.func,
    getValue: PropTypes.func,
  }),
  onSelect: PropTypes.func,
  onClose: PropTypes.func,
};

const GeonamesChooserDialog = withListModelProvider(
  GeonamesChooserDialogInitial,
  () => ({
    ...LIST_MODEL_DEFAULT_VALUE,
    sort: { field: 'name', order: 'asc' },
  })
);

const showDialog = (binding, onSelect, field) => {
  const child = (
    <GeonamesChooserDialog
      key="geonamesChooser"
      binding={binding}
      onClose={() => field.removeChild(child)}
      onSelect={onSelect}
    />
  );
  field.appendChild(child);
};

export default showDialog;
