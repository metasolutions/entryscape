import PropTypes from 'prop-types';
import config from 'config';
import esteSchemeNLS from 'terms/nls/esteScheme.nls';
import esteTerminologyNLS from 'terms/nls/esteTerminology.nls';
import { useState, useEffect } from 'react';
import { shouldUseUriPattern } from 'commons/util/store';
import { useTranslation } from 'commons/hooks/useTranslation';
import { TextField, InputAdornment, MenuItem } from '@mui/material';
import ContentWrapper from 'commons/components/common/ContentWrapper';
import { useUserState } from 'commons/hooks/useUser';
import {
  createTerminology,
  getConceptSchemeEntityType,
  getPublishersByContext,
} from 'terms/utils';
import { updateContextTitle } from 'commons/util/context';
import { useEntityTitleAndURI } from 'commons/hooks/useEntityTitleAndURI';
import ListActionDialog from 'commons/components/common/dialogs/ListActionDialog';
import { CREATE, useListModel } from 'commons/components/ListView';
import useRestrictionDialog from 'commons/hooks/useRestrictionDialog';
import ProjectTypeChooser from 'commons/types/ProjectTypeChooser';
import Lookup from 'commons/types/Lookup';
import { useSnackbar } from 'commons/hooks/useSnackbar';
import useAsync, { PENDING } from 'commons/hooks/useAsync';
import LoadingButton from 'commons/components/LoadingButton';
import { ErrorCatcher } from 'commons/errors/ErrorCatcher';
import { ErrorWithMessage } from 'commons/errors/utils/errors';
import { useConfirmCloseAction } from 'commons/components/EntryListView/dialogs/create/handlers';
import useTerminologyRestriction from '../useTerminologyRestriction';

const CreateTerminologyDialog = ({ numberOfTerminologies, closeDialog }) => {
  const [, dispatch] = useListModel();
  const [selectedProjectType, setSelectedProjectType] = useState('');
  const entityType = getConceptSchemeEntityType(
    Lookup.getProjectType(selectedProjectType)
  );
  const [addSnackbar] = useSnackbar();
  const translate = useTranslation([esteSchemeNLS, esteTerminologyNLS]);
  const { runAsync, status, error: createError } = useAsync();

  const hasEntityTypeURIPattern = shouldUseUriPattern(entityType);
  const entityTypeUriPatternBase =
    hasEntityTypeURIPattern && entityType.getUriPatternBase();
  const uriPattern = (hasEntityTypeURIPattern && entityType.uriPattern) || '';

  const { userEntry, userInfo } = useUserState();
  const { title, name, isNameFree, handleTitleChange, handleNameChange } =
    useEntityTitleAndURI({
      uriPattern,
    });

  const [description, setDescription] = useState(''); // UI: description
  const [publishers, setPublishers] = useState([]);
  const [publisher, setPublisher] = useState(''); // UI: publisher RURI, if applicable
  const [pristine, setPristine] = useState(true);
  const confirmClose = useConfirmCloseAction(closeDialog);
  const close = () => confirmClose(!pristine);

  const withPublisher = !config.get('terms.createWithoutPublisher');
  const publisherFromContextId = config.get('terms.publisherFromContext');

  const { isRestricted, contentPath } = useTerminologyRestriction(
    numberOfTerminologies
  );
  useRestrictionDialog({
    open: isRestricted,
    contentPath,
    callback: closeDialog,
  });

  useEffect(() => {
    if (withPublisher) {
      getPublishersByContext(publisherFromContextId).then((publishersInfo) => {
        if (publishersInfo.length === 0) {
          throw Error(
            `No publishers were found in context ${publisherFromContextId}`
          );
          // TODO show some appropriate error dialog
        }
        setPublishers(publishersInfo);
        // TODO mimics an event to follow state setters API. Fix properly
        // setPublisher({ target: { value: publishersInfo[0].value } }); // select first as default
      });
    }
  }, [publisherFromContextId, withPublisher]);

  const formIsValid = hasEntityTypeURIPattern
    ? isNameFree &&
      name !== '' &&
      title !== '' &&
      (withPublisher ? publisher : true)
    : title !== '';

  /**
   *
   * @param evt
   */
  const handleDescriptionChange = (evt) => {
    setDescription(evt.target.value);
  };

  /**
   *
   * @param evt
   */
  const handlePublisherChange = (evt) => {
    setPublisher(evt.target.value);
  };

  const handleCreate = () =>
    createTerminology({
      title,
      description,
      name,
      publisher,
      entityType,
      projectType: selectedProjectType,
      hasAdminRights: userInfo.hasAdminRights,
    })
      .then((conceptSchemeEntry) => updateContextTitle(conceptSchemeEntry))
      .then(() => {
        // todo really needed?
        userEntry.setRefreshNeeded();
        return userEntry.refresh();
      })
      .then(closeDialog)
      .then(() => dispatch({ type: CREATE }))
      .then(() => addSnackbar({ message: translate('termCreateSuccess') }))
      .catch((error) => {
        throw new ErrorWithMessage(translate('termCreateFail'), error);
      });

  const actions = (
    <LoadingButton
      onClick={() => runAsync(handleCreate())}
      loading={status === PENDING}
      autoFocus
      disabled={!formIsValid}
    >
      {translate('createTerminologyButton')}
    </LoadingButton>
  );

  if (isRestricted) return null;

  return (
    <>
      <ListActionDialog
        id="create-terminology"
        closeDialog={close}
        title={translate('createTerminology')}
        actions={actions}
        maxWidth="md"
      >
        <ContentWrapper>
          <form autoComplete="off">
            <ProjectTypeChooser
              onChangeProjectType={setSelectedProjectType}
              selectedId={selectedProjectType}
              userInfo={userInfo}
            />
            <TextField
              id="create-terminology-title"
              label={translate('createEntityTitle')}
              value={title}
              onChange={(evt) => {
                setPristine(false);
                handleTitleChange(evt);
              }}
              placeholder={translate('createEntityTitlePlaceholder')}
              error={!pristine && !title}
              helperText={!pristine && !title && translate('nameRequired')}
              required
            />

            {hasEntityTypeURIPattern && (
              <TextField
                label={translate('createEntityName')}
                id="create-terminology-name"
                value={name}
                onChange={(evt) => {
                  setPristine(false);
                  handleNameChange(evt);
                }}
                error={!pristine && (!name || !isNameFree)}
                helperText={
                  !pristine &&
                  (!name
                    ? translate('URLRequired')
                    : !isNameFree && translate('unavailableURLFeedback'))
                }
                required
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      {entityTypeUriPatternBase}
                    </InputAdornment>
                  ),
                }}
              />
            )}
            <TextField
              multiline
              rows={4}
              id="create-terminology-description"
              label={translate('createEntityDesc')}
              value={description}
              onChange={(evt) => {
                setPristine(false);
                handleDescriptionChange(evt);
              }}
              placeholder={translate('descriptionPlaceholder')}
            />
            {withPublisher && (
              <TextField
                select
                label={translate('createEntityPublisher')}
                value={publisher}
                onChange={handlePublisherChange}
              >
                {publishers.map(({ value, label }) => (
                  <MenuItem key={value} value={value}>
                    {label}
                  </MenuItem>
                ))}
              </TextField>
            )}
          </form>
        </ContentWrapper>
      </ListActionDialog>
      <ErrorCatcher error={createError} />
    </>
  );
};

CreateTerminologyDialog.propTypes = {
  numberOfTerminologies: PropTypes.number,
  closeDialog: PropTypes.func.isRequired,
};

export default CreateTerminologyDialog;
