import PropTypes from 'prop-types';
import { EntityType } from 'entitytype-lookup';

export const entityTypePropType = PropTypes.instanceOf(EntityType);
