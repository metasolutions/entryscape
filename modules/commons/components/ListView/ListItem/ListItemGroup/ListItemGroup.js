import PropTypes from 'prop-types';
import { Grid } from '@mui/material';

// Wrapper component for list items without actions such as tags
export const ListItemGroup = ({
  xl = false,
  xs = 1,
  justifyContent = 'flex-start',
  children,
  ...props
}) => {
  return (
    <Grid
      item
      container
      xs={xs}
      xl={xl}
      justifyContent={justifyContent}
      alignItems="center"
      {...props}
    >
      {children}
    </Grid>
  );
};

ListItemGroup.propTypes = {
  xl: PropTypes.number,
  xs: PropTypes.number,
  justifyContent: PropTypes.string,
  children: PropTypes.node,
};
