import escaPreparationsNLS from 'catalog/nls/escaPreparations.nls';
import { isArchived } from './util';

export const ARCHIVED_TAG = {
  id: 'archivedTag',
  labelNlsKey: 'archived',
  nlsBundles: escaPreparationsNLS,
  isVisible: (entry) => isArchived(entry),
};
