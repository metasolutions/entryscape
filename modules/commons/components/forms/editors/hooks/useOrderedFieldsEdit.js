import { REFRESH } from '../EditorContext';
import { useFieldsEdit } from './useFieldsEdit';
import { OrderedFieldGroup } from '../utils/OrderedFieldGroup';

/**
 *
 * @param {object} props
 * @returns {object}
 */
export const useOrderedFieldsEdit = (props) => {
  const { fieldGroup, ...fieldEditResult } = useFieldsEdit({
    ...props,
    FieldGroupClass: OrderedFieldGroup,
  });
  const { dispatch } = props;

  const moveField = (firstField, sourceIndex, targetIndex) => {
    fieldGroup.moveField(firstField, sourceIndex, targetIndex);
    if (dispatch) {
      dispatch({ type: REFRESH });
    }
  };

  return {
    ...fieldEditResult,
    fieldGroup,
    moveField,
  };
};
