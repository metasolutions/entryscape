import { Entry } from '@entryscape/entrystore-js';
import { namespaces } from '@entryscape/rdfjson';
import { getLabel } from 'commons/util/rdfUtils';
import { getLinkLabelSetter, measureText } from './diagramUtils';
import { LABEL_TYPE_URI } from './definitions';
import { getClassLabel } from './formDataUtils';
import {
  domainProperties,
  getRdfsLabel,
  getTypedEntries,
  hasLiteralRange,
  hasDatatypeRange,
} from './rdfsDataUtils';

/**
 *
 * @param {Entry} classEntry
 * @param {Entry[]} propertyEntries
 * @param {object} diagramUtils
 * @param {string} labelType
 * @param {object} namespaceStore
 * @returns {object}
 */
const createClassRectangle = (
  classEntry,
  propertyEntries,
  diagramUtils,
  labelType,
  namespaceStore
) => {
  const fieldLabels = [];
  const fieldDefs = [];
  domainProperties(propertyEntries, classEntry).forEach((classProperty) => {
    const ranges = classProperty
      .getAllMetadata()
      .find(classProperty.getResourceURI(), 'rdfs:range')
      .map((stmt) => stmt.getValue());
    if (hasLiteralRange(ranges) || hasDatatypeRange(ranges)) {
      fieldLabels.push(getLabel(classProperty));
      fieldDefs.push({ propertyEntry: classProperty });
      // TODO add to fieldDefs as well, must be in sync with formDiagram definitions though.
    }
  });

  return diagramUtils.createRectangleFromClass(
    classEntry,
    fieldDefs,
    getClassLabel(classEntry, labelType, namespaceStore),
    fieldLabels
  );
};

class ClassCellStore {
  constructor({
    context,
    diagramUtils,
    graph,
    includeImplicitClasses,
    labelType,
    namespaceStore,
    translate,
  }) {
    this._context = context;
    this._translate = translate;
    this._diagramUtils = diagramUtils;
    this._graph = graph;
    this._includeImplicitClasses = includeImplicitClasses;
    this._labelType = labelType;
    this._resourceURI2Rectangle = {};
    this._rectangleCells = [];
    this._linkCells = [];
    this._namespaceStore = namespaceStore;
  }

  _addRectangleCell(rectangle) {
    this._graph.addCell(rectangle);
    this._rectangleCells.push(rectangle);
  }

  _addLinkCell(link) {
    this._graph.addCell(link);
    this._linkCells.push(link);
  }

  async _addRectangle(classEntry, propertyEntries) {
    const classURI = classEntry.getResourceURI();
    const existingRect = this._resourceURI2Rectangle[classURI];
    if (existingRect) {
      existingRect.prop('classEntry', classEntry);
      existingRect.updateType();
      this._resourceURI2Rectangle[classURI] = existingRect;
    } else {
      const classRectangle = createClassRectangle(
        classEntry,
        propertyEntries,
        this._diagramUtils,
        this._labelType,
        this._namespaceStore
      );
      this._resourceURI2Rectangle[classEntry.getResourceURI()] = classRectangle;
      this._addRectangleCell(classRectangle);
    }
  }

  _addSubClassLinks(classEntry) {
    const subClassLinks = [];
    const superClasses = classEntry
      .getAllMetadata()
      .find(classEntry.getResourceURI(), 'rdfs:subClassOf')
      .map((statement) => statement.getValue());
    const sourceRectangle =
      this._resourceURI2Rectangle[classEntry.getResourceURI()];
    superClasses.forEach((superClass) => {
      let targetRectangle = this._resourceURI2Rectangle[superClass];
      if (!targetRectangle) {
        if (!this._includeImplicitClasses) return;
        targetRectangle = this._diagramUtils.createRectangleFromURI(
          superClass,
          [],
          this._namespaceStore.shortenKnown(superClass),
          []
        );
        this._resourceURI2Rectangle[superClass] = targetRectangle;
        this._addRectangleCell(targetRectangle);
      }

      const link = this._diagramUtils.createLinkForSubClassRelation(
        classEntry,
        superClass,
        sourceRectangle.id,
        targetRectangle.id
      );
      subClassLinks.push(link);
    });

    for (const link of subClassLinks) {
      this._addLinkCell(link);
    }
  }

  _addProperyLinks(propertyEntry) {
    const propertyLabel = getRdfsLabel(
      propertyEntry,
      this._labelType,
      this._namespaceStore
    );
    const propertyURI = propertyEntry.getResourceURI();
    const propertyMetadata = propertyEntry.getAllMetadata();
    const domains = propertyMetadata
      .find(propertyURI, 'rdfs:domain')
      .map((statement) => statement.getValue());
    if (domains.length === 0) {
      domains.push(namespaces.expand('rdfs:Resource'));
    }
    const ranges = propertyMetadata
      .find(propertyURI, 'rdfs:range')
      .map((statement) => statement.getValue());
    if (ranges.length === 0) {
      ranges.push(namespaces.expand('rdfs:Resource'));
    }

    if (domains.length > 0) {
      // Don't create links for properties that we draw like UML style attributes
      if (hasLiteralRange(ranges) || hasDatatypeRange(ranges)) {
        domains.forEach((domain) => {
          let sourceRectangle = this._resourceURI2Rectangle[domain];
          if (!sourceRectangle && this._includeImplicitClasses) {
            sourceRectangle = this._diagramUtils.createRectangleFromURI(
              domain,
              [{ propertyEntry }], // TODO fix a field definition as well
              this._namespaceStore.shortenKnown(domain),
              [propertyLabel]
            );
            this._resourceURI2Rectangle[domain] = sourceRectangle;
            this._addRectangleCell(sourceRectangle);
          } else if (sourceRectangle && sourceRectangle.implicitClass()) {
            // In this case the rectangle is an "implicit" one
            const fields = sourceRectangle.prop('fields');
            const fieldDefs = sourceRectangle.prop('fieldDefs');
            fields.push(
              getRdfsLabel(propertyEntry, this._labelType, this._namespaceStore)
            );
            fieldDefs.push({ propertyEntry });
            sourceRectangle.prop('fields', fields.slice(0));
            sourceRectangle.prop('fieldDefs', fieldDefs.slice(0));
            sourceRectangle.updateRectangle();
            sourceRectangle.resizeWidth(this._diagramUtils.measureText);
          }
        });
        return;
      }

      const property2Links = {};
      this._graph.getLinks().forEach((link) => {
        const property = link.prop('property');
        let arr = property2Links[property];
        if (!arr) {
          arr = [];
          property2Links[property] = arr;
        }
        arr.push(link);
      });

      domains.forEach((domain) => {
        let sourceRectangle = this._resourceURI2Rectangle[domain];
        if (!sourceRectangle) {
          if (!this._includeImplicitClasses) {
            return;
          }
          sourceRectangle = this._diagramUtils.createRectangleFromURI(
            domain,
            [],
            this._namespaceStore.shortenKnown(domain),
            []
          );
          this._resourceURI2Rectangle[domain] = sourceRectangle;
          this._addRectangleCell(sourceRectangle);
        }

        ranges.forEach((objectRange) => {
          let targetRectangle = this._resourceURI2Rectangle[objectRange];
          if (!targetRectangle) {
            if (!this._includeImplicitClasses) {
              return;
            }
            targetRectangle = this._diagramUtils.createRectangleFromURI(
              objectRange,
              [],
              this._namespaceStore.shortenKnown(objectRange),
              []
            );
            this._resourceURI2Rectangle[objectRange] = targetRectangle;
            this._addRectangleCell(targetRectangle);
          }
          const existingLink = (
            property2Links[propertyEntry.getResourceURI()] || []
          ).find(
            (link) =>
              link.prop('source/id') === sourceRectangle.id &&
              link.prop('target/id') === targetRectangle.id
          );
          if (existingLink) {
            existingLink.prop('propertyEntry', propertyEntry);
          } else {
            const link = this._diagramUtils.createLinkFromProperty(
              propertyEntry,
              propertyEntry.getResourceURI(),
              objectRange,
              propertyLabel,
              sourceRectangle.id,
              targetRectangle.id
            );
            link.prop('propertyEntry', propertyEntry);
            this._addLinkCell(link);
          }
        });
      });
    }
  }

  async labelByType(labelType) {
    this._rectangleCells.forEach((rectangle) => {
      if (labelType !== LABEL_TYPE_URI) {
        const classEntry = rectangle.get('classEntry');
        const label = classEntry
          ? getLabel(classEntry)
          : this._namespaceStore.shortenKnown(rectangle.get('class'));
        rectangle.set('label', label);
      } else {
        rectangle.set(
          'label',
          this._namespaceStore.shortenKnown(rectangle.get('class'))
        );
      }
      rectangle.updateRectangle();
      rectangle.resizeWidth(measureText);
    });

    const setLabel = getLinkLabelSetter(labelType);
    this._linkCells.forEach((linkCell) => {
      setLabel(linkCell, this._namespaceStore);
    });
  }

  async init() {
    this._graph.getElements().forEach((element) => {
      const cls = element.prop('class');
      if (cls) this._resourceURI2Rectangle[cls] = element;
    });

    const classEntries = await getTypedEntries(this._context, 'rdfs:Class');
    const propertyEntries = await getTypedEntries(
      this._context,
      'rdf:Property'
    );

    if (!classEntries.length && !propertyEntries.length) return null;

    for (const classEntry of classEntries) {
      await this._addRectangle(classEntry, propertyEntries);
    }

    for (const classEntry of classEntries) {
      this._addSubClassLinks(classEntry);
    }

    for (const propertyEntry of propertyEntries) {
      this._addProperyLinks(propertyEntry);
    }
  }
}

export default ClassCellStore;
