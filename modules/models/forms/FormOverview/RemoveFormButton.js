import { OverviewSidebarButton } from 'commons/components/overview';
import useRemoveFormDialog from 'models/forms/hooks/useRemoveFormDialog';
import esmoFormsNLS from 'models/nls/esmoForms.nls';
import escoDialogsNLS from 'commons/nls/escoDialogs.nls';
import { useTranslation } from 'commons/hooks/useTranslation';
import { useSnackbar } from 'commons/hooks/useSnackbar';
import { entryPropType } from 'commons/util/entry';
import { getIconFromActionId } from 'commons/actions';

const RemoveFormButton = ({ entry }) => {
  const translate = useTranslation([esmoFormsNLS, escoDialogsNLS]);
  const [addSnackbar] = useSnackbar();

  const { removeFormEntry } = useRemoveFormDialog(
    entry,
    () => addSnackbar({ message: translate('removeSuccessMessage') }),
    () => addSnackbar({ message: translate('removeFailMessage') })
  );

  return (
    <OverviewSidebarButton
      color="secondary"
      onClick={removeFormEntry}
      startIcon={getIconFromActionId('remove')}
    >
      {translate('remove')}
    </OverviewSidebarButton>
  );
};

RemoveFormButton.propTypes = {
  entry: entryPropType,
};

export default RemoveFormButton;
