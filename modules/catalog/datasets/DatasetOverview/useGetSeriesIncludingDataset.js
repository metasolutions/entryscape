import { useEffect, useState } from 'react';
import useAsync from 'commons/hooks/useAsync';
import { entrystoreUtil } from 'commons/store';
import config from 'config';

const useGetSeriesIncludingDataset = (datasetEntry) => {
  const [refreshCounter, setRefreshCounter] = useState(0);
  const refreshSeries = () => setRefreshCounter((prev) => prev + 1);
  const { data: datasetSeries, runAsync, status } = useAsync({ data: [] });
  const datasetSeriesEnabled = config.get('catalog.includeDatasetSeries');

  useEffect(() => {
    if (!datasetSeriesEnabled) return;

    const seriesResourceURIs = datasetEntry
      .getMetadata()
      .find(datasetEntry.getResourceURI(), 'dcat:inSeries')
      .map((statement) => statement.getValue());

    runAsync(
      entrystoreUtil.loadEntriesByResourceURIs(
        seriesResourceURIs,
        datasetEntry.getContext()
      )
    );
  }, [datasetEntry, runAsync, status, datasetSeriesEnabled, refreshCounter]);

  return [datasetSeries, refreshSeries];
};

export default useGetSeriesIncludingDataset;
