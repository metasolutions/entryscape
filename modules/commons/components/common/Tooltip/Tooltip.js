import PropTypes from 'prop-types';
import { forwardRef } from 'react';
import MuiTooltip from '@mui/material/Tooltip';
import { withStyles } from '@mui/styles';

const StyledTooltip = withStyles(() => ({
  tooltip: {
    borderRadius: '4px !important',
    padding: '2px 6px 3px 6px !important',
    minHeight: '23px',
    boxShadow: '0px 1px 3px #00000066',
    maxWidth: '168px',
  },
  tooltipPlacementTop: {
    margin: '3px',
  },
}))(MuiTooltip);

const Tooltip = forwardRef(
  ({ children, title, placement = 'top', ...rest }, ref) => {
    return title ? (
      <StyledTooltip placement={placement} title={title} ref={ref} {...rest}>
        {children}
      </StyledTooltip>
    ) : (
      children
    );
  }
);

export const TOOLTIP_DELAY = 500;

export default Tooltip;

Tooltip.propTypes = {
  children: PropTypes.node.isRequired,
  title: PropTypes.string,
  placement: PropTypes.string,
};
