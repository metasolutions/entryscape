import { validate, engine } from '@entryscape/rdforms';
import { useState, useCallback } from 'react';
import { useTranslation } from 'commons/hooks/useTranslation';
import escoRdformsNLS from 'commons/nls/escoRdforms.nls';

/**
 * @param {object[]} errors
 * @returns {undefined}
 */
const logErrors = (errors) => {
  for (const { code, item } of errors) {
    const id = item?.getId() || item.getSource()?.id;
    console.log(`Form error ${code} for ${id}`);
  }
};

/**
 *
 * @param {Error[]} errors
 * @returns {string}
 */
export const getFormErrorMessageAsNls = (errors) => {
  let hasFew = false;
  let hasMany = false;
  errors.forEach((err) => {
    switch (err.code) {
      case engine.CODES.TOO_FEW_VALUES:
        hasFew = true;
        break;
      case engine.CODES.TOO_MANY_VALUES:
      case engine.CODES.TOO_MANY_VALUES_DISJOINT:
        hasMany = true;
        break;
      default:
    }
  });

  if (hasFew && !hasMany) {
    return 'tooFewFields';
  }
  if (hasMany && !hasFew) {
    return 'tooManyFields';
  }
  return 'tooFewOrTooManyFields';
};

export const useFormErrorMessage = () => {
  const translate = useTranslation(escoRdformsNLS);

  const getErrorMessage = useCallback(
    (errors) => translate(getFormErrorMessageAsNls(errors)),
    [translate]
  );

  return getErrorMessage;
};

export const useRDFormsValidation = ({ onError, editor }) => {
  const [formErrorMessage, setFormErrorMessage] = useState(null);
  const [formErrors, setFormErrors] = useState([]);
  const getFormErrorMessage = useFormErrorMessage();

  const checkFormErrors = useCallback(
    (excludeCallback) => {
      const getErrors = () => {
        const { errors } = validate.bindingReport(editor.binding);
        if (!excludeCallback) return errors;
        return errors.filter(excludeCallback);
      };
      const errors = getErrors();
      const hasError = errors.length > 0;
      if (hasError) {
        setFormErrorMessage(getFormErrorMessage(errors));
        setFormErrors(errors);
        logErrors(errors);
        onError();
      }
      return hasError;
    },
    [editor, getFormErrorMessage, onError]
  );

  return {
    formErrors,
    formErrorMessage,
    setFormErrorMessage,
    checkFormErrors,
  };
};
