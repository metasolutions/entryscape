import { useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import useAsync from 'commons/hooks/useAsync';
import { getChartOptions } from './utils/chartOptions';
import './Chart.scss';

export const loadChartInstance = () => {
  return import(/* webpackChunkName: "chart-js" */ 'chart.js').then(
    (Chart) => Chart.default
  );
};

export const Chart = ({
  data,
  type = 'bar',
  options,
  height = '100%',
  width = '100%',
  label = '',
}) => {
  // const [chartOptions, setChartOptions] = useState(getChartOptions(options));
  const chartContainer = useRef();
  const { data: Chartjs, runAsync } = useAsync();
  const chart = useRef();

  useEffect(() => {
    runAsync(loadChartInstance());
  }, [options, type, runAsync]);

  useEffect(() => {
    if (!Chartjs || !data) return;

    const isOfSameType = chart.current?.config?.type === type;
    if (!isOfSameType) {
      // make sure old data is destroyed when switching chart type
      if (chart.current) chart.current.destroy();
      chart.current = new Chartjs(chartContainer.current, {
        data,
        type,
        options: getChartOptions(options),
      });
      return;
    }
    chart.current.data = data;
    chart.current.options = getChartOptions(options);
    chart.current.update();
  }, [Chartjs, options, data, type]);

  if (!data) {
    return null;
  }

  return (
    <div className="escoChart" style={{ height, width }}>
      <canvas
        aria-label={label}
        className={data ? '' : 'escoChart__hidden'}
        // eslint-disable-next-line jsx-a11y/no-interactive-element-to-noninteractive-role
        role="img"
        ref={chartContainer}
      />
    </div>
  );
};

Chart.propTypes = {
  data: PropTypes.shape({}),
  options: PropTypes.shape({}),
  type: PropTypes.string,
  height: PropTypes.string,
  width: PropTypes.string,
  label: PropTypes.string,
};

export default Chart;
