import PropTypes from 'prop-types';
import { Grid, Typography } from '@mui/material';
import { Add } from '@mui/icons-material';
import IconButton from 'commons/components/IconButton';

const DatasetList = ({ header }) => {
  return (
    <Grid container alignItems="center" spacing={2}>
      <Grid item>
        <Typography display="inline" variant="h2">
          {header}
        </Typography>
      </Grid>
      <Grid item>
        <IconButton icon={<Add />} />
      </Grid>
    </Grid>
  );
};

DatasetList.propTypes = {
  header: PropTypes.string,
};

export default DatasetList;
