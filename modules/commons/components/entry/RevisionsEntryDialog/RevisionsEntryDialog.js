import PropTypes from 'prop-types';
import { useEffect } from 'react';
import { Entry } from '@entryscape/entrystore-js';
import {
  useTranslation,
  nlsBundlesPropType,
} from 'commons/hooks/useTranslation';
import escoVersionsNLS from 'commons/nls/escoVersions.nls';
import useAsync from 'commons/hooks/useAsync';
import Revisions from 'commons/components/rdforms/Revisions';
import ContentWrapper from 'commons/components/common/ContentWrapper';
import ListActionDialog from 'commons/components/common/dialogs/ListActionDialog';
import { getTemplateId } from 'commons/util/entry';
import { useSnackbar } from 'commons/hooks/useSnackbar';

const RevisionsEntryDialog = ({
  entry,
  closeDialog,
  formTemplateId,
  excludeProperties = [],
  filterPredicates,
  onRevertCallback,
  nlsBundles = [],
}) => {
  const { data: templateId, runAsync } = useAsync(null);
  const [addSnackbar] = useSnackbar();

  useEffect(() => {
    if (!formTemplateId) {
      runAsync(getTemplateId(entry));
    }
  }, [entry, formTemplateId, runAsync]);

  const t = useTranslation(escoVersionsNLS);

  const onRevert = () => {
    Promise.resolve(onRevertCallback()).then(() => {
      addSnackbar({ message: t('revertSnackbarMessage') });
    });
  };

  return (
    <ListActionDialog
      id="revision-history-entry"
      closeDialog={closeDialog}
      title={t('versionsHeader')}
      closeDialogButtonLabel={t('versionsCloseButton')}
      maxWidth="md"
    >
      <ContentWrapper>
        <Revisions
          entry={entry}
          nlsBundles={[escoVersionsNLS, ...nlsBundles]}
          closeDialog={closeDialog}
          formTemplateId={templateId}
          filterPredicates={filterPredicates}
          excludeProperties={excludeProperties}
          onRevertCallback={onRevert}
        />
      </ContentWrapper>
    </ListActionDialog>
  );
};

RevisionsEntryDialog.propTypes = {
  entry: PropTypes.instanceOf(Entry),
  closeDialog: PropTypes.func,
  filterPredicates: PropTypes.shape({}),
  formTemplateId: PropTypes.string,
  excludeProperties: PropTypes.arrayOf(PropTypes.string),
  onRevertCallback: PropTypes.func,
  nlsBundles: nlsBundlesPropType,
};

export default RevisionsEntryDialog;
