import { useMemo, useState } from 'react';
import PropTypes from 'prop-types';
import { Entry } from '@entryscape/entrystore-js';
import {
  ListActions,
  List,
  ListItem,
  ListItemIcon,
  ListItemActionIconButton,
  ListItemText,
  ListHeader,
  ListHeaderItemText,
  ListHeaderItemSort,
  ListSearch,
  ListView,
  ListPagination,
  useListQuery,
  ListItemOpenMenuButton,
  ActionsMenu,
  useListModel,
  SORT,
  useFilterListActions,
  listPropsPropType,
} from 'commons/components/ListView';
import { RefreshButton } from 'commons/components/EntryListView';
import { ACTION_CREATE_ID, getIconFromActionId } from 'commons/actions';
import ControlTabs from 'commons/components/common/SegmentedControlTabs';
import BuiltinIcon from '@mui/icons-material/Settings';
import CustomIcon from '@mui/icons-material/Widgets';
import InfoIcon from '@mui/icons-material/Info';
import { useTranslation } from 'commons/hooks/useTranslation';
import escoListNLS from 'commons/nls/escoList.nls';
import esadEntityTypeNLS from 'admin/nls/esadEntityType.nls';
import esadTypeNLS from 'admin/nls/esadType.nls';
import {
  ENTITY_TYPE_BUILTIN,
  ENTITY_TYPE_CUSTOM,
} from 'admin/configs/utils/namespace';
import { getShortModifiedDate } from 'commons/util/metadata';
import { RESOLVED } from 'commons/hooks/useAsync';
import { getEntityTypesByFilter } from '../utils/entityTypeQuery';
import { rowActions, presenterAction, CreateEntryButton } from './actions';

const NLS_BUNDLES = [escoListNLS, esadEntityTypeNLS, esadTypeNLS];
const SEARCH_FIELDS = ['label'];

const EntityTypeEntryList = ({ entityTypeEntries, status, listProps }) => {
  const t = useTranslation(NLS_BUNDLES);
  const [{ search }, dispatch] = useListModel();
  const [selectedFilterTab, setSelectedFilterTab] =
    useState(ENTITY_TYPE_CUSTOM);

  const filteredEntityTypes = useMemo(() => {
    return status === RESOLVED
      ? getEntityTypesByFilter(entityTypeEntries, selectedFilterTab)
      : null;
  }, [entityTypeEntries, selectedFilterTab, status]);

  const { result: entityTypes, size } = useListQuery({
    items: filteredEntityTypes,
    searchFields: SEARCH_FIELDS,
  });

  const filterTabs = [
    { label: t('customFilterLabel'), value: ENTITY_TYPE_CUSTOM },
    { label: t('builtinFilterLabel'), value: ENTITY_TYPE_BUILTIN },
  ];

  const handleSelectTab = (selectedTab) => {
    setSelectedFilterTab(selectedTab);
    if (selectedTab === ENTITY_TYPE_BUILTIN) {
      dispatch({
        type: SORT,
        value: { field: 'label', order: 'desc' },
      });
      return;
    }
    dispatch({
      type: SORT,
      value: { field: 'modified', order: 'desc' },
    });
  };

  const defaultActions = [
    {
      id: ACTION_CREATE_ID,
      element: (
        <CreateEntryButton
          status={status}
          entityTypeEntries={entityTypeEntries}
          key={ACTION_CREATE_ID}
        />
      ),
    },
  ];
  const listActions = useFilterListActions({
    listActions: defaultActions,
    listActionsProps: {},
    excludeActions: listProps.excludeActions,
    nlsBundles: NLS_BUNDLES,
  });

  return (
    <ListView
      status={status}
      search={search}
      size={size}
      nlsBundles={NLS_BUNDLES}
    >
      <ListActions>
        <ListSearch />
        <RefreshButton />
        {listActions.map(({ element }) => element)}
      </ListActions>
      {status !== 'pending' ? (
        <>
          <ControlTabs
            items={filterTabs}
            selected={selectedFilterTab}
            onSelect={handleSelectTab}
          />
          <ListHeader>
            <ListHeaderItemText xs={1} text="" />
            {selectedFilterTab === ENTITY_TYPE_BUILTIN ? (
              <ListHeaderItemSort
                xs={7}
                sortBy="label"
                text={t('labelListHeader')}
              />
            ) : (
              <ListHeaderItemText xs={7} />
            )}
            {selectedFilterTab === ENTITY_TYPE_CUSTOM ? (
              <ListHeaderItemSort
                sortBy="modified"
                text={t('modifiedListHeader')}
              />
            ) : null}
          </ListHeader>
        </>
      ) : null}
      <List>
        {entityTypes.map((entityType) => {
          const { id, label, type, entry } = entityType;
          const isBuiltin = type === ENTITY_TYPE_BUILTIN;
          const { typeConfigToGraph, ...infoProps } = presenterAction;
          const typeInfoAction = {
            ...infoProps,
            entry: entityType.entry,
            title: t(infoProps.nlsKeyLabel),
            nlsBundles: NLS_BUNDLES,
            ...typeConfigToGraph({ entry, config: entityType.config }),
          };
          const actionsMenuItems = rowActions
            .filter(({ isVisible }) => isVisible(entityType))
            .map(({ nlsKeyLabel, ...action }) => ({
              label: t(nlsKeyLabel),
              icon: getIconFromActionId(action.id),
              action: { ...action, entry, entityTypeEntries },
            }));
          return (
            <ListItem key={id}>
              <ListItemIcon
                Icon={isBuiltin ? BuiltinIcon : CustomIcon}
                tooltip={isBuiltin ? t('builtinLabel') : t('customLabel')}
              />
              <ListItemText xs={isBuiltin ? 9 : 7} primary={label} />
              {!isBuiltin ? (
                <ListItemText secondary={getShortModifiedDate(entry)} xs={2} />
              ) : null}
              <ListItemActionIconButton
                title={t(presenterAction.nlsKeyLabel)}
                action={typeInfoAction}
                xs={1}
              >
                <InfoIcon />
              </ListItemActionIconButton>
              <ListItemOpenMenuButton />
              <ActionsMenu items={actionsMenuItems} />
            </ListItem>
          );
        })}
      </List>
      <ListPagination />
    </ListView>
  );
};

EntityTypeEntryList.propTypes = {
  entityTypeEntries: PropTypes.arrayOf(PropTypes.instanceOf(Entry)),
  status: PropTypes.string,
  listProps: listPropsPropType,
};

export default EntityTypeEntryList;
