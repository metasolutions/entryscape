import CatalogsView from 'catalog/catalogs/CatalogsView';
import DatasetsView from 'catalog/datasets/DatasetsView';
import ShowcasesView from 'catalog/showcases/ShowcasesView';
import IdeasView from 'catalog/ideas/IdeasView';
import ContactsView from 'catalog/contacts/ContactsView';
import DatasetOverview from 'catalog/datasets/DatasetOverview';
import PublisherOverview from 'catalog/publishers/PublisherOverview';
import SuggestionOverview from 'catalog/suggestions/SuggestionOverview';
import DataServiceOverview from 'catalog/dataServices/DataServiceOverview';
import ShowcaseOverview from 'catalog/showcases/ShowcaseOverview';
import ContactOverview from 'catalog/contacts/ContactOverview';
import IdeaOverview from 'catalog/ideas/IdeaOverview';
import DocumentOverview from 'catalog/documents/DocumentOverview';
import DatasetPreview from 'catalog/datasets/preview';
import DataServicesView from 'catalog/dataServices/DataServicesView';
import Overview from 'catalog/catalogs/CatalogOverview';
import PublishersView from 'catalog/publishers/PublishersView';
import SuggestionsView from 'catalog/suggestions/SuggestionsView';
import DocumentsView from 'catalog/documents/DocumentsView';
import StatisticsView from 'catalog/statistics';
import { getSecondaryNavViewsFromEntityTypes } from 'commons/util/site';
import {
  SUGGESTIONS_LISTVIEW,
  DATASETS_LISTVIEW,
  DATASERVICES_LISTVIEW,
  SHOWCASES_LISTVIEW,
  IDEAS_LISTVIEW,
  PUBLISHERS_LISTVIEW,
  CONTACTS_LISTVIEW,
  DOCUMENTS_LISTVIEW,
  CATALOG_OVERVIEW_NAME,
  SUGGESTION_OVERVIEW_NAME,
  DATASET_OVERVIEW_NAME,
  DATA_SERVICE_OVERVIEW_NAME,
  SHOWCASE_OVERVIEW_NAME,
  IDEA_OVERVIEW_NAME,
  DOCUMENT_OVERVIEW_NAME,
  PUBLISHER_OVERVIEW_NAME,
  CONTACT_OVERVIEW_NAME,
  CATALOGS_LISTVIEW,
} from './config';
import {
  CATALOGS_LISTVIEW_HELP,
  CATALOGS_OVERVIEW_HELP,
  SUGGESTIONS_LISTVIEW_HELP,
  SUGGESTIONS_OVERVIEW_HELP,
  DATASETS_LISTVIEW_HELP,
  DATASETS_OVERVIEW_HELP,
  DATA_SERVICES_LISTVIEW_HELP,
  DATA_SERVICES_OVERVIEW_HELP,
  SHOWCASES_LISTVIEW_HELP,
  SHOWCASES_OVERVIEW_HELP,
  IDEAS_LISTVIEW_HELP,
  IDEAS_OVERVIEW_HELP,
  PUBLISHERS_LISTVIEW_HELP,
  PUBLISHERS_OVERVIEW_HELP,
  CONTACTS_LISTVIEW_HELP,
  CONTACTS_OVERVIEW_HELP,
  DOCUMENTS_LISTVIEW_HELP,
  DOCUMENTS_OVERVIEW_HELP,
  STATISTICS_VIEW_HELP,
} from './helpLinks';

export default {
  modules: [
    {
      name: 'catalog',
      productName: { en: 'Catalog', sv: 'Catalog', de: 'Catalog' },
      faClass: 'archive',
      startView: 'catalog__list', // compulsory
      sidebar: true,
      getLayoutProps: getSecondaryNavViewsFromEntityTypes,
    },
  ],
  views: [
    {
      name: CATALOGS_LISTVIEW,
      class: CatalogsView,
      title: {
        en: 'Catalogs',
        sv: 'Kataloger',
        da: 'Kataloger',
        de: 'Kataloge',
      },
      constructorParams: {
        rowClickView: CATALOG_OVERVIEW_NAME,
      },
      faClass: 'archive',
      route: '/catalog',
      module: 'catalog',
      helpLinks: CATALOGS_LISTVIEW_HELP,
    },
    {
      name: CATALOG_OVERVIEW_NAME,
      title: { en: 'Overview', sv: 'Översikt', de: 'Überblick' },
      class: Overview,
      faClass: 'eye',
      route: '/catalog/:contextId/overview',
      parent: 'catalog__list',
      module: 'catalog',
      helpLinks: CATALOGS_OVERVIEW_HELP,
    },
    {
      name: SUGGESTIONS_LISTVIEW,
      title: {
        en: 'Sug\u00ADges\u00ADtions',
        sv: 'För\u00ADslag',
        de: 'Vor\u00ADschlä\u00ADge',
      },
      class: SuggestionsView,
      faClass: 'tasks',
      route: '/catalog/:contextId/suggestions',
      parent: 'catalog__list',
      module: 'catalog',
      helpLinks: SUGGESTIONS_LISTVIEW_HELP,
    },
    {
      name: SUGGESTION_OVERVIEW_NAME,
      class: SuggestionOverview,
      title: {
        en: 'Sug\u00ADges\u00ADtion',
        //  TODO: translate singular
        sv: 'För\u00ADslag',
        de: 'Vor\u00ADschlä\u00ADge',
      },
      route: '/catalog/:contextId/suggestions/:entryId/overview',
      parent: 'catalog__suggestions',
      module: 'catalog',
      helpLinks: SUGGESTIONS_OVERVIEW_HELP,
    },
    {
      name: DATASETS_LISTVIEW,
      class: DatasetsView,
      faClass: 'cubes',
      title: {
        en: 'Datasets',
        sv: 'Data\u00ADmängder',
        da: 'Datasæt',
        de: 'Daten\u00ADsätze',
      },
      constructorParams: { createAndRemoveDistributions: true },
      route: '/catalog/:contextId/datasets',
      parent: 'catalog__list',
      module: 'catalog',
      helpLinks: DATASETS_LISTVIEW_HELP,
    },
    {
      name: DATASET_OVERVIEW_NAME,
      class: DatasetOverview,
      faClass: 'cubes',
      title: {
        en: 'Dataset',
        sv: 'Data\u00ADmängd',
        da: 'Datasæt',
        de: 'Daten\u00ADsatz',
      },
      constructorParams: { createAndRemoveDistributions: true },
      route: '/catalog/:contextId/datasets/:entryId/overview',
      parent: 'catalog__datasets',
      module: 'catalog',
      helpLinks: DATASETS_OVERVIEW_HELP,
    },
    {
      name: 'catalog__datasets__preview',
      class: DatasetPreview,
      title: {
        en: 'Dataset',
        sv: 'Data\u00ADmängd',
        da: 'Datasæt',
        de: 'Daten\u00ADsatz',
      },
      route: '/catalog/:contextId/datasets/:entryId',
      parent: 'catalog__datasets__dataset',
      module: 'catalog',
      public: true,
    },
    {
      name: DATASERVICES_LISTVIEW,
      title: { en: 'Data services', sv: 'Datatjänster', de: 'Datendienste' },
      class: DataServicesView,
      faClass: 'exchange-alt',
      route: '/catalog/:contextId/dataservices',
      parent: 'catalog__list',
      module: 'catalog',
      helpLinks: DATA_SERVICES_LISTVIEW_HELP,
    },
    {
      name: DATA_SERVICE_OVERVIEW_NAME,
      class: DataServiceOverview,
      title: {
        en: 'Data service',
        //  TODO: translate singular
        sv: 'Datatjänster',
        de: 'Datendienste',
      },
      route: '/catalog/:contextId/dataservices/:entryId/overview',
      parent: 'catalog__dataservices',
      module: 'catalog',
      helpLinks: DATA_SERVICES_OVERVIEW_HELP,
    },
    {
      name: SHOWCASES_LISTVIEW,
      title: {
        en: 'Showcases',
        sv: 'Showcases',
        da: 'Showcases',
        de: 'Showcases',
      },
      class: ShowcasesView,
      faClass: 'gem',
      route: '/catalog/:contextId/showcases',
      parent: 'catalog__list',
      module: 'catalog',
      helpLinks: SHOWCASES_LISTVIEW_HELP,
    },
    {
      name: SHOWCASE_OVERVIEW_NAME,
      class: ShowcaseOverview,
      title: {
        en: 'Showcase',
        sv: 'Showcase',
        de: 'Showcase',
      },
      route: '/catalog/:contextId/showcases/:entryId/overview',
      parent: 'catalog__showcases',
      module: 'catalog',
      helpLinks: SHOWCASES_OVERVIEW_HELP,
    },
    {
      name: IDEAS_LISTVIEW,
      title: { en: 'Ideas', sv: 'Idéer', da: 'Ideer', de: 'Ideen' },
      class: IdeasView,
      faClass: 'lightbulb',
      route: '/catalog/:contextId/ideas',
      parent: 'catalog__list',
      module: 'catalog',
      helpLinks: IDEAS_LISTVIEW_HELP,
    },
    {
      name: IDEA_OVERVIEW_NAME,
      class: IdeaOverview,
      title: {
        en: 'Ideas',
        //  TODO: translate singular
        sv: 'Idéer',
        da: 'Ideer',
        de: 'Ideen',
      },
      route: '/catalog/:contextId/ideas/:entryId/overview',
      parent: 'catalog__ideas',
      module: 'catalog',
      helpLinks: IDEAS_OVERVIEW_HELP,
    },
    {
      name: PUBLISHERS_LISTVIEW,
      class: PublishersView,
      faClass: 'users',
      title: {
        en: 'Publishers',
        sv: 'Organisa\u00ADtioner',
        da: 'Organisa\u00ADtioner',
        de: 'Heraus\u00ADgeber',
      },
      constructorParams: { publishers: true, contacts: false },
      route: '/catalog/:contextId/publishers',
      parent: 'catalog__list',
      module: 'catalog',
      helpLinks: PUBLISHERS_LISTVIEW_HELP,
    },
    {
      name: PUBLISHER_OVERVIEW_NAME,
      class: PublisherOverview,
      title: {
        en: 'Publisher',
        sv: 'Organisa\u00ADtioner',
        da: 'Organisa\u00ADtioner',
        de: 'Heraus\u00ADgeber',
      },
      route: '/catalog/:contextId/publishers/:entryId/overview',
      parent: 'catalog__publishers',
      module: 'catalog',
      helpLinks: PUBLISHERS_OVERVIEW_HELP,
    },
    {
      name: CONTACTS_LISTVIEW,
      class: ContactsView,
      faClass: 'phone',
      title: {
        en: 'Contacts',
        sv: 'Kontakter',
        da: 'Kontakter',
        de: 'Kontakte',
      },
      constructorParams: { publishers: false, contacts: true },
      route: '/catalog/:contextId/contacts',
      parent: 'catalog__list',
      module: 'catalog',
      helpLinks: CONTACTS_LISTVIEW_HELP,
    },
    {
      name: CONTACT_OVERVIEW_NAME,
      class: ContactOverview,
      title: {
        en: 'Contact',
        //  TODO: translate singular
        sv: 'Kontakter',
        da: 'Kontakter',
        de: 'Kontakte',
      },
      route: '/catalog/:contextId/contacts/:entryId/overview',
      parent: 'catalog__contacts',
      module: 'catalog',
      helpLinks: CONTACTS_OVERVIEW_HELP,
    },
    {
      name: DOCUMENTS_LISTVIEW,
      class: DocumentsView,
      faClass: 'file',
      title: { en: 'Documents', sv: 'Dokument', de: 'Dokumente' },
      route: '/catalog/:contextId/documents',
      parent: 'catalog__list',
      module: 'catalog',
      helpLinks: DOCUMENTS_LISTVIEW_HELP,
    },
    {
      name: DOCUMENT_OVERVIEW_NAME,
      class: DocumentOverview,
      title: {
        en: 'Document',
        //  TODO: translate singular
        sv: 'Dokument',
        de: 'Dokumente',
      },
      route: '/catalog/:contextId/documents/:entryId/overview',
      parent: 'catalog__documents',
      module: 'catalog',
      helpLinks: DOCUMENTS_OVERVIEW_HELP,
    },
    {
      name: 'catalog__statistics',
      title: { en: 'Statistics', sv: 'Statistik', de: 'Statistik' },
      class: StatisticsView, // StatisticsView,
      faClass: 'chart-bar',
      route: '/catalog/:contextId/statistics',
      parent: 'catalog__list',
      module: 'catalog',
      helpLinks: STATISTICS_VIEW_HELP,
    },
  ],
};
