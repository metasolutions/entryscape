import { Graph, namespaces as ns } from '@entryscape/rdfjson';
import { localizeLabel } from 'commons/locale';
import { URI } from 'commons/components/forms/utils/nodetypes';

/**
 *  Expands choices if nodetype is URI
 *
 * @param {object[]} choices
 * @param {string} nodetype
 * @returns {object[]}
 */
export const expandChoices = (choices, nodetype) => {
  if (nodetype !== URI) return choices;
  return choices.map(({ value, ...rest }) => ({
    value: ns.expand(value),
    ...rest,
  }));
};

/**
 * Localizes labels of choices
 *
 * @param {object[]} choices
 * @param {Function} translate
 * @returns {object[]}
 */
export const localizeChoices = (choices, translate) => {
  return choices.map((choice) => {
    const { label, labelNlsKey } = choice;
    return { ...choice, label: localizeLabel(label) ?? translate(labelNlsKey) };
  });
};

/**
 *
 * @param {object[]} choices
 * @param {string} nodetype
 * @param {Function} translate
 * @returns {object[]}
 */
export const normalizeChoices = (choices, nodetype, translate) => {
  const expandedChoices = expandChoices(choices, nodetype);
  return localizeChoices(expandedChoices, translate);
};

/**
 * Get available nodetypes
 *
 * @param {Graph} graph
 * @param {string} resourceURI
 * @param {object[]} choices
 * @returns {object[]}
 */
export const getAvailableChoices = (graph, resourceURI, choices) => {
  return choices.filter(({ constrainedTo }) => {
    if (!constrainedTo) return true;
    // TODO: use isType when 3.14 is merged to develop
    return constrainedTo.find((rdfType) => {
      return graph.find(resourceURI, 'rdf:type', rdfType).length;
    });
  });
};
