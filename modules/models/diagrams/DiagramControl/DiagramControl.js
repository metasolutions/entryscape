import React, { useState } from 'react';
import PropTypes from 'prop-types';
import Typography from '@mui/material/Typography';
import IconButton from '@mui/material/IconButton';
import CenterFocusStrongIcon from '@mui/icons-material/CenterFocusStrong';
import ZoomInIcon from '@mui/icons-material/ZoomIn';
import ZoomOutIcon from '@mui/icons-material/ZoomOut';
import EditIcon from '@mui/icons-material/Edit';
import OpenInFullIcon from '@mui/icons-material/OpenInFull';
import EditEntryDialog from 'commons/components/entry/EditEntryDialog';
import CreateDiagramDialog from 'models/diagrams/dialogs/CreateDiagramDialog';
import { useTranslation } from 'commons/hooks/useTranslation';
import esmoDiagramsNLS from 'models/nls/esmoDiagrams.nls';
import { withListModelProvider } from 'commons/components/ListView/hooks/useListModel';
import { getDiagramLabel } from 'models/diagrams/utils/diagram';
import { DEFAULT_DIAGRAM_ID } from 'models/diagrams/utils/definitions';
import Tooltip from 'commons/components/common/Tooltip';
import './DiagramControl.scss';
import {
  SET_DIAGRAM_ENTRY,
  useDiagramContext,
} from '../hooks/useDiagramContext';
import DiagramSwitch from './DiagramSwitch';

const DiagramControl = ({ diagramUtils, paper, edit, onMaximize, onEdit }) => {
  const translate = useTranslation([esmoDiagramsNLS]);
  const [editDialogOpen, setEditDialogOpen] = useState(false);
  const { diagramEntry, diagramEntryId, isPrototype, dispatch } =
    useDiagramContext();
  const zoomIn = () => diagramUtils.zoomIn(paper);
  const zoomOut = () => diagramUtils.zoomOut(paper);
  const fitToContent = () => diagramUtils.fitContent(paper);

  return (
    <div
      className={`esmoDiagramControl${
        onMaximize ? '' : ' esmoDiagramControl--maximized'
      }`}
    >
      <div className="esmoDiagramControl__controlGroup">
        <Tooltip title={translate('zoomIn')}>
          <IconButton aria-label={translate('zoomIn')} onClick={zoomIn}>
            <ZoomInIcon />
          </IconButton>
        </Tooltip>
        <Tooltip title={translate('zoomOut')}>
          <IconButton aria-label={translate('zoomOut')} onClick={zoomOut}>
            <ZoomOutIcon />
          </IconButton>
        </Tooltip>
        <Tooltip title={translate('fitDiagram')}>
          <IconButton
            aria-label={translate('fitDiagram')}
            onClick={fitToContent}
          >
            <CenterFocusStrongIcon />
          </IconButton>
        </Tooltip>
        {onEdit ? (
          <Tooltip title={translate('editDiagram')}>
            <IconButton aria-label={translate('editDiagram')} onClick={onEdit}>
              <EditIcon />
            </IconButton>
          </Tooltip>
        ) : null}
        {onMaximize ? (
          <Tooltip title={translate('maximizeDiagram')}>
            <IconButton
              aria-label={translate('maximizeDiagram')}
              onClick={onMaximize}
            >
              <OpenInFullIcon />
            </IconButton>
          </Tooltip>
        ) : null}
      </div>

      {edit ? (
        <>
          <Typography
            id="diagram-control-heading"
            className="esmoDiagramControl__heading"
            variant="h1"
          >
            {getDiagramLabel(
              diagramEntry,
              translate,
              diagramEntryId === DEFAULT_DIAGRAM_ID
            )}
          </Typography>
          <IconButton
            className="esmoDiagramControl__editMetadataButton"
            aria-label={translate('editDiagramMetadata')}
            onClick={() => setEditDialogOpen(true)}
          >
            <EditIcon />
          </IconButton>
          {editDialogOpen && !isPrototype ? (
            <EditEntryDialog
              entry={diagramEntry}
              closeDialog={() => setEditDialogOpen(false)}
            />
          ) : null}
          {editDialogOpen && isPrototype ? (
            <CreateDiagramDialog
              entry={diagramEntry}
              entryId={DEFAULT_DIAGRAM_ID}
              onEntryEdit={(newDiagramEntry) =>
                dispatch({
                  type: SET_DIAGRAM_ENTRY,
                  diagramEntry: newDiagramEntry,
                  isProtoType: false,
                })
              }
              closeDialog={() => setEditDialogOpen(false)}
            />
          ) : null}
        </>
      ) : (
        <DiagramSwitch onEdit={onEdit} />
      )}
    </div>
  );
};

DiagramControl.propTypes = {
  diagramUtils: PropTypes.shape({
    fitContent: PropTypes.func,
    zoomIn: PropTypes.func,
    zoomOut: PropTypes.func,
  }),
  paper: PropTypes.shape({}),
  edit: PropTypes.bool,
  onMaximize: PropTypes.func,
  onEdit: PropTypes.func,
};

export default withListModelProvider(DiagramControl);
