import PropTypes from 'prop-types';
import { useCallback } from 'react';
import { entrystore } from 'commons/store';
import ListActionDialog from 'commons/components/common/dialogs/ListActionDialog';
import ContentWrapper from 'commons/components/common/ContentWrapper';
import {
  EntryListView,
  useSolrQuery,
  TITLE_COLUMN,
  MODIFIED_COLUMN,
  ACTION_MENU_COLUMN,
  INFO_COLUMN,
} from 'commons/components/EntryListView';
import { LIST_ACTION_INFO } from 'commons/components/EntryListView/actions';
import LinkedDataBrowserDialog from 'commons/components/LinkedDataBrowserDialog';
import RevisionsEntryDialog from 'commons/components/entry/RevisionsEntryDialog';
import { ACTION_REVISIONS } from 'commons/actions';
import { withListModelProvider } from 'commons/components/ListView';
import { useESContext } from 'commons/hooks/useESContext';
import {
  useTranslation,
  nlsBundlesPropType,
} from 'commons/hooks/useTranslation';
import escoListNLS from 'commons/nls/escoList.nls';
import { getLabel } from 'commons/util/rdfUtils';
import { entryPropType } from 'commons/util/entry';

const DatasetAssociatedEntriesDialog = ({
  datasetEntry,
  associatedEntryType,
  associatedEntryNLSBundles,
  dialogHeaderNLSKey,
  closeDialog,
  listPlaceholder = '',
}) => {
  const nlsBundles = [...associatedEntryNLSBundles, escoListNLS];
  const translate = useTranslation(nlsBundles);

  const rowActions = [{ ...ACTION_REVISIONS, Dialog: RevisionsEntryDialog }];

  const { context } = useESContext();
  const createQuery = useCallback(
    () =>
      entrystore
        .newSolrQuery()
        .rdfType(associatedEntryType)
        .uriProperty('dcterms:source', datasetEntry.getResourceURI())
        .context(context),
    [associatedEntryType, context, datasetEntry]
  );
  const queryResults = useSolrQuery({ createQuery });

  return (
    <ListActionDialog
      id="associated-entries-dialog"
      closeDialog={closeDialog}
      title={translate(dialogHeaderNLSKey, getLabel(datasetEntry))}
      closeDialogButtonLabel={translate('close')}
      maxWidth="md"
      fixedHeight
    >
      <ContentWrapper>
        <EntryListView
          {...queryResults}
          nlsBundles={nlsBundles}
          viewPlaceholderProps={{ label: listPlaceholder }}
          getListItemProps={({ entry }) => ({
            action: {
              id: 'info',
              Dialog: LinkedDataBrowserDialog,
              labelNlsKey: 'infoEntry',
              entry,
            },
          })}
          columns={[
            { ...TITLE_COLUMN, xs: 7 },
            { ...MODIFIED_COLUMN, xs: 3 },
            {
              ...INFO_COLUMN,
              getProps: ({ entry, translate: t }) => ({
                action: {
                  ...LIST_ACTION_INFO,
                  entry,
                  nlsBundles,
                  Dialog: LinkedDataBrowserDialog,
                },
                title: t('infoEntry'),
              }),
            },
            {
              ...ACTION_MENU_COLUMN,
              actions: rowActions,
            },
          ]}
        />
      </ContentWrapper>
    </ListActionDialog>
  );
};

DatasetAssociatedEntriesDialog.propTypes = {
  datasetEntry: entryPropType,
  associatedEntryType: PropTypes.string,
  associatedEntryNLSBundles: nlsBundlesPropType,
  AssociatedInfoDialog: PropTypes.func,
  dialogHeaderNLSKey: PropTypes.string,
  closeDialog: PropTypes.func.isRequired,
  listPlaceholder: PropTypes.string,
};

export default withListModelProvider(DatasetAssociatedEntriesDialog);
