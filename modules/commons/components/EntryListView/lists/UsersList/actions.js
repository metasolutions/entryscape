import escoListNLS from 'commons/nls/escoList.nls';
import escoSharingSetttingsNls from 'commons/nls/escoSharingSettings.nls';
import LinkedDataBrowserDialog from 'commons/components/LinkedDataBrowserDialog';
import RemoveUserDialog from './dialogs/RemoveUserDialog';
import AddUserToGroup from './dialogs/AddUserToGroupDialog';

export const nlsBundles = [escoListNLS, escoSharingSetttingsNls];

export const listActions = [
  {
    id: 'add-user',
    Dialog: AddUserToGroup,
    labelNlsKey: 'addUserToGroupNLS',
  },
];

export const rowActions = [
  {
    id: 'remove',
    Dialog: RemoveUserDialog,
    isVisible: ({ groupEntry }) => groupEntry.canWriteResource(),
    labelNlsKey: 'removeEntryLabel',
  },
];

export const presenterAction = {
  id: 'info',
  Dialog: LinkedDataBrowserDialog,
  labelNlsKey: 'infoEntry',
  formTemplateId: 'esc:User',
};
