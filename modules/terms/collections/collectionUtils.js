import { spreadEntry } from 'commons/util/store';

/**
 * Removes concept from collection
 *
 * @param {Entry} collectionEntry
 * @param {Entry} conceptEntry
 * @returns {Promise}
 */
export const removeConcept = (collectionEntry, conceptEntry) => {
  const conceptURI = conceptEntry.getResourceURI();
  const collectionURI = collectionEntry.getResourceURI();

  collectionEntry
    .getMetadata()
    .findAndRemove(collectionURI, 'skos:member', conceptURI);

  return collectionEntry.commitMetadata();
};

/**
 * Replaces the concepts of a collection
 *
 * @param {Entry} collectionEntry
 * @param {string[]} conceptRURIs - Resource URIs of collection concepts
 * @returns {Promise}
 */
export const replaceConcepts = (collectionEntry, conceptRURIs) => {
  const { metadata: collectionMetadata, ruri: collectionRURI } =
    spreadEntry(collectionEntry);

  collectionMetadata.findAndRemove(collectionRURI, 'skos:member');
  for (const conceptRURI of conceptRURIs) {
    collectionMetadata.add(collectionRURI, 'skos:member', conceptRURI);
  }

  return collectionEntry.commitMetadata();
};
