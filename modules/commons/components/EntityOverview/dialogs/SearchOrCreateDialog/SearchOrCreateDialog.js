import PropTypes from 'prop-types';
import { useCallback, useState } from 'react';
import { Button } from '@mui/material';
import { Entry } from '@entryscape/entrystore-js';
import { entityTypePropType } from 'commons/types/EntityType';
import {
  withListModelProvider,
  useListModel,
  ListItemButtonAction,
  REFRESH,
} from 'commons/components/ListView';
import {
  EntryListView,
  useSolrQuery,
  MODIFIED_COLUMN,
  TITLE_COLUMN,
  INFO_COLUMN,
  MultiCreateButton,
} from 'commons/components/EntryListView';
import {
  nlsBundlesPropType,
  nlsPropType,
  useTranslation,
} from 'commons/hooks/useTranslation';
// eslint-disable-next-line max-len
import ListContentViewerDialog from 'commons/components/EntryListView/dialogs/ListContentViewerDialog';
import { LIST_ACTION_INFO } from 'commons/components/EntryListView/actions';
import { localize } from 'commons/locale';
import { entrystore } from 'commons/store';
import { useESContext } from 'commons/hooks/useESContext';
import { useEntry } from 'commons/hooks/useEntry';
import ListActionDialog from 'commons/components/common/dialogs/ListActionDialog';
import ContentWrapper from 'commons/components/common/ContentWrapper';
import { addRelationToParent } from 'commons/types/utils/relations';
import { applyEntityTypeParams } from 'commons/util/solr';

import { nlsBundles as standardNlsBundles, listAction } from './actions';
import CreateEntityDialog from '../CreateEntityDialog';

const SearchOrCreateDialog = ({
  actionParams: {
    entityType,
    afterCreate,
    relation,
    refreshSubEntities,
    CreateDialog = CreateEntityDialog,
    createActions = [],
    entry: actionEntry,
    createDialogParams = {},
    getQuery,
    connectedButtonLabel = 'connectedButtonLabel',
    connectButtonLabel = 'connectButtonLabel',
    disableCreateActions = false,
  },
  closeDialog,
  nlsBundles: optionalNlsBundles = [],
}) => {
  const nlsBundles = [...optionalNlsBundles, ...standardNlsBundles];
  const translate = useTranslation(nlsBundles);
  const { context, contextName } = useESContext();
  const parentEntry = useEntry();
  const [, dispatch] = useListModel();
  const [hasConnected, setHasConnected] = useState(false);
  const rdfType = entityType.get('rdfType');
  const label = relation.label || entityType.get('label');

  const createQuery = useCallback(
    () =>
      getQuery?.(context) ||
      entrystore.newSolrQuery().rdfType(rdfType).context(context),
    [context, rdfType, getQuery]
  );

  const applyQueryParams = useCallback(
    (query, queryOptions) => {
      return applyEntityTypeParams(query, queryOptions, entityType.get());
    },
    [entityType]
  );

  const queryResults = useSolrQuery({
    createQuery,
    applyQueryParams,
  });

  const getConnectButtonProps = ({ entry, translate: t }) => {
    const relationStatements = parentEntry
      .getMetadata()
      .find(null, relation.property, entry.getResourceURI());
    const connected = Boolean(relationStatements.length);

    const onClick = async () => {
      await addRelationToParent(parentEntry, entry, relation.property).then(
        () => {
          dispatch({ type: REFRESH });
          setHasConnected(true);
        }
      );
    };

    return {
      disabled: connected,
      children: connected ? t(connectedButtonLabel) : t(connectButtonLabel),
      variant: 'text',
      onClick,
      justifyContent: 'center',
    };
  };

  const afterCreateEntry = (newEntry) => {
    closeDialog();
    afterCreate(newEntry);
  };

  const handleClose = () => {
    closeDialog();
    if (hasConnected) refreshSubEntities();
  };

  const createAction =
    createActions.length > 1
      ? { ...listAction, Component: MultiCreateButton, items: createActions }
      : { ...listAction, Dialog: CreateDialog };

  return (
    <ListActionDialog
      id="search-or-create-dialog"
      title={translate(
        disableCreateActions ? 'searchDialogTitle' : 'searchOrCreateDialogTitle'
      )}
      closeDialog={handleClose}
      closeDialogButtonLabel={translate('close')}
      maxWidth="md"
      fixedHeight
    >
      <ContentWrapper md={12}>
        <EntryListView
          {...queryResults}
          nlsBundles={nlsBundles}
          listActions={disableCreateActions ? [] : [createAction]}
          listActionsProps={{
            actionParams: {
              entityType,
              afterCreateEntry,
              context,
              contextName,
              label,
              nlsBundles,
              entry: actionEntry,
              createDialogParams,
              refreshSubEntities,
            },
            nlsBundles,
          }}
          viewPlaceholderProps={{
            label: translate('emptyMessageWithName', localize(label)),
          }}
          columns={[
            { ...TITLE_COLUMN, xs: 7 },
            MODIFIED_COLUMN,
            {
              ...INFO_COLUMN,
              getProps: ({ entry }) => ({
                action: {
                  ...LIST_ACTION_INFO,
                  entry,
                  nlsBundles,
                  Dialog: ListContentViewerDialog,
                  entityType,
                },
              }),
            },
            {
              id: 'connect',
              xs: 2,
              Component: ListItemButtonAction,
              ButtonComponent: Button,
              getProps: getConnectButtonProps,
            },
          ]}
        />
      </ContentWrapper>
    </ListActionDialog>
  );
};

SearchOrCreateDialog.propTypes = {
  closeDialog: PropTypes.func,
  actionParams: PropTypes.shape({
    entityType: entityTypePropType,
    label: nlsPropType,
    afterCreate: PropTypes.func,
    relation: PropTypes.shape({
      property: PropTypes.string,
      label: nlsPropType,
    }),
    refreshSubEntities: PropTypes.func,
    CreateDialog: PropTypes.func,
    createActions: PropTypes.arrayOf(PropTypes.shape({})),
    entry: PropTypes.instanceOf(Entry),
    createDialogParams: PropTypes.shape({}),
    getQuery: PropTypes.func,
    connectedButtonLabel: PropTypes.string,
    connectButtonLabel: PropTypes.string,
    disableCreateActions: PropTypes.bool,
  }),
  nlsBundles: nlsBundlesPropType,
};

export default withListModelProvider(SearchOrCreateDialog);
