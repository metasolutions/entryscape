import useAsync from 'commons/hooks/useAsync';
import { useEffect } from 'react';
import { getVisualizationData } from 'catalog/datasets/DatasetOverview/utils/visualizations';

const useVisualizationData = ({
  datasetEntry,
  visualizationEntry,
  translate,
}) => {
  const {
    data: visualizationData,
    runAsync,
    isLoading,
    error,
  } = useAsync({ data: {} });

  useEffect(() => {
    runAsync(getVisualizationData(datasetEntry, visualizationEntry, translate));
  }, [runAsync, datasetEntry, visualizationEntry, translate]);

  return { data: visualizationData, isLoading, error };
};

export default useVisualizationData;
