export const URI = 'URI';
export const LITERAL = 'LITERAL';
export const LANGUAGE_LITERAL = 'LANGUAGE_LITERAL';
export const ONLY_LITERAL = 'ONLY_LITERAL';
export const DATATYPE_LITERAL = 'DATATYPE_LITERAL';
export const BLANK = 'BLANK';
export const GROUP = 'GROUP';
export const XSD_BOOLEAN = 'xsd:boolean';
