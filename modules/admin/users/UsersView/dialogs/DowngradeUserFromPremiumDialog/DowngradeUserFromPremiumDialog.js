import { useEffect } from 'react';
import PropTypes from 'prop-types';
import { Entry } from '@entryscape/entrystore-js';
import useAsync from 'commons/hooks/useAsync';
import { useSnackbar } from 'commons/hooks/useSnackbar';
import { useTranslation } from 'commons/hooks/useTranslation';
import esadUserNLS from 'admin/nls/esadUser.nls';
import { useListModel, RERENDER } from 'commons/components/ListView';
import { downgradeUserFromPremium } from 'commons/util/user';

const DowngradeUserFromPremiumDialog = ({ entry: userEntry }) => {
  const [, dispatch] = useListModel();
  const translate = useTranslation(esadUserNLS);
  const { runAsync } = useAsync(null);
  const [addSnackbar] = useSnackbar();

  useEffect(() => {
    runAsync(
      downgradeUserFromPremium(userEntry).then(() => {
        dispatch({ type: RERENDER });
        addSnackbar({ message: translate('downgradeFromPremiumNotification') });
      })
    );
  }, [addSnackbar, dispatch, runAsync, translate, userEntry]);

  return null;
};

DowngradeUserFromPremiumDialog.propTypes = {
  entry: PropTypes.instanceOf(Entry),
};

export default DowngradeUserFromPremiumDialog;
