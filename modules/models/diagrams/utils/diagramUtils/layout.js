import { layout } from 'jointjs';
import dagre from 'dagre';
import graphlib from 'graphlib';

/**
 * Applies auto layout on graph. Can for example be used when setting an initial
 * layout or clearing a layout.
 *
 * @param {object} graph
 * @returns {undefined}
 */
export const applyAutoLayout = (graph) => {
  layout.DirectedGraph.layout(graph, {
    setLinkVertices: false,
    marginX: 50,
    marginY: 50,
    nodeSep: 40,
    edgeSep: 20,
    rankSep: 20,
    dagre,
    graphlib,
  });
};
