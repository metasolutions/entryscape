import useRDFormsPresenter from 'commons/components/rdforms/hooks/useRDFormsPresenter';
import initLinkBehaviour from 'commons/components/rdforms/LinkBehaviour';
import { useEffect } from 'react';

const useLDBFormsPresenter = (onLinkClick, ...rest) => {
  useEffect(() => {
    initLinkBehaviour(onLinkClick);
    return () => {
      initLinkBehaviour();
    };
  }, [onLinkClick]);

  return useRDFormsPresenter(...rest);
};

export default useLDBFormsPresenter;
