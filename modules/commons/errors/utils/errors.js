export class ErrorWithMessage extends Error {
  constructor(message, originalError) {
    super(message);
    this.name = 'ErrorWithMessage';
    this.originalError = originalError;
  }
}
