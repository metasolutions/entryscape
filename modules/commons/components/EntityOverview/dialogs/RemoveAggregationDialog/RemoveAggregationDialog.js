import PropTypes from 'prop-types';
import { useCallback } from 'react';
import RemoveEntryDialog from 'commons/components/entry/RemoveEntryDialog';
import {
  useTranslation,
  nlsBundlesPropType,
} from 'commons/hooks/useTranslation';
import { removeEntryAndRelations } from 'commons/types/utils/relations';
import { entryPropType } from 'commons/util/entry';
import useCheckReferences from 'commons/hooks/references/useCheckReferences';
import escaReferencesNLS from 'catalog/nls/escaReferences.nls';
import { RESOLVED } from 'commons/hooks/useAsync';

const RemoveAggregationDialog = ({
  entry: connectedEntry,
  closeDialog,
  refresh: refreshOverview,
  nlsBundles,
}) => {
  const bundles = [...nlsBundles, escaReferencesNLS];
  const translate = useTranslation(bundles);

  const checkCanRemove = useCallback(
    (refs, status) => status === RESOLVED && refs.length === 1,
    []
  );
  const { references, checkReferencesStatus } = useCheckReferences({
    entry: connectedEntry,
    nlsBundles: bundles,
    callback: closeDialog,
    checkCanRemove,
  });

  const onRemove = () =>
    removeEntryAndRelations(connectedEntry, references, () => {
      closeDialog();
      refreshOverview();
    });

  if (checkReferencesStatus !== RESOLVED) return null;

  return references.length === 1 ? (
    <RemoveEntryDialog
      entry={connectedEntry}
      closeDialog={closeDialog}
      onRemove={onRemove}
      removeConfirmMessage={<p>{translate('removeAggregationConfirmation')}</p>}
    />
  ) : null;
};

RemoveAggregationDialog.propTypes = {
  entry: entryPropType,
  closeDialog: PropTypes.func,
  refresh: PropTypes.func,
  nlsBundles: nlsBundlesPropType,
};

export default RemoveAggregationDialog;
