import PropTypes from 'prop-types';
import { Checkbox } from '@mui/material';
import Tooltip from 'commons/components/common/Tooltip';
import { ListItemAction } from '../ListItemAction';

export const ListItemCheckbox = ({
  ariaLabel,
  checked,
  value,
  disabled,
  onChange,
  tooltip,
}) => {
  return (
    <ListItemAction xs={1}>
      <Tooltip title={tooltip}>
        <span>
          <Checkbox
            checked={checked}
            value={value}
            disabled={disabled}
            inputProps={{
              'aria-label': `${ariaLabel} checkbox`,
            }}
            onChange={onChange}
          />
        </span>
      </Tooltip>
    </ListItemAction>
  );
};

ListItemCheckbox.propTypes = {
  ariaLabel: PropTypes.string,
  checked: PropTypes.bool,
  value: PropTypes.string,
  disabled: PropTypes.bool,
  onChange: PropTypes.func,
  tooltip: PropTypes.string,
};

export default ListItemCheckbox;
