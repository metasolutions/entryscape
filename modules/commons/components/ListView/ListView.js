import PropTypes from 'prop-types';
import escoListNLS from 'commons/nls/escoList.nls';
import { nlsBundlesPropType } from 'commons/hooks/useTranslation';
import { ListPlaceholder } from './ListPlaceholder';
import { ListViewContext } from './hooks/useListView';

// To create a list view, both the ListView component and a list model provider
// must be included for state handling in list view components. The list view
// receives props that are shared by many of its children, for example
// nlsBundles and props that usually are the result from an async call, such as
// size and status. The hook useListview is used to read these props.
// To show a placeholder when the list view is empty, a renderPlaceholder
// function must be provided.
export const ListView = ({
  renderPlaceholder = (Placeholder) => <Placeholder />,
  showPlaceholder = true,
  nlsBundles = escoListNLS,
  search = '',
  size,
  status,
  children,
}) => {
  const isEmpty = search.length === 0 && size === 0;

  return (
    <ListViewContext.Provider value={{ search, size, status, nlsBundles }}>
      {isEmpty && status === 'resolved' && showPlaceholder && renderPlaceholder
        ? renderPlaceholder(ListPlaceholder)
        : children}
    </ListViewContext.Provider>
  );
};

ListView.propTypes = {
  renderPlaceholder: PropTypes.func,
  search: PropTypes.string,
  showPlaceholder: PropTypes.bool,
  size: PropTypes.number,
  status: PropTypes.string,
  nlsBundles: nlsBundlesPropType,
  children: PropTypes.node,
};
