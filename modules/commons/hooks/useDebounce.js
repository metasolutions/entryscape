import { useCallback } from 'react';
import { debounce } from 'lodash-es';

const useDebounce = (callbackFn, delay) => {
  const debouncedFn = useCallback(
    debounce((...args) => callbackFn(...args), delay),
    [delay]
  );
  return debouncedFn;
};

export default useDebounce;
