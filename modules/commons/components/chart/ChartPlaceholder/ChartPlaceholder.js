import PropTypes from 'prop-types';
import { useEffect, useRef } from 'react';
import { CircularProgress, Typography } from '@mui/material';
import { getPlaceholderOptions } from './util';
import './ChartPlaceholder.scss';

const ChartPlaceholder = ({ label, loading = false }) => {
  const chartContainer = useRef(null);

  useEffect(() => {
    if (chartContainer && chartContainer.current) {
      import(/* webpackChunkName: "chart-js" */ 'chart.js')
        .then((Chart) => Chart.default)
        .then((Chart) => {
          // eslint-disable-next-line no-new
          new Chart(chartContainer.current, {
            type: 'bar',
            options: getPlaceholderOptions(),
            data: { datasets: [] },
          });
        });
    }
  }, [chartContainer]);

  return (
    <div className="escoChartPlaceholder">
      <canvas
        aria-label="Statistics placeholder"
        // eslint-disable-next-line jsx-a11y/no-interactive-element-to-noninteractive-role
        role="img"
        ref={chartContainer}
      />
      {loading ? (
        <CircularProgress
          classes={{ root: 'escoChartPlaceholder__progress' }}
        />
      ) : (
        <Typography className="escoChartPlaceholder__label" variant="body1">
          {label}
        </Typography>
      )}
    </div>
  );
};

ChartPlaceholder.propTypes = {
  label: PropTypes.string,
  loading: PropTypes.bool,
};

export default ChartPlaceholder;
