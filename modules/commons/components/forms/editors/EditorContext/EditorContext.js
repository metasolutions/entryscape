import {
  createContext,
  useContext,
  useReducer,
  useState,
  useCallback,
  useEffect,
} from 'react';
import PropTypes from 'prop-types';
import { Graph } from '@entryscape/rdfjson';
import {
  refreshEntry,
  isNewEntry,
  graphPropType,
  entryPropType,
} from 'commons/util/entry';
import useAsync from 'commons/hooks/useAsync';
import { validateFieldSet } from '../utils/validate';

export const ADD_FIELD = 'ADD_FIELD';
export const ADD_FIELDS = 'ADD_FIELDS';
export const REMOVE_FIELD = 'REMOVE_FIELD';
export const REMOVE_FIELD_GROUP = 'REMOVE_FIELD_GROUP';
export const REMOVE_FIELD_GROUPS = 'REMOVE_FIELD_GROUPS';
export const REFRESH = 'REFRESH';
export const CHANGE = 'CHANGE';

const EditorContext = createContext();

export const useEditorContext = () => {
  const context = useContext(EditorContext);
  if (!context)
    throw new Error('Editor context must be used within context provider');
  return context;
};

// Wrapper to extract onRemove and onChange callbacks from dispatch calls
export const useCustomDispatch = ({ dispatch, field, onRemove, onChange }) => {
  const customDispatch = useCallback(
    (action) => {
      const { type, value } = action;
      if (type === CHANGE && onChange) {
        onChange(field, value);
      } else if (type === REMOVE_FIELD && onRemove) {
        onRemove(field);
      }
      dispatch(action);
    },
    [dispatch, field, onRemove, onChange]
  );

  return customDispatch;
};

/**
 * The reducer handles the fieldSet state, which is used to keep track of all
 * fields. This is for example needed when validating the form.
 *
 * @param {object} state
 * @param {object} action
 * @returns {object}
 */
const fieldsReducer = (state, action) => {
  switch (action.type) {
    case ADD_FIELD: {
      const { field, formType } = action;
      const { fieldSet } = state;
      fieldSet.add(field);
      if (!('formType' in action)) return { ...state };
      return {
        ...state,
        formType,
        previousFormType: state.formType,
      };
    }
    case ADD_FIELDS: {
      const { fields } = action;
      const { fieldSet } = state;
      for (const field of fields) {
        fieldSet.add(field);
      }
      return state;
    }
    case REMOVE_FIELD: {
      return { ...state };
    }
    case REMOVE_FIELD_GROUP: {
      const { field } = action;
      const { fieldSet } = state;
      fieldSet.delete(field);
      return { ...state };
    }
    case REMOVE_FIELD_GROUPS: {
      const { fieldGroups } = action;
      const { fieldSet } = state;
      fieldGroups.forEach((fieldGroup) => {
        fieldGroup.remove();
        fieldSet.delete(fieldGroup);
      });
      return { ...state };
    }
    case REFRESH: {
      return { ...state };
    }
    case CHANGE: {
      const { formType } = action;

      if (!formType) return { ...state };

      // If a field group is dependent on form type, it should be reset if the
      // type changes
      if (state.previousFormType !== null) {
        state.fieldSet.forEach((fieldGroup) => {
          if (!fieldGroup.getResetOnFormType()) return;
          fieldGroup.clear();
        });
      }

      return {
        ...state,
        formType,
        previousFormType: state.formType,
      };
    }
    default:
      return state;
  }
};

export const EditorProvider = ({
  entry,
  graph,
  resourceURI,
  rootEditors,
  ...props
}) => {
  const [editGraph, setEditGraph] = useState(null);
  const { runAsync, data: initialGraph } = useAsync(null);

  useEffect(() => {
    // refreshes entry and makes a copy of the graph
    const refreshAndExportGraph = async (entryToRefresh) => {
      const refreshedEntryPromise = isNewEntry(entryToRefresh)
        ? Promise.resolve(entryToRefresh)
        : refreshEntry(entryToRefresh);
      const refreshedEntry = await refreshedEntryPromise;
      return new Graph(refreshedEntry.getMetadata().exportRDFJSON());
    };

    // if initial graph provided
    if (graph) {
      setEditGraph(new Graph(graph.exportRDFJSON()));
      return;
    }
    runAsync(refreshAndExportGraph(entry));
  }, [runAsync, entry, graph]);

  const [{ fieldSet, formType }, dispatch] = useReducer(fieldsReducer, {
    fieldSet: new Set(),
    formType: null,
    previousFormType: null,
    rootEditors,
  });

  const validate = useCallback(() => {
    const errors = validateFieldSet(fieldSet);
    dispatch({ type: REFRESH });
    return errors;
  }, [fieldSet]);

  const canSubmit =
    fieldSet.size > 0 &&
    Array.from(fieldSet).every((fieldGroup) => !fieldGroup.hasError());

  return (
    <EditorContext.Provider
      value={{
        entry,
        graph: editGraph || initialGraph,
        resourceURI: resourceURI || entry.getResourceURI(),
        setGraph: setEditGraph,
        dispatch,
        validate,
        fieldSet,
        formType,
        canSubmit,
        rootEditors,
      }}
      {...props}
    />
  );
};

EditorProvider.propTypes = {
  entry: entryPropType,
  graph: graphPropType,
  resourceURI: PropTypes.string,
  rootEditors: PropTypes.arrayOf(PropTypes.shape({})),
};

// hoc to add EditorProvider
export const withEditorProvider =
  (Component, rootEditors) =>
  // eslint-disable-next-line react/prop-types
  ({ entry, graph, resourceURI, ...props }) => (
    <EditorProvider
      entry={entry}
      graph={graph}
      resourceURI={resourceURI}
      rootEditors={rootEditors}
    >
      <Component {...props} />
    </EditorProvider>
  );
