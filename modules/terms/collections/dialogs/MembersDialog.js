import React, { useState, useEffect, useCallback } from 'react';
import PropTypes from 'prop-types';
import { Entry } from '@entryscape/entrystore-js';
import { Button } from '@mui/material';
import ContentWrapper from 'commons/components/common/ContentWrapper';
import esteCollectionNLS from 'terms/nls/esteCollection.nls';
import esteConceptNLS from 'terms/nls/esteConcept.nls';
import { useTranslation } from 'commons/hooks/useTranslation';
import { getConceptCount } from 'terms/utils';
import { useMainDialog } from 'commons/hooks/useMainDialog';
import { AcknowledgeAction } from 'commons/components/common/dialogs/MainDialog';
import ListActionDialog from 'commons/components/common/dialogs/ListActionDialog';
import Placeholder from 'commons/components/common/Placeholder';
import useAsync from 'commons/hooks/useAsync';
import { checkEntriesHaveConflict } from 'commons/util/entry';
import { ErrorCatcher } from 'commons/errors/ErrorCatcher';
import { ErrorWithMessage } from 'commons/errors/utils/errors';
import { useConfirmCloseAction } from 'commons/components/EntryListView/dialogs/create/handlers';
import CollectionTree from '../CollectionTree';
import useRemoteStore from '../CollectionOverview/hooks/useRemoteStore';

const saveMembers = (collectionEntry, treeModel, remoteStore) => {
  const checkedEntries = [];
  const uncheckedEntries = [];
  const { checkedNodes } = treeModel;
  const { uncheckedNodes } = treeModel;
  const collectionURI = collectionEntry.getResourceURI();
  const membersPromises = [];

  Object.keys(checkedNodes).forEach((checkedNode) =>
    membersPromises.push(
      treeModel
        .getEntry(checkedNode)
        .then((checkedEntry) => checkedEntries.push(checkedEntry))
    )
  );

  Object.keys(uncheckedNodes).forEach((uncheckedNode) =>
    membersPromises.push(
      treeModel
        .getEntry(uncheckedNode)
        .then((uncheckedEntry) => uncheckedEntries.push(uncheckedEntry))
    )
  );

  return Promise.all(membersPromises).then(() =>
    checkEntriesHaveConflict(
      [collectionEntry, ...checkedEntries, ...uncheckedEntries],
      remoteStore
    ).then((hasConflict) => {
      if (hasConflict) throw Error('At least one entry was modified');

      checkedEntries.forEach((checkedEntry) => {
        const conceptURI = checkedEntry.getResourceURI();
        collectionEntry
          .getMetadata()
          .add(collectionURI, 'skos:member', conceptURI);
      });

      uncheckedEntries.forEach((uncheckedEntry) => {
        const conceptURI = uncheckedEntry.getResourceURI();
        collectionEntry
          .getMetadata()
          .findAndRemove(collectionURI, 'skos:member', conceptURI);
      });

      return collectionEntry.commitMetadata();
    })
  );
};

const MembersDialog = ({
  collectionEntry,
  closeDialog,
  callback = () => {},
  conceptSchemeEntry,
}) => {
  const translate = useTranslation([esteCollectionNLS, esteConceptNLS]);
  const { remoteStore } = useRemoteStore();
  const { openMainDialog } = useMainDialog();
  const [conceptTree, setConceptTree] = useState(null);
  const [isButtonLocked, setIsButtonLocked] = useState(true);
  const [domNode, setDomNode] = useState(null);
  // Currently the members dialog is only available via source terminologies, which by
  // definition have concepts, so this can be removed unless we plan to use the tree elsewhere
  const { data: conceptCount, runAsync } = useAsync(null);
  const [saveError, setSaveError] = useState();
  const confirmClose = useConfirmCloseAction(closeDialog);

  const checkChange = useCallback(() => {
    const { checkedNodes, uncheckedNodes } = conceptTree.getTreeModel();
    const isLocked = !(
      Object.keys(checkedNodes).length + Object.keys(uncheckedNodes).length >
      0
    );
    setIsButtonLocked(isLocked);
  }, [conceptTree]);

  useEffect(() => {
    runAsync(getConceptCount(conceptSchemeEntry));
  }, [conceptSchemeEntry, runAsync]);

  // import dependencies
  useEffect(() => {
    import(/* webpackChunkName: "jstree" */ 'jstree');
    import(
      // eslint-disable-next-line max-len
      /* webpackChunkName: "jstree-bootstrap-theme" */ 'jstree-bootstrap-theme/dist/themes/proton/style.min.css'
    );
  }, []);

  // create jsTree
  useEffect(() => {
    if (!domNode) return;
    setConceptTree(new CollectionTree({ domNode }));
  }, [domNode]);

  // render jsTree
  useEffect(() => {
    if (!conceptTree) return;
    conceptTree.showEntry(conceptSchemeEntry, collectionEntry, checkChange);
  }, [checkChange, collectionEntry, conceptSchemeEntry, conceptTree]);

  const handleSave = () => {
    saveMembers(collectionEntry, conceptTree.getTreeModel(), remoteStore)
      .then(callback)
      .then(closeDialog)
      .catch((error) => {
        if (error?.message?.includes('At least one entry was modified')) {
          openMainDialog({
            content: translate('concurrentConflictMessage'),
            actions: <AcknowledgeAction onDone={closeDialog} />,
          });
        } else {
          setSaveError(
            new ErrorWithMessage(translate('editCollectionFail'), error)
          );
        }
      });
  };

  const actions = (
    <Button autoFocus onClick={handleSave} disabled={isButtonLocked}>
      {translate('manageMembersButton')}
    </Button>
  );

  return (
    <>
      <ListActionDialog
        closeDialog={() => confirmClose(!isButtonLocked)}
        id="manage-collections-concepts"
        title={translate('manageMembersHeader')}
        actions={actions}
        maxWidth="sm"
      >
        {conceptCount === 0 ? (
          <Placeholder
            variant="dialog"
            label={translate('manageMembersPlaceholder')}
          />
        ) : (
          <ContentWrapper xs={10}>
            <div ref={setDomNode} />
          </ContentWrapper>
        )}
      </ListActionDialog>
      <ErrorCatcher error={saveError} />
    </>
  );
};

MembersDialog.propTypes = {
  collectionEntry: PropTypes.instanceOf(Entry),
  closeDialog: PropTypes.func,
  callback: PropTypes.func,
  conceptSchemeEntry: PropTypes.instanceOf(Entry),
};

export default MembersDialog;
