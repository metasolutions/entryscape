import { useCallback, useMemo } from 'react';
import { Entry } from '@entryscape/entrystore-js';
import { RDF_TYPE_CONCEPT, RDF_TYPE_TERMINOLOGY } from 'commons/util/entry';
import { useSolrQuery } from 'commons/components/EntryListView';
import useSearchFilters from 'commons/components/filters/hooks/useSearchFilters';
import { getConceptPath } from 'commons/tree/skos/util';
import {
  ANY,
  ANY_FILTER_ITEM,
} from 'commons/components/filters/utils/filterDefinitions';
import Filter from 'commons/components/filters/utils/filter';
import { i18n } from 'esi18n';
import { getFullLengthLabel } from 'commons/util/rdfUtils';
import { truncate } from 'commons/util/util';
import useRemoteStore from 'terms/collections/CollectionOverview/hooks/useRemoteStore';
import { entrystore } from 'commons/store';
import { FILTER, RERENDER, useListModel } from 'commons/components/ListView';

const SOURCE_TERMINOLOGIES = 'source';
const OTHER_SOURCE_TERMINOLOGIES = 'other';

const SOURCE_FILTER_ID = 'source-filter';
const TERMINOLOGY_FILTER_ID = 'terminology-filter';

const TERMINOLOGY_FILTER = {
  type: 'autocomplete',
  headerNlsKey: 'editConceptsTerminologySelectLabel',
  applyFilterParams: (query, filter) => {
    const value = filter.getSelected();
    if (value === 'all') return query;
    return query.context(value);
  },
  id: TERMINOLOGY_FILTER_ID,
};

/**
 * Fetch termonology entries and sort them into source and other. Currently
 * included concepts in the collection belongs to source.
 *
 * @param {Entry[]} collectionConcepts
 * @param {object} store
 * @returns {object}
 */
const getTerminologiesBySource = async (collectionConcepts, store) => {
  const terminologyEntries = await store
    .newSolrQuery()
    .rdfType(RDF_TYPE_TERMINOLOGY)
    .limit(100)
    .list()
    .getEntries();

  if (!terminologyEntries.length) return {};

  const sourceTerminologyIds = [
    ...new Set(collectionConcepts.map((entry) => entry.getContext().getId())),
  ];
  const sourceTerminologyEntries = terminologyEntries.filter((entry) =>
    sourceTerminologyIds.includes(entry.getContext().getId())
  );
  const otherTerminologyEntries = terminologyEntries.filter(
    (entry) => !sourceTerminologyIds.includes(entry.getContext().getId())
  );

  return { sourceTerminologyEntries, otherTerminologyEntries };
};

/**
 *
 * @param {{remoteStore: object, remoteStoreLabel: string}} remoteConfig
 * @param {Function} translate
 * @returns {object[]}
 */
const getStoreChoices = ({ remoteStore, remoteStoreLabel }, translate) => {
  const locale = i18n.getLocale();
  return [
    {
      value: ANY,
      label: translate('defaultStoreLabel'),
    },
    ...(remoteStore
      ? [
          {
            value: 'remote',
            label: remoteStoreLabel[locale],
          },
        ]
      : []),
  ];
};

/**
 *
 * @param {{sourceTerminologyEntries: Entry[], otherTerminologyEntries: Entry[]}} terminologies
 * @param {Function} translate
 * @returns {object[]}
 */
const getTerminologyChoices = (
  { sourceTerminologyEntries, otherTerminologyEntries },
  translate
) => {
  return [
    ANY_FILTER_ITEM,
    ...(sourceTerminologyEntries.length
      ? [
          {
            value: SOURCE_TERMINOLOGIES,
            label: translate('editConceptsTerminologySelectSource'),
            labelNlsKey: 'editConceptsTerminologySelectSource',
            divider: true,
          },
        ]
      : []),
    ...sourceTerminologyEntries.map((entry, index) => {
      return {
        value: entry.getContext().getId(),
        label: truncate(getFullLengthLabel(entry), 100),
        divider: index === sourceTerminologyEntries.length - 1,
        type: translate('editConceptsTerminologySelectSubheaderSource'),
        entry,
        sourceType: SOURCE_TERMINOLOGIES,
      };
    }),
    ...otherTerminologyEntries.map((entry) => {
      return {
        value: entry.getContext().getId(),
        label: truncate(getFullLengthLabel(entry), 100),
        type: translate('editConceptsTerminologySelectSubheaderOther'),
        sourceType: OTHER_SOURCE_TERMINOLOGIES,
      };
    }),
  ];
};

/**
 *
 * @param {string} selection
 * @param {object[]} terminologyItems
 * @returns {string|string[]}
 */
const getIncludedContexts = (selection, terminologyItems) => {
  if (selection === ANY) return null;
  if (selection === SOURCE_TERMINOLOGIES) {
    return terminologyItems
      .filter(({ sourceType }) => sourceType === SOURCE_TERMINOLOGIES)
      .map(({ entry }) => entry.getContext().getId());
  }
  return selection;
};

/**
 *
 * @param {Entry[]} collectionConcepts
 * @param {object} storeConfig
 * @param {Function} translate
 * @param {Function} dispatch
 * @returns {Filter[]}
 */
const createFilters = (
  collectionConcepts,
  storeConfig,
  translate,
  dispatch
) => {
  let selectedStore = entrystore;
  const { remoteStore } = storeConfig;

  const terminologyFilter = new Filter({
    ...TERMINOLOGY_FILTER,
    loadItems: async () => {
      const terminologyEntriesBySource = await getTerminologiesBySource(
        collectionConcepts,
        selectedStore
      );
      return getTerminologyChoices(terminologyEntriesBySource, translate);
    },
    // rerender the filter when the new items are loaded
    onLoadItems: () => {
      dispatch({ type: RERENDER });
    },
    applyFilterParams: (query, filter) => {
      const value = filter.getSelected();
      const items = filter.getItems();
      const contexts = getIncludedContexts(value, items);
      if (contexts) {
        query.context(contexts);
      }
      return query;
    },
  });

  const sourceFilter = new Filter({
    id: SOURCE_FILTER_ID,
    headerNlsKey: 'storeSelectLabel',
    initialSelection: ANY,
    onSelect: (value) => {
      selectedStore = value === ANY ? entrystore : remoteStore;
      terminologyFilter.setSelected(ANY);
      // update terminpology filter in list model as it's not set with
      // onFilterSelect here
      dispatch({
        type: FILTER,
        filter: { [terminologyFilter.getId()]: ANY },
      });
      terminologyFilter.loadItems();
    },
    items: getStoreChoices(storeConfig, translate),
  });

  return [sourceFilter, terminologyFilter];
};

/**
 *
 * @param {Filter[]} filters
 * @param {object} remoteStore
 * @returns {object}
 */
const getStoreFromFilters = (filters, remoteStore) => {
  if (!remoteStore) return entrystore;
  const storeFilter = filters.find(
    (filter) => filter.getId() === SOURCE_FILTER_ID
  );
  if (!storeFilter) return entrystore;
  return storeFilter.getSelected() === ANY ? entrystore : remoteStore;
};

const useGetConcepts = (collectionConcepts, translate) => {
  const { remoteStore, remoteStoreLabel } = useRemoteStore();
  const [{ limit }, dispatch] = useListModel();
  const filters = useMemo(() => {
    return createFilters(
      collectionConcepts,
      { remoteStore, remoteStoreLabel },
      translate,
      dispatch
    );
  }, [collectionConcepts, translate, dispatch, remoteStore, remoteStoreLabel]);

  const selectedStore = getStoreFromFilters(filters, remoteStore);

  const { applyFilters, isLoading, hasFilters, ...filtersProps } =
    useSearchFilters(null, filters);

  const createQuery = useCallback(() => {
    return selectedStore.newSolrQuery().rdfType(RDF_TYPE_CONCEPT);
  }, [selectedStore]);

  const getConceptListItems = useCallback(
    async ({ size, entries: conceptEntries }) => {
      const conceptListItemPromises = conceptEntries.map(async (entry) => {
        const path = await getConceptPath(entry, [], selectedStore);
        return { entry, path };
      });
      const listItems = await Promise.all(conceptListItemPromises);
      return { listItems, size };
    },
    [selectedStore]
  );

  const { listItems, size, search, status } = useSolrQuery({
    createQuery,
    limit,
    applyFilters,
    callback: getConceptListItems,
    wait: isLoading,
  });

  return {
    listItems,
    size,
    search,
    status,
    filtersProps,
    includeFilters: hasFilters,
  };
};

export default useGetConcepts;
