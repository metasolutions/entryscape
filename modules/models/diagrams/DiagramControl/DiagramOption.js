import React from 'react';
import PropTypes from 'prop-types';
import { Entry } from '@entryscape/entrystore-js';
import { useTranslation } from 'commons/hooks/useTranslation';
import esmoDiagramsNLS from 'models/nls/esmoDiagrams.nls';
import './DiagramOption.scss';
import {
  getDiagramLabel,
  getDiagramDescription
} from '../utils/diagram';

const DiagramOption = ({ diagramEntry, defaultDiagram = false }) => {
  const translate = useTranslation([esmoDiagramsNLS]);
  const heading = getDiagramLabel(diagramEntry, translate, defaultDiagram);
  const description = getDiagramDescription(
    diagramEntry,
    translate,
    defaultDiagram
  );
  return (
    <div style={{ display: 'flex', flexDirection: 'column' }}>
      <div>
        <h2 className="esmoDiagramOption__heading">{heading}</h2>
      </div>
      <div className="esmoDiagramOption__description">{description}</div>
    </div>
  );
};

DiagramOption.propTypes = {
  defaultDiagram: PropTypes.bool,
  diagramEntry: PropTypes.instanceOf(Entry),
};

export default DiagramOption;
