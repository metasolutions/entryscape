import PropTypes from 'prop-types';
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  Button,
} from '@mui/material';
import escoListNLS from 'commons/nls/escoList.nls';
import { useTranslation } from 'commons/hooks/useTranslation';
import RdformsAlert from 'commons/components/rdforms/RdformsAlert';
import './index.scss';

// TODO nls 'yes' and 'no' and defaults
const ListActionDialog = ({
  // fixed props - passed by (list) SecondaryActions
  id,
  title,
  actions,
  closeDialog,
  maxWidth = 'lg',
  children,
  closeDialogButtonLabel,
  alert: alertProps = {},
  fixedHeight,
}) => {
  const t = useTranslation(escoListNLS);

  return (
    <Dialog
      classes={{
        paper: fixedHeight ? 'escoListActionDialog--fixedHeight' : '',
      }}
      onClose={closeDialog}
      aria-labelledby={`${id}-title`}
      open
      fullWidth
      maxWidth={maxWidth}
      PaperProps={{ id }}
    >
      {title && (
        <DialogTitle
          id={`${id}-title`}
          onClose={closeDialog}
          className="escoListActionDialog__title"
        >
          {title}
        </DialogTitle>
      )}
      <DialogContent dividers={!!title}>
        {children}
        <RdformsAlert {...alertProps} />
      </DialogContent>
      <DialogActions>
        <Button autoFocus={!actions} variant="text" onClick={closeDialog}>
          {closeDialogButtonLabel || t('cancel')}
        </Button>
        {actions}
      </DialogActions>
    </Dialog>
  );
};

ListActionDialog.propTypes = {
  id: PropTypes.string,
  title: PropTypes.string,
  actions: PropTypes.node,
  closeDialog: PropTypes.func,
  maxWidth: PropTypes.string,
  children: PropTypes.node,
  closeDialogButtonLabel: PropTypes.string,
  alert: PropTypes.shape({
    message: PropTypes.string,
    setMessage: PropTypes.func,
    props: PropTypes.shape({}),
  }),
  fixedHeight: PropTypes.bool,
};

export default ListActionDialog;
