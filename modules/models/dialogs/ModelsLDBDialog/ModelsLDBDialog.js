import React from 'react';
import { Graph } from '@entryscape/rdfjson';
import { entryPropType } from 'commons/util/entry';
import { useTranslation } from 'commons/hooks/useTranslation';
import LinkedDataBrowserDialog from 'commons/components/LinkedDataBrowserDialog';
import { renderFieldsPresenter } from 'commons/components/FieldsLinkedDataBrowserDialog';
import { FIELDS_FORMS_INFO } from 'models/utils/fieldDefinitions';
import { fieldDefinitions as namespaceFields } from 'models/namespaces/utils/fieldDefinitions';
import { matchesType } from 'models/utils/metadata';
import {
  RDF_TYPE_FORM,
  RDF_TYPE_FIELD,
  RDF_TYPE_PREFIX,
} from 'models/utils/ns';
import esmoCommonsNLS from 'models/nls/esmoCommons.nls';
import escoDialogsNLS from 'commons/nls/escoDialogs.nls';

const NLS_BUNDLES = [esmoCommonsNLS, escoDialogsNLS];

/**
 *
 * @param {Graph} graph
 * @param {string} resourceURI
 * @returns {object[]|undefined}
 */
const getPresenterFields = (graph, resourceURI) => {
  const isRfs = matchesType(graph, resourceURI, [
    RDF_TYPE_FORM,
    RDF_TYPE_FIELD,
  ]);
  if (isRfs) return FIELDS_FORMS_INFO;
  const isNamespace = matchesType(graph, resourceURI, [RDF_TYPE_PREFIX]);
  if (isNamespace) return namespaceFields;
};

const ModelsLinkedDataBrowserDialog = ({ entry, ...rest }) => {
  const translate = useTranslation(NLS_BUNDLES);

  const renderPresenter = ({ entry: currentEntry }) => {
    const graph = currentEntry.getMetadata();
    const resourceURI = currentEntry.getResourceURI();
    const fields = getPresenterFields(graph, resourceURI);
    if (fields) {
      return renderFieldsPresenter({
        entry: currentEntry,
        translate,
        fields,
      });
    }
    return null;
  };

  return (
    <LinkedDataBrowserDialog
      {...rest}
      entry={entry}
      renderPresenter={renderPresenter}
    />
  );
};

ModelsLinkedDataBrowserDialog.propTypes = {
  entry: entryPropType,
};

export default ModelsLinkedDataBrowserDialog;
