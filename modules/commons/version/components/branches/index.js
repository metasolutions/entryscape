import {
  getAvailableBranches,
  getBranch,
} from 'commons/version/util/fetchBranches';
import { useEffect, useState } from 'react';
import { FormControl, InputLabel, MenuItem, Select } from '@mui/material';

export default () => {
  const [branch, setBranch] = useState('');
  const [branches, setBranches] = useState([]);
  const [loading, setLoading] = useState(true);

  /**
   * Add the branch as a search param and reload.
   *
   * @param {object} event
   */
  const changeBranchHandler = (event) => {
    const urlQuery = window.location.search;
    const searchParams = new URLSearchParams(urlQuery);
    searchParams.set('branch', event.target.value);
    window.location.assign(`?${searchParams.toString()}`);
  };

  /**
   * Update the state with available branches
   */
  const setAvailableBranches = () => {
    getAvailableBranches().then((branches) => {
      setBranches(branches);
      setLoading(false);
    });
  };

  useEffect(() => {
    setBranch(getBranch());
    setAvailableBranches();
  }, []);

  return (
    <>
      <h2>Switch branch</h2>
      <br />
      <p>
        Your current branch is <b>{branch}</b>
      </p>
      <div className="row">
        {loading ? (
          <div>Fetching available branches...</div>
        ) : (
          <div>
            <FormControl variant="filled" sx={{ width: '240px' }}>
              <InputLabel id="branch-select-label">Select branch</InputLabel>
              <Select
                labelId="branch-select-label"
                id="branch-select"
                value={branch}
                onChange={changeBranchHandler}
              >
                {branches.map((branch) => (
                  <MenuItem value={branch} key={branch}>
                    {branch}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>
          </div>
        )}
      </div>
    </>
  );
};
