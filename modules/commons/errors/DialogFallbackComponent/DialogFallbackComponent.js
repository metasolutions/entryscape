import PropTypes from 'prop-types';
import { useTranslation } from 'commons/hooks/useTranslation';
import errorsNls from 'commons/nls/escoErrors.nls';
import { ErrorWithMessage } from '../utils/errors';
import { ErrorDialog } from '../ErrorDialog';

const DialogFallbackComponent = ({ error, resetError }) => {
  const translate = useTranslation(errorsNls);

  const handleClose = () => {
    resetError();
  };

  const getMessage = () => {
    return error instanceof ErrorWithMessage
      ? error.message
      : translate('errorHeader');
  };

  return error ? (
    <ErrorDialog open onClose={handleClose} errorText={getMessage()} />
  ) : null;
};

DialogFallbackComponent.propTypes = {
  error: PropTypes.instanceOf(Error),
  resetError: PropTypes.func,
};

export default DialogFallbackComponent;
