import PropTypes from 'prop-types';
import { Typography } from '@mui/material';
import './OverviewDescription.scss';

const OverviewDescription = ({ label, ...typographyProps }) => {
  return (
    <Typography {...typographyProps} className="escoOverview__description">
      {label}
    </Typography>
  );
};

OverviewDescription.propTypes = {
  label: PropTypes.string,
};

export default OverviewDescription;
