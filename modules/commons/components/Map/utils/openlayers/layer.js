import { Tile as TileLayer, Vector as VectorLayer } from 'ol/layer';
import TileWMS from 'ol/source/TileWMS';
import XYZ from 'ol/source/XYZ';
import Feature from 'ol/Feature';
import VectorSource from 'ol/source/Vector';
import Point from 'ol/geom/Point';
import { fromLonLat } from 'ol/proj';
import config from 'config';
import { createMarker } from './style';

/**
 * @returns {TileLayer}
 */
export const createDefaultBasemap = ({ visible = true } = {}) => {
  const geochooserMapURL = config.get('itemstore.geochooserMapURL', null);
  const geochooserAttribution = config.get(
    'itemstore.geochooserMapAttribution',
    null
  );

  return new TileLayer({
    source: new XYZ({
      attributions: geochooserAttribution,
      url: geochooserMapURL,
    }),
    title: 'Background map',
    visible,
    type: 'basemap',
  });
};

/**
 * @param {Object} layerOptions
 * @returns {VectorLayer}
 */
export const createVectorLayer = ({ features = [], style } = {}) => {
  const vectorLayer = new VectorLayer({
    source: new VectorSource({
      features,
      wrapX: false,
    }),
    style,
  });
  return vectorLayer;
};

/**
 * Adds xy data as an layer to the map. Coordinates are assumed to be in lat lon.
 * @param {Map} map
 * @param {Object[]} data
 * @param {Style} style
 * @returns {VectorLayer}
 */
export const addXyData = (map, data, style = createMarker()) => {
  const features = data.map(
    ({ geometry, ...fields }) =>
      new Feature({
        geometry: new Point(fromLonLat(geometry)),
        ...fields,
      })
  );
  const xyLayer = createVectorLayer({
    features,
    style,
  });
  map.addLayer(xyLayer);
  return xyLayer;
};

/**
 *
 * @param {Object} layerOptions
 * @returns {TileLayer}
 */
export const createTileWMS = ({ url, title, name, visible }) => {
  return new TileLayer({
    source: new TileWMS({
      url,
      params: { LAYERS: name, TILED: true },
      transition: 0,
    }),
    title,
    name,
    visible,
    type: 'wms',
  });
};
