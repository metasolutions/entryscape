import { getEntityTypeViews } from 'workbench/utils/entitytypes';
import EntityOverview from '../entitytypes/EntityOverview';
import ProjectsView from '../projects/ProjectsView';
import EntityTypesList from '../entitytypes';
import Overview from '../projects/ProjectOverview/ProjectOverview';
import { PROJECT_OVERVIEW_NAME, ENTITY_OVERVIEW_NAME } from './config';

export default {
  modules: [
    {
      name: 'workbench',
      productName: { en: 'Workbench', sv: 'Workbench', de: 'Workbench' },
      sidebar: true,
      startView: 'workbench__list',
      getLayoutProps: getEntityTypeViews,
    },
  ],
  views: [
    {
      name: 'workbench__list',
      class: ProjectsView,
      title: { en: 'Projects', sv: 'Projekt', de: 'Projekte' },
      route: '/workbench',
      module: 'workbench',
    },
    {
      name: PROJECT_OVERVIEW_NAME,
      class: Overview,
      title: { en: 'Overview', sv: 'Översikt', de: 'Überblick' },
      route: '/workbench/:contextId/overview',
      parent: 'workbench__list',
      module: 'workbench',
    },
    {
      name: 'workbench__entities__type',
      class: EntityTypesList,
      title: { en: 'Entities', sv: 'Entiteter', de: 'Objekte' },
      route: '/workbench/:contextId/entitytype/:entityName',
      parent: 'workbench__list',
      module: 'workbench',
      navbar: false,
    },
    {
      name: ENTITY_OVERVIEW_NAME,
      class: EntityOverview,
      title: {
        en: 'Entity',
        sv: 'Entitet',
        de: 'Objekt',
      },
      route: '/workbench/:contextId/entitytype/:entityName/:entryId/overview',
      parent: 'workbench__entities__type',
      module: 'workbench',
    },
  ],
};
