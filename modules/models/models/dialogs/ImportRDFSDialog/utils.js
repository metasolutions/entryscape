import { utils, Graph, namespaces } from '@entryscape/rdfjson';
import { promiseUtil, Context } from '@entryscape/entrystore-js';

/**
 *
 * @param {Graph} graph
 * @param {string[]} uris
 * @param {Context} context
 * @param {Function} importProgress
 * @returns {undefined}
 */
export const importClasses = (graph, uris, context, importProgress) =>
  promiseUtil.forEach(uris, async (uri) => {
    const subgraph = utils.extract(graph, new Graph(), uri);
    // Make sure it is explicitly typed
    graph.add(uri, 'rdf:type', 'rdfs:Class');
    await context.newLink(uri).setMetadata(subgraph).commit();
    importProgress();
  });

/**
 *
 * @param {Graph} graph
 * @param {string[]} uris
 * @param {Context} context
 * @param {Function} importProgress
 * @returns {undefined}
 */
export const importProperties = (graph, uris, context, importProgress) =>
  promiseUtil.forEach(uris, async (uri) => {
    const subgraph = utils.extract(graph, new Graph(), uri);
    // Make sure it is explicitly typed
    graph.add(uri, 'rdf:type', 'rdf:Property');
    await context.newLink(uri).setMetadata(subgraph).commit();
    importProgress();
  });

const isURI = (uri) => uri.startsWith('http');

/**
 *
 * @param {string[]} excludes
 * @param {Graph} graph
 * @returns {boolean}
 */
export const filterGenerator = (excludes = [], graph) => {
  const set = new Set();
  excludes.forEach((uri) => set.add(namespaces.expand(uri)));
  return (uri) => !set.has(uri) && graph.find(uri).length > 0 && isURI(uri);
};

export const excludeClasses = ['owl:Thing', 'rdfs:Class', 'rdf:Property'];

export const excludeProperties = [];
