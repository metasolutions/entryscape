import { useEffect } from 'react';
import PropTypes from 'prop-types';
import { Entry } from '@entryscape/entrystore-js';
import { useListModel, REFRESH } from 'commons/components/ListView';
import useAsync from 'commons/hooks/useAsync';
import { useSnackbar } from 'commons/hooks/useSnackbar';
import { useTranslation } from 'commons/hooks/useTranslation';
import esadContextNLS from 'admin/nls/esadContext.nls';
import { downgradeContextFromPremium } from 'commons/util/context';

const DowngradeProjectFromPremiumDialog = ({ entry: contextEntry }) => {
  const translate = useTranslation(esadContextNLS);
  const [, dispatch] = useListModel();
  const { runAsync } = useAsync(null);
  const [addSnackbar] = useSnackbar();

  useEffect(() => {
    runAsync(
      downgradeContextFromPremium(contextEntry).then(() => {
        dispatch({ type: REFRESH });
        addSnackbar({ message: translate('downgradeFromPremiumNotification') });
      })
    );
  }, [addSnackbar, contextEntry, dispatch, runAsync, translate]);

  return null;
};

DowngradeProjectFromPremiumDialog.propTypes = {
  entry: PropTypes.instanceOf(Entry),
};

export default DowngradeProjectFromPremiumDialog;
