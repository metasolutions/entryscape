import { utils } from '@entryscape/rdforms';

export default (params) => {
  const valueBindings = params.value.parent.getChildBindingsFor(
    params.value.item
  );
  const isChoice = params.value.item.getType() === 'choice';
  if (Array.isArray(valueBindings) && valueBindings.length > 0) {
    return valueBindings
      .filter((binding) => binding.isValid())
      .map((b) => {
        if (isChoice) {
          const choice = b.getChoice();
          if (choice && choice.label && !choice.load) {
            return utils.getLocalizedValue(choice.label).value;
          }
        }
        return b.getGist();
      })
      .join(', ');
  }

  return '';
};
