import Statistics from 'harvest/Statistics/index';

const StatisticsView = () => {
  return <Statistics />;
};

export default StatisticsView;
