import { V } from 'jointjs';
import { Entry } from '@entryscape/entrystore-js';
import { AttributedBox } from './AttributedBox';

let svgDocument;
export const initMeasureText = (paper) => {
  svgDocument = paper.svg;
};

export const measureText = (text, attrs) => {
  const vText = V('text');
  vText.attr(attrs);
  vText.text(text);
  vText.appendTo(svgDocument);
  const bbox = vText.getBBox();
  vText.remove();
  return bbox;
};

const getGenericProps = (label, fieldDefs, fields) => ({
  position: { x: 50, y: 100 },
  size: { width: 50, height: 50 },
  label,
  fields,
  fieldDefs,
  attrs: {},
});

/**
 * Creates a rectangle element from a form entry.
 *
 * @param {Entry} formEntry
 * @param {object[]} fields
 * @param {string} formLabel
 * @param {string[]} itemLabels
 * @returns {object}
 */
export const createRectangleFromForm = (
  formEntry,
  fields,
  formLabel,
  itemLabels
) => {
  const props = getGenericProps(formLabel, fields, itemLabels);
  props.formEntry = formEntry;
  const rectangle = new AttributedBox(props);
  rectangle.resizeWidth(measureText);
  return rectangle;
};

/**
 * Creates a rectangle element from a class entry.
 *
 * @param {Entry} entry
 * @param {object[]} fields
 * @param {string} classLabel
 * @param {string[]} itemLabels
 * @returns {object}
 */
export const createRectangleFromClass = (
  entry,
  fields,
  classLabel,
  itemLabels
) => {
  const props = getGenericProps(classLabel, fields, itemLabels);
  props.class = entry.getResourceURI();
  props.classEntry = entry;
  const rectangle = new AttributedBox(props);
  rectangle.resizeWidth(measureText);
  return rectangle;
};

/**
 * Creates a rectangle element from a uri.
 *
 * @param {string} uri
 * @param {object[]} fields
 * @param {string} label
 * @param {string[]} itemLabels
 * @returns {object}
 */
export const createRectangleFromURI = (uri, fields, label, itemLabels) => {
  const props = getGenericProps(label, fields, itemLabels);
  props.class = uri;
  const rectangle = new AttributedBox(props);
  rectangle.resizeWidth(measureText);
  return rectangle;
};
