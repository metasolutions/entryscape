import {
  ACTION_EDIT,
  ACTION_REVISIONS,
  ACTION_REMOVE,
} from 'commons/components/overview/actions';
import { withOverviewRefresh } from 'commons/components/overview';
import { canWriteMetadata } from 'commons/util/entry';
import DisconnectDialog from 'commons/components/EntityOverview/dialogs/DisconnectDialog';
import SuggestionChecklistDialog from '../dialogs/SuggestionChecklistDialog';
import ArchiveSuggestionDialog from '../dialogs/ArchiveSuggestionDialog';
import UnarchiveSuggestionDialog from '../dialogs/UnarchiveSuggestionDialog';
import { isArchived } from '../util';

export const sidebarActions = [
  {
    ...ACTION_EDIT,
    getProps: ({ entry }) => ({ disabled: isArchived(entry) }),
  },
  ACTION_REVISIONS,
  {
    id: 'checklist',
    Dialog: withOverviewRefresh(SuggestionChecklistDialog, 'onSave'),
    labelNlsKey: 'progressMenu',
    getProps: ({ entry }) => ({ disabled: isArchived(entry) }),
    highlightId: 'overviewActionChecklist',
  },
  {
    id: 'archive',
    Dialog: withOverviewRefresh(ArchiveSuggestionDialog, 'onArchive'),
    labelNlsKey: 'archiveMenu',
    isVisible: ({ entry }) => !isArchived(entry),
    highlightId: 'overviewActionArchive',
  },
  {
    id: 'unarchive',
    Dialog: withOverviewRefresh(UnarchiveSuggestionDialog, 'onUnarchive'),
    labelNlsKey: 'unArchiveMenu',
    isVisible: ({ entry }) => isArchived(entry),
  },
  ACTION_REMOVE,
];

export const datasetActions = [
  {
    id: 'disconnect',
    Dialog: DisconnectDialog,
    isVisible: ({ entry }) => canWriteMetadata(entry),
    labelNlsKey: 'disconnectMenuItemLabel',
  },
];
