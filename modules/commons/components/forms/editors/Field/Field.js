import PropTypes from 'prop-types';
import { Grid } from '@mui/material';
import { ClearButton, RemoveButton } from '../FieldControl';
import './Field.scss';

export const Field = ({
  children,
  onRemove,
  onClear,
  size,
  disabled,
  ...gridProps
}) => {
  return (
    <div className="escoField">
      <Grid container alignItems="center" {...gridProps}>
        {children}
      </Grid>
      {size > 1 ? (
        <RemoveButton disabled={disabled} onClick={onRemove} />
      ) : null}
      {size === 1 ? (
        <ClearButton disabled={disabled} onClick={onClear} />
      ) : null}
    </div>
  );
};

Field.propTypes = {
  onClear: PropTypes.func,
  onRemove: PropTypes.func,
  size: PropTypes.number,
  disabled: PropTypes.bool,
  children: PropTypes.node,
};

export default Field;
