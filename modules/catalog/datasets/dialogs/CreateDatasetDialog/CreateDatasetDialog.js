import PropTypes from 'prop-types';
import { useEffect } from 'react';
import { useESContext } from 'commons/hooks/useESContext';
import useAsync from 'commons/hooks/useAsync';
import {
  commitMetadata,
  RDF_TYPE_DATASET,
  RDF_TYPE_DATASET_SERIES,
} from 'commons/util/entry';
import {
  createPrototypeDatasetEntry,
  updateCatalogWithDataset,
} from 'catalog/datasets/DatasetOverview/utils/datasets';
// eslint-disable-next-line max-len
import CreateEntryByTemplateDialog from 'commons/components/EntryListView/dialogs/create/CreateEntryByTemplateDialog';
import { LEVEL_MANDATORY } from 'commons/components/rdforms/LevelSelector';
import useRestrictionDialog from 'commons/hooks/useRestrictionDialog';
import { useSnackbar } from 'commons/hooks/useSnackbar';
import { useTranslation } from 'commons/hooks/useTranslation';
import escaDatasetNLS from 'catalog/nls/escaDataset.nls';
import escoListNLS from 'commons/nls/escoList.nls';
import useErrorHandler from 'commons/errors/hooks/useErrorHandler';
import useDatasetRestriction from '../../useDatasetRestriction';

const CreateDialog = (props) => {
  const {
    closeDialog,
    actionParams: { numberOfDatasets, prototypeCallback },
    onCreate,
    rdfType = RDF_TYPE_DATASET,
    nlsKeys = {
      dialogHeader: 'createDatasetHeader',
      createSuccess: 'createDatasetSuccess',
      createFailure: 'createDatasetFail',
    },
  } = props;

  const { context } = useESContext();
  const { data: prototypeEntry, runAsync, error } = useAsync(null);
  const { isRestricted, contentPath } = useDatasetRestriction(
    numberOfDatasets,
    context
  );
  const translate = useTranslation([escaDatasetNLS, escoListNLS]);
  useRestrictionDialog({
    open: isRestricted,
    contentPath,
    callback: closeDialog,
  });
  const [addSnackbar] = useSnackbar();
  useErrorHandler(error);

  useEffect(() => {
    runAsync(
      createPrototypeDatasetEntry(context, rdfType).then(prototypeCallback)
    );
  }, [context, rdfType, runAsync, prototypeCallback]);

  const handleCreateEntry = async (datasetPrototypeEntry, graph) => {
    const datasetEntry = await commitMetadata(datasetPrototypeEntry, graph);

    await updateCatalogWithDataset(datasetEntry);
    await onCreate(datasetEntry);

    addSnackbar({
      message: translate(nlsKeys.createSuccess),
    });

    return datasetEntry;
  };

  if (isRestricted) return null;

  return prototypeEntry ? (
    <CreateEntryByTemplateDialog
      {...props}
      entry={prototypeEntry}
      createEntry={handleCreateEntry}
      editorLevel={LEVEL_MANDATORY}
      createHeaderLabel={translate(nlsKeys.dialogHeader)}
      createErrorNls={nlsKeys.createFailure}
    />
  ) : null;
};

CreateDialog.propTypes = {
  actionParams: PropTypes.shape({
    numberOfDatasets: PropTypes.number,
    prototypeCallback: PropTypes.func,
  }),
  closeDialog: PropTypes.func,
  onCreate: PropTypes.func,
  rdfType: PropTypes.oneOf([RDF_TYPE_DATASET, RDF_TYPE_DATASET_SERIES]),
  nlsKeys: PropTypes.shape({
    dialogHeader: PropTypes.string,
    createSuccess: PropTypes.string,
    createFailure: PropTypes.string,
  }),
};
export default CreateDialog;
